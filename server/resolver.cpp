// Copyright (C) 2000-2001 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "server.h"

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

bool Resolver::instance = false;

Resolver::Resolver() :
Server(keythreads.priResolver())
{
	if(instance)
		throw(this);
	instance = true;
	interval = 60l * keythreads.getResolver();
}

void Resolver::run(void)
{
	time_t last, now;
	long diff;
	Protocol *pro = Protocol::first;
	const char *addr;
	struct in_addr **bptr;
	struct hostent *hp;

	slog(Slog::levelInfo) << "resolver: starting" << interval << endl;

	time(&last);
	for(;;)
	{
		time(&now);
		diff = interval - (now - last);
		if(diff > 0)
			Thread::sleep(diff * 1000);
		time(&last);
		slog(Slog::levelDebug) << "resolver: updating" << endl;
		while(pro)
		{
			addr = pro->getLast("resolver");
			if(!addr)
			{
				pro = pro->next;
				continue;
			}
			hp = gethostbyname(addr);
			if(!hp)
			{
				pro = pro->next;
				continue;
			}
			bptr = (struct in_addr **)hp->h_addr_list;
			while(*bptr != NULL)
				++bptr;
			--bptr;
			pro->update(InetHostAddress(**bptr));
			pro = pro->next;
		}		
	}
}

void Resolver::stop(void)
{
	slog(Slog::levelDebug) << "resolver: stopping" << endl;
	terminate();
}

#ifdef	CCXX_NAMESPACES
};
#endif
