// Copyright (C) 2000-2001 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "server.h"
#include <cc++/strchar.h>

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

Fifo fifo;

#define	ANY ((Trunk *)(-1))

static unsigned short getDigitMask(const char *cp)
{
	static char *digits = "0123456789*#abcd";
	unsigned short mask = 0;
	const char *dp;

	if(!cp)
		return 0;

        while(*cp)
        {
                dp = strchr(digits, tolower(*cp));
                ++cp;
                if(dp)
                        mask |= (1 << (int)(dp - digits));
        }
        return mask;
}

Trunk *Fifo::getSymbol(char **argv)
{
	char result[256];
	Trunk *trunk = Driver::getTrunk(argv[1]);
	Trunk *rts = trunk;
	
	if(!trunk)
		return NULL;

	argv += 2;
	trunk->enterMutex();

	if(trunk->tgi.fd < 0)
		rts = NULL;
	
	while(*argv && trunk->tgi.fd > -1)
	{
		snprintf(result, sizeof(result), "#%s=%s\n",
			*argv, trunk->getSymbol(*argv));
		trunk->writeShell(result);
		++argv;
	}
	trunk->leaveMutex();
	return rts;
}

Trunk *Fifo::logResult(char **argv)
{
	char path[512];
	unsigned len = 0;
	unsigned argc = 1;
	unsigned fd;
	char *fmt = "%s";

	snprintf(path, sizeof(path), "%s/%s.%s",
		keypaths.getLast("logpath"), keyserver.getNode(), argv[0]);

	fd = ::open(path, O_WRONLY | O_APPEND | O_CREAT, 0660);
	if(fd < 0)
		return false;

	while(len < 500 && argv[argc])
	{
		snprintf(path + len, sizeof(path) - len, fmt, argv[argc++]);
		fmt = " %s";
		len = strlen(path);
	}
	path[len++] = '\n';
	path[len] = 0;
	::write(fd, path, len);
	::close(fd);
	return ANY;
}
	
Trunk *Fifo::optionCmd(char **argv)
{
	Trunk *trunk = Driver::getTrunk(argv[1]);
	const char *lang;
	time_t timer, adj;
	
	if(!trunk)
		return false;

	if(!argv[2])
		return false;

	trunk->enterMutex();
	
	if(trunk->tgi.fd < 0)
	{
		trunk->leaveMutex();
		return NULL;
	}

	if(!stricmp(argv[2], "voice") && argv[3])
	{
		trunk->setSymbol(SYM_VOICE, argv[3]);
		lang = keyvoices.getLast(argv[3]);
		if(lang)
		{
			if(getTranslator(lang) != NULL)
				trunk->setSymbol(SYM_LANGUAGE, lang);
		}	
	}
	else if(!stricmp(argv[2], "timer") && argv[3])
	{
		trunk->synctimer = 0;
		adj = atol(argv[3]);
		if(adj)
		{
			time(&timer);
			trunk->synctimer = timer + adj;
		}
	}
	else if(!stricmp(argv[2], "dtmf"))
	{
		trunk->tgi.dtmf = true;
	}
	else if(!stricmp(argv[2], "nodtmf"))
	{
		trunk->tgi.dtmf = false;
	}

	trunk->leaveMutex();
	return trunk;
}
	
Trunk *Fifo::collectCmd(char **argv)
{
	TrunkEvent event;
	Trunk *trunk = Driver::getTrunk(argv[1]);
	Trunk *rts = trunk;
	timeout_t timeout = 5000;
	unsigned short term = 0;

	if(!trunk)
		return NULL;

	if(!argv[2])
		return NULL;

	if(argv[3])
	{
		timeout = atoi(argv[3]) * 1000;
		term = getDigitMask(argv[4]);
	}

	trunk->enterMutex();

	if(trunk->tgi.fd < 0)
	{
		trunk->leaveMutex();
		return NULL;
	}

	event.id = TRUNK_MAKE_STEP;
	event.parm.step = TRUNK_STEP_COLLECT;
	trunk->data.collect.count = atoi(argv[2]);
	if(trunk->data.collect.count > MAX_DIGITS)
		trunk->data.collect.count = MAX_DIGITS;
	trunk->data.collect.var = NULL;
	trunk->data.collect.map = NULL;
	trunk->data.collect.term = term;
	trunk->data.collect.ignore = 0;
	trunk->data.collect.timeout = timeout;
	if(!trunk->postEvent(&event))
		rts = NULL;
	trunk->leaveMutex();
	return rts;
}

Trunk *Fifo::sendEvent(char **argv)
{
        TrunkEvent event;
        Trunk *trunk = Driver::getTrunk(argv[1]);
        
        if (!trunk) return(NULL);
        
        event.id = TRUNK_SEND_MESSAGE;
        event.parm.send.src = NULL;
        if (argv[2]) 
		event.parm.send.msg = argv[2];
        else 
		event.parm.send.msg = "";

        trunk->postEvent(&event);
	return trunk;
}

Trunk *Fifo::clearRDGroup(char **argv)
{
	const char *members = argv[3];
	unsigned port;
	Trunk *trunk;
	Driver *drv = Driver::drvFirst;
	unsigned dig;

	if(!members)
		return NULL;

	while(*members)
	{
		if(!isdigit(*members))
		{
			++members;
			continue;
		}
		dig = *(members++) - '0';

		while(drv)
		{
			port = drv->getTrunkCount();
			while(port--)
			{
				trunk = drv->getTrunkPort(port);
				if(!trunk)
					continue;

				trunk->dialgroup[dig] = false;
			}
			drv = drv->drvNext;
		}
	}
	return ANY;
}

Trunk *Fifo::clearRing(char **argv)
{
	Trunk *trunk;
	char *members;
	unsigned dig;
	char name[128];

	if(!argv[2])
		return NULL;

	if(!stricmp(argv[2], "group"))
		return clearRDGroup(argv);

	snprintf(name, sizeof(name), "Ext/%s", argv[2]);
	trunk = Driver::getTrunk(name);
	if(!trunk)
		return NULL;

	members = argv[3];
	if(!members)
		members = "0123456789";

	while(*members)
	{
		if(!isdigit(*members))
		{
			++members;
			continue;
		}
		dig = *(members++) - '0';
		trunk->dialgroup[dig] = false;
	}	
	return ANY;
}

Trunk *Fifo::clearDial(char **argv)
{
        Trunk *trunk;
        char *members;
        unsigned dig;
	char name[128];

        if(!argv[2])
                return NULL;

        if(!stricmp(argv[2], "group"))
                return clearRDGroup(argv);

	snprintf(name, sizeof(name), "PSTN/%s", argv[2]);

	trunk = Driver::getTrunk(name);
        if(!trunk)
                return NULL;

        members = argv[3];
        if(!members)
                members = "0123456789";

        while(*members)
        {
                if(!isdigit(*members))
                {
                        ++members;
                        continue;
                }
                dig = *(members++) - '0';
                trunk->dialgroup[dig] = false;
        }
	return ANY;
}

Trunk *Fifo::assignRing(char **argv)
{
        Trunk *trunk;
        char *members;
        unsigned dig;
	char name[128];

        if(!argv[2])
                return false;

	snprintf(name, sizeof(name), "Ext/%s", argv[2]);
	trunk = Driver::getTrunk(name);
        if(!trunk)
                return NULL;

        members = argv[3];
        if(!members)
                members = "0123456789";

        while(*members)
        {
                if(!isdigit(*members))
                {
                        ++members;
                        continue;
                }
                dig = *(members++) - '0';
                trunk->dialgroup[dig] = true;
        }
	return trunk;
}

Trunk *Fifo::assignDial(char **argv)
{
        Trunk *trunk;
        char *members;
        unsigned dig;
	char name[128];

        if(!argv[2])
                return NULL;

	snprintf(name, sizeof(name), "Ext/%s", argv[2]);
	trunk = Driver::getTrunk(name);
        if(!trunk)
                return NULL;

        members = argv[3];
        if(!members)
                members = "0123456789";

        while(*members)
        {
                if(!isdigit(*members))
                {
                        ++members;
                        continue;
                }
                dig = *(members++) - '0';
                trunk->dialgroup[dig] = true;
        }
	return trunk;
}

Trunk *Fifo::stop(char **argv)
{
	TrunkEvent event;
	Trunk *trunk = Driver::getTrunk(argv[1]);
	
	if(!trunk)
		return NULL;

	if(!trunk->isRunning())
		return NULL;

	event.id = TRUNK_STOP_STATE;
	trunk->postEvent(&event);
	return trunk;
}
	
Trunk *Fifo::shellStart(char **argv)
{
	struct stat ino;
	int fd;
	TrunkEvent event;
	Trunk *trunk = Driver::getTrunk(argv[1]);
	if(!trunk)
		return NULL;

	if(!argv[2])
		return NULL;

	stat(argv[2], &ino);
	if(!S_ISFIFO(ino.st_mode))
		return NULL;	

	fd = ::open(argv[2], O_WRONLY | O_NONBLOCK);
	remove(argv[2]);
	if( fd < 0)
		return NULL;

	event.id = TRUNK_SHELL_START;
	event.parm.fd = fd;
	if(trunk->postEvent(&event))
		return trunk;
	close(fd);
	return NULL;
}
	
Trunk *Fifo::waitPid(char **argv)
{
	int pid;
	unsigned short seq;
	TrunkEvent event;
	Trunk *trunk = Driver::getTrunk(argv[1]);
	if(!trunk)
		return NULL;

	if(!argv[2])
		return NULL;

	if(argv[3])
		seq = atoi(argv[3]);
	else
		seq = trunk->tgi.seq;

	pid = atoi(argv[2]);
	event.id = TRUNK_WAIT_SHELL;
	event.parm.waitpid.pid = pid;
	event.parm.waitpid.seq = seq;
	if(trunk->postEvent(&event))
		return trunk;
	kill(pid, SIGHUP);
	return NULL;
}

Trunk *Fifo::addSymbol(char **argv)
{
	unsigned len;
	Script::Symbol *sym;
	Trunk *trunk = Driver::getTrunk(argv[1]);
	if(!trunk)
		return NULL;

	if(!argv[3] || !argv[2])
		return NULL;

	trunk->enterMutex();
	sym = trunk->mapSymbol(argv[2]);
	if(sym && sym->type != symCONST)
	{
		len = strlen(sym->data);
		snprintf(sym->data + len, sym->size + 1 - len, 
			"%s", argv[3]);
		/*if(sym->flags.commit)
			trunk->commit(sym);*/
	}
	trunk->leaveMutex(); 
	return trunk;
}

Trunk *Fifo::setSize(char **argv)
{
	Trunk *trunk = Driver::getTrunk(argv[1]);
	if(!trunk)
		return NULL;

	if(!argv[2] || !argv[3])
		return NULL;

	// XXX fixme
	/*if(trunk->setVariable(argv[2], atoi(argv[3])))
		return trunk;*/

	return NULL;
}

Trunk *Fifo::setSymbol(char **argv)
{
	Script::Symbol *sym;
	Trunk *trunk = Driver::getTrunk(argv[1]);
	Trunk *rts = trunk;
	if(!trunk)
		return NULL;

	if(!argv[3] || !argv[2])
		return NULL;

	// XXX fixme
	/*trunk->enterMutex();
	if(!trunk->setVariable(argv[2], 0, argv[3]))
		rts = NULL;
	trunk->leaveMutex();*/
	return rts;
}

Trunk *Fifo::reload(char **argv)
{
	Module *mod = Module::cmdFirst;
	int argc = 1;
	Trunk *rts = ANY;
	const char *id;

	if(!argv[1])
	{
		while(mod)
		{
			mod->reload();
			mod = mod->cmdNext;
		}
		return ANY;
	}

	while(argv[argc])
	{
		mod = getModule(MODULE_FIFO, argv[argc++]);
		if(mod)
			mod->reload();
		else
			rts = NULL;
	}
	return rts;
}

Trunk *Fifo::testScript(char **argv)
{
	char *pass[2];
	Trunk *trunk;
	TrunkEvent event;
	char name[128];

	if(!argv[1])
		return NULL;

	snprintf(service, sizeof(service), "test::%s", argv[1]);

	pass[0] = service;
	pass[1] = NULL;
	argv += 2;

	while(*argv)
	{
		snprintf(name, sizeof(name), "PSTN/%s", *argv);
		trunk = Driver::getTrunk(name);
		if(!trunk)
			continue;

		event.id = TRUNK_RING_START;
		event.parm.argv = pass;
		trunk->postEvent(&event);
		++argv;
	}
	return ANY;
}

Trunk *Fifo::ringScript(char **argv)
{
	Trunk *trunk = Driver::getTrunk(argv[1]);
	TrunkEvent event;

	if(!trunk)
		return NULL;

	event.id = TRUNK_RING_START;
	event.parm.argv = &argv[2];

	if(!trunk->postEvent(&event))
		return NULL;
	return trunk;
}

Trunk *Fifo::redirectScript(char **argv)
{
	Trunk *trunk = Driver::getTrunk(argv[1]);
	TrunkEvent event;

	if(!trunk || !argv[2])
		return NULL;

	event.id = TRUNK_RING_REDIRECT;
	event.parm.argv = &argv[2];

	if(!trunk->postEvent(&event))
		return NULL;
	return trunk;
}

Trunk *Fifo::reqScript(char **argv)
{
	TrunkGroup *grp;
	char *exp = argv[1];
	
	if(!exp || !argv[2])
		return NULL;

	grp = getGroup(argv[2]);
	if(!grp || !argv[3])
		return NULL;

	request(grp, &argv[3], atoi(exp));
	return ANY;
}

Trunk *Fifo::startScript(char **argv)
{
	Trunk *trunk = Driver::getTrunk(argv[1]);
	TrunkGroup *group = getGroup(argv[1]);	
	TrunkEvent event;
	Driver *drv = Driver::drvFirst;
	int port;
	unsigned threashold = atoi(argv[0] + 5);
	const char *cp = strchr(argv[1], '-');

	if(!trunk)
		trunk = Driver::getTrunk(argv[1], true);

	if((!trunk && !group) || !argv[2])
		return NULL;

	if(trunk && !group)
	{
		event.id = TRUNK_START_SCRIPT;
		event.parm.argv = &argv[2];
		if(!trunk->postEvent(&event))
			return NULL;
		return trunk;
	}
	if(!group)
		return NULL;

	while(drv)
	{
		port = drv->getTrunkCount();
		while(port--)
		{
			if(drv->getTrunkGroup(port) != group)
				continue;

			trunk = drv->getTrunkPort(port);
			if(!trunk)
			{
				trunk = drv->getOutboundTrunk(port);
				if(!trunk)
					continue;
			}

			event.id = TRUNK_START_SCRIPT;
			event.parm.argv = &argv[2];
			if(trunk->postEvent(&event))
				return trunk;
		}
		drv = drv->drvNext;
	}
	return NULL;
}

Trunk *Fifo::postKey(char **argv)
{
	Trunk *trunk = Driver::getTrunk(argv[1]);
	char *digits = argv[2];
	TrunkEvent event;
	Trunk *rtn = trunk;

	if(!trunk || !argv[2])
		return NULL;

	while(digits && rtn)
	{
		event.id = TRUNK_DTMF_KEYUP;
		event.parm.dtmf.duration = 40;
		event.parm.dtmf.e1 = event.parm.dtmf.e2 = 0;
		switch(*digits)
		{
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			event.parm.dtmf.digit = *digits - '0';
			if(!trunk->postEvent(&event))
				rtn = NULL;
			break;
		case '*':
			event.parm.dtmf.digit = 10;
			if(!trunk->postEvent(&event))
				rtn = NULL;
			break;
		case '#':
			event.parm.dtmf.digit = 11;
			if(!trunk->postEvent(&event))
				rtn = NULL;
			break; 
		default:
			rtn = NULL;
		}
		if(*(++digits))
			Thread::sleep(60);
	}
	return rtn;
}

Trunk *Fifo::exitPid(char **argv)
{
	Trunk *trunk = Driver::getTrunk(argv[1]);
	TrunkEvent event;

	if(!trunk)
		return NULL;

	event.id = TRUNK_EXIT_SHELL;
	if(argv[2])
		event.parm.exitpid.status = atoi(argv[2]);
	else
		event.parm.exitpid.status = 0;

	if(argv[3])
		event.parm.exitpid.seq = atoi(argv[3]);
	else
		event.parm.exitpid.seq = trunk->tgi.seq;

	if(!trunk->postEvent(&event))
		return NULL;
	return trunk;
}

Trunk *Fifo::text(char **argv)
{
	TrunkEvent event;
	Trunk *trunk = Driver::getTrunk(argv[1]);
	int pid, seq, argc = 4;
	Trunk *rts = trunk;
	char msg[512];
	
	msg[0] = 0;

	if(!trunk)
		return NULL;

	if(!argv[2])
		return NULL;

	seq = atoi(argv[2]);

	if(!argv[3])
		return NULL;

	pid = atoi(argv[3]);
	
	if(!argv[4])
		argv[4] = "";

	trunk->enterMutex();
	if(trunk->asr.seq != seq)
	{
		trunk->leaveMutex();
		kill(pid, SIGHUP);
		return NULL;
	}

	if(!stricmp(argv[0], "partial"))
	{
		if(!trunk->asr.partial)
		{
			trunk->leaveMutex();
			return trunk;
		}
		else
			event.id = TRUNK_ASR_PARTIAL;
	}
	else
		event.id = TRUNK_ASR_TEXT;

	msg[0] = 0;

	while(argv[argc])
	{
		if(msg[0] != '\0')
			strcat(msg, ",");
		strcat(msg, argv[argc++]);
	}

	event.parm.send.msg = msg;

	if(!trunk->postEvent(&event))
		rts = NULL;

	trunk->leaveMutex();
	return rts;
}

Trunk *Fifo::asr(char **argv)
{
	TrunkEvent event;
	Trunk *trunk = Driver::getTrunk(argv[1]);
	int pid, seq;

	if(!argv[2])
		return NULL;

	if(argv[3])
		seq = atoi(argv[3]);
	else
		return NULL;

	if(argv[4])
		pid = atoi(argv[4]);
	else
		pid = 0;

	if(!trunk)
	{
		if(pid)
			kill(pid, SIGHUP);
		remove(argv[2]);
		return NULL;
	}

	trunk->enterMutex();
	if(trunk->asr.seq != seq)
	{
		if(pid)
			kill(pid, SIGHUP);
		remove(argv[2]);
		trunk->leaveMutex();
		return NULL;
	}

	trunk->asr.fd = ::open(argv[2], O_WRONLY | O_NONBLOCK);
	remove(argv[2]);
	if(trunk->asr.fd < 0)
	{
		if(pid)
			kill(pid, SIGHUP);
		trunk->leaveMutex();
		return NULL;
	}
	if(argv[4] && argv[5])
		trunk->asr.rate = atoi(argv[5]);
	trunk->asr.pid = pid;
	event.id = TRUNK_ASR_START;
	trunk->postEvent(&event);
	trunk->leaveMutex();
	return trunk;
}

Trunk *Fifo::run(char **argv)
{
	TrunkGroup *group;
	Trunk *trunk;
	TrunkEvent event;
	Driver *drv = Driver::drvFirst;
	int port;
	char script[128];
	char parent[65];
	unsigned short seq;
	int fd;
	Trunk *rts = NULL;
	trunkevent_t id = TRUNK_START_SCRIPT;
	unsigned threashold = atoi(argv[0] + 3);

	if(!argv[1])
		return NULL;

	group = getGroup(argv[1]);
	trunk = Driver::getTrunk(argv[1]);

	fd = ::open(argv[2], O_WRONLY | O_NONBLOCK);
	if(fd < 0)
		return NULL;

	if(!argv[3])
		return NULL;

	if(argv[3])
		seq = atoi(argv[3]);
	else
		return NULL;

	if(argv[4])
		if(strchr(argv[4], '='))
			id = TRUNK_RING_START;

	if(!trunk && !group)
	{
		snprintf(script, sizeof(script), "fail %d bad port or group\n", seq);
		write(fd, script, strlen(script));
		close(fd);
		return NULL;
	}

	if(trunk && !group)
	{
		event.id = id;
		event.parm.argv = &argv[4];
		trunk->enterMutex();
		if(trunk->postEvent(&event))
			rts = trunk;
	}	

	if(!threashold && atoi(argv[4]) > 0)
	{
		threashold = atoi(argv[4]);
		++argv;
	}

	if(threashold > 0)
		if(threashold > group->getStat(STAT_AVAIL_CALLS))
		{
			snprintf(script, sizeof(script), "fail %d call threashold\n", seq);
			write(fd, script, strlen(script));
			close(fd);
			return NULL;
		}

	while(group && drv)
	{
		port = drv->getTrunkCount();
		while(port--)
		{
			if(drv->getTrunkGroup(port) != group)
				continue;

			trunk = drv->getTrunkPort(port);
			if(!trunk)
				continue;

			trunk->enterMutex();
			event.id = id;
			event.parm.argv = &argv[4];
			if(trunk->postEvent(&event))
				rts = trunk;
			if(rts)
				break;
			trunk->leaveMutex();
			trunk = NULL;
			++port;
		}
		drv = drv->drvNext;
	}

	if(!rts)
	{
		snprintf(script, sizeof(script), "fail %d no script\n", seq);
		write(fd, script, strlen(script));
		close(fd);
	}
	else if(trunk)
	{
		trunk->rpc.fd = fd;
		trunk->rpc.seq = seq;
		trunk->writeRPC("init", trunk->getSymbol(SYM_GID));
	}
	if(trunk)
		trunk->leaveMutex();
	return rts;
}

Trunk *Fifo::hangupLine(char **argv)
{
	TrunkGroup *group = getGroup(argv[1]);
	Trunk *trunk = Driver::getTrunk(argv[1]);
	TrunkEvent event;
	Driver *drv;
	int port;

	if(trunk && !group)
	{
		event.id = TRUNK_STOP_DISCONNECT;
		if(trunk->postEvent(&event))
			return trunk;
		else
			return NULL;
	}

	if(!group)
		return NULL;

	drv = Driver::drvFirst;
	while(drv)
	{
		for(port = 0; port < drv->getTrunkCount(); ++port)
		{
			if(drv->getTrunkGroup(port) != group)
				continue;

			trunk = drv->getTrunkPort(port);
			if(!trunk)
				continue;

			event.id = TRUNK_STOP_DISCONNECT;
			trunk->postEvent(&event);
		}
		drv = drv->drvNext;
	}
	return ANY;
}

Trunk *Fifo::busyLine(char **argv)
{
	TrunkGroup *group = getGroup(argv[1]);
	Trunk *trunk = Driver::getTrunk(argv[1]);
	Driver *drv = Driver::drvFirst;
	TrunkEvent event;
	int port;

	if(trunk && !group)
	{
		event.id = TRUNK_MAKE_BUSY;
		if(trunk->postEvent(&event))
			return trunk;
		return NULL;
	}
	if(!group)
		return NULL;

	while(drv)
	{
		for(port = 0; port < drv->getTrunkCount(); ++port)
		{
			if(drv->getTrunkGroup(port) != group)
				continue;

			trunk = drv->getTrunkPort(port);
			if(!trunk)
				continue;

			event.id = TRUNK_MAKE_BUSY;
			trunk->postEvent(&event);
		}
		drv = drv->drvNext;
	}
	return ANY;
}

Trunk *Fifo::setSpan(char **argv)
{
	TrunkEvent event;
	unsigned span;
	Driver *drv = Driver::drvFirst;

	const char *drvname = argv[1];
	const char *cspan = argv[2];
	const char *mode = argv[3];

	if(!drvname || !cspan || !mode)
		return NULL;

	span = atoi(cspan);
	if(!stricmp(mode, "busy"))
		event.id = TRUNK_MAKE_BUSY;
	else if(!stricmp(mode, "idle") || !stricmp(mode, "up"))
		event.id = TRUNK_MAKE_IDLE;
	else if(!stricmp(mode, "stop") || !stricmp(mode, "down"))
		event.id = TRUNK_MAKE_STANDBY;
	else
		return NULL;

	while(drv)
	{
		if(!strncasecmp(drv->getName(), drvname, strlen(drvname)))
		{
			if(drv->spanEvent(span, &event))
				return ANY;
			return NULL;
		}
		drv = drv->drvNext;
	}

	return NULL;
}


Trunk *Fifo::setCard(char **argv)
{
        TrunkEvent event;
        unsigned card;
	Driver *drv = Driver::drvFirst;

	const char *drvname = argv[1];
        const char *ccard = argv[2];
        const char *mode = argv[3];

        if(!drvname || !ccard || !mode)
                return NULL;

        card = atoi(ccard);
        if(!stricmp(mode, "busy"))
                event.id = TRUNK_MAKE_BUSY;
        else if(!stricmp(mode, "idle") || !stricmp(mode, "up"))
                event.id = TRUNK_MAKE_IDLE;
        else if(!stricmp(mode, "stop") || !stricmp(mode, "down"))
                event.id = TRUNK_MAKE_STANDBY;
        else
                return NULL;

	while(drv)
	{
		if(!strncasecmp(drv->getName(), drvname, sizeof(drvname)))
		{
			if(drv->cardEvent(card, &event))
				return ANY;
			return NULL;
		}
		drv = drv->drvNext;
	}

	return NULL;
}

Trunk *Fifo::idleLine(char **argv)
{
	TrunkGroup *group = getGroup(argv[1]);
	Trunk *trunk = Driver::getTrunk(argv[1]);
	TrunkEvent event;
	Driver *drv = Driver::drvFirst;
	int port;

	if(trunk && !group)
	{
		event.id = TRUNK_MAKE_IDLE;
		if(trunk->postEvent(&event))
			return trunk;
		return NULL;
	}

	if(!group)
		return NULL;

	while(drv)
	{
		for(port = 0; port < drv->getTrunkCount(); ++port)
		{
			if(drv->getTrunkGroup(port) != group)
				continue;

			trunk = drv->getTrunkPort(port);
			if(!trunk)
				continue;

			event.id = TRUNK_MAKE_IDLE;
			trunk->postEvent(&event);
		}
		drv = drv->drvNext;
	}
	return ANY;
}

#ifdef	SCHEDULER_SERVICES

Trunk *Fifo::setSchedule(char **argv)
{
	if(argv[1])
	{
		strcpy(schedule, argv[1]);
		if(!stricmp(schedule, "*"))
			scheduler.altSchedule(NULL);
		else
			scheduler.altSchedule(schedule);
	}
	else
		scheduler.altSchedule(NULL);
	return ANY;
}

#endif
	
Trunk *Fifo::command(const char *cmd, ostream *fd)
{
	Module *next;
	char buffer[PIPE_BUF / 2];
	Trunk *rts = NULL;
	char *args[65];
	int argc = 0;
	char **argv = args;
	char *arg;
	char *sp;
	char *ptr;
	const char *token = keyserver.getToken();
	int tlen = strlen(token);
	int pid = 0;

	if(!fd)
		fd = &slog;

	strncpy(buffer, cmd, sizeof(buffer) - 1);
	buffer[sizeof(buffer) - 1] = 0;

	enterMutex();
	slog(Slog::levelDebug) << "fifo: cmd=" << buffer << endl;
	if(strstr(buffer, token))
	{
		ptr = buffer;
		while(isspace(*ptr))
			++ptr;
	
		ptr = strtok_r(ptr, "\n", &sp);	
		while(NULL != (sp = strstr(ptr, token)))
		{
			argv[argc++] = ptr;
			*sp = 0;
			ptr = sp + tlen;
		}
		while(isspace(*ptr))
			++ptr;
		if(*ptr)
			argv[argc++] = ptr;
	}
	else
	{
		argv[argc++] = strtok_r(buffer, " \t\n\r", &sp);
		while(argc < 64)
		{
			arg = strtok_r(NULL, " \t\n\r", &sp);
			if(!arg)
				break;
			argv[argc++] = arg;
		}
	}
	argv[argc] = NULL;

	if(!argv[0])
	{
		leaveMutex();
		return NULL;
	}

	if(!*argv[0])
	{
		leaveMutex();
		return NULL;
	}

	if(isdigit(**argv))
	{
		pid = atoi(argv[0]);
		++argv;
		--argc;
		if(!argv[0])
		{
			++argc;
			--argv;
		}
	}

	next = Module::cmdFirst;

	while(next && !rts)
	{
		if(next->command(argv, fd))
			rts = ANY;
		next = next->cmdNext;
	}

	if(!stricmp(argv[0], "service"))
	{
		if(!argv[1])
		{
			argv[0] = "-";
			rts = NULL;
		}
		else if(!stricmp(argv[1], "up"))
			argv[0] = "up";
	}

	if(!stricmp(argv[0], "restart"))
	{
		restart_server = true;
		raise(SIGINT);
		rts = ANY;
	}
	else if(!stricmp(argv[0], "compile"))
	{
		new aaImage(aascript);
		rts = ANY;
	}		
	else if(!stricmp(argv[0], "wait"))
		rts = waitPid(argv);
	else if(!stricmp(argv[0], "exit"))
		rts = exitPid(argv);
	else if(!stricmp(argv[0], "get") || !stricmp(argv[0], "query"))
		rts = getSymbol(argv);
	else if(!stricmp(argv[0], "set"))
		rts = setSymbol(argv);
	else if(!stricmp(argv[0], "add"))
		rts = addSymbol(argv);
	else if(!stricmp(argv[0], "size"))
		rts = setSize(argv);
	else if(!stricmp(argv[0], "debug"))
	{
		if(debug->debugFifo(argv))
			rts = ANY;
		else
			rts = NULL;
	}
	else if(!stricmp(argv[0], "ring"))
		rts = ringScript(argv);
	else if(!stricmp(argv[0], "redirect"))
		rts = redirectScript(argv);
	else if(!stricmp(argv[0], "busy"))
		rts = busyLine(argv);
	else if(!stricmp(argv[0], "idle"))
		rts = idleLine(argv);
	else if(!stricmp(argv[0], "span"))
		rts = setSpan(argv);
	else if(!stricmp(argv[0], "card"))
		rts = setCard(argv);
	else if(!strnicmp(argv[0], "start", 5))
		rts = startScript(argv);
	else if(!strnicmp(argv[0], "run", 3))
		rts = run(argv);
	else if(!stricmp(argv[0], "stop"))
		rts = stop(argv);	
	else if(!stricmp(argv[0], "request"))
		rts = reqScript(argv);
	else if(!stricmp(argv[0], "disconnect") || !stricmp(argv[0], "hangup"))
		rts = hangupLine(argv);
	else if(!stricmp(argv[0], "post") || !stricmp(argv[0], "key"))
		rts = postKey(argv);
#ifdef	SCHEDULER_SERVICES
	else if(!stricmp(argv[0], "schedule"))
		rts = setSchedule(argv);
#endif
	else if(!stricmp(argv[0], "reload"))
		rts = reload(argv);
	else if(!stricmp(argv[0], "send"))
		rts = sendEvent(argv);
	else if(!stricmp(argv[0], "shell"))
		rts = shellStart(argv);
	else if(!stricmp(argv[0], "collect"))
		rts - collectCmd(argv);
	else if(!stricmp(argv[0], "option"))
		rts = optionCmd(argv);
	else if(!stricmp(argv[0], "cdr") || !stricmp(argv[0], "calls"))
	{
		argv[0] = "calls";
		rts = logResult(argv);
	}
	else if(!stricmp(argv[0], "audit") || !stricmp(argv[0], "stats") || !stricmp(argv[0], "errors"))
		rts = logResult(argv); 
	else if(!stricmp(argv[0], "asr"))
		rts = asr(argv);
	else if(!stricmp(argv[0], "text") || !stricmp(argv[0], "partial"))
		rts = text(argv);
	leaveMutex();
#ifdef	SIGUSR2
	if(pid && rts)
		kill(pid, SIGUSR1);
	else if(pid && !rts)
		kill(pid, SIGUSR2);
#endif
	return rts;
}


#ifdef	CCXX_NAMESPACES
};
#endif
