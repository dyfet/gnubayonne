// Copyright (C) 2000-2001 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "server.h"
#include <cstdlib>
#include <climits>

#if defined(HAVE_SYS_PARAM_H)
#include <sys/param.h>
#endif

#ifndef	PATH_MAX
#define	PATH_MAX	MAXPATHLEN
#endif

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

#ifdef	XML_SCRIPTS

URLAudio::URLAudio() :
AudioFile(), URLStream()
{
	offset = 0;
	setProtocol(protocolHttp1_0);
}

URLAudio::~URLAudio()
{
	AudioFile::close();
}

bool URLAudio::afOpen(const char *path)
{
	offset = 0;

	setProxy(NULL, 0);

	if(URLStream::errSuccess == get(path, 0))
	{
		file.fd = so;
		return true;
	}
	file.fd = -1;
	return false;
}

bool URLAudio::isOpen(void)
{
	if(so > -1)
		return true;

	return false;
}

bool URLAudio::hasPositioning(void)
{
	if(Socket::state == Socket::STREAM)
		return true;

	return false;
}

unsigned long URLAudio::getTransfered(void)
{
	unsigned long pos;

	if(so < 0)
		return 0;

	if(Socket::state == Socket::STREAM)
		pos = ::lseek(so, 0l, SEEK_CUR) - getHeader();
	else
		pos = offset;

	return toSamples(getEncoding(), pos);
}

bool URLAudio::afPeek(unsigned char *buffer, unsigned len)
{
	int got;
	if(Socket::state == Socket::STREAM)
		got = ::read(so, buffer, len);
	else
		got = ::recv(so, buffer, len, MSG_PEEK | MSG_WAITALL);
	if(got < (int)len)
		return false;
	return true;
}

bool URLAudio::afSeek(unsigned long pos)
{
	char temp[128];
	unsigned len;
	if(Socket::state == Socket::STREAM)
	{
		::lseek(so, pos, SEEK_SET);
		return true;
	}
	if(offset == pos)
		return true;

	if(offset > pos)
		return false;	

	while(offset < pos)
	{
		len = pos - offset;
		if(len < sizeof(temp))
			len = sizeof(temp);
		len = ::recv(so, temp, len, MSG_WAITALL);
		if(len < 1)
			return false;
		offset += len;
	}
	return true;
}

int URLAudio::afRead(unsigned char *buffer, unsigned len)
{
	if(Socket::state == Socket::STREAM)
	{
		len = ::read(so, buffer, len);
	}
	else
	{
		len = ::recv(so, buffer, len, MSG_WAITALL);
	}
	if(len > 0)
		offset += len;
	return len;
}


void URLAudio::afClose(void)
{
	if(file.fd != -1 && file.fd != so)
		::close(file.fd);
	file.fd = -1;
	URLStream::close();
}

#endif

#ifdef	HAVE_REALPATH

bool permitAudioAccess(const char *path, bool write)
{
	char *cp, *dp, *ext;

	char source[PATH_MAX];
	char target[PATH_MAX];
	const char *var = keypaths.getDatafiles();
	const char *alt = keypaths.getLast("altprefix");
	const char *prompts = keypaths.getLast("prompts");
	unsigned lalt, lprompts, lvar;
		
	if(!alt)
		alt = keypaths.getLast("altprompts");

	snprintf(source, sizeof(source), "%s", path);
	cp = strrchr(source, '/');
	if(!cp)
		return false;

	*cp = 0;

	if(!realpath(source, target))
		return false;

	if(!isDir(target))
		return false;

	cp = strrchr(target, '/');
	if(!cp)
		return false;

	if(write)
	{
		ext = strrchr(path, '.');
		if(!ext)
			return false;

		if(stricmp(ext, ".au") &&
		   stricmp(ext, ".wav") &&
		   stricmp(ext, ".al") &&
		   stricmp(ext, ".ul") &&
		   stricmp(ext, ".snd") &&
		   stricmp(ext, ".raw") &&
		   stricmp(ext, ".vox") &&
		   stricmp(ext, ".gsm"))
			return false;
	}

	++cp;

	if(!stricmp(cp, ".bayonne"))
		return true;

	if(*cp == '.')
		return false;

	realpath(alt, source);
	alt = source;
	lalt = strlen(alt);

	if(!strnicmp(alt, target, lalt))
	{
		if(target[lalt] == '/' || !target[lalt])
			return true;
		else
			return false;
	}

	if(!write && !strnicmp("/var/", target, 5))
		return true;

	realpath(var, source);
	var = source;
	lvar = strlen(var);

	if(!strnicmp(var, target, lvar))
	{
		if(target[lvar] != '/')
			return false;

		++lvar;
		if(target[lvar] == '.')
			return false;

		return true;
	}

	if(write)
		return false;

	realpath(prompts, source);
	prompts = source;
	lprompts = strlen(prompts);

	if(!strnicmp(prompts, target, lprompts))
	{
		if(target[lprompts] != '/')
			return false;

		return true;
	}

	return true;
}

bool permitFileAccess(const char *path)
{
	char *cp, *dp, *text;

	char source[PATH_MAX];
	char target[PATH_MAX];
	const char *var = keypaths.getDatafiles();
	unsigned lvar;

	snprintf(source, sizeof(source), "%s", path);
	cp = strrchr(source, '/');
	if(!cp)
		return false;

	*cp = 0;
	if(!realpath(source, target))
		return false;

	if(!isDir(target))
		return false;

	cp = strrchr(target, '/');
	if(!cp)
		return false;

	++cp;

	if(!stricmp(cp, ".bayonne"))
		return true;

	realpath(var, source);
	var = source;
	lvar = strlen(var);

	if(!strnicmp(var, target, lvar))
	{
		if(target[lvar] != '/')
			return false;
		++lvar;
		if(target[lvar] == '.')
			return false;

		return true;
	}
	else
		return false;
}

#else

bool permitFileAccess(const char *path)
{
	if(strstr(path, ".."))
		return false;
	
	if(*path == '/')
	{
		if(strstr(path, "/.bayonne/"))
			return true; 
		return false;
	}

	if(*path == '.')
		return false;

	return true;
}

bool permitAudioAccess(const char *path, bool write)
{
	
	if(strstr(path, ".."))
		return false;

	if(*path == '.')
		return false;

	if(!write)
		return true;

	if(*path == '/')
	{
		if(!strstr(path, "/.bayonne/"))
			return false;
	}

	path = strrchr(path, '.');

	if(!path)
		return false;

	if(!stricmp(path, ".au"))
		return true;

	if(!stricmp(path, ".snd"))
		return true;

	if(!stricmp(path, ".al"))
		return true;

	if(!stricmp(path, ".wav"))
		return true;

	if(!stricmp(path, ".raw"))
		return true;

	if(!stricmp(path, ".ul"))
		return true;

	if(!stricmp(path, ".gsm"))
		return true;

	return false;
}

#endif

#ifdef	CCXX_NAMESPACES
};
#endif



