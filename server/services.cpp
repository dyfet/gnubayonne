// Copyright (C) 2000-2001 Open Source Telecom Corporation.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "server.h"
#include <cc++/url.h>
#include <cc++/file.h>

#ifdef	HAVE_SSTREAM
#include <sstream>
#else
#include <strstream>
#endif

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

Session *Session::first = NULL;
Session *Session::last = NULL;
Mutex Session::mutex;

Session::Session()
{
	mutex.enterMutex();
	if(last)
		last->next = this;

	prev = last;
	next = NULL;

	last = this;
	if(!first)
		first = this;
	mutex.leaveMutex();
}

void Session::unlink(void)
{
	if(next == this)
		return;

	mutex.enterMutex();
	if(prev)
		prev->next = next;
	else
		first = next;
	if(next)
		next->prev = prev;
	else
		last = prev;
	next = prev = this;
	mutex.leaveMutex();
}

void Session::clean(void)
{
	time_t now, expr;
	Session *base, *next;

	mutex.enterMutex();
	base = first;
	time(&now);

	while(base)
	{
		next = base->next;
		expr = base->getExpires();
		if(expr && expr < now)
		{
			base->unlink();
			delete base;
		}	
		base = next;
	}
	mutex.leaveMutex();
}

Service::Service(Trunk *trk, int pri) :
#ifdef	COMMON_OST_NAMESPACE
Semaphore(), Thread(pri, keythreads.getStack()), AudioService()
#else
Semaphore(), Thread((Semaphore *)this, pri, keythreads.getStack()), AudioService()
#endif
{
	trunk = trk;
	stopped = false;
	if(trunk->thread)
		trunk->stopServices();
	trunk->thread = this;
	data = &trunk->data;
	group = trunk->group;
}

void Service::failure(void)
{
	TrunkEvent event;

	event.id = TRUNK_SERVICE_FAILURE;
	trunk->postEvent(&event);
	setCancel(cancelImmediate);
	Thread::sleep(~0);
}

void Service::success(void)
{
	TrunkEvent event;

	event.id = TRUNK_SERVICE_SUCCESS;
	trunk->postEvent(&event);
	setCancel(cancelImmediate);
	Thread::sleep(~0);
}

AudioService::AudioService() {};

char *AudioService::getPrompt(char *name, const char *voice)
{
	Module *mod = Module::urlFirst;
#ifdef	HAVE_SSTREAM
	ostringstream str;
	str.str() = "";
#else
	strstream str(filename, sizeof(filename));
	filename[0] = 0;
#endif
	char buffer[256];
	char *cp;
	const char *ext;
	const char *local = keypaths.getLast("local");
	const char *prompts = keypaths.getLast("prompts");
	const char *altprompts = keypaths.getLast("altprompts");
	bool app = false;

	if(!local)
		local = keypaths.getLast("localprefix");

	if(!altprompts)
		altprompts = keyserver.getPrefix();

	if(!voice)
		voice = trunk->getSymbol(SYM_VOICE);

	while(mod)
	{
		cp = mod->getPrompt(name);
		if(cp)
			return cp;
		mod = mod->urlNext;
	}

	if(NULL == strstr(name, "::"))
	{
		cp = NULL;
		if(!strnicmp(name, "http:", 5))
			return name;

		if(!strnicmp(name, "sys:", 4))
		{
			cp = "/sys/";
			name += 4;
		}		
		else if(!strnicmp(name, "alt:", 4))
		{
			if(altprompts)
				prompts = altprompts;
			name += 4;
			cp = NULL;
		}
		else if(!strnicmp(name, "local:", 6))
		{
			prompts = local;
			name += 6;
			cp = NULL;
		} 
                else if(!strnicmp(name, "var:", 4))
                {
                        name += 4;
                        prompts = "";
                        snprintf(buffer, sizeof(buffer), "%s/%s", 
				trunk->apppath, name);
                        cp = buffer;
                }
		else if(!strnicmp(name, "usr:", 4))
		{
			if(altprompts)
				prompts = altprompts;
			name += 4;
                        if(trunk->apppath[0])
                        {
                                snprintf(buffer, sizeof(buffer), "%s/%s/",
                                        prompts, trunk->apppath);
                                if(isDir(buffer))
                                {
                                        printf(buffer, "%s/", trunk->apppath);
                                        cp = buffer;
                                }
                                else
                                        cp = "/";
                        }
                        else
				cp = "/";
		}
		else if(!strnicmp(name, "app:", 4))
		{
			app = true;
			name += 4;
			cp = NULL;
		}
		else if(!strnicmp(name, "music:", 6))
		{
			cp = "/music/";
			name += 6;
		}
		else if(!strnicmp(name, "audio:", 6))
		{
			cp = "/audio/";
			name += 6;
		}
		else if(!strnicmp(name, "tmp:", 4))
		{
			cp = "/";
			prompts = keypaths.getLast("tmp");
			name += 4;
		}
		else if(!strnicmp(name, "mem:", 4))
		{
			cp = "/";
			prompts = keypaths.getLast("tmpfs");
			name += 4;
		}

		if(!cp)
		{
			cp = strchr(name, ':');
			if(cp)
			{
				*(cp++) = 0;
				str << prompts << "/sys/" << name << "/" << cp;
				cp = strrchr(cp, '.');
				if(!cp)
					str << trunk->getLibexec();
#ifdef	HAVE_SSTREAM
				snprintf(filename, sizeof(filename), "%s", str.str().c_str());
#else
				str << ends;
#endif
				return filename;
			}
			if(strchr(name, '/'))
			{
				strcpy(filename, name);	
				return filename;
			}
			else
				app = true;
		}
		else
		{
			str << prompts << cp << name;
			cp = strrchr(name, '.');
			if(!cp)
				str << trunk->getLibexec();
#ifdef	HAVE_SSTREAM
			snprintf(filename, sizeof(filename), "%s", str.str().c_str());
#else
			str << ends;
#endif
			return filename;
		}
	}

	if(!strncmp(name, "~::", 3))
	{
		str << keyserver.getPrefix() << "/aaprompts/";
		name += 3;
	}
	else if(!strncmp(name, "*::", 3))
	{
		str << prompts << "/sys/" << (name + 3);
		cp = strrchr(name, '.');
		if(!cp)
			str << trunk->getLibexec();
#ifdef	HAVE_SSTREAM
		snprintf(filename, sizeof(filename), "%s", str.str().c_str());
#else
		str << ends;
#endif
		return filename;
	}
	else if(*name == '*')
	{
		++name;
		if(trunk->apppath[0])
		{
			snprintf(buffer, sizeof(buffer), "%s/%s/%s",
				prompts, voice, trunk->apppath);
			if(isDir(buffer))
				str << buffer << "/";
			else 
			{

				snprintf(buffer, sizeof(buffer), "%s/sys/%s",
					prompts, trunk->apppath);
				if(isDir(buffer))
					str << buffer << "/";
				else
					trunk->apppath[0] = 0;
			}
		}
		if(!trunk->apppath[0])
			str << prompts << "/sys/";

		voice = NULL;
	}
	else if(*name == '~')
	{
		str << keyserver.getPrefix() << "/aaprompts/";
		++name;
	}
	else if(!strncmp(name, "::", 2))
	{
		str << altprompts << "/" << ((ScriptInterp *)trunk)->getName()->name << "/";
//		str << altprompts << "/" << trunk->getSymbol(SYM_APPL) << "/";
		name += 2;
	}
	else if(NULL != (cp = strstr(name, "::")))
	{
		strcpy(buffer, name);
		cp = strchr(buffer, ':');
		name = cp + 2;
		if(cp)
			*cp = 0;
		str << altprompts << "/" << buffer << "/";
	}
	else if(trunk->apppath[0] && app)
	{
		snprintf(buffer, sizeof(buffer), "%s/%s/%s", 
			prompts, voice, trunk->apppath);
		if(isDir(buffer))		
		{
			str << prompts << "/" << voice << "/" << trunk->apppath << "/";
			voice = NULL;
		}
		else
		{
			trunk->apppath[0] = 0;
			str << prompts << "/";
		}
	}
	else
		str << prompts << "/";

	if(voice)
		str << voice << "/";

	str << name;
	cp = strchr(name, '.');
	if(!cp)
		str << trunk->getLibexec();
#ifdef	HAVE_SSTREAM
	snprintf(filename, sizeof(filename), "%s", str.str().c_str());
#else
	str << ends;
#endif
	urlDecode(filename);
	return filename;
}

char *AudioService::getPlayfile(void)
{
	char buffer[128];
	char *cp = buffer;
	const char *ext;

	if(!trunk->data.play.name)
		return NULL;

	if(!*trunk->data.play.name)
		return NULL;

	while(*trunk->data.play.name && *trunk->data.play.name != ',')
		*(cp++) = *(trunk->data.play.name++);

	*cp = 0;
	while(*trunk->data.play.name && *trunk->data.play.name == ',')
		++trunk->data.play.name;

	cp = getPrompt(buffer, trunk->data.play.voice);
	if(*cp == '/')
		return cp;

	ext = strrchr(cp, '/');
	if(!ext)
		ext = cp;
	ext = strrchr(ext, '.');
	if(ext)
		return cp;

 	if(!ext)
		ext = trunk->data.play.extension;

	if(ext)
		strcat(cp, ext);

	return cp;
}

timeout_t Service::stop(void)
{
	stopped = true;
	return keythreads.getResetDelay();
}

#ifdef	CCXX_NAMESPACES
};
#endif
