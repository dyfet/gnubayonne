// Copyright (C) 2000-2001 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "server.h"

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

Protocol *Protocol::first = NULL;

Protocol::Protocol(const char *keyid, tpport_t p) :
Keydata(keyid), InetHostAddress(), ThreadLock()
{
	const char *cp;

	static Keydata::Define keys[] = {
	{"address", "127.0.0.1"},
	{"sessions", "1"},
	{NULL, NULL}};

	load(keys);
	cp = getLast("port");
	if(cp)
		port = atoi(cp);
	else
		port = p;

	InetHostAddress addr(getLast("address"));
	*((InetHostAddress*)(this)) = addr;
	sessions = new Semaphore(atoi(getLast("sessions")));
	
	next = first;
	first = this;

	cp = getLast("resolver");
	if(cp)
		return;

	cp = getLast("address");
	if(*cp >= '0' && *cp <= '9')
		return;

	setValue("resolver", cp);
}

Protocol::~Protocol()
{
	if(sessions)
		delete sessions;
}

InetHostAddress Protocol::getAddress(void)
{
	InetHostAddress addr("127.0.0.1");

	readLock();
	addr = *this;
	unlock();
	return addr;
}

void Protocol::update(InetHostAddress addr)
{
	writeLock();
	*((InetHostAddress*)(this)) = addr;
	unlock();
}

#ifdef	CCXX_NAMESPACES
};
#endif
