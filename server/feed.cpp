// Copyright (C) 2000-2001 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "server.h"

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

AudioFeed *AudioFeed::first = NULL;

AudioFeed::AudioFeed(const char *str, unsigned bufcount)
{
	name = str;
	feedsize = 960; // 120ms of audio
	buffers = new unsigned char[bufcount * feedsize];
	current = buffers;
	active = false;
	memset((void *)current, 0xff, bufsize);

	next = first;
	first = this;
}

AudioFeed::~AudioFeed()
{
	delete[] buffers;
}
	
bool AudioFeed::enable(void)
{
	if(tryWriteLock())
	{
		active = true;
		unlock();
		return true;
	}
	return false;
}

void AudioFeed::disable(void)
{
	writeLock();
	active = false;
	unlock();
}

AudioFeed *getAudioFeed(const char *name)
{
	AudioFeed *feed = AudioFeed::first;

	while(feed)
	{
		if(!stricmp(name, feed->name))
			return feed;
		feed = feed->next;
	}
	return NULL; 
}

void AudioFeed::putBuffer(unsigned char *buffer, size_t size)
{
	if(!size)
		size = feedsize;
	else
		size *= 8;

	// must be true multiple

	if(feedsize % size)
		return;

	writeLock();
	memcpy((void *)current, buffer, size);
	current += size;
	if(current >= buffers + bufsize)
		current = buffers;
	memset((void *)current, 0xff, size);
	unlock();
}	
	
unsigned char *AudioFeed::getBuffer(unsigned char *prior, size_t size)
{
	if(!size)
		size = feedsize;
	else
		size *= 8;

	// must be true multiple

	if(feedsize % size)
		return NULL;

	readLock();
	if(!active)
	{
		unlock();
		return (unsigned char *)current;
	}

	if(prior == current)
	{
		unlock();
		return (unsigned char *)current;
	}

	if(!prior)
	{
		prior = (unsigned char *)current + size;
		if(prior >= buffers + bufsize)
			prior = buffers;
	}
	
	prior += size;
	if(prior >= buffers + bufsize)
		prior = buffers;

	unlock();
	return prior;
}

#ifdef	CCXX_NAMESPACES
};
#endif
