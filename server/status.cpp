// Copyright (C) 2000-2001 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "server.h"
#include <iomanip>

#ifdef	COMMON_OST_NAMESPACE
namespace ost {
using namespace std;
#endif

KeyPaths::KeyPaths() :
Keydata("/bayonne/paths")
{ 
        static Keydata::Define defpaths[] = {
        {"libpath", "/usr/lib/bayonne"},
        {"libexec", "/usr/lib"},
        {"tgipath", "/usr/libexec/bayonne:/bin:/usr/bin"},
        {"datafiles", "/var/bayonne"},
        {"scripts", "/usr/share/aascripts"},
        {"prompts", "/usr/share/aaprompts"},
	{"wrappers", "/usr/share/aawrappers"},
        {"spool", "/var/spool/bayonne"},
        {"runfiles", "/var/run/bayonne"},
        {"precache", "/var/bayonne/cache"},
        {"config", "bayonne runtime configuration"},
        {NULL, NULL}};                                                         

	load(defpaths);
 
}                                                    

extern "C" int main(int argc, char **argv)
{
	KeyPaths keypaths;
	char buffer[256];
	int fd;
	statnode_t node;
	InetAddress addr;
	time_t now;
	time_t live;
	char lv;

	strcpy(buffer, keypaths.getRunfiles());
	strcat(buffer, "/bayonne.nodes");
	if(!canModify(buffer))
	{
		strcpy(buffer, getenv("HOME"));
		strcat(buffer, "/.bayonne.nodes");
	}

	if(argc != 1)
	{
		cerr << "use: bayonne_status" << endl;
		exit(-1);
	}

	fd = ::open(buffer, O_RDONLY);
	if(fd < 0)
	{
		cerr << "bayonne_status: " << buffer << ": cannot access" << endl;
		exit(-1);
	}
	cout << setiosflags(ios::left);
	cout << setw(20) << "name" << setw(0) << " ";
	cout << setw(20) << "address" << setw(0);
	cout << setw(5) << "live" << setw(0); 
	cout << setw(0) << "status" << endl << endl;

	time(&now);
	for(;;)
	{
		if(::read(fd, &node, sizeof(node)) < sizeof(node))
			break;
		if(!node.name[0])
			continue;

		live = now - node.update;
		if(live > 9999)
			node.service = '-';
		addr = node.addr;
		live = node.uptime;
		lv = 's';
		if(live < 0)
		{
			live = 0;
		}
		live /= 60;
		lv = 'm';
		if(live > 59)
		{
			live /= 60;
			lv = 'h';
			if(live > 23)
			{
				live /= 24;
				lv = 'd';
			}
		}

		cout << setiosflags(ios::left);
		cout << setw(20) << node.name << setw(0) << " ";
		cout << setw(20) << addr << setw(0);
		switch(node.service)
		{
		case '-':
			cout << "---- "; break;
		case 'd':
		case 'D':
			cout << "down "; break;
		case 't':
		case 'T':
			cout << "test "; break;
		default:
			cout << setiosflags(ios::right);
			cout << setw(3) << live << setw(0) << lv << " ";
			cout << setiosflags(ios::left);
		}
		cout << setw(0) << node.stat << endl;
	}
	exit(0);
}

#ifdef	COMMON_OST_NAMESPACE
};
#endif
