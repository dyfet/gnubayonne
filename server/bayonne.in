#!/bin/sh
# Copyright (C) 2000-2001 Open Source Telecom Corporation.
#
# This file is free software; as a special exception the author gives
# unlimited permission to copy and/or distribute it, with or without
# modifications, as long as this notice is preserved.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
#
# a wrapper for starting of a bayonne server based on seeking the config
# directory.

cxxcompiler="@CXX@"
cxxflags="@CXXFLAGS@"
basename="bayonne"
output=""

prefix="@prefix@"
exec_prefix="@exec_prefix@"
sysconfig="@sysconfdir@"
dsopath="@libdir@/bayonne/@VERSION@"
datapath="@datadir@/bayonne"
repository="ftp://ftp.gnu.org/gnu/bayonne"
method="wget"
release="3"

gui=""
tflag=""
trflag=""
control=""
program=""
driver="$BAYONNE_DRIVER"
dflag=""
cflag=""
opts=""
check=""
vflag=""
vpath=""
vlib="UsEngM"
gdb_arg=""

pidofproc() {
        base=${1##*/}

        # Test syntax.
        if [ "$#" -eq 0 ] ; then
                gprintf "Usage: pidofproc {program}\n"
                return 1
        fi

        # First try "/var/run/*.pid" files
        if [ -f /var/run/$base.pid ]; then
                local line p pid=
                read line < /var/run/$base.pid
                for p in $line ; do
                       [ -z "${p//[0-9]/}" -a -d /proc/$p ] && pid="$pid $p"
                done
                if [ -n "$pid" ]; then
                        echo $pid
                        return 0
                fi
        fi
        pidof -o $$ -o $PPID -o %PPID -x $1 || \
                pidof -o $$ -o $PPID -o %PPID -x $base
}



case "$1" in
--music)
	shift
	cd "$install_prefix$datapath/music"
	for music in $* ; do
		wget "$repository/music/$music".au
	done
	echo "converting music..."
	filelist=`echo *.au | sed s/.au//g`
	for file in $filelist ; do
		sox "$file".au -A "$file".al
	done
	exit 0
	;;
--preconvert)
	shift
	if test -z "$1" ; then
		dp="$install_prefix$datapath"
	else
		dp="$1"
	fi
	cd "$dp"
	dirlist=`find . -type d | sed s:^[.]/:: | sed s:.*/CVS$::g`
	for dir in $dirlist ; do
		if test "$dir" != "." ; then
			cd "$dp/$dir"
			echo "converting $dir..."
			filelist=`echo *.au | sed s/.au//g`
			for file in $filelist ; do
				sox $file.au -A $file.al
			done
		fi
	done
	exit 0
	;;
--apps|--depend)
	basename=`basename $0`
	if test -d @prefix@/etc/$basename ; then
		config=@prefix@/etc/$basename
	elif test -d /etc/$basename ; then
		config=/etc/$basename
	else
		config=/etc
	fi
	if test -f /etc/sysconfig/bayonne ; then
		. /etc/sysconfig/bayonne
		if test ! -z "$REPOSITORY" ; then
			repository="$REPOSITORY" ; fi
		if test ! -z "$METHOD" ; then
			method="$METHOD" ; fi
		if test ! -z "$RELEASE" ; then
			release="$RELEASE" ; fi
		if test ! -z "$CONFIG" ; then
			config="$CONFIG" ; fi
	fi
 	mode="$1"
	bayonne="$0"
	shift
	if test ! -w "$install_prefix$datapath" ; then
		echo "*** unable to save application to $install_prefix$datapath"
		exit -1
	fi
	cd "$install_prefix$datapath"
	if test ! -d .installed ; then 
		mkdir .installed ; fi
	for app in $* ; do
		wget "$repository/apps$release/$app".tar.gz
		if test ! -f "$app".tar.gz ; then
			echo "*** missing application $app"
		else
			tar -xzf "$app".tar.gz
			rm -f "$app".tar.gz
			date >.installed/$app
                        if test -f .post/$app ; then
                                /bin/sh -c .post/$app ; fi
                        depends=""
                        if test -f .depend/$app ; then
                                depends=`cat .depends/$app` ; fi
                        for depend in $depends ; do
                                if test ! -f .installed/$depend ; then
                                        "$bayonne" --depend $depend ; fi
                        done
			if test -f .config/$app ; then
				if test ! -f $config/$app.conf ; then
					cp .config/$app $config/$app.conf ; fi
			fi
		fi
	done
        if test "$mode" == "--depend" ; then
                exit 0 ; fi

	"$bayonne" --preconvert
	run=`ps ax | grep bayonne.bin | grep -v grep`
	if test -z "$run" ; then
		exit 0
	fi
	"$bayonne" --compile
	exit 0
	;;	
--voices)
	shift
	if test ! -w "$install_prefix$datapath" ; then
		echo "*** unable to save voices to $install_prefix$datapath"
		exit -1
	fi
	cd "$install_prefix$datapath"

	if test -f /etc/sysconfig/bayonne ; then
		. /etc/sysconfig/bayonne
		if test ! -z "$REPOSITORY" ; then
			repository="$REPOSITORY" ; fi

		if test ! -z "$METHOD" ; then
			method="$METHOD" ; fi		
	fi

	for voice in $* ; do
		wget "$repository/voices/$voice".tar.gz
		if test ! -f "$voice".tar.gz ; then
			echo "*** missing voice $voice"
		else
			tar -xzf "$voice".tar.gz
			rm -f "$voice".tar.gz
			cd $voice
			dirlist=`find . -type d | sed s:^[.]/::`
			for dir in $dirlist ; do
				if test "$dir" != "CVS" ; then
					cd "$install_prefix$datapath/$voice/$dir"
					if test "$dir" = "." ; then
						echo "converting $voice..."
					else
						echo "converting $voice/$dir..."
					fi
					filelist=`echo *.au | sed s/.au//g`
					for file in $filelist ; do
						if test -f "$file".au ; then
							sox "$file".au -A "$file".al
						fi
					done
				fi
			done
		fi
	done
	exit 0
	;;
--sendmail|--deliver)
	output="@SENDMAIL@"
	shift
	path=`dirname $0`
	if test -x $path/shell.bin ; then
		program=$path/shell.bin
	else
		program=$dsopath/shell.bin
	fi
	;;
--shell)
        shift
        path=`dirname $0`
        if test -x $path/shell.bin ; then
                program=$path/shell.bin
        else
                program=$dsopath/shell.bin
        fi
        ;;
--kill)
	killall -9 bayonne.bin
	exit 0
	;;
--status)
	shift
	path=`dirname $0`
	if test -x $path/status.bin ; then
		program=$path/status.bin
	else
		program=$dsopath/status.bin
	fi
	;;
--setup)
	shift
	program=$dsopath/setup.bin
	;;
--policy)
	shift
	program=$dsopath/policy.bin
	;;
--dsopath)
	echo $dsopath
	exit 0
	;;
--datapath)
	echo $datapath
	exit 0
	;;
--homepath)
	echo /home/bayonne
	exit 0
	;;
--down|--stop)
	control="down"
	;;
--service)
	if test "$2" == "up"
	then
		control="up"
	else
		control="down $2"
	fi
	;;
--restart)
	control="restart"
	;;
--up)
	control="up"
	;;
--flags)
	echo $cxxflags
	exit 0
	;;
--make|--module)
	shift
	for path in $* ; do
		$cxxcompiler $cxxflags -c $path
	done
	exit 0
	;;
--link)
	shift
	name="$1"
	shift
	exec libtool --mode=link $cxxcompiler -o $dsopath/$name -module -shared $*
	exit 0
	;;
--install)
	shift
	dir="$datapath/$1"
	shift
	mkdir -p $dir
	for path in $* ; do
		base=`basename $path`
		install $path $dir/$base
	done
	exit 0
	;;
--version)
	echo @VERSION@
	exit 0
	;;
--compile)
	control="compile"
	;;
--devel)
	shift
	cd /home/bayonne
	ln -sf "$1" devel
	control="compile"
	;;
--start)
	shift
	;;
--control)
	shift
	control="$*"
	;;
-*)
	;;
*)
	if test -f $1 ; then
		path=`dirname $0`
		if test -x $path/shell.bin ; then
			program=$path/shell.bin
		else
			program=$dsopath/shell.bin
		fi		
	fi
esac

if test ! -z "$program" ; then
	if test -f /etc/sysconfig/bayonne ; then
		. /etc/sysconfig/bayonne
		if test ! -z "$CONFIG" ; then
			sysconfig="$CONFIG"
		fi
	fi
	if test ! -z "$BAYONNE_CONFIG" ; then
		sysconfig="$BAYONNE_CONFIG"
	fi

	CONFIG_KEYDATA=$sysconfig/
	export CONFIG_KEYDATA

	if test -z "$output" ; then
		exec $program $*
	else
		exec $program $* | $output
	fi
	exit -1
fi

if test ! -z "$control" ; then

	run=`ps ax | grep bayonne.bin | grep -v grep`
	if test -z "$run" ; then
        	echo "bayonne --control: no bayonne instance running"
        	exit -1
	fi

	if test -w /var/run/bayonne/bayonne.ctrl ; then
        	echo "$control" >/var/run/bayonne/bayonne.ctrl
	elif test -w /var/run/bayonne.ctrl ; then
        	echo "$control" >/var/run/bayonne.ctrl
	elif test -w ~/.bayonne.ctrl ; then
        	echo "$control" >~/.bayonne.ctrl
	else
        	echo "bayonne --control: unable to find fifo"
		exit -1
	fi
	exit 0
fi

if test -f /etc/sysconfig/bayonne ; then
	DRIVER=""
	CONFIG=""
	PRELOAD=""
	VOICE=""
	DIALPLAN=""
	LANGUAGE=""
	PROXY_SERVER=""
	PROXY_PORT=""

	. /etc/sysconfig/bayonne

	if test -z "$BAYONNE_EXTENSIONS" ; then
		if test ! -z "$EXTENSIONS" ; then
			BAYONNE_EXTENSIONS=$EXTENSIONS
		fi
	fi

	if test -z "$BAYONNE_DRIVER" ; then
		if test ! -z "$DRIVER" ; then
			BAYONNE_DRIVER=$DRIVER
		fi
	fi
	if test -z "$BAYONNE_PRELOAD" ; then
		if test ! -z "$PRELOAD" ; then
			BAYONNE_PRELOAD=$PRELOAD
		fi
	fi
	if test -z "$BAYONNE_CONFIG" ; then
		if test ! -z "$CONFIG" ; then
			BAYONNE_CONFIG=$CONFIG/
		fi
	fi
	if test -z "$BAYONNE_DIALPLAN" ; then
		if test ! -z "$DIALPLAN" ; then
			BAYONNE_DIALPLAN=$DIALPLAN
		fi
	fi
	if test -z "$BAYONNE_VOICE" ; then
		if test ! -z "$VOICE" ; then
			BAYONNE_VOICE=$VOICE
		fi
	fi
	if test -z "$BAYONNE_LANGUAGE" ; then
		if test ! -z "$LANGUAGE" ; then
			BAYONNE_LANGUAGE=$LANGUAGE
		fi
	fi
	if test -z "$BAYONNE_PROXY_SERVER" ; then
		if test ! -z "$PROXY_SERVER" ; then
			BAYONNE_PROXY_SERVER=$PROXY_SERVER
		fi
	fi			
	if test -z "$BAYONNE_PROXY_PORT" ; then
		if test ! -z "$PROXY_PORT" ; then
			BAYONNE_PROXY_PORT=$PROXY_PORT
		fi
	fi
fi

if test ! -z "$BAYONNE_VOICE" ; then
	vlib="$BAYONNE_VOICE"
fi

if test ! -z "$BAYONNE_DRIVER" ; then
	driver=$BAYONNE_DRIVER
fi

if test ! -d "$datapath/$vlib" -a -w "$datapath" ; then
	wd=`pwd`
	cd "$datapath"
	wget "$repository/voices/$vlib".tar.gz
	if test -f "$vlib".tar.gz ; then
		tar -xzf "$vlib".tar.gz
		rm -f "$vlib".tar.gz
	fi
	cd "$wd"
fi

for opt in $* ; do
	if test ! -z $dflag ; then
		driver=$opt
		dflag=""
	elif test ! -z $cflag ; then
		BAYONNE_CONFIG=$opt/
		cflag=""
		opt=""
	else
	case $opt in
	--test)
		tflag="--test"
		trflag="--trace"
		;;
	--gui|-G)
		gui="--gui"
		;;
	--dialogic)
		driver="dialogic"
		;;
	--dummy | --sound*)
		driver="dummy"
		;;
	--vpb | --voicetronix)
		driver="vpb"
		;;
	--openline*)
		driver="openline4"
		;;
	--openswitch)
		driver="openswitch"
		;;
	--aculab)
		driver="aculab"
		;;
	--quicknet)
		driver="phonedev"
		;;
	--capi*)
		driver="capi20"
		;;
	--global*)
		driver="globalcall"
		;;
	--oh323)
		driver="oh323"
		;;
	--iax)
		driver="iax"
		;;
	--gdb)
		gdb_arg="gdb --args"
		;;
	--check)
		check=$opt
		;;
	--config=*)
		BAYONNE_CONFIG=`echo $opt s/--config=//`/
		;;		
	--config)
		cflag=$opt
		;;
	--driver=*)
		driver=`echo $opt | sed s/--driver=//`
		;;
	-d | --driver)
		dflag="$opt"
		;;
	--version=*)
		vpath=`echo $opt | sed s/--version=//`
		dsopath="@libdir@/bayonne/$vpath"
		;;
	--version | -V)
		vflag="--version"
		;;
	--status)
		vflag="--status"
		;;
	*)
		opts="$opts $opt"
		;;
	esac
	fi
done

if test ! -z "$driver" ; then
	case $driver in
	global*)
		driver="globalcall"
		;;
	capi*)
		driver="capi20"
		;;
	voicetronix)
		driver="vpb"
		;;
	quicknet)
		driver="phonedev"
		;;
	sound*)
		driver="dummy"
		;;
	esac
	BAYONNE_DRIVER=$driver
	if test -z "$BAYONNE_PRELOAD" ; then
		case $driver in
		dialogic)
			BAYONNE_PRELOAD=/usr/lib/libLiS.so
			;;
		globalcall)
			BAYONNE_PRELOAD=/usr/lib/libLiS.so:/usr/dialogic/lib/libgc.so:/usr/dialogic/lib/libdevmap.so
			;;
		esac
	fi
fi

if test ! -z "$BAYONNE_DRIVER" ; then
	export BAYONNE_DRIVER
fi

if test ! -z "$BAYONNE_LANGUAGE" ; then
	export BAYONNE_LANGUAGE
fi

if test ! -z "$BAYONNE_VOICE" ; then
	export BAYONNE_VOICE
fi

if test ! -z "$BAYONNE_PROXY" ; then
	export BAYONNE_PROXY
fi

if test ! -z "$BAYONNE_DIALPLAN" ; then
	export BAYONNE_DIALPLAN
fi

if test ! -z "$BAYONNE_PROXY_SERVER" ; then
	export BAYONNE_PROXY_SERVER
fi

if test ! -z "$BAYONNE_PROXY_PORT" ; then
	export BAYONNE_PROXY_PORT
fi

if test -d /etc/$basename ; then
	CONFIG_KEYDATA=/etc/$basename/
elif test -d "@prefix@/etc/$basename" ; then
	CONFIG_KEYDATA="@prefix@/etc/$basename/"
fi

if test ! -z "$BAYONNE_CONFIG" ; then
	CONFIG_KEYDATA=$BAYONNE_CONFIG
fi

if test -z "$CONFIG_KEYDATA" ; then
	CONFIG_KEYDATA=$sysconfig/
fi

export CONFIG_KEYDATA

if test ! -z "$BAYONNE_PRELOAD" ; then
	if test -z "$LD_PRELOAD" ; then
		export LD_PRELOAD="$BAYONNE_PRELOAD"
	else
		export LD_PRELOAD="$LD_PRELOAD:$BAYONNE_PRELOAD"
	fi
fi

if test ! -z "$vflag" ; then opts=$vflag ; fi

if test ! -z "$BAYONNE_EXTENSIONS" ; then opts="-$BAYONNE_EXTENSIONS $opts" ; fi

if test -z $check ; then
	run=`ps ax | grep bayonne.bin | grep -v grep`
	if test ! -z "$run" ; then
		echo "bayonne: *** already running"
		exit -1
	fi

	path=`dirname $0`
	if test -x $path/bayonne.bin -a $path != "$exec_prefix"/sbin ; then
		exec $gdb_arg $path/bayonne.bin --prefix "@prefix@" $gui $tflag $opts
	else
		exec $gdb_arg $dsopath/bayonne.bin --prefix "@prefix@" $gui $trflag $opts
	fi
	exit -1
fi

if test -z $CONFIG_KEYDATA ; then
	CONFIG_KEYDATA="/etc/"
fi

echo "PRELOAD = $LD_PRELOAD"
echo "DRIVER = $BAYONNE_DRIVER"
echo "CONFIG = $CONFIG_KEYDATA"
