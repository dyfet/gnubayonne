// Copyright (C) 2000-2001 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef	__CCXX_BAYONNE_H__
#define	__CCXX_BAYONNE_H__

#ifndef	__CCXX_SCRIPT_H__
#include <cc++/script3.h>
#endif

#ifdef	COMMON_XML_PARSING
#ifndef	__CCXX_XML_H__
#include <cc++/xml.h>
#endif
#endif

#ifndef	__CCXX_URL_H__
#include <cc++/url.h>
#endif

#ifndef	__CCXX_SLOG_H__
#include <cc++/slog.h>
#endif

#ifndef	__CCXX_DSO_H__
#include <cc++/file.h>
#endif

#ifndef	__CCXX_SOCKET_H__
#include <cc++/socket.h>
#endif

#ifndef	__CCXX_AUDIO_H__
#include <cc++/audio2.h>
#endif

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <climits>

// PIPE_BUF is the atomic i/o size for the pipe.  This ultimately is used to represent
// the maximum size of a tgi command buffer.

#ifndef	PIPE_BUF
#define	PIPE_BUF	512
#endif

#ifdef	__FreeBSD__
#undef	read
#undef	write
#undef	readv
#undef	writev
#endif

#ifdef	CCXX_NAMESPACES 
namespace ost {
#endif

/* Bayonne uses a universal event record for all driver plugins and
   state handlers.  Not all drivers impliment the entire set of events
   and some drivers have specific events associated with their unique
   characteristcs.  However, most events are considered "universal".
*/

class Trunk;
class TrunkImage;
class Service;
class phTone;
class TrunkGroup;
class Module;
class Request;
class Conference;
class Driver;
class TGI;
class Translator;
class phTone;

#define	MAX_DIGITS	48	// maximum dtmf receive digit buffer size
#define MAX_NAME_LEN	64

struct callrec_t;

// This is used to represent the trunk select policy, if trunks should be selected
// by first to last, or last to first, as per bayonne.conf.

typedef enum
{
	SELECT_FIRST,
	SELECT_LAST
} seltype_t;

// This is used for the mode in matching data statements or indexed maps.

typedef enum
{
	MAP_PREFIX,	// match by lead digits only
	MAP_SUFFIX,	// match by comparing with trailing digits only
	MAP_ABSOLUTE	// match by absolute value
} mapmode_t;

// This specifies the mode of a channel join, whether it is full duplex, or in a specific
// half duplex mode.

typedef enum
{
	JOIN_RECV,
	JOIN_XMIT,
	JOIN_FULL
} joinmode_t;

// This is used to specify the current dialing mode being used, particularly for drivers
// that use softdial support.

typedef enum
{
	PULSE_DIALER,	// pulse dial mode
	DTMF_DIALER,	// dtmf tone dialing
	MF_DIALER	// mf tone dialing
} dialmode_t;

// This defines the base type of a generic loadable plugin module.

typedef enum {
	MODULE_GENERIC,		// module has no specific function
	MODULE_DELIVERY,	// module is used to deliver messages
	MODULE_SENDFILE,	// module is used to send files
	MODULE_SENDFAX,		// module is related to fax spooling
	MODULE_NOTIFY,		// module is related to message or call notification
	MODULE_FIFO,		// module is bayonne.ctrl extension
	MODULE_TGI,		// module is a tgi plugin
	MODULE_URL,		// module processes url
	MODULE_NET,		// module does networking
	MODULE_REPLY,	
	MODULE_SWITCH,		// module does switch (external pbx) integration
	MODULE_THREAD,		// module uses threads for non-blocking
	MODULE_PLAY,		// module is related to playing
	MODULE_RECORD,		// module is related to recording
	MODULE_LIBEXEC,		// new libexec wrapper plugin
	MODULE_SQL,		// module is sql driver plugin
	MODULE_ANY		// placeholder
}	modtype_t;

// When a script command requires the driver to be in a specific processing state to
// perform a blocking operation, it sends a trunkStep() request to the driver.  Different
// trunkstep requests put the driver into different processing states, which then fall
// back to scripting when the state completes.

typedef enum {
	// step change requests
	TRUNK_STEP_HANGUP = 0,
	TRUNK_STEP_SLEEP,
	TRUNK_STEP_ACCEPT,
	TRUNK_STEP_REJECT,
	TRUNK_STEP_ANSWER,
	TRUNK_STEP_COLLECT,
	TRUNK_STEP_PLAY,
	TRUNK_STEP_PLAYWAIT,
	TRUNK_STEP_RECORD,
	TRUNK_STEP_TONE,
	TRUNK_STEP_DIALXFER,
	TRUNK_STEP_SOFTDIAL,
	TRUNK_STEP_FLASH,
	TRUNK_STEP_JOIN,
	TRUNK_STEP_LISTEN,
	TRUNK_STEP_RTP,
	TRUNK_STEP_DUPLEX,
	TRUNK_STEP_DETECT,
	TRUNK_STEP_REQUIRES,
	TRUNK_STEP_LOADER,
	TRUNK_STEP_TRANSFER,	// pbx internal transfer
	TRUNK_STEP_INTERCOM,	// pbx internal intercom
	TRUNK_STEP_PICKUP,	// pbx internal line pickup
	TRUNK_STEP_THREAD,
	TRUNK_STEP_SENDFAX,
	TRUNK_STEP_RECVFAX,
	TRUNK_STEP_EXIT = TRUNK_STEP_HANGUP,
	TRUNK_STEP_DRIVER,
	TRUNK_STEP_SOFTJOIN
} trunkstep_t;

// Trunk signals are used by drivers to notify scripting that a specific ^xxx handler
// is needed based on a driver state event.  This is used to define the specific
// requests and event branch points.  The "TRUNK_SIGNAL_STEP" request is special, in that 
// it simply requests processing to resume at the next script statement.

typedef	enum {
	// notify script from state handler
	TRUNK_SIGNAL_STEP = 0,				// branch to next script step

	TRUNK_SIGNAL_EXIT,				// ask script to exit
	TRUNK_SIGNAL_HANGUP=TRUNK_SIGNAL_EXIT,
	TRUNK_SIGNAL_ERROR,				// ^error handler
	TRUNK_SIGNAL_TIMEOUT,				// ^timeout handler
	TRUNK_SIGNAL_DTMF,

	TRUNK_SIGNAL_0,
	TRUNK_SIGNAL_1,
	TRUNK_SIGNAL_2,
	TRUNK_SIGNAL_3,

	TRUNK_SIGNAL_4,
	TRUNK_SIGNAL_5,
	TRUNK_SIGNAL_6,
	TRUNK_SIGNAL_7,

	TRUNK_SIGNAL_8,
	TRUNK_SIGNAL_9,
	TRUNK_SIGNAL_STAR,
	TRUNK_SIGNAL_POUND,

	TRUNK_SIGNAL_A,
	TRUNK_SIGNAL_OVERRIDE = TRUNK_SIGNAL_A,
	TRUNK_SIGNAL_B,
	TRUNK_SIGNAL_FLASH = TRUNK_SIGNAL_B,
	TRUNK_SIGNAL_C,
	TRUNK_SIGNAL_IMMEDIATE = TRUNK_SIGNAL_C,
	TRUNK_SIGNAL_D,
	TRUNK_SIGNAL_PRIORITY = TRUNK_SIGNAL_D,

	TRUNK_SIGNAL_SILENCE,
	TRUNK_SIGNAL_BUSY,
	TRUNK_SIGNAL_CANCEL,
	TRUNK_SIGNAL_FAIL = TRUNK_SIGNAL_CANCEL,
	TRUNK_SIGNAL_INVALID = TRUNK_SIGNAL_CANCEL,
	TRUNK_SIGNAL_NOTIFY,

	TRUNK_SIGNAL_NOANSWER,
	TRUNK_SIGNAL_RING,
	TRUNK_SIGNAL_ANSWER = TRUNK_SIGNAL_RING,
	TRUNK_SIGNAL_PICKUP = TRUNK_SIGNAL_RING,
	TRUNK_SIGNAL_TONE,
	TRUNK_SIGNAL_EVENT,

	TRUNK_SIGNAL_TIME,
	TRUNK_SIGNAL_MAXTIME = TRUNK_SIGNAL_TIME,
	TRUNK_SIGNAL_CHILD,
	TRUNK_SIGNAL_DRIVER,

	TRUNK_SIGNAL_ASR,

	TRUNK_SIGNAL_GOTO = 65			// special goto completion request,
						// used by states that process labels.
}	trunksignal_t;

// Each channel receives a generic event with an id as defined from this type whenever
// the low level api or device driver needs to notify the Bayonne driver of some 
// functional change in line state, or whenever the application wishes to generate
// events to feed into the Bayonne driver state machines.

typedef enum {
	// primary state handlers

	TRUNK_ENTER_STATE = 100,// newly entered handler state
	TRUNK_EXIT_STATE,	// exiting prior state (unused)
	TRUNK_STOP_STATE,	// request state termination
	TRUNK_NOTIFICATION,	// death notify event
	TRUNK_SERVICE_SUCCESS,	// service completion successful
	TRUNK_SERVICE_FAILURE,	// service completion failed
	TRUNK_SERVICE_LOGIN,	// login transaction
	TRUNK_SIGNAL_TEXT,	// unsolicited text event message
	TRUNK_SEND_MESSAGE,	// event message ipc
	TRUNK_JOIN_TRUNKS,	// join two trunks together
	TRUNK_PART_TRUNKS,	// split two trunks that were joined
	TRUNK_NULL_EVENT,	// used to push pipe driven systems
	TRUNK_CHILD_START,	// notify parent of child startup
	TRUNK_CHILD_FAIL,	// notify parent child start failed
	TRUNK_CHILD_EXIT,	// notify child channel died
	TRUNK_SIGNAL_JOIN,	// send join notification (^event)
	TRUNK_FAX_EVENT,	// fax event
	// tgi/integration control state handlers


	TRUNK_EXIT_SHELL = 200,	// tgi completion event
	TRUNK_START_SCRIPT,	// start of script
	TRUNK_RING_START,	// smdi/integrated answer
	TRUNK_RING_REDIRECT,	// smdi/integrated answer options
	TRUNK_STOP_DISCONNECT,	// integrated hangup notification
	TRUNK_SHELL_START,	// fifo shell request
	TRUNK_WAIT_SHELL,	// report wait
	TRUNK_ASR_START,	// asr startup notification
	TRUNK_ASR_TEXT,		// asr text message
	TRUNK_ASR_PARTIAL,	// asr partial text message
	TRUNK_ASR_VOICE,	// asr voice detect
	TRUNK_SYNC_PARENT,	// parent notification

	// in the future these will be used

	TRUNK_START_INCOMING = TRUNK_RING_START,
	TRUNK_START_OUTGOING = TRUNK_START_SCRIPT,

	// primary "mode" selection controls

	TRUNK_MAKE_TEST =  300,	// request driver perform line test
	TRUNK_MAKE_BUSY,	// request driver lockout line
	TRUNK_MAKE_IDLE,	// request driver reset line
	TRUNK_MAKE_STEP,	// pass step event internally
	TRUNK_MAKE_STANDBY,	// standby mode for carrier grade events

	// basic trunk events

	TRUNK_LINE_WINK = 400,	// used for line disconnect notification
	TRUNK_TIMER_EXPIRED,	// driver specific port timer expired
	TRUNK_TIMER_EXIT,
	TRUNK_TIMER_SYNC,
	TRUNK_RINGING_ON,	// some drivers distinguish start/stop
	TRUNK_RINGING_OFF,	// default ring event
	TRUNK_TEST_IDLE,	// some drivers have line test completion
	TRUNK_TEST_FAILURE,	// some drivers notify errors
	TRUNK_ON_HOOK,		// some drivers notify on hook
	TRUNK_OFF_HOOK,		// some drivers notify off hook
	TRUNK_CALLER_ID,	// caller id parse request
	TRUNK_RINGING_DID,	// did digit ring signal
	TRUNK_CALL_DETECT,	// ISDN call detected notification
	TRUNK_CALL_CONNECT,	// ISDN call connection notification
	TRUNK_CALL_RELEASE,	// ISDN call release notification
	TRUNK_CALL_ACCEPT,	// ISDN incoming call accepted
	TRUNK_CALL_ANSWERED,	// ISDN connect sent to the network
	TRUNK_CALL_HOLD,	// ISDN call placed on hold
	TRUNK_CALL_NOHOLD,	// ISDN call hold was rejected
	TRUNK_CALL_DIGITS,	// requested digits received
	TRUNK_CALL_OFFER,	// ISDN call offered
	TRUNK_CALL_ANI,		// ANI received
	TRUNK_CALL_ACTIVE,	// ISDN call taken off hold
	TRUNK_CALL_NOACTIVE,	// ISDN call hold retrieve failed
	TRUNK_CALL_BILLING,	// ISDN call billing acknowledge
	TRUNK_CALL_RESTART,	// ISDN call restart, success or failure
	TRUNK_CALL_SETSTATE,	// ISDN acknowledge state change
	TRUNK_CALL_FAILURE,	// ISDN midcall failure
	TRUNK_CALL_ALERTING,	// ISDN call alerting
	TRUNK_CALL_INFO,	// ISDN call info message
	TRUNK_CALL_BUSY,	// ISDN conjestion message
	TRUNK_CALL_DIVERT,	// ISDN call diversion notification
	TRUNK_CALL_FACILITY,	// ISDN call facility
	TRUNK_CALL_FRAME,	// ISDN call frame
	TRUNK_CALL_NOTIFY,	// ISDN call notify
	TRUNK_CALL_NSI,		// ISDN call nsi message
	TRUNK_CALL_RINGING,	// digital T1 incoming call
	TRUNK_CALL_DISCONNECT,	// digital T1 circuit break
	TRUNK_DEVICE_OPEN,	// device open
	TRUNK_DEVICE_CLOSE,	// device close
	TRUNK_DEVICE_BLOCKED,	// channel blocked
	TRUNK_DEVICE_UNBLOCKED,	// channel unblocked

	// basic audio processing events

	TRUNK_AUDIO_IDLE = 500,	// audio reset or completion event
	TRUNK_INPUT_PENDING,	// some drivers monitor audio i/o status
	TRUNK_OUTPUT_PENDING,	// some drivers monitor audio i/p status
	TRUNK_AUDIO_BUFFER,	// some drivers return audio buffers
	TRUNK_TONE_IDLE,	// tone generator completion event
	TRUNK_DTMF_KEYDOWN,	// some drivers distinguish tone down
	TRUNK_DTMF_KEYUP,	// default dtmf event
	TRUNK_TONE_START,	// tone detected
	TRUNK_TONE_STOP,	// some drivers have tone completion event
	TRUNK_VOX_DETECT,	// speaker detected
	TRUNK_VOX_SILENCE,	// silence detected
	TRUNK_AUDIO_START,	// some drivers may "vox" compress
	TRUNK_AUDIO_STOP,	// some drivers may "vox" compress
	TRUNK_CPA_DIALTONE,	// dialtone heard on the line
	TRUNK_CPA_BUSYTONE,
	TRUNK_CPA_RINGING,
	TRUNK_CPA_RINGBACK = TRUNK_CPA_RINGING,
	TRUNK_CPA_INTERCEPT,
	TRUNK_CPA_NODIALTONE,
	TRUNK_CPA_NORINGBACK,
	TRUNK_CPA_NOANSWER,
	TRUNK_CPA_CONNECT,
	TRUNK_CPA_FAILURE,
	TRUNK_CPA_GRUNT,
	TRUNK_CPA_REORDER,
	TRUNK_DSP_READY,	// dsp resource became available
	TRUNK_CPA_STOPPED,

	// basic station processing event extensions

	TRUNK_START_RINGING = 600,	// ring for incoming lines
	TRUNK_START_TRANSFER,		// transfer mode ring
	TRUNK_START_INTERCOM,		// intercom mode ring
	TRUNK_START_RECALL,		// transfer recall mode ring
	TRUNK_START_DIALING,		// initiate outgoing call
	TRUNK_STOP_RINGING,		// cancel a ring request
	TRUNK_STATION_OFFHOOK,
	TRUNK_STATION_ONHOOK,
	TRUNK_STATION_FLASH,
	TRUNK_STATION_ANSWER,		// answering ringing port
	TRUNK_STATION_PICKUP,		// pickup foreign port
	TRUNK_STATION_CONNECT,		// auto-answer connect

	// driver specific events and anomolies

	TRUNK_DRIVER_SPECIFIC=8000	// very oddball events
} trunkevent_t;

// This is most often associated with drivers that dynamically allocate dsp resources as
// needed on demand.  This is used to indicate which dsp resource has currently been
// allocated for the port.

typedef enum
{
	DSP_MODE_INACTIVE = 0,	// dsp is idle
	DSP_MODE_VOICE,		// standard voice processing
	DSP_MODE_CALLERID,	// caller id support
	DSP_MODE_DATA,		// fsk modem mode
	DSP_MODE_FAX,		// fax support
	DSP_MODE_TDM,		// TDM bus with echo cancellation
	DSP_MODE_RTP,		// VoIP full duplex
	DSP_MODE_DUPLEX,	// mixed play/record
	DSP_MODE_JOIN,		// support of joined channels
	DSP_MODE_CONF,		// in conference
	DSP_MODE_TONE		// tone processing
} dspmode_t;

// the dtmfmode structure determines how dtmf will be processed during a running script.
// The normal mode is "Line" mode, where dtmf is only processed on lines that are not
// in a dtmf event trap.  This is controlled by the options dtmf= keyword.  On some
// telephony cards, dtmf may be a managed resource.

typedef enum
{
	DTMF_MODE_LINE = 0,	// default, evaluate on a per line basis
	DTMF_MODE_SCRIPT,	// if dtmf traps in script, keep active in all parts
	DTMF_MODE_ON,		// enable dtmf continually regardless of use
	DTMF_MODE_OFF		// disable dtmf detect completely
} dtmfmode_t;

// This specifies the mode of the trunk port, particularly in regard to cdr logging,
// whether it is being used for an incoming or outgoing call, or is available, or has
// been taken out of service.

typedef enum
{
	TRUNK_MODE_INCOMING = 0,	// this channel is receiving a call
	TRUNK_MODE_OUTGOING,		// this channel is placing a call
	TRUNK_MODE_INACTIVE,		// this channel is idle
	TRUNK_MODE_UNAVAILABLE		// this channel is not in service
} trunkmode_t;

// This is used to represent statistical data that is collected by Bayonne.  Each
// trunk group can collect it's own statistical data, which can be used to compute
// traffic engineering, busy hour info for acd agents, etc.

typedef enum
{
	STAT_MAX_INCOMING,
	STAT_MAX_OUTGOING,
	STAT_TOT_INCOMING,
	STAT_TOT_OUTGOING,
	STAT_ACTIVE_CALLS,
	STAT_NOW_INCOMING,
	STAT_NOW_OUTGOING,
	STAT_SYS_INCOMING,
	STAT_SYS_OUTGOING,
	STAT_SYS_UPTIME,
	STAT_SYS_ACTIVITY,
	STAT_CURRENT_CALLS,
	STAT_CMAX_INCOMING,
	STAT_CMAX_OUTGOING,
	STAT_AVAIL_CALLS,
} statitem_t;

// This is used to determine how each driver's play command will work.

typedef	enum
{
	PLAY_MODE_NORMAL,	// play audio normally
	PLAY_MODE_ONE,		// play only first audio file found
	PLAY_MODE_ANY,		// play any (all) audio files found
	PLAY_MODE_TEMP,		// play and delete temporary audio file
	PLAY_MODE_TEXT,		// perform text to speech processing for this play
	PLAY_MODE_FILE,		// perform text to speech from an input file
	PLAY_MODE_MOH		// play in music on hold mode
} playmode_t;

// This is actually not used, but may be used later to determine the file write mode
// for a record operation

typedef enum
{
	WRITE_MODE_NONE,	// no write
	WRITE_MODE_REPLACE,	// replace original
	WRITE_MODE_APPEND,	// append to original
	WRITE_MODE_INITIAL	// write if intial only
} writemode_t;

// This is used for tgi based text to speech support

typedef enum
{
	TTS_GATEWAY_TEXT,
	TTS_GATEWAY_FILE
} ttsmode_t;

// Each trunk port has a capability flag which indicates what features that port 
// supports.

#define TRUNK_CAP_VOICE		0x00000001	// supports voice telephone calls
#define	TRUNK_CAP_DIAL		0x00000002	// supports dialing
#define TRUNK_CAP_SENDFAX	0x00000004	// supports sending of faxes
#define	TRUNK_CAP_RECVFAX	0x00000008	// supports receipt of faxes
#define	TRUNK_CAP_DATA		0x00000010	// supports fsk modem
#define	TRUNK_CAP_TTS		0x00000020	// supports text to speech directly
#define	TRUNK_CAP_ASR		0x00000040	// supports asr (listen command)
#define	TRUNK_CAP_STATION	0x00000080	// is pots to plugin telephone
#define TRUNK_CAP_TIE		0x00000160	// is a tie line
#define TRUNK_CAP_TRUNK		0x00002000	// is a trunk port

// This structure holds the executive data for managing a tgi session for each channel

typedef	struct
{
	bool dtmf;		// whether dtmf is enabled for this tgi
	int fd;			// file descriptor of tgi input
	int pid;		// process id of tgi process
	unsigned short seq;	// sequence check value to make sure not stale
	void *data;		// unused?
}	execdata_t;

// Per-channel Interface to enable external process to receive events from a running 
// script.  This may be used by apennine to monitor script completion to soap
// requests that run scripts, or even potentially by external scripting system.

typedef struct
{
	unsigned short seq;	// sequence check id to make sure not stale
	int fd;			// fd to write to
}	rpcdata_t;

// Per-channel interface for communicating with external "giza" asr engine.

typedef struct
{
	unsigned rate;		// sample rate (default 8000)
	unsigned seq;		// sequence check value
	int pid, fd;		// giza session process id and fifo for audio
	bool partial;		// partial results accept flag
}	asrdata_t;

typedef struct
{
	char tmpl[512];
	unsigned timeout;
	unsigned hold_frames;
	unsigned on_frames;
	unsigned pause_frames;
	bool barge_in;
	void *result;
}	acuasrdata_t;

// Used to indicate audio playback speed.

typedef enum
{
	SPEED_FAST,
	SPEED_SLOW,
	SPEED_NORMAL
}	trunkspeed_t;

// This is a special "data" block that is embedded in each channel.  The content of this
// data block depends on which state the driver is currently processing (stepped) into.
// Hence, different driver states each use different representions of this state config
// data.

typedef	union
{
	// when in driver answer state

	struct
	{
		unsigned rings;		// rings to answer on
		timeout_t timeout;	// maximum wait time
		const char *transfer;	// id to transfer to if transfering answer
		Trunk *intercom;	// intercom for transfering answer
		const char *station;	// if answer to fax tone, station id
		const char *fax;	// fax branch script vector
	}	answer;

	// to be used in future fax state

	struct
	{
		char pathname[256];
		const char *station;	// fax station id
	}	fax;

	// used when in playing state

	struct
	{
		char list[256];		// a buffer to store comma seperated prompt list
		char *name;		// a pointer into the list
		const char *extension;	// file extension if passed in play command
		unsigned long offset;	// offset from start of file
		unsigned long limit;	// maximum bytes into file
		unsigned short term;	// keys that can directly terminate play
		playmode_t mode;	// mode of play
		writemode_t write;		// write mode for any output
		timeout_t timeout, maxtime;	// maximum play time
		unsigned repeat;		// number of repeats
		unsigned volume;		// play volume
		float gain, pitch;		// pitch and gain control
		trunkspeed_t speed;		// speed of playback
		const char *voice;		// voice library override
		const char *text;		// text alt tag for driver
		const char *cache;		// cache to use
		bool lock;			// lock flag
	}	play;

	// processing data block for record command

	struct
	{
		char *name, *save;		// filename to record, and optionally
						// save under if successful
		const char *encoding;		// audio encoding mode
		const char *annotation;		// file annotation to embed
		const char *extension;		// file extension to use
		const char *text;		// alt tag text for driver
		timeout_t timeout;		// max record time
		unsigned long offset;		// starting offset in file
		unsigned short term;		// dtmf keys to exit record
		unsigned long silence;
		unsigned long trim;		// samples to trim from end
		unsigned long minsize;		// minimum valid file size
		unsigned volume;		// record volume and gain
		float gain;
		short frames;			// frames to buffer if rotating
		bool append;			// flag to append to existing file
		bool info;	
		char filepath[65];		// buffer for full filename
		char savepath[65];		// buffer for save name if used
		char altinfo[128];
	}	record;

	// dial command processing

	struct
	{
		char digits[65];		// number to dial
		char *digit;			// a pointer into digits
             	const char *callingdigit;
		bool exit;			// hangup on completion flag
		dialmode_t dialer;		// dialing mode
		timeout_t interdigit;		// delay between digits (tone)
		timeout_t digittimer;		// timer for pulse dialing
		timeout_t timeout;		// wait time for call progress
		timeout_t offhook;		// hook time for flash
		timeout_t onhook;		// hook time for flase
		unsigned pulsecount;		// counter for pulse dialing
		bool sync;			// syncflag
	}	dialxfer;

	// collect handling

	struct
	{
		timeout_t timeout, first;	// interdigit and first digit timeout
		unsigned count;			// total digits to collect
		unsigned short term;		// digits which are terminating
		unsigned short ignore;		// digits to ignore
		void *map;			// map table, unused
		void *var;			// var to save results into if given
	}	collect;

	// asr listen state handling

	struct
	{
		timeout_t first;		// delay for first word
		timeout_t next;			// interword delay
		unsigned short term, count;	// word count
		float gain, pitch;		// gain control
		unsigned volume;		// volume control
		void *save;			// var to save results into
		char *wordlist[32];		// optional terminating wordlist
		char buffer[256];		// buffer for wordlist
	}	listen;

	// sleep state handling

	struct
	{
		timeout_t wakeup;		// wakeup timer
		unsigned rings;			// wakeup on ring n
		unsigned loops;			// loop
		const char *save;		// var to save tgi results
	}	sleep;

	// tone state handling

	struct
	{
		timeout_t wakeup, duration;	// tone and silence duration
		unsigned loops;			// number of times to repeat
		phTone *tone;			// tone entry if from config
		unsigned freq1, freq2;		// tone definition if manually set
		int ampl1, ampl2;		// tone definition if manually set
		Trunk *dialing;			// used when intercom dialing
		bool recall;			// used when intercom dialing
	}	tone;
	struct
	{
		Trunk *src;
		const char *msg;
		unsigned seq;
	}	send;

	// join state handling to join calls

	struct
	{
		timeout_t wakeup, maxwait;	// join wait times
		bool hangup;			// hangup if join is done?
		joinmode_t direction;		// full or half duplex?
		Trunk *trunk, *waiting;		// trunk to wait for
		phTone *tone;			// tone while waiting
		float inpgain, outgain;		// gain control pads
		char *recfn;			// record file if logged
		const char *encoding;		// encoding if logged
		const char *annotation;		// annotation if logged
		const char *extension;		// extension if logged
		unsigned count;			// retry count to sync
		unsigned seq;			// sequence check
		trunkevent_t reason;
		bool local;
	}	join;

	// xml load state handler

	struct
	{
		TrunkImage *image;		// image buffer to transcode into
		const char *url;		// url being requested
		const char *section;		// section to start at
		const char *parent;		// parent that requested
		const char *fail;		// fail label to branch to
		const char *database;		// used by sql driver
		char **vars;			// vars to pass
		bool post, attach, gosub;
		timeout_t timeout;		// maximum runtime before giving up
		char userid[64];		// user logged in as
		char filepath[256];		// work buffer
	}	load;
	
	// intercom state handling

	struct
	{
		Trunk *answer;			// answering for
		const char *transfer;		// transfering to
	}	intercom;
}	trunkdata_t;

// When posting an event to a trunk port, each event id also has optional data that
// may be passed based on the event id type.  The different trunk event data items
// are shown here:

typedef struct
{
	// event id

	trunkevent_t id;	// event id

	// data block based on event id

	union
	{
		// used for dtmf events from low level device

		struct
		{
			unsigned digit: 4;		// dtmf digit recieved
			unsigned duration: 12;		// duration of digit
			unsigned e1: 8;			// energy level of tones
			unsigned e2: 8;
		} dtmf;

		// used for tone detect from low level driver

		struct
		{
			unsigned tone: 8;
			unsigned energy: 8;		// energy level of tone
			unsigned duration: 16;		// duration of tone
			char *name;			// name of tone
		} tone;
	
		// used by events that pass intertrunk messages which must be
		// sequence checked to confirm.  Usually send command.

		struct
		{
			unsigned seq;		// sequence id to confirm
			Trunk *src;		// source making request
			const char *msg;	// text message
		} send;

		// used by low level driver to notify inbound ring

		struct
		{
			unsigned digit:  4;	// optional id if ring cadence supported
			unsigned duration: 24;	// duration of ring
		} ring;

		// used to process results of lookup requests

		struct
		{
			unsigned seq;
			bool result;
			char *data;
		} lookup;

		// used to perform intercom transfer

		struct
		{
			unsigned tid;		// original port 
			const char *transfer;	// session id being transfered
		} intercom;

		// used by some fax code

		struct
                {
                        int type;    
                        int param0;
                        int param1;
                        int channel;
                } fax;

		// used by tgi when exiting

		struct
		{
			unsigned status;	// process status result
			unsigned short seq;	// sequence check to validate
		}	exitpid;

		// used by tgi to notify started

		struct
		{
			int pid;		// process id of new tgi process
			unsigned short seq;	// sequence value
		}	waitpid;

		struct
		{
			const char *msg;
			const char *id;
		}	sync;

		trunkevent_t reason;		// generic reason code
		unsigned span;			// span related event, span id
		unsigned card;			// card related event, card id
		unsigned tid;			// port related event, port id
		bool ok;			
		int status, fd, pid;
		Trunk *trunk;			// inter-trunk related, ref trunk
		void *data;
		char **argv;			// argument list
		const char *error;	
		timeout_t duration;		// time related
		trunkstep_t step;		// step request
		char dn[8];
		dspmode_t dsp;
	} parm;
} TrunkEvent;

#pragma pack(1)

// The status node is used to report and share Bayonne line states when Bayonne servers
// form a global call state map.  It is also reported to Bayonne site monitoring tools.

typedef	struct {
	time_t	update;			// update time (live) of last message
	char name[16];			// name of node
	struct in_addr addr;		// ip address of node
	unsigned long uptime;		// total uptime of node
	unsigned long calls;		// total calls processed for node
	unsigned char version;		// protocol version id
	unsigned char buddies;		// number of failover buddies elected
	unsigned char spansize;		// number of spans
	unsigned short ports;		// total port count
	unsigned char service;		// service level flag
	char schedule[16];		// current scheduled mode
	char stat[960];			// port statistics
}	statnode_t;

#pragma pack()

/* This is used to bind user defined "functions" which may be loaded
   in a DSO module.
*/

bool getLogical(const char *string);
timeout_t getSecTimeout(const char *string);
timeout_t getMSTimeout(const char *string);
TGI *getInterp(char *cmd);
void getInterp(char *cmd, char **args);
void stopServers(void);
void startServers(void);
TrunkGroup *getGroup(const char *name);
Module *getModule(modtype_t mtype, const char *name);
Translator *getTranslator(const char *name);
void attachModules(Trunk *trunk);
void detachModules(Trunk *trunk);
phTone *getphTone(const char *name);
Request *request(TrunkGroup *group, char **argv, unsigned timeout, const char *tag, const char *pid);

/**
 * A call statistic class is used to manipulate call stats with a mutex
 * lock to prevent errors during "adjustment" periods.
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short call statistic collection object.
 */
class CallStat : public Mutex
{
protected:
	static unsigned long upincoming, upoutgoing;
	static time_t uptime;
	volatile bool dirty;

	time_t updated, prior, idled;

	int capacity;
	struct
	{
		int incoming;
		int outgoing;
	}	active, max, lastmax;

	struct
	{
		long incoming;
		long outgoing;
	}	total, lasttotal;

public:
	CallStat();

	/**
	 * Get current call capacity.
	 *
	 * @return capacity of this stat item.
	 */
	inline int getCapacity(void)
		{return capacity;};

	/**
	 * get a stat item.
	 *
	 * @param statitem to request.
	 * @return item value.
	 */
	long getStat(statitem_t item);

	/**
	 * Get a stat record at once.
	 *
	 * @param pointer to stats to copy.
	 */
	void getStat(unsigned long *save);

	/**
	 * inc active incoming call count.
	 */
	void incIncoming(void);

	/**
	 * dec active incoming call count.
	 */
	void decIncoming(void);

	/**
	 * inc active outging call count.
	 */
	void incOutgoing(void);

	/**
	 * dec active outgoing call count.
	 */
	void decOutgoing(void);

	/**
	 * Get idle time in seconds.
	 */
	long getIdleTime();

	/**
	 * Update stats, active to last.
	 */
	void update(void);
};

/**
 * Phrasebook modules are used to convert things like numbers into
 * sets of prompts that form spoken words.  Translations are dso based
 * and are represented by the "languages" plugin.  A translator for
 * spanish may handle not just numbers, dates, etc, but also the
 * selection of masculine and feminine forms of numbers based on
 * usage, etc.  The translator classes provide some basic mechanics
 * for phrasebook tts in Bayonne.
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short phrase translations dso base for tts.
 */
class Translator : protected Keydata
{
private:
	friend Translator *getTranslator(const char *name);
	static Translator *first;
	Translator *next;

protected:
	/**
	 * Return the language this translator supports.
	 *
	 * @return language supported.
	 */
	virtual char *getName(void) = 0;

	/**
	 * get play buffer object.
	 *
	 * @return put buffer
	 * @param trunk object
	 */
	char *getPlayBuffer(Trunk *trunk);

	Translator(const char *conf);

public:
	/**
	 * Perform a phrasebook translation of the current script
	 * statement and issue play request.
	 *
	 * @return ccscript error message or NULL.
	 * @param trunk object for phrasebook.
	 */
	virtual char *speak(Trunk *trunk) = 0;
};

/* Bayonne config file istanciation classes.  In Bayonne these are
   created as keydata objects out of bayonne.conf during process
   startup automatically.  This is Bayonne runtime configuration magic.
*/

/**
 * Load /etc/bayonne [tones] key values for user defined tone sets.
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short Load and build user defined tones.
 */
class KeyTones : protected Keydata
{
public:
	/**
	 * Initialize tone data.
	 */
	KeyTones();
};

/**
 * Load localization rules from [localize].
 * May have defaults appropriate to US.
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short Load localization rules.
 */
class KeyLocal : public Keydata
{
public:
	/**
	 * Load local rule set.
	 */
	KeyLocal();
};

/**
 * Load /etc/bayonne [handlers] for special gateway support applications.
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short Load handlers configuration data.
 */
class KeyHandlers : public Keydata
{
public:
	/**
	 * Initialize handlers.
	 */
	KeyHandlers();
};

/**
 * Load /etc/bayonne [voices] to select and map voice libraries to
 * different translators.
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short Load interpreter imports to use.
 */
class KeyVoices : public Keydata
{
public:
	/**
	 * Initialize keyimports data.
	 */
	KeyVoices();
};

/**
 * Load /etc/bayonne [imports] to provide interpreter pre-loading of
 * external modules.  This speeds startup.
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short Load interpreter imports to use.
 */
class KeyImports : public Keydata
{
public:
        /**
         * Initialize keyimports data.
         */
        KeyImports();
};


/**
 * Load /etc/bayoone [paths] key value pairs.  Has internal defaults
 * if section or file is missing.
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short Load keypaths path location configuration data.
 */
class KeyPaths : public Keydata
{
public:
	/**
	 * Initialize keypath data.
	 */
	KeyPaths();

	/**
	 * Get the driver and tgi base directories.
	 */
	inline const char *getLibexec(void)
		{return getLast("libexec");};

	/**
	 * Get library tgi exec search path.
	 */
	inline const char *getTgipath(void)
		{return getLast("tgipath");};

	/**
	 * Get prefix for DSO modules at install.
	 */
	inline const char *getLibpath(void)
		{return getLast("libpath");};

	/**
	 * Get the primary working storage for writable messages.
	 */
	inline const char *getDatafiles(void)
		{return getLast("datafiles");};

	/**
	 * Get the wrappers working space for inter-system bridging.
	 */
	inline const char *getWrappers(void)
		{return getLast("wrappers");};

	/**
	 * Get the runfile directory.
	 */
	inline const char *getRunfiles(void)
		{return getLast("runfiles");};

	/**
	 * Get the spool directory.
	 */
	inline const char *getSpool(void)
		{return getLast("spool");};

	/**
	 * Get the log directory.
	 */
	inline const char *getLogpath(void)
		{return getLast("logpath");};

	/**
	 * Get the primary script prefix directory.
	 */
	inline const char *getScriptFiles(void)
		{return getLast("scripts");};

	/**
	 * Get the prompt directory.
	 */
	inline const char *getPromptFiles(void)
		{return getLast("prompts");};

	/**
	 * Get the pre-cache directory.
	 */
	inline const char *getCache(void)
		{return getLast("cache");};

	/**
	 * Set auto-location prefix.
	 */
	void setPrefix(char *path);
};

/**
 * Load /etc/bayonne [network] key value pairs.  These are used to
 * specify dilu access methods and bindings.
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short Load database access bindings.
 */
class KeyNetwork : public Keydata
{
public:
	/**
	 * Initialize keydatabase data.
	 */
	KeyNetwork();

	/**
	 * Get refresh timer in seconds for inter-node activity.
	 *
	 * @return refresh in seconds.
	 */
	unsigned getRefresh(void)
		{return atoi(getLast("refresh"));};

	/**
	 * Get time delay for declairing a node "dead".
	 *
	 * @return time delay in seconds.
	 */
	unsigned getTimeToLive(void)
		{return atoi(getLast("live"));};

	/**
	 * Get time to elect a new buddy.
	 *
	 * @return time to elect in seconds.
	 */
	unsigned getTimeToElect(void)
		{return atoi(getLast("elect"));};

	/**
	 * Get time to expire a buddy.
	 *
	 * @return time to expire a buddy.
	 */
	unsigned getTimeToExpire(void)
		{return atoi(getLast("expire"));};

	/**
	 * Get broadcast address to use.
	 *
	 * @return broadcast address.
	 */
	InetHostAddress getBroadcast(void);

	/**
	 * Get bind address to use.
	 *
	 * @return binding address.
	 */
	InetAddress getAddress(void);

	/**
	 * Get bind address for monitoring module.
	 *
	 * @return monitor address.
	 */
	InetAddress getMonitorAddress(void);

	/**
	 * Get port for binding.
	 *
	 * @return port.
	 */
	tpport_t getPort(void);

	/**
	 * Get port for monitoring module.
	 *
	 * @return monitor port.
	 */
	tpport_t getMonitorPort(void);

	/**
	 * Get the host for bayonnedb broadcast.
	 *
	 * @return broadcast address.
	 */
	InetHostAddress getDBHost(void);

	/**
	 * Get the database port number.
	 *
	 * @return db port number.
	 */
	tpport_t getDBPort(void);
};

/**
 * Load /etc/bayonne [memory] key value pairs.  This is used to
 * configurate space management properties of the runtime environment
 * including audio buffering, page size allocations, etc.
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short Load memory related options.
 */
class KeyMemory : public Keydata
{
public:
	/**
	 * Initialize keymemory data.
	 */
	KeyMemory();

	/**
	 * Get default symbol space size for variables.
	 *
	 * @return number of bytes of storage for new symbols.
	 */
	inline int getSymbolSize(void)
		{return atoi(getLast("symbols"));};

	/**
	 * Get default page allocation size to use for "paged" memory
	 * allocations.
	 *
	 * @return page size for default paging.
	 */
	inline int getPageSize(void)
		{return atoi(getLast("page"));};

	/**
	 * Get maximum users.
	 *
	 * @return maximum users.
	 */
	inline size_t getUserCount(void)
		{return atoi(getLast("users"));};

	/**
	 * Get maximum preferences per user.
	 *
	 * @return maximum preferences.
	 */
	inline size_t getPrefCount(void)
		{return atoi(getLast("prefs"));};
};

/**
 * Load /etc/bayonne [thread] key value pairs.  Has internal defaults
 * if section or file is missing.
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short Load keythreads priority and session count configuration.
 */
class KeyThreads : public Keydata
{
public:
	/**
	 * Initialize keythread data.
	 */
	KeyThreads();

	/**
	 * Get relative priority to run service threads at.
	 *
	 * @return service thread priority (relative).
	 */
	inline int priService(void)
		{return atoi(getLast("services"));};

	/**
	 * Get the relative priority to run scheduler at.
	 *
	 * @return scheduler priority (relative).
	 */
	inline int priScheduler(void)
		{return atoi(getLast("scheduler"));};

	/**
	 * Get the relative priority to run gui at.
	 *
	 * @return gui priority (relative).
	 */
	inline int priGUI(void)
		{return atoi(getLast("gui"));};

	/**
	 * Get the relative priority to run rtp at.
	 *
	 * @return rtp priority (relative).
	 */
	inline int priRTP(void)
		{return atoi(getLast("rtp"));};

	/**
	 * Get number of milliseconds to delay each script step.
	 *
	 * @return millisecond delay interval.
	 */
	inline int getStepDelay(void)
		{return atoi(getLast("stepdelay"));};

	/**
	 * Get the minimal step interval.
	 *
	 * @return millisecond minimal interval.
	 */
	inline int getStepInterval(void)
		{return atoi(getLast("stepinterval"));};

	/**
	 * Get the reset delay required for settling the DSP.
	 *
	 * @return millisecond delay from dsp reset.
	 */
	inline int getResetDelay(void)
		{return atoi(getLast("resetdelay"));};

	/**
	 * Get default stack size for threads.
	 *
	 * @return stack size in "k" increments.
	 */
	size_t getStack(void);

	/**
	 * Get count of service pool threads to use.
	 *
	 * @return thread count for service pool.
	 */
	int getServices(void);

	/**
	 * Get the execution priority of the resolver thread.
	 *
	 * @return priority of resolver.
	 */
	int priResolver(void);

	/**
	 * Get the execution interval of the resolver thread, or
	 * 0 if no resolver scheduler.
	 *
	 * @return number of minutes for interval.
	 */
	int getResolver(void);

	/**
	 * Get relative priority to run audio streams at.
	 *
	 * @return audio thread priority (relative).
	 */
	inline int priAudio(void)
		{return atoi(getLast("audio"));};

	/**
	 * Get the auditing flag.
	 *
	 * @return thread auditing flag.
	 */
	inline bool getAudit(void)
		{return getLogical(getLast("audit"));};
	/**
	 * Get relative priority to run gateway (TGI) sessions at.
	 *
	 * @return tgi gateway process priority (niceness).
	 */
	inline int priGateway(void)
		{return atoi(getLast("gateways"));};

	/**
	 * Get the relative priority to run network management sessions.
	 *
	 * @return priority for management threads.
	 */
	inline int priManager(void)
		{return atoi(getLast("managers"));};

	/**
	 * Get the relative priority for network service thread.
	 *
	 * @return priority for lookup thread.
	 */
	inline int priNetwork(void)
		{return atoi(getLast("network"));};

	/**
	 * Get the relative priority for switch integration module.
	 *
	 * @return priority for switch integration.
	 */
	inline int priSwitch(void)
		{return atoi(getLast("switch"));};

	/**
	 * Scheduler execution interval.
	 *
	 * @return scheduler interval in minutes.
	 */
	inline int getInterval(void)
		{return atoi(getLast("interval"));};

	/**
	 * Scheduler network node rebroadcast frequency.
	 *
	 * @return node broadcast update interval in seconds.
	 */
	inline int getRefresh(void)
		{return atoi(getLast("refresh"));};

	/**
	 * Get number of tgi gateway proccesses to make available.
	 *
	 * @return number of tgi gateway processes.
	 */
	int getGateways(void);
	
	/**
	 * Get default Bayonne system priority (niceness) to start
	 * under before we adjust any relative priorities.
	 *
	 * @return primary "nice" process priority.
	 */
	inline int getPriority(void)
		{return atoi(getLast("priority"));};

	/**
	 * Get scheduling policy to use.
	 *
	 * @return policy id, or "0" for default.
	 */
	int getPolicy(void);

	/**
	 * Get memory locking and local pages.
	 *
	 * @return number of k of stack to pre-allocate.
	 */
	inline int getPages(void)
		{return atoi(getLast("pages"));};
};

/**
 * A base class for plugins that are used to modify policies.  These are
 * typically used for outbound calling systems, to process distribution
 * lists, etc.
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short Policy expansion class.
 */
class Policy
{
private:
	friend class TrunkGroup;

	Policy *next;
	TrunkGroup *group;

protected:
	Policy(TrunkGroup *grp);
	
	virtual Request *loPriority(void)
		{return NULL;};

	virtual Request *hiPriority(void)
		{return NULL;};
};

/**
 * Requests are used to queue service requests to a trunk group.  These
 * are usually for scripts that will perform some form of outbound
 * dialing.  Requests are queued until either they can be serviced, or
 * they are expired.
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short Request service.
 */
class Request
{
private:
	friend class TrunkGroup;

	static unsigned seq;

	unsigned id;
	Request *next;
	time_t expires;
	TrunkGroup *group;

	char *argv[33];
	char buffer[512];
	char tagid[65];
	char parent[65];

	void detach(void);

public:
	Request(TrunkGroup *grp, const char *text, unsigned expire, const char *tag = NULL, const char *pid = NULL);
	~Request()
		{detach();};

	inline const char *getParent(void)
		{return (const char *)parent;};

	inline char **getList(void)
		{return argv;};

	inline char *getTag(void)
		{return tagid;};

	bool isExpired(void);

	friend void cancel(TrunkGroup *group, const char *tag);
	friend Request *request(TrunkGroup *group, char **argv, unsigned timeout, const char *tag = NULL, const char *pid = NULL);
	friend Request *locate(TrunkGroup *group, const char *tag, int *pos);
};

/**
 * Trunk "groups" provide keydata configuration information that apply
 * to a group of trunk ports represented under a common "group" identity.
 * These are initially given a [trunks] section for default group values
 * followed by a specific entry.  The [server] groups key lists the active
 * trunk groups.  A special default group is also created.
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short Trunk group configuration.
 */
class TrunkGroup : public Keydata, public CallStat
{
private:
	friend class KeyServer;
	friend class Scheduler;
	friend class TestDebug;
	friend class Request;
	friend class Policy;
	friend class Trunk;
	friend class MappedStats;
	static TrunkGroup *first;
	TrunkGroup *next;
	char schedule[65];
	char planned[65];
	unsigned trump;
	Request *reqfirst, *reqlast;
	Policy *polFirst;
	unsigned members;

	void setSchedule(const char *str);

	friend inline const char *getGroups(void)
		{return TrunkGroup::first->getLast("groups");};

	friend void cancel(TrunkGroup *group, const char *tag);
	friend Request *locate(TrunkGroup *group, const char *tag, int *pos = NULL);


public:
	/**
	 * Create a trunk group data key from the bayonne.conf file.
	 *
	 * @param name of trunk group to load.
	 */
	TrunkGroup(char *name = NULL);

	/**
	 * Get the name of this trunk group.
	 *
	 * @return name of trunk group.
	 */
	inline const char *getName(void)
		{return getLast("name");};

	/**
	 * Interface to force logging of call statistics.
	 */
	static void logStats(void);

	/**
	 * Get the number of rings before answering.
	 *
	 * @return number of rings to answer.
	 */
	inline unsigned getAnswer(void)
		{return atoi(getLast("answer"));};

	/**
	 * Get the rpc support state for this group.
	 *
	 * @return true if rpc permitted.
	 */
	bool getRPC(void);

	/**
	 * Get the accepting state option for this trunk.
	 *
	 * @return true if should accept.
	 */
	bool getAccept(void);

	/**
	 * Get dialtone detect state for the trunk.
	 *
	 * @return true if dialtone detect should be enabled.
	 */
	bool getDetect(void);

	/**
	 * Get the number of milliseconds for active caller id.
	 *
	 * @return number of milliseconds of caller id.
	 */
	inline timeout_t getCallerid(void)
		{return getMSTimeout(getLast("callerid"));};

	/**
	 * Get pickup gaurd time for this trunk group.
	 *
	 * @return number of milliseconds for call pickup.
	 */
	inline timeout_t getPickup(void)
		{return getMSTimeout(getLast("pickup"));};

	/**
	 * Get handling for trunk group "pending requests".
	 *
	 * @return symbol or timer value.
	 */
	inline const char *chkRequest(void)
		{return getLast("requests");};

	/**
	 * Get a trunk group port selection method.
	 *
	 * @return select method.
	 */
	seltype_t getSelect(void);

	/**
	 * Get trunk threashold for requests to be posted.
	 *
	 * @return threashold.
	 */
	inline unsigned getThreashold(void)
		{return atoi(getLast("threashold"));};

	/**
	 * Get call progress analysis timeout for dialing.
	 *
	 * @return timeout in seconds.
	 */
	inline unsigned getAnalysis(void)
		{return atoi(getLast("analysis"));};

	/**
	 * Get ready timer for trunk before handling requests when
	 * idle.
	 *
	 * @return ready timer in milliseconds.
	 */
	inline timeout_t getReady(void)
		{return getMSTimeout(getLast("ready"));};
	
	/**
	 * Get the initial idle timer for this group.
	 *
	 * @return idle time initial.
	 */
	inline timeout_t getIdleTime(void)
		{return getMSTimeout(getLast("idletime"));};

	/**
	 * Get the trunk seizure timer for dialtone detect.
	 *
	 * @return siezure time for dialtone.
	 */
	inline unsigned getSiezeTime(void)
		{return atoi(getLast("siezetime"));};

	/**
	 * Get the time delay of each ring.
	 *
	 * @return ring time between rings.
	 */
	inline unsigned getRingTime(void)
		{return atoi(getLast("ringtime"));};

	/**
	 * Get call progress minimum digits
	 *
	 * @return number of digits (only for dialogic drivers).
	 */
	inline unsigned getMinDigits(void)
		{return atoi(getLast("mindigits"));};

	/**
	 * Get call progress time out to get more digits
	 *
	 * @return Timeout in seconds
	 */
	inline unsigned getMDigTimeOut(void)
		{return atoi(getLast("mdigtimeout"));};

	/**
	 * Get disconnect gaurd time before answering.
	 *
	 * @return gaurd time in milliseconds.
	 */
	inline timeout_t getHangup(void)
		{return getMSTimeout(getLast("hangup"));};

	/**
	 * Get default hook flash time for this trunk group.
	 *
	 * @return hookflash time in milliseconds.
	 */
	inline timeout_t getFlash(void)
		{return getMSTimeout(getLast("flash"));};

	/**
	 * Get dialtone wait time for this trunk group.
	 *
	 * @return dialtone wait time in milliseconds.
	 */
	inline timeout_t getDialtone(void)
		{return getMSTimeout(getLast("dialtone"));};

	/**
	 * Get dialing speed in milliseconds.
	 *
	 * @return dialspeed in milliseconds.
	 */
	inline timeout_t getDialspeed(void)
		{return getMSTimeout(getLast("dialspeed"));};

	/**
	 * Get the telephone number associated with this trunk group
	 * if one is associated with it.
	 *
	 * @return telephone number if known.
	 */
	const char *getNumber(void);

	/**
	 * Get the name of the script to "schedule" for this group.
	 *
	 * @return name of scheduled script.
	 * @param buffer to copy schedule information into.
	 */
	const char *getSchedule(char *buf);

	/**
	 * Get the name of the script to "schedule" for a given
	 * call redirection mode for this group.  If no redirect
	 * option is found, then getSchedule is invoked.
	 *
	 * @return name of scheduled script.
	 * @param redirect identifer.
	 * @param buffer to copy schedule information into.
	 */
	const char *getRedirect(const char *redirect, char *buf);

	/**
	 * Used when mapping trunk groups to activated trunks.
	 */
	inline void incCapacity(void)
		{++capacity;};

	/**
	 * Get the next active request pending for this group.
	 *
	 * @return next request queued or NULL.
	 */
	Request *getRequest(void);

	/**
	 * Find a named trunk group.
	 *
	 * @return trunk group object if found.
	 */
	friend TrunkGroup *getGroup(const char *name = NULL);

	/**
	 * Get next group record.
	 *
	 * @return next group link.
	 */
	inline TrunkGroup *getNext(void)
		{return next;};
};

/**
 * This class is a cache for server specific configuration information
 * which may be configured from /etc/bayonne.conf [server].
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short load server configuration data.
 */
class KeyServer : public Keydata
{
private:
	unsigned uid, gid;
	const char *altdir;
	const char *phrdir;

public:
	/**
	 * Load active server configuration.
	 */
	KeyServer();

	/**
	 * Get the name of the node identifier.
	 *
	 * @return node id.
	 */
	inline const char *getNode(void)
		{return getLast("node");};

	/**
	 * Return default script attach login state.
	 *
	 * @return login id.
	 */
	inline const char *getLogin(void)
		{return getLast("login");};

	/**
	 * Return policy search order.
	 *
	 * @return policy search order.
	 */
	inline const char *getPolicyOrder(void)
		{return getLast("policy");};

	/**
	 * Get the remote access password.
	 *
	 * @return remote access password.
	 */
	inline const char *getPassword(void)
		{return getLast("password");};

	/**
	 * Get tgi token seperator.
	 *
	 * @return token seperator.
	 */
	inline const char *getToken(void)
		{return getLast("token");};

	/**
	 * Load all active trunk group records.
	 */
	void loadGroups(bool test);

	/**
	 * Get default trunk group schedule name.
	 *
	 * @return schedule name.
	 */
	inline const char *getDefault(void)
		{return getLast("default");};

	/**
	 * get group id.
	 *
	 * @return gid
	 */
	inline unsigned getGid(void)
		{return gid;};

	/**
	 * get user id.
	 *
	 * @return uid
	 */
	inline unsigned getUid(void)
		{return uid;};

	/**
	 * set the user's id if you can.
	 */
	void setUid(void);

	/**
	 * set the user's gid if you can.
	 */
	void setGid(void);

	/**
	 * Get number of nodes.
	 *
	 * @return node count.
	 */
	inline int getNodeCount(void)
		{return atoi(getLast("nodes"));};

	/**
	 * Get alternate home config directory for root startup.
	 *
	 * @return alternate directory or NULL.
	 */
	inline const char *getPrefix(void)
		{return altdir;};

	/**
	 * Get local phrasebook config directory.
	 *
	 * @return alternate phrasebook.
	 */
	inline const char *getPhrases(void)
		{return phrdir;};
};

/**
 * This class is used to load and manage "plugin" modules as called for
 * in /etc/bayonne.conf [plugins].
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short Load and manage plugins support.
 */
class Plugins : public Keydata
{
private:
	unsigned pidcount;
	int pids[32];

public:
	/**
	 * Load plugins key data and initialize.
	 */
	Plugins();

	/**
	 * Unload all active DSO modules for plugins.
	 */
	~Plugins();

	/**
	 * Get the name of the driver API being used.
	 *
	 * @return driver api name.
	 */
	char *getDriverName(void);

	/**
	 * Load a debug module or stub interface.
	 */
	void loadDebug(void);

	/**
	 * Load a monitor module or stub interface.
	 */
	void loadMonitor(void);

	/**
	 * Attempt to load a DSO IVR API driver subsystem on top of the
	 * Bayonne server.  On failure a DSO exception is thrown.
	 */
	void loadDriver(void);

	/**
	 * Attempt to load an optional switch integration module for
	 * GNU Bayonne.
	 */
	void loadSwitch(void);

	/**
	 * Attempt to load an optional text to speech plugin module for
	 * GNU Bayonne.
	 */
	void loadTTS(void);

	/**
	 * Attempt to load a sql query module for Bayonne.
	 */
	void loadSQL(void);

	/**
	 * Load automatic application extensions module.
	 */
	void loadExtensions(void);

	/**
	 * Attempt to load DSO based generic functions into the server.
	 */
	void loadPreload(void);

	/**
	 * Pre-load ccaudio codec modules that may be needed.
	 */
	void loadCodecs(void);

	/**
	 * Attempt to load DSO based protocol modules into the server.
	 */
	void loadModules(void);

	/**
	 * Attempt to load TGI based resident interpreters.
	 */
	void loadTGI(void);

	/**
	 * Attempt to load DSO network management interfaces.
	 */
	void loadManagers(void);

	/**
	 * Attempt to load DSO based TTS translation modules into server.
	 */
	void loadTranslators(const char *lcp = NULL);

};

/**
 * We derive a Bayonne server version of ScriptCommand, aaScript,
 * which holds most common elements of the script engine for Bayonne
 * use.  Individual drivers may further derive sub-dialects.
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short Bayonne script dialect.
 */
class aaScript : public ScriptRuntime
{
protected:
	/**
	 * New GetTrapMask() used to set DTMF bit when dtmf digits
	 * are also requested in ^traps.
	 *
	 * @return trap mask to apply.
	 * @param trap name being evaluated.
	 */
	unsigned long getTrapMask(const char *trapname);

public:
	/**
	 * Default scripting environment.
	 */
	aaScript();

	/**
	 * Used to bind "generic" modules to aaScript.
	 *
	 * @param Module to bind.
	 */
	void addModule(Module *module, const char *alias = NULL);

	/**
	 * Add unused commands to aaScript for compiler clean-ness.
	 *
	 * @param names of commands to add.
	 */
	void addDummy(const char *names);

	int mapicmp(const char *s1, const char *s2);
        int mapnicmp(const char *s1, const char *s2, size_t n);

	virtual const char *getExternal(const char *opt);
};

/**
 * We derive a Bayonne compiler and script image container, aaImage,
 * to hold active script sets for the trunk class script engine.  This
 * class is almost never further derived in drivers, though the driver
 * "getScript" method is used to load it.
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short Bayonne script image.
 */
class aaImage : public ScriptCompiler
{
public:
	class Schedule
	{
	public:
		Schedule *next;
		char *name[2], *day[2], *start[2], *command[2];
		void update(void);
	};
private:
	int	fduser, fdhunt, fdline;
	static Schedule *active;
	Schedule *current;

protected:
	/**
	 * Used to parse and determine if a given directory file
	 * should be "compiled".  Usually tests for *.scr.
	 *
	 * @return true if this file should be compiled.
	 * @param file name to test.
	 */
	virtual bool isScript(const char *scriptname);

	/**
	 * Used to scan and compile a script collection from a specified
	 * directory for modules, "pass 1".
	 *
	 * @param directory to scan.
	 */
	void scanDir1(char *path);

	/**
	 * Used to scan for default .scr scripts unless overriden by
	 * module specific scripts in pass 1.
	 *
	 * @param directory to scan.
	 */
	void scanDir2(char *path);

        /**
         * used to create dialect specific pre-precessor directives.  A
	 * virtual in ccScript 2.1.1, useless otherwise...
         *
         * @return true if directive claimed.
         * @param directive
         * @param script object being built
         */
        bool preProcess(const char *directive, Name *script);

public:
	/**
	 * Default image compiler.
	 */
	aaImage(aaScript *script);

	/**
	 * get active schedule.
	 */
	static Schedule *getSchedule(void)
		{return active;};
};

class SoftJoin : protected Mutex
{
public:
	SoftJoin(Trunk *t1, Trunk *t2);
	~SoftJoin();

private:
	Trunk *trk1;
	Trunk *trk2;

	AudioBuffer *buf1;
	AudioBuffer *buf2;
};

/**
 * The mixer object is a resource for conferences.
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short Bayonne conference mixer.
 */
class Mixer : protected Mutex
{
private:
	friend class Conference;
	void addGroup(void);
	void delGroup(void);

protected:
	Mixer();
	unsigned avail, members, groups;

public:
	unsigned getAvail(void)
		{return avail;};

	unsigned getMembers(void)
		{return members;};

	unsigned getGroups(void)
		{return groups;};

	virtual bool setMixer(int groups, int members) = 0;

	virtual Conference *getConference(int group) = 0;
};

/**
 * The conference object references a conference generic resource.
 * These may be created and destroyed as needed, typically thru
 * the management of an advance conference resource scheduler.
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short Bayonne conference resource.
 */
class Conference : protected Mutex
{
protected:
	Mixer *mixer;
	unsigned limit;
	unsigned members;
	int *membership;

	virtual ~Conference();

	Conference(Mixer *m);
public:
	inline virtual Mixer *getMixer(void)
		{return mixer;};

	int *getMembership(void)
		{return membership;};

	Trunk *getTrunk(unsigned member);

	unsigned getMembers(void)
		{return members;};

	unsigned getLimit(void)
		{return limit;};

	virtual bool setConference(unsigned max) = 0;
};

/**
 * We derive a Bayonne server version of ScriptInterp, "Trunk",
 * which holds most common elements of the script engine for Bayonne
 * use.  This is also the base of the channel port structure for
 * Bayonne.  Drivers will further derive this as "DriverTrunk".
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short Bayonne channel port script engine.
 */
class __EXPORT Trunk : public ScriptInterp
{
private:
	friend class MappedCalls;
	friend class TestDebug;
	friend class aaScript;
	friend class Translator;
	friend class __EXPORT ScriptInterp;
	//friend class ScriptInterface;
	friend class Service;
	friend class AudioService;
	friend class Fifo;
	friend class Driver;
	friend class Module;

	typedef union
	{
        	Symbol sym;
        	char data[sizeof(Symbol) + 12];
	}       Number;

	typedef union
	{
        	Symbol bin;
        	char data[sizeof(Symbol) + MAX_DIGITS];
	}       Digit;

protected:
	static char status[];
	phTone *tonetmp;
	int tsid;
	static struct callrec_t *callrec;
	Trunk *ctx;

private:
	unsigned member;
	Line	*cdrv;
	int cdrc;
	char apppath[64];

	bool scrSession(void);
	bool scrPolicy(void);
	bool scrConfig(void);
	bool scrSend(void);
	bool scrStart(void);
#ifdef	HAVE_TGI
	bool scrLibexec(void);
#endif
	bool scrHangup(void);
	bool scrDebug(void);
	bool scrAlog(void);
	bool scrAudit(void);
	bool scrSleep(void);

#ifdef	SCRIPT_IF_OVERRIDE
public:
	static bool hasDriver(ScriptInterp *interp, const char *v);
	static bool hasVoice(ScriptInterp *interp, const char *v);
	static bool hasAppVoice(ScriptInterp *interp, const char *v);
	static bool hasSysVoice(ScriptInterp *interp, const char *v);
	static bool hasAltVoice(ScriptInterp *interp, const char *v);
	static bool hasGroup(ScriptInterp *interp, const char *v);
	static bool hasPlugin(ScriptInterp *interp, const char *v);
	static bool hasSysPrompt(ScriptInterp *interp, const char *v);
	static bool hasVarPrompt(ScriptInterp *interp, const char *v);
	static bool isNode(ScriptInterp *interp, const char *v);
	static bool isService(ScriptInterp *interp, const char *v);
	static bool isSchedule(ScriptInterp *interp, const char *v);
	static bool ifDTMF(ScriptInterp *interp, const char *v);
	static bool ifFeature(ScriptInterp *interp, const char *v);
	static bool isDnd(ScriptInterp *interp, const char *v);
	static bool ifRinging(ScriptInterp *interp, const char *v);
	static bool ifRunning(ScriptInterp *interp, const char *v);
	static bool ifPort(ScriptInterp *interp, const char *v);
#endif

protected:
	bool scrSync(void);
	bool scrAnswer(void);
	bool scrDial(void);
	bool scrTransfer(void);
	bool scrHold(void);
	bool scrRedirect(void);
	bool scrRoute(void);

private:
	bool scrTone(void);
	bool scrAccept(void);
	bool scrReject(void);
	bool scrCollect(void);
	bool scrFlash(void);
	bool scrSlog(void);
	bool scrSay(void);
	bool scrAsr(void);
	bool scrListen(void);
	bool scrCleardigits(void);
	bool scrAltPlay(void);
	bool scrAltSpeak(void);
	bool scrPlay(void);
	bool scrSendFax(void);
	bool scrRecvFax(void);
	bool scrExiting(void);
	bool scrCount(void);
	bool scrScan(void);
	bool scrCopy(void);
	bool scrBuild(void);
	bool scrDelete(void);
	bool scrMove(void);
	bool scrErase(void);
	bool scrRecord(void);
	bool scrSpeak(void);
	bool scrModule(void);
	bool scrDummy(void);
	bool scrSchedule(void);
	bool scrSignal(void);
	bool scrIdle(void);
	bool scrBusy(void);
	bool scrExamine(void);
	bool scrService(void);
	bool scrJoin(void);
	bool scrWait(void);
	bool scrRing(void);
	bool altDial(void);

protected:

	static char digit[16];
	//ScriptInterface *script;
	TrunkGroup *group;
	int id;
	unsigned span;
	time_t starttime, idletime, synctimer, exittimer;

	char exitmsg[128];
	volatile unsigned seq;
	int idle_timer;
	unsigned rings, digits;

	Driver *driver;

public:
	AudioBuffer *softJoin;

protected:
	Trunk *joined;

	Service *thread;
	trunkdata_t data;
	execdata_t tgi;
	rpcdata_t rpc;
	asrdata_t asr;
	acuasrdata_t acuasr;
	Digit dtmf;
	char buffer[65];
	char extNumber[5];
	bool dialgroup[10];
	bool *ringIndex;
	bool isRinging;

	struct
	{
		bool offhook: 1;
		bool dtmf: 1;
		bool script: 1;
		bool reset: 1;
		bool timer : 1;
		bool audio: 1;
		bool once : 1;
		bool ready : 1;
		bool echo : 1;
		unsigned onexit : 1;
		trunkmode_t trunk: 2;
		dspmode_t dsp: 4;
		dtmfmode_t digits: 2;
		bool dnd : 1;
		bool cid : 1;	// captured cid state
		bool sent : 1;	// cid info sent
		bool listen : 1; // listening
		bool bgm : 1;	// bgm playing
	} flags;

	/**
	 * Set idle count of the driver.
	 */
	void setIdle(bool mode);

	/**
	 * Set symbol constants on attach.
	 */
	virtual void initSyms(void) = 0;

	/**
	 * Our default mask includes timeout.
	 *
	 * @return default mask.
	 */
	unsigned long getTrapDefault(void)
		{return 0x00000007;};

	/**
	 * Replace a symbol if a valid value is passed for a replacement.
	 *
	 * @param symbol name.
	 * @param replacement value.
	 */
	void repSymbol(const char *id, const char *data);

public:
	/**
	 * This provides an interface to internal symbol definitions.
	 *
	 * @return symbol value.
	 * @param symbol name.
 	 */
	virtual const char *getExternal(const char *opt);

	/**
	 * A derived Commit handler, allows "clear %digits", etc.
	 *
	 * @param symbol entry.
 	 */
	void commit(Symbol *sym);

	bool isStation;

	int getASR(void)
		{return asr.fd;};

	/**
	 * Flag if admin user.
	 */
	bool isAdmin(void);

	/**
	 * Get a fax station identifier for a script.
	 *
	 * @return pointer to station id string.
	 */
	const char *getStation(void);

	/**
	 * Get a "timeout" option.  This is like getValue, however
	 * the default timeout supplied is from the constant table,
	 * and special options for numeric times of various types can
	 * be used.
	 *
	 * @return default timeout in milli-seconds.
	 * @param optional string to parse rather than option.
	 */
	timeout_t getTimeout(const char *keyword = NULL);

	/**
	 * Extract an extension reference, either directly, or from
	 * an encoded global/local session identifier.
	 *
	 * @return extension number or NULL if invalid.
	 * @param extension number or session id.
	 */
	static const char *getExtReference(const char *ref);

	/**
	 * Get a "interdigit" timeout option.  This is like getValue,
	 * however the interdigit value supplied is from the const table.
	 *
	 * @return interdigit timeout in milli-seconds.
	 */
	timeout_t getInterdigit(const char *keyword = NULL);

	/**
 	 * Get a dtmf bit "mask".
	 *
	 * @return dtmf bit mask of digits.
	 */
	unsigned short getDigitMask(const char *keyword = NULL);

	/**
	 * Notify the script subsystem of a completion event.
	 *
	 * @param completion event signal id.
	 */
	bool trunkSignal(trunksignal_t);

	/**
	 * Branch to a named script event handler.
	 *
	 * @return true if handler exists
	 * @param name of handler to branch to.
	 */
	bool trunkEvent(const char *event);

protected:
	/**
	 * Rewrite a telephone number based on trunk group stored
	 * digit rewrite rules, and post into dial command buffer.
	 *
	 * @param number to dial.
	 */
	void dialRewrite(const char *dialstring);

	/**
	 * Post ASR result for script event branching.
	 *
	 * @return true if asr branch.
	 * @param partial asr result.
	 */
	bool asrPartial(const char *text);

	/**
	 * This function sets dtmf detection based on the script
	 * interpreter's current trap mask.  This is often used as
	 * the initial handler for setting dtmf detection when
	 * entering each trunk driver's state.
	 */
	virtual void setDTMFDetect(void);

	/**
	 * Set actual dtmf detection in the derived trunk class driver.
	 * This typically is called by the "initial" state handler when
	 * entering a trunk driver call processing state when an implicit
	 * setting is required (such as "collect", which forces enable).
	 *
	 * @param true to enable DTMF detection.
	 */
	virtual void setDTMFDetect(bool enable)
		{flags.dtmf = enable;};

	/**
	 * This is used to reset service threads and generally cleanup
	 * the session handler.  It is a virtual since driver specific
	 * implimentations may vary.
	 */
	virtual void stopServices(void);

	/**
	 * Used to perform state transitions when trunk is in "step"
	 * state, from the Bayonne script engine.  This call is used
         * rather than "postEvent" with TRUNK_MAKE_STEP since the Bayonne
	 * engine is already in the context of the callback thread
	 * and invoked from a postEvent initiated call.  Hence, this
	 * saves the overhead rather of a recursive postEvent call.
	 *
	 * @param new state to use.
	 */
	virtual void trunkStep(trunkstep_t step) = 0;

	/**
	 * This is used to see if the total timer has expired.
	 *
	 * @return true if should hangup.
	 */
	bool idleHangup();

	/**
	 * This is used to determine if the trunk is currently "idle"
	 * and for how long.
	 *
	 * @return number of seconds idle.
 	 */
	virtual unsigned long getIdleTime(void) = 0;

public:
	/**
	 * Compute a prefix path based on prefix passed
	 *
	 * @return prefix path.
	 */
	const char *getPrefixPath(void);

protected:
	/**
	 * This is used to post a step update back to the script engine.
	 */
	inline bool scriptStep(void)
		{return ScriptInterp::step();};	

	/**
	 * We override ScriptInterp::Attach with our own that adds
	 * additional support.  This attach initializes a series of
	 * required and default "variables" for the script interpreter.
	 *
	 * @return true on success
	 * @param name of script to start.
	 */
	bool attach(const char *scrname);

	/**
	 * We override ScriptInterp::Detach with our own that adds
	 * additional support to purge the variable pool.
	 */
	void detach(void);

	/**
	 * We get the scheduled or dnis or callerid map table values.
	 *
	 * @return argument list for startup.
	 * @param buffer for defaults.
	 */
	char **getInitial(char **args);

	/**
	 * Set a list of keyword values into the variable space of the
	 * script interpreter.
	 *
	 * @param list of keyword pairs.
	 */
	void setList(char **list);

	/**
	 * Accept method for accept scripts.
	 */
	virtual void accept(void)
		{return;};

	/**
	 * Get the voice library extension set to use.
	 */
	virtual const char *getLibexec(void)
		{return ".au";};

	/**
	 * Start the listen thread for the current port and return true
	 * if supported.
	 */
	virtual bool setListen(bool on)
		{return false;};

	/**
	 * Start the background audio play thread for the current port.
	 */
	virtual bool setBMG(bool on)
		{return false;};

public:
	/**
	 * Get the default audio encoding format to use.
	 */
	virtual const char *getDefaultEncoding(void)
		{return "ulaw";};

	/**
	 * Get the string name of an audio encoding type.
	 */
	static const char *getEncodingName(Audio::Encoding e);

protected:
	/**
	 * Reject method for reject scripts.  In case needed.
	 */
	virtual void reject(void)
		{return;};

	/**
	 * Enter a state and post it's identifier in local sym.
	 */
	void enterState(const char *state);

	/**
	 * Write output to an attached tgi, signal and disconnect if
	 * write fails (assumed non-blocking).
	 */
	bool writeShell(const char *text);

	/**
	 * Write output to an rpc receiver.
	 */
	bool writeRPC(const char *msgtype, const char *text);

	Trunk(int port, Driver *drv, int card = 0, int span = 0);
public:
	/**
	 * Get the timeslot port number.
	 *
	 * @return driver port.
	 */
	unsigned getId(void)
		{return id;};
	/**
	 * Get the trunk sequence number.
	 *
	 * @return sequence.
	 */
	inline unsigned getSequence(void)
		{return seq;};

	inline Driver *getDriver(void)
		{return driver;};

	/**
	 * Set the sleep save marker for sleeping tasks.
	 *
	 * @param sym name.
	 */
	inline void setSave(const char *save)
		{data.sleep.save = save;};

	/**
	 * Get driver capabilities.
	 *
	 * @return capability mask.
	 */
	virtual unsigned long getCapabilities(void)
		{return TRUNK_CAP_VOICE | TRUNK_CAP_DIAL;};

	/**
	 * Get the device logical name number.
	 *
	 * @param Buffer to store name.
	 */
	virtual void getName(char *buffer) = 0;

	/**
	 * Invoke a runtime state handler for a trunk driver.  This
	 * must be in an event in the derived TrunkDriver class.
	 *
	 * @return true if event claimed.
	 * @param derived method to call.
	 */
	virtual bool postEvent(TrunkEvent *event) = 0;

protected:
	/**
	 * Receive an event message from a foreign trunk and process it
	 * locally.
	 *
	 * @return true if event message valid.
	 */
	bool recvEvent(TrunkEvent *event);

	/**
	 * Post a sync event to the parent.
	 *
	 * @return true if successful.
	 */
	bool syncParent(const char *msg);

	/**
	 * Set callrec to call type specified.
	 *
	 * @param calltype.
	 */
	void setCalls(const char *mode);

	/**
	 * Set a field, typically in the callrec structure.
	 *
	 * @param pointer to field in callrec.
	 * @param content to copy.
	 * @param size of field.
	 */
	void setField(char *field, const char *str, unsigned len);

public:
	/**
	 * Compute the DTMF digit id of a character.
	 *
	 * @return dtmf digit or -1.
	 */
	int getDigit(char digit);

	/**
	 * See if trunk is idle and available.
	 *
	 * @return true if ready.
	 */
	bool isReady(void);

#ifdef	HAVE_TGI
	/**
	 * Perform a gateway TTS operation on behalf of the current
	 * trunk.
	 *
	 * @param filename or text string.
	 * @param mode argument.
	 */
	void libtts(const char *msg, ttsmode_t mode);
#endif

	/**
	 * Get group membership.
	 *
	 * @return member id.
	 */
	inline unsigned getMemberId(void)
		{return member;};

	/**
	 * Process a soft tone buffer.
	 *
	 * @return soft tone object.
	 */
	phTone *getTone(void);

	/**
	 * Fetch the current trunk mode flag.
	 *
	 * @return trunk call mode.
	 */
	inline trunkmode_t getTrunkMode(void)
		{return flags.trunk;};

	inline Trunk *getJoined(void)
		{return joined;};

	inline void setJoined(Trunk *trk)
		{joined = trk;};
};

/**
 * The system fifo is a class that both handles a fifo "control interface"
 * and that can process string commands as events through the Bayonne
 * driver.  The Fifo is used by tgi and may be used by other system
 * services.
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short Bayonne fifo command executive.
 */
class Fifo : public Mutex, public Script
{
protected:
	char schedule[33];

	Trunk *logResult(char **args);
	Trunk *assignRing(char **args);
	Trunk *assignDial(char **args);
	Trunk *sendEvent(char **args);
	Trunk *clearRing(char **args);
	Trunk *clearDial(char **args);
	Trunk *clearRDGroup(char **args);
	Trunk *exitPid(char **args);
	Trunk *waitPid(char **args);
	Trunk *setSymbol(char **argv);
	Trunk *putSymbol(char **argv);
	Trunk *addSymbol(char **argv);
	Trunk *setSize(char **argv);

	Trunk *login(char **argv);
	Trunk *startScript(char **argv);
	Trunk *testScript(char **argv);
	Trunk *ringScript(char **argv);
	Trunk *redirectScript(char **argv);
	Trunk *setSpan(char **argv);
	Trunk *setCard(char **argv);
	Trunk *busyLine(char **argv);
	Trunk *idleLine(char **argv);
	Trunk *hangupLine(char **argv);
	Trunk *reqScript(char **argv);
	Trunk *setSchedule(char **argv);
	Trunk *postKey(char **argv);
	Trunk *setMixer(char **argv);
	Trunk *setLimit(char **argv);
	Trunk *mapFiles(char **argv);
	Trunk *submit(char **argv);
	Trunk *reload(char **argv);
	Trunk *shellStart(char **argv);
	Trunk *stop(char **argv);
	Trunk *collectCmd(char **argv);
	Trunk *optionCmd(char **argv);
	Trunk *getSymbol(char **argv);
	Trunk *run(char **argv);
	Trunk *asr(char **argv);
	Trunk *text(char **argv);
	Trunk *notify(char **argv);

public:
	/**
	 * Issue a "command" request either from the fifo port itself
	 * or from a service thread.
	 *
	 * @return NULL if failed.
	 * @param request string as would be passed by fifo.
	 * @param fd optional output redirection.
	 */
	Trunk *command(const char *cmdstring, std::ostream *fd = NULL);

	Fifo() : Mutex(), Script() {};
};

#define	MAX_SPANS	64
#define	MAX_CARDS	64

/**
 * The driver class represents an abstract means of accessing the
 * internals of a Bayonne driver plug-in module.
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short Bayonne driver interface class.
 */
class Driver
{
protected:
	friend class TrunkGroup;
	friend class Trunk;
	friend class MappedDrivers;

	TrunkGroup **groups, *spans[MAX_SPANS], *cards[MAX_CARDS], *stacards[MAX_CARDS];
	bool active;
	unsigned portCount, downCount;
	unsigned extCount, trkCount, tieCount, numCount;
	Trunk **extIndex;
	volatile unsigned idleCount;

	unsigned tsid;
	int index;

	inline Driver *getFirst(void)
		{return drvFirst;};

public:
	friend Driver *getDriver(const char *name);
	static Driver *drvFirst;
	static int drvIndex;
	Driver *drvNext;

	enum {	capDaemon = 0x0001,
		capPSTN = 0x0002,
		capJoin = 0x00004,
		capSpans = 0x0008,
		capSpeed = 0x0010,	// driver can do rate adj
		capPitch = 0x0020,	// driver can pitch adj
		capGain = 0x0040,	// driver can gain adj
		capListen = 0x0080,	// driver can listen to asr
		capIP = 0x00160,	// driver is IP based
		capSwitch = 0x2000,
		capConference = 0x4000,
		capOffline = 0x8000};

	/**
	 * Second tick interval counter.
	 */
	virtual void secTick(void);

	/**
	 * Return driver capabilities.
	 */
	virtual unsigned getCaps(void)
		{return capDaemon | capPSTN;};

	/**
	 * Create an instance of the driver.
	 */
	Driver();

	/**
	 * Get the name of the driver.
	 */
	virtual char *getName(void)
		{return "Default";};

	/**
	 * Report if driver is idle.
	 */
	bool isIdle(void);

	/**
	 * Report if driver is down.
	 */
	bool isDown(void);	

	/**
	 * Start a driver; start service threads, build device nodes,
	 * etc.
	 * 
	 * @return number of active device ports.
	 */
	virtual int start(void) = 0;

	/**
	 * Shutdown the driver, service threads, etc.
	 */
	virtual void stop(void) = 0;

	/**
	 * Get a local copy of the status string.
	 *
	 * @param local status buffer.
	 */
	static void getStatus(char *buffer);

	/**
	 * Get total driver port capacity as a count.
	 *
	 * @return total port capacity count.
	 */
	static unsigned getCount(void);

	/**
	 * Load a script image.  Usually this is not driver specific,
	 * though it can be made to be so.
	 *
	 * @return ScriptImage base abstract.
	 */
	virtual aaImage *getImage(void);

	/**
	 * Get total number of port identifiers (timeslots) used by
	 * the current driver.
	 *
	 * @return trunk timeslots/ports allocated.
	 */
	virtual unsigned getTrunkCount(void) = 0;

	/**
	 * Get active number of ports actually deployed by the current
	 * driver.
	 *
	 * @return actual trunks used.
	 */
	virtual unsigned getTrunkUsed(void) 
		{return getTrunkCount();};

	/**
	 * Set the trunk group by span and card as well as port.
	 */
	void setTrunkGroup(int id, int card, int span);

	/**
	 * Get the trunk group of an identified port.
	 * 
	 * @return trunk group pointer.
	 * @param id
	 */
	TrunkGroup *getTrunkGroup(int id)
		{return groups[id];}; 

	TrunkGroup *getSpanGroup(int id)
		{return spans[id];};

	TrunkGroup *getCardGroup(int id)
		{return cards[id];};

	TrunkGroup *getStaGroup(int id)
		{return stacards[id];};

	/**
	 * Get the trunk group member trunk.
	 *
	 * @return trunk group port id.
	 * @param trunk group to locate.
	 * @param member id to locate.
	 */
	int getTrunkMember(TrunkGroup *grp, unsigned member);
	
	/**
	 * Get an individual port from the driver by timeslot/id.
	 *
	 * @return trunk port object.
	 */
	virtual Trunk *getTrunkPort(int id) = 0;

	enum {
		TRUNK_URI = 0x0001,
		TRUNK_EXTENSION = 0x0002,
		TRUNK_GROUP = 0x0004,
		TRUNK_TIE = 0x0008,
		TRUNK_LOCAL = 0x0032,
		TRUNK_START = 0x0160
	};

	static Trunk *getTrunk(const char *name, bool create = false,
		Driver *driver = NULL);
	static Driver *getIndexedDriver(int idx);

	/**
	 * Get an individual port from the driver by timeslot/id
	 * for the outbound (gives drivers which dynamically 
	 * allocate trunks the opportunity to create one).
	 *
	 * @return trunk port object.
	 */
	virtual Trunk *getOutboundTrunk(int id)
		{ return getTrunkPort(id); };

	/**
	 * Get a trunk by a id or global id string.
	 *
	 * @param trunk or port reference id.
	 * @return trunk port object.
	 */
	Trunk *getTrunkId(const char *id);

	/**
	 * Get a trunk by extension reference.
	 *
	 * @param extension number.
	 * @return trunk port object.
	 */
	Trunk *getExtNumber(const char *ext);

	/**
	 * Set a physical port to an extension number in the dialing plan.
	 *
	 * @param physical extension port.
	 * @param extension number to associate.
	 */
	bool setExtNumber(unsigned id, const char *ext);

	/**
	 * Set or assign a virtual extension number in the dialing plan.
	 *
	 * @param physical extension port tp associate with.
	 * @param virtual extension number.
	 */
	bool setExtVirtual(unsigned id, const char *ext);

	/**
	 * Clear an extension number entry.
	 *
	 * @param extension to clear.
	 */
	bool clrExtNumber(const char *ext);

	/**
	 * Get a trunk by trunk reference.
	 *
	 * @param trunk number
	 * @return trunk port object.
	 */
	virtual Trunk *getTrkNumber(const char *trk)
		{return getTrunkId(trk);};

	/**
	 * Get a trunk by tie trunk reference.
	 *
	 * @param tie trunk id
	 * @return trunk port object.
	 */
	virtual Trunk *getTieNumber(const char *tie)
		{return NULL;};

	/**
	 * Get the extension count.
	 *
	 * @return total extension count.
	 */
	unsigned getExtCount(void)
		{return extCount;};

	/**
	 * Get the total trunk count.	
	 *
	 * @return trunk count.
	 */
	unsigned getTrkCount(void)
		{return trkCount;};

	/**
	 * Get thr total tie line count.
	 *
	 * @return tie count.
	 */
	unsigned getTieCount(void)
		{return tieCount;};
	
	/**
	 * Get an individual conference resource.
	 *
	 * @return conference number.
	 */
	virtual Conference *getConference(int id)
		{return NULL;};

	/**
	 * Get an individual mixer resource by number.
	 *
	 * @return conference mixer.
	 */
	virtual Mixer *getMixer(int id)
		{return NULL;};

	/**
	 * Get the driver's chip count/conference unit capacity.
	 *
	 * @return conference mixer resources.
	 */
	virtual unsigned getMixers(void)
		{return 0;};

	/**
	 * Get the total conference groups available.
	 *
	 * @return conference groups.
	 */
	virtual unsigned getGroups(void)
		{return 0;};

	/**
	 * Span event operations.
	 *
	 * @return true if success.
	 * @param span to effect.
	 * @param event to pass.
	 */
	virtual bool spanEvent(unsigned span, TrunkEvent *event);

	/**
	 * card event operations.
	 *
	 * Card event operations.
	 *
	 * @return true if successful.
	 * @param card to effect.
	 * @param event to pass.
	 */
	virtual bool cardEvent(unsigned card, TrunkEvent *event)
		{return false;};

	int getDriverIndex(void)
		{return index;};
};

/**
 * DSO class for installing a monitoring plugin.
 *
 * @author Mark Lipscombe <markl@gasupnow.com>
 * @short Monitoring DSO interface.
 */
class Monitor : protected Mutex, protected Script
{
public:
	/**
	 * Register object.
	 */
	Monitor();

	/**
	 * Monitoring interface for event processing tracing
	 */
	virtual void monitorEvent(Trunk *trunk, TrunkEvent *event)
		{return;};

	/**
	 * Monitoring interface for state handlers
	 */
	virtual void monitorState(Trunk *trunk, char *state)
		{return;};

	/**
	 * Monitoring interface for script steps
	 */
	virtual void monitorStep(Trunk *trunk, Line *line)
		{monitorState(trunk, "step");};
};

/**
 * New DSO class for installing a "debugging"/regression test plugin.
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short Regression test/debug DSO interface.
 */
class Debug : protected Mutex, protected Script
{
public:
	/**
	 * Register DSO debug object.
	 */
	Debug();

	/**
	 * Stack trace support.
	 */
	static void stackTrace(int signo);

	/**
	 * Regression test interface called in server startup.
	 *
	 * @return true to force exit after test.
	 */
	virtual bool debugTest(void)
		{return false;};

	/**
	 * Interface for "final" handling of debug.
	 *
	 * @return true if ignore final exit.
	 */
	virtual bool debugFinal(int signo)
		{return false;};

	/**
	 * Debug interface for event processing "taps".
	 */
	virtual void debugEvent(Trunk *trunk, TrunkEvent *event)
		{return;};

	/**
	 * Debug interface for state handler entry "taps".
	 */
	virtual void debugState(Trunk *trunk, char *state)
		{return;};

	/**
	 * Debug service loop code "tap".
	 */
	virtual void debugService(Trunk *trunk, char *msg)
		{return;};

	/**
	 * Debug interface for "debug" script step.
	 */
	virtual void debugScript(Trunk *trunk, char *msg)
		{return;};

	/**
	 * Debug interface for script step "tap".
	 */
	virtual void debugStep(Trunk *trunk, Line *line)
		{debugState(trunk, "step");};

	/**
	 * Debug interface for fifo "debug" statement.
	 */
	virtual bool debugFifo(char **argv)
		{return true;};

	/**
	 * Debug interface for login/logout changes.
	 */
	virtual void debugLogin(Trunk *trunk)
		{return;};
};

/**
 * AudioService holds the logic for processing audio channels.
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short audio service processing.
 */
class AudioService
{
private: 
	char filename[256];

protected:
	Trunk *trunk;
	/**
	 * Compute and get a prompt file name.
	 *
	 * @param partial name.
	 * @return full path.
	 */
	char *getPrompt(char *name, const char *voice = NULL);

	/**
	 * Fetch the next prompt for the play list.
	 *
	 * @return get partial name.
	 */
	char *getPlayfile(void);

	/**
	 * Construct the object.
	 */
	AudioService(void);
};

/**
 * Services are threads used to support a trunk class, such as audio
 * services used for audio, etc.
 *
 * @author David Sugar <dyfet@ostel.com>
 * @short service thread support.
 */
class Service : public Semaphore, public Thread, public AudioService
{
protected:
	volatile bool stopped;
	trunkdata_t *data;
	TrunkGroup *group;

	/**
	 * Signal "successful" completion result.
	 */
	void success(void);

	/**
	 * Signal "failure" completion result.
	 */
	void failure(void);

	/**
	 * Mark required dsp reset.
	 */
	inline void dspReset(void)
		{trunk->flags.reset = true;};

	/**
	 * Set audio marker.
	 */
	inline void setAudio(void)
		{trunk->flags.audio = true;};

	/**
	 * Clear audio marker.
	 */
	inline void clrAudio(void)
		{trunk->flags.audio = false;};

public:
	/**
	 * Create a service thread on an existing trunk object.
	 *
	 * @param trunk object to use.
	 * @param process priority.
	 */
	Service(Trunk *trunk, int pri);

	/**
	 * request to stop a service and obtain default "delay" time
	 * to use for reset interval.
	 *
	 * @return delay in milliseconds.
	 */
	virtual timeout_t stop(void);

	/**
	 * Indicate if the service thread is exiting and/or can be
	 * deleted now.  If false, then it will be cleaned by the
	 * scheduler thread.
	 *
	 * @return false if held to scheduler.
	 */
	virtual bool isExiting(void)
		{return true;};

	/**
	 * Invoke termination.
	 */
	void endService(void)
		{terminate();};

	/**
	 * Termination of service.
	 */
	virtual ~Service()
		{terminate();}
};

/**
 * Server classes are used for threaded entities such as network
 * management interfaces, which may be started and stopped under
 * server control.  Sometimes server is mixed into other dso
 * classes to stabilize start/stop of service threads.
 *
 * @short threaded server service.
 * @author David Sugar <dyfet@ostel.com>
 */
class Server : public Thread
{
private:
	static Server *first;
	Server *next;
	friend void startServers(void);
	friend void stopServers(void);

protected:
	Server(int pri);

	/**
	 * Used for stopServers call interface.
	 */
	virtual void stop(void)
		{terminate();};
};

/**
 * The Sync class is used to create dso objects which have entites
 * that are repetitivly called through the scheduler thread.  These
 * are sometimes used to update disk based databases from memory, or
 * perform other interval timed operations.
 */
class Sync
{
private:
	static Sync *first;
	Sync *next;
	time_t runtime;

protected:
	/**
	 * Abstract class, protected constructor.
	 */
	Sync(void);

	/**
	 * Return if ready for update.  If not, the current update
	 * interval may be skipped entirely.
	 */
	virtual bool isScheduled(void)
		{return true;};

	/**
	 * Return execution interval of this sync object.  How many
	 * xx minutes before attempting a new sync.
	 */
	virtual unsigned getInterval(void)
		{return 10;};

	/**
	 * Operation to perform when scheduled.
	 */
	virtual void schedule(void) = 0;

	/**
	 * Return name used in slog event when scheduling item.
	 */
	virtual char *getSyncName(void) = 0;

public:
	static void check(void);
};

/**
 * The tone class is used to build sampled single and dual frequency
 * tones that may be fed to the telephony device.  Tones are defined
 * in the "tones" section of bayonne.conf.
 *
 * @short generated sample tone.
 * @author David Sugar.
 */
class phTone
{
private:
	friend class KeyTones;
	friend phTone *getphTone(const char *name);
	static phTone *first;
	static int ulaw[256];
	static unsigned char alaw[256];
	static unsigned char a2u[128];
	phTone *next;

protected:
	char name[33];
	unsigned char *samples;
	timeout_t duration, playtime;
	unsigned freq1, freq2;

public:
	static unsigned char linear2ulaw(int sample);
	static inline unsigned char linear2alaw(int sample)
		{return alaw[linear2ulaw(sample)];};

	static unsigned char alaw2ulaw(unsigned char al);

	static inline unsigned char ulaw2alaw(unsigned char ul)
		{return alaw[ul];};

	static short ulaw2linear(unsigned char ul);
	static short alaw2linear(unsigned char al);

	/**
	 * Create single frequency tone.
	 *
	 * @param name of tone.
	 * @param duration in milliseconds.
	 * @param frequency of tone.
	 */
	phTone(const char *name, timeout_t duration, unsigned f);

	/**
	 * Create dual frequency tone.
	 *
	 * @param name of tone.
	 * @param duration in milliseconds.
	 * @param first frequency.
	 * @param second frequency.
	 */
	phTone(const char *name, timeout_t duration, unsigned f1, unsigned f2);

	~phTone();

	/**
	 * If the telephony card is capable of generating it's own
	 * tones, then it can use the clear method to remove any
	 * allocated memory for sampled data.
	 */
	void clear(void);

	/**
	 * Fetch the sample area of the tone.
	 *
	 * @return sample area.
	 */
	inline unsigned char *getSamples(void)
		{return samples;};

	/**
	 * Get the duration of the tone.
	 *
	 * @return duration of tone.
	 */
	inline timeout_t getDuration(void)
		{return duration;};

        /**
         * Get the duration of the tone.
         *
         * @return playtime of tone.
         */
        inline timeout_t getPlaytime(void)
                {return playtime;};
};

/**
 * This class is used for interfacing to DSO loaded TGI interpreters.
 *
 * @short TGI interpreter module.
 * @author David Sugar <dyfet@ostel.com>
 */
class TGI
{
private:
	static TGI *first;
	TGI *next;

protected:
	TGI();

	/**
	 * Check a file extension to see if it belongs to a first
	 * stage tgi script interpreter (avoids second fork).
	 *
	 * @return true if claimed.
	 * @param extension.
	 */
	virtual bool getExtension(const char *ext)
		{return false;};

public:
	/**
	 * Check command for interpreter and, if is, execute it.  If
	 * so it doesn't return but uses "exit".  This is a "second"
	 * stage tgi mod interface and is effective for single session
	 * interpreter libraries.
	 *
	 * @param script name resolved.
	 * @param argument list.
	 */
	virtual void script(char *cmd, char **args)
		{return;};

	/**
	 * Execute a first stage interpreter, uses local functions.
	 *
	 * @return shell exit code.
	 * @param fifo file descriptor.
	 * @param port number.
	 * @param unparsed command string.
	 */
	virtual int parse(int fd, char port[64], char *cmd)
		{return -1;};	

	friend void getInterp(char *cmd, char **args);
	friend TGI *getInterp(char *cmd);
};

/**
 * The tts syth classes intercept core functions as needed to create
 * an interface to a native text to speech subsystem, usually by
 * manipulating data.play.
 */
class TTS
{
protected:
	friend class Plugins;

	TTS();

public:
	virtual const char *getName(void) = 0;

	virtual const char *getVoices(void)
		{return "none";};

	virtual bool synth(unsigned id, trunkdata_t *data) = 0;
};

/**
 * Modules are used for protocol modules and detached threads used to
 * service key Bayonne protocols and interfaces.  These are done as
 * loadable modules and may be replaced with application specific
 * interfaces.
 *
 * @short Module interface class.
 * @author David Sugar <dyfet@ostel.com>
 */
class Module : public Script
{
private:
	friend class Fifo;
	friend class AudioService;
	friend class TrunkGroup;
	friend class Network;
	friend class aaImage;
	friend class aaScript;
	friend class Trunk;

	Method prior;
	static Module *modFirst, *sesFirst, *cmdFirst, *urlFirst, *reqFirst, *netFirst, *symFirst;
	Module *modNext, *sesNext, *cmdNext, *urlNext, *reqNext, *netNext, *symNext;

protected:
	static Module *modImport;

	Module();

	inline Module *getFirst(void)
		{return modFirst;};

	inline bool executePrior(Trunk *trunk)
		{return trunk->execute(prior);};

	/**
	 * Preprocessor import directives...
	 *
	 * @param token
	 */
	virtual void import(const char *token)
		{return;};

	/**
	 * Get alternate script extensions.
	 *
	 * @return extension if used.
	 */
	virtual const char *getExtension(void)
		{return NULL;};

	/**
	 * Get a global symbol space for this module.
	 *
	 * @return global symbol space if used.
	 */
	virtual Script::Symbol *getSymbol(Trunk *trk, const char  *map, unsigned size)
		{return NULL;};

	/**
	 * Get the "type" identifer of this module.
	 *
	 * @return type id.
	 */
	virtual modtype_t getType(void) = 0;

	/**
	 * Add a symbol space for this module.
	 */
	void addSymbols(void);

	/**
	 * Add as a session handling module.
	 */
	void addSession(void);

	/**
	 * Add fifo command handling module.
	 */
	void addCommand(void);

	/**
	 * Add module as url processing.
	 */
	void addPrompts(void);

	/**
	 * Add module for net processing.
	 */
	void addNetwork(void);

	/**
	 * Add request handler back-end.
	 */
	void addRequest(void);

	/**
	 * Set service thread in trunk object.
	 *
	 * @param trunk
	 * @param service object
	 */
	void setThread(Trunk *trunk, Service *svc);
	
	/**
	 * Parse a url/prompt file into a fixed file path.
	 */
	virtual char *getPrompt(const char *original)
		{return NULL;};

	/**
	 * Notify an expired request.
	 */
	virtual void expires(Request *request)
		{return;};

	/**
	 * Notify a running request.
	 */
	virtual void running(Request *request)
		{return;};

	/**
	 * Notify a cancelled request.
	 */
	virtual void cancelled(Request *request)
		{return;};

	/**
	 * Notify when a module network packet has been received and
	 * offer reply possibility.
	 *
	 * @return != 0 to claim packet, >0 if reply.
	 * @param packet buffer.
	 * @param packet length.
	 * @param node entity.
	 */
	virtual int accept(char *buffer, socklen_t len, statnode_t *node)
		{return 0;};

	/**
	 * Notify when a heartbeat dies...
	 *
	 * @param node that heartbeat died on.
	 */
	virtual void failover(statnode_t *node)
		{return;};

	/**
	 * Network synchronization to broadcast messages from a module thread.
	 */
	virtual void broadcast(void)
		{return;};

	friend void broadcast(char *msgbuf, unsigned msglen);
	friend void cancel(TrunkGroup *grp, const char *tag);
public:
	inline Module *getNext(void)
		{return modNext;};

	/**
	 * Get the "name" identifer of this module.
	 *
	 * @return name string.
	 */
	virtual char *getName(void) = 0;

	/**
	 * Execute the script request.  Usually this involves creating
	 * a detached service thread.
	 *
	 * @return error message or NULL.
	 * @param trunk object to reference.
	 */
	virtual char *dispatch(Trunk *trunk)
		{return NULL;};

	/**
	 * Fetch the data pointer of a trunk.
	 */
	inline trunkdata_t *getData(Trunk *trunk)
		{return &trunk->data;};

	/**
	 * Post execute commit for modules that schedule delay.
	 *
	 * @param trunk object to reference.
	 */
	virtual void commit(Trunk *trunk)
		{return;};

	/**
	 * This is used to determine if a timeout/sleep should occur
	 * or if script advance is immediate.
	 *
	 * @return number of seconds in sleep state.
	 * @param trunk object to reference.
	 */
	virtual unsigned sleep(Trunk *trunk)
		{return 0;};

	/**
	 * Notify for detach.  As each call is detached, this will
	 * get called.
	 *
	 * @param trunk to detach from.
	 */
	virtual void detach(Trunk *trunk)
		{return;};

	/**
	 * Notify for attach.  This can be used to set constants in
	 * the trunks symbol space for this module's use at call
	 * setup.
	 *
	 * @param trunk to attach to.
	 */
	virtual void attach(Trunk *trunk)
		{return;};

	/**
	 * reload provisioning interface for fifos.
	 */
	virtual void reload(void)
		{return;};

	/**
	 * fifo command extension.
	 */
	virtual bool command(char **argv, std::ostream *out)
		{return false;};

	friend Module *getModule(modtype_t mod, const char *name = NULL);
	friend void detachModules(Trunk *trunk);
	friend void attachModules(Trunk *trunk);
};

/**
 * Sessions are used for "garbage collected" entities which may be
 * detached and removed after an expiration time rather than falling
 * out of scope immediately.  This process is managed by the scheduler
 * thread.
 *
 * @short Garbage collectable objects.
 * @author David Sugar <dyfet@ostel.com>
 */
class Session
{
private:
	static Mutex mutex;
	static Session *first;
	static Session *last;
	Session *next, *prev;

public:
	static void clean(void);

protected:
	Session();
	virtual ~Session()
		{unlink();};

	/**
	 * Unlink the session object.
	 */
	void unlink(void);

	/**
	 * Indicate if and when this session is to be "garbage collected"
	 * by UNIX timestamp.
	 *
	 * @return 0 if still running, else garbage collection period.
	 */
	virtual time_t getExpires(void) = 0;
};

/**
 * Somewhat generic queue processing class to establish a producer
 * consumer queue.  This may be used to buffer cdr records, or for
 * other purposes where an in-memory queue is needed for rapid
 * posting.  This class is derived from Mutex and maintains a linked
 * list.  A thread is used to dequeue data and pass it to a callback
 * method that is used in place of "run" for each item present on the
 * queue.  The conditional is used to signal the run thread when new
 * data is posted.
 *
 * @short in memory data queue interface.
 * @author David Sugar <dyfet@ostel.com>
 */
class DataQueue : public Mutex, public Thread, public Semaphore
{
private:
	typedef struct _data
	{
		struct _data *next;
		unsigned len;
		char data[0];
	}	data_t;

	bool started;

	data_t *first, *last;		// head/tail of list

	void run(void);			// private run method

protected:
	const char *name;		// used for save/restore file

	/**
	 * Start of dequeing.  Maybe we need to connect a database
	 * or something, so we have a virtual...
	 */
	virtual void startQueue(void)
		{return;};

	/**
	 * End of dequeing, we expect the queue is empty for now.  Maybe
	 * we need to disconnect a database or something, so we have
	 * another virtual.
	 */
	virtual void stopQueue(void)
		{return;};

	/**
	 * Virtual callback method to handle processing of a queued
	 * data items.  After the item is processed, it is deleted from
	 * memory.  We can call multiple instances of runQueue in order
	 * if multiple items are waiting.
	 *
	 * @param data item being dequed. 
	 */
	virtual void runQueue(void *data) = 0;

public:
	/**
	 * Create instance of our queue and give it a process priority.
	 *
	 * @param process priority.
	 */
	DataQueue(const char *id, int pri);

	/**
	 * Destroy the queue.
	 */
	~DataQueue();

	/**
	 * Put some unspecified data into this queue.  A new qd
	 * structure is created and sized to contain a copy of 
	 * the actual content.
	 *
	 * @param pointer to data.
	 * @param size of data.
	 */	
	void postData(const void *data, unsigned len);
};

/**
 * Protocols are used for processing transactional requests such as
 * those performed thru modules, but thru a TCP protocol.  These support
 * the resolver thread.
 *
 * @short resolved network protocol session interface.
 * @author David Sugar <dyfet@ostel.com>
 */
class Protocol : public Keydata, public InetHostAddress, public ThreadLock
{
private:
	friend class Resolver;
	static Protocol *first;
	Protocol *next;
	Semaphore *sessions;
	tpport_t port;

	void update(InetHostAddress addr);
public:
	Protocol(const char *keypath, tpport_t port);
	~Protocol();

	/**
	 * Get inet host address for this service.
	 *
	 * @return inet host address of server.
	 */
	InetHostAddress getAddress(void);

	/**
	 * Get inet port address of this service.
	 *
	 * @return inet port address of service.
	 */
	inline tpport_t getPort(void)
		{return port;};

	/**
	 * Get client connection limiting semaphore that is used as the
	 * starting semaphore for the client connection thread.
	 *
	 * @return semaphore session limiter.
	 */
	inline Semaphore *getSessions(void)
		{return sessions;};
};

const char *getState(void);
void setReply(const char *sym, const char *value);
void errlog(const char *level, const char *fmt, ...);
void control(const char *ctrlstring);
void loader(const char *path, const char *ext);
statnode_t *getNodes(const char *name);
bool isFHS(void);
bool hasTTS(void);
bool permitAudioAccess(const char *path, bool write);
bool permitFileAccess(const char *path);

extern unsigned numbering;
extern bool aliases;
extern bool running;
extern Keydata application;
extern KeyServer keyserver;
extern KeyThreads keythreads;
extern KeyMemory keymemory;
extern KeyPaths keypaths;
extern KeyImports keyimports;
extern KeyVoices keyvoices;
extern KeyLocal keylocal;
extern KeyNetwork keynetwork;
extern KeyHandlers keyhandlers;
extern Plugins modules;
//extern Driver *driver;
extern TTS *tts;
extern Debug *debug;
extern Monitor *monitor;
extern Fifo fifo;
extern ThreadLock cachelock;
extern aaScript *aascript;

#ifdef	CCXX_NAMESPACES
};
#endif

#endif
