// Copyright (C) 2000-2001 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "server.h"
#include <cc++/strchar.h>

#ifdef	HAVE_SSTREAM
#include <sstream>
#else
#include <strstream>
#endif

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

char Trunk::digit[16] = {
	'0', '1', '2', '3',
	'4', '5', '6', '7',
	'8', '9', '*', '#',
	'a', 'b', 'c', 'd'};

callrec_t *Trunk::callrec = NULL;

Trunk::Trunk(int port, Driver *drv, int card, int dspan) :
ScriptInterp()
{
	int i;

	driver = drv;
	ctx = this;

	driver->setTrunkGroup(port, card, dspan);
	group = driver->getTrunkGroup(port);
	group->enterMutex();
	group->incCapacity();
	member = ++group->members;
	group->leaveMutex();

	++driver->portCount;

	for(i = 0; i < 10; ++i)
		dialgroup[i] = false;

	flags.listen = flags.bgm = flags.dtmf = flags.offhook = flags.reset = false;
	flags.trunk = TRUNK_MODE_INACTIVE;
	flags.dsp = DSP_MODE_INACTIVE;
	flags.script = false;
	flags.ready = false;
	flags.onexit = false;	
	flags.dnd = false;
	id = port;
	tsid = port + drv->tsid;
	span = dspan;
	rings = 0;
	starttime = idletime = synctimer = exittimer = 0;
	thread = NULL;
	digits = 0;
	apppath[0] = 0;
	extNumber[0] = 0;
	ringIndex = NULL;
	isStation = isRinging = false;

	softJoin = NULL;
	joined = NULL;

	seq = 0;
	tonetmp = NULL;
	tgi.pid = 0;
	tgi.fd = -1;
	tgi.dtmf = true;
	rpc.fd = -1;
	asr.pid = 0;
	asr.fd = -1;
	asr.rate = 16000;

	dtmf.bin.id = SYM_DIGITS;
	dtmf.bin.type = symNORMAL;
	dtmf.bin.size = MAX_DIGITS;
	dtmf.bin.next = NULL;

	dtmf.bin.data[0] = 0;
	setCalls("init");
}

const char *Trunk::getEncodingName(Audio::Encoding e)
{
	switch(e)
	{
	case Audio::g721ADPCM:
		return "g721";
	case Audio::gsmVoice:
		return "gsm";
	case Audio::mulawAudio:
		return "mulaw";
	case Audio::alawAudio:
		return "alaw";
	case Audio::okiADPCM:
	case Audio::voxADPCM:
		return "vox";
	case Audio::pcm8Mono:
		return "pcm8";
	case Audio::pcm16Mono:
		return "pcm";
	}
	return "unsupported";
}

bool Trunk::isAdmin(void)
{
	const char *cp = getSymbol(SYM_LOGIN);
	if(!cp)
		return false;

	if(!stricmp(cp, "admin"))
		return true;

	return false;
}

void Trunk::dialRewrite(const char *dialstring)
{
	char rulebuf[65];
	char id[16];
	unsigned len = 0;
	char *cp;
	const char *rule = NULL;
	const char *prefix = "";
	const char *suffix = "";

	while(isdigit(dialstring[len]) && len < 15)
		++len;
	
	while(len && !rule)
	{
		memcpy(id, dialstring, len);
		id[len--] = 0;
		rule = group->getLast(id);
	}

	if(rule)
	{
		snprintf(rulebuf, sizeof(rulebuf), "%s", rule);
		dialstring += atoi(rule);	// prefix digits to strip
		rule = strchr(rulebuf, ',');
		if(rule)
		{
			prefix = ++rule;
			cp = strchr(rule, ',');
			if(cp)
			{
				*(cp++) = 0;
				suffix = cp;
			 }
		}
	}	
	snprintf(data.dialxfer.digits, sizeof(data.dialxfer.digits), 
		"%s%s%s", prefix, dialstring, suffix);
	data.dialxfer.digit = data.dialxfer.digits;
}	

const char *Trunk::getStation(void)
{
	const char *kw = getKeyword("station");
	if(!kw)
		kw = group->getLast("station");

	return kw;
}

void Trunk::setIdle(bool mode)
{
	if(mode)
		++driver->idleCount;
	else
		--driver->idleCount;
}

phTone *Trunk::getTone(void)
{
	if(data.tone.tone)
		return data.tone.tone;

	if(tonetmp)
		delete tonetmp;

	if(!data.tone.freq2)
		tonetmp = new phTone(NULL, data.tone.duration, data.tone.freq1);
	else
		tonetmp = new phTone(NULL, data.tone.duration, data.tone.freq1, data.tone.freq2);

	data.tone.tone = tonetmp;
	return tonetmp;
}

bool Trunk::writeRPC(const char *msgtype, const char *msg)
{
	char buffer[82];
	unsigned len;
	
	if(rpc.fd < 0)
		return false;

	snprintf(buffer, sizeof(buffer), "%s %s %s\n", msgtype, rpc.seq, msg);	
	len = strlen(buffer);
	if(::write(rpc.fd, buffer, len) != len)
	{
		close(rpc.fd);
		rpc.fd = -1;
		return false;
	}

	if(!stricmp(msgtype, "exit"))
	{
		::close(rpc.fd);
		rpc.fd = -1;
	}

	return true;
}

bool Trunk::writeShell(const char *text)
{
	char fifo[33];
	unsigned len = strlen(text);

	if(tgi.fd < 0)
		return false;

	if(::write(tgi.fd, text, len) == len)
		return true;

	if(tgi.pid > 0)
		kill(tgi.pid, SIGPIPE);
	::close(tgi.fd);
	tgi.fd = -1;
	return false;
}

void Trunk::enterState(const char *state)
{
	char fifo[32];
	unsigned len;

	if(tgi.fd > -1)
	{
		snprintf(fifo, sizeof(fifo), "-%s\n", state);
		writeShell(fifo);
	}

	debug->debugState(this, (char *)state);
	monitor->monitorState(this, (char *)state);
}	

int Trunk::getDigit(char dig)
{
	int i;

	dig = tolower(dig);
	for(i = 0; i < 16; ++i)
	{
		if(digit[i] == dig)
			return i;
	}
	return -1;
}

#ifdef	HAVE_TGI
void Trunk::libtts(const char *msg, ttsmode_t mode)
{
	tgicmd_t cmd;
#ifdef	HAVE_SSTREAM
	ostringstream str;
	str.str() = "";
#else
	strstream str(cmd.cmd, sizeof(cmd.cmd));
#endif

	cmd.seq = ++tgi.seq;
	snprintf(cmd.port, sizeof(cmd.port), "%s/%d", driver->getName(), id);
	cmd.mode = TGI_EXEC_AUDIO;
	cmd.cmd[0] = 0;

	str << keyhandlers.getLast("say") << ".tts";
	str << " language=" << getSymbol(SYM_LANGUAGE);
	str << " audio=" << "temp/.tts." << id << ".ul";
	str << " voice=" << getSymbol(SYM_VOICE);
	switch(mode)
	{
	case TTS_GATEWAY_TEXT:
		str << " phrase=" << msg;
		break;
	case TTS_GATEWAY_FILE:
		str << " source=" << msg;
		break;
	}
#ifdef	HAVE_SSTREAM
	snprintf(cmd.cmd, sizeof(cmd.cmd), "%s", str.str().c_str());
#else
	str << ends;
#endif
	::write(tgipipe[1], &cmd, sizeof(cmd));
}

#endif

bool Trunk::isReady(void)
{
	trunkmode_t trk = flags.trunk;
	if(trk != TRUNK_MODE_INACTIVE)
		return false;

	return flags.ready;
}

void Trunk::setDTMFDetect(void)
{
	Line *line = getLine();

	if(tgi.fd > -1)
	{
		setDTMFDetect(tgi.dtmf);
		return;
	}

	if(line)
	{
		setDTMFDetect(true);
		switch(flags.digits)
		{
		case DTMF_MODE_LINE:
			if(line->scr.method == (Method)&Trunk::scrCollect)
				setDTMFDetect(true);
			else
				setDTMFDetect(getMask() & 0x00000008);
			break;
		case DTMF_MODE_SCRIPT:
			if(line->scr.method == (Method)&Trunk::scrCollect)
				setDTMFDetect(true);
			else
				setDTMFDetect(getLine()->mask & 0x8);
			break;
		case DTMF_MODE_ON:
			setDTMFDetect(true);
			break;
		case DTMF_MODE_OFF:
			setDTMFDetect(false);
		}
	}
	else
		setDTMFDetect(false);
}

bool Trunk::asrPartial(const char *text)
{
	char evt[65];
	char *cp;

	snprintf(evt, sizeof(evt), "asr:%s", text);
	cp = strrchr(evt, ' ');
	if(cp)
		*cp = 0;

	if(trunkEvent(evt))
		return true;

	text = strrchr(text, ',');
	if(!text)
		return false;

	snprintf(evt, sizeof(evt), "asr:%s", ++text);
	cp = strrchr(evt, ' ');
	if(cp)
		*cp = 0;
	return trunkEvent(evt);
}	

void Trunk::setCalls(const char *mode)
{
	struct tm *dt, trec;
	time_t now;
	char buf[11];
	char *cp;

	if(!callrec)
		return;

	if(!mode)
		mode = "none";

	time(&now);
	dt = localtime_r(&now, &trec);
	snprintf(buf, 6, "%02d:%02d", dt->tm_hour, dt->tm_min);
	memcpy(callrec[tsid].cr_time, buf, 5);
	setField(callrec[tsid].cr_type, mode, sizeof(callrec_t) - 7);
	callrec[tsid].cr_nl = '\n';
}

void Trunk::setField(char *field, const char *str, unsigned len)
{
	if(!callrec)
		return;

	if(!str)
		str = "";

	while(--len)
	{
		if(*str)
			*(field++) = tolower(*(str++));
		else
			*(field++) = ' ';
	}
} 

bool Trunk::syncParent(const char *msg)
{
	const char *p = getSymbol(SYM_PARENT);
	const char *g = getSymbol(SYM_GID);
	Trunk *trunk;
	TrunkEvent event;

	if(!p)
		return false;

	if(!*p)
		return false;

	trunk = Driver::getTrunk(p);
	if(!trunk)
		return false;

	if(g)
		g = strchr(g, '-');
	else
		g = "none";

	event.id = TRUNK_SYNC_PARENT;
	event.parm.sync.msg = msg;
	event.parm.sync.id = g;
	return trunk->postEvent(&event);
}

bool Trunk::recvEvent(TrunkEvent *event)
{
	char evt[65];
	Trunk *trk = event->parm.send.src;
	const char *gid = getSymbol(SYM_GID);
	if(!gid)
		return false;

	if(!trk)
	{
		snprintf(evt, sizeof(evt), "control:%s", event->parm.send.msg);
		setSymbol(SYM_EVENTID, "server-control");
		setSymbol(SYM_EVENTMSG, event->parm.send.msg);
		if(trunkEvent(evt))
			return true;
		return trunkSignal(TRUNK_SIGNAL_EVENT);
	}

	snprintf(evt, sizeof(evt), "event:%s", event->parm.send.msg);

	trk->enterMutex();
	if(trk->seq != event->parm.send.seq)
	{
		trk->leaveMutex();
		return false;
	}

	gid = trk->getSymbol(SYM_GID);
	if(!gid)
	{
		trk->leaveMutex();
		return false;
	}
	setSymbol(SYM_EVENTID, gid);
	setSymbol(SYM_EVENTMSG, event->parm.send.msg);

	trk->leaveMutex();
	if(trunkEvent(evt))
		return true;
	return trunkSignal(TRUNK_SIGNAL_EVENT);
}

char **Trunk::getInitial(char **args)
{
	char namebuf[sizeof(buffer)];
	char *name;
	args[1] = NULL;

	name = (char *)group->getSchedule(buffer);

	if(!name)
	{
		args[0] = "down::service";
		return args;
	}

	if(strstr(name, "::"))
	{
		args[0] = name;
		return args;
	}

	if(service[0])
	{
		args[0] = service;
		return args;
	}


	if(upflag && !strnicmp(name, "http:", 5))
	{
		snprintf(namebuf, sizeof(namebuf), "href=%s", name);
		strcpy(buffer, namebuf);
		args[0] = "up::load";
		args[1] = buffer;
		args[2] = NULL;
		return args;
	}
	else if(upflag)
	{
		snprintf(namebuf, sizeof(namebuf), "up::%s", name);
		strcpy(buffer, namebuf);
		name = buffer;
	}

	args[0] = name;
	args[1] = NULL;
	return args;
}

void Trunk::setList(char **list)
{
	char buffer[256];
	char *tok, *key, *value;

	while(*list)
	{
		strcpy(buffer, *list);
		key = strtok_r(buffer, "=", &tok);
		value = strtok_r(NULL, "=", &tok);
		if(key && value)
		{
			if(!strnicmp(key, "server.", 7))
				key = NULL;
			else if(!stricmp(key, SYM_LOGIN))
				key = NULL;
			else if(!strnicmp(key, "driver.", 7))
				key = NULL;
			else if(!strnicmp(key, "user.", 5))
				key = NULL;  
			else if(!strnicmp(key, "line.", 5))
				key = NULL;
			else if(!strnicmp(key, "sql.", 4))
				key = NULL;
			else if(!strnicmp(key, "global.", 7))
				key = NULL;
		}
		if(key && value) 
			setConst(key, urlDecode(value));
		++list;
	}
}

void Trunk::commit(Symbol *sym)
{
	const char *gid;

	if(sym == &dtmf.bin)
		digits = strlen(dtmf.bin.data);
	else
		commit(sym);
}

void Trunk::repSymbol(const char *id, const char *data)
{
	if(data)
		setSymbol(id, data);
}

const char *Trunk::getExternal(const char *id)
{
	ScriptImage *img = getImage();
	time_t now;
	struct tm *dt, dtr;

	const char *p = strchr(id, '.');
	char *tmp;

	if(!p || ((p - id) < 6))
		return NULL;

	if(!strnicmp("session.", id, 8)) {
		id += 8;
		// XXX fixme
		if(!stricmp(id, "gid") || !stricmp(id, "id") || !stricmp(id, "sid"))
		{
			tmp = getTemp();
			snprintf(tmp, 16, "%d", tsid);
			return tmp;
		}
		else if(!stricmp(id, "index"))
		{
			tmp = getTemp();
			snprintf(tmp, 10, "%d", tsid);
			return tmp;
		}
		else if(!stricmp(id, "timestamp"))
		{
			tmp = getTemp();
			time(&now);
			snprintf(tmp, 16, "%ld", now);
			return tmp;
		}
		else if(!stricmp(id, "uid"))
		{
			tmp = getTemp();
			time(&now);
			snprintf(tmp, 16, "%08lx-%04x", now, tsid);
			return tmp;
		}
		else if(!stricmp(id, "mid"))
		{
			tmp = getTemp();
			time(&now);
			snprintf(tmp, 24, "%08lx-%04x.%s", now, tsid, img->getLast("extension"));
			return tmp;
		}
		else if(!stricmp(id, "libext"))
			return img->getLast("extension");
		else if(!stricmp(id, "status"))
			return "active";
		else if(!stricmp(id, "line"))
		{
			if(flags.offhook)
				return "offhook";
			else
				return "idle";
		}
		else if(!stricmp(id, "timeslot"))
		{
			tmp = getTemp();
			snprintf(tmp, 16, "%d", tsid);
			return tmp;
		}
		else if(!stricmp(id, "servertype"))
			return "sa";
		else if(!stricmp(id, "deviceid"))
		{
			tmp = getTemp();
			getName(tmp);
			return tmp;
		}
		else if(!stricmp(id, "driverid"))
			return driver->getName();
		else if(!stricmp(id, "date"))
		{
			tmp = getTemp();
			time(&now);
			dt = localtime_r(&now, &dtr);

			if(dt->tm_year < 1900)
				dt->tm_year += 1900;

			snprintf(tmp, 12, "%04d-%02d-%02d",
				dt->tm_year, dt->tm_mon + 1, dt->tm_mday);
			return tmp;
		}
		else if(!stricmp(id, "time"))
		{
			tmp = getTemp();
			time(&now);
			dt = localtime_r(&now, &dtr);

			snprintf(tmp, 12, "%02d:%02d:%02d",
				dt->tm_hour, dt->tm_min, dt->tm_sec);
			return tmp;
		}
		else if(!stricmp(id, "duration"))
		{
			if(!starttime)
				return "0:00:00";

			time(&now);
			now -= starttime;

			tmp = getTemp();
			snprintf(tmp, 12, "%ld:%02ld:%02ld",
				now / 3600, (now / 60) % 60, now %60);
			return tmp;
		}
		else if(!stricmp(id, "rings"))
		{
			tmp = getTemp();
			snprintf(tmp, 4, "%d", rings);
			return tmp;
		}
		else if(!stricmp(id, "digits"))
		{
			tmp = getTemp();
			snprintf(tmp, 10, "%d", digits);
			return tmp;
		}
		return NULL;
	}

	if(!strnicmp("script.", id, 7))
		return ScriptInterp::getExternal(id);

	if(!strnicmp("server.", id, 7))
		return ScriptInterp::getExternal(id);
	
	return NULL;
}

bool Trunk::attach(const char *name)
{
	char buf[65];
	class Module *mod;
	time_t now;
	struct tm *dt;
	struct tm tbuf;
	char buffer[33];
	char *index[128];
	char **pol = index;
	const char *xml;
	const char *pid;
	trunkmode_t trk = flags.trunk;
	Trunk *parent = NULL;
	TrunkEvent event;
	const char *args[2];
	char *e;
	const char *cp;
	const char *login;
	Symbol *sym;
	

	// This concerns me.  Why are we returning false?
	// What, in fact, is this case?

	// Since we just set TRUNK_MODE_OUTGOING, in our parent
	// stack frame, I'll kill the flags.script case if we
	// are outbound scheduled.

	//Matt B

	if(!running)
		return false;

	exitmsg[0] = 0;
	cdrv = NULL;
	getName(buffer);
//	slog(Slog::levelDebug) << buffer << ": low water mark at attach; " << getPages() << endl;


	if(trk != TRUNK_MODE_OUTGOING) 
	{
	    if(flags.script)
	        return false;
	}

	flags.reset = flags.dtmf = flags.offhook = flags.once = flags.onexit = false;
	asr.rate = 16000;
	asr.pid = 0;
	asr.fd = -1;
	rpc.fd = -1;
	tgi.pid = 0;
	tgi.fd = -1;
	tgi.dtmf = true;
	flags.digits = DTMF_MODE_LINE;

	//autoloop(true);	// default to loop

	pid = getSymbol(SYM_PARENT);
	if(pid)
		parent = Driver::getTrunk(pid, driver);

	if(!ScriptInterp::attach(aascript, name))
	{
		errlog("missing", "Script=%s", name);
		if(parent)
		{
			event.id = TRUNK_CHILD_FAIL;
			parent->postEvent(&event);
		}
		return false;
	}

	ScriptImage *img = getImage();

	flags.script = true;

	setSymbol(SYM_FREE, df_free, 3);
	setSymbol(SYM_USED, df_used, 3);

	setSymbol(SYM_HOME, "");
	setSymbol(SYM_ERROR, "none");

	setSymbol(SYM_EXTENSION, img->getLast("extension"), 8);

	cp = group->getLast("dialplan");
	if(!cp)
		cp = keyserver.getLast("dialplan");

	setSymbol(SYM_DIALING, cp, 3);

	setSymbol(SYM_FORMAT, getDefaultEncoding(), 8);

	setSymbol(SYM_PLAYWAIT, keyhandlers.getLast("timeout"), 3);
	
	setSymbol(SYM_TRIM, "1200", 8);

	setSymbol(SYM_BASE, "http://localhost/", 160);

	e = getenv("BAYONNE_LANGUAGE");
	if(e)
		setSymbol(SYM_LANGUAGE, e, 16);
	else
		setSymbol(SYM_LANGUAGE, img->getLast("language"), 16);
	repSymbol(SYM_LANGUAGE, group->getLast("language"));

	e = getenv("BAYONNE_VOICE");
	if(e)
		setSymbol(SYM_VOICE, e, 16);
	else
		setSymbol(SYM_VOICE, img->getLast("voice"), 16);
	repSymbol(SYM_VOICE, group->getLast("voice"));

	setSymbol(SYM_APPL, img->getLast("application"), 16);
	repSymbol(SYM_APPL, group->getLast("application"));

	setSymbol(SYM_VOLUME, group->getLast("volume"), 3);

	setSymbol(SYM_BUFFER, "8000", 8);

	setConst(SYM_NAME, "UNKNOWN");
	setConst(SYM_CALLER, "UNKNOWN");
	setConst(SYM_DIALED,  group->getNumber());
	setConst(SYM_INFODIGITS, "00");
	setConst(SYM_CLID, "UNKNOWN");
	setConst(SYM_DNID, "UNKNOWN");

	snprintf(buf, sizeof(buf), "port/%d", tsid);
	setConst(SYM_PORTID, buf);

	const char *test = Trunk::getExternal("session.date");
	if(extNumber[0])
	{
		setConst(SYM_EXTNUMBER, extNumber);
		setSymbol(SYM_LOGIN, extNumber);
	}
	else
	{
		login = keyserver.getLogin();
		if(!stricmp(login, "none") || !stricmp(login, "admin"))
			setSymbol(SYM_LOGIN, login);
		else if(!stricmp(login, "port"))
		{
			snprintf(buf, 4, "%03d", id);
			setSymbol(SYM_LOGIN, buf);
		}
		else
			setSymbol(SYM_LOGIN, "none");
	}

	initSyms();

	sprintf(buf, "%03d", id);
	setConst(SYM_ID, buf);
	setConst(SYM_TRUNKID, buf);

	setConst(SYM_DRIVER, driver->getName());
	sprintf(buf, "%02d", driver->tsid);
	setConst(SYM_DRIVERID, buf);
	sprintf(buf, "%04d", driver->getTrunkCount());
	setConst(SYM_DRIVERSIZE, buf);

//	sprintf(buf, "%d", driver->getTrunkCount());
//	setConst(SYM_PORTS, buf);

	time(&now);

	sprintf(buf, "%s-%03d-%02d%x", keyserver.getNode(), id, driver->getDriverIndex(), now);
	setConst(SYM_GID, buf);
	setConst(SYM_CALLFWD, "none");

	switch(flags.trunk)
	{
	case TRUNK_MODE_INCOMING:
		setConst(SYM_CALLTYPE, "incoming");
		break;
	case TRUNK_MODE_OUTGOING:
		setConst(SYM_CALLTYPE, "outgoing");
	}

	if(parent)
	{
		event.id = TRUNK_CHILD_START;
		event.parm.trunk = this;
		parent->postEvent(&event);
	}

	if(span)
	{
		sprintf(buf, "%d", span);
		setConst(SYM_SPAN, buf);
	}

	dt = localtime_r(&now, &tbuf);
	sprintf(buf, "%04d%02d%02d",
		dt->tm_year + 1900, dt->tm_mon + 1, dt->tm_mday);
	setConst(SYM_STARTDATE, buf);
	sprintf(buf, "%02d%02d%02d",
		dt->tm_hour, dt->tm_min, dt->tm_sec);
	setConst(SYM_STARTTIME, buf);

//	setConst(SYM_RELEASE, "1");
//	setConst(SYM_VERSION, cmd->getLast("version"));
//	setConst(SYM_SERVER, cmd->getLast("server"));
//	setConst(SYM_DRIVER, plugins.getDriverName());
//	setConst(SYM_NODE, cmd->getLast("node"));
	setConst(SYM_START, name);
//	setConst(SYM_SCRIPTS, keypaths.getScriptFiles());
//	setConst(SYM_PROMPTS, keypaths.getPromptFiles());

	sprintf(buf, "%d", member);
	setConst(SYM_MEMBER, buf);

	group->getIndex(index, 127);
	while(*pol)
	{
		// do not post dial rewrite rules into %policy.xxx vars!

		if(isdigit(**pol))
		{
			++pol;
			continue;
		}

		sprintf(buf, "policy.%s", *pol);
		setConst(buf, group->getLast(*pol));
		++pol;
	}

	application.getIndex(index, 127);
	pol = index;
	while(*pol)
	{
		sprintf(buf, "application.%s", *pol);
		setSymbol(buf, group->getLast(*pol));
		++pol;
	}

	attachModules(this);
	cdrc = 0;
	if(!starttime)
	{
		time(&starttime);
		time(&idletime);
		idle_timer = group->getIdleTime();
	}

	debug->debugState(this, "attach script");

	snprintf(buf, 5, "%s", getSymbol(SYM_CALLTYPE));
	setCalls(buf);
	setField(callrec[tsid].cr_caller, getSymbol(SYM_CLID), 16);
	cp = getSymbol(SYM_DIALER);
	if(!cp)
		cp = getSymbol(SYM_DNID);
	setField(callrec[tsid].cr_dialed, cp, 16);
	setField(callrec[tsid].cr_script, name, 12);
	setField(callrec[tsid].cr_login, getSymbol(SYM_LOGIN), 11);
	return true;
}

void Trunk::detach(void)
{
	Trunk *child = NULL;
	TrunkEvent event;
	char buffer[256];
	char overflow[256];
	int i = 0;
	const char *tag, *val, *gid;
	unsigned len = 0;

	ctx = this;

	setCalls("exit");

	if(!flags.script || !running)
		return;

	if(tonetmp)
		delete tonetmp;

	tonetmp = NULL;

	++seq;
	getName(buffer);
//	slog(Slog::levelDebug) << buffer << ": hi water mark at detach; " << getPages() << endl;
	strcpy(buffer, "cdr");
	len = 3;
	if(cdrv)
	{
		while(i < cdrv->argc && len < sizeof(buffer) - 3)
		{
			tag = cdrv->args[i++];
			if(*tag == '=')
				val = getContent(cdrv->args[i++]);
			else if(*tag == '%')
				val = getContent(tag);
			else
				continue;

			if(!val)
				continue;

			if(!*val)
				continue;

			urlEncode(val, overflow, sizeof(overflow));
			buffer[len++] = ' ';
			snprintf(buffer + len, sizeof(buffer) - len,
				" %s=%s", ++tag, overflow);
			len = strlen(buffer);
		}
		buffer[len++] = '\n';
		buffer[len] = 0;
		control(buffer);
	}

	cdrv = NULL;

	tag = getSymbol(SYM_PARENT);
	if(tag)
		child = Driver::getTrunk(tag, driver);

        gid = getSymbol(SYM_GID);
        if(gid)
                gid = strchr(gid, '-');

	if(child)
	{
		child->enterMutex();
		event.id = TRUNK_CHILD_EXIT;
		if(child->postEvent(&event))
		{
			child->setSymbol(SYM_EVENTID, gid);
			child->setSymbol(SYM_EVENTMSG, exitmsg);
		}
		child->leaveMutex();
	}
	
	dtmf.bin.data[0] = 0;
	digits = 0;
	cdrc = 0;
	ScriptInterp::detach();
	detachModules(this);
	ScriptSymbols::purge();
	starttime = 0;
	flags.script = false;
	flags.onexit = false;
	flags.listen = flags.bgm = false;
	if(tgi.pid)
		kill(tgi.pid, SIGHUP);
	tgi.pid = 0;
	if(tgi.fd > -1)
	{
		close(tgi.fd);
		tgi.fd = -1;
	}

	++asr.seq;

	if(asr.pid)
		kill(asr.pid, SIGHUP);
	asr.pid = 0;

	if(asr.fd > -1)
	{
		close(asr.fd);
		asr.fd = -1;
	}

	if(rpc.fd > -1)
		writeRPC("exit", "detach");

	if(tonetmp)
	{
		delete tonetmp;
		tonetmp = NULL;
	}

	debug->debugState(this, "detach script");
}

void Trunk::stopServices(void)
{
	if(thread)
	{
		if(thread->isExiting())
			delete thread;
	}
	thread = NULL;
}

const char *Trunk::getPrefixPath(void)
{
	const char *prefix = getMember();

        if(!prefix)
                prefix = "";

        if(!stricmp(prefix, "feed"))
                prefix = "memory";
        else
                prefix = getKeyword("prefix");

        if(!prefix)
                return prefix;

        if(!stricmp(prefix, "memory"))
                return keypaths.getLast("tmpfs");
        return prefix;
}

const char *Trunk::getExtReference(const char *ref)
{
	const char *cp = strchr(ref, '-');
	Trunk *trunk;

	if(!cp)
		return ref;

	trunk = Driver::getTrunk(++cp);
	if(!trunk)
		return NULL;

	if(!trunk->extNumber[0])
		return NULL;

	return trunk->extNumber;
}

timeout_t Trunk::getTimeout(const char *keywd)
{
	ScriptImage *img = getImage();
	if(keywd)
		keywd = getKeyword(keywd);
	if(!keywd)
		keywd = getValue("86400");
	return getSecTimeout(keywd);
}

timeout_t Trunk::getInterdigit(const char *keywd)
{
	ScriptImage *img = getImage();

	if(keywd)
		keywd = getKeyword(keywd);
	if(!keywd)
		keywd = getValue(img->getLast("interdigit"));
	return getSecTimeout(keywd);
}

unsigned short Trunk::getDigitMask(const char *cp)
{
	static char *digits = "0123456789*#abcd";
	unsigned short mask = 0;
	const char *dp;

	if(cp)
		cp = getKeyword(cp);

	if(!cp)
		cp = getValue(NULL);

	if(!cp)
		return 0;
	
	while(*cp)
	{
		dp = strchr(digits, tolower(*cp));
		++cp;
		if(dp)
			mask |= (1 << (int)(dp - digits));
	}
	return mask;
}

bool Trunk::trunkEvent(const char *evt)
{
#ifdef	SCRIPT_NAMED_EVENTS
	Line *line;
	char outline[256];
	Name::Event *ev;
	unsigned len, idx;
	const char *cp;
	bool partial = false;
	bool match = false;
	char dig, mask;
	char cbuf[8];
	unsigned clen = 0;

	if(!isRunning())
		return false;

	if(tgi.fd > -1)
	{
		snprintf(outline, sizeof(outline), "@%s\n", evt);
		writeShell(outline);
		return false;
	}

	if(!strnicmp(evt, "route:", 6))
	{
		snprintf(cbuf, sizeof(cbuf), "%s/", getSymbol(SYM_DIALING));
		clen = strlen(cbuf);
		goto plan;
	}

	if(!strnicmp(evt, "digits:", 7))
	{
		snprintf(cbuf, sizeof(cbuf), "%s/", getSymbol(SYM_DIALING));
		clen = strlen(cbuf);
		goto plan;
	}

	goto trap;

plan:
	ev = getObject()->events;	 
	
	while(ev)
	{
		len = strchr(evt, ':') - evt + 1;
		cp = ev->name;
		if(strchr(cp, ':'))
		{
			if(!strnicmp(cp, evt, len))
				cp += len;
			else
			{
				ev = ev->next;
				continue;
			}
		}

		if(strchr(cp, '/'))
		{
			if(!strnicmp(cp, cbuf, clen))
				cp += clen;
			else
			{
				ev = ev->next;
				continue;
			}	
		}
		idx = 0;
		if(!stricmp(cp, evt + len))
			break;
		while(cp[idx] && evt[idx + len])
		{
			dig = toupper(evt[idx + len]);
			mask = toupper(cp[idx]);
			if(mask == dig)
			{
				++idx;
				continue;
			}
			if(mask == 'X' && dig >= '0' && dig <= '9')
			{
				++idx;
				continue;
			}
			if(mask == 'N' && dig >= '2' && dig <= '9')
			{
				++idx;
				continue;
			}
			if(mask == 'O' && dig > '1' && dig <= '9')
			{
				++idx;
				--len;
				continue;
			}
			if(mask == '0' && dig == '1')
			{
				++idx;
				continue;
			}
			if(mask == 'Z' && dig > '0' && dig <= '9')
			{
				++idx;
				--len;
				continue;
			}
			if(mask == 'Z' && dig == '0')
			{
				++idx;
				continue;
			}
			break;
		}
		if(!evt[idx + len])
		{
			if(!strnicmp(evt, "route:", 6))
				break;
			partial = true;
		}
		if(!cp[idx] && !evt[idx + len])
			break;
		ev = ev->next;
	}

	if(ev)
		evt = ev->name;
	else if(!strnicmp(evt, "digits:", 7))
	{
		if(partial)
			evt = "digits:partial"; 
		else
			evt = "digits:invalid";
	}

trap:
	if(event(evt))
	{
               line = getLine();
               if(line)
                        if(line->method == (Method)&Trunk::scrRedirect)
                                (this->*(line->method))();
                return true;
	}
#endif
	return false;
}

bool Trunk::trunkSignal(trunksignal_t signal)
{
	Line *line;
	char buffer[256];

	if(!isRunning())
		return true;

	if(signal == TRUNK_SIGNAL_HANGUP)
	{
		if(flags.onexit)
			return false;
	}

	if(!signal)
	{
		if(tgi.fd > -1)
			return true;
		advance();
		return true;
	}

	if(tgi.fd > -1)
	{
		switch(signal)
		{
		case TRUNK_SIGNAL_HANGUP:
			if(tgi.pid)
				kill(tgi.pid, SIGHUP);
			tgi.pid = 0;
			close(tgi.fd);
			tgi.fd = -1;
			break;
		case TRUNK_SIGNAL_ERROR:
			snprintf(buffer, sizeof(buffer), "+error=%s\n",
				getSymbol(SYM_ERROR));
			writeShell(buffer);
			return true;
		case TRUNK_SIGNAL_TIMEOUT:
			writeShell("timeout\n");
			return true;
		case TRUNK_SIGNAL_0:
		case TRUNK_SIGNAL_1:
		case TRUNK_SIGNAL_2:
		case TRUNK_SIGNAL_3:
		case TRUNK_SIGNAL_4:
		case TRUNK_SIGNAL_5:
		case TRUNK_SIGNAL_6:
		case TRUNK_SIGNAL_7:
		case TRUNK_SIGNAL_8:
		case TRUNK_SIGNAL_9:
		case TRUNK_SIGNAL_STAR:
		case TRUNK_SIGNAL_POUND:
			snprintf(buffer, sizeof(buffer), "%c\n",
				digit[signal - TRUNK_SIGNAL_0]);
			writeShell(buffer);
			return false;
		case TRUNK_SIGNAL_OVERRIDE:
			writeShell("override\n");
			return false;
		case TRUNK_SIGNAL_FLASH:
			writeShell("flash\n");
			return false;
		case TRUNK_SIGNAL_IMMEDIATE:
			writeShell("immediate\n");
			return false;
		case TRUNK_SIGNAL_PRIORITY:
			writeShell("priority\n");
			return false;
		case TRUNK_SIGNAL_BUSY:
			writeShell("busy\n");
			return true;
		case TRUNK_SIGNAL_FAIL:
			writeShell("fail\n");
			return true;
		case TRUNK_SIGNAL_NOANSWER:
			writeShell("noanswer\n");
			return true;
		case TRUNK_SIGNAL_RING:
			writeShell("line\n");
			return false;
		case TRUNK_SIGNAL_TONE:
			snprintf(buffer, sizeof(buffer), "+tone=%s\n",
				getSymbol(SYM_TONE));
			writeShell(buffer);
			return false;
		case TRUNK_SIGNAL_EVENT:
			snprintf(buffer, sizeof(buffer), "+event=%s;msg=%s\n",
				getSymbol(SYM_EVENTID), getSymbol(SYM_EVENTMSG));
			writeShell(buffer);
			return false;
		case TRUNK_SIGNAL_TIME:
			writeShell("sync\n");
			return false;
		case TRUNK_SIGNAL_CHILD:
			writeShell("child\n");
			return false;
		default:
			return true;
		}
	}

	if(signal == TRUNK_SIGNAL_GOTO)
	{
		line = getLine();
		if(line->argc)
                        //scrGoto();
			slog(Slog::levelError) << "TRUNK_SIGNAL_GOTO fix me!" << endl;
		else
			advance();
		return true;
	}

	if(ScriptInterp::signal((unsigned long)(signal) - 1))
	{
		line = getLine();
		if(signal == TRUNK_SIGNAL_HANGUP)
		{
			//autoloop(false);	// no loop in exit handlers!
			if(line)
				flags.onexit = true;
		}
		if(line)
			if(line->scr.method == (Method)&Trunk::scrRedirect)
				(this->*(line->scr.method))();
		return true;
	}

	return false;
}

bool Trunk::idleHangup(void)
{
	time_t now;

	if(!idle_timer)
		return false;

	time(&now);
	if(now - idletime > idle_timer)
	{
		exit();
		return true;
	}
	return false;
}

timeout_t getMSTimeout(const char *opt)
{
        char *end;
        char decbuf[4];
        long value;
        unsigned len;

        if(!opt)
                opt = "0";

        value = strtol(opt, &end, 10) * 1000;
        if(*end == '.')
        {
                strncpy(decbuf, ++end, 3);
                decbuf[4] = 0;
                len = strlen(decbuf);
                while(len < 3)
                        decbuf[len++] = '0';
                value += strtol(decbuf, &end, 10);
        }


        switch(*end)
        {
        case 'h':
        case 'H':
                return value * 3600;
        case 'm':
        case 'M':
                if(end[1] == 's' || end[1] == 'S')
                        return value / 1000;
                return value * 60;
        default:
                return value / 1000;
        }
}
		
timeout_t getSecTimeout(const char *opt)
{
	char *end;
	char decbuf[4];
	long value;
	unsigned len;

	if(!opt)
		opt = "0";

	value = strtol(opt, &end, 10) * 1000;
	if(*end == '.')
	{
		strncpy(decbuf, ++end, 3);
		decbuf[4] = 0;
		len = strlen(decbuf);
		while(len < 3)
			decbuf[len++] = '0';
		value += atol(decbuf);
	}

	switch(*end)
	{
	case 'h':
	case 'H':
		return value * 3600;
	case 'm':
	case 'M':
		if(end[1] == 's' || end[1] == 'S')
			return value / 1000;
		return value * 60;
	default:
		return value;
	}				
}

bool getLogical(const char *str)
{
	if(*str == '.')
		++str;
	switch(*str)
	{
	case '0':
	case 'f':
	case 'F':
	case 'N':
	case 'n':
		return false;
	}
	return true;
}

#ifdef	SCRIPT_IF_OVERRIDE

bool Trunk::ifFeature(ScriptInterp *interp, const char *v)
{
	Driver *drv = ((Trunk *)(interp))->getDriver();

	if(!stricmp(v, "tts") && hasTTS())
		return true;

#ifdef	HAVE_TGI
	if(!stricmp(v, "tgi"))
		return true;
#else
	if(!stricmp(v, "tgi"))
		return false;
#endif

	if(!stricmp(v, "join") && (drv->getCaps() & Driver::capJoin))
		return true;

	if(!stricmp(v, "switch") && (drv->getCaps() & Driver::capSwitch))
		return true;

	if(!stricmp(v, "spans") && (drv->getCaps() & Driver::capSpans))
		return true;

	if(!stricmp(v, "speed") && (drv->getCaps() & Driver::capSpeed))
		return true;

	if(!stricmp(v, "gain") && (drv->getCaps() & Driver::capGain))
		return true;

	if(!stricmp(v, "pitch") && (drv->getCaps() & Driver::capPitch))
		return true;

	if(!stricmp(v, "tts") && hasTTS())
		return true;

#ifdef	HAVE_TGI
	if(!stricmp(v, "tgi"))
		return true;
#else
	if(!stricmp(v, "tgi"))
		return false;
#endif

	if(!stricmp(v, "say") && tts)
		return true;

	if(!stricmp(v, "listen") && (drv->getCaps() & Driver::capListen))
		return true;

	if(!strnicmp(v, "conf", 4) && (drv->getCaps() & Driver::capConference))
		return true;

	if(!stricmp(v, "voice") && (((Trunk *)(interp))->getCapabilities() & TRUNK_CAP_VOICE))
		return true;

        if(!stricmp(v, "dial") && (((Trunk *)(interp))->getCapabilities() & TRUNK_CAP_DIAL))
                return true;

        if(!stricmp(v, "fax") && (((Trunk *)(interp))->getCapabilities() & (TRUNK_CAP_SENDFAX|TRUNK_CAP_RECVFAX)))
                return true;

        if(!stricmp(v, "data") && (((Trunk *)(interp))->getCapabilities() & TRUNK_CAP_DATA))
                return true;

        if(!stricmp(v, "station") && (((Trunk *)(interp))->getCapabilities() & TRUNK_CAP_STATION))
                return true;

	return false;
}

bool Trunk::ifDTMF(ScriptInterp *interp, const char *v)
{
	if(!stricmp(v, "line") && ((Trunk *)(interp))->flags.digits == DTMF_MODE_LINE)
		return true;

	if(!stricmp(v, "script") && ((Trunk *)(interp))->flags.digits == DTMF_MODE_SCRIPT)
		return true;

	if(!stricmp(v, "on") && ((Trunk *)(interp))->flags.digits == DTMF_MODE_ON)
		return true;

	if(!stricmp(v, "off") && ((Trunk *)(interp))->flags.digits == DTMF_MODE_OFF)
		return true;

	return false;
}

bool Trunk::ifRinging(ScriptInterp *interp, const char *v)
{
	Trunk *trunk = Driver::getTrunk(v);

	if(!trunk)
		return false;

	if(!trunk->rings)
		return false;

	if(trunk->flags.offhook)
		return false;

	return true;
}

bool Trunk::ifRunning(ScriptInterp *interp, const char *v)
{
	Trunk *trunk = Driver::getTrunk(v);
	
	if(!trunk)
		return false;

	if(trunk->isRunning())
		return true;

	return false;
}

bool Trunk::ifPort(ScriptInterp *interp, const char *v)
{
	Trunk *trunk = Driver::getTrunk(v);
	if(trunk)
		return true;

	return false;
}
	
bool Trunk::isDnd(ScriptInterp *interp, const char *v)
{
        Trunk *trunk;
	char name[128];

	v = getExtReference(v);

        if(!v)
                return false;

	snprintf(name, sizeof(name), "Ext/%s", v);
        trunk = Driver::getTrunk(name);
        if(!trunk)
                return false;

        return trunk->flags.dnd;
}

bool Trunk::isSchedule(ScriptInterp *interp, const char *v)
{
	if(!v)
		return false;

	if(!stricmp(v, "none"))
		v = "";
	
	if(!stricmp(v, "*"))
		v = "";

#ifdef	SCHEDULER_SERVICES
	if(!stricmp(v, scheduler.getSchedule()))
		return true;
#endif

	return false;
}

bool Trunk::isService(ScriptInterp *interp, const char *v)
{
	const char *svc;

	if(!v)
		return false;

	if(!stricmp(v, "up") && !service[0])
		return true;

	if(!service[0])
		return false;

        if(!stricmp(v, "test"))
        {
                if(!strnicmp(service, "test::", 6))
                        return true;
        }

        if(!strnicmp(service, "test::", 6))
		return false;

	if(!stricmp(v, "down"))
		return true;

	if(strchr(service, ':'))
		svc += 2;
	else
		svc = service;

	if(!stricmp(v, svc))
		return true;

	return false;
}

bool Trunk::isNode(ScriptInterp *interp, const char *v)
{
	const char *node = keyserver.getNode();
	if(!node)
		return false;

	if(!stricmp(node, v))
		return true;

	return false;
}

bool Trunk::hasDriver(ScriptInterp *interp, const char *v)
{
	if(ost::getDriver(v))
		return true;

	return false;
}

bool Trunk::hasGroup(ScriptInterp *interp, const char *v)
{
	if(getGroup(v))
		return true;

	return false;
}

bool Trunk::hasPlugin(ScriptInterp *interp, const char *v)
{
	if(getModule(MODULE_ANY, v))
		return true;

	return false;
}

bool Trunk::hasVoice(ScriptInterp *interp, const char *v)
{
	const char *prompts = keypaths.getLast("prompts");
	char buf[256];

	snprintf(buf, sizeof(buf), "%s/%s", prompts, v);
	if(isDir(buf))
		return true;

	return false;
}

bool Trunk::hasVarPrompt(ScriptInterp *interp, const char *v)
{
	char buf[256];
	char *ext = "";
	char *cp;
	char *prefix = interp->getKeyword("prefix");

	cp = strchr(v, '/');
	if(cp)
		cp = strchr(++cp, '.');
	else
		cp = strchr(v, '.');

	if(!cp)
		ext = interp->getSymbol(SYM_EXTENSION);

	if(prefix)
		snprintf(buf, sizeof(buf), "%s/%s%s", prefix, v, ext);
	else
		snprintf(buf, sizeof(buf), "%s%s", v, ext);

	if(!permitAudioAccess(buf, false))
		return false;

	if(isFile(buf))
		return true;

	return false;
}	

bool Trunk::hasSysPrompt(ScriptInterp *interp, const char *v)
{
	Name *scr = interp->getObject();
	const char *prompts = keypaths.getLast("prompts");
	char buf[256];
	char name[65];
	char *cp;
	char *ext = "";

	snprintf(name, sizeof(name), "%s", scr->name);
	cp = strstr(name, "::");
	if(cp)
		*cp = 0;

	if(strchr(v, '/'))
		return false;

	if(!strchr(v, '.'))
		ext = ".au";

	snprintf(buf, sizeof(buf), "%s/sys/%s/%s%s",
		prompts, name, v, ext);

	if(isFile(buf))
		return true;

	return false;
}

bool Trunk::hasSysVoice(ScriptInterp *interp, const char *v)
{
        const char *prompts = keypaths.getLast("prompts");
        char buf[256];

        snprintf(buf, sizeof(buf), "%s/sys/%s", prompts, v);
        if(isDir(buf))
                return true;

        return false;
}

bool Trunk::hasAltVoice(ScriptInterp *interp, const char *v)
{
        const char *prompts = keypaths.getLast("altprompts");
        char buf[256];

	if(!prompts)
		prompts = keyserver.getPrefix();

        snprintf(buf, sizeof(buf), "%s/%s", prompts, v);
        if(isDir(buf))
                return true;

        return false;
}

bool Trunk::hasAppVoice(ScriptInterp *interp, const char *v)
{
        const char *prompts = keypaths.getLast("prompts");
	Name *scr = interp->getObject();
	char *cp;
        char buf[256];
	char name[65];

	snprintf(name, sizeof(name), "%s", scr->name);
	cp = strchr(name, ':');
	if(cp)
		*cp = 0;

        snprintf(buf, sizeof(buf), "%s/%s/%s", prompts, v, name);
        if(isDir(buf))
                return true;

        return false;
}


#endif

#ifdef	CCXX_NAMESPACES
};
#endif
