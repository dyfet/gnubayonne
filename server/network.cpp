// Copyright (C) 2000-2001 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "server.h"

#ifdef	NODE_SERVICES

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

#define	PROTOCOL_VERSION	3
#define	MAX_BUDDIES		10

static	buddy_t		buddy[MAX_BUDDIES];
static	int		buddies = 0;	// how many node buddies do I have

Network::Network() :
#ifdef	COMMON_OST_NAMESPACE
Thread(keythreads.priNetwork(), keythreads.getStack()),
#else
Thread(NULL, keythreads.priNetwork(), keythreads.getStack()),
#endif
UDPSocket(keynetwork.getAddress(), keynetwork.getPort()),
MappedFile(getPath(), accessReadWrite)
{
}

void Network::initial(void)
{
	statnode_t node;
	int i, nodes = keyserver.getNodeCount();

	time(&last);
	UDPSocket::setError(false);
	RandomFile::setError(false);
	bcast_in = NULL;
	maddr_in = NULL;
	if(setBroadcast(true))
		slog(Slog::levelWarning) << "network: no broadcast access" << endl;
	else
	{
		bcast_in = (struct sockaddr_in *)&bcast;
		bcast_in->sin_addr = getaddress(keynetwork.getBroadcast());
		bcast_in->sin_port = htons(keynetwork.getPort());
		bcast_in->sin_family = AF_INET;
	}

	if(keynetwork.getLast("monitor"))
	{
		maddr_in = (struct sockaddr_in *)&maddr;
		maddr_in->sin_addr = getaddress(keynetwork.getMonitorAddress());
		maddr_in->sin_port = htons(keynetwork.getMonitorPort());
		maddr_in->sin_family = AF_INET;
	}
		 	
	memset(&node, 0, sizeof(node));
	for(i = 0; i < nodes; ++i)
		::write(fd, &node, sizeof(node));

	map = (statnode_t *)MappedFile::fetch(0, nodes * sizeof(node));
	if(!map)
		slog(Slog::levelError) << "network: mapping failed" << endl;

	memset(&buddy, 0, sizeof(buddy));
}

void Network::run(void)
{
	int rtn, bud;
	int nodes = keyserver.getNodeCount();
	time_t now;
	long diff;
	int refresh = keynetwork.getRefresh();
	statnode_t node;
	int ports = 0;
	statnode_t *list, *from;
	char *buffer = (char *)&node;
	struct sockaddr_in addr;
	socklen_t alen = sizeof(addr);
	Module *net;
	TrunkGroup *grp = getGroup();
	Driver *drv;

	time(&last);
	last -= refresh - 1;
	slog(Slog::levelInfo) << "network: starting" << endl;

	for(;;)
	{
		failover();
		setCancel(cancelImmediate);
		time(&now);
		if(now >= last + refresh)
		{
			if(nodes > 1)
				elect();
			net = Module::netFirst;
			while(net)
			{
				net->broadcast();
				net = net->netNext;
			}
			memset(&node, 0, sizeof(node));
			node.version = PROTOCOL_VERSION;
			node.buddies = buddies;
			strncpy(node.name, keyserver.getNode(), sizeof(node.name));
			strncpy((char *)&node.update, "*MON", 4);
			node.ports = Driver::getCount();
			if(!strnicmp(service, "test::", 6))
				node.service = 't';
			else if(service[0])
				node.service = 'd';
			else
				node.service = 'u';
#ifdef	SCHEDULER_SERVICES
			snprintf(node.schedule, sizeof(node.schedule), "%s", scheduler.getSchedule());
#else
			snprintf(node.schedule, sizeof(node.schedule), "none");
#endif

			node.uptime = grp->getStat(STAT_SYS_UPTIME);
			node.calls = grp->getStat(STAT_SYS_ACTIVITY);
			Driver::getStatus(node.stat);
			if(bcast_in)
			{
				::sendto(so, &node, sizeof(node), 0,
					&bcast, sizeof(struct sockaddr_in));
			}
			if(maddr_in)
			{
				::sendto(so, &node, sizeof(node), 0,
					&maddr, sizeof(struct sockaddr_in));
			}
			time(&node.update);
			memcpy(map, &node, sizeof(node));
			time(&last);
		}
		diff = last + refresh - now;
		if(diff < 0)
			continue;


		if(diff < 1)
			diff = 100;
		else
			diff = diff * 1000;

		if(!isPending(Socket::pendingInput, diff))
			continue;

		setCancel(cancelDeferred);
		memset(&node, 0, sizeof(node));
		alen = sizeof(addr);
		rtn = ::recvfrom(so, buffer, sizeof(node), 0, 
			(struct sockaddr *)&addr, &alen);
		if(rtn < 1)	
		{		
			slog(Slog::levelWarning) << "network: input error..." << rtn << endl;
			Thread::sleep(5000);
			continue;
		}
		if(!strnicmp(buffer, "*BUD", 4))
		{
			list = getNode(node.name);
			from = getAddr(&addr.sin_addr);
			if(list != map || !from)	
				continue;
			for(bud = 0; bud < MAX_BUDDIES; ++bud)
			{
				if(buddy[bud].buddy == from)
				{
					time(&buddy[bud].update);
					break;
				}
			}
			for(bud = 0; bud < MAX_BUDDIES; ++bud)
			{
				if(!buddy[bud].buddy)
				{
					time(&buddy[bud].update);
					buddy[bud].buddy = from;
					++buddies;
					break;
				}
			}
		}
		else if(!strnicmp(buffer, "*MON", 4))
		{
			memcpy(&node.addr, &addr.sin_addr, sizeof(node.addr));
			time(&node.update);
			list = map;

			while(list->name[0])
			{
				if(!memcmp(list->name, node.name, sizeof(node.name)))
					break;
				++list;
			}
			memcpy(list, &node, sizeof(node));
		}
		else
		{
			net = Module::netFirst;
			alen = rtn;
			list = getAddr(&addr.sin_addr);
			while(net)
			{
				rtn = net->accept(buffer, alen, list);
				if(rtn > 0)
				{
					::sendto(so, buffer, rtn, 0,
						(struct sockaddr *)&addr,
						sizeof(sockaddr_in));
				}
				if(rtn)
					break;
				net = net->netNext;
			}
		}
	}
}

void Network::stop(void)
{
	slog(Slog::levelDebug) << "network: stopping" << endl;
	terminate();
	endSocket();
}

char *Network::getPath(void) 
{
        static char path[256];
	int fd;
 
        if(canModify(keypaths.getRunfiles()))
                sprintf(path, "%s/bayonne.nodes", keypaths.getRunfiles());
        else
                sprintf(path, "%s/.bayonne.nodes", getenv("HOME"));
	keypaths.setValue("nodefile", path);
	remove(path);
	fd = creat(path, 0640);
	::close(fd);
	chown(path, keyserver.getUid(), keyserver.getGid());
        return path;
}

statnode_t *Network::getAddr(struct in_addr *addr)
{
	statnode_t *list = map;
	while(list->name[0])
	{
		if(!memcmp(addr, &list->addr, sizeof(addr)))
			return list;
		++list;
	}
	return NULL;
} 

statnode_t *Network::getNode(const char *name) 
{
        statnode_t *list = map;
 
        while(list->name[0])
        {
                if(!strncmp(list->name, name, 16))
                        return list;
                ++list;
        }
        return NULL; 
}

void Network::elect(void)
{
	static time_t last = 0;
	statnode_t *list = map;
	statnode_t *buddy = NULL;
	statnode_t node;
	int level = 255;
	int elect = keynetwork.getTimeToElect();
	time_t now;

	if(!bcast_in)
		return;

	time(&now);
	if(last + elect > now)
		return;

	time(&last);
	if(list->name[0])
		++list;
	while(list->name[0])
	{
		if(list->buddies < level)
		{
			buddy = list;
			level = buddy->buddies;
		}
		++list;
	}	
	if(!buddy)
	{
		slog(Slog::levelWarning) << "network: cannot elect" << endl;
		return;
	}
	slog(Slog::levelInfo) << "network: " << buddy->name << ": elected" << endl;
	memset(&node, 0, sizeof(node));
	strncpy(node.name, buddy->name, 16);
	strncpy((char *)&node.update, "*BUD", 4);
	::sendto(so, &node, 0, 24, &bcast, sizeof(struct sockaddr_in));
}

void Network::failover(void)
{
	statnode_t *list = map;
	int live = keynetwork.getTimeToLive();
	int expire = keynetwork.getTimeToExpire();
	time_t now;
	Module *net;
	int bud;

	time(&now);
	while(list->name[0])
	{
		if(!list->update)
		{
			++list;
			continue;
		}

		if(now < (list->update + live))	                   
		{
			++list;
			continue;
		}
		slog(Slog::levelInfo) << "network: " << list->name << ": gone..." << endl;
		for(bud = 0; bud < MAX_BUDDIES; ++bud)
		{
			if(buddy[bud].buddy == list)
				break;
		}
		if(bud >= MAX_BUDDIES)
		{
			++list;
			continue;
		}
	
		net = Module::netFirst;
		while(net)
		{
			net->failover(list);
			net = net->netNext;
		}
		list->update = 0;
		++list;
	}
	for(bud = 0; bud < MAX_BUDDIES; ++bud)
	{
		if(!buddy[bud].update)
			continue;
		if(buddy[bud].update + expire < now)
		{
			memset(&buddy[bud], 0, sizeof(buddy_t));
			--buddies;
		}
	}
}

void Network::send(char *msgbuf, int msglen)
{
	if(!bcast_in)
		return;

	::sendto(so, msgbuf, msglen, 0,
		&bcast, sizeof(struct sockaddr_in));
}
	
void broadcast(char *msgbuf, int msglen)
{
	network.send(msgbuf, msglen);
}

statnode_t *getNodes(const char *name)
{
	if(!name)
		return network.map;

	return network.getNode(name);
}

#ifdef	CCXX_NAMESPACES
};
#endif

#endif
