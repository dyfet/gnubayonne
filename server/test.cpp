// Copyright (C) 2000-2001 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "bayonne.h"
#include <iomanip>

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

static RETSIGTYPE sigfault(int signo)
{
	Debug::stackTrace(signo);
	exit(signo);
}

class TestDebug : public Debug
{
private:
	bool script;
	int test;
	int success;

public:
	TestDebug();
//	bool debugFinal(int signo);
	void debugService(Trunk *trunk, char *msg);
	void debugState(Trunk *trunk, char *state);
	void debugScript(Trunk *trunk, char *msg);
	void debugStep(Trunk *trunk, Line *line);
	bool debugTest(void);
	bool debugFifo(char **argv);
} testdebug;

TestDebug::TestDebug() :
Debug()
{
	success = 0;
	test = 0;
	slog(Slog::levelInfo) << "debug: test module loaded" << endl;
}

//bool TestDebug::debugFinal(int signo)
//{
//	Debug::stackTrace(signo);
//	return false;
//}	

bool TestDebug::debugFifo(char **argv)
{
	++success;
	slog(Slog::levelInfo) << "debug: fifo->";
	while(*argv)
		slog() << ' ' << *(argv++);
	slog() << endl;
	return true;
}

bool TestDebug::debugTest(void)
{
	TrunkEvent event;
	Trunk *trunk = Driver::drvFirst->getTrunkPort(0);

	if(!trunk)
	{
		slog(Slog::levelError) << "debug: trunk 0 not available for tests" << endl;
		return true;
	}

	signal(SIGSEGV, sigfault);
	signal(SIGABRT, sigfault);

	rmdir("temp");
	rmdir("cache");
	rmdir("maps");
	rmdir("prompts");
	trunk->group->setValue("answer", "1");
	trunk->group->setValue("xml", "bayonnexml");
	slog(Slog::levelInfo) << "debug: *** start of regression test series" << endl;
	Thread::sleep(1000);
	event.id = TRUNK_MAKE_IDLE;
	trunk->postEvent(&event);
	Thread::sleep(1000);

/*	slog(Slog::levelInfo) << "debug: *** start of cdr test" << endl;
	++test;
	script = false;
	keyserver.setValue("default", "test::test1");
	trunk->group->setValue("name", "test::test1");
	trunk->group->setValue("script", "test::test1");
	event.parm.ring.digit = 0;
	event.id = TRUNK_RINGING_ON;
	trunk->postEvent(&event);
	Thread::sleep(250);
	event.id = TRUNK_RINGING_OFF;
	trunk->postEvent(&event);
	Thread::sleep(2000);
	if(script)
	{
		++success;
		slog(Slog::levelInfo) << "debug: *** test cdr successful" << endl;
	}
	else
		slog(Slog::levelInfo) << "debug: *** test cdr failed" << endl;

	slog(Slog::levelInfo) << "debug: *** start of state handler test" << endl;
	++test;
	script = false;
	keyserver.setValue("default", "test::test2");
	trunk->group->setValue("name", "test::test2");
	trunk->group->setValue("script", "test::test2");
	event.parm.ring.digit = 0;
	event.id = TRUNK_RINGING_ON;
	trunk->postEvent(&event);
	Thread::sleep(250);
	event.id = TRUNK_RINGING_OFF;
	trunk->postEvent(&event);
	Thread::sleep(4000);
	if(script)
	{
		++success;
		slog(Slog::levelInfo) << "debug: *** test state successful" << endl;
	}
	else
		slog(Slog::levelInfo) << "debug: *** test state failed" << endl;
*/

	slog(Slog::levelInfo) << "debug: *** start of trap test" << endl;
	++test;
	script = false;
	keyserver.setValue("default", "test::test3");
	trunk->group->setValue("name", "test::test3");
	trunk->group->setValue("script", "test::test3");
	event.parm.ring.digit = 0;
	event.id = TRUNK_RINGING_ON;
	trunk->postEvent(&event);
	event.id = TRUNK_RINGING_OFF;
	trunk->postEvent(&event);
	Thread::sleep(500);
	event.id = TRUNK_DTMF_KEYUP;
	event.parm.dtmf.digit = 1;
	trunk->postEvent(&event);
	Thread::sleep(1000);
	if(script)
	{
		++success;
		slog(Slog::levelInfo) << "debug: *** test trap successful" << endl;
	}
	else
		slog(Slog::levelInfo) << "debug: *** test trap failed" << endl;

	slog(Slog::levelInfo) << "debug: *** start of exittimer test" << endl;
	++test;
	script = false;
	keyserver.setValue("default", "test::test4");
        trunk->group->setValue("name", "test::test4");
	trunk->group->setValue("script", "test::test4");
        event.parm.ring.digit = 0;
        event.id = TRUNK_RINGING_ON;
        trunk->postEvent(&event);
        event.id = TRUNK_RINGING_OFF;
        trunk->postEvent(&event);
        Thread::sleep(3000);
	Driver::drvFirst->secTick();
        Thread::sleep(1000);
        if(script)
        {
                ++success;
                slog(Slog::levelInfo) << "debug: *** test exittimer successful" << endl;
        }
        else
                slog(Slog::levelInfo) << "debug: *** test exittimer failed" << endl;


	Thread::sleep(2000);
	++test;
	slog(Slog::levelInfo) << "debug: *** fifo test..." << endl;
	fifo.command("debug&first&last");
	slog(Slog::levelInfo) << "debug: *** test 5 complete" << endl;
	slog(Slog::levelInfo) << "debug: *** test series completed ";
	slog() << success << " of " << test << " successful" << endl;
	return true;
}

void TestDebug::debugService(Trunk *trunk, char *msg)
{
	char buffer[32];
	if(test > 1)
		return;

	enterMutex();
	if(trunk)
	{
		trunk->getName(buffer);
		slog(Slog::levelDebug) << "debug: " << buffer << ": ";
	}
	else
		slog(Slog::levelDebug) << "debug: ";
	slog() << "service " << msg << endl;
	leaveMutex();
}

void TestDebug::debugScript(Trunk *trunk, char *msg)
{
	char buffer[32];

	script = true;
	enterMutex();
	script = true;
	trunk->getName(buffer);
	slog(Slog::levelDebug) << buffer << ": " << msg << endl;
	leaveMutex();
}

void TestDebug::debugStep(Trunk *trunk, Line *line)
{
	int i;

	char buffer[32];
	trunk->getName(buffer);

	if(!line)
	{
		slog(Slog::levelDebug) << buffer << ": exit" << endl;
		return;
	}

	enterMutex();
	slog(Slog::levelDebug) << buffer << ": step ";
	slog() << setbase(16) << line->mask << " " << setbase(10);
	slog() << line->cmd << "(";
	for(i = 0; i < line->argc; ++i)
	{
		if(i)
			slog() << ",";
		slog() << line->args[i];
	}
	slog() << ")" << endl;	
	leaveMutex();
}

void TestDebug::debugState(Trunk *trunk, char *state)
{
	char buffer[32];
	trunk->getName(buffer);

	enterMutex();
	slog(Slog::levelDebug) << buffer << ": " << state << endl;
	leaveMutex();
}

#ifdef	CCXX_NAMESPACES
};
#endif
