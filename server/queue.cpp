// Copyright (C) 2000-2001 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "server.h"

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

DataQueue::DataQueue(const char *id, int pri) :
Thread(pri), Semaphore(), Mutex()
{
	name = id;
	first = last = NULL;
	started = false;
}

DataQueue::~DataQueue()
{
	data_t *data, *next;
	if(started)
	{
		started = false;
		Semaphore::post();
		terminate();
	}
	data = first;
	while(data)
	{
		next = data->next;
		delete[] data;
	}
}

void DataQueue::run(void)
{
	started = true;
	for(;;)
	{
		Semaphore::wait();
		if(!started)
			sleep(~0);
		startQueue();
		for(;;)
		{
			runQueue(first->data);
			enterMutex();
			first = first->next;
			delete[] first;
			if(!first)
				last = NULL;
			leaveMutex();
			if(first)
				Semaphore::wait(); // demark semaphore
			else
				break;
		}
		stopQueue();
	}
}

void DataQueue::postData(const void *dp, unsigned len)
{
	data_t *data = (data_t *)new char[sizeof(data_t) + len];
	memcpy(data->data, dp, len);
	data->len = len;
	data->next = NULL;
	enterMutex();
	if(!first)
		first = data;
	if(last)
		last->next = data;
	last = data;
	if(!started)
	{
		start();
		started = true;
	}
	leaveMutex();
	Semaphore::post();
}
	
#ifdef	CCXX_NAMESPACES
};
#endif
