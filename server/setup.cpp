// Copyright (C) 2000-2001 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifdef	__FreeBSD__
#define getopt(a, b, c) getopt()
#include <unistd.h>
#undef getopt
#endif

#include <cc++/misc.h>
#include <cc++/strchar.h>
#include "ivrconfig.h"
#ifdef HAVE_MKSTEMP
#include <fstream>
#include <stdlib.h> // mkstemp for Linux, Solaris
#include <unistd.h> // for BSD
#include <errno.h>
#endif
#include <getopt.h>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <sys/stat.h>

#ifdef	CCXX_NAMESPACES
using namespace ost;
using namespace std;
#endif

static struct option long_options[] = {
	{"section", 1, 0, 's'},
	{"modify", 0, 0, 'm'},
	{"libpath", 0, 0, 'l'},
	{"driver", 0, 0, 'd'},
	{"runfiles", 0, 0, 'r'},
	{0, 0, 0, 0}};

static char *strlwr(char *str)
{
	char *ret = str;
	while(*str)
	{
		*str = tolower(*str);
		++str;
	}
	return ret;
}

static const char *List(char *group, char *mkey)
{
	char buffer[65];
	char *keys[128];
	char **key = keys;

	sprintf(buffer, "/bayonne/%s", group);
	Keydata policy(buffer);
	int count = policy.getIndex(keys, 128);
	while(*key)
	{
		if(!mkey)
			cout << strlwr(*key) << "=" << policy.getLast(*key) << endl;
		if(mkey && !stricmp(*key, mkey))
			return policy.getLast(*key);
		++key;
	}
	exit(0);
}

int main(int argc, char **argv)
{
	static bool usage = false;
	char *keys[128];
	char **key = keys;
	char conf[256];
	char buffer[256];	
	char *group;
	int opt, opt_index;
	fstream cf;
#ifdef HAVE_MKSTEMP
	int fd;
	char *s = strdup("/tmp/bayonne_setup_XXXXXX");
#else
	char *tmp = tmpnam(NULL);
#endif
	bool modify = false;
	bool libpath = false;
	bool driver = false;
	bool runfiles = false;
	char *sp;
	
	while(EOF != (opt = getopt_long(argc, argv, "s:mld",
	   long_options, &opt_index)))
		switch(opt)
		{
		case 'd':
			driver = true;
			break;
		case 'l':
			libpath = true;
			break;
		case 's':
			group = optarg;
			break;
		case 'm':
			modify = true;
			break;
		case 'r':
			runfiles = true;
			break;
		default:
			usage = true;
		}

	if(usage || !group)
	{
		cerr << "bayonne_setup: -s section [--modify] [options]" << endl;
		exit(-1);
	}

	strcpy(conf, ETC_PREFIX);
	strcat(conf, "bayonne.conf");

	if(libpath)
	{
		cout << List("paths", "libpath") << "/" << VERPATH << endl;
		exit(0);
	}

	if(runfiles)
	{
		cout << List("paths", "runfiles") << endl;
		exit(0);
	}

	if(driver)
	{
		cout << List("modules", "driver") << endl;
		exit(0);
	}

	if(!modify)
	{
		List(group, NULL);
		exit(0);
	}

	cf.open(conf, ios::in | ios::out);

	if(!cf.is_open())
	{
		cerr << "bayonne_setup: " << conf << ": denied" << endl;
		exit(-1);
	}
#ifdef HAVE_MKSTEMP
	fd = mkstemp(s);
	if(fd == -1) {
		cerr << "bayonne_setup: error opening tempfile: " << strerror(errno) << endl;
		exit(-1);
	}
	free(s);
	std::fstream of(fd);
#else
	std::fstream of;
	remove(tmp);
	of.open(tmp, ios::in | ios::out);
	// WARNING: big fat race condition right here
	chmod(tmp, 0600);
	remove(tmp);
#endif
	if(!of.is_open())
	{
		cerr << "bayonne_setup: tempfile: denied" << endl;
			exit(-1);
	}

	for(;;)
	{
		cf.getline(buffer, sizeof(buffer) - 1);
		if(cf.eof())
			break;

		sp = buffer;
		while(*sp == ' ' || *sp == '\t')
			++sp;
		if(*sp == '[')
		{
			if(!strnicmp(++sp, group, strlen(group)))
				break;
		}
		of << buffer << endl;
	}

	strcpy(buffer, "/bayonne/");
	strcat(buffer, group);
	Keydata policy(buffer);
	for(;;)
	{
		cf.getline(buffer, sizeof(buffer) - 1);
		if(cf.eof())
			break;
		sp = buffer;
		while(*sp == ' ' || *sp == '\t')
			++sp;
		if(*sp == '[')
			break;
	}

	while(argv[optind])
	{
		sp = strchr(argv[optind], '=');
		if(*sp)
			*(sp++) = 0;
		else
			sp = "";
		policy.setValue(argv[optind], sp);
		++optind;
	}

		
	of << "[" << group << "]" << endl;
	policy.getIndex(keys, 128);
	while(*key)
	{
		of << *key << " = " << policy.getLast(*key) << endl;
		++key;
	}

	of << endl;
	of << buffer << endl;

	for(;;)
	{
		cf.getline(buffer, sizeof(buffer) - 1);
		if(cf.eof())
			break;
		of << buffer << endl;
	}
	cf.sync();
	of.sync();
	of.seekp(0);
	cf.close();
	cf.open(conf, ios::out | ios::trunc);
	for(;;)
	{
		of.getline(conf, sizeof(conf) - 1);
		if(of.eof())
			break;
		cf << conf << endl;
	}
	cf.sync();
	cf.close();
	of.close();
}





























