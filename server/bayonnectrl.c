/* Copyright (C) 2000-2001 Open Source Telecom Corporation.
 *  
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software 
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <cc++2/cc++/config.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>

enum
{
	EXIT_OK = 0,
	EXIT_NOSERVER,
	EXIT_ERROR,
	EXIT_TIMEOUT
};

static RETSIGTYPE success(int signo)
{
	exit(EXIT_OK);
}

static RETSIGTYPE failure(int signo)
{
	exit(EXIT_ERROR);
}

int main(int argc, char **argv)
{
	char home[256];
	const char *ctrl = (const char *)getenv("BAYONNE_CONTROL");
	unsigned len = 0;
	int count = 1;
	int fd;

	if(argc < 2)
	{
		fprintf(stderr, "use: bayonnectrl string...\n");
		exit(-1);
	}

	if(ctrl)
	{
		snprintf(home, sizeof(home), "%s.ctrl", ctrl);
		ctrl = home;
	}
	else
		ctrl = "/var/run/bayonne/bayonne.ctrl";

	fd = open(ctrl, O_WRONLY | O_NONBLOCK);
	if(fd < 0)
	{
		snprintf(home, sizeof(home), "%s/.bayonne.ctrl", getenv("HOME"));
		fd = open(home, O_WRONLY | O_NONBLOCK);
	}
	if(fd < 0)
		exit(EXIT_NOSERVER);

	signal(SIGUSR1, &success);
	signal(SIGUSR2, &failure);
	
	snprintf(home, sizeof(home), "%d", getpid());
	len = strlen(home);
	while(count < argc && len < (sizeof(home) - 5))
	{
		snprintf(home + len, sizeof(home) - len, " %s", argv[count++]);
		len = strlen(home);
	}
	home[len++] = '\n';
	home[len] = 0;
	write(fd, home, strlen(home));
	sleep(2);
	exit(EXIT_TIMEOUT);
}	

