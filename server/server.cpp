// Copyright (C) 2000-2001 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifdef	__FreeBSD__
#define	getopt(a, b, c) getopt()
#include <unistd.h>
#include <sys/param.h>
#include <sys/mount.h>
#undef getopt
#endif

#include <cc++/config.h>
#include <cc++/process.h>
#include <cc++/url.h>
#ifdef	linux
#include <sys/vfs.h>
#endif
#include <sys/wait.h>
#include <sys/utsname.h>
#include <netinet/tcp.h>
#include <sys/un.h>
#include <stdarg.h>
#include <getopt.h>
#include <cerrno>
#include "server.h"
#include <iomanip>

#ifdef	DLL_SERVER
#define	PLUGIN_SUBDIR	".libs/"
#else
#define	PLUGIN_SUBDIR	""
#endif

#ifdef	_POSIX_MEMLOCK
#include <sys/mman.h>
#endif

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

ThreadLock cachelock;
bool running = false;
unsigned numbering = 0;
int mainpid;
int tgipipe[2];
bool restart_server = false;
bool upflag = false;
char service[65] = "";
char df_used[4] = "100";
char df_free[4] = "0";
static Thread *mainthread = NULL;
static int *tgipids = NULL;
static Resolver *resolver = NULL;
static char **restart_argv;
static char **cmds;
static char *inits[128];
static unsigned icount = 0;
aaScript *aascript = NULL;

static int pidfile(char *fpath)
{
	int fd;
	pid_t pid;
	time_t now;
	struct stat ino;
	int len, status;
	int mask;
	char buffer[65];

	for(;;)
	{
		fd = open(fpath, O_WRONLY | O_CREAT | O_EXCL, 0660);
		if(fd > 0)
		{
			pid = getpid();
			snprintf(buffer, sizeof(buffer), "%d\n", pid);
			write(fd, buffer, strlen(buffer));
			close(fd);
			return 0;
		}
		if(fd < 0 && errno != EEXIST)
			return -1;

		fd = open(fpath, O_RDONLY);
		if(fd < 0)
		{
			if(errno == ENOENT)
				continue;
			return -1;
		}
		sleep(2);
		status = read(fd, buffer, sizeof(buffer) - 1);
		if(status < 1)
		{
			close(fd);
			continue;
		}

		buffer[status] = 0;
		pid = atoi(buffer);
		if(pid)
		{
			if(pid == getpid())
			{
				status = -1;
				errno = 0;
			}
			else
				status = kill(pid, 0);

			if(!status || (errno == EPERM))
			{
				close(fd);
				return pid;
			}
		}
		close(fd);
		unlink(fpath);
	}
}

#ifndef	HAVE_SETENV
static void setenv(char *name, const char *value, int overwrite)
{
	char strbuf[256];

	snprintf(strbuf, sizeof(strbuf), "%s=%s", name, value);
	putenv(strbuf);
}
#endif

static void purge(const char *path)
{
	if(!isDir(path))
		return;

	char fullpath[256];
	char *ex[4];
	int pid;
	Dir dir(path);
	const char *name;

	while(NULL != (name = dir.getName()))
	{
		if(!stricmp(name, "."))
			continue;

		if(!stricmp(name, ".."))
			continue;

		pid = fork();
		if(pid < 0)
			return;

		if(pid)
		{
			waitpid(pid, NULL, 0);
			continue;
		}

		snprintf(fullpath, sizeof(fullpath), "%s/%s", path, name);
		ex[0] = "rm";
		ex[1] = "-rf";
		ex[2] = fullpath;
		ex[3] = NULL;

		exit(execvp("rm", ex));
	}
}
		
static void rt(void)
{
	int min, max;
	int pages = keythreads.getPages();
	int pri = keythreads.getPriority();
	int policy = keythreads.getPolicy();
	char memory[1024 * pages];

	memset(memory, sizeof(memory), 0);

#ifdef	_POSIX_PRIORITY_SCHEDULING
	struct sched_param p;

	sched_getparam(0, &p);
	pri += p.sched_priority;
	if(!policy)
		policy = sched_getscheduler(0);

	min = sched_get_priority_min(policy);
	max = sched_get_priority_max(policy);
	if(pri < min)
		pri = min;
	if(pri > max)
		pri = max;
	p.sched_priority = pri;
	sched_setscheduler(getpid(), policy, &p);
#else
	nice(-pri);
#endif

#ifdef	MCI_CURRENT
	if(pages)
		mlockall(MCI_CURRENT | MCI_FUTURE);
#endif
}

#ifdef	HAVE_TGI

static RETSIGTYPE tgiusr1(int signo)
{
}

static void tgi(int cfd)
{
	int argc;
	char *argv[65];
	char *user;
	char buffer[PIPE_BUF / 2];
	int vpid, pool, stat;
	int count = keythreads.getGateways();
	tgipids = new int[count];
	sigset_t sigs;
	struct sigaction act;
	tgicmd_t cmd;
	TGI *tgi;
	int status;
	char *cp;
	int len, fd, ifd = -1;
	char **arg;
	const char *prefix;
	char fifopath[32];

	if(canModify(keypaths.getRunfiles()))
	{
		strcpy(buffer, keypaths.getRunfiles());
		strcat(buffer, "/bayonne");
		setenv("SERVER_CONTROL", buffer, 1);
		strcpy(buffer, keypaths.getRunfiles());
		strcat(buffer, "/bayonne.ctrl");
	}
	else
	{
		sprintf(buffer, "%s/.bayonne", getenv("HOME"));
		setenv("SERVER_CONTROL", buffer, 1);
		sprintf(buffer, "%s/.bayonne.ctrl", getenv("HOME"));
	}

	pipe(tgipipe);

	for(pool = 0; pool < count; ++pool)
	{
#ifdef	__FreeBSD__
		tgipids[pool] = vfork();
#else
		tgipids[pool] = fork();
#endif
		if(!tgipids[pool])
			break;
	}
	
	if(pool == count)
	{
		close(tgipipe[0]);
		return;
	}

	close(tgipipe[1]);

	arg = cmds;
	while(*arg)
	{	
		len = strlen(*arg);
		memset(*arg, 0, len);
		++arg;
	}
	strcpy(cmds[0], "[tgi]");
	keyserver.setGid();
	keyserver.setUid();
	sigemptyset(&sigs);
	sigaddset(&sigs, SIGINT);
	signal(SIGINT, SIG_DFL);
	sigprocmask(SIG_UNBLOCK, &sigs, NULL);

	act.sa_handler = &tgiusr1;
	act.sa_flags = SA_RESTART;
	sigemptyset(&act.sa_mask);
	sigaction(SIGUSR1, &act, NULL);	

	slog(Slog::levelNotice) << "tgi: initialized; uid=" << getuid() << " pid=" << getpid() << endl;
	nice(-keythreads.priGateway());

	while(cfd > -1 && cfd < 3)
		cfd = dup(cfd);
	if(cfd < 0)
		slog(Slog::levelError) << "tgi: fifo failed; buffer=" << buffer << "; failure=" << strerror(errno) << endl;
	else
		slog(Slog::levelDebug) << "tgi: buffer=" << buffer << "; cfd=" << cfd << "; time=" << count * 10 << endl;

	while(read(tgipipe[0], &cmd, sizeof(cmd)) == sizeof(cmd))
	{
		ifd = -1;
		argc = 0;
		slog(Slog::levelDebug) << "tgi: cmd=" << cmd.cmd << endl;
		if(cmd.mode == TGI_EXEC_IMMEDIATE)
			tgi = getInterp(cmd.cmd);
		else
			tgi = NULL;
		if(tgi)
		{
			status = tgi->parse(cfd, cmd.port, cmd.cmd);
			continue;
		}
		if(cmd.mode == TGI_EXEC_POST)
		{
			snprintf(fifopath, sizeof(fifopath),
				"/tmp/.tgi%d", vpid);
			remove(fifopath);
			mkfifo(fifopath, 0660);
			ifd = ::open(fifopath, O_RDWR);
			while(ifd > -1 && ifd < 3)
				ifd = dup(ifd);
			if(ifd < 0)
			{
				slog(Slog::levelError) << "tgi: post error" << endl;
				continue;
			}
		}	
		vpid = vfork();
		if(vpid)
		{
			if(ifd > -1)
				close(ifd);

			if(cmd.mode == TGI_EXEC_POST)
			{
                                sprintf(buffer, "post %s %d %d\n",
                                        cmd.port, vpid, cmd.seq);
                                write(cfd, buffer, strlen(buffer));
                                if(keythreads.getAudit())
					slog(Slog::levelDebug) << "thread: gateway posting on pid " << vpid << endl;
			}
			else if(cmd.mode != TGI_EXEC_DETACH)
			{
				sprintf(buffer, "wait %s %d %d\n", 
					cmd.port, vpid, cmd.seq);
				write(cfd, buffer, strlen(buffer));
				if(keythreads.getAudit())
slog(Slog::levelDebug) << "thread: gateway waiting on pid " << vpid << endl;
			}
			else
			{
				if(keythreads.getAudit())
					slog(Slog::levelDebug) << "thread: gateway detached pid " << vpid << endl;
			}

			if(cmd.mode != TGI_EXEC_DETACH)
			{
#ifdef	__FreeBSD__
				wait4(vpid, &stat, 0, NULL);
#else
				waitpid(vpid, &stat, 0);
#endif
				if(keythreads.getAudit())
					slog(Slog::levelDebug) << "thread: gateway exiting pid " << vpid << endl;
				sprintf(buffer, "exit %s %d %d\n", cmd.port, WEXITSTATUS(stat), cmd.seq);
				write(cfd, buffer, strlen(buffer));
			}
			else
#ifdef	__FreeBSD__
				wait4(vpid, &stat, 0, NULL);
#else
				waitpid(vpid, &stat, 0);
#endif
			continue;
		}

		switch(cmd.mode)
		{
		case TGI_EXEC_POST:
			setenv("SERVER_METHOD", "post", 1);
			break;
		default:
			setenv("SERVER_METHOD", "get", 1);
		}

		if(cmd.mode == TGI_EXEC_DETACH)
		{
#ifndef	COMMON_PROCESS_ATTACH
			::close(0);
                	::close(1);
               		::close(2);
#endif
                	Process::detach();
#ifndef	COMMON_PROCESS_ATTACH
                	::open("/dev/null", O_RDWR);
                	::open("/dev/null", O_RDWR);
                	::open("/dev/null", O_RDWR);
#endif
			strcpy(cmds[0], "-detach");
		}
		else
			sprintf(cmds[0], "tgi/%03d", cmd.port);

		user = NULL;
		//sprintf(buffer, "%d", cmd.port);
		setenv("PORT_NUMBER", cmd.port, 1);
		argv[argc++] = strtok(cmd.cmd, " \t");
		while(NULL != (argv[argc] = strtok(NULL, " \t")))
		{
			/* Used for standard TGI execution */

			if(!strnicmp(argv[argc], "user=", 5))
			{
				user = argv[argc] + 5;
				setenv("PORT_USER", user, 1);
				continue;
			}

			if(!strnicmp(argv[argc], "digits=", 7))
			{
				setenv("PORT_DIGITS", argv[argc] + 7, 1);
				continue;
			}

			if(!strnicmp(argv[argc], "dnid=", 5))
			{
				setenv("PORT_DNID", argv[argc] + 5, 1);
				continue;
			}

			if(!strnicmp(argv[argc], "clid=", 5))
			{
				setenv("PORT_CLID", argv[argc] + 5, 1);
				continue;
			}

			if(!strnicmp(argv[argc], "query=", 6))
			{
				setenv("PORT_QUERY", argv[argc] + 6, 1);
				continue;
			}

			// Added for TTS audio format selection

			if(!strnicmp(argv[argc], "format=", 6))
			{
				setenv("TTS_FORMAT", argv[argc] + 6, 1);
				continue;
			}

			// Added for TTS audio file type and ASR filename

			if(!strnicmp(argv[argc], "audio=", 6))
			{
				if(cmd.mode == TGI_EXEC_AUDIO)
				{
					remove(argv[argc] + 6);
					setenv("TTS_AUDIO", argv[argc] + 6, 1);
				}
				else
					setenv("ASR_AUDIO", argv[argc] + 6, 1);
				setenv("TEMP_AUDIO", argv[argc] + 6, 1);
				continue;
			}

			// Added for TTS/ASR language specification

			if(!strnicmp(argv[argc], "language=", 9))
			{
				setenv("TTS_LANGUAGE", argv[argc] + 9, 1); 
				setenv("ASR_LANGUAGE", argv[argc] + 9, 1);
				continue;
			}

			// Added for TTS/ASR voice domain selection

			if(!strnicmp(argv[argc], "voice=", 6))
			{
				setenv("TTS_VOICE", argv[argc] + 6, 1);
				setenv("ASR_VOICE", argv[argc] + 6, 1);
				continue;
			}

			// Added for domain vocabulary selection

			if(!strnicmp(argv[argc], "domain=", 7))
			{
				setenv("ASR_DOMAIN", argv[argc] + 7, 1);
				continue;
			}

			// Added for ASR event trapping

			if(!strnicmp(argv[argc], "asrevt=", 7))
			{
				setenv("ASR_EVENTS", argv[argc] + 7, 1);
				continue;
			}

			// Added for passing TTS phrases

			if(!strnicmp(argv[argc], "phrase=", 7))
			{
				while(NULL != (cp = strchr(argv[argc], '+')))
					*cp = ' ';
				while(NULL != (cp = strchr(argv[argc], ',')))
					*cp = ' ';
				setenv("TTS_PHRASE", argv[argc] + 7, 1);
				continue;
			}

			// Added for passing TTS source if file based

			if(!strnicmp(argv[argc], "source=", 7))
			{
				setenv("TTS_SOURCE", argv[argc] + 7, 1);
				continue;
			} 

			// Added for url helpers and such
			if(!strnicmp(argv[argc], "href=", 5))
			{
				setenv("URL_HREF", argv[argc] + 5, 1);
				continue;
			}

			if(!strnicmp(argv[argc], "base=", 5))
			{
				setenv("URL_BASE", argv[argc] + 5, 1);
				continue;
			}

			if(!strnicmp(argv[argc], "url=", 4))
			{
				setenv("URL_SOURCE", argv[argc] + 4, 1);
				continue;
			}

			// Added for passing TTS cache path

			if(!strnicmp(argv[argc], "cache=", 6))
			{
				setenv("TTS_CACHE", argv[argc] + 6, 1);
				continue;
			}

			++argc;
		}
		argv[argc] = NULL;
//		slog.close();
		close(0);
		close(1);
//		close(2);
		if(cmd.mode == TGI_EXEC_SHELL)
		{
			snprintf(fifopath, sizeof(fifopath), "/tmp/.tgi%d", cmd.port);
			if(open(fifopath, O_RDWR) != 0)
			{
				slog(Slog::levelError) << "tgi: input error; " << buffer << endl;
				exit(-1);
			}
			unlink(fifopath);	// early clean up
		}
		else
			open("/dev/null", O_RDWR);
		if(cfd > 1)
		{
			dup2(cfd, 1);
			close(cfd);
		}

		if(ifd > 1)
		{
			dup2(ifd, 0);
			close(ifd);
		}

		prefix = getenv("SERVER_LIBEXEC");

		snprintf(buffer, sizeof(buffer), "%s/%s", prefix, *argv);
		getInterp(buffer, argv);

		prefix = getenv("SERVER_ALTEXEC");
		if(prefix)
		{
			snprintf(buffer, sizeof(buffer), "%s/%s", prefix, *argv);
			getInterp(buffer, argv);
		}

		slog(Slog::levelDebug) << "tgi: exec " << buffer << endl;
		execvp(buffer, argv);
		slog(Slog::levelError) << "tgi: exec failed; " << buffer << endl;
		exit(-1);
	}
	exit(SIGINT);
}

/*
static void tgisignal(void)
{
	int count = keythreads.getGateways();
	int pid;

	if(!tgipids)
		return;

	for(pid = 0; pid < count; ++pid)
		kill(tgipids[pid], SIGUSR1);
}
*/

#endif
		
static RETSIGTYPE final(int sig)
{
	int count = keythreads.getGateways();
	int i;
	const char *cp;

	if(debug)
		if(debug->debugFinal(sig))
			return;

	if(sig)
		errlog("failed", "Bayonne exiting; reason=%d", sig);
	else
		errlog("notice", "Bayonne exiting; normal termination");

	if(getpid() != mainpid)
	{
		kill(mainpid, sig);
#ifdef	COMMON_THREAD_SLEEP
		Thread::sleep(~0);
#else
		ccxx_sleep(~0);
#endif
	}
	signal(SIGINT, SIG_IGN);
	signal(SIGABRT, SIG_IGN);
	signal(SIGTERM, SIG_IGN);
	signal(SIGQUIT, SIG_IGN);

#ifdef	NODE_SERVICES
	network.stop();
#endif
#ifdef	SCHEDULER_SERVICES
	scheduler.stop();
#endif
	stopServers();

	if(resolver)
		delete resolver;

	if(Driver::drvFirst)
	{
		Driver *drv = Driver::drvFirst;
		while(drv)
		{
			drv->stop();
			drv = drv->drvNext;
		}
	}

	if(sig)
		slog(Slog::levelWarning) << "exiting: reason=" << sig << endl;
	else
		slog(Slog::levelNotice) << "normal shutdown" << endl;

	if(tgipids)
	{
		for(i = 0; i < count; ++i)
		{
			kill(tgipids[i], SIGINT);
			waitpid(tgipids[i], NULL, 0);
		}
		tgipids = NULL;
	}

	if(restart_server)
		execvp(*restart_argv, restart_argv);

	cp = keypaths.getLast("pidfile");
	if(cp)
		remove(cp);

	cp = keypaths.getLast("ctrlfile");
	if(cp)
		remove(cp);

	cp = keypaths.getLast("pktfile");
	if(cp)
		remove(cp);

	cp = keypaths.getLast("nodefile");
	if(cp)
		remove(cp);

	purge(keypaths.getLast("tmpfs"));
	purge(keypaths.getLast("tmp"));
	exit(sig);
}

static void initial(int argc, char **argv, int cfd)
{
	static struct option long_options[] = {
		{"background", 0, 0, 'D'},
		{"foreground", 0, 0, 'F'},
		{"daemon", 0, 0, 'D'},
		{"help", 0, 0, 'h'},
		{"priority", 1, 0, 'p'},
		{"port", 1, 0, 'P'},
		{"driver", 1, 0, 'd'},
		{"node", 1, 0, 'n'},
		{"test", 0, 0, 't'},
		{"trace", 0, 0, 'T'},
		{"thread", 0, 0, 'A'},
		{"version", 0, 0, 'V'},
		{"voice", 1, 0, 'v'},
		{"language", 1, 0, 'l'},
		{"gui", 0, 0, 'G'},
		{"display", 1, 0, 'X'},
		{"startup", 1, 0, 'S'},
		{"debug", 0, 0, 'x'},
		{"demo", 0, 0, 'c'},
		{"init", 0, 0, 'I'},
		{"alt", 1, 0, 'a'},
		{"groups", 0, 0, 'g'},
		{"tts", 1, 0, 'Y'},
		{"prefix", 1, 0, 'z'},
		{"loglevel", 1, 0, 'L'},
		{0, 0, 0, 0}};

	static bool daemon = false;
	static bool usage = false;
	static bool gui = false;
	static bool pcount = 0;
	static bool mcount = 0;
	static unsigned plen = 0;

	TrunkGroup *grp;
	struct utsname uts;
	TrunkGroup *policy;
	restart_argv = argv;
	char *envDriver = getenv("BAYONNE_DRIVER");
	char *envDialing = getenv("BAYONNE_DIALPLAN");
	Mixer *mixer;
	fstream mix;
	char prefix[256], libpath[256];
	const char *cp;
	char *pp, *tok;
	sigset_t sigs;
	int opt, opt_index;
	unsigned ports, id;
	bool test = false;
	bool schedule = true;
	unsigned count;
	ofstream php;
	const char *embed, *name;
	bool gdump = false;
	bool pref = false;
	const char *alt = NULL;
	char *sp = NULL;
	Driver *drv;

	cmds = argv;
	mainpid = getpid();
	mainthread = getThread();
	sigemptyset(&sigs);
	sigaddset(&sigs, SIGTERM);
	sigaddset(&sigs, SIGQUIT);
	sigaddset(&sigs, SIGINT);
	pthread_sigmask(SIG_BLOCK, &sigs, NULL);
	slog.level(Slog::levelNotice);

	strncpy(prefix, argv[0], sizeof(prefix));
	pp = strrchr(prefix, '/');
        if(pp)
        {
        	*pp = 0;
                chdir(prefix);
        }

	uname(&uts);

	getcwd(prefix, sizeof(prefix) - 1);
	pp = strrchr(prefix, '/');

	snprintf(libpath, sizeof(libpath), "%s/%s", 
		keypaths.getLast("libpath"), VERPATH);
	keypaths.setValue("modlibpath", libpath);

	snprintf(libpath, sizeof(libpath), "%s/%s/shell.bin",
		keypaths.getLast("libpath"), VERPATH);
	keypaths.setValue("shell", libpath);

	if(envDriver)
		modules.setValue("driver", envDriver);

	if(envDialing)
		keyserver.setValue("dialplan", envDialing);

	while(EOF != (opt = getopt_long(argc, argv, "z:123cthVxP:GTDFX:d:p:d:n:a:AS:s:v:l:gY:I", long_options, &opt_index)))
		switch(opt)
		{
		case 'z':
			sp = optarg;
			break;
		case '1':
			numbering = 1;
			break;
		case '2':
			numbering = 2;
			break;
		case '3':
			numbering = 3;
			break;
		case 'x':
			slog.level(Slog::levelDebug);
			break;
		case 'I':
			pref = true;
			break;
		case 'g':
			gdump = true;
			break;
		case 'v':
			setenv("BAYONNE_VOICE", optarg, 1);
			break;
		case 'Y':
			modules.setValue("tts", optarg);
			break;
		case 'l':
			setenv("BAYONNE_LANGUAGE", optarg, 1);
			break;
		case 'X':
			setenv("DISPLAY", optarg, 1);
		case 'G':
			gui = true;
			modules.setValue("debug", "gui");
			break;		
		case 'P':
			modules.setValue("debug", "tcpmon");
			setenv("TCPMON", optarg, 1);
			break;	
		case 'S':
			inits[icount++] = optarg;
			break;
		case 'V':
			cout << VERPATH << endl;
			exit(0);
		case 'A':
			keythreads.setValue("audit", "1");
			slog.level(Slog::levelDebug);
			break;
		case 'T':
			daemon = false;
			slog.level(Slog::levelDebug);
			modules.setValue("debug", "trace");
			break;
		case 'a':
			alt = optarg;
			break;
		case 't':
			schedule = false;
			test = true;
			daemon = false;
			slog.level(Slog::levelDebug);
#if defined(have_montecarlo_h) || defined(HAVE_MONTECARLO_H)
			strcpy(pp, "/drivers/pika/" PLUGIN_SUBDIR "pika.ivr");
#elif defined(HAVE_DIALOGIC_SDK)
			strcpy(pp, "/drivers/dialogic/" PLUGIN_SUBDIR "dialogic.ivr");
#elif defined(HAVE_VPBAPI_H)
			strcpy(pp, "/drivers/vpb/" PLUGIN_SUBDIR "vpb.ivr");
#elif defined(HAVE_CAPI20_H)
			strcpy(pp, "/drivers/capi20/" PLUGIN_SUBDIR "capi20.ivr");
#elif defined(HAVE_LINUX_TELEPHONY_H)
			strcpy(pp, "/drivers/phonedev/" PLUGIN_SUBDIR "phonedev.ivr");
#else
			strcpy(pp, "/drivers/dummy/" PLUGIN_SUBDIR "dummy.ivr");
#endif
			if(envDriver)
				sprintf(pp, "/drivers/%s/%s%s.ivr",
					envDriver, PLUGIN_SUBDIR, envDriver);
			modules.setValue("driver", prefix);
			strcpy(pp, "/modules/translators/" PLUGIN_SUBDIR "english.tts");
			modules.setValue("languages", prefix);
			modules.setValue("switch", "");
#ifdef	HAVE_FLITE
			strcpy(pp, "/modules/flite/" PLUGIN_SUBDIR "flite.tts");
			modules.setValue("tts", prefix);
#endif

#if defined(HAVE_POSTGRES) || defined(HAVE_PGSQL_POSTGRES)
			strcpy(pp, "/modules/postgres/" PLUGIN_SUBDIR "postgres.sql");
			modules.setValue("sql", prefix);
#endif

			strcpy(pp, "/server/shell.bin");
			keypaths.setValue("shell", prefix);

//			strcpy(pp, "/modules/perl/" PLUGIN_SUBDIR "perl.tgi");
//			modules.setValue("tgi", prefix);
			strcpy(pp, "/modules/protocols/"  PLUGIN_SUBDIR "files.mod");
			modules.setValue("modules", prefix);
			modules.setValue("auditing", "");
			if(gui)
				strcpy(pp, "/modules/gui/" PLUGIN_SUBDIR "gui.dbg");
			else
				strcpy(pp, "/server/" PLUGIN_SUBDIR "test.dbg");
			modules.setValue("debug", prefix);
			keypaths.setValue("cache", "cache");
			keypaths.setValue("spool", "spool");
			strcpy(pp, "/data/script");
			keypaths.setValue("scripts", prefix);
			strcpy(pp, "/data");
			keypaths.setValue("prompts", prefix);
			if(alt)
				cp = strrchr(alt, '/');
			else
				cp = "/alt";
			if(!cp)
				cp = alt;
			else
				++cp;
			sprintf(pp, "/data/%s", cp);
			alt = NULL;
			keypaths.setValue("altlibexec", prefix);
			keypaths.setValue("altprompts", prefix);
			keypaths.setValue("altscripts", prefix);
			keypaths.setValue("datafiles", "../var");
			keypaths.setValue("logpath", "../var/log");
			break;
		case 'n':
			keyserver.setValue("node", optarg);
			break;
		case 'c':
			optarg = "dummy";
			daemon = false;
		case 'd':
			if(*optarg != '/')
				envDriver = optarg;

			if(test && *optarg != '/')
			{
				sprintf(pp, "/drivers/%s/%s%s.ivr",
					optarg, PLUGIN_SUBDIR, optarg);
				optarg = prefix;
			} 
			modules.setValue("driver", optarg);
			break;
		case 'p':
			keythreads.setValue("priority", optarg);
			break;
		case 'D':
			daemon = true;
			break;
		case 'F':
			daemon = false;
			break;
		case 'L':
                        if (!strcmp(optarg, "debug")) {
                          slog.level(Slog::levelDebug);
                        } else if (!strcmp(optarg, "info")) {
			  slog.level(Slog::levelInfo);
                        } else if (!strcmp(optarg, "notice")) {
			  slog.level(Slog::levelNotice);
			} else if (!strcmp(optarg, "warning")) {
			  slog.level(Slog::levelWarning);
			} else if (!strcmp(optarg, "error")) {
			  slog.level(Slog::levelError);
			} else if (!strcmp(optarg, "critical")) {
			  slog.level(Slog::levelCritical);
			} else if (!strcmp(optarg, "alert")) {
			  slog.level(Slog::levelAlert);
			} else if (!strcmp(optarg, "emergency")) {
			  slog.level(Slog::levelEmergency);
			} else {
			  cout << "Unknown log level. Must be one of debug, " <<
			    "info, notice, warning, error, critical, alert and emergency" << endl;
			  exit(-1);
			}
			break;
		default:
		case 'h':
			usage = true;
		}

	if(optind < argc)
	{
		if(test && !gui)
		{
			strcpy(pp, "/modules/auditing/trace.dbg");
			modules.setValue("debug", prefix);
		}
		keyserver.setValue("default", argv[optind++]);
		schedule = false;
	}

	if(usage || optind < argc)
	{
		clog << "use: bayonne [-options] [defscript]" << endl;
		exit(-1);
	}

	if(alt)
	{
		if(*alt != '/')
		{
			cp = keypaths.getLast("altprefix");
			if(cp)
				snprintf(prefix, sizeof(prefix), "%s/%s", cp, alt);
			else
			{
				snprintf(prefix, sizeof(prefix) - sizeof(alt), "%s", keypaths.getLast("scripts"));
				cp = strrchr(prefix, '/');
				strcpy((char *)++cp, alt);
			}
			alt = prefix;
		}
		keypaths.setValue("altscripts", alt);
		keypaths.setValue("altprompts", alt);
		keypaths.setValue("altlibexec", alt);
	}

	endKeydata();
	if(sp)
		keypaths.setPrefix(sp);

	keyserver.setGid();

	if(!getuid())
	{
		umask(003);
		if(!isDir(keypaths.getRunfiles()))
			mkdir(keypaths.getRunfiles(), 0770);
	}
	mkdir(keypaths.getLogpath(), 0750);
	mkdir(keypaths.getDatafiles(), 0750);
	chdir(keypaths.getDatafiles());
	purge("temp");
	purge(keypaths.getSpool());
	purge(keypaths.getCache());
	purge(keypaths.getLast("tmpfs"));
	purge(keypaths.getLast("tmp"));

	mkdir(keypaths.getLast("tmpfs"), 0770);
	mkdir(keypaths.getLast("tmp"), 0770);
	mkdir("temp", 0770);
	mkdir(keypaths.getCache(), 0770);
	mkdir("users", 0770);
	symlink(keypaths.getCache(), "cache");
	mkdir(keypaths.getSpool(), 0770);
	symlink(keypaths.getSpool(), "spool");
	mkdir("music", 0770);

	if(daemon)
	{
#ifndef	COMMON_PROCESS_ATTACH
		::close(0);
		::close(1);
		::close(2);
#endif
		Process::detach();
#ifndef	COMMON_PROCESS_ATTACH
		::open("/dev/null", O_RDWR);
		::open("/dev/null", O_RDWR);
		::open("/dev/null", O_RDWR);
#endif
		mainthread = getThread();
		mainpid = getpid();
		slog.open("bayonne", Slog::classDaemon);
		slog(Slog::levelNotice) << "daemon mode started" << endl;
	}
	else if(getppid() == 1)
	{
		close(0);
		close(1);
		close(2);
		open("/dev/null", O_RDWR);
		open("/dev/null", O_RDWR);
		open("/dev/null", O_RDWR);
		slog.open("bayonne", Slog::classDaemon);
		slog(Slog::levelNotice) << "daemon init started" << endl;
	}

	if(canModify(keypaths.getRunfiles()))
		snprintf(prefix, sizeof(prefix), "%s/bayonne.pid",
			keypaths.getRunfiles());
	else
		snprintf(prefix, sizeof(prefix), "%s/.bayonne.pid",
			Process::getEnv("HOME"));

	keypaths.setValue("pidfile", prefix);

	switch(pidfile(prefix))
	{
	case -1:
		slog(Slog::levelWarning) << "server: cannot create pidfile " << prefix << endl;
	case 0:
		break;
	default:
		slog(Slog::levelCritical) << "server: another instance running; cannot continue" << endl;
		final(-1);
	}

	setenv("SERVER_SHELL", keypaths.getLast("shell"), 1);
	setenv("SERVER_PLATFORM", modules.getDriverName(), 1);
	setenv("SERVER_LIBEXEC", keypaths.getLibexec(), 1);
	if(keypaths.getLast("altlibexec"))
		setenv("SERVER_ALTEXEC", keypaths.getLast("altlibexec"), 1);
	setenv("SERVER_SOFTWARE", "bayonne", 1);
	setenv("SERVER_PROTOCOL", "3.0", 1);
	setenv("SERVER_VERSION", VERPATH, 1);
	setenv("SERVER_TOKEN", keyserver.getToken(), 1);

	strcpy(prefix, keyserver.getPrefix());
	strcat(prefix, "/bayonne");
	if(isDir(prefix))
	{
		strcat(prefix, ":");
		strcat(prefix, keypaths.getTgipath());
		setenv("PATH", prefix, 1);
	}
	else
		setenv("PATH", keypaths.getTgipath(), 1);

	slog(Slog::levelInfo) << "SERVER VERSION " << VERPATH << "; ";
	slog() << uts.machine << " ";
	slog() << uts.sysname << " " << uts.release << endl;
	slog(Slog::levelInfo) << "TGI VERSION 3.0";
	slog() << "; driver=" << modules.getDriverName();
	slog() << "; prefix=" << prefix;
	slog() << "; etc=" << keypaths.getLast("etc") << endl;

	slog(Slog::levelDebug) << "Loading TGI modules..." << endl;
	try
	{
		modules.loadTGI();
	}
	catch(DSO *dso)
	{
		slog(Slog::levelCritical) << dso->getError() << endl;
		final(-1);
	}
#ifdef	HAVE_TGI
	tgi(cfd);
	sleep(1);
#endif
	rt();

#ifdef	SCRIPT_IF_OVERRIDE
	addConditional("voice", &Trunk::hasVoice);
	addConditional("alt", &Trunk::hasAltVoice);
	addConditional("altvoice", &Trunk::hasAltVoice);
	addConditional("sys", &Trunk::hasSysVoice);
	addConditional("sysvoice", &Trunk::hasSysVoice);
	addConditional("sysprompt", &Trunk::hasSysPrompt);
	addConditional("varprompt", &Trunk::hasVarPrompt);
	addConditional("prompt", &Trunk::hasVarPrompt);
	addConditional("app", &Trunk::hasAppVoice);
	addConditional("appvoice", &Trunk::hasAppVoice);
	addConditional("group", &Trunk::hasGroup);
	addConditional("policy", &Trunk::hasGroup);
	addConditional("plugin", &Trunk::hasPlugin);
	addConditional("driver", &Trunk::hasDriver);
	addConditional("node", &Trunk::isNode);
	addConditional("service", &Trunk::isService);
	addConditional("schedule", &Trunk::isSchedule);
	addConditional("dtmf", &Trunk::ifDTMF);
	addConditional("feature", &Trunk::ifFeature);
	addConditional("ext", &Trunk::isExtension);
	addConditional("extension", &Trunk::isExtension);
	addConditional("station", &Trunk::isStationPort);
	addConditional("virtual", &Trunk::isVirtual);
	addConditional("user", &Trunk::isActiveUser);
	addConditional("dnd", &Trunk::isDnd);
	addConditional("hunt", &Trunk::isHunt);
	addConditional("running", &Trunk::ifRunning);
	addConditional("ringing", &Trunk::ifRinging);
	addConditional("port", &Trunk::ifPort);
#endif

	try
	{
		slog(Slog::levelDebug) << "Loading DSO plugin images..." << endl;
		modules.loadManagers();
		if(!test)
			modules.loadExtensions();
		
		modules.loadDebug();
		modules.loadMonitor();
		aascript = new aaScript();
		modules.loadDriver();
		modules.loadSwitch();
		modules.loadTTS();
		modules.loadSQL();
		modules.loadModules();
		modules.loadPreload();
		modules.loadCodecs();
		modules.loadTranslators();
		keyserver.loadGroups(test);

		new aaImage(aascript);
	}
	catch(Driver *drvr)
	{
		slog(Slog::levelCritical) << "multiple drivers loaded or driver failure" << endl;
		final(-1);
	}
	catch(Socket *sock)
	{
		slog(Slog::levelCritical) << "socket interface binding failure" << endl;
		final(-1);
	}
	catch(Dir *dir)
	{
		slog(Slog::levelCritical) << keypaths.getScriptFiles(); 
		slog() << ": no script directory" << endl;
		final(-1);
	}
	catch(DSO *dso)
	{
		slog(Slog::levelCritical) << dso->getError() << endl;
		final(-1);
	}
	catch(InetAddress *in)
	{
		slog(Slog::levelCritical) << "protocol resolver failed" << endl;
		final(-1);
	}
	catch(Thread *)
	{
		slog(Slog::levelCritical) << "service failure" << endl;
		final(-1);
	}
	catch(...)
	{
		slog(Slog::levelCritical) << "unknown failure" << endl;
		final(-1);
	}

	if(!Driver::drvFirst)
	{
		slog(Slog::levelCritical) << "no driver loaded" << endl;
		final(-1);
	}

	if(gdump)
	{
		while(drv)
		{
			for(id = 0; id < drv->getTrunkCount(); ++id)
			{
				grp = drv->getTrunkGroup(id);
				if(!grp)
					continue;
				cout << "port " << id << " group=" << grp->getLast("name") << endl;
			}
			for(id = 0; id < MAX_SPANS; ++id)
			{
				grp = drv->getSpanGroup(id);
				if(!grp)
					continue;
				cout << "span " << id << " group=" << grp->getLast("name") << endl;
			}
			for(id = 0; id < MAX_CARDS; ++id)
			{
				grp = drv->getCardGroup(id);
				if(!grp)
					continue;
				cout << "card " << id << " group=" << grp->getLast("name") << endl;
			}
			drv = drv->drvNext;
		}
		exit(0);
	}

	sync();
	drv = Driver::drvFirst;
	ports = 0;
	while(drv)
	{
		slog(Slog::levelDebug) << "Starting " << drv->getName() << " driver..." << endl;
		ports += drv->start();
		drv = drv->drvNext;
	}
	//ports = driver->start();
	if(ports)
		slog(Slog::levelNotice) << "driver started " << ports << " port(s)" << endl;
	else
	{
		slog(Slog::levelCritical) << "no trunk ports activated" << endl;
		final(-1);
	}
	policy = getGroup(NULL);
	embed = keypaths.getLast("embed");
	if(!embed)
		embed = "php";
	if(!*embed)
		embed = NULL;

        cp = keypaths.getLast("php");
        if(cp && !test)
	{
               	php.open(cp, ios::out);
		chmod(cp, 0660);
	}
        if(php.is_open())
        {
		if(embed)
	               	php << "<?php" << embed << endl;

		php << "# This file is automatically generated, do not edit" << endl;

		drv = Driver::drvFirst;
		while(drv)
		{
               		php << "$BAYONNE_DRIVER[" << drv->getDriverIndex() << "]=\"" 
		    		<< drv->getName() <<"\";" << endl;
			drv = drv->drvNext;
		}
		php << "$BAYONNE_HOME=\""
		    << getenv("HOME") << "\";" << endl;
		php << "$BAYONNE_LIBPATH=\""
		    << keypaths.getLibpath() << "\";" << endl;
		php << "$BAYONNE_LIBEXEC=\"" 
		    << getenv("SERVER_LIBEXEC") << "\";" << endl;
		php << "$BAYONNE_NODE=\""
		    << keyserver.getNode() << "\";" << endl;
		php << "$BAYONNE_TOKEN=\""
		    << keyserver.getToken() << "\";" << endl;
		php << "$BAYONNE_DATAFILES=\""
		    << keypaths.getDatafiles() << "\";" << endl;
		php << "$BAYONNE_RUNFILES=\""
		    << keypaths.getRunfiles() << "\";" << endl;
		php << "$BAYONNE_PROMPTS=\""
		    << keypaths.getPromptFiles() << "\";" << endl;
		php << "$BAYONNE_SCRIPTS=\""
		    << keypaths.getScriptFiles() << "\";" << endl;
		strcpy(prefix, policy->getLast("groups"));
		pp = strtok_r(prefix, " \t\n,;", &tok);
		while(pp)
		{
			php << "$BAYONNE_POLICY[" << pcount << "]=\"";
	  		php << pp << "\";" << endl;
			++pcount;
			pp = strtok_r(NULL, " \t\n,;", &tok);
		}
		php << "$BAYONNE_POLICIES="
		    << pcount << ";" << endl;
		php << "$BAYONNE_PORTS="
		    << ports << ";" << endl;
               	php << "$BAYONNE_VERSION=\"" VERPATH "\";" << endl;
		// XXX reenable
		/*php << "$BAYONNE_MIXERS="
		    << driver->getMixers() << ";" << endl;
		while(mcount < driver->getMixers())
		{
			mixer = driver->getMixer(mcount);
			php << "$MIXER_GROUPS[" << mcount << "]=";
                        php << mixer->getGroups() << ";" << endl;
			php << "$MIXER_MEMBERS[" << mcount << "]=";
			php << mixer->getMembers() << ";" << endl;
			++mcount;
		}*/

		if(embed)
	               	php << "?>" << endl;
               	php.close();
		chown(cp, keyserver.getUid(), keyserver.getGid());
	}

	if(keythreads.getResolver())
		resolver = new Resolver();

	startServers();

#ifdef	NODE_SERVICES
	network.start();
#endif

	if(canModify(keypaths.getRunfiles()))
	{
		strcpy(prefix, keypaths.getRunfiles());
		strcat(prefix, "/bayonne.mixer");
	}
	else
		sprintf(prefix, "%s/.bayonne.mixer", getenv("HOME"));

	remove(prefix);
	mix.open(prefix, ios::out);
	if(mix.is_open())
	{
		chmod(prefix, 0640);
		chown(prefix, keyserver.getUid(), keyserver.getGid());
		count = 0;
		// XXX re-enable
		/*while(count < driver->getMixers())
		{
			mixer = driver->getMixer(count);
			if(!mixer)
				break;

			mix << count++ << " " << mixer->getGroups() << " " << mixer->getMembers() << endl;
		}*/
		mix.close();
	}
	else
		slog(Slog::levelWarning) << "startup: mixer open failed" << endl;

	signal(SIGTERM, final);
	signal(SIGQUIT, final);
	signal(SIGINT, final);
	signal(SIGABRT, final);
	pthread_sigmask(SIG_UNBLOCK, &sigs, NULL);
	slog("bayonne", Slog::classDaemon);
	if(keyserver.getLast("config"))
		slog(Slog::levelNotice) << "normal startup; " << keyserver.getLast("config") << endl;
	else
		slog(Slog::levelNotice) << "normal startup" << endl;

	if(policy->getLast("groups"))
		strcpy(prefix, policy->getLast("groups"));
	else
		strcpy(prefix, "*");

	char pbuf[256];
	pbuf[0] = 0;
	pp = strtok_r(prefix, " ,;\t\n", &tok);
	while(pp && plen < sizeof(pbuf))
	{
		if(plen)
			pbuf[plen++] = ',';
		strncpy(pbuf + plen, pp, sizeof(pbuf) - plen);
		pbuf[sizeof(pbuf) - 1] = 0;
		plen = strlen(pbuf);
		pp = strtok_r(NULL, " ,;\t\n", &tok);
	}

	unsigned total = 0;

	drv = Driver::drvFirst;
	while(drv)
	{
		total += drv->getTrunkCount();
		drv = drv->drvNext;
	}	
	sprintf(prefix, "%d", total);

	new KeyTones();
	new MappedCalls();
	running = true;
}

void loader(const char *path, const char *ext)
{
	char buffer[256];
	Dir dir(path);
	const char *name;
	const char *tail;

	while(NULL != (name = dir.getName()))
	{
		tail = strrchr(name, '.');
		if(!tail)
			continue;

		if(stricmp(tail, ext))
			continue;

		snprintf(buffer, sizeof(buffer), "%s/%s", path, name);
		new DSO(path);
	}
}

void check(void)
{
	Dir dir(keypaths.getCache());
	const char *entry;
	char path[128];
	time_t now;
	struct stat ino;

	slog(Slog::levelDebug) << "server: checking cache..." << endl;

	time(&now);

	while(NULL != (entry = dir.getName()))
	{
		if(*entry == '.')
			continue;

		snprintf(path, sizeof(path), "cache/%s", entry);
		if(stat(path, &ino))
			continue;

		if(now - ino.st_mtime >= 3600)
		{
			cachelock.writeLock();
			remove(path);
			cachelock.unlock();
		}
	}
}

static int fd = -1;
static int so = -1;

void control(const char *cmd)
{
	if(fd < 0)
		return;

	write(fd, cmd, strlen(cmd));
}

void errlog(const char *level, const char *fmt, ...)
{
	static Keydata *missing = NULL;
	static Mutex mutex;
	char buffer[256];
	struct tm *dt, tbuf;
	time_t now;
	time(&now);
	int year;
	dt = localtime_r(&now, &tbuf);
	va_list args;
	unsigned len;
	static char *months[12] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	char *m;
	const char *cp;

	if(!fmt)
	{
		mutex.enterMutex();
		if(missing)
			delete missing;
		missing = new Keydata();
		fmt = "Configuration reloaded";
		mutex.leaveMutex();
	}

	year = dt->tm_year;
	if(year < 100)
		year += 1900;

	m = months[--dt->tm_mon];

	if(!stricmp(level, "missing"))
		level = "access";

	va_start(args, fmt);
	snprintf(buffer, sizeof(buffer),
		"errors [%s %02d %02d:%02d:%02d %d] [%s] ",
		m, dt->tm_mday, dt->tm_hour, dt->tm_min, dt->tm_sec, year, level);
	len = strlen(buffer);
	vsnprintf(buffer + len, sizeof(buffer) - len - 1, fmt, args);

	if(!stricmp(level, "access"))
	{
		mutex.enterMutex();
		if(!missing)
			missing = new Keydata();
		cp = missing->getLast(buffer + len);
		if(cp)
		{
			mutex.leaveMutex();
			return;
		}
		missing->setValue(buffer + len, "");
		mutex.leaveMutex();
	}

	len = strlen(buffer);
	if(buffer[len - 1] != '\n')
	{
		buffer[len++] = '\n';
		buffer[len] = 0;
	}
	if(getThread() == mainthread)
		fifo.command(buffer);
	else
		control(buffer);	
}

#define	PROTOCOL_VERSION	3

static char packet[512];

static void packetio(void)
{
	Trunk *trunk;
	char *p = packet + 2;
	struct sockaddr_un caddr;
	socklen_t clen = sizeof(caddr);
	int len = ::recvfrom(so, packet, sizeof(packet), 0, (struct sockaddr *)&caddr, &clen); 
	const char *cp;

	if(len < 0)
	{
		slog(Slog::levelWarning) << "packet: invalid read" << endl;
		return;
	}

	while(isspace(*p))
		++p;
	packet[1] = '-';
	trunk = fifo.command(p);

	if(!packet[0])
		return;

	if(packet[1] == '-')
		packet[2] = 0;

	if(packet[1] == '-' && trunk)
		packet[1] = '1';
	if(packet[1] == '-' && !trunk)
		packet[2] == '0';

	cp = NULL;
	if(trunk && trunk != ((Trunk *)(-1)))
		cp = trunk->getSymbol(SYM_GID);

	if(cp)
		cp = strchr(cp, '-');
	if(cp && packet[1] == '1')
	{
		packet[1] = 'S';
		snprintf(packet + 2, 64, "session.trunkid=%s", cp);
	}

	clen = sizeof(caddr);
	len = ::sendto(so, packet, strlen(packet) + 1, 0, (struct sockaddr *)&caddr, clen);	
}

void setReply(const char *sym, const char *msg)
{
	if(packet[1] != '-')
		return;

	packet[1] = 'S';
	if(!msg)
		msg = "";
	snprintf(packet + 2, sizeof(packet) - 2, "%s=%s", sym, msg);
}

extern "C" int main(int argc, char **argv)
{
	static char buffer[PIPE_BUF / 2];
	static char packet[256];
	struct sockaddr_un server_addr;
	long total, used;
	struct statfs fs;
	char *p;
	const char *etc;
	int bpos = 0;
#ifdef	HAVE_POLL
	struct pollfd pfd[2];
#else
	struct timeval tv;
	int maxfd;
	fd_set sfd;
#endif
	TimerPort timer;
	timeout_t step;
	unsigned ic = 0;
	unsigned seccount = 0, mincount = 0, minhour = 0;
	ifstream init;
#ifndef	NODE_SERVICES
	statnode_t node;
	static char nodepath[256];
	int nodes;
	TrunkGroup *grp;
#endif
	Driver *drv;
	int len;
	int statinterval = atoi(keyserver.getLast("stats"));


	if(canModify(keypaths.getRunfiles()))
	{
		snprintf(buffer, sizeof(buffer), "%s/bayonne.drivers",
			keypaths.getRunfiles());
		keypaths.setValue("drvmap", buffer);
		snprintf(buffer, sizeof(buffer), "%s/bayonne.asr",
			keypaths.getRunfiles());
		keypaths.setValue("asr", buffer);
		snprintf(buffer, sizeof(buffer), "%s/bayonne.stats",
			keypaths.getRunfiles());
		keypaths.setValue("stats", buffer);
		snprintf(buffer, sizeof(buffer), "%s/bayonne.calls",
			keypaths.getRunfiles());
		keypaths.setValue("calls", buffer);
		snprintf(buffer, sizeof(buffer), "%s/bayonne.ctrl",
			keypaths.getRunfiles());
		snprintf(packet, sizeof(packet), "%s/bayonne.packet",
			keypaths.getRunfiles());
#ifndef	NODE_SERVICES
		snprintf(nodepath, sizeof(nodepath), "%s/bayonne.nodes",
			keypaths.getRunfiles());
#endif
	}
	else
	{
		snprintf(buffer, sizeof(buffer), "%s/.bayonne.drivers",
			getenv("HOME"));
		keypaths.setValue("drvmap", buffer);
		snprintf(buffer, sizeof(buffer), "%s/.bayonne.asr",
			getenv("HOME"));
		keypaths.setValue("asr", buffer);
		snprintf(buffer, sizeof(buffer), "%s/.bayonne.stats",
			getenv("HOME"));
		keypaths.setValue("stats", buffer);
		snprintf(buffer, sizeof(buffer), "%s/.bayonne.calls",
			getenv("HOME"));
		keypaths.setValue("calls", buffer);
		snprintf(buffer, sizeof(buffer), "%s/.bayonne.ctrl",
			getenv("HOME"));
		snprintf(packet, sizeof(packet), "%s/.bayonne.packet",
			getenv("HOME"));
		
#ifndef	NODE_SERVICES
		snprintf(nodepath, sizeof(nodepath), "%s/.bayonne.nodes",
			keypaths.getRunfiles());
#endif
	}

	keypaths.setValue("ctrlfile", buffer);
	keypaths.setValue("pktfile", packet);

	if(!getuid())
		umask(003);

	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sun_family = AF_UNIX;
	strncpy(server_addr.sun_path, packet, sizeof(server_addr.sun_path));

#ifdef  __SUN_LEN
        len = sizeof(server_addr.sun_len) + strlen(server_addr.sun_path) 
			+ sizeof(addr.sun_family) + 1;

        addr.sun_len = len;
#else
        len = strlen(server_addr.sun_path) + sizeof(server_addr.sun_family) + 1;
#endif	
	remove(packet);
	so = socket(AF_UNIX, SOCK_DGRAM, 0);
	if(so > -1)
	{
		if(bind(so, (struct sockaddr *)&server_addr, len))
		{
			slog(Slog::levelError) << "packet interface failed; path=" << packet << endl;
			so = -1;
			remove(packet);
		}
	}

	remove(buffer);
	mkfifo(buffer, 0660);
	fd = open(buffer, O_RDWR);

	initial(argc, argv, fd);

	if(debug->debugTest())
		final(0);
	
	slog(Slog::levelDebug) << "fifo: path=" << buffer << endl;
	if(fd < 0)
	{
		errlog("failed", "fifo access; path=%s", buffer);
		slog(Slog::levelWarning) << "fifo: open failed" << endl;
		sleep((unsigned)(-1));
	}

	chown(buffer, keyserver.getUid(), keyserver.getGid());
#ifndef	NODE_SERVICES
	nodes = creat(nodepath, 0640);
	chown(nodepath, keyserver.getUid(), keyserver.getGid());
	grp = getGroup();
#endif

#ifdef	SCHEDULER_SERVICES
	scheduler.initial();
#endif

	while(ic < icount)
		fifo.command(inits[ic++]);
		
        etc = keypaths.getLast("etc");
        etc = strchr(etc + 1, '/');
        if(etc)
                ++etc;
        else
                etc = "";

        strcpy(buffer, keypaths.getLast("etc"));
        if(*etc)
                strcat(buffer, "startup.conf");
        else
                strcat(buffer, "bayonne.init");
        if(!canAccess(buffer))
        {
                strcpy(buffer, keypaths.getLast("etc"));
                strcat(buffer, "bayonne.init");
        }

	init.open(buffer);
	if(init.is_open())
	{
		while(!init.eof())
		{
			init.getline(buffer, sizeof(buffer));
			p = buffer + strlen(buffer) - 1;
			while(isspace(*p) && p >= buffer)
			{
				*(p--) = 0;
			}
			p = buffer;
			while(isspace(*p))
				++p;

			if(!isalpha(*p))
				continue;

			fifo.command(p);
		}
		init.close();
	}

	MappedStats stats;

	errlog("notice", "Bayonne/%s running", VERPATH); 
	timer.setTimer(1000);
	for(;;)
	{
		Thread::yield();
#ifdef	HAVE_POLL
		pfd[0].fd = fd;
		pfd[0].events = POLLIN | POLLRDNORM;
		pfd[0].revents = 0;
		if(so > -1)
		{
			pfd[1].fd = so;
			pfd[1].events = POLLIN | POLLRDNORM;
			pfd[1].revents = 0;
		}
#else
		maxfd = fd;
		FD_ZERO(&sfd);
		FD_SET(fd, &sfd);
		if(so > -1)
			FD_SET(so, &sfd);
#endif
		step = timer.getTimer();
		if(!step)
		{
			drv = Driver::drvFirst;
			while(drv)
			{
				drv->secTick();
				drv = drv->drvNext;
			}
				
			timer.setTimer(1000);
#ifndef	NODE_SERVICES
			if(!(seccount % 10) && nodes > -1)
			{
				lseek(nodes, 0l, SEEK_SET);
				memset(&node, 0, sizeof(node));
	                        node.version = PROTOCOL_VERSION;
                        	node.buddies = 0;
				strncpy(node.name, keyserver.getNode(), sizeof(node.name));
				node.ports = Driver::getCount();
	                        if(!strnicmp(service, "test::", 6))
        	                        node.service = 't';
                	        else if(service[0])
                        	        node.service = 'd';
                        	else
                                	node.service = 'u';
				node.dialing = driver->getExtNumbering();
#ifdef	SCHEDULER_SERVICES
				snprintf(node.schedule, sizeof(node.schedule),
                                	"%s", scheduler.getSchedule());
#else
				snprintf(node.schedule, sizeof(node.schedule), "none");
#endif
	                       	node.uptime = grp->getStat(STAT_SYS_UPTIME);
                        	node.calls = grp->getStat(STAT_SYS_ACTIVITY);
                        	driver->getStatus(node.stat);
				time(&node.update);
				::write(nodes, &node, sizeof(node));
			}
#endif	
			
			
			if(!seccount && !statfs(keypaths.getLast("datafiles"), &fs))
			{
				total = fs.f_blocks / 100;
				used = fs.f_blocks - fs.f_bfree;
				snprintf(df_used, sizeof(df_used), 
					"%d", used / total);
				snprintf(df_free, sizeof(df_free),
					"%d", fs.f_bavail / total);
			}

			if(!(seccount % statinterval))
				stats.scan();

			if(++seccount >= 60)
			{
				TrunkGroup::logStats();
				Session::clean();
				Sync::check();
#ifdef	SCHEDULER_SERVICES
				scheduler.sync();
#endif
				seccount = 0;
				if(++mincount >= 10)
				{
					check();
					mincount = 0;
				}
				if(++minhour >= 60)
				{
					minhour = 0;
				}
			}	
		}

#ifdef	HAVE_POLL
		len = 1;
		if(so > -1)
			len = 2;
		poll(pfd, len, step);
		if(so > -1 && pfd[1].revents & POLLIN)
			packetio();
		if(pfd[0].revents & POLLIN)
                {
#else
		tv.tv_sec = step / 1000;
		tv.tv_usec = (step % 1000) * 1000;
		select(maxfd + 1, &sfd, NULL, &sfd, &tv);
		if(so > -1)
			if(FD_ISSET(so, &sfd))
				packetio();

		if(FD_ISSET(fd, &sfd))
		{
#endif
                        bpos = 0;
                        for(;;)
                        {
                                read(fd, &buffer[bpos], 1);
                                if(buffer[bpos] == '\n')
                                        break;
                                if(buffer[bpos] != '\r' && bpos < sizeof(buffer))
                                        ++bpos;
                        }
                        buffer[bpos] = 0;
			p = buffer + strlen(buffer) - 1;
			while(p > buffer)
			{
				if(*p == ' ' || *p == '\t' || *p == '\n' || *p == '\r')
					*(p--) = 0;
				else
					break;
			}
			p = buffer;
			while(*p == ' ' || *p == '\t')
				++p;
			if(!*p)
				continue;
			fifo.command(p);
		}
	}
	close(fd);
	final(0);
}

const char *getState(void)
{
	const char *svc;

	if(!service[0])
		return "up";

	if(!strnicmp(service, "test::", 6))
		return "test";

	svc = strchr(service, ':');
	if(svc)
		svc += 2;
	else
		svc = service;

	return svc;
}	

#ifdef	CCXX_NAMESPACES
};
#endif
