// Copyright (C) 2000-2001 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "server.h"
#include <pwd.h>
#include <grp.h>
#include <math.h>

#ifdef	HAVE_DLFCN_H
#include <dlfcn.h>
#endif

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

#define	LIB_VERSION	"/" VERPATH "/"

bool hasTTS(void)
{
	if(tts)
		return true;

	if(keyhandlers.getLast("say"))
		return true;

	return false;
}

bool isFHS(void)
{
	const char *etc = getenv("CONFIG_KEYDATA");
	if(!etc)
		return false;
	etc = strchr(etc + 1, '/');
	if(!etc)
		return false;
	if(*(++etc))
		return true;
	return false;
}

KeyLocal::KeyLocal() :
Keydata("/bayonne/localize")
{
	static Keydata::Define keydefs[] = {
	{"primarycurrency", "dollar"},
	{"primarychange", "cent"},
	{"convertcurrency", "1.00"},
	{NULL, NULL}};

	load(keydefs);
}

KeyVoices::KeyVoices() :
Keydata("/bayonne/voices")
{
	static Keydata::Define defimports[] = {
	{"UsEngM", "english"},
	{"UsEngF", "english"},
	{"EnglishM", "english"},
	{"EnglishF", "english"},
	{"SpanishM", "spanish"},
	{"SpanishF", "spanish"},
	{"FrenchM", "french"},
	{"FrenchF", "french"},
	{"RussianM", "russian"},
	{"RussianF", "russian"},
	{"BengaliM", "bengali"},
	{"BengaliF", "bengali"},
	{"ItalianF", "italian"},
	{"ItalianM", "italian"},
	{"GermanF", "german"},
	{"GermanM", "german"},
	{NULL, NULL}};

	load(defimports);
}

KeyImports::KeyImports() :
Keydata("/bayonne/imports")
{
	static Keydata::Define defimports[] = {
	{"perl", ""},
	{"python", ""},
	{NULL, NULL}};

	load("~bayonne/imports");
	load(defimports);
}

KeyPaths::KeyPaths() :
Keydata("/bayonne/paths")
{
	static Keydata::Define defpaths[] = {
//	{"libpath", "/usr/lib/bayonne"},
//	{"libexec", "/usr/libexec/bayonne"},
//	{"tgipath", "/usr/libexec/bayonne:/bin:/usr/bin"},
//	{"datafiles", "/var/lib/bayonne"},
//	{"scripts", "/usr/share/aascripts"},
//	{"prompts", "/usr/share/aaprompts"},
	{"spool", "/var/spool/bayonne"},
	{"cache", "/var/cache/bayonne"},
	{"runfiles", "/var/run/bayonne"},
	{"logpath", "/var/log/bayonne"},
	{"sox", "/usr/bin/sox"},
	{NULL, NULL}};

	load("~bayonne/paths");
	load(defpaths);
}

void KeyPaths::setPrefix(char *prefix)
{
	char tmpfs[65];
	char *pp = prefix + strlen(prefix);
	const char *cp;

	cp = getLast("libpath");
	if(!cp)
	{
		strcpy(pp, "/../lib/bayonne");
		setValue("libpath", prefix);
	}

	cp = getLast("libexec");
	if(!cp)
	{
		strcpy(pp, "/../libexec/bayonne");
		setValue("libexec", prefix);
	}
	cp = getLast("tgipath");
	if(!cp)
	{
		strcpy(pp, "/../libexec/bayonne:/bin:/usr/bin");
		setValue("tgipath", prefix);
	}

	cp = getLast("scripts");
	if(!cp)
	{
		strcpy(pp, "/../share/aascripts");
		setValue("scripts", prefix);
	}

	cp = getLast("prompts");
	if(!cp)
	{
		strcpy(pp, "/../share/aaprompts");
		setValue("prompts", prefix);
	}

	cp = getLast("tmpfs");
	if(!cp)
		cp = "/dev/shm";

	if(!isDir(cp))
		cp = "/tmp";

	snprintf(tmpfs, sizeof(tmpfs), "%s/.bayonne", cp);
	setValue("tmpfs", tmpfs);
	setValue("tmp", "/tmp/.bayonne");

	cp = getenv("CONFIG_KEYDATA");
	if(!cp)
		cp = ETC_PREFIX;
	setValue("etc", cp);

	*pp = 0;
}
	
KeyHandlers::KeyHandlers() :
Keydata("/bayonne/handlers")
{
	static Keydata::Define defpaths[] = {
	{"url", "wget"},
	{"timeout", "90"},
	{NULL, NULL}};

	load("~bayonne/handlers");
	load(defpaths);
}

KeyServer::KeyServer() :
Keydata("/bayonne/server")
{
	static Keydata::Define defkeys[] = {
	{"user", "bayonne"},
	{"group", "bayonne"},
	{"default", "default"},
	{"nodes", "1"},
	{"stats", "15"},
	{"token", "&"},
	{"password", "fts"},
	{"login", "none"},
	{"dialplan", "1"},
	{"policy", "dnis,card,span,port"},
	{NULL, NULL}};

	struct passwd *pwd;
	struct group *grp;
	char namebuf[128];
	char prefix[256];
	char *cp;

	strcpy(prefix, getenv("HOME"));
	strcat(prefix, "/.bayonne");
	altdir = strdup(prefix);
	phrdir = "~.bayonne/phrases/";

	load("~bayonne/server");

	if(!getLast("node"))
	{
		gethostname(namebuf, sizeof(namebuf) - 1);
		cp = strchr(namebuf, '.');
		if(cp)
			*cp = 0;
		cp = strchr(namebuf, '-');
		if(cp)
			*cp = 0;
		setValue("node", namebuf);
	}
	load(defkeys);

	uid = getuid();
	gid = getgid();

	pwd = getpwnam(getLast("user"));
	if(pwd)
		gid = pwd->pw_gid;

	if(pwd && !uid)
	{
		uid = pwd->pw_uid;
		strcpy(prefix, pwd->pw_dir);
		strcat(prefix, "/apps");
		altdir = strdup(prefix);
		phrdir = "~phrases/";
	}
	if(!pwd && !uid)
	{
		uid = 0xffff;
		gid = 0xffff;
	}
}

void KeyServer::loadGroups(bool test)
{
	char *group, *tok;
	char buffer[256];

	new TrunkGroup();
	slog(Slog::levelDebug) << "loading default trunk group" << endl;
	
	group = (char *)TrunkGroup::first->getLast("groups");

	if(test)
		TrunkGroup::first->setValue("script", keyserver.getDefault());

	if(!group || test)
		return;

	strcpy(buffer, group);
	group = strtok_r(buffer, " \t\n", &tok);
	while(group)
	{
		slog(Slog::levelDebug) << "loading " << group << " trunk group" << endl;
		new TrunkGroup(group);
		group = strtok_r(NULL, " \t\n", &tok);
	}
}

void KeyServer::setUid(void)
{
	setuid(uid);
	uid = getuid();
}

void KeyServer::setGid(void)
{
	setgid(gid);
	gid = getgid();
}

KeyThreads::KeyThreads() :
Keydata("/bayonne/threads")
{
	static Keydata::Define defpaths[] = {
	{"audit", "0"},
	{"audio", "0"},
	{"priority", "0"},
	{"gateways", "0,1"},
	{"services", "1,1"},
	{"database", "0"},
	{"network", "0"},
	{"switch", "0"},
	{"managers", "0"},
	{"scheduler", "0"},
	{"gui", "0"},
	{"rtp", "0"},
	{"resetdelay", "18"},
	{"stepdelay", "36"},
	{"stepinterval", "18"},
	{"interval", "15"},
	{"refresh", "5"},
	{"policy", "other"},
	{"pages", "0"},
#ifdef	__FreeBSD__
	{"stack", "8"},
#endif
	{NULL, NULL}};

	const char *cp = getLast("pri");
	
	if(cp)
		setValue("priority", cp);

	load(defpaths);
}

int KeyThreads::getPolicy()
{
	const char *cp = getLast("policy");
#ifdef	SCHED_RR
	if(!stricmp(cp, "rr"))
		return SCHED_RR;
#endif
#ifdef	SCHED_FIFO
	if(!stricmp(cp, "fifo"))
		return SCHED_FIFO;
#endif
	return 0;
}

#ifdef	NODE_SERVICES

KeyNetwork::KeyNetwork() :
Keydata("/bayonne/network")
{
	static Keydata::Define defkeys[] = {
	{"refresh", "5"},
	{"live", "25"},
	{"elect", "120"},
	{"expire", "300"},
	{"database", "127.0.0.1:7002"},
	{"address", "127.0.0.1:7001"},
	{"broadcast", "127.255.255.255"},
	{"monitor", "127.0.0.1:7070"},
	{NULL, NULL}};

	load("~bayonne/network");

	load(defkeys);
}

InetAddress KeyNetwork::getAddress(void)
{
	char buffer[65];
	char *cp;

	strcpy(buffer, getLast("address"));
	cp = strchr(buffer, ':');
	if(*cp)
		*cp = 0;
	return InetAddress(buffer);
}

InetAddress KeyNetwork::getMonitorAddress(void)
{
	char buffer[65];
	char *cp;

	strcpy(buffer, getLast("monitor"));
	cp = strchr(buffer, ':');
	if(*cp)
		*cp = 0;
	return InetAddress(buffer);
}

InetHostAddress KeyNetwork::getDBHost(void)
{
	char buffer[65];
	char *cp;

	strcpy(buffer, getLast("database"));
	cp = strchr(buffer, ':');
	if(*cp)
		*cp = 0;
	return InetHostAddress(buffer);
}

InetHostAddress KeyNetwork::getBroadcast(void)
{
	return InetHostAddress(getLast("broadcast"));
}

tpport_t KeyNetwork::getPort(void)
{
	char buffer[65];
	char *cp;

	strcpy(buffer, getLast("address"));
	cp = strchr(buffer, ':');
	if(cp)
		return atoi(++cp);
	return 0;
}

tpport_t KeyNetwork::getMonitorPort(void)
{
	char buffer[65];
	char *cp;

	strcpy(buffer, getLast("monitor"));
	cp = strchr(buffer, ':');
	if(cp)
		return atoi(++cp);
	return 0;
}

tpport_t KeyNetwork::getDBPort(void)
{
	char buffer[65];
	char *cp;

	strcpy(buffer, getLast("database"));
	cp = strchr(buffer, ':');
	if(cp)
		return atoi(++cp);
	return 0;
}

#endif

KeyMemory::KeyMemory() :
Keydata("/bayonne/memory")
{
	static Keydata::Define defpaths[] = {
	{"symbols", "64"},
	{"page", "1024"},
	{"users", "1000"},
	{"prefs", "256"},
	{NULL, NULL}};

	load(defpaths);
}

size_t KeyThreads::getStack(void)
{
	const char *cp = getLast("stack");

	if(cp)
		return atoi(cp) * 1024;
	
	return 0;
}

int KeyThreads::priResolver(void)
{
	const char *cp = getLast("resolver");
	if(cp)
		return atoi(cp);

	return 0;
}

int KeyThreads::getResolver(void)
{
	char buf[32];
	const char *cp = getLast("resolver");

	if(!cp)
		return 0;

	strcpy(buf, cp);
	cp = strchr(buf, ',');
	if(cp)
	{
		++cp;
		while(*cp == ' ' || *cp == '\t')
			++cp;
		return atoi(cp);
	}
	return 15;
}

int KeyThreads::getServices(void)
{
	char buf[32];
	int cnt;

	strcpy(buf, getLast("services"));
	char *cp = strchr(buf, ',');
	if(cp)
	{
		++cp;
		while(*cp == ' ' || *cp == '\t')
			++cp;
		cnt = atoi(cp);
		if(cnt > 0)
			return cnt;
	}
	return 1;
}

int KeyThreads::getGateways(void)
{
	char buf[32];

	strcpy(buf, getLast("gateways"));
	char *cp = strchr(buf, ',');
	if(cp)
	{
		++cp;
		if(*cp == ' ' || *cp == '\t')
			++cp;
		return atoi(cp);
	}
	else
		return 1;
}

Plugins::Plugins() :
Keydata("/bayonne/plugins")
{
	if(!getLast("codecs"))
		setValue("codecs", "g.711");

	if(getLast("driver"))
		return;

#if defined(HAVE_LINUX_TELEPHONY_H)
	setValue("driver", "phonedev");
#elif defined(have_montecarlo_h) || defined(HAVE_MONTECARLO_H)
	setValue("driver", "pika");
#else
	setValue("driver", "dummy");
#endif
	pidcount = 0;	
}

Plugins::~Plugins()
{
//	dynunload();
	while(pidcount)
		kill(pids[--pidcount], SIGTERM);
}

char *Plugins::getDriverName(void)
{
	static char name[33];

	const char *drv = getLast("driver");
	char *cp = strrchr(drv, '/');
	if(cp)
		++cp;
	else
		cp = (char *)drv;
	strncpy(name, cp, 32);
	name[33] = 0;
	cp = strrchr(name, '.');
	if(!cp)
		return name;

	if(!strcmp(cp, ".ivr"))
		*cp = 0;
	return name;
}

void Plugins::loadManagers(void)
{
	char path[256];
	char list[412];
	char *argv[3];
	char *cp;

	cp = (char *)getLast("managers");
	if(!cp)
		return;

	strcpy(list, cp);
	cp = strtok(list, " \t\n;,");
	while(cp)
	{
		if(!*cp)
			break;

		if(*cp == '/')
			path[0] = 0;
		else
		{
			strcpy(path, keypaths.getLibexec());
			strcat(path, "/");
		}
		strcat(path, "bayonne.");
		strcat(path, cp);
		pids[pidcount] = fork();
		if(pids[pidcount] == 0)
		{
			argv[0] = path;
			argv[1] = (char *)keypaths.getRunfiles();
			argv[2] = NULL; 
			execv(path, argv);
			slog(Slog::levelError) << "libexec: " << path << ": unable to run" << endl;
			exit(-1);
		}
		else if(pids[pidcount] > 0)
			++pidcount;
		else
			slog(Slog::levelError) << "libexec: " << path << ": unable to fork" << endl;
	}
}

void Plugins::loadCodecs(void)
{
        char list[512];
        char *cp;

        cp = (char *)getLast("preload");
        if(!cp)
                return;

        if(!stricmp(cp, "none"))
                return;

        if(!stricmp(cp, "no"))
                return;

        strcpy(list, cp);
        cp = strtok(list, " \t\n;,");
        while(cp)
        {
                if(!*cp)
                        break;

#ifdef	AUDIO_CODEC_MODULES
                AudioCodec::load(cp);
#endif
                cp = strtok(NULL, " \t\n;,");
        }
}

void Plugins::loadPreload(void)
{
	char list[512];
	char *cp;

	cp = (char *)getLast("preload");
	if(!cp)
		return;

	if(!stricmp(cp, "none"))
		return;

	if(!stricmp(cp, "no"))
		return;

	strcpy(list, cp);
	cp = strtok(list, " \t\n;,");
	while(cp)
	{
		if(!*cp)
			break;

		Script::use(cp);
		cp = strtok(NULL, " \t\n;,");
	}
}

void Plugins::loadModules(void)
{
	char path[256];
	char list[512];
	char *cp;

	cp = (char *)getLast("modules");

	if(!cp)
		return;

	if(!stricmp(cp, "no"))
		return;

	if(!stricmp(cp, "none"))
		return;

	if(!stricmp(cp, "*") || !stricmp(cp, "all"))
	{
		snprintf(path, sizeof(path), "%s/%s",
			keypaths.getLibpath(), LIB_VERSION);
		loader(path, ".mod");
		return;
	}

	strcpy(list, cp);
	cp = strtok(list, " \t\n;,");
	while(cp)
	{
		if(!*cp)
			break;

		if(*cp == '/')
			path[0] = 0;
		else
			snprintf(path, sizeof(path), "%s/%s",
				keypaths.getLibpath(), LIB_VERSION);

		strcat(path, cp);
		if(*cp != '/')
			strcat(path, ".mod");
		new DSO(path);
		cp = strtok(NULL, " \t\n;,");
	}
}

void Plugins::loadTGI(void)
{
	char path[256];
	char list[512];
	char *cp;

	cp = (char *)getLast("tgi");
	if(!cp)
		return;

	strcpy(list, cp);
	cp = strtok(list, " \t\n;,");
	while(cp)
	{
		if(!*cp)
			break;

		if(*cp == '/')
			path[0] = 0;
		else
		{
			strcpy(path, keypaths.getLibpath());
			strcat(path, LIB_VERSION);
		}
		strcat(path, cp);
		if(*cp != '/')
			strcat(path, ".tgi");
#ifdef	HAVE_DLFCN_H
		dlopen(path, RTLD_LAZY | RTLD_GLOBAL);
#else
		new DSO(path, false);
#endif
		cp = strtok(NULL, " \t\n;,");
	}
}


void Plugins::loadTranslators(const char *lcp)
{
	char path[256];
	char list[512];
	char *cp;

	if(!lcp)
		lcp = getLast("languages");

	if(!lcp)
		return;

	strcpy(list, lcp);
	cp = strtok(list, " \t\n;,");
	while(cp)
	{
		if(!*cp)
			break;

		if(*cp == '/')
			path[0] = 0;
		else
		{
			strcpy(path, keypaths.getLibpath());
			strcat(path, LIB_VERSION);
		}

		strcat(path, cp);
		if(*cp != '/')
			strcat(path, ".tts");
		new DSO(path);
		cp = strtok(NULL, " \t\n;,");
	}
}

void Plugins::loadExtensions(void)
{
	char path[256];
	strcpy(path, keypaths.getLibpath());
	strcat(path, LIB_VERSION);
	strcat(path, "bayonne.ext");
	if(canAccess(path))
		new DSO(path);
}

void Plugins::loadDebug(void)
{
	char path[256];
	char *d = (char *)getLast("debug");

	if(!d)
	{
		new Debug();
		return;
	}

	if(*d != '/')
	{
		strcpy(path, keypaths.getLibpath());
		strcat(path, LIB_VERSION);
		strcat(path, d);
		strcat(path, ".dbg");			
		d = path;
	}
	new DSO(d);
	if(!debug)
	{
		slog(Slog::levelNotice) << "no debug handler installed" << endl;
		new Debug();
	}
}

void Plugins::loadMonitor(void)
{
	char path[256];
	char *d = (char *)getLast("monitors");

	if(!d)
	{
		new Monitor();
		return;
	}

	if(*d != '/')
	{
		strcpy(path, keypaths.getLibpath());
		strcat(path, LIB_VERSION);
		strcat(path, d);
		strcat(path, ".mon");
		d = path;
	}
	new DSO(d);
	if(!monitor)
	{
		slog(Slog::levelNotice) << "no monitor handler installed" << endl;
		new Monitor();
	}
}

void Plugins::loadSwitch(void)
{
	char path[256];
	const char *sim = getLast("switch");

	if(!sim)
		return;

	if(*sim)
	{
		snprintf(path, sizeof(path), "%s/%s/%s.sim",
			keypaths.getLibpath(), LIB_VERSION, sim);
		new DSO(path);
	}
}

void Plugins::loadSQL(void)
{
	char path[256];
	const char *t = getLast("sql");

	if(!t)
		return;

	if(!*t)
		return;

	if(*t == '/')
		new DSO(t);
	else
	{
                snprintf(path, sizeof(path), "%s/%s/%s.sql",
                        keypaths.getLibpath(), LIB_VERSION, t);
                new DSO(path);
	}
}

void Plugins::loadTTS(void)
{
        char path[256];
        const char *t = getLast("tts");
 
        if(!t)
                return;

 	if(!*t)
		return;

        if(*t == '/')
		new DSO(t);
	else
        {
                snprintf(path, sizeof(path), "%s/%s/%s.tts",
                        keypaths.getLibpath(), LIB_VERSION, t);
                new DSO(path);
        }
}

void Plugins::loadDriver(void)
{
	char path[256];
	char list[512];
	const char *drv = getLast("driver");
	char *drvpath;
	char *cp;

	snprintf(list, sizeof(list), "%s", drv);
	cp = strtok(list, " \t\n;,");

	while(cp)
	{
		if(!*cp)
			break;

		if(*cp != '/')
		{
			snprintf(path, sizeof(path), "%s%s%s.ivr",
				keypaths.getLibpath(), LIB_VERSION, cp);	
			drvpath = path;
		}
		else
			drvpath = cp;
	
		new DSO(drvpath);
		cp = strtok(NULL, " \t\n;,");
	}
}

KeyTones::KeyTones() :
Keydata("/bayonne/tones")
{
        static Keydata::Define keydefs[] = {
	{"ringback", "440 480 2000"},
	{"busytone", "480 620 500"},
	{"reorder", "480 620 250"},
	{"dialtone", "350 440 1000"},
	{"pbx:dialtone", "cont 350 440 30000"},
	{"intercom", "350 440 100"},
	{NULL, NULL}};

	int v1, v2, v3;
	char *tone;
	phTone *pht;
	unsigned count = getCount() + 1;
	char **tones;
	bool cont, play;
	unsigned playtime;
	Driver *drv = Driver::drvFirst;
	
	if(count < 2)
	{
		load(keydefs);
		count = getCount() + 1;
	}
	tones = new char *[count];
	getIndex(tones, count);

	while(*tones)
	{
		playtime = 1000;
		cont = false;
		play = false;
		v1 = v2 = v3 = 0;
		tone = (char *)getLast(*tones);
		if(!tone || !stricmp(*tones, "tones"))
		{
			++tones;
			continue;
		}

		while(isspace(*tone))
			++tone;

		if(!strnicmp(tone, "cont", 4))
		{
			play = true;
			cont = true;
			playtime = 0;
		}
		else if(!strnicmp(tone, "play", 4))
		{
			play = true;
			tone = strchr(tone, ' ');
			while(isspace(*tone))
				++tone;
			playtime = atoi(tone);
		}
		if(play)
			tone = strchr(tone, ' ');
		sscanf(tone, "%d %d %d", &v1, &v2, &v3);
		if(v3)
			pht = new phTone(*tones, v3, v1, v2);
		else
			pht = new phTone(*tones, v2, v1);

		if(playtime && playtime < pht->duration)
			playtime = pht->duration;
		pht->playtime = playtime;
		++tones;
	}
}

TrunkGroup *TrunkGroup::first = NULL;

TrunkGroup::TrunkGroup(char *name) :
Keydata("/bayonne/trunks"), CallStat()
{
	Keydata::Define keys[] = {
		{"answer", "1"},
		{"accept", "1"},
		{"rpc", "1"},
		{"hangup", "100"},
		{"siezetime", "12"},
		{"ringtime", "7"},
		{"flash", "200"},
		{"dialtone", "2400"},
		{"dialspeed", "160"},
		{"volume", "80"},
		{"callerid", "1500"},
		{"pickup", "500"},
		{"requests", "hangup"},
		{"select", "last"},
		{"threashold", "0"},
		{"ready", "1000"},
		{"idletime", "600"},
		{"analysis", "16"},
		{"international", "011"},
		{"national", "1"},
		{"dialmode", "dtmf"},
		{"mindigits", "0"},
		{"mdigtimeout", "3"},
		{"interval", "60"},
		{NULL, NULL}};

	// XXX fixme
	int count = Driver::drvFirst->getTrunkCount();
	int i;
	char *cp;
	char namebuf[65];

	schedule[0] = 0;
	load("~bayonne/trunks");

	if(name)
	{
		strcpy(namebuf, "/bayonne/");
		strcat(namebuf, name);
		strcat(namebuf, "-trunks");
		load(namebuf);
		*namebuf = '~';
		load(namebuf);
		cp = (char *)getLast("trunks");
		if(cp)
			cp = strtok(cp, " ,;\t\n");
		while(cp)
		{
			i = atoi(cp);
			if(i >= 0 && i < count)
				Driver::drvFirst->groups[i] = this;
			cp = strtok(NULL, " ,;\t\n");
		}

		cp = (char *)getLast("spans");
		if(cp)
			cp = strtok(cp, " ,;\t\n");
		while(cp)
		{
			i = atoi(cp);
			if(i > 0 && i < MAX_SPANS)
				Driver::drvFirst->spans[i] = this;
			cp = strtok(NULL, " ,;\t\n");
		}

                cp = (char *)getLast("cards");
                if(cp)
                        cp = strtok(cp, " ,;\t\n");
                while(cp)
                {
                        i = atoi(cp);
                        if(i > 0 && i < MAX_CARDS)
                                Driver::drvFirst->cards[i] = this;
                        cp = strtok(NULL, " ,;\t\n");
                }

		cp = (char *)getLast("stations");
		if(cp)
			cp = strtok(cp, " ,;\t\n");
		while(cp)
		{
			if(!stricmp(cp, "*"))
				Driver::drvFirst->stacards[0] = this;
			i = atoi(cp);
			if(i > 0 && i < MAX_CARDS)
				Driver::drvFirst->stacards[i] = this;
			cp = strtok(NULL, " ,;\t\n");
		}	
	}
	else
	{
		Driver *drv = Driver::drvFirst;
		while(drv)
		{
			for(i = 0; i < drv->getTrunkCount(); ++i) {
				drv->groups[i] = this;
			}
			drv = drv->drvNext;
		}
		name = "*";
	}

	setValue("name", name);

	load(keys);
	next = NULL;
	reqfirst = reqlast = NULL;
	polFirst = NULL;
	members = 0;

	if(!first)
		first = this;
	else
	{
		next = first;
		while(next->next)
			next = next->next;
		next->next = this;
	}
	next = NULL;
}	

bool TrunkGroup::getAccept(void)
{
        const char *cp = getLast("accept");
        if(!cp)
                return false;

        switch(*cp)
        {
        case '0':
        case 'f':
        case 'F':
        case 'n':
        case 'N':
                return false;
        }
        return true;
}

bool TrunkGroup::getRPC(void)
{
	const char *cp = getLast("rpc");
	if(!cp)
		return false;

	switch(*cp)
	{
	case '0':
	case 'f':
	case 'F':
	case 'n':
	case 'N':
		return false;
	}
	return true;
}

bool TrunkGroup::getDetect(void)
{
	const char *cp = getLast("detect");
	if(!cp)
		return false;

	switch(*cp)
	{
	case '0':
	case 'f':
	case 'F':
	case 'n':
	case 'N':
		return false;
	}

	return true;
}

seltype_t TrunkGroup::getSelect(void)
{
	const char *cp = getLast("select");

	if(!stricmp(cp, "last"))
		return SELECT_LAST;

	return SELECT_FIRST;
}

Request *TrunkGroup::getRequest(void)
{
	Request *req = NULL;
	Policy *pol;
	Module *reply = Module::reqFirst;
	Trunk *trunk;
	TrunkEvent event;

	enterMutex();
	pol = polFirst;
	while(pol)
	{
		req = pol->hiPriority();
		if(req)
		{
			leaveMutex();
			return req;
		}
		pol = pol->next;
	}
	while(reqfirst)
	{
		reply = Module::reqFirst;
		if(reqfirst->isExpired())
		{
			slog(Slog::levelWarning) << "request: " << getName() << "(" << reqfirst->id << ") expired...";
			trunk = Driver::drvFirst->getTrunkId(reqfirst->parent);
			if(trunk)
			{
				event.id = TRUNK_SYNC_PARENT;
				event.parm.sync.msg = "start:expired";
				event.parm.sync.id = "none";
				if(!trunk->postEvent(&event))
				{
					event.id = TRUNK_CHILD_FAIL;
					trunk->postEvent(&event);
				}
			}
			while(reply)
			{
				reply->expires(reqfirst);
				reply = reply->reqNext;
			}
			delete reqfirst;
			continue;
		}
	}
	if(reqfirst)
	{
		reply = Module::reqFirst;
		while(reply)
		{
			reply->running(reqfirst);
			reply = reply->reqNext;
		}
		req = reqfirst;
		req->detach();
	}
	pol = polFirst;
	while(!req && pol)
	{
		req = pol->loPriority();
		pol = pol->next;
	}
	leaveMutex();
	return req;
}

const char *TrunkGroup::getRedirect(const char *redirect, char *buf)
{
	const char *cp = getLast(redirect);
	if(cp)
	{
		strcpy(buf, cp);
		return buf;
	}
	return getSchedule(buf);
}

const char *TrunkGroup::getSchedule(char *buf)
{
	const char *scr = getLast("script");	// check for .conf override
	if(scr)
	{
		strcpy(buf, scr);
		return buf;
	}

#ifdef	SCHEDULER_SERVICES

	scheduler.readLock();
	if(schedule[0])
		strcpy(buf, schedule);
	else if(this == first)
		strcpy(buf, keyserver.getDefault());
	else
		strcpy(buf, getName());
	scheduler.unlock();

#else
	strcpy(buf, keyserver.getDefault());
#endif

	return buf;
}

const char *TrunkGroup::getNumber(void)
{
	const char *num = getLast("number");

	if(num)
		return num;

	return "UNKNOWN";
}

void TrunkGroup::setSchedule(const char *str)
{
#ifdef	SCHEDULER_SERVICES
	if(!str)
		str = "";

	scheduler.writeLock();
	strncpy(schedule, str, sizeof(schedule) - 1);
	schedule[sizeof(schedule) - 1] = 0;
	scheduler.unlock();
#endif
}

void TrunkGroup::logStats(void)
{
	
	char buffer[128];
	TrunkGroup *grp = first;
	const char *name;
	time_t now;
	time_t current, prior, interval;
	struct tm *dt, tbuf, ubuf;
	int hr, min;

	time(&now);
	
	while(grp)
	{
		name = grp->getName();
		if(!stricmp(name, "*"))
			name = "default";
		interval = atol(grp->getLast("interval")) * 60l;
		prior = grp->updated / interval;
		current = now / interval;
		if(prior != current)
		{
			current = now - 60l;
			dt = localtime_r(&current, &tbuf);
			hr = dt->tm_hour;
			min = dt->tm_min;
			dt = localtime_r(&grp->updated, &ubuf);
			grp->enterMutex();
			snprintf(buffer, sizeof(buffer), 
				"stats %s %0ld/%ld %d %02d/%02d %02d:%02d %02d:%02d %ld %ld %d %d\n",
				name, prior, interval, grp->capacity,
				dt->tm_mon, dt->tm_mday, 
				dt->tm_hour, dt->tm_min, hr, min,
				grp->total.incoming, grp->total.outgoing,
				grp->max.incoming, grp->max.outgoing);
			grp->update();
			grp->leaveMutex();
			fifo.command(buffer);
		}
		grp = grp->next;
	}
}

phTone *phTone::first = NULL;

unsigned char phTone::alaw[256] = {
	0xab, 0x55, 0xd5, 0x15, 0x95, 0x75, 0xf5, 0x35,
	0xb5, 0x45, 0xc5, 0x05, 0x85, 0x65, 0xe5, 0x25,
	0xa5, 0x5d, 0xdd, 0x1d, 0x9d, 0x7d, 0xfd, 0x3d,
	0xbd, 0x4d, 0xcd, 0x0d, 0x8d, 0x6d, 0xed, 0x2d,
	0xad, 0x51, 0xd1, 0x11, 0x91, 0x71, 0xf1, 0x31,
	0xb1, 0x41, 0xc1, 0x01, 0x81, 0x61, 0xe1, 0x21,
	0x59, 0xd9, 0x19, 0x99, 0x79, 0xf9, 0x39, 0xb9,
	0x49, 0xc9, 0x09, 0x89, 0x69, 0xe9, 0x29, 0xa9,
	0xd7, 0x17, 0x97, 0x77, 0xf7, 0x37, 0xb7, 0x47,
	0xc7, 0x07, 0x87, 0x67, 0xe7, 0x27, 0xa7, 0xdf,
	0x9f, 0x7f, 0xff, 0x3f, 0xbf, 0x4f, 0xcf, 0x0f,
	0x8f, 0x6f, 0xef, 0x2f, 0x53, 0x13, 0x73, 0x33,
	0xb3, 0x43, 0xc3, 0x03, 0x83, 0x63, 0xe3, 0x23,
	0xa3, 0x5b, 0xdb, 0x1b, 0x9b, 0x7b, 0xfb, 0x3b,
	0xbb, 0xbb, 0x4b, 0x4b, 0xcb, 0xcb, 0x0b, 0x0b,
	0x8b, 0x8b, 0x6b, 0x6b, 0xeb, 0xeb, 0x2b, 0x2b,
	0xab, 0x54, 0xd4, 0x14, 0x94, 0x74, 0xf4, 0x34,
	0xb4, 0x44, 0xc4, 0x04, 0x84, 0x64, 0xe4, 0x24,
	0xa4, 0x5c, 0xdc, 0x1c, 0x9c, 0x7c, 0xfc, 0x3c,
	0xbc, 0x4c, 0xcc, 0x0c, 0x8c, 0x6c, 0xec, 0x2c,
	0xac, 0x50, 0xd0, 0x10, 0x90, 0x70, 0xf0, 0x30,
	0xb0, 0x40, 0xc0, 0x00, 0x80, 0x60, 0xe0, 0x20,
	0x58, 0xd8, 0x18, 0x98, 0x78, 0xf8, 0x38, 0xb8,
	0x48, 0xc8, 0x08, 0x88, 0x68, 0xe8, 0x28, 0xa8,
	0xd6, 0x16, 0x96, 0x76, 0xf6, 0x36, 0xb6, 0x46,
	0xc6, 0x06, 0x86, 0x66, 0xe6, 0x26, 0xa6, 0xde,
	0x9e, 0x7e, 0xfe, 0x3e, 0xbe, 0x4e, 0xce, 0x0e,
	0x8e, 0x6e, 0xee, 0x2e, 0x52, 0x12, 0x72, 0x32,
	0xb2, 0x42, 0xc2, 0x02, 0x82, 0x62, 0xe2, 0x22,
	0xa2, 0x5a, 0xda, 0x1a, 0x9a, 0x7a, 0xfa, 0x3a,
	0xba, 0xba, 0x4a, 0x4a, 0xca, 0xca, 0x0a, 0x0a,
	0x8a, 0x8a, 0x6a, 0x6a, 0xea, 0xea, 0x2a, 0x2a};

int phTone::ulaw[256] = { 
	0,0,1,1,2,2,2,2,3,3,3,3,3,3,3,3,
        4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
        5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
        5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
        6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
        6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
        6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
        6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
        7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
        7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
        7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
        7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
        7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
        7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
        7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
        7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7};

unsigned char phTone::a2u[128] = {
	1, 3, 5, 7, 9, 11, 13, 15, 
	16, 17, 18, 19, 20, 21, 22, 23, 
	24, 25, 26, 27, 28, 29, 30, 31, 
	32, 32, 33, 33, 34, 34, 35, 35, 
	36, 37, 38, 39, 40, 41, 42, 43, 
	44, 45, 46, 47, 48, 48, 49, 49, 
	50, 51, 52, 53, 54, 55, 56, 57, 
	58, 59, 60, 61, 62, 63, 64, 64, 
	65, 66, 67, 68, 69, 70, 71, 72, 
	73, 74, 75, 76, 77, 78, 79, 79, 
	80, 81, 82, 83, 84, 85, 86, 87, 
	88, 89, 90, 91, 92, 93, 94, 95, 
	96, 97, 98, 99, 100, 101, 102, 103, 
	104, 105, 106, 107, 108, 109, 110, 111, 
	112, 113, 114, 115, 116, 117, 118, 119, 
	120, 121, 122, 123, 124, 125, 126, 127}; 

short phTone::ulaw2linear(unsigned char ul)
{
	/*short		t;

	ul = ~ul;

	t = ((ul & 0x0f) << 3) + 0x84;
	t <<= ((unsigned)ul & 0x70) >> 4;

	return ((ul & 0x80) ? (0x84 - t) : (t - 0x84));*/
	static int exp_lut[8] = {0,132,396,924,1980,4092,8316,16764};
	int sign, exponent, mantissa, sample;
	ul = ~ul;
	sign = (ul >> 4) & 0x80;
	exponent = (ul >> 4) && 0x07;
	mantissa = ul & 0x0F;
	sample = exp_lut[exponent] + (mantissa << (exponent + 3));

	return sample;
}

short phTone::alaw2linear(unsigned char al)
{
	short		t;
	short		seg;

	al ^= 0x55;

	t = (al & 0x0f) << 4;
	seg = ((unsigned)al & 0x70) >> 4;
	switch (seg) {
	case 0:
		t += 8;
		break;
	case 1:
		t += 0x108;
		break;
	default:
		t += 0x108;
		t <<= seg - 1;
	}
	return ((al & 0x80) ? t : -t);
}

unsigned char phTone::alaw2ulaw(unsigned char al)
{
	al &= 0xff; 
 	return ((al & 0x80) ? (0xFF ^ a2u[al ^ 0xD5]) : 
		(0x7F ^ a2u[al ^ 0x55])); 
} 

unsigned char phTone::linear2ulaw(int sample)
{
	int sign, exponent, mantissa, retval;

	sign = (sample >> 8) & 0x80;
	if(sign != 0) sample = -sample;
	sample += 0x84;
	exponent = ulaw[(sample >> 7) & 0xff];
	mantissa = (sample >> (exponent + 3)) & 0x0f;
	retval = ~(sign | (exponent << 4) | mantissa);
	if(!retval)
		retval = 0x02;

	return retval;
}

phTone::phTone(const char *n, timeout_t dur, unsigned f)
{
	unsigned i;
	int sample;

	if(n)
	{
		next = first;
		first = this;
	}
	else
		n = "*temp*";

        duration = playtime = dur;
        samples = new unsigned char[dur * 8];
        freq1 = f;
        freq2 = 0;
	snprintf(name, sizeof(name), "%s", n);

	double freq = (f * M_PI * 2) / 8000.;
	double pos = 0;

	for(i = 0; i < dur * 8; ++i)
	{
		sample = (int)(sin(pos) * 20000.0);
		pos += freq;
		samples[i] = linear2ulaw(sample);
	}
}

phTone::phTone(const char *n, timeout_t dur, unsigned f1, unsigned f2)
{
	unsigned i;
	int sample;

	if(n)
	{
		next = first;
		first = this;
	}
	else
		n = "*temp*";

        duration = playtime = dur;
        samples = new unsigned char[dur * 8];
        freq1 = f1;
        freq2 = f2;
	snprintf(name, sizeof(name), "%s", n);

	double fa1 = (f1 * M_PI * 2) / 8000.;
	double fa2 = (f2 * M_PI * 2) / 8000.;
	double pos1 = 0, pos2 = 0;

	for(i = 0; i < dur * 8; ++i)
	{
		sample = (int)((sin(pos1) + sin(pos2)) * 10000.0);
		pos1 += fa1;
		pos2 += fa2;
		samples[i] = linear2ulaw(sample);
	}
}

 
phTone::~phTone()
{
	if(samples)
		delete[] samples;
}


void phTone::clear(void)
{
	if(samples)
		delete[] samples;

	samples = NULL;
}

phTone *getphTone(const char *name) 
{
	phTone *tone = phTone::first;
	while(tone)
	{
		if(!stricmp(tone->name, name))
			break;
		tone = tone->next;
	}
        if(tone)
                return tone;

        name = strchr(name, ':');
        if(name)
		tone = getphTone(++name);

	return tone;
}

TrunkGroup *getGroup(const char *name)
{
	TrunkGroup *group = TrunkGroup::first;

	if(!name)
		return group;

	if(!stricmp(name, "*"))
		return group;

	if(!stricmp(name, "default"))
		return group;

	while(group)
	{
		if(!stricmp(name, group->getName()))
			return group;
		group = group->next;
	}
	return NULL;
}		

Keydata application("/bayonne/application");
#ifdef	USER_HOSTING
Keydata keyusers;
#endif
KeyServer keyserver;
KeyPaths keypaths;
KeyLocal keylocal;
KeyHandlers keyhandlers;
#ifdef	NODE_SERVICES
KeyNetwork keynetwork;
#endif
KeyVoices keyvoices;
KeyThreads keythreads;
KeyMemory keymemory;
Plugins modules;
#ifdef	SCHEDULER_SERVICES
Scheduler scheduler;
#endif
#ifdef	NODE_SERVICES
Network network;
#endif

#ifdef	CCXX_NAMESPACES
};
#endif
