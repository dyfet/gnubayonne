// Copyright (C) 2000-2001 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "ivrconfig.h"
#include <cc++/strchar.h>

#define	BAYONNE_VERSION	VERSION
#define	BAYONNE_SERVER	PACKAGE

#include "bayonne.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

extern	bool upflag;

static unsigned funid(const char *keyword)
{
        unsigned key = 0;

        while(*keyword)
                key ^= key << 1 ^ (*(keyword++) & 0x1f);

        return key % SCRIPT_INDEX_SIZE;
}

aaScript::aaScript() :
ScriptRuntime()
{
	// define primary Bayonne dialect
	static ScriptInterp::Define interp[] = {
		{"cleardigits", false, (Method)&Trunk::scrCleardigits, 
			(Check)&ScriptChecks::chkIgnore},
		{"hangup", false, (Method)&Trunk::scrHangup, 
			(Check)&ScriptChecks::chkIgnore},
		{"audit", false, (Method)&Trunk::scrAudit, 
			(Check)&ScriptChecks::chkHasArgs},
		{"debug", false, (Method)&Trunk::scrDebug, 
			(Check)&ScriptChecks::chkHasArgs},
		{"sleep", false, (Method)&Trunk::scrSleep,
			(Check)&ScriptChecks::chkHasArgs},
		{"sync", false, (Method)&Trunk::scrSync, 
			(Check)&ScriptChecks::chkIgnore},
		{"accept", false, (Method)&Trunk::scrAccept, 
			(Check)&ScriptChecks::chkIgnore},
		{"reject", false, (Method)&Trunk::scrReject,
			(Check)&ScriptChecks::chkIgnore},
		{"answer", false, (Method)&Trunk::scrAnswer, 
			(Check)&ScriptChecks::chkIgnore},
		{"collect", false, (Method)&Trunk::scrCollect, 
			(Check)&ScriptChecks::chkHasArgs},
		{"flash", false, (Method)&Trunk::scrFlash, 
			(Check)&ScriptChecks::chkIgnore},
		{"altplay", false, (Method)&Trunk::scrAltPlay,
			(Check)&ScriptChecks::chkHasArgs},
		{"play", false, (Method)&Trunk::scrPlay, 
			(Check)&ScriptChecks::chkHasArgs},
		{"say", false, (Method)&Trunk::scrSay, 
			(Check)&ScriptChecks::chkHasArgs},
		{"asr", false, (Method)&Trunk::scrAsr,
			(Check)&ScriptChecks::chkHasArgs},
		{"listen", false, (Method)&Trunk::scrListen,
			(Check)&ScriptChecks::chkIgnore},
		{"record", false, (Method)&Trunk::scrRecord, 
			(Check)&ScriptChecks::chkHasArgs},	
		{"erase", false, (Method)&Trunk::scrErase,
			(Check)&ScriptChecks::chkHasArgs},
		{"copy", false, (Method)&Trunk::scrCopy,
			(Check)&ScriptChecks::chkHasArgs},
		{"append", false, (Method)&Trunk::scrCopy,
			(Check)&ScriptChecks::chkHasArgs},
		{"move", false, (Method)&Trunk::scrMove,
			(Check)&ScriptChecks::chkHasArgs},
		{"tone", false, (Method)&Trunk::scrTone,
			(Check)&ScriptChecks::chkHasArgs},
		{"dial", false, (Method)&Trunk::scrDial, 
			(Check)&ScriptChecks::chkHasArgs},
		{"transfer", false, (Method)&Trunk::scrTransfer, 
			(Check)&ScriptChecks::chkHasArgs},
		{"hold", false, (Method)&Trunk::scrHold, 
			(Check)&ScriptChecks::chkNoArgs},
#ifdef	HAVE_TGI
		{"libexec", false, (Method)&Trunk::scrLibexec, 
			(Check)&ScriptChecks::chkHasArgs},
#endif
		{"altspeak", false, (Method)&Trunk::scrAltSpeak,
			(Check)&ScriptChecks::chkHasArgs},
		{"build", false, (Method)&Trunk::scrBuild,
			(Check)&ScriptChecks::chkHasArgs},
		{"speak", false, (Method)&Trunk::scrSpeak, 
			(Check)&ScriptChecks::chkHasArgs},
		{"schedule", false, (Method)&Trunk::scrSchedule, 
			(Check)&ScriptChecks::chkIgnore},
		{"start", false, (Method)&Trunk::scrStart, 
			(Check)&ScriptChecks::chkHasArgs},
		{"busy", false, (Method)&Trunk::scrBusy,
			(Check)&ScriptChecks::chkIgnore},
		{"idle", false, (Method)&Trunk::scrIdle, 
			(Check)&ScriptChecks::chkIgnore},
		{"send", false, (Method)&Trunk::scrSend, 
			(Check)&ScriptChecks::chkHasArgs},
		{"config", false, (Method)&Trunk::scrConfig, 
			(Check)&ScriptChecks::chkIgnore},
		{"policy", false, (Method)&Trunk::scrPolicy, 
			(Check)&ScriptChecks::chkIgnore},
		{"slog", false, (Method)&Trunk::scrSlog,
			(Check)&ScriptChecks::chkIgnore},
		{"session", false, (Method)&Trunk::scrSession,
			(Check)&ScriptChecks::chkIgnore},
		{"service", false, (Method)&Trunk::scrService,
			(Check)&ScriptChecks::chkIgnore},
		{"redirect", false, (Method)&Trunk::scrRedirect,
			(Check)&ScriptChecks::chkHasArgs},
		{"route", false, (Method)&Trunk::scrRoute,
			(Check)&ScriptChecks::chkHasArgs},
		{"sendfax", false, (Method)&Trunk::scrSendFax,
			(Check)&ScriptChecks::chkHasArgs},
		{"recvfax", false, (Method)&Trunk::scrRecvFax,
			(Check)&ScriptChecks::chkHasArgs},
		{"exit", false, (Method)&Trunk::scrExiting,
			(Check)&ScriptChecks::chkIgnore},
		{"join", false, (Method)&Trunk::scrJoin,
			(Check)&ScriptChecks::chkHasArgs},
		{"wait", false, (Method)&Trunk::scrWait,
			(Check)&ScriptChecks::chkHasArgs},
		{"ring", false, (Method)&Trunk::scrRing,
			(Check)&ScriptChecks::chkHasArgs},
		{NULL, false, NULL, NULL}};

	// set standard aascript trap identifiers

#ifdef	SCRIPT_TRAP_MASKED
	trap("timeout", false);
#else
	trap("timeout");
#endif
	trap("dtmf");

	trap("0");	/* 0x10 */
	trap("1");
	trap("2");
	trap("3");

	trap("4");	/* 0x100 */
	trap("5");
	trap("6");
	trap("7");

	trap("8");	/* 0x1000 */
	trap("9");
	trap("star");
	trap("pound");
	
	trap("a");	/* 0x10000 */
	trap("b");
	trap("c");
	trap("d");

#ifdef	SCRIPT_TRAP_MASKED
	trap("silence", false);
	trap("busy", false);
	trap("cancel", false);
	trap("notify", false);
#else
	trap("silence");
	trap("busy");
	trap("cancel");
	trap("notify");
#endif

	trap("noanswer");
	trap("ring");
	trap("tone");
	trap("event");

	trap("time");
	trap("child");

	trap("asr");

	load(interp);
}

int aaScript::mapicmp(const char *s1, const char *s2)
{
	for(;;)
	{
		if(!*s1 || !*s2)
			return *s2 - *s1;
		if(tolower(*s1) != tolower(*s2))
			if(*s1 != '?' && *s2 != '?')
				return tolower(*s2) - tolower(*s1);
		++s1;
		++s2;
	}
}

int aaScript::mapnicmp(const char *s1, const char *s2, size_t n)
{
        while(n--)
        {
                if(!*s1 || !*s2)
                        return *s2 - *s1;
                if(tolower(*s1) != tolower(*s2))
                        if(*s1 != '?' && *s2 != '?')
                                return tolower(*s2) - tolower(*s1);
                ++s1;
                ++s2;
        }
	return 0;
}	
	
void aaScript::addModule(Module *module, const char *alias)
{
	char *name = module->getName();
	Script::Define keywords[2];
	char namebuf[65];
	
	if(!name)
		return;

	if(alias)
	{
		snprintf(namebuf, sizeof(namebuf), "%s-%s", alias, name);
		name = namebuf;
	}
	name = alloc(name);

	module->prior = getHandler(name);
	keywords[0].keyword = name;
	keywords[0].method = (Method)&Trunk::scrModule;
	keywords[0].check = (Check)&ScriptChecks::chkHasArgs;
	keywords[1].keyword = NULL;
	keywords[1].method = (Method)NULL;
	keywords[1].check = (Check)NULL;
	load(keywords);
}

void aaScript::addDummy(const char *name)
{
	Script::Define keywords[2];

	keywords[0].keyword = name;
	keywords[0].method = (Method)&Trunk::scrDummy;
	keywords[0].check = (Check)&ScriptChecks::chkIgnore;
	keywords[1].keyword = NULL;
	keywords[1].method = (Method)NULL;
	keywords[1].check = (Check)NULL;
	load(keywords);
}
	
unsigned long aaScript::getTrapMask(const char *trapname)
{
	unsigned long mask;

	if(!stricmp(trapname, "hangup"))
		return 0x00000001;

	if(!strcmp(trapname, "!"))
		return 0x0000fff8;

	if(!stricmp(trapname, "override"))
		return 0x00010000;

	if(!stricmp(trapname, "flash"))
		return 0x00020000;

	if(!stricmp(trapname, "immediate"))
		return 0x00040000;

	if(!stricmp(trapname, "priority"))
		return 0x00080000;

	if(!stricmp(trapname, "part"))
		return 0x00400000;

	if(!stricmp(trapname, "fail"))
		return 0x00400000;

	if(!stricmp(trapname, "invalid"))
		return 0x00400000;

	if(!stricmp(trapname, "nodialtone"))
		return 0x01000000;

	if(!stricmp(trapname, "noringback"))
		return 0x01000000;

	if(!stricmp(trapname, "busytone"))
		return 0x00200000;

	if(!stricmp(trapname, "text"))
		return 0x00800000;

	if(!stricmp(trapname, "signal"))
		return 0x08000000;

	if(!stricmp(trapname, "maxtime"))
		return 0x10000000;

	if(!stricmp(trapname, "answer"))
		return 0x02000000;

	if(!stricmp(trapname, "pickup"))
		return 0x02000000;

	mask = ScriptCommand::getTrapMask(trapname);
	if(mask == 0x00000008)
		return 0x0000fff8;

	if(mask & 0x0000fff0)
		mask |= 0x00000008;

	return mask;
}

const char *aaScript::getExternal(const char *opt)
{
	if(strnicmp(opt, "server.", 7))
		return NULL;
	
	opt += 7;

	if(!stricmp(opt, "software"))
		return "bayonne";
	else if(!stricmp(opt, "version"))
		return VERPATH;
	else if(!stricmp(opt, "node") || !stricmp(opt, "name"))
		return keyserver.getLast("node");
	return NULL;
}
	
void aaImage::Schedule::update(void)
{
	strcpy(day[1], day[0]);
	strcpy(name[1], name[0]);
	strcpy(start[1], start[0]);
	strcpy(command[1], command[0]);
}

aaImage::Schedule *aaImage::active = NULL;

aaImage::aaImage(aaScript *script) :
ScriptCompiler((ScriptCommand *)script, "/bayonne/script")
{
	Name *scr;
	Line *line, *last = NULL;
	unsigned key;
	unsigned long mask;
	unsigned lines = 0;

	char path[1024], topath[1024];
	char *users[128];
	char **user = users;
	char *cp, *tok;
	char username[128];
	const char *alt = keypaths.getLast("altscripts");
	const char *var = keypaths.getLast("varscripts");
	const char *local = keypaths.getLast("local");

	if(!local)
		local = keypaths.getLast("localprefix");

	snprintf(path, sizeof(path), "%s/users/.template.save",
		keypaths.getLast("datafiles"));
	remove(path);
	fduser = ::creat(path, 0640);
	chown(path, getuid(), keyserver.getGid());

        snprintf(path, sizeof(path), "%s/hunting/.template.save",
	        keypaths.getLast("datafiles"));
        remove(path);
	fdhunt = ::creat(path, 0640);
        chown(path, getuid(), keyserver.getGid());

        snprintf(path, sizeof(path), "%s/lines/.template.save",
                keypaths.getLast("datafiles"));
        remove(path);
        fdline = ::creat(path, 0640);
        chown(path, getuid(), keyserver.getGid());

	if(local)
	{
		if(canAccess(local) && isDir(local))
		{
			strcpy(path, local);
			scanDir1(path);
			scanDir2(path);
		}
	}

	if(alt)
	{
		if(canAccess(alt) && isDir(alt))
		{
			strcpy(path, alt);
			scanDir1(path);
			scanDir2(path);
		}
	}

	if(var)
	{
		if(canAccess(var) && isDir(var))
		{
			strcpy(path, var);
			scanDir1(path);
			scanDir2(path);
		}
	}

	strcpy(path, keypaths.getScriptFiles());
	if(canAccess(path))
	{
		scanDir1(path);
		scanDir2(path);
	}

	strcpy(path, keyserver.getPrefix());
	strcat(path, "/aascripts");
	if(isDir(path))
		if(canAccess(path))
		{
			scanDir1(path);
			scanDir2(path);
		}

       static Keydata::Define keys[] = {
                {"timeout", "60"},
                {"interdigit", "6"},
                {"extdigits", "4"},
                {"localdigits", "7"},
                {"natdigits", "10"},
                {"xferextension", "FW"},
                {"dialextension", "W"},
                {"diallocal", "W9"},
                {"xferlocal", "FW9"},
                {"dialnational", "W81"},
                {"xfernational", "FW81"},
                {"dialinternational", "W8011"},
                {"xferinternational", "FW8011"},
                {"localprefix", "732"},
                {"dialoperator", "0"},
                {"fastpickup", "2"},
                {"slowpickup", "5"},
                {"country", "1"},
                {"ringtime", "6"},
                {"language", "english"},
                {"extension", ".au"},
                {"voice", "UsEngM"},
                {NULL, NULL}};

        Keydata::load("~bayonne/script");
        setValue("node", keyserver.getNode());
        setValue("version", BAYONNE_VERSION);
        setValue("server", BAYONNE_SERVER);
        setValue("driver", modules.getDriverName());

	if(tts)
		setValue("tts", tts->getName());
	else
		setValue("tts", "none");

        Keydata::load(keys);

	if(fduser > -1)
		::close(fduser);

	if(fdhunt > -1)
		::close(fdhunt);

	if(fdline > -1)
		::close(fdline);

	commit();
	errlog("notice", NULL);
};

bool aaImage::preProcess(const char *directive, Name *script)
{
// cmds
	char rule[512];
	char tagbuf[128];
	char idbuf[32];
	char *tok, *tag;
	Translator *tts;
	size_t size;
	Symbol *sym;
	TrunkGroup *grp = getGroup("*");

	if(!stricmp(directive, ".mod") || !stricmp(directive, ".module"))
	{
		while(NULL != (tok = getToken()))
		{
			if(!getModule(MODULE_ANY, tok))
			{
				snprintf(rule, sizeof(rule), "%s/%s.mod",
					keypaths.getLast("modlibpath"), tok);
				try
				{
					new DSO(rule);
				}
				catch(DSO *dso)
				{
					slog(Slog::levelError) << "load: " << rule << " failed." << endl;
					return true;
				}
			}
		}
		return true;
	}

	if(!stricmp(directive, ".xml"))
		return true;

	if(!stricmp(directive, ".config"))
	{
		tok = getToken();
		snprintf(tagbuf, sizeof(tagbuf), "%s", tok);
		tok = tagbuf;
		tag = getToken();
		if(!tag)
			tag = tok;
		snprintf(rule, sizeof(rule), "/%s/%s", tok, tag);
		loadPrefix(rule, "CONFIG_KEYDATA");
		return true;
	}

	if(!stricmp(directive, ".rpc"))
	{
		while(NULL != (tok = getToken()))
		{
			snprintf(rule, sizeof(rule), "rpc::%s", tok);
			keyserver.setValue(rule, script->name);
		}
		return true;
	}

	if(!stricmp(directive, ".import"))
	{
		if(!Module::modImport)
			return true;

		while(NULL != (tok = getToken()))
			Module::modImport->import(tok);

		return true;
	}

	if(!stricmp(directive, ".clear"))
	{
		while(NULL != (tok = getToken()))
		{
			tag = (char *)grp->getLast(tok);
			if(!tag)
				tag = "";

			if(!stricmp(tag, "*"))
				continue;

			grp->setValue(tok, "*");
		}
	}			

	if(!strnicmp(directive, ".default", 8))
	{
		directive += 8;
		if(*directive == '.')
			++directive;
		if(*directive)
			size = atoi(directive);
		else
			size = 64;

		directive = getToken();
		if(!directive)
			return true;

		if(!stricmp(directive, "ringgroup"))
			return true;
	
		snprintf(idbuf, sizeof(idbuf), "%s", directive);
		tok = getToken();
		if(!tok)
			tok = "";

		snprintf(sym->data, sym->size + 1, "%s", tok);
                snprintf(tagbuf, sizeof(tagbuf), "%s %d %s\n",
                        idbuf, size, tok);
                if(fduser > -1)
                        ::write(fduser, tagbuf, strlen(tagbuf));
		
		return true;
	}		 

	if(!stricmp(directive, ".languages"))
	{
		while(NULL != (tok = getToken()))
		{
			tts = getTranslator(tok);
			if(!tts)
				modules.loadTranslators(tok);
		}
		return true;
	}

	if(!stricmp(directive, ".prefix") || !stricmp(directive, ".dir"))
	{
		while(NULL != (tok = getToken()))
			mkdir(tok, 0770);
		return true;
	}		

	if(!stricmp(directive, ".rule") || !stricmp(directive, ".phrase"))
		directive = "all";
	else
	{	
		tts = getTranslator(directive + 1);
		if(!tts)
			return false;	
		++directive;
	}

	rule[0] = 0;
	tag = getToken();
	if(!tag)
		return false;

	snprintf(tagbuf, sizeof(tagbuf), "_%s_%s", directive, tag);

	while(NULL != (tok = getToken()))
	{
		if(rule[0])
			strcat(rule, " ");
		strcat(rule, tok);
	}
	if(!rule[0])
		return false;

	setValue(tagbuf, rule);
	return true;
}

void aaImage::scanDir1(char *path)
{
	const char *scrname;
	char *pp = path + strlen(path);
	Dir dir(path);

	*(pp++) = '/';
	for(;;)
	{
		scrname = dir.getName();
		if(!scrname)
			break;
		if(!isScript(scrname))
			continue;

		strcpy(pp, scrname);
		compile(path);
	}
	*(--pp) = 0;
}

void aaImage::scanDir2(char *path)
{
        const char *scrname, *ext;
	char sbuf[65];
	char *ex;
        char *pp = path + strlen(path);
        Dir dir(path);

        *(pp++) = '/';
        for(;;)
        {
                scrname = dir.getName();
                if(!scrname)
                        break;


		ext = strrchr(scrname, '.');
		if(!ext)
			continue;

		if(stricmp(ext, ".scr"))
			continue;

		if(!stricmp(scrname, "up.scr"))
			upflag = true;

                strcpy(pp, scrname);

		snprintf(sbuf, sizeof(sbuf), "%s", scrname);
		ex = strrchr(sbuf, '.');
		if(ex && !stricmp(ex, ".scr"))
		{
			*ex = 0;
			if(getScript(sbuf))
				continue;
		}
                compile(path);
        }
	*(--pp) = 0;
}

bool aaImage::isScript(const char *scrname)
{
	char modbuf[32];
	char sw[5];
	Module *mod = Module::modFirst;
	Driver *drv = Driver::drvFirst;
	const char *mext;
	char *ext = strrchr(scrname, '.');
	if(!ext)
		return false;

	// handle in pass 2
	if(!stricmp(ext, ".scr"))
		return false;

	if(!stricmp(ext, ".tts") && tts)
		return true;

	if(!stricmp(ext, ".mod"))
	{
		strncpy(modbuf, scrname, 31);
		modbuf[31] = 0;
		ext = strchr(modbuf, '.');
		if(!ext)
			return false;

		*ext = 0;
		if(getModule(MODULE_ANY, modbuf))
			return true;
		return false;
	}

	while(mod)
	{
		mext = mod->getExtension();
		if(mext)
			if(!stricmp(ext, mext))
				return true;
		mod = mod->modNext;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
