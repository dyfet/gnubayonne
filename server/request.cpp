// Copyright (C) 2000-2001 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "server.h"

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

unsigned Request::seq = 0;

Policy::Policy(TrunkGroup *grp)
{
	group = grp;
	group->enterMutex();
	next = group->polFirst;
	group->polFirst = this;
	group->leaveMutex();
}

Request::Request(TrunkGroup *grp, const char *text, unsigned seconds, const char *tag, const char *pid)
{
	int argc = 0;
	char *cp;
	char *tok;

	next = NULL;
	strcpy(buffer, text);
	cp = strtok_r(buffer, " \t\n\r", &tok);

	while(argc < 32 && cp)
	{
		argv[argc++] = cp;
		cp = strtok_r(NULL, " \t\n\r", &tok);
	}
	argv[argc] = NULL;

	group = grp;
	time(&expires);
	expires += seconds;
	id = ++seq;
	if(!pid)
		pid = "-";

	snprintf(parent, sizeof(parent), "%s", pid);
	
	if(!tag)
		snprintf(tagid, sizeof(tagid), "req-%d", id);
	else
		strncpy(tagid, tag, sizeof(tagid) - 1);
	tagid[sizeof(tagid) - 1] = 0;
	
	if(group)
	{
		slog(Slog::levelDebug) << "request: " << group->getName() << "(" << id << ") posted" << endl;
		group->enterMutex();
		if(!group->reqfirst)
			group->reqfirst = this;
		if(group->reqlast)
			group->reqlast->next = this;
		group->reqlast = this;
		group->leaveMutex();
	}
}

void Request::detach(void)
{
	if(!group)
		return;

	group->enterMutex();
	if(this == group->reqfirst)
		group->reqfirst = next;
	if(this == group->reqlast)
		group->reqlast = next;
	group->reqlast = this;
	group->leaveMutex();
	group = NULL;
}

bool Request::isExpired(void)
{
	time_t now;
	time(&now);

	if(now > expires)
		return true;
	return false;
}

Request *locate(TrunkGroup *group, const char *tag, int *pos)
{
	int tmp;
	Request *req;

	if(!pos)
		pos = &tmp;

	*pos = 0;

	group->enterMutex();
	req = group->reqfirst;
	while(req)
	{
		++(*pos);
		if(!stricmp(req->tagid, tag))
		{
			group->leaveMutex();
			return req;
		}
		req = req->next;
	}
	return NULL;
}		

void cancel(TrunkGroup *group, const char *tag)
{
	Module *reply = Module::reqFirst;
	Request *req, *next;
	group->enterMutex();
	req = group->reqfirst;
	while(req)
	{
		next = req->next;
		if(!stricmp(req->tagid, tag))
		{
			reply = Module::reqFirst;
			while(reply)
			{
				reply->cancelled(req);
				reply = reply->reqNext;
			}
			delete req;
		}
		req = next;
	}
	group->leaveMutex();
}

Request *request(TrunkGroup *group, char **argv, unsigned timeout, const char *tag, const char *pid)
{
	unsigned threashold = group->getThreashold();
	seltype_t sel = group->getSelect();
	unsigned active = group->getStat(STAT_ACTIVE_CALLS);
	int port;
	unsigned ports = Driver::drvFirst->getTrunkCount();
	Trunk *trunk;
	TrunkEvent event;
	char buffer[512];
	int argc = 0;
	unsigned len = 0;

	if(threashold > active && sel == SELECT_FIRST)
	{
		for(port = 0; port < (int)ports; ++port)
		{
			if(Driver::drvFirst->getTrunkGroup(port) != group)
				continue;

			event.id = TRUNK_START_SCRIPT;
			event.parm.argv = argv;
			trunk = Driver::drvFirst->getTrunkPort(port);
			if(trunk->postEvent(&event))
				return NULL;
		}
	}
	if(threashold > active && sel == SELECT_LAST)
	{
		for(port = ports - 1; port > -1; --port)
		{
			if(Driver::drvFirst->getTrunkGroup(port) != group)
				continue;

			event.id = TRUNK_START_SCRIPT;
			event.parm.argv = argv;
			trunk = Driver::drvFirst->getTrunkPort(port);
			if(trunk->postEvent(&event))
				return NULL;
		}
	}

	strncpy(buffer, argv[0], sizeof(buffer));
	len = strlen(buffer);
	while(argv[++argc] && len < sizeof(buffer))
	{
		buffer[len++] = ' ';
		strncpy(buffer, argv[argc], sizeof(buffer) - len);
		buffer[sizeof(buffer) - 1] = 0;
		len = strlen(buffer);
	}

	return new Request(group, buffer, timeout, tag, pid);
}

#ifdef	CCXX_NAMESPACES
};
#endif


			















