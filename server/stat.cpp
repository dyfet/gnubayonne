// Copyright (C) 2000-2001 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "server.h"

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

unsigned long CallStat::upincoming = 0;
unsigned long CallStat::upoutgoing = 0;
time_t CallStat::uptime = 0;

CallStat::CallStat() :
Mutex()
{
	if(!uptime)
		time(&uptime);

	time(&updated);
	time(&idled);
	prior = 0;
	capacity = 0;
	dirty = true;
	total.incoming = total.outgoing = lasttotal.incoming = lasttotal.outgoing = 0;
	active.incoming = active.outgoing = max.incoming = max.outgoing = lastmax.incoming = lastmax.outgoing = 0;
}

void CallStat::update(void)
{
	enterMutex();
	dirty = true;
	memcpy(&lasttotal, &total, sizeof(total));
	memcpy(&lastmax, &max, sizeof(max));
	memcpy(&max, &active, sizeof(active));
	memset(&total, 0, sizeof(total));
	prior = updated;
	time(&updated);
	leaveMutex();
}

void CallStat::incIncoming(void)
{
	enterMutex();
	dirty = true;
	++upincoming;
	++total.incoming;
	if(++active.incoming > max.incoming)
		++max.incoming;
	leaveMutex();
}

void CallStat::decIncoming(void)
{
	enterMutex();
	dirty = true;
	--active.incoming;
	if(!active.incoming && !active.outgoing)
		time(&idled);
	leaveMutex();
}

void CallStat::incOutgoing(void)
{
	enterMutex();
	dirty = true;
	++upoutgoing;
	++total.outgoing;
	if(++active.outgoing > max.outgoing)
		++max.outgoing;
	leaveMutex();
}

void CallStat::decOutgoing(void)
{
	enterMutex();
	dirty = true;
	--active.outgoing;
	if(!active.incoming && !active.outgoing)
		time(&idled);
	leaveMutex();
}

long CallStat::getIdleTime(void)
{
	time_t now;
	time(&now);

	enterMutex();
	if(active.incoming || active.outgoing)
		now = idled;
	now -= idled;
	leaveMutex();
	return now;
}

void CallStat::getStat(unsigned long *list)
{
	enterMutex();
	*(list++) = max.incoming;
	*(list++) = max.outgoing;
	*(list++) = lastmax.incoming;
	*(list++) = lastmax.outgoing;
	*(list++) = total.incoming;
	*(list++) = total.outgoing;
	*(list++) = lasttotal.incoming;
	*(list++) = lasttotal.outgoing;
	leaveMutex();
}

long CallStat::getStat(statitem_t item)
{
	long value = 0;
	time_t now;

	enterMutex();
	switch(item)
	{
	case STAT_ACTIVE_CALLS:
		value = active.outgoing + active.incoming;
		break;
	case STAT_CURRENT_CALLS:
		value = total.outgoing + total.incoming;
		break;
	case STAT_AVAIL_CALLS:
		value = capacity - active.outgoing - active.incoming;
		break;
	case STAT_CMAX_INCOMING:
		value = max.incoming;
		break;
	case STAT_CMAX_OUTGOING:
		value = max.outgoing;
		break;
	case STAT_MAX_INCOMING:
		value = lastmax.incoming;
		break;
	case STAT_MAX_OUTGOING:
		value = lastmax.outgoing;
		break;
	case STAT_TOT_INCOMING:
		value = lasttotal.incoming;
		break;
	case STAT_TOT_OUTGOING:
		value = lasttotal.outgoing;
		break;
	case STAT_NOW_INCOMING:
		value = total.incoming;
		break;
	case STAT_NOW_OUTGOING:
		value = total.outgoing;
		break;
	case STAT_SYS_INCOMING:
		value = upincoming;
		break;
	case STAT_SYS_OUTGOING:
		value = upoutgoing;
		break;
	case STAT_SYS_UPTIME:
		time(&now);
		value = now - uptime;
		break;
	case STAT_SYS_ACTIVITY:
		value = upincoming + upoutgoing;
		break;
	}
	leaveMutex();
	return value;
}

//		name   size time      acti acto maxi      maxo      toti  toto 
#define	FORMAT	"%-16s %04d %04d %04d %04d/%04d %04d/%04d %06ld/%06ld %06ld/%06ld"

MappedCalls::MappedCalls() :
MappedFile(getPath(), accessReadWrite)
{
	unsigned count = Driver::getCount();
	unsigned idx = 0;
	callrec_t blank, *cr;
	char *cp = (char *)&blank;
	Trunk *trunk;
	char name[10];
	
	memset(&blank, 0x20, sizeof(blank) - 1);
	cp[sizeof(blank) - 1] = '\n';

	while(idx < count)
	{
		::write(fd, &blank, sizeof(blank));
		++idx;
	}
	Trunk::callrec = (callrec_t *)fetch(0, sizeof(blank) * count);
	idx = 0;
	while(idx < count)
	{
		cr = &Trunk::callrec[idx];
		snprintf(name, sizeof(name), "port/%d", idx++);
		trunk = Driver::getTrunk(name);
		if(!trunk)
			continue;
		trunk->setCalls("init");
	}
}

MappedStats::MappedStats() :
MappedFile(getPath(), accessReadWrite)
{
	TrunkGroup *grp = TrunkGroup::first;
	unsigned count = 0;
	char blank[80];

	memset(blank, 0x20, sizeof(blank));
	while(grp)
	{
		::write(fd, blank, sizeof(blank));
		++count;
		grp = grp->next;
	}
	map = (char *)fetch(0, sizeof(blank) * count);
	scan();
}

const char *MappedCalls::getPath(void)
{
	int fd;
	const char *path = keypaths.getLast("calls");

	remove(path);
	fd = creat(path, 0640);
	::close(fd);
	chown(path, keyserver.getUid(), keyserver.getGid());
	return path;
}

const char *MappedStats::getPath(void)
{
	int fd;
	const char *path = keypaths.getLast("stats");

	remove(path);
	fd = creat(path, 0640);
	::close(fd);
        chown(path, keyserver.getUid(), keyserver.getGid());
        return path;
}

void MappedStats::scan(void)
{
	TrunkGroup *grp = TrunkGroup::first;
	unsigned count = 0;
	char buf[80];
	char *p;
	unsigned len;
	
	while(grp)
	{
		if(!grp->dirty)
			goto skip;

		grp->enterMutex();
		grp->dirty = false;
		snprintf(buf, sizeof(buf), FORMAT, grp->getName(), grp->getCapacity(),
			grp->active.incoming, grp->active.outgoing,
			grp->max.incoming, grp->lastmax.incoming,
			grp->max.outgoing, grp->lastmax.outgoing,
			grp->total.incoming, grp->lasttotal.incoming, 
			grp->total.outgoing, grp->lasttotal.outgoing);
		grp->leaveMutex();

		len = strlen(buf);
		while(len < sizeof(buf) - 1)
			buf[len++] = ' ';
		buf[len] = '\n';
		memcpy(map + sizeof(buf) * count, buf, sizeof(buf));
skip:
		++count;
		grp = grp->next;
	}
}

#ifdef	CCXX_NAMESPACES
};
#endif
