// Copyright (C) 2000-2001 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "ivrconfig.h"
#include "bayonne.h"

#ifdef	CCXX_NAMESPACES
namespace ost {
#endif

#ifdef	HAVE_TGI

// determine size of tgi command buffer based on atomic size of fifo,
// ideally using a fraction of a xk buffer.

#if PIPE_BUF > 1024
#define	TGI_BUF (1024)
#else
#define	TGI_BUF	(512)
#endif

// type of tgi execution request

typedef enum
{
	TGI_EXEC_NORMAL,
	TGI_EXEC_DETACH,
	TGI_EXEC_AUDIO,
	TGI_EXEC_SHELL,
	TGI_EXEC_POST,
	TGI_EXEC_IMMEDIATE
} tgiexec_t;

#pragma pack(1)
typedef	struct
{
	char port[64];		// tgi port reference
	unsigned short seq;	// sequence check id
	tgiexec_t mode;		// execution mode
	char cmd[TGI_BUF - 64 - sizeof(short) - sizeof(tgiexec_t)];
} tgicmd_t;

typedef struct callrec_t
{
	char cr_time[6];
	char cr_type[5];
	char cr_caller[17];
	char cr_dialed[17];
	char cr_script[13];
	char cr_login[12];
	char cr_nl;
} callrec_t;

#endif

typedef struct {
	time_t update;
	statnode_t *buddy;
}	buddy_t;

typedef	struct {
	char grp[32];
	char scr[32];
}	schedtmp;

#pragma pack()

#ifdef	SCHEDULER_SERVICES

// these classes generally are not accessed  outside of the server image
// itself and so are not "exposed" through the bayonne header file.

class Scheduler : public ThreadLock
{
private:
	class Job
	{
	public:
		enum
		{
			job_time,
			job_space
		}	jobType;
		Job	*next, *prev;
		long	jobActivate;
		char	*jobCommand;
	}	*jobFirst, *jobLast;

	char altschedule[65];
	std::ifstream sched;
	int interval, rtmp;
	unsigned lastload;
	int load(void);
	void update(void);
	int resHour, resMinute;

public:
	Scheduler();
	~Scheduler();

	void initial(void);
	void sync(void);
	void altSchedule(const char *name);
	inline const char *getSchedule(void)
		{return altschedule;};
	inline bool isAlternate(void)
		{return (altschedule[0] != 0);};
	void stop(void);
};

#endif

#ifdef	NODE_SERVICES

// the network service thread.

class Network : public Thread, public UDPSocket, public MappedFile
{
private:
	friend void broadcast(char *msgbuf, int msglen);
	friend statnode_t *getNodes(const char *name);

	statnode_t *map;
	time_t last;
	char *getPath(void);
	statnode_t *getNode(const char *name);
	statnode_t *getAddr(struct in_addr *addr);
	struct sockaddr bcast, maddr;
	struct sockaddr_in *bcast_in, *maddr_in;

	void initial(void);
	void run(void);
	void failover(void);
	void elect(void);
	void send(char *msgbuf, int msglen);

public:
	Network();

	void stop(void);
	void refresh(int secs);
};

#endif

#ifdef	XML_SCRIPTS
class URLAudio : public AudioFile, protected URLStream
{
private:
	unsigned long offset;
	bool afOpen(const char *path);
	bool afPeek(unsigned char *data, unsigned size);
	bool afSeek(unsigned long pos);
	void afClose(void);
	int afRead(unsigned char *buffer, unsigned len);

public:
	bool hasPositioning(void);
	bool isOpen(void);

	unsigned long getTransfered(void);

	unsigned long getPosition(void)
		{return getTransfered();};

	void close(void)
		{AudioFile::close();};

	URLAudio();
	~URLAudio();
};
#else
class URLAudio : public AudioFile
{
public:
	inline unsigned long getTransfered()
		{return getPosition();};

	inline bool hasPositioning(void)
		{return true;};
};
#endif

// the resolver is an optional service thread.

class Resolver : public Server
{
private:
	static bool instance;
	time_t interval;

	void run(void);
	void stop(void);

public:
	Resolver();
};

class MappedCalls : public MappedFile
{
private:
	const char *getPath(void);

public:
	MappedCalls();
};

class MappedStats : public MappedFile
{
private:
	char *map;
	const char *getPath(void);

public:
	MappedStats();
	void scan(void);
};

#define	SYM_NETWORK	"driver.network"
#define	SYM_TRUNKID	"driver.trunkid"
#define	SYM_DRIVER	"driver.name"
#define	SYM_DRIVERID	"driver.index"
#define	SYM_DRIVERSIZE	"driver.ports"
#define	SYM_INTERFACE	"pstn.interface"
#define	SYM_TONE	"pstn.tone"
#define	SYM_PROGRESS	"pstn.tone"
#define	SYM_ANNOTATION	"audio.annotation"
#define	SYM_PLAYED	"audio.played"
#define	SYM_RECORDED	"audio.recorded"
#define	SYM_CREATED	"audio.created"
#define	SYM_EXTENSION	"audio.extension"
#define	SYM_FORMAT	"audio.encoding"
#define	SYM_OFFSET	"audio.offset"
#define	SYM_BUFFER	"audio.buffer"
#define	SYM_LANGUAGE	"session.language"
#define	SYM_CALLTYPE	"session.calltype"
#define	SYM_CALLFWD	"session.callfwd"
#define	SYM_TRIM	"audio.trim"
#define	SYM_VOICE	"session.voice"
#define	SYM_VOLUME	"audio.volume"
#define	SYM_PLAYWAIT	"audio.timeout"
#define	SYM_ERROR	"script.error"
#define	SYM_CALLER	"session.callerid"
#define	SYM_DIALED	"session.calledid"
#define	SYM_STATE	"session.state"
#define	SYM_CLID	"pstn.clid"
#define	SYM_CID		"pstn.clid"
#define	SYM_ANI		"pstn.clid"
#define	SYM_DNID	"pstn.dnid"
#define	SYM_DNIS	"pstn.dnid"
#define	SYM_DID		"pstn.dnid"
#define	SYM_NAME	"pstn.name"
#define	SYM_REDIRECT	"pstn.redirect"
#define	SYM_RINGID	"pstn.ringid"
#define SYM_INFODIGITS	"pstn.infodigits"
#define	SYM_APPL	"application.name"
#define	SYM_GID		"session.id"
#define	SYM_MEMBER	"policy.member"
#define	SYM_PORTID	"session.portid"
#define	SYM_DURATION	"session.duration"
#define	SYM_SERVICE	"server.state"
#define	SYM_SCHEDULE	"server.schedule"
#define	SYM_USER	"server.user"
#define	SYM_ID		"driver.id"
#define	SYM_CARD	"driver.card"
#define	SYM_BANK	"driver.bank"
#define	SYM_SPAN	"driver.span"
#define	SYM_PORTS	"server.ports"
#define	SYM_XML		"session.xml"
#define	SYM_LOCKFILE	"session.lockfile"
#define	SYM_STARTDATE	"session.startdate"
#define	SYM_STARTTIME	"session.starttime"
#define	SYM_USED	"server.used"
#define	SYM_FREE	"server.free"
#define	SYM_VERSION	"server.version"
#define	SYM_RELEASE	"server.release"
#define	SYM_SERVER	"server.software"
#define	SYM_NODE	"server.node"
#define	SYM_SCRIPTS	"server.scripts"
#define	SYM_PROMPTS	"server.prompts"
#define	SYM_POLICIES	"server.policies"
#define	SYM_DIALER	"session.dialing"
#define	SYM_DIALING	"session.dialplan"
#define	SYM_EXTNUMBER	"session.extension"
#define	SYM_POLICY	"session.policyid"
#define	SYM_START	"session.home"
#define	SYM_BASE	"session.base"
#define	SYM_DATE	"session.date"
#define	SYM_TIME	"session.time"
#define	SYM_COUNT	"session.count"
#define	SYM_DIGITS	"session.digits"
#define	SYM_JOINID	"session.joinid"
#define	SYM_PICKUP	"session.pickupid"
#define	SYM_RECALL	"session.recallid"
#define	SYM_TRUNK	"session.trunkid"
#define	SYM_TRANSFER	"session.transferid"
#define	SYM_INTERCOM	"session.intercomid"
#define	SYM_NOTIFYTEXT	"session.notifytext"
#define	SYM_NOTIFYTYPE	"session.notifytype"
#define SYM_EVENTID     "session.eventsenderid"
#define SYM_EVENTMSG    "session.eventsendermsg"
#define	SYM_HOME	"session.home"
#define	SYM_LOGIN	"session.loginid"
#define	SYM_PARENT	"session.parent"
#define	SYM_STARTID	"session.startid"
#define	SYM_RINGS	"pstn.rings"
#define SYM_ASRCONF	"asr.confidence"
#define SYM_ASRRESULT	"asr.result"

#define	REDIRECT_BUSY	"fwdbusy"
#define	REDIRECT_NA	"fwdna"
#define	REDIRECT_FWD	"forward"
#define	REDIRECT_RECALL	"recall"

extern char service[65];
extern Network network;
extern Scheduler scheduler;
extern int tgipipe[2];
extern int mainpid;
extern char df_used[4], df_free[4];
extern bool restart_server, upflag;

#ifdef CCXX_NAMESPACES
};
#endif
