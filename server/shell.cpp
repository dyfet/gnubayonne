// Copyright (C) 2000-2001 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <cc++/audio2.h>
#include <cc++/script3.h>
#include <cc++/slog.h>
#include <cc++/process.h>
#include <cc++/url.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/utsname.h>
#ifdef	linux
#include <sys/vfs.h>
#endif
#ifdef	__FreeBSD__
#include <sys/param.h>
#include <sys/mount.h>
#endif
#include <grp.h>
#include <climits>
#include "ivrconfig.h"

#ifdef	HAVE_EXECINFO_H
#include <execinfo.h>
#endif

extern char **environ;

#ifdef	COMMON_OST_NAMESPACE
namespace ost {
using namespace std;
#endif

static int iop, pid = 0;
static bool waiting = false;
static bool exited = false; 
static char *trapmode[2] = {NULL, NULL};
static int fifo = -1, rpc = -1, pkt = -1, nl = false; 
static int oddeven = 0;
static char *policies[65];
static unsigned huntcount = 0;
static unsigned prefixcount = 0;
static unsigned policycount = 0;
static struct sockaddr_un server_addr;
static char packet[256];
bool isUser(const char *id);

#pragma pack(1)

typedef struct {
        time_t  update;
        char name[16];
        struct in_addr addr;
        unsigned long uptime;
        unsigned long calls;
        unsigned char version;
        unsigned char buddies;
        unsigned char spansize;
        unsigned short ports;
        unsigned char service;
        unsigned char dialing;
        char schedule[16];
        char stat[840];
}       statnode_t;

#pragma pack()

static void sysExit(int code)
{
	remove(packet);
	if(fifo > -1)
		close(fifo);

	if(nl)
		cout << "\r\n";
	cout.flush();
	std::exit(code);
}

static RETSIGTYPE fault(int signo)
{
#ifdef	HAVE_EXECINFO_H
        const int maxTrace = 1000;
        void* buffer[maxTrace];
        int nTrace = backtrace ( buffer, maxTrace );
        char** trace = backtrace_symbols ( buffer, nTrace );

        if ( trace ) {
                for ( int i = 0; i < nTrace; ++i ) {
                slog(Slog::levelDebug) << "trace(" << i << "): "
                        << trace[i] << endl;
                }
        }
        // free memory
        free( trace );
#endif
	Process::setPosixSignal(SIGABRT, SIG_DFL);
	Process::setPosixSignal(SIGSEGV, SIG_DFL);
	sysExit(signo);
}

static RETSIGTYPE handler(int signo)
{
	switch(signo)
	{
	case SIGINT:
	case SIGHUP:
		if(exited)
			return;
		alarm(0);
		trapmode[oddeven] = "exit";
		break;
	case SIGALRM:
		if(!trapmode[oddeven])
			trapmode[oddeven] = "timeout";
		waiting = false;
		break;
	case SIGUSR2:
		if(!trapmode[oddeven] && waiting)
			trapmode[oddeven] = "failure";
		waiting = false;
		break;
	case SIGUSR1:
		if(!trapmode[oddeven] && waiting)
		{
			waiting = false;
			trapmode[oddeven] = "success";
		}
		break;
	}
}

static unsigned keyindex(const char *keyword)
{
        unsigned key = 0;

        while(*keyword)
                key ^= key << 1 ^ (*(keyword++) & 0x1f);

        return key % SCRIPT_INDEX_SIZE;
}

static bool binhex(const char *msg)
{
        const int linelen = 72;
        ifstream src;
        int i, c, lp = 0;
        bool eofmark = false;
        unsigned char input[3], output[5];
        static unsigned char table[] =
                "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

        src.open(msg);
        if(!src.is_open())
                return false;

        while(!eofmark)
        {
                memset(input, 0, sizeof(input));
                for(i = 0; i < 3; ++i)
                {
                        c = src.get();
                        if(c == EOF)
                        {
                                eofmark = true;
                                break;
                        }
                        input[i] = (unsigned char)c;
                }
                if(i > 0)
                {
                        output[0] = table[input[0] >> 2];
                        output[1] = table[((input[0] & 3) << 4) | (input[1] >> 4)];
                        output[2] = table[((input[1] & 0x0f) << 2) | (input[2] >> 6)];
                        output[3] = table[input[2] & 0x3f];
                }
                switch(i)
                {
                case 2:
                        output[3] = '=';
                        break;
                case 1:
                        output[2] = '=';
                }
                cout << output;
                lp += strlen((char *)output);
                if(lp >= linelen)
                {
                        cout << '\r' << endl;
                        lp = 0;
                }
        }
        if(lp)
                cout << '\r' << endl;

        src.close();
	return true;
}

class KeyPaths : public Keydata
{
public:
	KeyPaths();
};

class KeyMemory : public Keydata
{
public:
	KeyMemory();
};

class KeyServer : public Keydata
{
public:
        KeyServer();
};

class KeyPolicy : public Keydata
{
public:
	KeyPolicy();
};

class Cmd: public ScriptRuntime
{
public:
	Cmd();
};

class Interp : public ScriptInterp, public TimerPort
{
private:
	friend class Cmd;
	enum
	{
		HDR_NONE,
		HDR_SENT,
		HDR_DONE
	}	hdrState;

	const char	*tags[256];
	unsigned tagcount;
	unsigned session, sessions;

protected:
	ScriptImage *dynamic;
	time_t synctimer, exittimer, starttime;

	void commit(Symbol *sym);

public:
	static bool isActiveUser(ScriptInterp *interp, const char *v);
	static bool isHunt(ScriptInterp *interp, const char *v);
	static bool isPolicy(ScriptInterp *interp, const char *v);
	static bool isDriver(ScriptInterp *interp, const char *v);
	static bool hasVoice(ScriptInterp *interp, const char *v);
	static bool hasSysVoice(ScriptInterp *interp, const char *v);
	static bool hasAltVoice(ScriptInterp *interp, const char *v);
	static bool hasAppVoice(ScriptInterp *interp, const char *v);
	static bool hasSysPrompt(ScriptInterp *interp, const char *v);
	static bool hasVarPrompt(ScriptInterp *interp, const char *v);

protected:
	void input(void);
	bool getLogical(const char *opt);
	timeout_t getMSTimeout(const char *opt);
	timeout_t getSecTimeout(const char *opt);
	timeout_t getTimeout(const char *kwd);
	const char *getPolicy(const char *grp);
	bool sendPacket(const char *msg);
	bool getFifo(void);
	bool sendmail(void);
	bool endmail(void);
	bool scrHeader(void);
	bool scrEcho(void);
	bool scrTag(void);
	bool endTag(void);
	bool begTag(void);
	bool scrSchedule(void);
	bool scrService(void);
	bool scrSleep(void);
	bool scrSync(void);
	bool scrTest(void);
	bool scrRestart(void);
	bool scrCompile(void);
	bool scrStart(void);
	bool scrHangup(void);
	bool scrStop(void);
	bool scrBusy(void);
	bool scrIdle(void);
	bool scrInsert(void);

	bool exit(void)
		{sysExit(0); return true;};

public:
	Interp(Cmd *cmd, char *path);

	void advance(void)
		{ScriptInterp::advance();};

        bool signal(const char *sigid)
                {return ScriptInterp::signal(sigid);};

	bool getExit(void);
	bool getSync(void);
	bool delay(void);
};

KeyServer::KeyServer() :
Keydata("/bayonne/server")
{
	static Keydata::Define defpaths[] = {
	{"group", "bayonne"},
	{"token", "&"},
	{NULL, NULL}};

	struct group *grp;

	load("~bayonne/server");
	load(defpaths);

	grp = getgrnam(getLast("group"));
	if(grp)
		setgid(grp->gr_gid);
	endgrent();
}

KeyMemory::KeyMemory() :
Keydata("/bayonne/memory")
{
        static Keydata::Define defpaths[] = {
        {"symbols", "64"},
        {"page", "1024"},
        {"users", "1000"},
        {"prefs", "256"},
        {NULL, NULL}};

        load(defpaths);
}

KeyPolicy::KeyPolicy() :
Keydata("/bayonne/trunks")
{
	static Keydata::Define defpaths[] = {
                {"answer", "1"},
                {"accept", "1"},
                {"rpc", "1"},
                {"hangup", "100"},
                {"siezetime", "12"},
                {"ringtime", "7"},
                {"flash", "200"},
                {"dialtone", "2400"},
                {"dialspeed", "160"},
                {"volume", "80"},
                {"callerid", "1500"},
                {"pickup", "500"},
                {"requests", "hangup"},
                {"select", "last"},
                {"threashold", "0"},
                {"ready", "1000"},
                {"idletime", "600"},
                {"analysis", "16"},
                {"international", "011"},
                {"national", "1"},
                {"dialmode", "dtmf"},
                {"mindigits", "0"},
                {"mdigtimeout", "3"},
                {NULL, NULL}};

	load(defpaths);
}	

KeyPaths::KeyPaths() :
Keydata("/bayonne/paths")
{ 
        static Keydata::Define defpaths[] = {
        {"libpath", "/usr/lib/bayonne"},
        {"libexec", "/usr/lib"},
        {"tgipath", "/usr/libexec/bayonne:/bin:/usr/bin"},
        {"datafiles", "/var/lib/bayonne"},
        {"scripts", "/usr/share/aascripts"},
        {"prompts", "/usr/share/aaprompts"},
	{"wrappers", "/usr/share/aawrappers"},
        {"spool", "/var/spool/bayonne"},
        {"runfiles", "/var/run/bayonne"},
        {"precache", "/var/bayonne/cache"},
        {"config", "bayonne runtime configuration"},
        {NULL, NULL}};                                                         

	load("~bayonne/paths");
	load(defpaths);
}                                                    

KeyPaths keypaths;
KeyMemory keymemory;
KeyServer keyserver;
KeyPolicy keypolicy;
statnode_t node;

#ifdef	HAVE_REALPATH

bool permitAudioAccess(const char *path, bool write)
{
	char *cp, *dp, *ext;

	char source[PATH_MAX];
	char target[PATH_MAX];
	const char *var = keypaths.getLast("datafiles");
	const char *alt = keypaths.getLast("altprefix");
	const char *prompts = keypaths.getLast("prompts");
	unsigned lalt, lprompts, lvar;

	if(!alt)
		alt = keypaths.getLast("altprompts");
		
	snprintf(source, sizeof(source), "%s", path);
	cp = strrchr(source, '/');
	if(!cp)
		return false;

	*cp = 0;

	if(!realpath(source, target))
		return false;

	if(!isDir(target))
		return false;

	cp = strrchr(target, '/');
	if(!cp)
		return false;

	if(write)
	{
		ext = strrchr(path, '.');
		if(!ext)
			return false;

		if(stricmp(ext, ".au") &&
		   stricmp(ext, ".wav") &&
		   stricmp(ext, ".al") &&
		   stricmp(ext, ".ul") &&
		   stricmp(ext, ".snd") &&
		   stricmp(ext, ".raw") &&
		   stricmp(ext, ".vox") &&
		   stricmp(ext, ".gsm"))
			return false;
	}

	++cp;

	if(!stricmp(cp, ".bayonne"))
		return true;

	if(*cp == '.')
		return false;

	realpath(alt, source);
	alt = source;
	lalt = strlen(alt);

	if(!strnicmp(alt, target, lalt))
	{
		if(target[lalt] == '/' || !target[lalt])
			return true;
		else
			return false;
	}

	if(!write && !strnicmp("/var/", target, 5))
		return true;

	realpath(var, source);
	var = source;
	lvar = strlen(var);

	if(!strnicmp(var, target, lvar))
	{
		if(target[lvar] != '/')
			return false;

		++lvar;
		if(target[lvar] == '.')
			return false;

		return true;
	}

	if(write)
		return false;

	realpath(prompts, source);
	prompts = source;
	lprompts = strlen(prompts);

	if(!strnicmp(prompts, target, lprompts))
	{
		if(target[lprompts] != '/')
			return false;

		return true;
	}

	return true;
}

bool permitFileAccess(const char *path)
{
	char *cp, *dp, *text;

	char source[PATH_MAX];
	char target[PATH_MAX];
	const char *var = keypaths.getLast("datafiles");
	unsigned lvar;

	snprintf(source, sizeof(source), "%s", path);
	cp = strrchr(source, '/');
	if(!cp)
		return false;

	*cp = 0;
	if(!realpath(source, target))
		return false;

	if(!isDir(target))
		return false;

	cp = strrchr(target, '/');
	if(!cp)
		return false;

	++cp;

	if(!stricmp(cp, ".bayonne"))
		return true;

	realpath(var, source);
	var = source;
	lvar = strlen(var);

	if(!strnicmp(var, target, lvar))
	{
		if(target[lvar] != '/')
			return false;
		++lvar;
		if(target[lvar] == '.')
			return false;

		return true;
	}
	else
		return false;
}

#else

bool permitFileAccess(const char *path)
{
	if(strstr(path, ".."))
		return false;
	
	if(*path == '/')
	{
		if(strstr(path, "/.bayonne/"))
			return true; 
		return false;
	}

	if(*path == '.')
		return false;

	return true;
}

bool permitAudioAccess(const char *path, bool write)
{
	
	if(strstr(path, ".."))
		return false;

	if(*path == '.')
		return false;

	if(!write)
		return true;

	if(*path == '/')
	{
		if(!strstr(path, "/.bayonne/"))
			return false;
	}

	path = strrchr(path, '.');

	if(!path)
		return false;

	if(!stricmp(path, ".au"))
		return true;

	if(!stricmp(path, ".snd"))
		return true;

	if(!stricmp(path, ".al"))
		return true;

	if(!stricmp(path, ".wav"))
		return true;

	if(!stricmp(path, ".raw"))
		return true;

	if(!stricmp(path, ".ul"))
		return true;

	if(!stricmp(path, ".gsm"))
		return true;

	return false;
}

#endif

Interp::Interp(Cmd *cmd, char *fname) :
TimerPort(), ScriptInterp()
{
	int len = 0;

	if(!attach(cmd, "shell"))
	{
		slog(Slog::levelCritical) << fname << "; cannot execute" << endl;
		sysExit(-1);
	}
	session = sessions = 0;
	//autoloop(false);
	synctimer = 0;
	exittimer = 0;
	time(&starttime);
	hdrState = HDR_NONE;
	tagcount = 0;
	dynamic = NULL;
	pkt = socket(AF_UNIX, SOCK_DGRAM, 0);
	snprintf(packet, sizeof(packet), "/tmp/.shell%d.pkt", getpid());
	setConst("server.reply", packet); 	

        memset(&server_addr, 0, sizeof(server_addr));
        server_addr.sun_family = AF_UNIX;
        strncpy(server_addr.sun_path, packet, sizeof(server_addr.sun_path));

#ifdef  __SUN_LEN
        len = sizeof(server_addr.sun_len) + strlen(server_addr.sun_path)
                        + sizeof(addr.sun_family) + 1;

        addr.sun_len = len;
#else
        len = strlen(server_addr.sun_path) + sizeof(server_addr.sun_family) + 1;
#endif

	remove(packet);
        if(pkt > -1)
        {
                if(bind(pkt, (struct sockaddr *)&server_addr, len))
                {
                        slog(Slog::levelError) << "packet interface failed; path=" << packet << endl;
                        pkt = -1;
                        remove(packet);
                }
        }
	
}

bool Interp::sendPacket(const char *msg)
{
	static char id, cnt = 0;
        socklen_t clen = sizeof(server_addr);
	sockaddr_un reply_addr;
	char *cp;
	int len;

	char packet[512];

	snprintf(packet + 2, sizeof(packet) - 2, "%s", msg);
	if(!cnt)
		++cnt;

	packet[0] = id = cnt++;
	packet[1] = '-';

	len = ::sendto(pkt, packet, strlen(packet + 2) + 3, 0, 
		(struct sockaddr *)&server_addr, clen);

	if(len < 1)
		return false;

	for(;;)
	{
		clen = sizeof(reply_addr);
		len = ::recvfrom(pkt, packet, sizeof(packet), 0, 
			(struct sockaddr *)&reply_addr, &clen);
		if(len < 2)
			return false;
		if(packet[0] != id)
			continue;
		switch(packet[1])
		{
		case '0':
			return false;
		case 'S':
			cp = strchr(cp, '=');
			setSymbol(packet, ++cp);
			return true;
		default:
			return true;
		}
	}
}

void Interp::commit(Symbol *sym)
{
	char *cp;
	char cmd[128];
	char urlbuf[128];

	if(!strnicmp(sym->id, "global.", 7))
	{
		commit(sym);
		return;
	} 

	if(!getFifo())
		return;

	urlEncode(sym->data, urlbuf, sizeof(urlbuf));
	cp = strchr(sym->id, '.') + 1;
	snprintf(cmd, sizeof(cmd), "global %s %s\n", cp, urlbuf);
	write(fifo, cmd, strlen(cmd));
}

bool Interp::hasVoice(ScriptInterp *interp, const char *v)
{
        const char *prompts = keypaths.getLast("prompts");
        char buf[256];
        
        snprintf(buf, sizeof(buf), "%s/%s", prompts, v);
        if(isDir(buf))
                return true;
        
        return false;
}

bool Interp::hasVarPrompt(ScriptInterp *interp, const char *v)
{
	char buf[256];
	const char *ext;
	const char *prefix = interp->getKeyword("prefix");

	ext = strchr(v, '/');
	if(!ext)
		ext = v;

	if(strchr(ext, '.'))
		ext = "";
	else
		ext = ".au";

	if(prefix)
		snprintf(buf, sizeof(buf), "%s/%s%s", prefix, v, ext);
	else
		snprintf(buf, sizeof(buf), "%s%s", v, ext);
	
	if(isFile(buf))
		return true;

	return false;
}
bool Interp::hasSysPrompt(ScriptInterp *interp, const char *v)
{
        char buf[256];
        const char *ext;
        const char *prompts = keypaths.getLast("prompts");
        const char *prefix = interp->getKeyword("prefix");
        if(!prefix)
                prefix = interp->getKeyword("sys");

        ext = strchr(v, '/');
        if(!ext)
                ext = v;

        if(strchr(ext, '.'))
                ext = "";
        else
                ext = ".au";

        if(prefix)
                snprintf(buf, sizeof(buf), "%s/sys/%s/%s%s",
                        prompts, prefix, v, ext);
        else
                snprintf(buf, sizeof(buf), "%s/%s%s",
                        prompts, v, ext);

        if(isFile(buf))
                return true;

        return false;
}

bool Interp::hasSysVoice(ScriptInterp *interp, const char *v)
{
        const char *prompts = keypaths.getLast("prompts");
        char buf[256];
        
        snprintf(buf, sizeof(buf), "%s/sys/%s", prompts, v);
        if(isDir(buf))
                return true;
 
        return false;
}

bool Interp::hasAltVoice(ScriptInterp *interp, const char *v)
{
        const char *prompts = keypaths.getLast("altprompts");
        char buf[256];
        
        if(!prompts)
                prompts = keypaths.getLast("prompts");
        
        snprintf(buf, sizeof(buf), "%s/%s", prompts, v);
        if(isDir(buf))
                return true;
 
        return false;
}

bool Interp::hasAppVoice(ScriptInterp *interp, const char *v)
{
        const char *prompts = keypaths.getLast("prompts");
        Name *scr = interp->getName();
        char *cp;
        char buf[256];
        char name[65];
        
        snprintf(name, sizeof(name), "%s", scr->name);
        cp = strchr(name, ':');
        if(cp)
                *cp = 0;
        
        snprintf(buf, sizeof(buf), "%s/%s/%s", prompts, v, name);
        if(isDir(buf))
                return true;
        
        return false;
}

bool Interp::isDriver(ScriptInterp *interp, const char *v)
{
	char envname[65];
	snprintf(envname, sizeof(envname), "driver/%s.index", v);
	if(interp->getSymbol(envname))
		return true;
	return false;
}

bool Interp::isPolicy(ScriptInterp *interp, const char *v)
{
	unsigned count = 0;

	while(policies[count])
	{
		if(!stricmp(policies[count++], v))
			return true;
	}
	return false;
}

bool Interp::isActiveUser(ScriptInterp *interp, const char *v)
{
	char path[65];

	snprintf(path, sizeof(path), "users/%s", v);
	if(isFile(path))
		return true;
	return false;
}

bool Interp::isHunt(ScriptInterp *interp, const char *v)
{
        char path[65];
        
        snprintf(path, sizeof(path), "hunting/%s", v);
        if(isFile(path))
                return true;
        return false;  
}

void Interp::input(void)
{
}

timeout_t Interp::getMSTimeout(const char *opt)
{
        char *end;
        char decbuf[4];
        long value;
        unsigned len;

        if(!opt)
                opt = "0";

        value = strtol(opt, &end, 10) * 1000;
        if(*end == '.')
        {
                strncpy(decbuf, ++end, 3);
                decbuf[4] = 0;
                len = strlen(decbuf);
                while(len < 3)
                        decbuf[len++] = '0';
                value += strtol(decbuf, &end, 10);
        }

        switch(*end)
        {
        case 'h':
        case 'H':
                return value * 3600;
        case 'm':
        case 'M':
                if(end[1] == 's' || end[1] == 'S')
                        return value / 1000;
                return value * 60;
        default:
                return value / 1000;
        }
}


timeout_t Interp::getSecTimeout(const char *opt)
{
        char *end;
        char decbuf[4];
        long value;
        unsigned len;

        if(!opt)
                opt = "0";

        value = strtol(opt, &end, 10) * 1000;
        if(*end == '.')
        {
                strncpy(decbuf, ++end, 3);
                decbuf[4] = 0;
                len = strlen(decbuf);
                while(len < 3)
                        decbuf[len++] = '0';
                value += atol(decbuf);
        }

        switch(*end)
        {
        case 'h':
        case 'H':
                return value * 3600;
        case 'm':
        case 'M':
                if(end[1] == 's' || end[1] == 'S')
                        return value / 1000;
                return value * 60;
        default:
                return value;
        }
}

bool Interp::getLogical(const char *str)
{
        if(*str == '.')
                ++str;
        switch(*str)
        {
        case '0':
        case 'f':
        case 'F':
        case 'N':
        case 'n':
                return false;
        }
        return true;
}

timeout_t Interp::getTimeout(const char *keywd)
{
        ScriptImage *img = getImage();
        if(keywd)
                keywd = getKeyword(keywd);
        if(!keywd)
                keywd = getValue("86400");
        return getSecTimeout(keywd);
}

const char *Interp::getPolicy(const char *grp)
{
	unsigned count = 0;

	while(count < policycount)
	{
		if(!stricmp(policies[count++], grp))
			return grp;
	}
	return NULL;
}

bool isUser(const char *id)
{
	unsigned long num = atoi(id);

	if(!isdigit(*id) && stricmp(id, "admin"))
		return false;

	switch(node.dialing)
	{
	case 1:
		if(num > 9)
			return false;
		break;
	case 2:
		if(num < 10 || num > 99)
			return false;
		break;
	case 3:
		if(num < 100 || num > 999)
			return false;
		break;
	}
	return true;
}

bool Interp::getFifo(void)
{
	if(fifo > -1)
		return true;

	fifo = ::open(getSymbol("server.control"), O_WRONLY | O_NONBLOCK);
	if(fifo > -1)
		return true;

	if(!signal("down"))
		error("no-fifo");
	return false;
}

bool Interp::scrEcho(void)
{
	const char *opt;
	const char *member = getMember();

	if(!member)
		member = "";

	if(hdrState == HDR_SENT)
		cout << "\r\n";

	hdrState = HDR_DONE;
	while(NULL != (opt = getValue(NULL)))
		cout << opt;

	if(!stricmp(member, "text"))
		nl = true;
	else
	{
		nl = false;
		cout << "\r\n";
	}
	advance();
	return true;
}

bool Interp::scrTag(void)
{
	const char *member = getMember();
	bool text = false;
	Line *line = getLine();
	const char *opt;
	int argc = 0;

	if(hdrState == HDR_SENT)
		cout << "\r\n";
	
	hdrState = HDR_DONE;

	if(!member)
	{
		error("no-tag");
		return true;
	}

	cout << "<" << member;

	while(argc < line->argc)
	{
		opt = line->args[argc++];
		if(*opt != '=')
		{
			text = true;
			continue;
		}

		++opt;
		cout << " " << opt << "=\"" << getContent(line->args[argc++]) << "\"";
	}
	if(!text)
		cout << "/";
	cout << ">";
	argc = 0;

	while(argc < line->argc && text)
	{
		opt = line->args[argc++];
		if(*opt == '=')
		{
			++argc;
			continue;
		}
		cout << getContent(opt);
	}

	if(text)
		cout << "</" << member << ">";
	nl = true;
	advance();
	return true;
}

bool Interp::begTag(void)
{
	const char *member = getMember();
	int argc = 0;
	Line *line = getLine();
	const char *opt;

	if(hdrState == HDR_SENT)
		cout << "\r\n";

	hdrState = HDR_DONE;

	if(!member)
	{
		error("no-tag");
		return true;
	}

	tags[tagcount++] = member;
	cout << "<" << member;

	while(argc < line->argc)
	{
		opt = line->args[argc++];
		if(*opt != '=')
			continue;

		cout << " " << ++opt << "=\"" << getContent(line->args[argc++]) << "\"";
	}
	cout << ">";
	nl = true;		
	advance();
	return true;
}

bool Interp::sendmail(void)
{
	int iop[2];
	char *argv[65];
	int argc = 1;
	const char *opt;
	bool adv = true;
	
	argv[0] = "/usr/lib/sendmail";

	if(pid)
	{
		if(endmail())
			return true;
		adv = false;
	}

	while(argc < 64 && (NULL != (opt = getValue(NULL))))
		argv[argc++] = (char *)opt;

	argv[argc] = NULL;

	if(nl)
		cout << "\r\n";
	nl = false;	
	cout.flush();
	::pipe(iop);
	pid = vfork();
	if(!pid)
	{
		dup2(iop[0], 0);
		::close(iop[0]);
		::close(iop[1]);
		execv(argv[0], argv);
		::exit(-1);
	}		
	if(pid < 0)
	{
		pid = 0;
		error("cannot-fork");
		return true;
	}
	hdrState = HDR_NONE;
	dup2(iop[1], 1);
	::close(iop[0]);
	::close(iop[1]);
	if(adv)
		advance();
	return true;
}

bool Interp::endmail(void)
{
	char msg[16];
	int status;

	if(nl)
		cout << "\r\n";
	nl = false;

	if(!pid)
	{
		error("no-mailer");
		return true;
	}

	cout.flush();
	close(1);

#ifdef	__FreeBSD__
	wait4(pid, &status, 0, NULL);
#else
	waitpid(pid, &status, 0);
#endif
	status = WEXITSTATUS(status);
	dup2(iop, 1);
	if(status)
	{
		snprintf(msg, sizeof(msg), "sendmail-%d", status);
		error(msg);
	}
	else
		advance();
	return true;
}

bool Interp::endTag(void)
{
	const char *member = getMember();
	if(!member)
		member = "";

	if(hdrState == HDR_SENT)
		cout << "\r\n";

	hdrState = HDR_DONE;

	while(tagcount)
	{
		if(stricmp(member, tags[--tagcount]))
			cout << "</" << tags[tagcount] << ">";
		else
			break;
	}
	if(*member)
		cout << "</" << member << ">";
	if(tagcount)
		--tagcount;
	advance();
	nl = true;
	return true;
}

bool Interp::scrTest(void)
{
        char cmd[65];
        const char *mem = getMember();
	unsigned len;

        if(!mem)
                mem = getValue("up");

        if(!getFifo())
                return true;

        snprintf(cmd, sizeof(cmd), "test %s", mem);
	len = strlen(cmd);
	while(NULL != (mem = getValue(NULL)) && len < 59)
	{
		cmd[len++] = ' ';
		snprintf(cmd + len, sizeof(cmd) - len - 1, "%s", mem);
		len += strlen(mem);
	}
	len = strlen(cmd);
	cmd[len++] = '\n';
	cmd[len] = 0;
	
        write(fifo, cmd, strlen(cmd));
        advance();
        return true;
}

bool Interp::scrInsert(void)
{
	const char *mem = getMember();
	struct stat ino;
	AudioFile af;
	Audio::Encoding fmt;
	const char *name = getKeyword("name");
	const char *path = getValue(getKeyword("file"));
	const char *ext = getKeyword("extension");
	const char *prefix = getKeyword("prefix");
	time_t duration;
	static unsigned count = 0;
	char brd[64];
	char buffer[256];

	if(!mem)
		mem = "msg";

	if(!stricmp(mem, "multipart") || !stricmp(mem, "mixed") || !stricmp(mem, "mime"))
	{
		if(nl)
			cout << "\r\n";

		if(hdrState == HDR_DONE)
			cout << "\r\n";

		hdrState = HDR_SENT;
		time(&duration);
		snprintf(brd, sizeof(brd), "++---%ld-%d---++", duration, ++count);
		cout << "Mime-Version: 1.0\r\n";
		cout << "Content-Type: multipart/mixed; boundry=\"" << brd << "\"\r\n";
		setSymbol("script.boundry", brd);
		cout << "Content-Transfer-Encoding: 7bit\r\n\r\n";
		cout << "This is a multi-part message in MIME format.\r\n";
		cout << brd << "\r\n";
		nl = false;
		advance();
		cout.flush();
		return true;
	}

	if(!stricmp(mem, "inline"))
	{
		if(nl)
			cout << "\r\n";
		cout << "Content-Type: text/plain; charset=us-ascii\r\n";
		cout << "Content-Transfer-Encoding: 7bit\r\n";
		hdrState = HDR_SENT;
		cout.flush();
		advance();
		nl = false;
		return true;
	}

	if(!stricmp(mem, "end"))
	{
		if(nl)
			cout << "\r\n";
		cout << getSymbol("script.boundry") << "\r\n";
		hdrState = HDR_SENT;
		nl = false;
	}

	if(!ext)
		ext = "";

	if(prefix)
		snprintf(buffer, sizeof(buffer), "%s/%s%s", prefix, path, ext);
	else
		snprintf(buffer, sizeof(buffer), "%s%s", path, ext);

	path = buffer;

	ext = strrchr(path, '/');
	if(ext)
		ext = strrchr(ext, '.');
	else
		ext = strrchr(path, '.');

	if(!ext)
		ext = "";

	if(!permitAudioAccess(path, false))
	{
		error("invalid-path");
		return true;
	}

	if(stat(path, &ino))
	{
		error("cannot-access");
		return true;
	}

	af.open(path);
	if(!af.isOpen())
	{
		af.close();
		error("cannot-open");
		return true;
	}

	fmt = af.getEncoding();
	af.setPosition();
        duration = af.getPosition() / af.getSampleRate();
        af.close();

	if(nl)
	{
		cout << "\r\n";
		nl = false;
	}
	hdrState = HDR_SENT;

	cout << "\r\n--MessageBoundry\r\n";

        switch(fmt)
        {
        case Audio::okiADPCM:
        case Audio::g721ADPCM:
        case Audio::voxADPCM:
                cout << "Content-Type: Audio/32KADPCM" << '\r' << endl;
                break;
        default:
                if(!stricmp(ext, ".wav") || !stricmp(ext, ".wave"))
                        cout << "Content-Type: audio/x-wav\r\n";
                else
                        cout << "Content-Type: audio/basic\r\n";
        }
        cout << "Content-Transfer-Encoding: Base64\r\n";
        if(!stricmp(mem, "Name"))
        {
		cout << "Content-Disposition: inline; voice=Originator-Spoken-Name\r\n"; 
		cout << "Content-ID: " << getKeyword("id") << "\r\n";
        }
        else if(!stricmp(mem, "Forward"))
        {
		cout << "Content-Description: Forwarded Message Annotation\r\n"; 
		cout << "Content-Disposition: inline; voice=Voice-Message\r\n";
        }
        else
        {
		cout << "Content-Description: Bayonne Voice Message\r\n";
		cout << "Content-Disposition: inline; voice=Voice-Message; filename=" << path << "\r\n";
		cout << "Content-Duration: " << duration << "\r\n";
        }
	cout << "\r\n";
        binhex(path);
	cout.flush();
	advance();
	return true;
}

bool Interp::scrHangup(void)
{
	const char *mem = getMember();
	const char *id = getKeyword("id");
	char cmd[65];
	bool rtn;

	if(!id)
		id = getValue(NULL);

	if(!getFifo())
		return true;

	if(!mem)
	{
		// session mode cancel
		if(session)	
			id = getSymbol("session.id");
		else
			id = NULL;
		if(!id)
		{
			error("no-session");
			return true;
		}
		snprintf(cmd, sizeof(cmd), "rpc cancel %s\n", id);	
	}
	else if(!stricmp(mem, "port"))
	{
		if(!id)
		{
			error("no-port");
			return true;
		}
		snprintf(cmd, sizeof(cmd), "hangup %s\n", id);
	}
	else if(!stricmp(mem, "span"))
	{
		if(!id)
		{
			error("no-span");
			return true;
		}
		snprintf(cmd, sizeof(cmd), "span %s idle\n", id);
	}
	else if(!stricmp(mem, "card"))
	{
		if(!id)
		{
			error("no-card");
			return true;
		}
		snprintf(cmd, sizeof(cmd), "card %s idle\n", id);
	}
	else if(!stricmp(mem, "group"))
	{
		if(!id)
		{
			error("no-group");
			return true;
		}
		
		if(!getPolicy(id))
		{
			error("policy-invalid");
			return true;
		}
		snprintf(cmd, sizeof(cmd), "hangup %s\n", id);
	}
	else
	{
		error("hangup-unknown");
		return true;
	}
	if(sendPacket(cmd))
		advance();
	else
		error("hangup-invalid-port");
	return true;
}
	
bool Interp::scrStop(void)
{
	const char *mem = getMember();
	const char *id = getKeyword("id");
	unsigned count = 0;
	char cmd[65];

	if(!id)
		id = getValue(NULL);

	if(!getFifo())
		return true;

	if(!mem)
	{
		// session mode cancel
		if(session)	
			id = getSymbol("session.id");
		else
			id = NULL;
		if(!id)
		{
			error("no-session");
			return true;
		}
		snprintf(cmd, sizeof(cmd), "rpc cancel %s\n", id);	
	}
	else if(!stricmp(mem, "port"))
	{
		if(!id)
		{
			error("no-port");
			return true;
		}
		snprintf(cmd, sizeof(cmd), "hangup %s\n", id);
	}
	else if(!stricmp(mem, "span"))
	{
		if(!id)
		{
			error("no-span");
			return true;
		}
		snprintf(cmd, sizeof(cmd), "span %s stop\n", id);
	}
	else if(!stricmp(mem, "card"))
	{
		if(!id)
		{
			error("no-card");
			return true;
		}
		snprintf(cmd, sizeof(cmd), "card %s stop\n", id);
	}
	else if(!stricmp(mem, "group"))
	{
		if(!id)
		{
			error("no-group");
			return true;
		}
		
		if(!getPolicy(id))
		{
			error("policy-invalid");
			return true;
		}
		snprintf(cmd, sizeof(cmd), "hangup %s\n", id);
	}
	else
	{
		error("hangup-unknown");
		return true;
	}
	if(sendPacket(cmd))
		advance();
	else
		error("stop-unknown-port");
	return true;
}

bool Interp::scrIdle(void)
{
	const char *mem = getMember();
	const char *id = getKeyword("id");
	unsigned count = 0;
	char cmd[65];

	if(!id)
		id = getValue(NULL);

	if(!getFifo())
		return true;

	if(!mem)
	{
		error("no-target");
		return true;
	}
	else if(!stricmp(mem, "port"))
	{
		if(!id)
		{
			error("no-port");
			return true;
		}
		snprintf(cmd, sizeof(cmd), "idle %s\n", id);
	}
	else if(!stricmp(mem, "span"))
	{
		if(!id)
		{
			error("no-span");
			return true;
		}
		snprintf(cmd, sizeof(cmd), "span %s idle\n", id);
	}
	else if(!stricmp(mem, "card"))
	{
		if(!id)
		{
			error("no-card");
			return true;
		}
		snprintf(cmd, sizeof(cmd), "card %s idle\n", id);
	}
	else if(!stricmp(mem, "group"))
	{
		if(!id)
		{
			error("no-group");
			return true;
		}
		
		if(!getPolicy(id))
		{
			error("policy-invalid");
			return true;
		}
		snprintf(cmd, sizeof(cmd), "idle %s\n", id);
	}
	else
	{
		error("hangup-unknown");
		return true;
	}
	if(sendPacket(cmd))
		advance();
	else
		error("idle-unknown-port");
	return true;
}

bool Interp::scrBusy(void)
{
	const char *mem = getMember();
	const char *id = getKeyword("id");
	unsigned count = 0;
	char cmd[65];

	if(!id)
		id = getValue(NULL);

	if(!getFifo())
		return true;

	if(!mem)
	{
		error("no-session");
		return true;
	}
	else if(!stricmp(mem, "port"))
	{
		if(!id)
		{
			error("no-port");
			return true;
		}
		snprintf(cmd, sizeof(cmd), "busy %s\n", id);
	}
	else if(!stricmp(mem, "span"))
	{
		if(!id)
		{
			error("no-span");
			return true;
		}
		snprintf(cmd, sizeof(cmd), "span %s busy\n", id);
	}
	else if(!stricmp(mem, "card"))
	{
		if(!id)
		{
			error("no-card");
			return true;
		}
		snprintf(cmd, sizeof(cmd), "card %s busy\n", id);
	}
	else if(!stricmp(mem, "group"))
	{
		if(!id)
		{
			error("no-group");
			return true;
		}
		
		if(!getPolicy(id))
		{
			error("policy-invalid");
			return true;
		}
		snprintf(cmd, sizeof(cmd), "busy %s\n", id);
	}
	else
	{
		error("hangup-unknown");
		return true;
	}
	if(sendPacket(cmd))
		advance();
	else
		error("busy-unknown-port");
	return true;
}

bool Interp::scrStart(void)
{
	const char *mem = getMember();
	char cmd[512];
	char urlbuf[500];
	const char *id = getKeyword("id");
	const char *script = getKeyword("script");
	const char *grp = getKeyword("group");
	const char *opt, *tag;
	unsigned len, port;
	int argc = 0, count = 0;
	Line *line = getLine();
	timeout_t timeout;
	unsigned threashold = 0;
	
	if(!mem)
		mem = "port";

	if(!getFifo())
		return true;

	cmd[0] = 0;

	if(!strnicmp(mem, "in", 2) || !stricmp(mem, "ring"))
	{
		if(!id)
		{
			++count;
			id = getValue(NULL);
		}

		if(!script)
		{
			++count;
			script = getValue(NULL);
		}

		if(!id || !script)
		{
			error("start-options-missing");
			return true;
		}

		port = atoi(id);
		snprintf(cmd, sizeof(cmd), "ring %d %s", port, script);
	}
        else if(!stricmp(mem, "redirect"))
        {
                if(!id)
                {
                        ++count;
                        id = getValue(NULL);
                }

                if(!script)
                {
                        ++count;
                        script = getValue(NULL);
                }

                if(!id || !script)
                {
                        error("start-options-missing");
                        return true;
                }

                port = atoi(id);
                snprintf(cmd, sizeof(cmd), "redirect %d %s", port, script);

        }
        else if(!stricmp(mem, "request"))
        {
		timeout = getTimeout("timeout");
                if(!grp)
                {
                        ++count;
                        grp = getValue(NULL);
                }

                if(!script)
                {
                        ++count;
                        script = getValue(NULL);
                }

                if(!grp || !script)
                {
                        error("start-options-missing");
                        return true;
                }

                if(!getPolicy(grp))
                {
                        error("policy-invalid");
                        return true;
                }

                snprintf(cmd, sizeof(cmd), "request %d %s %s",
                        timeout, grp, script);
        }
	else if(!stricmp(mem, "group"))
	{
                if(!grp)
                {
                        ++count;
                        grp = getValue(NULL);
                }

                if(!script)
                {
                        ++count;
                        script = getValue(NULL);
                }

                if(!grp || !script)
                {
                        error("start-options-missing");
                        return true;
                }

		if(!getPolicy(grp))
		{
			error("policy-invalid");
			return true;
		}

		opt = getKeyword("limit");
		if(opt)
			threashold = atoi(opt);

                snprintf(cmd, sizeof(cmd), "start%d %s %s",
                        threashold, grp, script);
	}
	else if(!stricmp(mem, "port"))
	{
                if(!id)
		{
			++count;
                        id = getValue(NULL);
		}

                if(!script)
		{
			++count;
                        script = getValue(NULL);
		}

                if(!id || !script)
                {
                        error("start-options-missing");
                        return true;
                }

                port = atoi(id);
                snprintf(cmd, sizeof(cmd), "start %d %s", port, script);
	}
	else
	{
		error("start-unknown");
		return true;
	}

	if(!cmd[0])
	{
		error("unknown-start");
		return true;
	}
	len = strlen(cmd);
	while(len < sizeof(cmd) - 10 && argc < line->argc)
	{
		opt = line->args[argc++];
		if(!stricmp(opt, "=id") || !stricmp(opt, "=script") || !stricmp(opt, "=group") || !stricmp(opt, "timeout"))
		{
			argc += 2;
			continue;
		}
		if(*opt == '=')
		{
			tag = ++opt;
			opt = getContent(line->args[argc++]);
		}
		else if(*opt == '%' && !count)
		{
			tag = ++opt;
			opt = getSymbol(tag);
		}
		else
		{
			if(count)
				--count;
			opt = NULL;
		}

		++argc;
		if(!opt)
			continue;	

		urlEncode(opt, urlbuf, sizeof(urlbuf));
		snprintf(cmd + len, sizeof(cmd) - len - 1, " %s=%s", tag, urlbuf);
		len = strlen(cmd);				
	}
	cmd[len++] = '\n';
	cmd[len] = 0;
	if(sendPacket(cmd))
		advance();
	else
		error("start-unknown-port");
	return true;
}	

bool Interp::scrCompile(void)
{
        if(!getFifo())
                return true;

        write(fifo, "compile\n", 8);
        advance();
        return true;
}

bool Interp::scrRestart(void)
{
	if(!getFifo())
		return true;

	write(fifo, "restart\n", 8);
	::remove(getSymbol("server.control"));
	::close(fifo);
	fifo = -1;
	advance();
	return true; 
}

bool Interp::scrService(void)
{
        char cmd[65];
	const char *mem = getMember();

	if(!mem)
		mem = getValue("up");

        if(!getFifo())
                return true;

        snprintf(cmd, sizeof(cmd), "service %s\n", mem);
        write(fifo, cmd, strlen(cmd));
        advance();
        return true;
}

bool Interp::scrSchedule(void)
{
	char cmd[65];
	const char *mem = getMember();

	if(!mem)
		mem = getValue("");

	if(!getFifo())
		return true;

	snprintf(cmd, sizeof(cmd), "schedule %s\n", mem);
	write(fifo, cmd, strlen(cmd));
	advance();
	return true;
}

bool Interp::scrSync(void)
{
	timeout_t timer = getTimeout("time");
	time_t now;
	const char *mem = getMember();

	if(!mem)
		mem = "none";

	time(&now);

	if(!strnicmp(mem, "max", 3) || !stricmp(mem, "exit"))
	{
		if(timer)
			exittimer = starttime + (timer / 1000);
		else
			exittimer = 0;

		advance();
		return true;
	}

        if(!strnicmp(mem, "time", 4) || !stricmp(mem, "start"))
        {
                if(timer)
                        synctimer = starttime + (timer / 1000);
                else
                        synctimer = 0;
                advance();
                return true;
        }

        if(!stricmp(mem, "current"))
        {
                if(timer)
                        synctimer = now + (timer / 1000);
                else
                        synctimer = 0;
                advance();
                return true;
        }

        timer = timer - ((now - starttime) * 1000);
        if(timer < 1)
        {
                advance();
                return true;
        }

	setTimer(timer);
	return false;
}

bool Interp::scrSleep(void)
{
	timeout_t timer = getTimeout("maxTime");

	if(timer)
		setTimer(timer);

	return false;
}

bool Interp::scrHeader(void)
{
	const char *opt;
	const char *member = getMember();
	bool header = false;

	if(nl)
		cout << "\r\n";

	if(hdrState == HDR_DONE)
		cout << "\r\n";

	if(member)
	{
		header = true;
		cout << member << ": ";
	}

	hdrState = HDR_SENT;
	while(NULL != (opt = getValue(NULL)))
	{
		header = true;
		cout << opt;
	}

	nl = false;
	if(header)
		cout << "\r\n";
	advance();
	return true;
}

bool Interp::getSync(void)
{
	time_t now;
	time(&now);
	
	if(!synctimer)
		return false;

	if(now < synctimer)
		return false;

	synctimer = 0;
	return true;
}

bool Interp::getExit(void)
{
	time_t now;
	time(&now);

	if(!exittimer)
		return false;

	exittimer = 0;
	return true;
}

bool Interp::delay(void)
{
#ifdef	HAVE_POLL
	struct pollfd pfd[1];
#else
	struct timeval tv;
	fd_set sfd;
#endif
	timeout_t timeout = getTimer();

#ifdef	HAVE_POLL
	pfd[0].fd = rpc;
	pfd[0].events = POLLIN | POLLRDNORM;
	pfd[0].revents = 0;
#else
	if(rpc > -1)
	{
		FD_ZERO(&sfd);
		FD_SET(rpc, &sfd);
	}
#endif

	if(timeout == TIMEOUT_INF || !timeout)
	{
		endTimer();
		if(rpc > -1)
		{
#ifdef	HAVE_POLL
			poll(pfd, 1, 0);
			if(pfd[0].revents)
				input();
#else
			select(rpc + 1, &sfd, NULL, &sfd, NULL);
			if(FD_ISSET(rpc, &sfd))
				input();
#endif
		}
		return false;
	}

	Process::setInterruptSignal(SIGINT, &handler);
	Process::setInterruptSignal(SIGHUP, &handler);
	Process::setInterruptSignal(SIGALRM, &handler);
	if(waiting)
	{
		Process::setInterruptSignal(SIGUSR1, &handler);
		Process::setInterruptSignal(SIGUSR2, &handler);
	}

	if(rpc > -1)
	{
#ifdef	HAVE_POLL
		poll(pfd, 1, timeout);
		if(pfd[0].revents)
			input();
#else
		tv.tv_sec = timeout / 1000;
		tv.tv_usec = (timeout % 1000) * 1000;
		select(rpc + 1, &sfd, NULL, &sfd, &tv);
		if(FD_ISSET(rpc, &sfd))
			input();
#endif
	}
	else
		Thread::sleep(timeout);

	Process::setPosixSignal(SIGUSR1, &handler);
	Process::setPosixSignal(SIGUSR2, &handler);
	Process::setPosixSignal(SIGINT, &handler);
	Process::setPosixSignal(SIGHUP, &handler);
	Process::setPosixSignal(SIGALRM, &handler);

	return true;
}	
	

Cmd::Cmd() :
ScriptRuntime()
{
	static Script::Define interp[] = {
		{"header", false, (Method)&Interp::scrHeader,
			(Check)&ScriptChecks::chkHasArgs},
		{"echo", false, (Method)&Interp::scrEcho,
			(Check)&ScriptChecks::chkIgnore},
		{"tag", false, (Method)&Interp::scrTag,
			(Check)&ScriptChecks::chkIgnore},
		{"etag", false, (Method)&Interp::endTag,
			(Check)&ScriptChecks::chkNoArgs},
		{"btag", false, (Method)&Interp::begTag,
			(Check)&ScriptChecks::chkIgnore},
		{"schedule", false, (Method)&Interp::scrSchedule,
			(Check)&ScriptChecks::chkIgnore},
		{"service", false, (Method)&Interp::scrService,
			(Check)&ScriptChecks::chkIgnore},
		{"sleep", false, (Method)&Interp::scrSleep,
			(Check)&ScriptChecks::chkIgnore},
		{"sync", false, (Method)&Interp::scrSync,
			(Check)&ScriptChecks::chkIgnore},
		{"test", false, (Method)&Interp::scrTest,
			(Check)&ScriptChecks::chkIgnore},
		{"restart", false, (Method)&Interp::scrRestart,
			(Check)&ScriptChecks::chkNoArgs},
		{"compile", false, (Method)&Interp::scrCompile,
			(Check)&ScriptChecks::chkNoArgs},
		{"start", false, (Method)&Interp::scrStart,
			(Check)&ScriptChecks::chkHasArgs},
		{"stop", false, (Method)&Interp::scrStop,
			(Check)&ScriptChecks::chkIgnore},
		{"drop", false, (Method)&Interp::scrHangup,
			(Check)&ScriptChecks::chkIgnore},
		{"hangup", false, (Method)&Interp::scrHangup,
			(Check)&ScriptChecks::chkIgnore},
		{"busy", false, (Method)&Interp::scrBusy,
			(Check)&ScriptChecks::chkIgnore},
		{"idle", false, (Method)&Interp::scrIdle,
			(Check)&ScriptChecks::chkIgnore},
		{"endmail", false, (Method)&Interp::endmail,
			(Check)&ScriptChecks::chkNoArgs},
		{"sendmail", false, (Method)&Interp::sendmail,
			(Check)&ScriptChecks::chkIgnore},
		{"attachment", false, (Method)&Interp::scrInsert,
			(Check)&ScriptChecks::chkIgnore},
		{"attach", false, (Method)&Interp::scrInsert,
			(Check)&ScriptChecks::chkIgnore},
		{NULL, false, NULL, NULL}};

	trap("time");
	trap("timeout");
	trap("failure");
	trap("success");
	trap("down");

	load(interp);
}


extern "C" int main(int argc, char **argv)
{
	struct statfs fs;
	struct stat ino;
	static unsigned argcount = 1;
	char buf[96];
	char argname[13];
	char logname[32];
	int stepper;
	time_t now;
	timeout_t timeout;
	const char *ext;
	struct utsname uts;
	char hostname[64];
	char **env = environ;
	const char *ev, *eq;
	char envname[65];
	char output[900];
	char hex[3];
	char df[5];
	unsigned len;
	int nodes;
	bool advance = true;
	char *cp;
	long total, used;
	FILE *fp;
	char *drvname;
	unsigned drvidx = 0;

	iop = dup(1);

	cp = (char *)keypolicy.getLast("groups");
	cp = strtok(cp, " \t;,");
	policies[policycount++] = "*";
	while(cp && policycount < 64)
	{
		policies[policycount++] = cp;
		cp = strtok(NULL, " \t;,");
	}
	policies[policycount] = NULL;	

	Process::setPosixSignal(SIGUSR1, &handler);
	Process::setPosixSignal(SIGUSR2, &handler);
        Process::setPosixSignal(SIGINT, &handler);
        Process::setPosixSignal(SIGHUP, &handler);
        Process::setPosixSignal(SIGALRM, &handler);
//	Process::setPosixSignal(SIGSEGV, &fault);
//	Process::setPosixSignal(SIGABRT, &fault);

	if(argc < 2)
	{
		clog << "use: bayonne script.by" << endl;
		exit(-1);
	}

	ext = strrchr(argv[1], '.');

	snprintf(logname, sizeof(logname), "bayonne: shell(%d)", getpid());
	slog.open(logname, Slog::classDaemon);
	slog.clogEnable(false);
	if(!stricmp(ext, ".by") || !stricmp(ext, ".scr"))
		slog.clogEnable(true);

	Cmd cmd;
	//Img img(&cmd, argv[1]);
	Interp interp(&cmd, argv[1]);	

	gethostname(hostname, sizeof(hostname) - 1);
	uname(&uts);
	interp.setConst("sys.hostname", hostname);
	interp.setConst("sys.sysname", uts.sysname);
	interp.setConst("sys.nodename", uts.nodename);
	interp.setConst("sys.machine", uts.machine);	
	interp.setSymbol("script.boundry", "", 64);
	interp.setSymbol("session.trunkid", "", 15);
	interp.setSymbol("session.startid", "", 15);

	while(argcount < argc)
	{
		snprintf(argname, 12, "argv.%d", argcount);
		interp.setConst(argname, argv[argcount++]);
	}
	snprintf(argname, 8, "%d", argcount);
	interp.setConst("argv.count", argname);

	while(*env)
	{
		strcpy(envname, "env.");
		len = 0;
		while(len < 60 && (*env)[len] != '=')
		{
			envname[len + 4] = tolower((*env)[len]);			
			++len;
		}
		envname[len + 4] = 0;
		ev = strchr(*env, '=');
		if(ev)
			++ev;
		else
			ev = "";
		interp.setConst(envname, ev);
		if(!strncmp(*env, "PATH_INFO=", 10))
			interp.setConst("cgi.path", ev);
		else if(!strncmp(*env, "SERVER_VERSION=", 15))
			interp.setConst("server.version", ev);
		else if(!strncmp(*env, "SERVER_SOFTWARE=", 16))
		{
			interp.setConst("server.software", ev);
			interp.setConst("cgi.software", ev);
		}
		else if(!strncmp(*env, "SERVER_PROTOCOL=", 16))
		{
			interp.setConst("cgi.protocol", ev);
			interp.setConst("server.interface", ev);
		}
		else if(!strncmp(*env, "SERVER_PORT=", 12))
			interp.setConst("cgi.port", ev);
		else if(!strncmp(*env, "REQUEST_METHOD=", 15))
			interp.setConst("cgi.request", ev);
		else if(!strncmp(*env, "SCRIPT_NAME=", 12))
		   	interp.setConst("cgi.script", ev);
		else if(!strncmp(*env, "AUTH_TYPE=", 10))
			interp.setConst("cgi.auth", ev);
		else if(!strncmp(*env, "REMOTE_HOST=", 12))
			interp.setConst("cgi.host", ev);
		else if(!strncmp(*env, "REMOTE_USER=", 12))
			interp.setConst("cgi.user", ev);
		else if(!strncmp(*env, "REMOTE_IDENT=", 13))
			interp.setConst("cgi.ident", ev);
		else if(!strncmp(*env, "CONTENT_TYPE=", 13))
			interp.setConst("cgi.content", ev);
		else if(!strncmp(*env, "CONTENT_LENGTH=", 15))
			interp.setConst("cgi.length", ev);
		else if(!strncmp(*env, "HTTP_", 5))
		{
			strncpy(envname, "cgi", 3);
			interp.setConst(envname, ev);
		}
		else if(!strncmp(*env, "GATEWAY_INTERFACE=", 18))
			interp.setConst("cgi.interface", ev);
		else if(!strncmp(*env, "SERVER_VERSION=", 15))
			interp.setConst("server.version", ev);
		else if(!strncmp(*env, "SERVER_CONTROL=", 15))
		{
			snprintf(envname, sizeof(envname), "%s.ctrl", ev);
			interp.setConst("server.control", envname);
			snprintf(envname, sizeof(envname), "%s.nodes", ev);
			interp.setConst("server.nodes", envname);
			snprintf(envname, sizeof(envname), "%s.drivers", ev);
			interp.setConst("server.drivers", envname);
			snprintf(envname, sizeof(envname), "%s.packet", ev);
			interp.setConst("server.packet", envname);
		}
		else if(!strncmp(*env, "SERVER_TOKEN=", 13))
			interp.setConst("server.token", ev);
		else if(!strncmp(*env, "SERVER_NAME=", 12))
			interp.setConst("cgi.name", ev);
		else if(!strncmp(*env, "SERVER_NODE=", 12))
			interp.setConst("server.node", ev);
		else if(!strncmp(*env, "PORT_NUMBER=", 12))
			interp.setConst("driver.id", ev);
		else if(!strncmp(*env, "PORT_CLID=", 10))
			interp.setConst("driver.clid", ev);
		else if(!strncmp(*env, "PORT_DNID=", 10))
			interp.setConst("driver.dnid", ev);
		else if(!strncmp(*env, "PORT_DIGITS=", 12))
			interp.setConst("driver.digits", ev);
		else if(!strncmp(*env, "SERVER_RUNTIME=", 15))
			interp.setConst("server.prefix", ev);   
		else if(!strncmp(*env, "QUERY_STRING=", 13) || !strnicmp(*env, "PORT_QUERY=", 11))
		{
			while(*ev)
			{
				len = 6;
				strcpy(envname, "query.");
				while(*ev && *ev != '=' && len < 64)
					envname[len++] = tolower(*(ev++));
				envname[len] = 0;
				eq = strchr(ev, '=');
				if(!eq)
					ev = strchr(ev, '&');
				else
					ev = eq;
				if(!ev)
				{
					interp.setConst(envname, "");
					break;
				}
				if(*ev == '=')
					++ev;
				len = 0;
				while(*ev && *ev != '&' && len < 896)
				{
					if(*ev == '+')
					{
						output[len++] = ' ';
						++ev;
					}
					else if(*ev == '%')
					{
						if(*++ev)
							hex[0] = *ev;
						else
							hex[0] = '0';
						if(*++ev)
							hex[1] = *ev;
						else
							hex[1] = '0';
						if(*ev)
							++ev;
						hex[2] = 0;
						output[len++] = (char)
							strtol(hex, NULL, 16);
					}
					else
					{
						output[len++] = *(ev++);
					}
				}
				output[len] = 0;
				interp.setConst(envname, output);
				ev = strchr(ev, '&');
				if(!ev)
					break;
				++ev;
			}
		}
		++env;
	}

	ev = keypaths.getLast("runfiles");
	if(canModify(ev))
	{
		snprintf(envname, sizeof(envname), "%s/bayonne.ctrl", ev);
		interp.setConst("server.control", envname);
		snprintf(envname, sizeof(envname), "%s/bayonne.nodes", ev);	
		interp.setConst("server.nodes", envname);
		snprintf(envname, sizeof(envname), "%s/bayonne.drivers", ev);
		interp.setConst("server.drivers", envname);
		snprintf(envname, sizeof(envname), "%s/bayonne.packet", ev);
		interp.setConst("server.packet", envname);
	}
	else
	{
		snprintf(envname, sizeof(envname), "%s/.bayonne.ctrl", getenv("HOME"));
		interp.setConst("server.control", envname);
		snprintf(envname, sizeof(envname), "%s/.bayonne.nodes", getenv("HOME"));
		interp.setConst("server.nodes", envname);
		snprintf(envname, sizeof(envname), "%s/.bayonne.drivers", getenv("HOME"));
		interp.setConst("server.drivers", envname);
		snprintf(envname, sizeof(envname), "%s/.bayonne.packet", getenv("HOME"));
		interp.setConst("server.packet", envname);
	}

        memset(&server_addr, 0, sizeof(server_addr));
        server_addr.sun_family = AF_UNIX;
        strncpy(server_addr.sun_path, interp.getSymbol("server.packet"),
		sizeof(server_addr.sun_path));

#ifdef  __SUN_LEN
        len = sizeof(server_addr.sun_len) + strlen(server_addr.sun_path)
                        + sizeof(addr.sun_family) + 1;

        addr.sun_len = len;
#else
        len = strlen(server_addr.sun_path) + sizeof(server_addr.sun_family) + 1;
#endif


	interp.setConst("server.prefix", keypaths.getLast("datafiles"));
	chdir(interp.getSymbol("server.prefix"));
	interp.setConst("server.token", keyserver.getLast("token"));

	if(!statfs("./", &fs))
	{
		total = fs.f_blocks / 100;
		used = fs.f_blocks - fs.f_bfree;
		snprintf(df, sizeof(df), "%d", used / total);
		interp.setConst("server.used", df);
		snprintf(df, sizeof(df), "%d", fs.f_bavail / total);
		interp.setConst("server.free", df);
	}

	if(!statfs("/var", &fs))
	{
                total = fs.f_blocks / 100;
                used = fs.f_blocks - fs.f_bfree;
                snprintf(df, sizeof(df), "%d", used / total);
                interp.setConst("var.used", df);
                snprintf(df, sizeof(df), "%d", fs.f_bavail / total);
                interp.setConst("var.free", df);
	}		

        if(!statfs("/tmp", &fs))
        {
                total = fs.f_blocks / 100;
                used = fs.f_blocks - fs.f_bfree;
                snprintf(df, sizeof(df), "%d", used / total);
                interp.setConst("tmp.used", df);
                snprintf(df, sizeof(df), "%d", fs.f_bavail / total);
                interp.setConst("tmp.free", df);
        }

	fp = ::fopen(interp.getSymbol("server.drivers"), "r");
	while(fp)
	{
		::fgets(buf, sizeof(buf), fp);
		if(feof(fp))
			break;
		drvname = strtok(buf, " \t\n");
		if(!drvname)
			continue;
		snprintf(envname, sizeof(envname), "driver/%s.index", drvname);
		snprintf(df, sizeof(df), "%d", drvidx++);
		interp.setConst(envname, df);
		cp = strtok(NULL, " \t\n");
		snprintf(envname, sizeof(envname), "driver/%s.ports", drvname);
		interp.setConst(envname, cp);
		cp = strtok(NULL, " \t\n");
		snprintf(envname, sizeof(envname), "driver/%s.used", drvname);
		interp.setConst(envname, cp);
		cp = strtok(NULL, " \t\n");
		snprintf(envname, sizeof(envname), "driver/%s.exts", drvname);
		interp.setConst(envname, cp);
		cp = strtok(NULL, " \t\n");
		snprintf(envname, sizeof(envname), "driver/%s.trks", drvname);
		interp.setConst(envname, cp);
		cp = strtok(NULL, " \t\n");
		snprintf(envname, sizeof(envname), "driver/%s.ties", drvname);
		interp.setConst(envname, cp);
	}
	::fclose(fp);

	memset(&node, 0, sizeof(node));
	nodes = ::open(interp.getSymbol("server.nodes"), O_RDONLY);
	if(nodes > -1)
	{
		read(nodes, &node, sizeof(node));
		close(nodes);
	}
	switch(node.service)
	{
	case 0:
		interp.setConst("initial.service", "dead");
		break;
	case 'd':
	case 'D':
		interp.setConst("initial.service", "down");
		break;
	case 't':
	case 'T':
		interp.setConst("initial.service", "test");
		break;
	case 'u':
	case 'U':
		interp.setConst("initial.service", "up");
		break;
	default:
		interp.setConst("initial.service", "unknown");
	}

	interp.setConst("initial.status", node.stat);
	if(node.schedule[0])
		interp.setConst("initial.schedule", node.schedule);
	else
		interp.setConst("initial.schedule", "none");

	if(node.name[0])
		interp.setConst("server.node", node.name);

	snprintf(envname, 3, "%d", node.dialing); 
	interp.setConst("server.numbering", envname);
	snprintf(envname, 5, "%d", node.ports);
	interp.setConst("server.ports", envname);
	snprintf(envname, 5, "%d", node.calls);
	interp.setConst("initial.activity", envname);	

	Script::addConditional("user", &Interp::isActiveUser);
	Script::addConditional("hunt", &Interp::isHunt);
	Script::addConditional("driver", &Interp::isDriver);
	Script::addConditional("policy", &Interp::isPolicy);
	Script::addConditional("group", &Interp::isPolicy);
	Script::addConditional("voice", &Interp::hasVoice);
        Script::addConditional("alt", &Interp::hasAltVoice);
        Script::addConditional("altvoice", &Interp::hasAltVoice);
        Script::addConditional("sys", &Interp::hasSysVoice);
        Script::addConditional("sysvoice", &Interp::hasSysVoice);
        Script::addConditional("app", &Interp::hasAppVoice);
        Script::addConditional("appvoice", &Interp::hasAppVoice);
	Script::addConditional("sysprompt", &Interp::hasSysPrompt);
	Script::addConditional("varprompt", &Interp::hasVarPrompt);
	Script::addConditional("prompt", &Interp::hasVarPrompt);

	//img.addTables();

	for(;;)
	{
		stepper = oddeven;
		if(oddeven)
			oddeven = 0;
		else
			++oddeven;
		if(trapmode[stepper])
		{
			interp.endTimer();
			if(!stricmp(trapmode[stepper], "failure"))
			{
				interp.setSymbol("script.error", "command-failed");
				if(interp.signal("failure"))
				{
					advance = true;
					trapmode[stepper] = NULL;
					goto skip;
				}	
				else
					trapmode[stepper] = "error";
			}
			if(interp.signal(trapmode[stepper]))
				advance = true;				
			trapmode[stepper] = NULL;
		}	
		else if(interp.getSync())
		{
			interp.endTimer();
			if(interp.signal("time"))
				advance = true;
		}
		else if(interp.getExit())
		{
			interp.endTimer();
			if(!interp.signal("time"))
				interp.signal("exit");
		}
		else if(interp.delay())
		{
			if(!trapmode[oddeven])
				trapmode[oddeven] = "timeout";
			waiting = false;
			continue;
		}
skip:
		waiting = false;
		if(!advance)
			interp.advance();
		advance = true;
		if(!interp.step())
		{
			advance = false;
			cout.flush();
		}
		
	}
	exit(-1);
}

#ifdef	COMMON_OST_NAMESPACE
};
#endif
