// Copyright (C) 2000-2001 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "server.h"

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

ThreadLock Map::lock;
Map *Map::first = NULL;
Map *Map::last = NULL;

Map::Map(const char *pathname, const char *mapname)
{
	Map *map;
	next = prev = NULL;
	name = alloc((char *)mapname);
	int count = 0;
	ifstream input;
	char buffer[256];
	char *argv[33];
	unsigned len, argc = 0;
	char *cp;
	char *tok;
	mapsym_t *sym;
	mapkey_t *keys;
	unsigned path;

	memset(digits, 0, sizeof(digits));


	input.open(pathname, ios::in);
	while(input.is_open())
	{
		input.getline(buffer, sizeof(buffer) - 1);
		if(input.eof())
			break;

		cp = buffer;
		while(*cp == ' ' || *cp == '\r' || *cp == '\n' || *cp == '\t')
			++cp;

		if(!*cp || *cp == '#')
			continue;

		cp = strtok_r(cp, " \t\n\r", &tok);
		len = strlen(cp);
		if(len > 32)
			len = 32;
		cp[len] = 0;
		keys = digits[len - 1];
		if(!keys)
		{
			digits[len - 1] = keys = (mapkey_t *)alloc(sizeof(mapkey_t));
			memset(keys, 0, sizeof(mapkey_t));
		}
		path = getKey(cp, len);
		sym = (mapsym_t *)alloc(sizeof(mapsym_t *));
		sym->key = alloc(cp);
		while(NULL != (cp = strtok_r(NULL, " \t\r\n", &tok)) && argc < 32)
			argv[argc++] = cp;
		argv[argc++] = 0;
		sym->values = (char **)alloc(sizeof(char *) * argc);
		memcpy(sym->values, argv, sizeof(char *) * argc);
		sym->next = keys->keys[path];
		keys->keys[path] = sym;
		++count;
	}
	if(input.is_open())
		input.close();

	lock.writeLock();
	map = first;

	while(map)
	{
		if(!stricmp(map->name, mapname))
		{
			delete map;
			break;
		}
		map = map->next;
	}

	prev = last;
	if(last)
		last->next = this;

	last = this;
	if(!first)
		first = this;
	lock.unlock();
	slog(Slog::levelDebug) << "map: " << name << ": loaded " << count << " entries" << endl;
}

Map::~Map()
{
	slog(Slog::levelDebug) << "map: " << name << ": unloading" << endl;

	if(prev)
		prev->next = next;

	if(next)
		next->prev = prev;

	if(this == first)
		first = next;

	if(this == last)
		last = prev;
}

unsigned Map::getKey(const char *key, unsigned len)
{
	unsigned kv = 0;

	if(!len)
		len = strlen(key);

	while(*key && len--)
		kv = (kv << 1) ^ (*(key++) & 0x1f);
		
	return kv % (sizeof(mapkey_t) / sizeof(mapsym_t *));
}

bool Map::match(const char *mask, const char *key, unsigned len)
{
	while(len--)
	{
		switch(*mask)
		{
		case 'i':
		case 'I':
		case '?':
			if(*key == 0)
				--key;
			break;
		case 's':
		case 'S':
		case '*':
			if(*key == '*')
				break;
			return false;
		case 'p':
		case 'P':
		case '#':
			if(*key == '#')
				break;
			return false;
		case 'X':
		case 'x':
			break;
		case 'N':
		case 'n':
			if(*key == '0' || *key == '1')
				break;
			return false;
		default:
			if(*key != *mask)
				return false;
		}
		++mask;
		++key;
	}
	return true;
}

char **Map::getList(const char *key, unsigned len)
{
	mapsym_t *syms;
	mapkey_t *keys;
	if(len < 1 || len > 32)
		return NULL;

	keys = digits[len - 1];
	if(!keys)
		return NULL;

	syms = keys->keys[getKey(key, len)];
	while(syms)
	{
		if(match(syms->key, key, len))
			return syms->values;
		syms = syms->next;
	}
	return NULL;
}

char **getMap(const char *name, const char *key, mapmode_t mode)
{
	char **list = NULL;
	unsigned len = strlen(key);
	if(len > 32)
		len = 32;

	Map::lock.readLock();
	Map *map = Map::first;
	while(map)
	{
		if(!stricmp(map->name, name))
				break;
		map = map->next;
	}
	while(len && map && !list)
	{
		switch(mode)
		{
		case MAP_ABSOLUTE:
			list = map->getList(key, len);
			if(!list)
				len = 0;
			break;
		case MAP_SUFFIX:
			list = map->getList(key++, len--);
			break;
		case MAP_PREFIX:
			list = map->getList(key, len--);
			break;
		}
	}

	Map::lock.unlock();
	return list;
}

void endMaps(void)
{
	Map *next;

	Map::lock.writeLock();
	while(Map::first)
		delete Map::first;
	Map::lock.unlock();
}




#ifdef	CCXX_NAMESPACES
};
#endif
