// Copyright (C) 2000-2001 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "server.h"

#ifdef	HAVE_SSTREAM
#include <sstream>
#else
#include <strstream>
#endif

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

class FileThread : public Service
{
private:
	char buffer[128];
	int fd;
	void run(void);

public:
	FileThread(Trunk *trk, const char *file);
	~FileThread();
};

class CopyThread : public Service, public Audio
{
private:
	writemode_t mode;
	Audio::Info info;
	URLAudio inp;
	AudioFile out;
	bool done;
	char *output, *voice;
	unsigned lpos;
	char obuffer[65];

public:
	char list[256];

	CopyThread(Trunk *trk, char *path, writemode_t mode);
	~CopyThread();
	void run(void);
};

FileThread::FileThread(Trunk *trk, const char *path) : Service(trk, 0)
{
	fd = -1;
	snprintf(buffer, sizeof(buffer), "%s/%s.log",
		keypaths.getLogpath(), path);
}

FileThread::~FileThread()
{
	terminate();
	if(fd > -1)
		::close(fd);
}

void FileThread::run(void)
{
	const char *opt;
	unsigned len = 0;

	fd = ::open(buffer, O_CREAT | O_WRONLY | O_APPEND, 0660);
	if(fd < 0)
	{
		trunk->setSymbol(SYM_ERROR, "cannot-write-log");
		Service::failure();
	}
	buffer[0] = 0;
	while(NULL != (opt = (trunk->getValue(NULL))))
	{
		snprintf(buffer + len, sizeof(buffer) - len - 1, "%s", opt);
		len = strlen(buffer);
	}
	buffer[len++] = '\n';
	buffer[len++] = 0;
	::write(fd, buffer, len);
	::close(fd);
	fd = -1;
	Service::success();
}	

CopyThread::CopyThread(Trunk *trk, char *o, writemode_t m) :
Service(trk, 0)
{
	const char *fmt = trk->getKeyword("encoding");
	const char *ext;

	snprintf(obuffer, sizeof(obuffer), "%s", o);
	o = obuffer;

	ext = strrchr(o, '/');

	if(!fmt)
		fmt = trk->getDefaultEncoding();

	done = false;
	output = o;
	mode = m;
	lpos = 0;

	if(mode == WRITE_MODE_APPEND)
		done = true;

	if(!fmt)
		fmt = trk->getSymbol(SYM_FORMAT);

	if(!fmt)
		fmt = "raw";

	if(ext)
		ext = strrchr(ext, '.');
	else
		ext = strrchr(o, '.');

	info.format = raw;
	info.encoding = mulawAudio;
	// XXX fixme... dodgy!
	info.annotation = (char *)trk->getKeyword("annotation");
	if(!info.annotation)
		info.annotation = "copied";

	if(!stricmp(ext, ".al"))
		fmt = "alaw";
	else if(!stricmp(ext, ".ul"))
		fmt = "ulaw";
        else if(!stricmp(ext, ".au") || !stricmp(ext, ".snd"))
        {
                info.format = snd;
                info.order = __BIG_ENDIAN;
        }
        else if(!stricmp(ext, ".wav"))
        {
                info.format = riff;
                info.order = __LITTLE_ENDIAN;
        }

        if(!stricmp(fmt, "adpcm") || !stricmp(fmt, "g721") || !stricmp(fmt, "g.721"))
                info.encoding = g721ADPCM;
	else if(!stricmp(fmt, "alaw"))
		info.encoding = alawAudio;
	else if(!stricmp(fmt, "ulaw") || !stricmp(fmt, "mulaw"))
		info.encoding = mulawAudio;
        else if(!stricmp(fmt, "g723") || !stricmp(fmt, "g.723"))
                info.encoding = g723_3bit;
        else if(!stricmp(fmt, "pcm") || !stricmp(fmt, "l16") || !stricmp(fmt, "linear"))
                info.encoding = pcm16Mono;
	else if(!stricmp(fmt, "g711") || !stricmp(fmt, "g.721"))
	{
		if(info.encoding != alawAudio)
			info.encoding = mulawAudio;
	}
	else if(stricmp(fmt, "raw"))
		info.encoding = unknownEncoding;
}

CopyThread::~CopyThread()
{
	char buffer[16];

	terminate();

	inp.close();

	if(!done)
	{
		remove(output);
		trunk->setSymbol(SYM_RECORDED, "0");
	}
	else
	{
		snprintf(buffer, sizeof(buffer), "%ld", out.getPosition());
		trunk->setSymbol(SYM_RECORDED, buffer);
		chown(output, keyserver.getUid(), keyserver.getGid());
	}
	
	out.close();
}

void CopyThread::run(void)
{
	typedef enum
	{
		CVT_NONE,
		CVT_AU,	/* alaw to ulaw */
		CVT_UA,	/* ulaw to alaw */
		CVT_AL,	/* alaw to linear */	
		CVT_LA,	/* linear to alaw */
		CVT_UL,	/* ulaw to linear */
		CVT_LU,	/* linear to ulaw */
		CVT_UNKNOWN
	} cvt_t;
	unsigned lbuflen = 0;
	cvt_t cvt = CVT_NONE;

	char pbuffer[1024];
	unsigned char buffer[1024];
	short lbuffer[2048];
	short *linear = (short *)&buffer;
	int count, bufcnt;
	const char *path;
	const char *voice = trunk->getKeyword("voice");
	const char *prompts = keypaths.getLast("prompts");
	const char *altprompts = keypaths.getLast("altprompts");
	const char *cp;

	if(!permitAudioAccess(output, true))
	{
		done = true;
		trunk->setSymbol(SYM_ERROR, "write-access-denied");
		Service::failure();
	}		

	if(!voice)
		voice = trunk->getSymbol(SYM_VOICE);

	switch(mode)
	{
	case WRITE_MODE_REPLACE:
		remove(output);
		out.create(output, &info);
		break;
	case WRITE_MODE_APPEND:
		out.open(output);
		if(!out.isOpen())
		{
			done = false;
			out.create(output, &info);
		}
		cp = trunk->getKeyword("offset");
		if(cp)
			out.setPosition(atol(cp));
		else
			out.setPosition();
		break;
	}

	if(!out.isOpen())
	{
		trunk->setSymbol(SYM_ERROR, "write-failed");
		Service::failure();
	}

	while(list[lpos])
	{
		while(list[lpos] == ',')
			++lpos;
		path = list + lpos;
		while(list[lpos] && list[lpos] != ',')
			++lpos;
		if(list[lpos] == ',')
			list[lpos++] = 0;

		strcpy(pbuffer, path);
		path = getPrompt(pbuffer, voice);
		inp.open(path);
		if(!inp.isOpen())
			continue;

		if(inp.getEncoding() == out.getEncoding())
			cvt = CVT_NONE;
		else if(inp.getEncoding() == mulawAudio && out.getEncoding() == alawAudio)
			cvt = CVT_UA;
		else if(inp.getEncoding() == alawAudio && out.getEncoding() == mulawAudio)
			cvt = CVT_AU;
		else if(inp.getEncoding() == mulawAudio && out.getEncoding() == pcm16Mono)
			cvt = CVT_UL;
		else if(inp.getEncoding() == alawAudio && out.getEncoding() == pcm16Mono)
			cvt = CVT_AL;
		else if(inp.getEncoding() == pcm16Mono && out.getEncoding() == mulawAudio)
			cvt = CVT_LU;
		else if(inp.getEncoding() == pcm16Mono && out.getEncoding() == alawAudio)
			cvt = CVT_LA;
		else
			cvt = CVT_UNKNOWN;

		for(;;)
		{
			count = inp.getBuffer(buffer, 1024);
			if(count < 1)
				break;

			switch(cvt)
			{
			case CVT_LU:
				count /= 2;
				for(bufcnt = 0; bufcnt < count; ++bufcnt)
					buffer[bufcnt] = phTone::linear2ulaw(linear[bufcnt]);
				break;
			case CVT_LA:
				count /= 2;
				for(bufcnt = 0; bufcnt < count; ++bufcnt)
					buffer[bufcnt] = phTone::linear2alaw(linear[bufcnt]);
				break;
			case CVT_UL:
				for(lbuflen = 0; lbuflen < count; ++lbuflen)
					lbuffer[lbuflen] = phTone::ulaw2linear(buffer[lbuflen]);
				break;
			case CVT_AL:
				for(lbuflen = 0; lbuflen < count; ++lbuflen)
					lbuffer[lbuflen] = phTone::alaw2linear(buffer[lbuflen]);
				break;
			case CVT_UA:
				for(bufcnt = 0; bufcnt < count; ++bufcnt)
					buffer[bufcnt] = phTone::ulaw2alaw(buffer[bufcnt]);
				break;
                        case CVT_AU:
                                for(bufcnt = 0; bufcnt < count; ++bufcnt)
                                        buffer[bufcnt] = phTone::alaw2ulaw(buffer[bufcnt]);
                                break;
			}

			if(lbuflen)
				out.putBuffer((unsigned char *)lbuffer, lbuflen * 2);
			else
				out.putBuffer(buffer, count);
		}

		inp.close();
	}

	done = true;
	Service::success();
}

bool Trunk::scrExiting(void)
{
	const char *value = getValue("detach");
	const char *mem = getMember();
	unsigned len = 0;

	if(!mem)
		mem = "exit";

	while(value && len < sizeof(exitmsg) - 3)
	{
		snprintf(exitmsg + len, sizeof(exitmsg) - len, "%s", value);
		len = strlen(exitmsg);
		value = getValue(NULL);
	}

	exitmsg[len++] = '\n';
	exitmsg[len] = 0;
	writeRPC("exit", exitmsg);
	exitmsg[--len] = 0;

	if(!stricmp(mem, "rpc"))
	{
		advance();
		return true;
	}
	return ScriptInterp::exit();
}

bool Trunk::scrSession(void)
{
	Trunk *trk;
	const char *id = getKeyword("id");
	if(!id)
		id = getValue(NULL);

	if(!id)
	{
		ctx = this;
		advance();
		return false;	// make sure not in begin block
	}

	trk = Driver::getTrunk(id, false, getDriver());
	if(!trk)
	{
		error("invalid-session");
		return true;
	}

	if(trk == this)
	{
		ctx = this;
		return false;	// make sure not in begin block
	}

	if(*id != '-' && !isAdmin())
	{
		error("admin-required");
		return true;
	}

	ctx = trk;
	advance();
	return false;	// make sure not in begin block
}

bool Trunk::scrService(void)
{
	const char *mem = getMember();
	if(!mem)
		mem = getOption(NULL);

	if(!isAdmin())
	{
		error("admin required");
		return false;
	}

	if(!stricmp(mem, "up"))
		service[0] = 0;
	else
		snprintf(service, sizeof(service), "down::%s", getValue("service"));

	advance();
	return true;
}

bool Trunk::scrBusy(void)
{
	TrunkEvent event;
	Trunk *trk;
	TrunkGroup *grp = NULL;
	Driver *drv = Driver::drvFirst;
	const char *mem = getMember();
	unsigned port, tspan;

	if(!mem)
		mem = "self";
	else if(!isAdmin())
	{
		error("admin-required");
		return false;
	}

	if(!stricmp(mem, "port"))
	{
		event.id = TRUNK_MAKE_BUSY;
		mem = getValue(getKeyword("id"));
		trk = Driver::getTrunk(mem, false, getDriver());

		if(!trk)
		{
			error("busy-port-id-missing");
			return true;
		}

		if(*mem != '-' && !isAdmin())
		{
			error("busy-port-admin-required");
			return true;
		}

		if(trk == this)
		{
			error("busy-port-self-reference");
			return true;
		}

		trk->postEvent(&event);
		advance();
		return true;
	}

	if(!stricmp(mem, "span"))
	{
		if(!isAdmin())
		{
			error("busy-span-admin-required");
			return true;
		}
		mem = getValue(getKeyword("id"));
		if(!mem)
		{
			error("busy-span-id-missing");
			return true;
		}
		tspan = atoi(mem);
		if(!driver->spanEvent(tspan, &event))
			error("busy-span-id-invalid");
		else
			advance();
		return true;
	}

	if(!stricmp(mem, "card"))
	{
		if(!isAdmin())
		{
			error("busy-card-admin-required");
			return true;
		}
		mem = getValue(getKeyword("id"));
		if(!mem)
		{
			error("busy-card-id-missing");
			return true;
		}
		tspan = atoi(mem);
		if(!driver->cardEvent(tspan, &event))
			error("busy-card-id-invalid");
		else
			advance();
		return true;
	}

	if(!stricmp(mem, "group"))
	{
		if(!isAdmin())
		{
			error("busy-group-admin-required");
			return true;
		}
		mem = getValue(getKeyword("id"));
		if(mem)
			grp = getGroup(mem);
		if(!grp)
		{
			error("busy-group-id-invalid");
			return true;
		}

		while(drv)
		{
	        	for(port = 0; port < drv->getTrunkCount(); ++port)
        		{
                		if(drv->getTrunkGroup(port) != grp)
                       		 	continue;

	                	trk = drv->getTrunkPort(port);
        	        	if(!trk || trk == this)
                        		continue;

				event.id = TRUNK_MAKE_BUSY;
				trk->postEvent(&event);
			}
			drv = drv->drvNext;
		}

		advance();
		return true;
	}
	error("busy-self-reference");
	return true;
}

bool Trunk::scrSlog(void)
{
        unsigned id = getId();
        const char *member = getMember();
	const char *file = getKeyword("file");
	const char *cp = getKeyword("maxTime");
        const char *val;
        Name *obj = ScriptInterp::getName();
	char name[32];
	char buffer[256], encode[256];
#ifdef	HAVE_TGI
	tgicmd_t cmd;
#endif

        if(!member)
                member = getKeyword("level");

	if(file)
	{
		if(strchr(file, '/'))
		{
			error("logfile-invalid");
			return true;
		}
		thread = new FileThread(this, file);
		if(cp)
			data.sleep.wakeup = getSecTimeout(cp);
		else
			data.sleep.wakeup = 60000;
		trunkStep(TRUNK_STEP_THREAD);
		return false;
	}

        if(member)
        {
                if(!stricmp(member, "debug"))
                        slog(Slog::levelDebug);
                else if(!stricmp(member, "info"))
                        slog(Slog::levelInfo);
                else if(!stricmp(member, "notice"))
                        slog(Slog::levelNotice);
                else if(!strnicmp(member, "warn", 4))
                        slog(Slog::levelWarning);
                else if(!strnicmp(member, "err", 3))
                        slog(Slog::levelError);
                else if(!strnicmp(member, "crit", 4))
                        slog(Slog::levelCritical);
                else if(!stricmp(member, "alert"))
                        slog(Slog::levelAlert);
                else if(!strnicmp(member, "emerg", 5))
                        slog(Slog::levelEmergency);
                else
                        slog(Slog::levelNotice);
        }
        else
                slog(Slog::levelNotice);

	getName(name);


        slog() << name << ": " << obj->name;
        if(id)
                slog() << "(" << id << ")";

        slog() << ": ";
        while(NULL != (val = getValue(NULL)))
                slog() << val;
        slog() << endl;
        advance();
        return true;
}

bool Trunk::scrSend(void)
{
	const char *id = getKeyword("id");
	const char *pid;
	TrunkEvent event;
	Trunk *trk = NULL;
	Symbol *sym;
	int dig;
	const char *opt = getMember();
	const char *val;
	char buffer[76];
	unsigned len = 0;
	enum
	{
		byId,
		byExt,
		byTrk,
		byTie
	}	by = byId;

	if(!opt)
		opt = "message";

	if(!stricmp(opt, "status"))
	{
		while(NULL != (val = getValue(NULL)) && len < 70)
		{
			snprintf(buffer + len, sizeof(buffer) - len, "%s", val); 
			len = strlen(buffer);
		}
		writeRPC("send", buffer);
		advance();
		return true;
	}

	if(!stricmp(opt, "signal"))
	{
		if(!id)
			id = getValue(NULL);
       		trk = Driver::getTrunk(id, false, getDriver());
        	if(!trk)
        	{
                	error("signal-no-such-trunk");
                	return true;
        	}

	        event.id = TRUNK_SIGNAL_JOIN;
        	event.parm.error = getKeyword("message");
        	if(!event.parm.error)
                	event.parm.error = getValue(NULL);
        	if(!trk->postEvent(&event))
        	{
                	error("signal-not-waiting");
                	return true;
        	}
	        advance();
        	return true;
	}

	if(!stricmp(opt, "pickup"))
		pid = getSymbol(SYM_PICKUP);
	else if(!stricmp(opt, "recall"))
		pid = getSymbol(SYM_RECALL);
	else
		pid = getKeyword("id");

	if(!pid)
		pid = getValue(NULL);

	if(!pid)
	{
		error("send-no-id");
		return true;
	}

	trk = Driver::getTrunk(pid, false, getDriver());
	if(!trk)
	{
		error("send-no-session");
		return true;
	}

	if(trk == this)
	{
		error("send-self-reference");
		return true;
	}

	if(!strnicmp(opt, "dig", 3))
	{
		opt = getKeyword("digits");
		if(!opt)
			opt = getValue(NULL);
		if(!opt)
		{
			error("no-digits");
			return true;
		}
		while(*opt)
		{
			dig = getDigit(*(opt++));
			if(dig < 0)
				continue;
			event.id = TRUNK_DTMF_KEYUP;
			event.parm.dtmf.digit = dig;
			trk->postEvent(&event);
		}
	}
	else
	{
		event.id = TRUNK_SEND_MESSAGE;
		event.parm.send.seq = seq;
		event.parm.send.src = this;
		event.parm.send.msg = getKeyword("message");
		if(!event.parm.send.msg)
			event.parm.send.msg = getValue("");
		trk->postEvent(&event);
	}

	advance();
	return true;
}

bool Trunk::scrPolicy(void)
{
	Line *line = getLine();
	const char *opt;
	const char *value = NULL, *def;
	const char *member = getMember();
	int argc = 0;
	char local[65];

	while(argc < line->argc)
	{
		opt = line->args[argc++];
                if(*opt != '=')
                        continue;

                if(*(++opt) == '%')
                        ++opt;

		def = line->args[argc++];

                if(member)
                        snprintf(local, sizeof(local), "%s.%s", member, opt);
                else
                        snprintf(local, sizeof(local), "%s", opt);

		if(group)
			value = group->getLast(opt);

		if(!value)
			value = def;

		setConst(local, value);
	}
	advance();
	return true;
}

bool Trunk::scrConfig(void)
{
	ScriptImage *img = getImage();

	Name *scr = ScriptInterp::getName();
	Line *line = getLine();
	char *cp;
	const char *opt;
	const char *value, *def;
	const char *member = getMember();
	int argc = 0;
	char buffer[65];
	char appl[65];
	char local[65];

	snprintf(appl, sizeof(appl), "%s", scr->name);
	cp = strstr(appl, "::");
	if(cp)
		*cp = 0;

	while(argc < line->argc)
	{
		opt = line->args[argc++];
                if(*opt != '=')
                        continue;

                if(*(++opt) == '%')
                        ++opt;

		def = line->args[argc++];

                if(member)
		{
                        snprintf(local, sizeof(local), "%s.%s", member, opt);
			snprintf(buffer, sizeof(buffer), "%s.%s", member, opt);
		}
                else
		{
                        snprintf(local, sizeof(local), "%s", opt);
			snprintf(buffer, sizeof(buffer), "%s.%s", appl, opt);
		}

		value = img->getLast(buffer);
		if(!value)
			value = img->getLast(opt);

		if(!value)
			value = def;

		setConst(local, value);
	}
	advance();
	return true;
}

bool Trunk::scrDummy(void)
{
	error("not-supported");
	return true;
}

bool Trunk::scrRoute(void)
{
	char named[65];
	const char *opt;
	const char *rtype = getKeyword("type");
	
	if(!rtype)
		rtype = getMember();

	if(!rtype)
		rtype = "route";

	while(NULL != (opt = getValue(NULL)))
	{
		if(!*opt && stricmp(rtype, "route"))
			opt = "none";
		snprintf(named, sizeof(named), "%s:%s", rtype, opt);
		if(trunkEvent(named))
			return true;

		if(!stricmp(rtype, "route"))
			if(trunkEvent("route:default"))
				return true;
	}
	error("no-route");
	return true;
}

bool Trunk::scrRedirect(void)
{
	return scrCleardigits();
}

bool Trunk::scrCleardigits(void)
{
	const char *mem = getMember();
	Line *line = getLine();
	trunksignal_t sig;
	unsigned dig = 0;
	unsigned count;

	if(!mem)
	{
		if(line->scr.method == (Method)&Trunk::scrCleardigits)
			mem = "all";
		else
			mem = "none";
	}
	
	if(!stricmp(mem, "all") || !stricmp(mem, "clear"))
	{
		dtmf.bin.data[0] = 0;
		digits = 0;
	}
	else if(!stricmp(mem, "last") && digits)
	{
		dtmf.bin.data[0] = dtmf.bin.data[digits - 1];
		dtmf.bin.data[1] = 0;
		digits = 1;
	}
	else if(atoi(mem) > 0)
	{
		count = atoi(mem);
		if(count > digits)
			count = digits;

		while(dig < count)
			dtmf.bin.data[dig++] = '-';
	}
	else if(!stricmp(mem, "pop") && digits)
		dtmf.bin.data[0] = '-';
	else if(stricmp(mem, "trap"))
	{
		dtmf.bin.data[0] = 0;
		digits = 0;
	}
	
	if(line->scr.method == (Method)&Trunk::scrRedirect)
	{
		if(!redirect(getContent(line->args[0])))
			advance();
	}
	//else if(line->argc > 0)
	//	scrGoto();
	else
		advance();

retry:
	if(!digits)
		return true;

	switch(dtmf.bin.data[0])
	{
	case '*':
		sig = TRUNK_SIGNAL_STAR;
		break;
	case '#':
		sig = TRUNK_SIGNAL_POUND;
		break;
	case 'a':
	case 'A':
		sig = TRUNK_SIGNAL_A;
		break;
	case 'b':
	case 'B':
		sig = TRUNK_SIGNAL_B;
		break;
	case 'c':
	case 'C':
		sig = TRUNK_SIGNAL_C;
		break;
	case 'd':
	case 'D':
		sig = TRUNK_SIGNAL_D;
		break;
	case '0':
		sig = TRUNK_SIGNAL_0;
		break;
	case '1':
		sig = TRUNK_SIGNAL_1;
		break;
	case '2':
		sig = TRUNK_SIGNAL_2;
		break;
	case '3':
		sig = TRUNK_SIGNAL_3;
		break;
	case '4':
		sig = TRUNK_SIGNAL_4;
		break;
	case '5':
		sig = TRUNK_SIGNAL_5;
		break;
	case '6':
		sig = TRUNK_SIGNAL_6;
		break;
	case '7':
		sig = TRUNK_SIGNAL_7;
		break;
	case '8':
		sig = TRUNK_SIGNAL_8;
		break;
	case '9':
		sig = TRUNK_SIGNAL_9;
		break;
	default:
		sig = TRUNK_SIGNAL_STEP;
	}

	if(sig != TRUNK_SIGNAL_STEP)			
		if(trunkSignal(sig))
			return true;

	dig = 0;
	while(dig < digits)
	{
		dtmf.bin.data[dig] = dtmf.bin.data[dig + 1];
		++dig;
	}
	dtmf.bin.data[dig] = 0;
	digits = --dig;
	goto retry;	
}

bool Trunk::scrIdle(void)
{
	TrunkEvent event;
	Trunk *trk;
	TrunkGroup *grp = NULL;
	Driver *drv = Driver::drvFirst;
	const char *mem = getMember();
	unsigned port, tspan;

	if(!mem)
		mem = "self";
	else if(!isAdmin())
	{
		error("admin-required");
		return true;
	}

	if(!stricmp(mem, "port"))
	{
		event.id = TRUNK_MAKE_IDLE;
		mem = getValue(getKeyword("id"));
		trk = Driver::getTrunk(mem, false, getDriver());

		if(!trk)
		{
			error("idle-port-id");
			return true;
		}

		if(trk == this)
		{
			error("idle-self-reference");
			return true;
		}

		trk->postEvent(&event);
		advance();
		return true;
	}

	if(!stricmp(mem, "span"))
	{
		mem = getValue(getKeyword("id"));
		if(!mem)
		{
			error("idle-span-id");
			return true;
		}
		tspan = atoi(mem);
		if(driver->spanEvent(tspan, &event))
			advance();
		else
			error("idle-span-invalid");
		return true;
	}

        if(!stricmp(mem, "card"))
        {
                mem = getValue(getKeyword("id"));
                if(!mem)
                {
                        error("idle-card-id");
                        return true;
                }
                tspan = atoi(mem);
                if(driver->cardEvent(tspan, &event))
                        advance();
                else
                        error("idle-card-invalid");
                return true;
        }

	if(!stricmp(mem, "group"))
	{
		mem = getValue(getKeyword("id"));
		if(mem)
			grp = getGroup(mem);
		if(!grp)
		{
			error("idle-group-id");
			return true;
		}

		while(drv)
		{	
	        	for(port = 0; port < drv->getTrunkCount(); ++port)
        		{
                		if(drv->getTrunkGroup(port) != grp)
                       		 	continue;

	                	trk = drv->getTrunkPort(port);
        	        	if(!trk || trk == this)
                        		continue;

				event.id = TRUNK_MAKE_IDLE;
				trk->postEvent(&event);
			}
			drv = drv->drvNext;
		}

		advance();
		return true;
	}

	idle_timer = atoi(getValue("0"));
	advance();
	return true;
}

bool Trunk::scrSchedule(void)
{
	char cmd[65];
	const char *mem = getMember();
	if(!mem)
		mem = getValue("");

	snprintf(cmd, sizeof(mem), "schedule %s", mem);
	if(!fifo.command(cmd))
	{
		error("schedule-failed");
		return true;
	}
	advance();
	return true;
}

bool Trunk::scrModule(void)
{
	Line *line = getLine();
	const char *cmd = line->cmd;
	char keybuf[33];
	int len = 0;
	char *kw = keybuf;
	char *cp;

	cp = strchr(cmd, '-');
	if(cp)
		cmd = ++cp;

	while(len++ < 32 && *cmd && *cmd != '.')
		*(kw++) = *(cmd++);
	*kw = 0;

	Module *module = getModule(MODULE_ANY, keybuf);
	char *err;
	unsigned delay;

	if(!module)
	{
		error("module-not-found");
		return true;
	}

	err = module->dispatch(this);
	if(err)
	{
		if(err == ((char *)(-1)))
			return true;

		if(strchr(err, ':'))
		{
			if(trunkEvent(err))
				return true;
			advance();
			return true;
		}
		error(err);
		return true;
	}

	switch(module->getType())
	{
	case MODULE_LIBEXEC:
		delay = module->sleep(this);
		if(!delay)
			++delay;
		data.sleep.wakeup = delay * 1000;
		trunkStep(TRUNK_STEP_SLEEP);
		return false;
	case MODULE_THREAD:
                delay = module->sleep(this);
		if(delay)
	                data.sleep.wakeup = delay * 1000;
		if(!data.sleep.wakeup)
		{
			advance();
			return true;
		}
		trunkStep(TRUNK_STEP_THREAD);
		return false;
	case MODULE_PLAY:
		trunkStep(TRUNK_STEP_PLAY);
		return false;
	case MODULE_RECORD:
		trunkStep(TRUNK_STEP_RECORD);
		return false;
	}

	delay = module->sleep(this);
	if(!delay)
	{
		advance();
		return true;
	}
	if(delay == (unsigned)-1)
		return module->executePrior(this);

	data.sleep.wakeup = delay * 1000;
	data.sleep.rings = 0;
	data.sleep.loops = 1;
	data.sleep.save = NULL;
	if(thread)
		trunkStep(TRUNK_STEP_THREAD);
	else
		trunkStep(TRUNK_STEP_SLEEP);
	module->commit(this);
	return false;
}

bool Trunk::scrMove(void)
{
	const char *prefix = getPrefixPath();
	const char *n1 = getValue(NULL);
	const char *n2 = getValue(NULL);
	char buf1[256], buf2[256];

	if(!n1 || !n2)
	{
		error("move-no-files");
		return true;
	}

	if(prefix)
	{
		snprintf(buf1, sizeof(buf1), "%s/%s", prefix, n1);
		snprintf(buf2, sizeof(buf2), "%s/%s", prefix, n2);
		n1 = (const char *)buf1;
		n2 = (const char *)buf2;
	}
	if(!permitAudioAccess(n1, true) || !permitAudioAccess(n2, true))
		error("access-denied");
	else if(rename(n1, n2))
		error("move-failed");
	else
		advance();
	return true;
}

bool Trunk::scrErase(void)
{
	const char *prefix = getPrefixPath();	
	const char *name = getValue(NULL);

	if(!name)
	{
		error("erase-no-file");
		return true;
	}

	if(prefix)
	{
		snprintf(apppath, sizeof(apppath), "%s/%s",
			prefix, name);
		name = apppath;
	}
	if(!permitAudioAccess(name, true))
	{
		error("access-denied");
		return true;
	}
	remove(name);

	advance();
	return true;
}

bool Trunk::scrSendFax(void)
{
	const char *prefix = getPrefixPath();
	const char *file = getValue(NULL);

	if(!(TRUNK_CAP_SENDFAX & getCapabilities()))
	{
		error("no-fax-send");
		return true;
	}

	if(!file)
	{
		error("no-file-to-send");
		return true;
	}

	if(prefix)
		snprintf(data.fax.pathname, sizeof(data.fax.pathname),
			"%s/%s", prefix, file);
	else
		snprintf(data.fax.pathname, sizeof(data.fax.pathname),
			"%s", file);

	data.fax.station = getStation();
	trunkStep(TRUNK_STEP_SENDFAX);
	return false;
}
bool Trunk::scrRecvFax(void)
{
        const char *prefix = getPrefixPath();
        const char *file = getValue(NULL);

        if(!(TRUNK_CAP_RECVFAX & getCapabilities()))
        {
                error("no-fax-recv");
                return true;
        }

        if(!file)
        {
                error("no-file-to-recv");
                return true;
        }

        if(prefix)
                snprintf(data.fax.pathname, sizeof(data.fax.pathname),
                        "%s/%s", prefix, file);
        else
                snprintf(data.fax.pathname, sizeof(data.fax.pathname),
                        "%s", file);

        data.fax.station = getStation();
        trunkStep(TRUNK_STEP_RECVFAX);
        return false;
}

bool Trunk::scrRecord(void)
{
	const char *member = getMember();
	const char *cp;
	const char *prefix = getPrefixPath();
	const char *gain = getKeyword("gain");
	const char *vol = getKeyword("volume");

	if(flags.listen)
	{
		error("record-unavailable");
		return true;
	}

	cp = getKeyword("trim");
	if(!cp)
		cp = getSymbol(SYM_TRIM);
	if(!cp)	
		cp = "0";
	data.record.trim = atoi(cp);

	cp = getKeyword("frames");
	if(!cp)
		cp = "0";

	data.record.frames = atoi(cp);
	cp = getKeyword("minSize");
	if(!cp)
		cp = "0";
	data.record.minsize = atoi(cp);

	if(!vol)
		vol = getSymbol(SYM_VOLUME);

	if(!vol)
		vol = "100";

	apppath[0] = 0;

	if(!member)
		member="all";

	data.record.save = (char *)getKeyword("save");
	data.record.text = getKeyword("text");
	data.record.name = (char *)getValue("");	
	if(!data.record.name)
	{
		error("record-no-file");
		return true;
	}

	if(prefix)
	{
		snprintf(apppath + 1, sizeof(apppath) - 1, "%s/%s",
			prefix, data.record.name);
		data.record.name = apppath + 1;
	}


	cp = getKeyword("timeout");
	if(cp)
		data.record.timeout = getSecTimeout(cp);
	else			
		data.record.timeout = getTimeout("maxTime");
	data.record.term = getDigitMask("exit");
	data.record.offset = (unsigned long)-1;
	data.record.volume = atoi(vol);
	data.record.silence = 0;
	data.record.encoding = getKeyword("encoding");
	data.record.annotation = getKeyword("annotation");
	data.record.extension = getKeyword("extension");
	
	if(data.record.save)
	{
		cp = strrchr(data.record.save, '.');
		if(!cp)
			cp = data.record.extension;
		if(!cp)
			cp = getSymbol(SYM_EXTENSION);
		else
			cp = "";
	}

	if(prefix && data.record.save)
	{
		snprintf(data.record.altinfo, sizeof(data.record.altinfo),
			"%s/%s%s", prefix, data.record.save, cp);
		data.record.save = data.record.altinfo;
	}
	else if(data.record.save)
		snprintf(data.record.altinfo, sizeof(data.record.altinfo),
			"%s%s", data.record.save, cp);

	if(!data.record.encoding)
		data.record.encoding = getDefaultEncoding();

	if(!data.record.annotation)
		data.record.annotation = "";

	if(!data.record.extension)
		data.record.extension = getSymbol(SYM_EXTENSION);

	if(gain)
		data.record.gain = (float)strtod(gain, NULL);
	else
		data.record.gain = 0.0;
	
	data.record.info = false;

	if(!stricmp(member, "append"))
		data.record.append = true;
	else if(!stricmp(member, "info"))
	{
		data.record.append = true;
		data.record.info = true;
	}
	else
		data.record.append = false;
	if(NULL != (cp = getKeyword("offset")))
		data.record.offset = atoi(cp);
	if(NULL != (cp = getKeyword("volume")))
		data.record.volume = atoi(cp);
	if(NULL != (cp = getKeyword("silence")))
		data.record.silence = atoi(cp);

	trunkStep(TRUNK_STEP_RECORD);
	return false;
}

bool Trunk::scrTransfer(void)
{
	unsigned len;
	const char *cp = (char *)group->getLast("transfer");
	if(cp)
		strncpy(data.dialxfer.digits, cp, sizeof(data.dialxfer.digits));
	else
		data.dialxfer.digits[0] = 0;
	len = strlen(data.dialxfer.digits);

	while(NULL != (cp = getValue(NULL)) && len < sizeof(data.dialxfer.digits))
	{
		strncpy(data.dialxfer.digits + len, cp, sizeof(data.dialxfer.digits) - len);
		len = strlen(data.dialxfer.digits);
	}

	data.dialxfer.digits[sizeof(data.dialxfer.digits) - 1] = 0;
	data.dialxfer.interdigit = group->getDialspeed();
	data.dialxfer.digit = data.dialxfer.digits;
	data.dialxfer.exit = true;
	data.dialxfer.timeout = 0;
	cp = getKeyword("onhook");
	if(!cp)
		cp = getKeyword("flash");
	if(!cp)
		cp = getValue(group->getLast("flash"));
	data.dialxfer.onhook = getMSTimeout(cp);
	cp = getKeyword("dialtone");
	if(!cp)
		cp = getKeyword("offhook");
	if(!cp)
		cp = getValue(group->getLast("dialtone"));
        data.dialxfer.offhook = getMSTimeout(cp);
	trunkStep(TRUNK_STEP_FLASH);
	return false;
}

bool Trunk::scrHold(void)
{
	const char *cp = group->getLast("hold");
	if(!cp)
		cp = "";

	strcpy(data.dialxfer.digits, cp);
	data.dialxfer.interdigit = group->getDialspeed();
        data.dialxfer.digit = data.dialxfer.digits;
        data.dialxfer.exit = true;
        data.dialxfer.timeout = 0;
	cp = getKeyword("onhook");
	if(!cp)
		cp = getKeyword("flash");
	if(!cp)
		cp = getValue(group->getLast("flash"));
        data.dialxfer.onhook = getMSTimeout(cp);
	cp = getKeyword("offhook");
	if(!cp)
		cp = getKeyword("dialtone");
	if(!cp)
		cp = getValue(group->getLast("dialtone"));
        data.dialxfer.offhook = getMSTimeout(cp);
	trunkStep(TRUNK_STEP_FLASH);
	return false;
}
	
bool Trunk::scrCopy(void)
{
	char buffer[65];
	const char *prefix = getPrefixPath();
	const char *file = getKeyword("file");
	const char *cp = getKeyword("maxTime");
	const char *opt;
	const char *ext;
	char *wp;
	Name *scr = ScriptInterp::getName();
	Line *line = getLine();
	writemode_t mode = WRITE_MODE_REPLACE;
	unsigned len = 0;
	CopyThread *copy;

	if(!strnicmp(line->cmd, "append", 6))
		mode = WRITE_MODE_APPEND; 

        snprintf(apppath, sizeof(apppath), "%s", scr->name);
        wp = strstr(apppath, "::");
        if(wp)
                *wp = 0;

	if(!file)
		file = getValue(NULL);

	ext = strrchr(file, '/');
	if(ext)
		ext = strrchr(ext, '.');
	else
		ext = strrchr(file, '.');

	if(!ext)
	{
		ext = getKeyword("extension");
		if(!ext)
			ext = getSymbol(SYM_EXTENSION);
	}
	else
		ext = "";

	if(prefix)
		snprintf(buffer, sizeof(buffer), "%s/%s%s", prefix, file, ext);
	else
		snprintf(buffer, sizeof(buffer), "%s%s", file, ext);

	thread = copy = new CopyThread(this, buffer, mode);

	while(len < 255 && (NULL != (opt = getValue(NULL))))
	{
		snprintf(copy->list + len, 256 - len, ",%s", opt);
		len = strlen(copy->list);
	}

	if(cp)
		data.sleep.wakeup = getSecTimeout(cp);
	else
		data.sleep.wakeup = 60000;
	trunkStep(TRUNK_STEP_THREAD);
	return false;
}	

bool Trunk::scrBuild(void)
{
	Translator *tts;
	char *err;
	const char *cp = getKeyword("maxTime");
	char buffer[65];
	const char *lang = getKeyword("language");
	const char *voice = getKeyword("voice");
	const char *file = getKeyword("file");
	const char *prefix = getPrefixPath();
	const char *ext;
	CopyThread *copy;

	if(!file)
		file = getValue(NULL);

	if(voice && !lang)
		lang = keyvoices.getLast(voice);

	if(!lang)
		lang = getSymbol(SYM_LANGUAGE);

	if(!voice)
		voice = getSymbol(SYM_VOICE);

	tts = getTranslator(lang);

	if(!tts)
	{
		error("language-unsupported");
		return true;
	}

	err = tts->speak(this);
	if(err)
	{
		error(err);
		return true;
	}

	ext = strrchr(file, '/');
	if(ext)
		ext = strrchr(ext, '.');
        else
                ext = strrchr(file, '.');

        if(!ext)
        {
                ext = getKeyword("extension");
                if(!ext)
                        ext = getSymbol(SYM_EXTENSION);
        }
        else
                ext = "";
	
        if(prefix)
                snprintf(buffer, sizeof(buffer), "%s/%s%s", prefix, file, ext);
        else
                snprintf(buffer, sizeof(buffer), "%s%s", file, ext);

	thread = copy = new CopyThread(this, buffer, WRITE_MODE_REPLACE);
	if(!thread)
	{
		error("no-thread");
		return true;
	}

	strcpy(copy->list, data.play.list);
        if(cp)
                data.sleep.wakeup = getSecTimeout(cp);
        else
                data.sleep.wakeup = 60000;

        trunkStep(TRUNK_STEP_THREAD);
        return false;
}

bool Trunk::scrSpeak(void)
{
	Translator *tts;
	char *err;
	const char *member = getMember();
	const char *gain = getKeyword("gain");
	const char *speed = getKeyword("speed");
	const char *pitch = getKeyword("pitch");
	const char *lang = getKeyword("language");
	const char *voice = getKeyword("voice");
	const char *vol = getKeyword("volume");

	if(!vol)
		vol = getSymbol(SYM_VOLUME);

	if(!vol)
		vol = "100";

	if(voice && !lang)
		lang = keyvoices.getLast(voice);

	if(!lang)
		lang = getSymbol(SYM_LANGUAGE);

	apppath[0] = 0;

	if(!member)
		member="all";

	data.play.write = WRITE_MODE_NONE;
	data.play.text = getKeyword("text");
	if(!stricmp(member, "any"))
		data.play.mode = PLAY_MODE_ANY;
	else
		data.play.mode = PLAY_MODE_NORMAL;

	data.play.term = 0;
	data.play.voice = voice;

	tts = getTranslator(lang);

	if(!tts)
	{
		error("language-unsupported");
		return true;
	}
	err = tts->speak(this);
	if(err)
	{
		error(err);
		return true;
	}
	while(*data.play.name == ',')
		++data.play.name;

	if(gain)
		data.play.gain = (float)strtod(gain, NULL);
	else
		data.play.gain = 0.0;

	if(!speed)
		speed = "normal";

	if(!stricmp(speed, "fast"))
		data.play.speed = SPEED_FAST;
	else if(!stricmp(speed, "slow"))
		data.play.speed = SPEED_SLOW;
	else
		data.play.speed = SPEED_NORMAL;

	if(pitch)
		data.play.pitch = (float)strtod(pitch, NULL);
	else
		data.play.pitch = 0.0;

	data.play.extension = getKeyword("extension");
	if(!data.play.extension)
		data.play.extension = getSymbol(SYM_EXTENSION);

	data.play.offset = data.play.limit = 0;
	data.play.timeout = 0;
	data.play.maxtime = 0;
	data.play.repeat = 0;
	data.play.lock = false;
	data.play.volume = atoi(vol);
	trunkStep(TRUNK_STEP_PLAY);
	return false;
}
	
bool Trunk::altDial(void)
{
	unsigned len = 0, ilen = 0;
	const char *cp;
	const char *mem = getMember();
	char digits[64];
	char *digbuf = digits;
	char intprefix[5];
	bool intflag = false, natflag = false, soft = false;
	bool rewrite = false;

	if(!mem)
	{
		rewrite = true;
		mem = "dial";
	}

	data.dialxfer.dialer = DTMF_DIALER;
	if(!stricmp(mem, "dtmf"))
		soft = true;
	else if(!stricmp(mem, "pulse"))
	{
		data.dialxfer.dialer = PULSE_DIALER;
		soft = true;
	}
	else if(!stricmp(mem, "mf"))
	{
		data.dialxfer.dialer = MF_DIALER;
		soft = true;
	}

	cp = getKeyword("offhook");
	if(!cp)
		cp = getKeyword("flash");

	if(cp)
		data.dialxfer.offhook = getMSTimeout(cp);
	else
		data.dialxfer.offhook = 0;

	cp = getKeyword("onhook");
	if(cp)
		data.dialxfer.onhook = getMSTimeout(cp);
	else
		data.dialxfer.onhook = 0;

	while(NULL != (cp = getValue(NULL)) && len < sizeof(digits) - 1)
	{
		if(intflag)
			intflag = false;

		while(*cp && len < sizeof(digits) - 1)
		{
			if(driver->getCaps() & Driver::capPSTN)
			{
				switch(*cp)
				{
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					if(intflag && ilen < 4)
					{
						intprefix[ilen++] = *cp;
						break;
					}
				case '*':
				case '#':
				case ',':
				case '.':
				case 'a':
				case 'A':
				case 'b':
				case 'B':
				case 'c':
				case 'C':
				case 'd':
				case 'D':
					digits[len++] = *cp;
					break;
				case 's':
				case 'S':
				case 'k':
				case 'K':
					if(soft)
						digits[len++] = *cp;
			
					intflag = false;
					break;	
				case 't':
				case 'T':
				case 'p':
				case 'P':
				case '!':
				case 'f':
				case 'F':
				case 'm':
				case 'M':
					if(!stricmp(mem, "dial") || soft)
					{
						soft = true;
						digits[len++] = *cp;
					}
					intflag = false;
					break;	
				case '+':
					if(!len)
						intflag = true;
					break;
				default:
					intflag = false;
					break;
				}
			}
			else
			{
				digits[len++] = *cp;
			}
			cp++;
		}
	}
	digits[len] = 0;
	intprefix[ilen] = 0;
	digbuf = digits;

	cp = getKeyword("country");
	if(!cp)
		cp = "1";

	if(ilen)
	{
		if(!stricmp(intprefix, cp))
		{
			natflag = true;
			ilen = 0;
			digbuf += strlen(cp);
		}
	}

	cp = getKeyword("prefix");
	if(!cp)
	{
		if(!strnicmp(mem, "loc", 3))
			cp = group->getLast("local");
		else if(!strnicmp(mem, "nat", 3))
			cp = group->getLast("national");
		else if(!stricmp(mem, "cell"))
			cp = group->getLast("cell");
		else if(!strnicmp(mem, "int", 3))
			cp = group->getLast("international");
		else if(ilen)
			cp = group->getLast("international");
		else if(natflag)
			cp = group->getLast("national");
		else
			cp = group->getLast("prefix");

		if(cp)
			if(!strnicmp(digits, cp, sizeof(cp)))
				cp = "";
	}
	if(cp)
		rewrite = false;
	if(!cp)
		cp = "";

	strcpy(data.dialxfer.digits, cp);
	len = strlen(cp);

	cp = getKeyword("suffix");
	if(!len && !cp && rewrite)
	{
		dialRewrite(digbuf);
		len = strlen(data.dialxfer.digits);
	}
	else
	{
		strncpy(data.dialxfer.digits + len, digbuf, sizeof(data.dialxfer.digits) - len - 1);
		len += strlen(digbuf);
	}

	if(cp)
	{
		strncpy(data.dialxfer.digits + len, cp, sizeof(data.dialxfer.digits) - len - 1);
		len += strlen(cp);
	}

	data.dialxfer.digits[len] = 0;
	data.dialxfer.callingdigit = getKeyword("origin");
	data.dialxfer.digits[sizeof(data.dialxfer.digits) - 1] = 0;
	data.dialxfer.interdigit = group->getDialspeed();
	data.dialxfer.digittimer = data.dialxfer.interdigit / 2;
	data.dialxfer.digit = data.dialxfer.digits;
	data.dialxfer.exit = false;

	if(soft || !stricmp(mem, "digits"))
		data.dialxfer.timeout = 0;
	else
	{
		cp = getKeyword("maxTime");
		if(cp)
			data.dialxfer.timeout = getSecTimeout(cp);
		else
			data.dialxfer.timeout = group->getAnalysis() * 1000;
	}
//	advance();
//	return true;

	if(soft)
		trunkStep(TRUNK_STEP_SOFTDIAL);
	else
		trunkStep(TRUNK_STEP_DIALXFER);
	return false;
}

bool Trunk::scrDial(void)
{
	const char *mem = getMember();
	Trunk *trk, *ttrk = NULL;
	TrunkEvent event;
	bool rtn;
	const char *pid;
	const char *ringback = getKeyword("ringback");
	const char *count = getKeyword("count");
	const char *transfer = getKeyword("transfer");
	const char *origin = getKeyword("origin");
	const char *name = getKeyword("name");
	char pbuf[5], trkname[64];

	if(!origin)
		origin = extNumber;

	if(!isStation)
		return altDial();

	if(transfer)
		if(!*transfer)
			transfer = NULL;

	if(transfer)
	{
		if(*transfer != '-')
		{
			pid = strchr(transfer, '-');
			if(pid)
				transfer = pid;
		}
		ttrk = Driver::getTrunk(transfer, false, driver);
		if(!ttrk)
			transfer = NULL;
		else
			ttrk->isRinging = true;
	}

	if(!mem)
		mem = "";

	if(!ringback)
		ringback = getKeyword("ringtone");

	if(!ringback)
		ringback = "ringback";

	if(!count)
		count = "-1";

	data.tone.freq1 = data.tone.freq2 = 0;
	data.tone.ampl1 = data.tone.ampl2 = 0;
	data.tone.wakeup = data.tone.duration = 4000;
	data.tone.loops = atoi(count);
	data.tone.tone = getphTone(ringback);
	if(!data.tone.tone)
		data.tone.tone = getphTone("ringback");

	if(!strnicmp(mem, "int", 3))
	{
		while(NULL != (mem = getValue(NULL)))
		{
			snprintf(trkname, sizeof(trkname), "ext/%s", mem);
			trk = Driver::getTrunk(trkname);
			if(!trk)
			{
				if(!trunkSignal(TRUNK_SIGNAL_INVALID))
					error("dial-invalid-extension");
				return true;
			}
			if(trk == this)
				continue;
			event.id = TRUNK_START_INTERCOM;
			event.parm.intercom.tid = id;
			event.parm.intercom.transfer = transfer;
			rtn = trk->postEvent(&event);
			if(ttrk)
			{
				event.id = TRUNK_START_RINGING;
				event.parm.tid = ttrk->id;
				trk->postEvent(&event);
			}
			if(rtn)
			{
				isRinging = true;
				pid = trk->getSymbol(SYM_GID);
				if(pid)
					pid = strchr(pid, '-');
				else
				{
					snprintf(trkname, sizeof(trkname), "%s/%d",
						driver->getName(), trk->id);
					pid = trkname;
				}
				setSymbol(SYM_INTERCOM, pid);
				data.tone.recall = false;
				data.tone.dialing = trk;
				trunkStep(TRUNK_STEP_TONE);
				return false;
			}
		}
		if(!trunkSignal(TRUNK_SIGNAL_BUSY))
			error("dial-busy");
		return true;
	}
	return altDial();
}

bool Trunk::scrListen(void)
{
	const char *cp, *pp;
	const char *gain = getKeyword("gain");
	const char *vol = getKeyword("vol");
	const char *save = getKeyword("var");
	const char *mem = getMember();
	int wordcount = 0;
	char *tok;
	const char *wl;
	Symbol *sym = NULL;

	if(!mem)
		mem = "";

	if(!(driver->getCaps() & Driver::capListen))
	{
		if(!trunkSignal(TRUNK_SIGNAL_FAIL))
			error("asr-not-supported");
		return true;
	}

	if(asr.fd < 0)
	{
		error("asr-not-active");
		return true;
	}

	cp = getKeyword("count");
	if(cp)
			data.listen.count = atoi(cp);
	else
			data.listen.count = atoi(mem);

	pp = getKeyword("nextWord");
	if(!pp)
		pp = getKeyword("next");
	if(!pp)
		pp = getKeyword("timeout");
	if(!pp)
		pp = getValue(cp);
	if(pp)
		data.listen.first = getSecTimeout(pp);

        cp = getKeyword("firstWord");
        if(!cp)
                cp = getKeyword("first");
        if(cp)
		data.listen.first = getSecTimeout(cp);

	if(data.listen.count > 0)
		asr.partial = true;
	else
		asr.partial = false;

	data.listen.wordlist[0] = NULL;

	wl = getKeyword("wordlist");
	/* XXX fixme */
	/*if(wl)
	{
		snprintf(data.listen.buffer, sizeof(data.listen.buffer), "%s", wl);
		wl = data.listen.buffer;
		data.listen.wordlist[wordcount++] = strtok_r(wl, ",", &tok);
		while(wordcount < 31)
		{
			data.listen.wordlist[wordcount] = strtok_r(NULL, ",", &tok);
			if(!data.listen.wordlist[wordcount])
				break;
			wordcount++;
		}

		data.listen.wordlist[wordcount] = NULL;
		asr.partial = true;
	}*/

	data.listen.save = NULL;
	data.listen.next = getSecTimeout(pp);
	data.listen.term = getDigitMask("exit");

        if(gain)
                data.listen.gain = (float)strtod(gain, NULL);
        else
                data.listen.gain = 0.0;

        if(!vol)
                vol = getSymbol(SYM_VOLUME);

        if(!vol)
                vol = "100";

	if(save)
	{
		if(*save == '&')
			++save;
		sym = mapSymbol(save);
		if(!sym)
		{
			error("cannot-create-save");
			return true;
		}

		if(sym->type = symCONST)
		{
			error("cannot-write");
			return true;
		}

		data.listen.save = (void *)sym;
		sym->data[0] = 0;
		sym->type = symINITIAL;
		/*if(sym->flags.commit)
			commit(sym);*/
	}
	trunkStep(TRUNK_STEP_LISTEN);
	return false;
}

bool Trunk::scrAsr(void)
{
	const char *mem = getMember();
	const char *result = getKeyword("result");
	const char *opt;
	Symbol *sym = NULL;

	memset(&acuasr, 0, sizeof(acuasr));

	opt = getKeyword("template");
	if(!opt)
	{
		error("asr-no-template");
		return true;
	}
	snprintf(acuasr.tmpl, sizeof(acuasr.tmpl), "%s", opt);

	opt = getKeyword("bargein");
	if(!opt)
		opt = "no";
	if(!stricmp(opt, "yes") || !stricmp(opt, "1") || !stricmp(opt, "y"))
		acuasr.barge_in = true;
	else
		acuasr.barge_in = false;

	opt = getKeyword("maxTime");
	if(opt)
		acuasr.timeout = (getSecTimeout(opt) * 1000) / 16;
	else
		acuasr.timeout = 3750;

	opt = getKeyword("buffer");
	if(opt)
		acuasr.hold_frames = (getSecTimeout(opt) * 1000) / 16;
	else
		acuasr.hold_frames = 13;

	opt = getKeyword("initial");
	if(opt)
		acuasr.on_frames = (getSecTimeout(opt) * 1000) / 16;
	else
		acuasr.on_frames = 625;

	opt = getKeyword("interword");
	if(opt)
		acuasr.pause_frames = (getSecTimeout(opt) * 1000) / 16;
	else
		acuasr.pause_frames = 31;

	if(result)
	{
		if(*result == '&')
			++result;
		sym = mapSymbol(result);
		if(!sym)
		{
			error("cannot-create-save");
			return true;
		}
		if(sym->type = symCONST)
		{
			error("cannot-write");
			return true;
		}
		acuasr.result = (void *)sym;
		sym->data[0] = 0;
		sym->type = symINITIAL;
	}
	trunkStep(TRUNK_STEP_LISTEN);
	return false;
}

bool Trunk::scrSay(void)
{
	const char *cp;
	const char *prefix = getPrefixPath();
	const char *cache = getKeyword("cache");
	const char *gain = getKeyword("gain");
	const char *speed = getKeyword("speed");
	const char *pitch = getKeyword("pitch");
	const char *vol = getKeyword("volume");
	unsigned len = 0, lc = 0;
	unsigned long sum = 0l, sum1 = 0l;
	const char *mem = getMember();

	if(!mem)
		mem = "text";

	if(!stricmp(mem, "nocache") && !tts)
	{
		advance();
		return true;
	}

	if(!stricmp(mem, "cache") && !tts)
	{
		advance();
		return true;
	}

	if(cache && !tts)
		cache = NULL;

	if(!hasTTS())
	{
		advance();
		return true;
	}

	if(!vol)
		vol = getSymbol(SYM_VOLUME);

	if(!vol)
		vol = "100";

	data.play.list[0] = 0;

	while(NULL != (cp = getValue(NULL)) && len < sizeof(data.play.list))
	{
		if(len)
		{
			if(tts)
				data.play.list[len++] = ' ';
			else
				data.play.list[len++] = '+';
		}
		strncpy(data.play.list + len, cp, sizeof(data.play.list) - len);
		len += strlen(cp);
	}
	data.play.list[len] = 0;
	if(!data.play.list[0])
	{
		error("tts-no-output");
		return true;
	}


	data.play.text = getKeyword("text");
	if(!data.play.text)
		data.play.text = data.play.list;

	data.play.voice = getKeyword("voice");
	if(!data.play.voice)
		data.play.voice = getSymbol(SYM_VOICE);
	data.play.list[sizeof(data.play.list) - 1] = 0;
#ifdef	HAVE_TGI
	if(!tts)
	{
		libtts(data.play.list, TTS_GATEWAY_TEXT);
		sprintf(data.play.list, "temp/.tts.%d.ul", id);
	}
#else
	if(!tts)
	{
		error("no-tts");
		return true;
	}
#endif
	data.play.name = data.play.list;
	data.play.repeat = 0;
	data.play.lock = false;
	data.play.maxtime = 0;
	data.play.lock = false;
	if(tts)
		data.play.timeout = 0;
	else
		data.play.timeout = getSecTimeout(getSymbol(SYM_PLAYWAIT));
	data.play.volume = atoi(vol);
	data.play.term = 0;
	data.play.write = WRITE_MODE_NONE;
	data.play.mode = PLAY_MODE_TEMP;
	data.play.limit = data.play.offset = 0;
	data.play.extension = NULL;

	if(gain)
		data.play.gain = (float)strtod(gain, NULL);
	else
		data.play.gain = 0.0;

	if(!speed)
		speed = "normal";

	if(!stricmp(speed, "fast"))
		data.play.speed = SPEED_FAST;
	else if(!stricmp(speed, "slow"))
		data.play.speed = SPEED_SLOW;
	else
		data.play.speed = SPEED_NORMAL;

	if(pitch)
		data.play.pitch = (float)strtod(pitch, NULL);
	else
		data.play.pitch = 0.0;

	if(NULL != (cp = getKeyword("offset")))
		data.play.offset = atol(cp);
	if(NULL != (cp = getKeyword("volume")))
		data.play.volume = atoi(cp);
	if(NULL != (cp = getKeyword("timeout")) && !tts)
		data.play.timeout = getSecTimeout(cp);
	if(NULL != (cp = getKeyword("limit")))
		data.play.limit = atol(cp);

	if(!stricmp(mem, "cache") && !cache)
	{
		sprintf(apppath, "cache/-tts-%s-", data.play.voice);
		len = strlen(apppath);
		cp = data.play.list;
		while(*cp)
		{
			if(*cp != ' ')
			{
				++lc;
				sum ^= (sum << 3) ^ (*cp & 0x1f);
				sum1 ^= ((sum >> 29) ^ lc) ^ (*cp & 0x0f);
			}
			++cp;
		}
		cp = data.play.list;
		while(*cp)
		{
			sum1 = (sum1 << 3) ^ (*cp & 0x1f);
			++cp;
		}
		sprintf(apppath + len, "%08lx%08lx", sum, sum1);
		char *pp = apppath;
		while(*pp)
		{
			*pp = tolower(*pp);
			++pp;
		}
		data.play.cache = apppath;
	}
	else if(cache)
	{
		if(!prefix && !strchr(cache, '/'))
			prefix = "cache";
		if(prefix)
		{
			snprintf(apppath, sizeof(apppath), "%s/%s", prefix, cache);
			cache = apppath;
		}
		data.play.cache = cache;
	}
	else
		data.play.cache = NULL;

	if(tts)
	{
		if(!stricmp(mem, "file"))
			data.play.mode = PLAY_MODE_FILE;
		else
			data.play.mode = PLAY_MODE_TEXT;
		trunkStep(TRUNK_STEP_PLAY);
	}
	else
		trunkStep(TRUNK_STEP_PLAYWAIT);
	return false;
}

bool Trunk::scrAltPlay(void)
{
	if(hasTTS())
	{
		advance();
		return true;
	}
	return scrPlay();
}

bool Trunk::scrAltSpeak(void)
{
	if(hasTTS())
	{
		advance();
		return true;
	}
	return scrSpeak();
}

bool Trunk::scrPlay(void)
{
	Name *scr = ScriptInterp::getName();
	char *p;
	const char *cp;
	unsigned long mask = 0;
	const char *prefix = getPrefixPath();
	const char *gain = getKeyword("gain");
	const char *speed = getKeyword("speed");
	const char *pitch = getKeyword("pitch");
	const char *vol = getKeyword("volume");
	bool feed = false;
	unsigned len = 0;

	snprintf(apppath, sizeof(apppath), "%s", scr->name);
	p = strstr(apppath, "::");
	if(p)
		*p = 0;

	const char *member = getMember();
	if(!member)
		member = "all";

	if(!stricmp(member, "feed"))
		feed = true;

	data.play.write = WRITE_MODE_NONE;

	if(!stricmp(member, "moh"))
	{
		data.play.mode = PLAY_MODE_MOH;
	}
	else if(!stricmp(member, "any"))
		data.play.mode = PLAY_MODE_ANY;
	else if(!stricmp(member, "one"))
		data.play.mode = PLAY_MODE_ONE;
	else if(!stricmp(member, "temp") || !stricmp(member, "tmp"))
		data.play.mode = PLAY_MODE_TEMP;
	/* else if(0 != (mask = getScriptMask(member)))
		data.play.mode = PLAY_MODE_ANY; */
	else
		data.play.mode = PLAY_MODE_NORMAL;

	data.play.text = getKeyword("text");
	data.play.voice = getKeyword("voice");

	if(mask)
	{
		if((mask & scr->mask) != mask)
		{
			advance();
			return true;
		}
	}

	data.play.list[0] = 0;
	while(NULL != (cp = getValue(NULL)))
	{
		if(len)
			data.play.list[len++] = ',';
		if(prefix)
			snprintf(data.play.list + len, sizeof(data.play.list) - len, "%s/%s", prefix, cp);
		else
			strncpy(data.play.list + len, cp, sizeof(data.play.list) - len);
		len = strlen(data.play.list);
		if(feed)
			break;
	}

	if(!vol)
		vol = getSymbol(SYM_VOLUME);
	if(!vol)
		vol = "100";

	data.play.list[sizeof(data.play.list) - 1] = 0;
	data.play.name = data.play.list;
	data.play.volume = atoi(vol);
	data.play.timeout = 0;
	data.play.repeat = 0;
	data.play.lock = false;
	data.play.maxtime = 0;
	data.play.term = 0;
	data.play.lock = false;
	data.play.extension = getKeyword("extension");
	if(!data.play.extension)
		data.play.extension = getSymbol(SYM_EXTENSION);

	if(feed)
		data.play.maxtime = getTimeout("maxTime");

	while(*data.play.name == ',')
		++data.play.name;
	if(!*data.play.name)
	{
		error("play-no-files");
		return true;
	}

	if(gain)
		data.play.gain = (float)strtod(gain, NULL);
	else
		data.play.gain = 0.0;

	if(!speed)
		speed = "normal";

	if(!stricmp(speed, "fast"))
		data.play.speed = SPEED_FAST;
	else if(!stricmp(speed, "slow"))
		data.play.speed = SPEED_SLOW;
	else
		data.play.speed = SPEED_NORMAL;

	if(pitch)
		data.play.pitch = (float)strtod(pitch, NULL);
	else
		data.play.pitch = 0.0;

	data.play.limit = data.play.offset = 0;

	if(NULL != (cp = getKeyword("offset")))
		data.play.offset = atol(cp);

	if(NULL != (cp = getKeyword("limit")))
		data.play.limit = atol(cp);

	if(NULL != (cp = getKeyword("volume")))
		data.play.volume = atoi(cp);

	trunkStep(TRUNK_STEP_PLAY);
	return false;
}	

bool Trunk::scrFlash(void)
{
	const char *cp = getKeyword("onhook");
	if(!cp)
		cp = getKeyword("flash");
	if(!cp)
		cp = getValue(group->getLast("flash"));
	data.dialxfer.onhook = getMSTimeout(cp);

	cp = getKeyword("offhook");
	if(!cp)
		cp = getKeyword("dialtone");
	if(!cp)
		cp = getValue(group->getLast("dialtone"));

	data.dialxfer.offhook = getMSTimeout(cp);

	data.dialxfer.digit = NULL;
	trunkStep(TRUNK_STEP_FLASH);
	return false;
}

bool Trunk::scrCollect(void)
{
	unsigned copy = 0;
	Symbol *sym = NULL;
	const char *cp = getKeyword("count");
	const char *mem = getMember();
	const char *var = getKeyword("var");
	const char *ignore = getKeyword("ignore");
	const char *term = getKeyword("exit");
	unsigned digpos = 0, digscan;
	bool trim = false;

	if(!ignore)
		ignore = "";

	if(!term)
		term = "";

	if(!mem)
		mem = "all";

	if(!cp)
		cp = getKeyword("digits");

	if(!cp)
		cp = getValue("0");

	if(var)
		if(*var == '&')
			++var;

	data.collect.count = atoi(cp);
	if(data.collect.count > MAX_DIGITS)
		data.collect.count = MAX_DIGITS;

	if(var)
		sym = mapSymbol(var, data.collect.count);

	if(sym)
	{
		if(sym->type = symCONST)
		{
			error("collect-read-only");
			return true;
		}
	}

	if(sym)
	{
		copy = sym->size;
		sym->data[0] = 0;
		/*if(sym->flags.commit)
			commit(sym);*/
	}

	if(copy > data.collect.count)
		copy = data.collect.count;		

	data.collect.var = (void *)sym;
	data.collect.map = NULL;	
	data.collect.first = data.collect.timeout = getInterdigit("timeout");
	data.collect.term = getDigitMask("exit");
	data.collect.ignore = getDigitMask("ignore");

	cp = getKeyword("nextDigit");
	if(!cp)
		cp = getKeyword("next");
	if(cp)
	{
		data.collect.timeout = getSecTimeout(cp);
		cp = getKeyword("timeout");
		if(!cp)
			data.collect.first = data.collect.timeout;
	}

	cp = getKeyword("firstDigit");
	if(!cp)
		cp = getKeyword("first");
	if(cp)
		data.collect.first = getSecTimeout(cp);

	if(!stricmp(mem, "clear"))
	{
		digits = 0;
		dtmf.bin.data[0] = 0;
	}
	else if(!stricmp(mem, "trim"))
		trim = true;
	else if(!stricmp(mem, "input"))
		trim = true;
	
	while(digpos < digits)
	{
		if(strchr(term, dtmf.bin.data[digpos]))
		{
			if(copy > digpos)
				copy = digpos;
			if(sym)
			{
				if(copy)
					strncpy(sym->data, dtmf.bin.data, copy);
				sym->data[copy] = 0;
				/*if(sym->flags.commit)
					commit(sym);*/
				digscan = ++digpos;
				while(digscan < digits)
				{
					dtmf.bin.data[digscan - digpos] = dtmf.bin.data[digscan];
					++digscan;
				}
				digits = digscan - digpos;
				dtmf.bin.data[digits] = 0;
				digits = digscan;
			}	
			else
			{
				digits = digpos;
				dtmf.bin.data[digits] = 0;
			}
			advance();
			return true;
		}

		if(strchr(ignore, dtmf.bin.data[digpos]))
		{
			digscan = digpos;
			while(digscan < digits)
			{
				dtmf.bin.data[digscan] = dtmf.bin.data[digscan + 1];
				++digscan;
			}
			continue;
		}	

		if(++digpos >= data.collect.count)
		{
			if(sym)
			{
				if(copy)
					strncpy(sym->data, dtmf.bin.data, copy);
				sym->data[copy] = 0;
				while(digpos < digits)
				{
					dtmf.bin.data[digpos - data.collect.count] = dtmf.bin.data[digpos];
					++digpos;
				}
				digits = digpos - data.collect.count;
				dtmf.bin.data[digits] = 0;
			}
			else if(trim || *term)
			{
				digits = digpos;
				dtmf.bin.data[digits] = 0;
			}
			advance();
			return true;
		}
	}
	trunkStep(TRUNK_STEP_COLLECT);
	return false;
}

bool Trunk::scrAccept(void)
{
	if (group->getAccept())
	{
		advance();
		return true;
	}

	trunkStep(TRUNK_STEP_ACCEPT);
	return false;
}

bool Trunk::scrReject(void)
{
	trunkStep(TRUNK_STEP_REJECT);
	return false;
}
	
bool Trunk::scrAnswer(void)
{
	const char *mem = getMember();
	const char *kw;
	Trunk *trk = NULL;
	TrunkEvent event;
	unsigned pid;
	char *port;
	char trkname[MAX_NAME_LEN];

	/*mem = getMember();
	if(mem)
		if(!stricmp(mem, "media"))
			data.answer.media = 1;*/

	if(NULL != (kw = getKeyword("maxRing")))
		data.answer.rings = atoi(kw);
	else 	
		data.answer.rings = atoi(getValue("0"));
	if(NULL != (kw = getKeyword("maxTime")))
		data.answer.timeout = getSecTimeout(kw);
	else
		data.answer.timeout =  getSecTimeout(getValue(group->getLast("ringtime")));

	data.answer.station = getStation();
	data.answer.fax = getKeyword("fax");

	trunkStep(TRUNK_STEP_ANSWER);
	return false;
}	

bool Trunk::scrHangup(void)
{
	TrunkEvent event;
	Trunk *trk;
	TrunkGroup *grp = NULL;
	const char *mem = getMember();
	unsigned port, tspan;
	const char *id = getKeyword("id");

	if(!mem)
		mem = "self";
	else if(!isAdmin())
	{
		error("admin-required");
		return true;
	}

	if(!stricmp(mem, "port"))
	{
		event.id = TRUNK_STOP_DISCONNECT;
		mem = getValue(id);
		trk = Driver::getTrunk(mem, false, getDriver());

		if(!trk)
		{
			error("hangup-port-id-invalid");
			return true;
		}

		if(trk == this)
		{
			error("hangup-port-self-reference");
			return true;
		}

		if(*mem != ' ' && !isAdmin())
		{
			error("hangup-port-admin-required");
			return true;
		}

		trk->postEvent(&event);
		advance();
		return true;
	}

	if(!stricmp(mem, "span"))
	{
		if(!isAdmin())
		{
			error("hangup-span-admin-required");
			return true;
		}
		mem = getValue(id);
		if(!mem)
		{
			error("hangup-span-id-missing");
			return true;
		}
		tspan = atoi(mem);
		if(driver->spanEvent(tspan, &event))
			advance();
		else
			error("hangup-span-id-invalid");
		return true;			
	}

        if(!stricmp(mem, "card"))
        {
		if(!isAdmin())
		{
			error("hangup-card-admin-required");
			return true;
		}
                mem = getValue(id);
                if(!mem)
                {
                        error("hangup-card-id-missing");
                        return true;
                }
                tspan = atoi(mem);
                if(driver->cardEvent(tspan, &event))
                        advance();
                else
                        error("hangup-card-id-invalid");
                return true;
        }

	if(!stricmp(mem, "group"))
	{
		if(!isAdmin())
		{
			error("hangup-group-admin-required");
			return true;
		}
		mem = getValue(id);
		if(mem)
			grp = getGroup(mem);
		if(!grp)
		{
			error("hangup-group-id-missing");
			return true;
		}

	        for(port = 0; port < driver->getTrunkCount(); ++port)
        	{
                	if(driver->getTrunkGroup(port) != grp)
                        	continue;

	                trk = driver->getTrunkPort(port);
        	        if(!trk || trk == this)
                        	continue;

			event.id = TRUNK_STOP_DISCONNECT;
			trk->postEvent(&event);
		}

		advance();
		return true;
	}
	
	if(id)
	{
		if(!strchr(id, '-') && !isAdmin())
		{
			error("admin-required");
			return true;
		}
		trk = Driver::getTrunk(id, false, getDriver());
		if(!trk)
		{
			error("hangup-id-invalid");
			return true;
		}
		event.id = TRUNK_STOP_DISCONNECT;
		trk->postEvent(&event);
		advance();
		return true;
	}

	if(!ScriptInterp::signal((unsigned int)0))
		ScriptInterp::exit();

	return true;
}

bool Trunk::scrAudit(void)
{
	const char *mem = getMember();
	char buffer[256];
	char overflow[256];
	Line *line = NULL;
	const char *val, *tag;
	int argc = 0;
	unsigned len = 0;
	bool post = false;

	if(!mem)
		mem = "set";

	if(!stricmp(mem, "clear"))
		cdrv = NULL;
	else if(!stricmp(mem, "log"))
		line = getLine();
	else if(!stricmp(mem, "post"))
	{
		post = true;
		line = getLine();
	}
	else
		cdrv = getLine();

	if(!line)
	{
		advance();
		return true;
	}

	if(post)
	{
		strcpy(buffer, "cdr");
		len = 3;
	}
	else
	{
		strcpy(buffer, "audit");
		len = 5;
	}
	while(argc < line->argc && len < sizeof(buffer) - 3)
	{
		tag = line->args[argc++];
		if(*tag == '%')
			val = getContent(tag);
		else if(*tag == '=')
			val = getContent(line->args[argc++]);
		else
			continue;

		if(!val)
			continue;

		if(!*val)
			continue;

		urlEncode(val, overflow, sizeof(overflow));
		snprintf(buffer + len, sizeof(buffer) - len, " %s=%s", ++tag, overflow);
		len = strlen(buffer);
	}
	buffer[len++] = '\n';
	buffer[len] = 0;
	control(buffer);
	advance();
	return true;
}

bool Trunk::scrDebug(void)
{
	char buf[256];
	const char *value;

	buf[0] = 0;
	while(NULL != (value = getValue(NULL)))
		strcat(buf, value);
	debug->debugScript(this, buf);
	advance();
	return true;
}

bool Trunk::scrSync(void)
{
	const char *cp;
	timeout_t timer = getTimeout("time");
	time_t now;
	const char *mem = getMember();

	if(!mem)
		mem = "none";

	time(&now);


	if(!strnicmp(mem, "max", 3) || !stricmp(mem, "exit"))
	{
		if(timer)
			exittimer = starttime + (timer / 1000);
		else
			exittimer = 0;
		advance();
		return true;
	}

	if(!strnicmp(mem, "time", 4) || !stricmp(mem, "start"))
	{
		if(timer)
			synctimer = starttime + (timer / 1000);
		else
			synctimer = 0;
		advance();
		return true;
	}

	if(!stricmp(mem, "current"))
	{
		if(timer)
			synctimer = now + (timer / 1000);
		else
			synctimer = 0;
		advance();
		return true;
	}

	timer = timer - ((now - starttime) * 1000);
	if(timer < 1)
	{
		advance();
		return true;
	}
	data.sleep.wakeup = timer;
	data.sleep.save = NULL;
	cp = getKeyword("maxRing");
	if(!cp)
		cp = getValue("0");
	data.sleep.rings = atoi(cp);
	data.sleep.loops = 1;
	trunkStep(TRUNK_STEP_SLEEP);
	return false;
}

bool Trunk::scrStart(void)
{
	Trunk *trunk;
	TrunkGroup *grp = NULL;
	TrunkEvent event;
	Symbol *sym;
	int argc = 0;
	char *argv[32];
	char *tok;
	const char *arg = NULL;
	const char *login = getSymbol("login");
	const char *submit = getKeyword("submit");
	const char *tid = NULL;
	timeout_t exp = 0;
	bool notify = false;
	bool rtn = true;
	const char *mem = getMember();
	char buffer[256];
	char args[512];
	char content[512];
	unsigned alen = 0;
	unsigned offset = 0, last = 0, span = 0;
	bool start = false;
	bool ports = false;
	Driver *startDriver = driver;
	Name *scr = ScriptInterp::getName();
	const char *cp;

	args[0] = 0;

	if(!mem)
		mem = "none";

	if(!stricmp(mem, "wait"))
	{
		exp = getTimeout("maxTime");
		notify = true;
	}
	else if(!stricmp(mem, "port"))
	{
		arg = getKeyword("first");
		if(arg)
			offset = atoi(arg);
		else
			offset = atoi(getValue("0"));
		arg = getKeyword("last");
		if(arg)
			last = atoi(arg);
		else
			last = offset;

		ports = true;
	}
	else if(!stricmp(mem, "id"))
	{
		tid = getValue("0");
		trunk = Driver::getTrunk(tid, true, driver);
		if(!trunk)
		{
			error("invalid-trunk");
			return true;
		}
		offset = last = trunk->id;
		startDriver = trunk->getDriver();
		ports = true;
	}
	else if(!stricmp(mem, "offset"))
	{
		arg = getKeyword("first");
		if(arg)
			offset = atoi(arg) + id;
		else
			offset = atoi(getValue("1")) + id;
		arg = getKeyword("last");
		if(arg)
			last = id + atoi(arg);
		else
			last = driver->getTrunkCount() - 1;
		ports = true;
	}
	else if(!stricmp(mem, "group"))
		start = true;
	else
		exp = getTimeout("maxTime");

	if(!ports && !span)
	{
		arg = getKeyword("group");
		if(!arg)
			arg = getValue("*");

		grp = getGroup(arg);
		if(!grp)
		{
			error("request-unknown-group");
			return true;
		}
		arg = getKeyword("limit");
		if(arg)
		{
			if(atoi(arg) > grp->getStat(STAT_AVAIL_CALLS))
			{
				error("call-limit-exceeded");
				return true;
			}
		}
	}

	if(start)
	{
		if(grp)
			snprintf(args + alen, sizeof(args) - alen, "start %s ", grp->getName());
		else if(tid)
			snprintf(args + alen, sizeof(args) - alen, "start %s ", tid);
		else
			snprintf(args + alen, sizeof(args) - alen, "start %d ", offset);
		alen = strlen(args);
	}

	arg = getKeyword("script");
	if(!arg)
		arg = getValue(NULL);
	if(!arg)
	{
		error("request-no-script");
		return true;
	}

	if(!strnicmp(arg, "::", 2))
	{
		strcpy(content, scr->name);
		tok = strstr(content, "::");
		if(!tok)
			tok = content + strlen(content);
		strcpy(tok, arg);
		arg = content;
	}

	strncpy(args + alen, arg, sizeof(args) - alen);
	alen = strlen(args);

	cp = getSymbol(SYM_GID);
	if(cp)
		cp = strchr(cp, '-');
	else
		cp = "none";

	if(start || ports || span)
		snprintf(args + alen, sizeof(args) - alen, " %s=%s", SYM_PARENT, cp);
	alen = strlen(args);

	while(NULL != (arg = getOption(NULL)))
	{
		if(*arg != '%')
			continue;
		urlEncode(getContent(arg), content, sizeof(content));
		snprintf(args + alen, sizeof(args) - alen, " %s=%s", ++arg, content);
		alen = strlen(args);
	}

        if(submit)
        {
                snprintf(buffer, 255, "%s", submit);
                submit = strtok_r(buffer, ",", &tok);
	}

	while(submit)
        {
                sym = mapSymbol(submit);
                submit = strtok_r(NULL, ",", &tok);
                if(!sym)
                        continue;

		urlEncode(sym->data, content, sizeof(content));
		snprintf(args + alen, sizeof(args) - alen, " %s=%s", sym->id, content);
		alen = strlen(args);
	}

	if(notify)
	{
		if(!exp)
			exp = 1000;

		data.sleep.wakeup = exp;
	        data.sleep.loops = 1;
	        data.sleep.rings = 0;
        	//data.sleep.save = NULL;
        	trunkStep(TRUNK_STEP_SLEEP);
		rtn = false;
	}
	else
		advance();

	if(start)
	{
		trunk = fifo.command(args);
		if(!trunk)
		{
			if(!trunkEvent("start:busy"))
				error("start-ports-busy");
		}
		cp = trunk->getSymbol(SYM_GID);
		if(cp)
			cp = strchr(cp, '-');
		return rtn;
	}

	argv[argc++] = strtok_r(args, " ", &tok);
	while(argc < 31)
		argv[argc++] = strtok_r(NULL, " ", &tok);

	argv[argc] = NULL;

	if(!ports && !span)
	{
		request(grp, argv, exp / 1000, NULL, getSymbol(SYM_GID));
		return rtn;
	}

	if(offset > last)
		last = offset;

	while(offset <= last)
	{
		trunk = startDriver->getTrunkPort(offset++);
		if(!trunk)
			continue;

		if(span)
			if(trunk->span != span)
				continue;

		event.id = TRUNK_START_SCRIPT;
		event.parm.argv = argv;
		if(trunk->postEvent(&event))
		{
			trunk->enterMutex();
			sym = trunk->mapSymbol(SYM_LOGIN);
			if(sym)
				snprintf(sym->data, sym->size + 1, "%s", login);			
			trunk->leaveMutex();
			break;
		}
	}

	if(offset > last)
	{
		event.id = TRUNK_CHILD_FAIL;
		postEvent(&event);
	}

	return rtn;
}

bool Trunk::scrJoin(void)
{
	Trunk *trk = NULL;
	const char *mem = getMember();
	const char *port = NULL;
	char trkname[MAX_NAME_LEN];

	data.join.hangup = false;
	data.join.waiting = NULL;
	data.join.direction = JOIN_FULL;
	data.join.recfn = NULL;
	data.join.local = true;
	data.join.wakeup = 0;

	if(!mem)
		mem = "duplex";

	if(!stricmp(mem, "pickup"))
		port = getSymbol(SYM_PICKUP);
	else if(!stricmp(mem, "trunk"))
	{
		snprintf(trkname, sizeof(trkname), "trunk/%s", getSymbol(SYM_TRUNK));
		port = trkname;
	}
	else if(!stricmp(mem, "intercom"))
		port = getSymbol(SYM_INTERCOM);
	else if(!stricmp(mem, "transfer"))
		port = getSymbol(SYM_TRANSFER);
	else if(!stricmp(mem, "parent"))
		port = getSymbol(SYM_PARENT);
	else
		port = getValue(getKeyword("id"));

	if(!stricmp(mem, "hangup"))
		data.join.hangup = true;

	trk = Driver::getTrunk(port, false, driver);
	if(!trk)
	{
		error("join-no-port");
		return true;
	}

	if(trk->getDriver() != getDriver())
		data.join.local = false;

	mem = getKeyword("count");
	if(mem)
		data.join.count = atoi(mem);
	else
		data.join.count = 0;

	mem = getKeyword("waitTime");
	if(mem)
		data.join.count = getSecTimeout(mem) /
			keythreads.getResetDelay() + 1;

	mem = getKeyword("record");
	if(mem)
	{
		data.join.recfn = (char *)mem;
		data.join.encoding = getKeyword("encoding");
		data.join.annotation = getKeyword("annotation");
		data.join.extension = getKeyword("extension");
		if(!data.join.encoding)
			data.join.encoding = getDefaultEncoding();
		if(!data.join.extension)
			data.join.extension = getSymbol(SYM_EXTENSION);
	}

	mem = getKeyword("maxTime");
	if(mem)
		data.join.wakeup = getSecTimeout(mem);

	data.join.seq = trk->getSequence();
	data.join.waiting = data.join.trunk = trk;
	data.join.wakeup = getTimeout("maxTime");

	if(data.join.local)
		trunkStep(TRUNK_STEP_JOIN);
	else
		trunkStep(TRUNK_STEP_JOIN);
	return false;
}

bool Trunk::scrWait(void)
{
	const char *mem = getMember();
	const char *port = NULL;
	char trkname[MAX_NAME_LEN];

	data.join.local = true;

	if(mem)
	{
		data.join.waiting = NULL;
		data.join.hangup = false;
		if(!stricmp(mem, "hangup"))
			data.join.hangup = true;
		else if(!stricmp(mem, "parent"))
			port = getSymbol(SYM_PARENT);
		else if(!stricmp(mem, "intercom"))
			port = getSymbol(SYM_INTERCOM);
		else if(!stricmp(mem, "pickup") || !stricmp(mem, "hold"))
			port = getSymbol(SYM_PICKUP);
		else if(!stricmp(mem, "trunk"))
		{
			snprintf(trkname, sizeof(trkname), "trunk/%s", getSymbol(SYM_TRUNK));
			port = trkname;
		}
		else if(!stricmp(mem, "transfer"))
			port = getSymbol(SYM_PARENT);
		else if(!stricmp(mem, "recall"))
			port = getSymbol(SYM_RECALL);

		if(port)
			data.join.waiting = Driver::getTrunk(port, false, driver);

		if(data.join.waiting && data.join.waiting->getDriver() != getDriver())
			data.join.local = false;
	}
	else
	{
		data.join.waiting = NULL;
		data.join.hangup = false;
	}

	data.join.count = 0;
	data.join.trunk = NULL;
	if(getKeyword("maxTime"))
		data.join.wakeup = getTimeout("maxTime");
	else
		data.join.wakeup = getTimeout("waitTime");

	trunkStep(TRUNK_STEP_JOIN);

	return false;
}

bool Trunk::scrRing(void)
{
	TrunkGroup *grp = NULL;
	TrunkEvent event;
	Trunk *trk, *ring = NULL;
	unsigned pid = 0, sid, count = driver->getTrunkCount();
	unsigned max = driver->getTrunkCount();
	trunkevent_t evtype = TRUNK_START_RINGING;
	char extbuf[256];
	char trkname[MAX_NAME_LEN];
	char *tok = NULL;
	const char *src = getKeyword("source");
	const char *mem = getMember();
	const char *opt = NULL;
	unsigned i;
	bool found;

	if(!mem)
		mem = "start";

	if(src)
	{
		trk = Driver::getTrunk(src, false, driver);
		if(!trk)
		{
			error("ring-source-invalid");
			return true;
		}
		trk->isRinging = true;
		sid = trk->id;
	}
	else
		sid = id;

	src = getKeyword("group");
	if(src)
		grp = getGroup(src);

	if(!stricmp(mem, "clear"))
	{
		pid = 0;
		evtype = TRUNK_STOP_RINGING;
	}
	else if(!strnicmp(mem, "att", 3))
	{
		grp = getGroup(mem);
		if(grp)
		{
			pid = 0;
		}
		else
		{
			trk = Driver::getTrunk("Ext/0");
			if(trk)
			{
				count = 1;
				pid = trk->id;
			}
			else
			{
				count = 0;
				pid = max;
			}
		}
	}
	else if(!stricmp(mem, "trunk") || !strnicmp(mem, "dial", 4))
	{
		ring = this;
		pid = 0;
	}
	else if(!stricmp(mem, "all"))
	{
		pid = 0;
	}
	else if(!stricmp(mem, "card"))
	{
		// XXX fixme
	}
	else if(!stricmp(mem, "bank"))
	{
		// XXX fixme
	}
	else if(!stricmp(mem, "immediate") || !stricmp(mem, "delayed"))
	{
		if(grp)
			mem = grp->getLast(mem);
		else
			mem = group->getLast(mem);
		if(!mem)
			mem = "";
		strcpy(extbuf, mem);
		mem = strtok_r(extbuf, ",;:", &tok);
		while(mem)
		{
			snprintf(trkname, sizeof(trkname), "ext/%s", mem);
			trk = Driver::getTrunk(trkname);
			mem = strtok_r(NULL, ",;:", &tok);
			if(!trk)
				continue;
			if(trk == this)
				continue;
			if(!trk->isStation)
				continue;
			if(trk->ringIndex[id])
				continue;
			event.id = evtype;
			event.parm.tid = sid;
			trk->postEvent(&event);
		}
		goto end;
	}
	else if(!stricmp(mem, "transfer"))
	{
		while(NULL != (mem = getValue(NULL)))
		{
			snprintf(trkname, sizeof(trkname), "ext/%s", mem);
			trk = Driver::getTrunk(trkname);
			if(!trk)
				continue;
			if(trk == this)
				continue;

			trk->enterMutex();
			event.id = TRUNK_START_INTERCOM;
			event.parm.intercom.tid = id;
			event.parm.intercom.transfer = getKeyword("source");
			trk->postEvent(&event);
			event.id = TRUNK_STOP_RINGING;
			event.parm.tid = id;
			trk->postEvent(&event);
			trk->leaveMutex();
		}
		goto end;
	}
	else if(!stricmp(mem, "start") || !stricmp(mem, "stop"))
	{
		if(!stricmp(mem, "stop"))
			evtype = TRUNK_STOP_RINGING;
		while(NULL != (opt = getValue(NULL)))
		{
			snprintf(trkname, sizeof(trkname), "ext/%s", opt);
			trk = Driver::getTrunk(trkname);
			if(!trk)
				continue;
			if(trk == this)
				continue;
			if(grp)
				if(trk->group != grp)
					continue;
			if(!trk->isStation)
				continue;
			if(!stricmp(mem, "start") && trk->ringIndex[id])
				continue;
			event.id = evtype;
			event.parm.tid = sid;
			trk->postEvent(&event);
		}
		if(evtype == TRUNK_START_RINGING)
			isRinging = true;

		advance();
		return true;
	}
	while(count-- && pid < max)
	{
		snprintf(trkname, sizeof(trkname), "%d", pid++);
		trk = Driver::getTrunk(trkname, false, driver);
		if(!trk)
			continue;
		if(trk == this)
			continue;
		if(grp)
			if(trk->group != grp)
				continue;
		if(!trk->isStation)
			continue;

		if(ring)
		{
			found = false;
			for(i = 0; i < 10; ++i)
			{
				if(ring->dialgroup[i] && trk->dialgroup[i])
				{
					found = true;
					break;
				}
			}
		}
		else
			found = true;

		if(!found)
			continue;

		if(evtype == TRUNK_START_RINGING)
		{
			if(trk->ringIndex[id])
				continue;
		}
		else
		{
			if(!trk->ringIndex[id])
				continue;
		}
		event.id = evtype;
		event.parm.tid = sid;
		trk->postEvent(&event);
	}
end:
	if(evtype == TRUNK_START_RINGING)
		isRinging = true;
	advance();
	return true;
}

#ifdef	HAVE_TGI
bool Trunk::scrLibexec(void)
{
	tgicmd_t cmd;
	int argc = 0;
	const char *user = NULL;
	const char *gain = getKeyword("gain");
	const char *speed = getKeyword("speed");
	const char *pitch = getKeyword("pitch");
	Line *line = getLine();
	Name *scr = ScriptInterp::getName();
	char query[sizeof(cmd.cmd) - 160];
	char urlbuf[sizeof(cmd.cmd) - 160];
#ifdef	HAVE_SSTREAM
	ostringstream str;
	str.str() = "";
#else
	strstream str(cmd.cmd, sizeof(cmd.cmd));
#endif
	unsigned qlen = 0;
	char *oc;
	const char *qc, *opt, *tag;
	const char *member = getMember();
	char namebuf[64];

	if(!member)
		member="tgi";

	if(!stricmp(member, "play"))
		data.sleep.wakeup =  getSecTimeout(getValue(getSymbol(SYM_PLAYWAIT)));
	else
		data.sleep.wakeup = getTimeout("maxTime");
	data.sleep.rings = 0;
	data.sleep.loops = 1;
	data.sleep.save = NULL;

	cmd.seq = ++tgi.seq;
	snprintf(cmd.port, sizeof(cmd.port), "%s/%d", driver->getName(), id);
	cmd.mode = TGI_EXEC_NORMAL;
	cmd.cmd[0] = 0;
	query[0] = 0;

	opt = getValue("--");
	if(!strnicmp(opt, "~/", 2))
	{
		snprintf(namebuf, sizeof(namebuf), "%s", scr->name);
		oc = strchr(namebuf, ':');
		if(oc)
			*oc = 0;
		if(namebuf[0] == '~')
			user = namebuf + 1;
		else if(namebuf[0] == '#')
		{
			user = getSymbol(SYM_HOME);
			if(!*user)
				user = NULL;
		}
		opt += 2;
	}

	str << opt;
	while(NULL != (qc = getOption(NULL)) && qlen < sizeof(query))
	{
		if(*qc != '%')
			continue;

		if(!strnicmp(qc, "%lib.", 5))
			tag = qc + 5;
		else
			tag = qc + 1;

		if(qlen)
			query[qlen++] = *keyserver.getToken();

		qc = getSymbol(qc);
		if(!qc)			// ignore non-initialized...
			continue;

		urlbuf[0] = 0;
		urlEncode(qc, urlbuf, sizeof(urlbuf));
		snprintf(query + qlen, sizeof(query) - qlen, "%s=%s", tag, urlbuf);
		qlen = strlen(query);
	}

	while(argc < line->argc && qlen < sizeof(query))
	{
		opt = line->args[argc++];
		if(*opt != '=')
			continue;

		tag = opt + 1;
		opt = line->args[argc++];

		if(qlen)
			query[qlen++] = *keyserver.getToken();

		qc = getContent(opt);
		if(!qc)
			continue;

		urlbuf[0] = 0;
		urlEncode(qc, urlbuf, sizeof(urlbuf));
		snprintf(query + qlen, sizeof(query) - qlen, "%s=%s", tag, urlbuf);
		qlen = strlen(query);
	}

	if(user)
		str << " user=" << user;

	str << " query=" << query;

	if(dtmf.bin.data)
		str << " digits=" << dtmf.bin.data;

	qc = getSymbol(SYM_CALLER);
	if(qc)
		str << " clid=" << qc;

	qc = getSymbol(SYM_DIALED);
	if(qc)
		str << " dnid=" << qc;

	if(!data.sleep.wakeup)
		cmd.mode = TGI_EXEC_DETACH;

	if(!stricmp(member, "play"))
	{
		data.play.write = WRITE_MODE_NONE;
		data.play.voice = NULL;
		data.play.timeout = data.sleep.wakeup;
		data.play.repeat = 0;
		data.play.lock = false;
		data.play.name = data.play.list;
		data.play.term = 0;
		data.play.mode = PLAY_MODE_TEMP;
		data.play.limit = data.play.offset = 0;
		data.play.volume = atoi(getSymbol(SYM_VOLUME));
		data.play.extension = NULL;

		if(gain)
			data.play.gain = (float)strtod(gain, NULL);
		else
			data.play.gain = 0.0;

		if(!speed)
			speed = "normal";

		if(!stricmp(speed, "fast"))
			data.play.speed = SPEED_FAST;
		else if(!stricmp(speed, "slow"))
			data.play.speed = SPEED_SLOW;
		else
			data.play.speed = SPEED_NORMAL;

		if(pitch)
			data.play.pitch = (float)strtod(pitch, NULL);
		else
			data.play.pitch = 0.0;

		sprintf(data.play.list, "temp/.tmp.%d.%s",
			id, getSymbol(SYM_EXTENSION));
		str << " audio=" << data.play.name;
	}

#ifdef	HAVE_SSTREAM
	snprintf(cmd.cmd, sizeof(cmd.cmd), "%s", str.str().c_str());
#else
	str << ends;
#endif
	::write(tgipipe[1], &cmd, sizeof(cmd));

	if(!stricmp(member, "play"))
	{
		trunkStep(TRUNK_STEP_PLAYWAIT);
		return false;
	}

	if(!data.sleep.wakeup)
	{
		advance();
		return true;
	}

	trunkStep(TRUNK_STEP_SLEEP);
	return false;
}

#endif

bool Trunk::scrTone(void)
{
	const char *cp;

        data.tone.recall = false;
        data.tone.dialing = NULL;
        data.tone.tone = NULL;
        data.tone.freq1 = data.tone.freq2 = 0;
        data.tone.ampl1 = data.tone.ampl2 = 0;

	cp = getValue(NULL);
	if(cp)
	{
		data.tone.tone = getphTone(cp);
		if(!data.tone.tone)
		{
			error("no-tone");
			return true;
		}
	}
	else
	{
		cp = getKeyword("frequency");
		if(cp)
		{
			data.tone.freq1 = atoi(cp);
			cp = getKeyword("amplitude");
			if(cp)
				data.tone.ampl1 = atoi(cp);
		}
		else
		{
			cp = getKeyword("freq1");
			if(cp)
				data.tone.freq1 = atoi(cp);

			cp = getKeyword("ampl1");
			if(cp)
				data.tone.ampl1 = atoi(cp);

			cp = getKeyword("freq2");
			if(cp)
				data.tone.freq2 = atoi(cp);

			cp = getKeyword("ampl2");
			if(cp)
				data.tone.ampl2 = atoi(cp);
		}
	}

	cp = getKeyword("timeout");
	if(!cp)
		cp = getValue(NULL);
	if(cp)
		data.tone.wakeup = data.tone.duration = getSecTimeout(cp);
	else
	{
		data.tone.wakeup = data.tone.tone->getPlaytime();
		data.tone.duration = data.tone.tone->getDuration();
		if(!data.tone.wakeup)
			data.tone.wakeup = 1000;
	}

	cp = getKeyword("length");
	if(cp)
		data.tone.duration = getMSTimeout(cp);

	if(data.tone.duration > data.tone.wakeup)
		data.tone.duration = data.tone.wakeup;
		
	cp = getKeyword("count");
	if(!cp)
		cp = getValue("1");
	if(!data.tone.tone->getPlaytime())
	{
		if(atoi(cp) == 1)
			data.tone.duration = data.tone.wakeup = data.tone.tone->getDuration();
		else
			data.tone.duration = data.tone.wakeup = data.tone.wakeup * atoi(cp);
		cp = "1";
	}

	data.tone.loops = atoi(cp);
	trunkStep(TRUNK_STEP_TONE);
	return false;
}

bool Trunk::scrSleep(void)
{
	timeout_t timer = getTimeout("maxTime");

	if(!timer)
	{
		advance();
		return true;
	}

	data.sleep.wakeup = timer;
	data.sleep.loops = 1;
	data.sleep.rings = 0;
	data.sleep.save = NULL;
	trunkStep(TRUNK_STEP_SLEEP);
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
