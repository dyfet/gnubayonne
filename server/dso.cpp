// Copyright (C) 2000-2001 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "server.h"
#include "ivrconfig.h"

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

Server *Server::first = NULL;
Translator *Translator::first = NULL;
TGI *TGI::first = NULL;
Module *Module::modFirst = NULL;
Module *Module::sesFirst = NULL;
Module *Module::cmdFirst = NULL;
Module *Module::urlFirst = NULL;
Module *Module::reqFirst = NULL;
Module *Module::netFirst = NULL;
Module *Module::symFirst = NULL;
Module *Module::modImport = NULL;
Driver *Driver::drvFirst = NULL;
int Driver::drvIndex = 0;
Sync *Sync::first = NULL;
char Trunk::status[1024];

TTS::TTS()
{
	tts = this;
}

Driver::Driver()
{
	active = false;

	groups = NULL;
	tsid = getCount();	// prior drivers for our base

	drvNext = drvFirst;
	drvFirst = this;
	index = Driver::drvIndex++;

	portCount = downCount = idleCount = 0;
	extCount = trkCount = tieCount = 0;
	numCount = 0;

	extIndex = NULL;

	memset(spans, 0, sizeof(spans));
	memset(cards, 0, sizeof(cards));
	memset(stacards, 0, sizeof(cards));
}

bool Driver::isIdle(void)
{
	if(portCount - idleCount)
		return false;

	return true;
}

bool Driver::isDown(void)
{
	if(portCount - downCount)
		return false;

	return true;
}

bool Driver::spanEvent(unsigned span, TrunkEvent *evt)
{
	unsigned port;
	unsigned rtn = false;
	Trunk *trunk;

	if(!span)
		return false;

	for(port = 0; port < (unsigned)getTrunkCount(); ++port)
	{
		TrunkEvent ev = *evt;
		trunk = getTrunkPort(port);
		if(!trunk)
			continue;

		if(trunk->span != span)
			continue;

		rtn = true;
		trunk->postEvent(&ev);
	}
	return rtn;
}
	
aaImage *Driver::getImage(void)
{
	//return new aaImage((aaScript *)this);
	return new aaImage(aascript);
}

Trunk *Driver::getTrunkId(const char *id)
{
	Trunk *trk, *ret;
	const char *cp;

	if(!id)
		return NULL;

	cp = strchr(id, '-');
	if(!cp)
		return getTrunkPort(atoi(id));

	trk = ret = getTrunkPort(atoi(++cp));
	if(!trk)
		return NULL;

	trk->enterMutex();
	cp = trk->getSymbol(SYM_GID);
	if(!cp)
		cp = "";

	if(*id == '-')
		cp = strchr(cp, '-');
	if(!cp)
		cp = "";

	if(strcmp(cp, id))	
		ret = NULL;

	trk->leaveMutex();
	return ret;
}

Driver *Driver::getIndexedDriver(int idx)
{
	Driver *drv = Driver::drvFirst;
	while(drv)
	{
		if(drv->getDriverIndex() == idx)
			return drv;
		drv = drv->drvNext;
	}
	return NULL;
}

Trunk *Driver::getTrunk(const char *name, bool create, Driver *driver)
{
	Driver *drv = drvFirst;
	Trunk *trunk = NULL;
	char *cp, *trktype;
	char drvid[3] = {0,0,0};
	int port;
	unsigned long caps = 0;

	if(!name)
		return NULL;

	cp = strchr(name, '/');
	if(!cp)
	{
		cp = strchr(name, '-');
		if(cp)
		{
			cp = strrchr(name, '-');
			if(cp)
			{
				strncpy(drvid, ++cp, 2);
				slog(Slog::levelDebug) << drvid << endl;
				drv = getIndexedDriver(atoi(drvid));
			}
		}
		else if(driver)
			drv = driver;
		else
		{
			slog(Slog::levelWarning) << "getTrunk: fall-back to default driver" << endl;
			drv = drvFirst;
		}

		if(!drv)
			return NULL;

		if(create)
			return drv->getOutboundTrunk(atoi(name));
		else
			return drv->getTrunkId(name);
	}

	//*cp = 0;
	++cp;

	if(!strnicmp(name, "port", 4) && isdigit(*cp))
	{
		port = atoi(cp);
		while(drv)
		{
			if(port >= drv->tsid)
				if(port < drv->tsid + drv->getTrunkCount())
					break;
			drv = drv->drvNext;
		}
		if(!drv)
			return NULL;
		return drv->getTrunkId(cp);
	}		

	if(!strnicmp(name, "ext", 3))
		caps = TRUNK_CAP_STATION;
	else if(!strnicmp(name, "tie", 3))
		caps = TRUNK_CAP_TIE;
	else if(!strnicmp(name, "pstn", 4) || !strnicmp(name, "trunk", 5) || !strnicmp(name, "trk", 3))
		caps = TRUNK_CAP_TRUNK;

	if(*cp == '*')
		port = -1;
	else
		port = atoi(cp);

	while(drv)
	{
		switch(caps)
		{
		case 0:
			if(!strncasecmp(drv->getName(), name, strlen(drv->getName())))
			{
				if(port < 0)
				{
					for(port = 0; port < drv->getTrunkCount(); port++)
					{
						if(create)
							trunk = drv->getOutboundTrunk(atoi(cp));
						else
							trunk = drv->getTrunkId(cp);

						if(trunk)
							return trunk;
					}
				}
				else if(create)
					return drv->getOutboundTrunk(atoi(cp));
				else
					return drv->getTrunkId(cp);
			}
			break;
		case TRUNK_CAP_STATION:
			break;
		case TRUNK_CAP_TIE:
			trunk = drv->getTieNumber(cp);
			if(trunk)
				return trunk;
			break;
		case TRUNK_CAP_TRUNK:
			slog(Slog::levelDebug) << "gettrknumber" << endl;
			trunk = drv->getTrkNumber(cp);
			if(trunk)
				return trunk;
			break;
		}
		drv = drv->drvNext;
	}
	return NULL;
}

void Driver::secTick(void)
{
	unsigned id, max = getTrunkCount();
	Trunk *trunk;
	TrunkEvent event;
	time_t now;

	time(&now);

	for(id = 0; id < max; ++ id)
	{
		trunk = getTrunkPort(id);
		if(!trunk)
			continue;

		if(!trunk->exittimer && !trunk->synctimer)
			continue;

		if(now >= trunk->exittimer)
		{
			event.id = TRUNK_TIMER_EXIT;
			trunk->postEvent(&event);
			trunk->exittimer = 0;
		}

		if(now >= trunk->synctimer)
		{
			event.id = TRUNK_TIMER_SYNC;
			trunk->postEvent(&event);
			trunk->synctimer = 0;
		}
	}
}

void Driver::setTrunkGroup(int id, int card, int span)
{
	if(id < 0 || id > getTrunkCount())
		slog(Slog::levelError) << "server: setTrunkGroup; invalid" << endl;

	TrunkGroup *grp = getTrunkGroup(id);
	if(grp != getGroup(NULL))
		return;

	if(!grp)
		return;

	if(spans[span])
		grp = spans[span];
	else if(cards[card])
		grp = cards[card];

	groups[id] = grp;
}

int Driver::getTrunkMember(TrunkGroup *group, unsigned member)
{
	int id, count = getTrunkCount();
	Trunk *trunk;

	for(id = 0; id < count; ++id)
	{
		if(groups[id] != group)
			continue;

		trunk = getTrunkPort(id);
		if(trunk->getMemberId() == member)
			return id;
	}
	return -1;
}

unsigned Driver::getCount(void)
{
	unsigned count = 0;
	Driver *drv = drvFirst;
	while(drv)
	{
		count += drv->getTrunkCount();
		drv = drv->drvNext;
	}
	return count;
}

void Driver::getStatus(char *buffer)
{
	static bool initial = false;
	unsigned count = 0;
	Driver *drv = Driver::drvFirst;
	if(!initial)
	{
		memset(Trunk::status, ' ', sizeof(Trunk::status)); 
		initial = true;
	}
	memcpy(buffer, Trunk::status, getCount());
}

Driver *getDriver(const char *name)
{
	Driver *drv = Driver::drvFirst;
	while(drv)
	{
		if(!stricmp(drv->getName(), name))
			break;
		drv = drv->drvNext;
	}
	if(drv)
		return drv;
}

Debug::Debug() :
Mutex()
{
	if(debug)
		throw this;

	debug = this;
}

Monitor::Monitor() :
Mutex()
{
	if(monitor)
		throw this;

	monitor = this;
}

Translator::Translator(const char *conf) :
Keydata(conf)
{
	char keypath[33];

	next = first;
	first = this;

	strcpy(keypath, conf);
	*keypath = '~';
	load(keypath);
}

char *Translator::getPlayBuffer(Trunk *trunk)
{
	char *pbuf;
	
	pbuf = trunk->data.play.list;
	trunk->data.play.name = pbuf;
	trunk->data.play.limit = trunk->data.play.offset = 0;
	*pbuf = 0;
	return pbuf;
}

Translator *getTranslator(const char *name)
{
	Translator *trans = Translator::first;

	while(trans)
	{
		if(!stricmp(name, trans->getName()))
			return trans;
		trans = trans->next;
	}
	return NULL;
}

Sync::Sync()
{
	next = first;
	first = this;
	time(&runtime);
}

void Sync::check(void)
{
	time_t now;
	struct tm *dt, tbuf;
	Sync *sync = Sync::first;

	time(&now);
	dt = localtime_r(&now, &tbuf);

	while(sync)
	{
                if(((time_t)sync->runtime +60 * (time_t)sync->getInterval()) < now)
                {
                        time(&sync->runtime);
                        if(sync->isScheduled())
                        {	
				slog(Slog::levelInfo) << "sync: "<< sync->getSyncName() << " updated" << endl;
                                sync->schedule();
                        }
                }
                sync = sync->next;
        }
}

Server::Server(int pri) :
#ifdef	COMMON_OST_NAMESPACE
Thread(pri, keythreads.getStack())
#else
Thread(NULL, pri, keythreads.getStack())
#endif
{
	next = first;
	first = this;
}

void startServers(void)
{
	Server *server = Server::first;

	while(server)
	{
		server->start();
		server = server->next;
	}
}

void stopServers(void)
{
	Server *server = Server::first;

	while(server)
	{
		server->stop();
		server = server->next;
	}
}

TGI::TGI()
{
	next = first;
	first = this;
}

TGI *getInterp(char *cmd)
{
	TGI *tgi = TGI::first;
	char *ext;
	char buffer[512];
	strcpy(buffer, cmd);
	cmd = strtok_r(buffer, " \t\n", &ext);
	ext = strrchr(cmd, '.');

	if(!ext)
		ext = cmd;
	
	while(tgi)
	{
		if(tgi->getExtension(ext))
			return tgi;
		tgi = tgi->next;
	}
	return NULL;
}

void getInterp(char *cmd, char **args)
{
	TGI *tgi = TGI::first;

	while(tgi)
	{
		tgi->script(cmd, args);
		tgi = tgi->next;
	}
}

Module::Module()
{
	modNext = modFirst;
	modFirst = this;
	prior = NULL;
}

void Module::addSymbols(void)
{
	symNext = symFirst;
	symFirst = this;
}

void Module::addSession(void)
{
	sesNext = sesFirst;
	sesFirst = this;
}

void Module::addNetwork(void)
{
	netNext = netFirst;
	netFirst = this;
}

void Module::addRequest(void)
{
	reqNext = reqFirst;
	reqFirst = this;
}

void Module::setThread(Trunk *trunk, Service *svc)
{
	if(trunk->thread)
		trunk->stopServices();
	trunk->thread = svc;
}

void Module::addCommand(void)
{
	cmdNext = cmdFirst;
	cmdFirst = this;
}

void Module::addPrompts(void)
{
	urlNext = urlFirst;
	urlFirst = this;
}

void detachModules(Trunk *trunk)
{
	Module *mod = Module::sesFirst;

	while(mod)
	{
		mod->detach(trunk);
		mod = mod->sesNext;
	}
}

void attachModules(Trunk *trunk)
{

	Module *mod = Module::sesFirst;

	while(mod)
	{
		mod->attach(trunk);
		mod = mod->sesNext;
	}
}

Module *getModule(modtype_t mtype, const char *name)
{
	Module *mod = Module::modFirst;

	while(mod)
	{
		if(mod->getType() == mtype || mtype == MODULE_ANY)
		{
			if(!name)
				return mod;

			if(!stricmp(name, mod->getName()))
				return mod;
		}
		mod = mod->modNext;
	}
	return NULL;
}

#ifdef  HAVE_EXECINFO_H

#include <execinfo.h>

void    Debug::stackTrace(int signo)
{
        const int maxTrace = 1000;
        void* buffer[maxTrace];
        int nTrace = backtrace ( buffer, maxTrace );
        char** trace = backtrace_symbols ( buffer, nTrace );

	if(signo != 2)
	{
        	slog(Slog::levelDebug) << "trace: pid=" << pthread_self()
               	 << " reason=" << signo << endl;


        	if ( trace ) {
               		for ( int i = 0; i < nTrace; ++i ) {
                		slog(Slog::levelDebug) << "trace(" << i << "): "
                       			<< trace[i] << endl;
                	}
        	}
	}
        // free memory
        free( trace );
}

#else
void    Debug::stackTrace(int signo) {}
#endif


Debug *debug = NULL;
Monitor *monitor = NULL;
Driver *driver = NULL;
TTS *tts = NULL;

#ifdef	CCXX_NAMESPACES
};
#endif
