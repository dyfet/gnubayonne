// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "acd.h"

ACDModule acd;

ACDModule::ACDModule() :
Module(), Keydata("/bayonne/acd")
{
	static KEYDEF keys[] = {
	{NULL, NULL}};

	Load(keys);

	aascript->addModule(this);
}

char *ACDModule::Dispatch(Trunk *trunk)
{
	const char *member = trunk->getMember();
	return NULL;
}

void ACDModule::Attach(Trunk *trunk)
{
	char gname[16];
	char c;

	// reference to group queue items
	for(c = '0'; c < '9'; ++c)
	{
		sprintf(gname, "%s%c", "acd.group", c);
		trunk->setPointer(gname, NULL);
	}
	trunk->setSymbol("acd.groups", 1);
	trunk->setSymbol("acd.groups", "0");

	// acd transfer tracker for disconnect
	trunk->setPointer("acd.pointer", NULL);
}

void ACDModule::Detach(Trunk *trunk)
{
}
	

