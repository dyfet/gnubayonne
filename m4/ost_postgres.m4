dnl Copyright (C) 1999 Open Source Telecom Corporation.
dnl  
dnl This program is free software; you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 2 of the License, or
dnl (at your option) any later version.
dnl 
dnl This program is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl 
dnl You should have received a copy of the GNU General Public License
dnl along with this program; if not, write to the Free Software 
dnl Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
dnl 
dnl As a special exception to the GNU General Public License, if you 
dnl distribute this file as part of a program that contains a configuration 
dnl script generated by Autoconf, you may include it under the same 
dnl distribution terms that you use for the rest of that program.

AC_DEFUN(OST_POSTGRES,[
	POSTGRES_FLAGS=""
	ost_cv_postgres_database=false
	AC_LANG_SAVE
	AC_LANG_CPLUSPLUS
	AC_CHECK_HEADER(libpq++.h,[
		AC_DEFINE(HAVE_POSTGRES, [1], [has postgresql c++ libs])
		ost_cv_postgres_database=true
	],[
		AC_CHECK_HEADER(pgsql/libpq++.h,[
			POSTGRES_FLAGS='-I$(includedir)/pgsql'
			AC_DEFINE(HAVE_PGSQL_POSTGRES, [1], [alternate libpq++ install])
			ost_cv_postgres_database=true
		])
	])
	AC_SUBST(POSTGRES_FLAGS)
	AC_LANG_RESTORE
])


