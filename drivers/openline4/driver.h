// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <ivrconfig.h>
#include <server.h>
#include <vpbapi.h>
#include <pthread.h>

#define CID_BUF_SZ 8000*4  // DR - up to 10 seconds of CID buffer

#ifdef	VPB_RING_STATION_ONCE
#define	vpb_ring_station_sync(a, b) vpb_ring_station_async(a, b, 0)
#endif

#ifdef	CCXX_NAMESPACES
namespace ost {
#endif

class VPBTrunk;
class VPBDriver;

extern VPBDriver vpbivr;

typedef bool (VPBTrunk::*trunkhandler_t)(TrunkEvent *event);

class VPBTrunk: private Trunk
{
protected:
	friend class VPBDriver;
	friend class VPBSend;
	friend class VPBCid;
	friend class VPBJoin;
	friend class VPBTone;
	friend class VPBPlay;
	friend class VPBDigit;
	friend class VPBListen;

	static unsigned _trkcount;

	VPBListen *listen;
	//bool *ringIndex;
	char name[16];
	int _card, _port, _bank;
	unsigned _trk;
	bool inTimer;
	bool stopped;
	//bool isStation, isRinging;
	float	inpgain, outgain;
	void *timer;
	int handle;
	unsigned bridge;
	bool answered;
	int x;
	time_t lastring;
	trunkhandler_t handler;
	VPBJoin *sjoined;
	short cidbuf[CID_BUF_SZ]; // DR - storage for CID signal
	char cidnbr[VPB_MAX_STR]; // DS - storage for cid parsing
	int cidlen;

	VPBTrunk(int card, int ts, bool sta = false);
	~VPBTrunk();

	void stopThreads(void);
	void getName(char *buffer);
	unsigned long getIdleTime(void);
	unsigned getPickupTimer(void);
	unsigned long getCapabilities(void);
	void clearRinging(void);

	bool setListen(bool start);

	bool scrRTP(void);
	bool scrJoin(void);
	bool scrWait(void);
	bool scrRing(void);
	bool scrPickup(void);
	bool scrAnswer(void);
	bool scrDial(void);

	bool endDialer(TrunkEvent *event);
	bool digitDialer(TrunkEvent *event);	
	bool stepHandler(TrunkEvent *event);
	bool busyHandler(TrunkEvent *event);
	bool idleHandler(TrunkEvent *event);
	bool seizeHandler(TrunkEvent *event);
	bool ringIntercom(TrunkEvent *event);
	bool ringStation(TrunkEvent *event);
	bool ringHandler(TrunkEvent *event);
	bool waitHandler(TrunkEvent *event);
	bool loadHandler(TrunkEvent *event);
	bool dialHandler(TrunkEvent *event);
	bool toneHandler(TrunkEvent *event);
	bool playHandler(TrunkEvent *event);
	bool playStopHandler(TrunkEvent *event);
	bool playwaitHandler(TrunkEvent *event);
	bool recordHandler(TrunkEvent *event);
	bool listenHandler(TrunkEvent *event);
	bool sleepHandler(TrunkEvent *event);
	bool threadHandler(TrunkEvent *event);
	bool hangupHandler(TrunkEvent *event);
	bool answerHandler(TrunkEvent *event);
	bool pickupHandler(TrunkEvent *event);
	bool collectHandler(TrunkEvent *event);
	bool flashonHandler(TrunkEvent *event);
	bool flashoffHandler(TrunkEvent *event);
	bool joinHandler(TrunkEvent *event);
	bool softHandler(TrunkEvent *event);
	bool rtpHandler(TrunkEvent *event);

	void initSyms(void);
	void setTimer(timeout_t timeout = 0);
	void endTimer(void);
	void setOffhook(void);

	void disjoin(trunkevent_t reason);

	bool postEvent(TrunkEvent *event);
	void exit(void);
	void trunkStep(trunkstep_t step);

	void exitThread(void);
public:
	Driver *getDriver(void)
		{return (Driver*)&vpbivr;};
};

class VPBCid : private Service
{
private:
	friend class VPBTrunk;

	VPBTrunk *trk;
	int handle;
	bool reset;

	void run(void);

	VPBCid(VPBTrunk *trunk, int h);
	~VPBCid();
};

class VPBSend : private Service
{
private:
	friend class VPBTrunk;

        VPBTrunk *trk;
        int handle;
        bool reset;

        void run(void);

        VPBSend(VPBTrunk *trunk, int h);
        ~VPBSend();
};

class VPBDigit : private Service, private TimerPort
{
private:
	friend class VPBTrunk;

	VPBTrunk *trk;
	int handle;
	bool reset;

	void run(void);

	VPBDigit(VPBTrunk *trunk, int h);
	~VPBDigit();
};

class VPBTone : private Service, private TimerPort
{
private:
	friend class VPBTrunk;

	VPBTrunk *trk;
	int handle;
	bool reset;

	void run(void);

	VPBTone(VPBTrunk *trunk, int h);
	~VPBTone();
};

class VPBRecord: private AudioFile, public Service
{
private:
	bool reset;
	size_t bufsize, samples;
	int handle;
	VPBTrunk *vpbtr; // DR

	void initial(void);
	void run(void);

public:
	VPBRecord(VPBTrunk *trunk, int h);
	~VPBRecord();
};

class VPBListen: public Service
{
private:
	bool reset;
	size_t bufsize, samples;
	int handle;
	VPBTrunk *vpbtr; // DR
	unsigned level;
	float gain;

	void initial(void);
	void run(void);

public:
	VPBListen(VPBTrunk *trunk, int h, float g = 0.0, bool start = true);
	~VPBListen();
	void setListen(bool start);
	bool getListen(void);
};

class VPBPlay : private URLAudio, public Service
{
private:
	bool reset;
	size_t bufsize, samples;
	int handle, id;
	VPBTrunk *vpbtr; // DR

	void initial(void);
	void run(void);
	char *getContinuation(void);

public:
	VPBPlay(VPBTrunk *trunk, int h);
	~VPBPlay();
};

class VPBJoin : public Service
{
private:
	void initial(void);
	void run(void);
	bool reset;
	Trunk *source;
	Trunk *target;
	int shandle;
public:
	VPBJoin(VPBTrunk *source, Trunk *target);
	~VPBJoin();
};

class VPBConfig : public Keydata
{
public:
	timeout_t	stepTimer, syncTimer;

	VPBConfig();

	unsigned getOffset(void)
		{return strtol(getLast("first"), NULL, 16);};

	unsigned getCards(void)
		{return atoi(getLast("cards"));};

	inline unsigned getHookTimer(void)
		{return atoi(getLast("hooktime"));};

	size_t getStack(void);

	const char *getFirmware(void)
		{return getLast("firmware");};

	unsigned getDelay(void)
		{return atoi(getLast("delay"));};

	int getModel(void);
	unsigned getCardSize(void);

};

class VPBDriver : public Driver, public VPBConfig, public Thread
{
private:
	VPBTrunk **ports, **trunks;
	bool *bridges;
	unsigned port_count, bridge_count;

public:
	VPBDriver();
	~VPBDriver();

	void run(void);

	int start(void);
	void stop(void);

	unsigned getCaps(void);
	unsigned getBridge(int card);
	void freeBridge(int card, unsigned bridge);
	Trunk *getTrkNumber(const char *trk);

	unsigned getTrunkCount(void)
		{return port_count;};

	Trunk *getTrunkPort(int id);

	char *getName(void)
		{return "VPB";};
};

#ifdef	CCXX_NAMESPACES
};
#endif

