// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

VPBRTP::VPBRTP(VPBTrunk *trunk, int h, InetAddress &bind, tpport_t port, audioencoding_t codec ) :
Service((Trunk *)trunk, keythreads.priAudio()), TimerPort(),
RTPSocket(bind, port, keythreads.priRTP())
{
	handle = h;
	reset = false;
	stopped = false;
	payload = RTP_PAYLOAD_PCMU;
	encoding = codec;
	
	switch(encoding)
	{
	case MULAW_AUDIO_ENCODING:
		payload = RTP_PAYLOAD_PCMU;
		frame = 160;
		break;
	case ALAW_AUDIO_ENCODING:
		payload = RTP_PAYLOAD_PCMA;
		frame = 160;
		break;
	case PCM16_AUDIO_ENCODING:
		payload = RTP_PAYLOAD_L16_MONO;
		frame = 320;
		break;
	case G723_3BIT_ENCODING:
		payload = RTP_PAYLOAD_G723;
		frame = 60;
		break;
	}
}

VPBRTP::~VPBRTP()
{
	char buffer[12];
	struct stat ino;
	int trim;

	endService();
	endQueue();
	endSocket();
	if(reset)
	{
		vpb_play_terminate(handle);
		vpb_record_terminate(handle);
		dspReset();
	}
}

void VPBRTP::Initial(void)
{
	int codec = VPB_MULAW;

	switch(encoding)
	{
	case G723_3BIT_ENCODING:
		codec = VPB_OKIADPCM24;
		break;
	case ALAW_AUDIO_ENCODING:
		codec = VPB_ALAW;
		break;
	case PCM16_AUDIO_ENCODING:
		codec = VPB_LINEAR;
		break;
	}
	reset = true;
	vpb_record_buf_start(handle, codec);
	vpb_play_buf_start(handle, codec);
}

void VPBRTP::Run(void)
{
	InetHostAddress addr(data->rtp.addr);
	unsigned char buffer[frame];
	unsigned long sendstamp = 0, recvstamp = 0;
	
	Connect(addr, data->rtp.port);
	for(;;)
	{
		if(getPacket(recvstamp, buffer, frame))
			vpb_play_buf_sync(handle, (char *)buffer, frame);
		recvstamp += 160;
		TimerPort::incTimer(20);
		vpb_record_buf_sync(handle, (char *)buffer, frame);
		putPacket(sendstamp, payload, buffer, frame);
		sendstamp += 160;
		ccxx_sleep(TimerPort::getTimer());
		if(recvstamp > 160)	// jitter for receiver
		{
			if(getPacket(recvstamp, buffer, frame))
				vpb_play_buf_sync(handle, (char *)buffer, frame);
			recvstamp += 160;
		}
	}

	vpb_record_buf_finish(handle);
	vpb_play_buf_finish(handle);
	reset = false;
	Success();
}
	
bool VPBTrunk::rtpHandler(TrunkEvent *event)
{
	unsigned short mask;
	InetAddress bind("*");

	switch(event->id)
	{
	case TRUNK_DTMF_KEYUP:
		mask = (1 << event->parm.dtmf.digit);
		if(!(mask & data.rtp.term))
			return false;
	case TRUNK_SERVICE_SUCCESS:
		TrunkSignal(TRUNK_SIGNAL_STEP);
		handler = &VPBTrunk::stepHandler;
		return true;
	case TRUNK_SERVICE_FAILURE:
		setSymbol(SYM_ERROR, "rtp-failed");
		TrunkSignal(TRUNK_SIGNAL_ERROR);
		handler = &VPBTrunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		debug->DebugState(this, "rtp");
		flags.dsp = DSP_MODE_RTP;
		status[tsid] = 'r';
		if(!flags.offhook)
		{
			flags.offhook = true;
			vpb_sethook_async(handle, VPB_OFFHOOK);
			setTimer(getPickupTimer());
			return true;
		}
	case TRUNK_TIMER_EXPIRED:
		if(data.record.term)
			setDTMFDetect(true);
		else
			setDTMFDetect();
		thread = new VPBRTP(this, handle, bind, data.rtp.bind, data.rtp.codec);
		thread->Start();
		return true;
	}
	return false;
}

