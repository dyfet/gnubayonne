// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

bool VPBTrunk::flashoffHandler(TrunkEvent *event)
{
	switch(event->id)
	{
	case TRUNK_DTMF_KEYUP:
	case TRUNK_STOP_DISCONNECT:
		return true;
	case TRUNK_CPA_DIALTONE:
		endTimer();
		setSymbol(SYM_TONE, "dial");
		if(data.dialxfer.digit)
		{
			endTimer();
			handler = &VPBTrunk::dialHandler;
			return true;
		}
		if(trunkSignal(TRUNK_SIGNAL_TONE))
		{
			endTimer();
			handler = &VPBTrunk::stepHandler;
			return true;
		}
	case TRUNK_TIMER_EXPIRED:
		if(data.dialxfer.digit)
		{
			handler = &VPBTrunk::dialHandler;
			return true;
		}
		trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &VPBTrunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		enterState("flashoff");
		setSymbol(SYM_TONE, "none");
		flags.offhook = true;
		vpb_sethook_async(handle, VPB_OFFHOOK);
		if(data.dialxfer.offhook < getPickupTimer())
			data.dialxfer.offhook = getPickupTimer();
		setTimer(data.dialxfer.offhook);
		return true;
	}
	return false;
}

bool VPBTrunk::flashonHandler(TrunkEvent *event)
{
	switch(event->id)
	{
	case TRUNK_DTMF_KEYUP:
	case TRUNK_STOP_DISCONNECT:
		return true;
	case TRUNK_TIMER_EXPIRED:
		handler = &VPBTrunk::flashoffHandler;
		return true;
	case TRUNK_ENTER_STATE:
		enterState("flashon");
		setDTMFDetect(false);
		flags.dsp = DSP_MODE_VOICE;
		status[tsid] = 'f';
		if(!flags.offhook)
		{
			handler = &VPBTrunk::flashoffHandler;
			return true;
		}
		flags.offhook = false;
		vpb_sethook_async(handle, VPB_ONHOOK);
		setTimer(data.dialxfer.onhook);
		return true;
	}
	return false;
}		

#ifdef	CCXX_NAMESPACES
};
#endif
