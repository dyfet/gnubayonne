// Copyright (C) 2000 Open Source Telecom Corporation.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	XML_SCRIPTS

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

bool DummyTrunk::loadHandler(TrunkEvent *event)
{

	switch(event->id)
	{
	case TRUNK_STOP_STATE:
		stopServices();
		endTimer();
		handler = &DummyTrunk::stepHandler;
		return true;
	case TRUNK_SERVICE_SUCCESS:
		flags.reset = true;
		if(!data.load.section[0])
			trunkSignal(TRUNK_SIGNAL_STEP);
		else
		{
			if(data.load.gosub)
				push();
			if(!redirect(data.load.section))
			{
				if(data.load.gosub)
					pull();
				setSymbol(SYM_ERROR, "xml-redirect-failed");
				trunkSignal(TRUNK_SIGNAL_ERROR);
			}
		}
		endTimer();
		handler = &DummyTrunk::stepHandler;
		return true;
	case TRUNK_SERVICE_FAILURE:
		flags.reset = true;
		endTimer();
		setSymbol(SYM_ERROR, "xml-load-failed");
		if(data.load.fail)
			redirect(data.load.fail);
		else
			trunkSignal(TRUNK_SIGNAL_ERROR);
		handler = &DummyTrunk::stepHandler;
		return true;
	case TRUNK_TIMER_EXPIRED:
	case TRUNK_NULL_EVENT:
		stopServices();
		endTimer();
		setSymbol(SYM_ERROR, "xml-load-timeout");
		if(data.load.fail)
			redirect(data.load.fail);
		else if(!trunkSignal(TRUNK_SIGNAL_TIMEOUT))
			trunkSignal(TRUNK_SIGNAL_ERROR);
		handler = &DummyTrunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		enterState("load");
		status[tsid] = 'x';
		setTimer(data.load.timeout);
		Trunk::setDTMFDetect();
		thread = new XMLService(this);
		thread->start();
		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif

#endif
