// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

bool DummyTrunk::ringHandler(TrunkEvent *event)
{
	char *args[2];
	char **start = NULL;
	TrunkGroup *grp = NULL;

	switch(event->id)
	{
	case TRUNK_ENTER_STATE:
		enterState("ring");
		status[tsid] = '!';
		flags.dsp = DSP_MODE_CALLERID;
		return true;
	case TRUNK_START_SCRIPT:
		return false;
	case TRUNK_RING_START:
		flags.dsp = DSP_MODE_INACTIVE;
		return idleHandler(event);
	case TRUNK_RINGING_ON:
		++rings;
		endTimer();
		return true;
	case TRUNK_NULL_EVENT:
		++rings;
		endTimer();
	case TRUNK_RINGING_OFF:
		if(rings > 1)
			flags.dsp = DSP_MODE_INACTIVE;
		if(rings < group->getAnswer())
		{
			setTimer(group->getRingTime() * 1000);
			return true;
		}
		if(!grp)
			grp = group;
		start = getInitial(args);
		if(attach(*start))
		{
			setList(++start);
			handler = &DummyTrunk::stepHandler;
			return true;
		}
		rings = 0;
		slog(Slog::levelError) << name << ": cannot answer call" << endl;	
		return true;
	case TRUNK_STOP_DISCONNECT:
	case TRUNK_CPA_DIALTONE:
	case TRUNK_TIMER_EXPIRED:
		flags.dsp = DSP_MODE_INACTIVE;
		handler = &DummyTrunk::idleHandler;
		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
