// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

bool DummyTrunk::stepHandler(TrunkEvent *event)
{
	Trunk *alt = ctx;

	switch(event->id)
	{
	case TRUNK_MAKE_STEP:
		if(idleHangup())
			return true;
		switch(event->parm.step)
		{
		case TRUNK_STEP_THREAD:
			handler = &DummyTrunk::threadHandler;
			return true;
		case TRUNK_STEP_SLEEP:
			handler = &DummyTrunk::sleepHandler;
			return true;
		case TRUNK_STEP_HANGUP:
			handler = &DummyTrunk::hangupHandler;
			return true;
		case TRUNK_STEP_ANSWER:
			trunkSignal(TRUNK_SIGNAL_STEP);
			handler = &DummyTrunk::stepHandler;
			return true;
		case TRUNK_STEP_COLLECT:
			handler = &DummyTrunk::collectHandler;
			return true;
		case TRUNK_STEP_FLASH:
			trunkSignal(TRUNK_SIGNAL_STEP);
			handler = &DummyTrunk::stepHandler;
			return true;
		case TRUNK_STEP_PLAY:
			handler = &DummyTrunk::playHandler;
			return true;
//		case TRUNK_STEP_TONE:
//			handler = &DummyTrunk::toneHandler;
//			return true;
		case TRUNK_STEP_RECORD:
			handler = &DummyTrunk::recordHandler;
			return true;
//		case TRUNK_STEP_DIALXFER:
//			handler = &DummyTrunk::dialHandler;
//			return true;
		case TRUNK_STEP_PLAYWAIT:
			handler = &DummyTrunk::playwaitHandler;
			return true;
		}
		return false;
	case TRUNK_SERVICE_SUCCESS:
	case TRUNK_SERVICE_FAILURE:
		endTimer();
		setTimer(keythreads.getResetDelay());
		return true;
	case TRUNK_STOP_STATE:
	case TRUNK_TIMER_EXPIRED:
		flags.reset = false;
		stopServices();
	case TRUNK_ENTER_STATE:
		flags.offhook = true;
		if(flags.trunk == TRUNK_MODE_OUTGOING)
			status[tsid] = 'o';
		else
			status[tsid] = 'i';

                if(tgi.fd > -1 && digits)
                {
                        snprintf(buffer, sizeof(buffer), "%s\n", dtmf.bin.data);
                        writeShell(buffer);
                        digits = 0;
                }
                setDTMFDetect();
		endTimer();

		// if a dsp reset has occured, then we add a delay
		// for the dsp to settle before stepping
		if(flags.reset || thread)
		{
			enterState("reset");
			if(thread)
				setTimer(thread->stop());
			else
				setTimer(keythreads.getResetDelay());
			return true;
		}

                if(tgi.fd > -1)
		{
                        enterState("shell");
                        return true;
		}

		debug->debugStep(this, getLine());
		monitor->monitorStep(this, getLine());

		// step and delay if still in same handler

		if(alt != this)
			alt->enterMutex();
		scriptStep();
		if(alt != this)
			alt->leaveMutex();

		if(handler == &DummyTrunk::stepHandler)
			setTimer(keythreads.getStepDelay());

		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
