// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <ivrconfig.h>
#include <server.h>
#include <cc++/audio.h>

extern "C" {
#include <termios.h>
#include <fcntl.h>
};

#ifdef	HAVE_AIO_H
#include <aio.h>
#endif

#ifdef  CCXX_NAMESPACES
namespace ost {
#endif

class DummyTrunk;
class DummyDriver;

extern DummyDriver dummyivr;

typedef bool (DummyTrunk::*trunkhandler_t)(TrunkEvent *event);

class DummyTrunk : public Trunk, private TimerPort
{
private:
	friend class DummyDriver;
	friend class DummyPlay;
	friend class DummyRecord;
	int dev;
        time_t lastring;
        trunkhandler_t handler;
	char name[16];

	DummyTrunk(int ts);
	~DummyTrunk();

	void initSyms(void);
	void getName(char *buffer);
	unsigned long getIdleTime(void);
	bool threadHandler(TrunkEvent *event);
	bool toneHandler(TrunkEvent *event);
        bool stepHandler(TrunkEvent *event);
        bool busyHandler(TrunkEvent *event);
        bool idleHandler(TrunkEvent *event);
        bool seizeHandler(TrunkEvent *event);
        bool ringHandler(TrunkEvent *event);
        bool waitHandler(TrunkEvent *event);
        bool dialHandler(TrunkEvent *event);
        bool playHandler(TrunkEvent *event);
        bool playwaitHandler(TrunkEvent *event);
        bool recordHandler(TrunkEvent *event);
        bool sleepHandler(TrunkEvent *event);
        bool hangupHandler(TrunkEvent *event);
        bool answerHandler(TrunkEvent *event);
        bool collectHandler(TrunkEvent *event);
        bool flashonHandler(TrunkEvent *event);
        bool flashoffHandler(TrunkEvent *event);

        bool postEvent(TrunkEvent *event);
        void setTimer(timeout_t timeout = 0)
		{TimerPort::setTimer(timeout);};
        bool exit(void);
        void trunkStep(trunkstep_t step);

protected:
	const char *getDefaultEncoding(void)
		{return "pcm";};

public:
        inline int getDevice(void)
                {return dev;};
};

class DummyRecord : private AudioFile, public Service
{
private:
	int dev;
	bool reset;
	size_t bufsize, samples;

public:
	DummyRecord(DummyTrunk *trunk);
	~DummyRecord();
	void initial(void);
	void run(void);
};

class DummyPlay : private URLAudio, public Service
{
private:
        int dev, rate, id;
        bool reset;
        size_t bufsize, samples;
	char *getContinuation(void);

#ifdef	HAVE_AIO_H
	struct aiocb iob;
#endif

public:
        DummyPlay(DummyTrunk *trunk);
        ~DummyPlay();
        void initial(void);
        void run(void);
};

class DummyConfig : public Keydata
{
public:
	DummyConfig();

        inline int getDeviceCount(void)
                {return atoi(getLast("count"));};

        inline int getBuffers(void)
                {return atoi(getLast("buffers"));};

	const char *getDevice(int ts);

        size_t getStack(void);

};

class DummyDriver : public Driver, public DummyConfig, public Thread
{
private:
	DummyTrunk **ports;
	int port_count;
	termios t;

public:
	DummyDriver();
	~DummyDriver();

	int start(void);
	void stop(void);

	void run(void);

	unsigned getCaps(void)
		{return capSpeed;};

	unsigned getTrunkCount(void)
		{return 1;};

	char *getName(void)
		{return "Dummy";};

	Trunk *getTrunkPort(int id);
	aaScript *getScript(void);
};

#ifdef	CCXX_NAMESPACES
};
#endif
