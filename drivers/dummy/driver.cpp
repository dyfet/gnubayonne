// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef  CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

DummyConfig::DummyConfig() :
Keydata("/bayonne/soundcard")
{
	static Keydata::Define defkeys[] = {
	{"device", "/dev/dsp"},
	{"count", "1"},
	{"buffers", "8"},
	{"stack", "64"},
	{NULL, NULL}};

	if(isFHS())
		load("/drivers/soundcard");

	load(defkeys);
}

size_t DummyConfig::getStack(void)
{
        const char *cp = getLast("stack");

        if(!cp)
                return keythreads.getStack();

        return atoi(cp) * 1024;
}

const char *DummyConfig::getDevice(int ts)
{
	char devname[16];
	const char *cp;

	snprintf(devname, 16, "device%d", ts);
	cp = getLast(devname);
	if(!cp)
		cp = getLast("device");
	return cp;
}

DummyDriver::DummyDriver() :
Driver(), DummyConfig(), Thread(keythreads.priService(), dummyivr.getStack())
{
	port_count = getDeviceCount();

	ports = new DummyTrunk *[port_count];
	groups = new TrunkGroup *[port_count];

	if(ports)
		memset(ports, 0, sizeof(DummyTrunk *) * port_count);

	if(groups)
		memset(groups, 0, sizeof(TrunkGroup *) * port_count);

	slog(Slog::levelInfo) << "Generic Test (dummy) driver loaded; capacity=" << port_count << endl;
}

DummyDriver::~DummyDriver()
{
	if(active)
		stop();

	if(ports)
		delete ports;

	if(groups)
		delete groups;
}

int DummyDriver::start(void)
{
	int count = 0;

	if(active)
	{
		slog(Slog::levelError) << "driver already started" << endl;
		return 0;
	}


	for(count = 0; count < getDeviceCount(); ++count)
        	ports[count] = new DummyTrunk(count);

	tcgetattr(0, &t);
	slog(Slog::levelInfo) << "driver starting..." << endl;
	Thread::start();

	active = true;
	return count;
}

void DummyDriver::stop(void)
{
	unsigned count;
	DummyTrunk *trk;
	if(!active)
		return;

	terminate();
        tcsetattr(0, TCSANOW, &t);

	if(ports)
	{
		for(count = 0; count < (unsigned)getDeviceCount(); ++count)
		{
			trk = ports[count];
			if(trk)
				delete trk;
		}
		memset(ports, 0, sizeof(DummyTrunk *) * port_count);
	}

	active = false;
	slog(Slog::levelInfo) << "driver stopping..." << endl;
}

void DummyDriver::run(void)
{
	TrunkEvent event;
	timeout_t timeout;
	pollfd pfd[1];
	char ch;
	struct termios n;
	DummyTrunk *trunk;
	int port = 0;
	char name[16];

	setCancel(cancelImmediate);
	slog(Slog::levelNotice) << "dummy: start thread" << endl;\

	pfd[0].fd = 0;
	
        tcgetattr(0, &t);
        tcgetattr(0, &n);
        n.c_lflag &= ~(ECHO|ICANON);
        tcsetattr(0, TCSANOW, &n);

	for(;;)
	{
		Thread::yield();
		trunk = ports[port];
		if(!trunk)
		{
			slog(Slog::levelError) << "dummy: missing trunk" << endl;
			sleep(1000);
			continue;
		}

		snprintf(name, sizeof(name), "dummy%d", port);
		
		trunk->enterMutex();
		timeout = trunk->getTimer();
		if(!timeout)
		{
			trunk->endTimer();
			event.id = TRUNK_TIMER_EXPIRED;
			trunk->postEvent(&event);
		}
		trunk->leaveMutex();

		if(timeout > 500)
			timeout = 500;

                pfd[0].events = POLLIN | POLLRDNORM;
                pfd[0].revents = 0;

                if(poll(pfd, 1, timeout) > 0)
		{
			::read(0, &ch, 1);
                        switch(ch)
                        {
                        case '#':
                                slog(Slog::levelDebug) << name
                                        << ": digit #..." << endl;
                                event.id = TRUNK_DTMF_KEYUP;
                                event.parm.dtmf.digit = 11;
                                event.parm.dtmf.duration = 60;
                                trunk->postEvent(&event);
                                sleep(10);
                                break;
                        case '*':
                                slog(Slog::levelDebug) << name
                                        << ": digit *..." << endl;
                                event.id = TRUNK_DTMF_KEYUP;
                                event.parm.dtmf.digit = 10;
                                event.parm.dtmf.duration = 60;
                                trunk->postEvent(&event);
                                sleep(10);
                                break;
                        case '0':
                        case '1':
                        case '2':
                        case '3':
                        case '4':
                        case '5':
                        case '6':
                        case '7':
                        case '8':
                        case '9':
                                slog(Slog::levelDebug) << name
                                        << ": digit " << ch << "..." << endl;
                                event.id = TRUNK_DTMF_KEYUP;
                                event.parm.dtmf.digit = ch - '0';
                                event.parm.dtmf.duration = 60;
                                trunk->postEvent(&event);
                                sleep(10);
                                break;
                        case 'r':
                        case 'R':
                                slog(Slog::levelDebug) << name
                                        << ": ringing..." << endl;
                                event.id = TRUNK_RINGING_ON;
                                event.parm.ring.digit = 0;
                                event.parm.ring.duration = 50;
                                trunk->postEvent(&event);
                                sleep(100);
                                event.id = TRUNK_RINGING_OFF;
                                trunk->postEvent(&event);
                                break;
			case 'n':
			case 'N':
				if(++port >= getTrunkCount())
					port = 0;
				break;
                        case 'D':
                        case 'd':
                                slog(Slog::levelDebug) << name
                                        << ": dialtone..." << endl;
                                event.id = TRUNK_CPA_DIALTONE;
                                trunk->postEvent(&event);
                                break;
                        case 'B':
                        case 'b':
                                slog(Slog::levelDebug) << name
                                        << ": busytone..." << endl;
                                event.id = TRUNK_CPA_BUSYTONE;
                                trunk->postEvent(&event);
                                break;
                        case ' ':
                                slog(Slog::levelDebug) << name
                                        << ": step/start..." << endl;
                                event.id = TRUNK_NULL_EVENT;
                                trunk->postEvent(&event);
                                break;
                        case 'H':
                        case 'h':
                                slog(Slog::levelDebug) << name
                                        << ": hangup..." << endl;
                                event.id = TRUNK_STOP_DISCONNECT;
                                trunk->postEvent(&event);
                                break;
                        case 'i':
                        case 'I':
                                slog(Slog::levelDebug) << name
                                        << ": idle..." << endl;
                                event.id = TRUNK_MAKE_IDLE;
                                trunk->postEvent(&event);
                                break;
                        case 'X':
                        case 'x':
                        case 3:
                                slog(Slog::levelDebug) << name
                                        << ": exiting..." << endl;
                                raise(SIGINT);
			}
		}
	}
}
	
Trunk *DummyDriver::getTrunkPort(int id)
{
	if(id < 0 || id >= port_count)
		return NULL;

	if(!ports)
		return NULL;

	return (Trunk *)ports[id];
}

DummyDriver dummyivr;

#ifdef	CCXX_NAMESPACES
};
#endif
