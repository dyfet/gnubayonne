// Copyright (C) 2000 Open Source Telecom Corporation.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"
#include <sys/ioctl.h>
#include <sys/stat.h>

#include <sys/soundcard.h>

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

DummyRecord::DummyRecord(DummyTrunk *trunk) :
AudioFile(), Service((Trunk *)trunk, keythreads.priAudio())
{
	reset = false;
	dev = trunk->dev;
}

DummyRecord::~DummyRecord()
{
        char buffer[12];
        struct stat ino;
        int trim;

        terminate();

	sprintf(buffer, "%ld", getPosition());
	trunk->setSymbol(SYM_OFFSET, buffer);

	if(data->record.minsize)
	{
		if(getPosition() < data->record.minsize)
		{
			trunk->setSymbol(SYM_RECORDED, "0");
			remove(data->record.name);
			AudioFile::close();
			if(reset && dev > -1)
				ioctl(dev, SOUND_PCM_RESET, 0);
			return;
		}
	}


	trim = toBytes(getEncoding(), data->record.trim);
        stat(data->record.name, &ino);
	if(ino.st_size <= trim || data->record.frames)
	{
		remove(data->record.name);
		trunk->setSymbol(SYM_RECORDED, "0");
	}
	else
	{
		sprintf(buffer, "%ld",
			getPosition() - data->record.trim);
		trunk->setSymbol(SYM_RECORDED, buffer);
               	truncate(data->record.name, ino.st_size - trim);
               	chown(data->record.name, keyserver.getUid(), keyserver.getGid());
		if(data->record.save)
			rename(data->record.name, data->record.save);
	}

        AudioFile::close();
	if(reset && dev > -1)
		ioctl(dev, SOUND_PCM_RESET, 0);
}

void DummyRecord::initial(void)
{
	int codec = AFMT_MU_LAW; 
	int rate = 8000;
	int div = 4;
	int chn = 1;
	char buffer[32];
	Audio::Info info;
	const char *ext;
	const char *fmt = data->record.encoding;
	
	trunk->getName(buffer);

        info.format = raw;
        info.encoding = pcm16Mono;
        info.order = 0;
        info.annotation = (char *)data->record.annotation;
        info.rate = 8000;

        ext = strrchr(data->record.name, '/');
        if(!ext)
                ext = data->record.name;

        ext = strrchr(ext, '.');

        if(!ext)
        {
                ext = data->record.extension;
                strcat(data->record.name, ext);
        }

	if(!fmt)
		fmt = "pcm";

        if(!stricmp(ext, ".al"))
		fmt = "alaw";
	else if(!stricmp(ext, ".ul"))
		fmt = "mulaw";
        else if(!stricmp(ext, ".au") || !stricmp(ext, ".snd"))
        {
                info.format = snd;
                info.order = __BIG_ENDIAN;
        }
        else if(!stricmp(ext, ".wav"))
        {
                info.format = riff;
                info.order = __LITTLE_ENDIAN;
        }

	if(!stricmp(fmt, "pcm") || !stricmp(fmt, "l16") || !stricmp(fmt, "linear"))
		info.encoding = pcm16Mono;
	else if(!stricmp(fmt, "alaw"))
		info.encoding = alawAudio;
	else if(!stricmp(fmt, "g.711") || !stricmp(fmt, "g711"))
	{
		if(info.encoding != alawAudio)
			info.encoding = mulawAudio;
	}
	else if(!stricmp(fmt, "ulaw") || !stricmp(fmt, "mulaw"))
		info.encoding = mulawAudio;
	else if(stricmp(fmt, "raw"))
	{
		slog(Slog::levelError) << buffer << ": unsupported codec requested" << endl;
		Service::failure();
	}

	if(data->record.offset != (unsigned long)-1)
	{
		open(data->record.name);
		setPosition(data->record.offset);
	}
        else if(data->record.append)
                open(data->record.name);
        else
	{
		remove(data->record.name);
                create(data->record.name, &info);
	}
        if(!isOpen())
        {
                slog(Slog::levelError) << data->record.name << ": cannot open" << endl;
                Service::failure();
        }
        if(data->record.append)
                setPosition();

	switch(getEncoding())
	{
	case mulawAudio:
//		codec = AFMT_MU_LAW;
		codec = AFMT_S16_LE;
		rate = 8000;
		break;
	case alawAudio:
//		codec = AFMT_A_LAW;
		codec = AFMT_S16_LE;
		rate = 8000;
		break;
	case pcm16Mono:
                switch(getFormat())
                {
                case snd:
                        codec = AFMT_U16_BE;
                        break;
                default:
                        codec = AFMT_S16_LE;
                        break;
                }

		rate = 8000;
		break;
	default:
		slog(Slog::levelError) << buffer << ": unsupported codec required" << endl;
		Service::failure();
	}

	reset = true;

	ioctl(dev, SOUND_PCM_RESET);	
	ioctl(dev, SOUND_PCM_SYNC);
	ioctl(dev, SOUND_PCM_SUBDIVIDE, &div);
	ioctl(dev, SOUND_PCM_SETFMT, &codec);
	ioctl(dev, SOUND_PCM_READ_RATE, &rate);
	ioctl(dev, SOUND_PCM_READ_CHANNELS, &chn);
	ioctl(dev, SNDCTL_DSP_GETBLKSIZE, &bufsize);
	samples = toSamples(getEncoding(), bufsize);
}

void DummyRecord::run(void)
{
	char name[33];
	char *ob;
	signed short buffer[samples];
	char cvt[samples];
	Audio::Error status = Audio::errSuccess;
	int len = 0;
	int frames = 0;
	AudioCodec *codec = AudioCodec::getCodec(getEncoding(), "g.711");

	trunk->getName(name);
	setCancel(cancelImmediate);

	if(data->record.info)
		Service::success();

	while(status == Audio::errSuccess && !stopped)
	{
		Thread::yield();
                if(data->record.frames && frames++ >= data->record.frames)
                {
                        frames = 0;
                        setPosition(0l);
                }
                len = ::read(dev, buffer, samples * 2);
                if(len < (int)bufsize)
                        status = errReadIncomplete;
                else switch(getEncoding())
		{
		case pcm16Mono:
                        status = putSamples(buffer, samples);
			break;
		case alawAudio:
			codec->encode(buffer, cvt, samples);
			status = putSamples(cvt, samples);
			break;
		case mulawAudio:
			codec->encode(buffer, cvt, samples);			
			status = putSamples(cvt, samples);
			break;
		}
	}
	if(status == errWriteFailure)
		slog(Slog::levelError) << name << ": failed record" << endl;
//	ioctl(dev, SOUND_PCM_SYNC);
	reset = false;
	Service::success();
}

bool DummyTrunk::recordHandler(TrunkEvent *event)
{
	unsigned short mask;

	switch(event->id)
	{
        case TRUNK_DTMF_KEYUP:
                mask = (1 << event->parm.dtmf.digit);
                if(!(mask & data.record.term))
                        return false;
		trunkSignal(TRUNK_SIGNAL_STEP);
	case TRUNK_STOP_STATE:
		if(dev > -1)
			ioctl(dev, SOUND_PCM_RESET, 0);
		handler = &DummyTrunk::stepHandler;
		return true;
	case TRUNK_SERVICE_SUCCESS:
		trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &DummyTrunk::stepHandler;
		return true;
	case TRUNK_SERVICE_FAILURE:
		setSymbol(SYM_ERROR, "record-failed");
		trunkSignal(TRUNK_SIGNAL_ERROR);
		handler = &DummyTrunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		enterState("record");
		flags.dsp = DSP_MODE_VOICE;
		status[tsid] = 'r';
		if(data.record.text)
			cout << "*** " << data.record.text << endl;
		if(dev < 0)
		{
			trunkSignal(TRUNK_SIGNAL_STEP);
			handler = &DummyTrunk::stepHandler;
			return true;
		}
		if(data.record.term)
			setDTMFDetect(true);
		else
			setDTMFDetect();
		thread = new DummyRecord(this);
		thread->start();
		setTimer(data.record.timeout);
		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
