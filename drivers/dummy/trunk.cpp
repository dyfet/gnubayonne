// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef  CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

DummyTrunk::DummyTrunk(int ts) :
Trunk(ts, &dummyivr)
{
	const char *devname = dummyivr.getDevice(ts);
	TrunkEvent event;
	snprintf(name, sizeof(name), "dummy/%d", ts);
	
	dev = ::open(devname, O_RDWR);
	if(dev < 0)
	{
		errlog("failed", "Device=%s", devname);
		slog(Slog::levelError) << name << ": cannot open " << devname << endl;
	}
	else
		slog(Slog::levelInfo) << name << ": using " << devname << endl;
		
	event.id = TRUNK_ENTER_STATE;
	handler = &DummyTrunk::idleHandler;
	postEvent(&event);
}

DummyTrunk::~DummyTrunk()
{
	long flags;
	if(dev > -1)
	{
		flags = fcntl(dev, F_GETFL) | O_NDELAY;
		fcntl(dev, F_SETFL, flags);
		::close(dev);
	}
	slog(Slog::levelInfo) << name << ":device stopped" << endl;
}

void DummyTrunk::initSyms(void)
{
	setConst(SYM_NETWORK, "none");
	setConst(SYM_INTERFACE, "none");
}
	
void DummyTrunk::getName(char *buffer)
{
	strcpy(buffer, name);
}

bool DummyTrunk::exit(void)
{
        if(!flags.onexit)
                if(redirect("::exit"))
                {
			//autoloop(false);
                        flags.onexit = true;
                        return false;
                }

          handler = &DummyTrunk::hangupHandler;
	  return true;
}

unsigned long DummyTrunk::getIdleTime(void)
{
        time_t now;

        time(&now);
        if(handler == &DummyTrunk::idleHandler)
                return now - idletime;

        return 0;
}

void DummyTrunk::trunkStep(trunkstep_t step)
{
        TrunkEvent event;

        if(handler != &DummyTrunk::stepHandler)
                return;

        event.id = TRUNK_MAKE_STEP;
        event.parm.step = step;
        stepHandler(&event);
}

bool DummyTrunk::postEvent(TrunkEvent *event)
{
        bool rtn = true;
        trunkhandler_t prior;
	char evt[65];
	
        enterMutex();
        switch(event->id)
        {
	case TRUNK_TIMER_SYNC:
		if(!synctimer)
			rtn = false;
		synctimer = 0;
		break;
	case TRUNK_TIMER_EXIT:
		if(!exittimer)
			rtn = false;
		exittimer = 0;
		break;
        case TRUNK_DTMF_KEYUP:
                if(flags.offhook)
                        time(&idletime);
                if(!flags.dtmf)
                        rtn = false;
                break;
        }
        if(!rtn)
        {
                leaveMutex();
                return false;
        }

        if(!handler)
        {
                slog(Slog::levelWarning) << name
                	<< ": no handler active; event=" << event->id << endl;
                leaveMutex();
                return false;
        }

retry:
        debug->debugEvent(this, event);
        prior = handler;
        rtn = (this->*handler)(event);
        if(rtn)
        {
                if(handler != prior)
                {
			if(prior == &DummyTrunk::idleHandler)
				setIdle(false);
                        event->id = TRUNK_ENTER_STATE;
                        goto retry;
                }
                leaveMutex();
                return true;
        }


        // default handler

        rtn = true;
        switch(event->id)
        {
        case TRUNK_RINGING_ON:
                ++rings;
		snprintf(evt, sizeof(evt), "ring:%d", rings);
		if(trunkEvent(evt))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
                break;
        case TRUNK_ENTER_STATE:
                if(flags.offhook)
                        setDTMFDetect();
                else
                        setDTMFDetect(false);
                endTimer();
                break;
        case TRUNK_LINE_WINK:
		if(trunkEvent("line:wink"))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
                if(!flags.offhook)
                        break;
		goto drop;
        case TRUNK_CPA_DIALTONE:
		if(trunkEvent("tone:dialtone"))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		goto drop;
        case TRUNK_STOP_DISCONNECT:
drop:
                if(flags.onexit)
                        break;
                if(trunkSignal(TRUNK_SIGNAL_HANGUP))
		{
                	event->id = TRUNK_STOP_STATE;
                	goto retry;
		}
                break;
        case TRUNK_SEND_MESSAGE:
                if(recvEvent(event))
                {
                        event->id = TRUNK_STOP_STATE;
                        goto retry;
                }
                break;
        case TRUNK_TIMER_SYNC:
                if(trunkSignal(TRUNK_SIGNAL_TIME))
                {
                        event->id = TRUNK_STOP_STATE;
                        goto retry;
                }
                break;
        case TRUNK_TIMER_EXIT:
                if(trunkSignal(TRUNK_SIGNAL_TIME))
                        event->id = TRUNK_STOP_STATE;
                else
                        event->id = TRUNK_STOP_DISCONNECT;
                goto retry;
        case TRUNK_TIMER_EXPIRED:
                if(!trunkSignal(TRUNK_SIGNAL_TIMEOUT))
			trunkSignal(TRUNK_SIGNAL_STEP);
                event->id = TRUNK_STOP_STATE;
                goto retry;
                break;
	case TRUNK_SYNC_PARENT:
		if(trunkEvent(event->parm.sync.msg))
		{
			setSymbol(SYM_STARTID, event->parm.sync.id);
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		rtn = false;
		break;
        case TRUNK_CHILD_EXIT:
                if(trunkSignal(TRUNK_SIGNAL_CHILD))
                {
                        event->id = TRUNK_STOP_STATE;
                        goto retry;
                }
                break;
        case TRUNK_DTMF_KEYUP:
                if(digits < MAX_DIGITS)
                        dtmf.bin.data[digits++] = digit[event->parm.dtmf.digit];
                dtmf.bin.data[digits] = 0;
		snprintf(evt, sizeof(evt), "digits:%s", dtmf.bin.data);
		if(trunkEvent(evt))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
                if(trunkSignal((trunksignal_t)(event->parm.dtmf.digit + TRUNK_SIGNAL_0)))
                {
                        event->id = TRUNK_STOP_STATE;
                        goto retry;
                }
                break;
        case TRUNK_EXIT_SHELL:
                if(event->parm.exitpid.seq != tgi.seq)
                        break;
                tgi.pid = 0;
                if(tgi.fd > -1)
                {
                        close(tgi.fd);
                        tgi.fd = -1;
			snprintf(evt, sizeof(evt), "exit:%d", 
				event->parm.exitpid.status);
			if(!trunkEvent(evt))
	                        trunkSignal(TRUNK_SIGNAL_STEP);
                        event->id = TRUNK_STOP_STATE;
                        goto retry;
                }
                break;
        case TRUNK_STOP_STATE:
                endTimer();
                handler = &DummyTrunk::stepHandler;
                break;
        case TRUNK_EXIT_STATE:
                break;
        case TRUNK_MAKE_IDLE:
                handler = &DummyTrunk::hangupHandler;
                break;
        default:
                rtn = false;
        }
        if(handler != prior)
        {
                event->id = TRUNK_ENTER_STATE;
                goto retry;
        }
        leaveMutex();
        return rtn;
}

#ifdef	CCXX_NAMESPACES
};
#endif
