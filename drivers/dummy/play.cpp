// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <cerrno>

#include <sys/soundcard.h>

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

DummyPlay::DummyPlay(DummyTrunk *trunk) :
URLAudio(), Service((Trunk *)trunk, keythreads.priAudio())
{
	reset = false;
	dev = trunk->dev;
	id = trunk->id;
	rate = 8000;
}

DummyPlay::~DummyPlay()
{
	char buffer[12];
#ifdef	HAVE_AIO_H
	if(!isThread() && reset && dev > -1)
		aio_cancel(dev, &iob);
#endif
	terminate();
	sprintf(buffer, "%ld", getTransfered());
	trunk->setSymbol(SYM_PLAYED, buffer);
	sprintf(buffer, "%ld", getPosition());
	trunk->setSymbol(SYM_OFFSET, buffer);
	URLAudio::close();
	if(reset && dev > -1)
		ioctl(dev, SOUND_PCM_RESET, 0);
}

char *DummyPlay::getContinuation(void)
{
	char *fn;

	if(data->play.mode == PLAY_MODE_TEMP || data->play.mode == PLAY_MODE_ONE)
		return NULL;
retry:
	fn = getPlayfile();
	if(fn && data->play.mode == PLAY_MODE_ANY)
	{
		if(!canAccess(fn))
			goto retry;
	}
	return fn;
}

void DummyPlay::initial(void)
{
	int codec = AFMT_MU_LAW; 
	int div = 4;
	int chn = 1;
	char buffer[32];
	char *fn;
	struct stat ino;
	struct tm *dt;
	struct tm tbuf;
	char *ann;

	trunk->getName(buffer);

	if(tts)
		if(!tts->synth(id, data))
			Service::failure();

retry:
	fn = getPlayfile();
	if(!fn)
	{
		if(data->play.lock)
		{
			cachelock.unlock();
			data->play.lock = false;
		}
		if(data->play.mode == PLAY_MODE_ANY)
			Service::success();
		slog(Slog::levelError) << "no file" << endl;
		Service::failure();
	}
	stat(fn, &ino);
	open(fn);
	if(data->play.lock)
	{
		cachelock.unlock();
		data->play.lock = false;
	}	
	if(!isOpen())
	{
		if(data->play.mode == PLAY_MODE_ANY || data->play.mode == PLAY_MODE_ONE)
			goto retry;
		errlog("missing", "Prompt=%s", fn);
		slog(Slog::levelError) << fn << ": cannot open" << endl;
		Service::failure();
	}

	if(data->play.mode == PLAY_MODE_TEMP)
		remove(fn);
	if(data->play.offset)
		setPosition(data->play.offset);
	if(data->play.limit)
		setLimit(data->play.limit);
	dt = localtime_r(&ino.st_ctime, &tbuf);
	sprintf(buffer, "%04d%02d%02d,%02d%02d%02d",
		dt->tm_year + 1900, dt->tm_mon + 1, dt->tm_mday,
		dt->tm_hour, dt->tm_min, dt->tm_sec);
	trunk->setSymbol(SYM_CREATED, buffer);
	ann = getAnnotation();
	if(ann)
		trunk->setSymbol(SYM_ANNOTATION, ann);
	else
		trunk->setSymbol(SYM_ANNOTATION, "");

	switch(data->play.speed)
	{
	case SPEED_NORMAL:
		rate = 8000;	
		break;
	case SPEED_FAST:
		rate = 9600;
		break;
	case SPEED_SLOW:
		rate = 6400;
		break;
	}

	switch(getEncoding())
	{
	case mulawAudio:
//		codec = AFMT_MU_LAW;
		codec = AFMT_S16_LE;
		break;
	case alawAudio:
//		codec = AFMT_A_LAW;
		codec = AFMT_S16_LE;
		break;
	case pcm16Mono:
		switch(getFormat())
		{
		case snd:
			codec = AFMT_S16_BE;
			break;
		default:
			codec = AFMT_S16_LE;
			break;
		}
		break;
	default:
		slog(Slog::levelError) << buffer << ": unsupported codec required" << endl;
		Service::failure();
	}

	reset = true;

	ioctl(dev, SOUND_PCM_RESET);	
	ioctl(dev, SOUND_PCM_SYNC);
	ioctl(dev, SOUND_PCM_SUBDIVIDE, &div);
	ioctl(dev, SOUND_PCM_SETFMT, &codec);
	ioctl(dev, SOUND_PCM_WRITE_RATE, &rate);
	ioctl(dev, SOUND_PCM_WRITE_CHANNELS, &chn);
	ioctl(dev, SNDCTL_DSP_GETBLKSIZE, &bufsize);
	samples = toSamples(getEncoding(), bufsize);
}

void DummyPlay::run(void)
{
	char name[33];
	short linear[bufsize];
	Audio::Error status = Audio::errSuccess;
	Audio::Encoding encoding = getEncoding();
	int len = 0;
#ifdef	HAVE_AIO_H
	const struct aiocb *iobp = &iob;
#endif
	TimerPort runclock;
	timeout_t runtime;
	size_t count;

	trunk->getName(name);
	setCancel(cancelDeferred);

	runclock.setTimer(0);
	runtime = samples * 1000l / rate;

	while(status == Audio::errSuccess && !stopped)
	{
		Thread::yield();
		runclock.incTimer(runtime);

		count = getLinear(linear, samples);
		
		if(count < 1 && data->play.maxtime)
		{
			setPosition(0);
			status = Audio::errSuccess;
			continue;
		}

		if(count < 1)
			break;

		count *= 2;

#ifdef	HAVE_AIO_H
		memset(&iob, 0, sizeof(iob));
#ifdef	AIO_RAW
		iob.aio_flags = AIO_RAW;
#endif
		iob.aio_fildes = dev;
		iob.aio_buf = linear;
		iob.aio_sigevent.sigev_notify = SIGEV_NONE;
		iob.aio_nbytes = count;
		if(!aio_write(&iob))
		{
			aio_suspend(&iobp, 1, NULL);
			len = aio_return(&iob);
		}
#ifdef	ENOSYS
		else if(errno == ENOSYS)
			len = ::write(dev, linear, count);
#endif
#else
		len = ::write(dev, linear, count);	
#endif
	}
	if(status == errReadFailure)
		slog(Slog::levelError) << name << ": failed playback" << endl;
	runtime = runclock.getTimer();
	if(runtime)
		Thread::sleep(runtime);
//	ioctl(dev, SOUND_PCM_SYNC);
	reset = false;
	Service::success();
}

bool DummyTrunk::playHandler(TrunkEvent *event)
{
	switch(event->id)
	{
	case TRUNK_STOP_STATE:
		if(dev > -1)
			ioctl(dev, SOUND_PCM_RESET, 0);
		handler = &DummyTrunk::stepHandler;
		return true;
	case TRUNK_SERVICE_SUCCESS:
		trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &DummyTrunk::stepHandler;
		return true;
	case TRUNK_SERVICE_FAILURE:
		setSymbol(SYM_ERROR, "play-failed");
		trunkSignal(TRUNK_SIGNAL_ERROR);
		handler = &DummyTrunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		enterState("play");
		flags.dsp = DSP_MODE_VOICE;
		status[tsid] = 'p';
		if(data.play.text)
			cout << "*** " << data.play.text << endl;
		if(dev < 0)
		{
			trunkSignal(TRUNK_SIGNAL_STEP);
			handler = &DummyTrunk::stepHandler;
			return true;
		}
	case TRUNK_TIMER_EXPIRED:
		if(thread)
			return false;

		if(data.play.maxtime)
			setTimer(data.play.maxtime);

		setDTMFDetect();
		thread = new DummyPlay(this);
		thread->start();
		return true;
	}
	return false;
}

bool DummyTrunk::playwaitHandler(TrunkEvent *event)
{
	switch(event->id)
	{
        case TRUNK_EXIT_SHELL:
                if(!tgi.pid)
                        return true;
                tgi.pid = 0;
                endTimer();
                if(event->parm.status)
                {
                        trunkSignal(TRUNK_SIGNAL_ERROR);
                        sprintf(buffer, "play-failed-exit-%d",
                                event->parm.status);
                        setSymbol(SYM_ERROR, buffer);
                        handler = &DummyTrunk::stepHandler;
                        return true;
                }
                handler = &DummyTrunk::playHandler;
                return true;  

        case TRUNK_TIMER_EXPIRED:
                if(tgi.pid)
                {
                        kill(tgi.pid, SIGTERM);
                        tgi.pid = 0;
                }
                sprintf(buffer, "play-failed-timeout");
                setSymbol(SYM_ERROR, buffer);
                trunkSignal(TRUNK_SIGNAL_ERROR);
                handler = &DummyTrunk::stepHandler;
                return true;                                                     
                                                                                         
	case TRUNK_ENTER_STATE:
		enterState("playwait");
                endTimer();
                Trunk::setDTMFDetect();
                setTimer(data.play.timeout);
                return true;
        }
        return false;                                                            
}

#ifdef	CCXX_NAMESPACES
};
#endif
