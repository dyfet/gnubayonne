// Copyright (C) 2000-2003 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

bool OH323Trunk::playHandler(TrunkEvent *event)
{
	char *fn;
	struct tm *dt, tbuf;
	struct stat ino;
	char buffer[32];
	int rtn;

	slog(Slog::levelDebug) << "OH323Trunk::playHandler(event->id=" << event->id << ")" << endl;	
	switch(event->id)
	{
	case TRUNK_STOP_STATE:
		endTimer();
		_stopping_state = true;
		if(!stopChannel(AUDIO_TRANSMIT))
			handler = &OH323Trunk::stepHandler;
		return true;
	case TRUNK_AUDIO_IDLE:
		endTimer();
		if(!_stopping_state)
			trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &OH323Trunk::stepHandler;
		endPlay();
		return true;
	case TRUNK_ENTER_STATE:
		enterState("play");
		status[tsid] = 'p';
		_stopping_state = false;
	case TRUNK_TIMER_EXPIRED:
		if(lockConnection())
		{
			if(!conn->channel)
			{
				setSymbol(SYM_ERROR, "play-no-channel");
				trunkSignal(TRUNK_SIGNAL_ERROR);
				handler = &OH323Trunk::stepHandler;
				releaseConnection();
				return true;
			}
			releaseConnection();
		}

		if(data.play.maxtime)
			setTimer(data.play.maxtime);

		endTimer();
		Trunk::setDTMFDetect();
		getName(buffer);
		setSymbol(SYM_PLAYED, "0");
		setSymbol(SYM_OFFSET, "0");
		if(tts)
		{
			if(!tts->synth(id, &data))
			{
				setSymbol(SYM_ERROR, "play-tts-failed");
				trunkSignal(TRUNK_SIGNAL_ERROR);
				handler = &OH323Trunk::stepHandler;
				return true;
			}
		}
retry:
		fn = getPlayfile();
		if(!fn && data.play.lock)
		{
			cachelock.unlock();
			data.play.lock = false;
		}
		if(!fn && data.play.mode != PLAY_MODE_ANY)
		{
			slog(Slog::levelError) << buffer << ": no file to play" << endl;
			setSymbol(SYM_ERROR, "play-failed");
			trunkSignal(TRUNK_SIGNAL_ERROR);
		}
		if(!fn && data.play.mode == PLAY_MODE_ANY)
			trunkSignal(TRUNK_SIGNAL_STEP);
		if(!fn)
		{
			handler = &OH323Trunk::stepHandler;
			return true;
		}
		stat(fn, &ino);
		open(fn);
		if(data.play.lock)
		{
			cachelock.unlock();
			data.play.lock = false;
		}
		if(!isOpen())
		{
			if(data.play.mode == PLAY_MODE_ANY || data.play.mode == PLAY_MODE_ONE)
				goto retry;
			slog(Slog::levelError) << buffer << ": " << fn << ": cannot open" << endl;
			setSymbol(SYM_ERROR, "play-failed");
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &OH323Trunk::stepHandler;
			return true;
		}
		if(data.play.mode == PLAY_MODE_TEMP)
			remove(fn);
		if(data.play.offset)
			setPosition(data.play.offset);
		else
			setPosition(0);
		if(data.play.limit)
			setLimit(data.play.limit);
		dt = localtime_r(&ino.st_ctime, &tbuf);
		sprintf(buffer, "%04d%02d%02d,%02d%02d%02d",
			dt->tm_year + 1900, dt->tm_mon + 1, dt->tm_mday,
			dt->tm_hour, dt->tm_min, dt->tm_sec);
		setSymbol(SYM_CREATED, buffer);
		fn = getAnnotation();
		if(fn)
			setSymbol(SYM_ANNOTATION, fn);
		else
			setSymbol(SYM_ANNOTATION, "");
		Trunk::flags.reset = false;
		if(lockConnection())
		{
			conn->transmit = new URLAudioChannel((URLAudio *)this);
			conn->channel->attachTransmitter(conn->transmit);
			releaseConnection();
		}
		else
		{
			setSymbol(SYM_ERROR, "play-failed");
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &OH323Trunk::stepHandler;
		}
		return true;
	}
	return false;
}

#ifdef CCXX_NAMESPACES
};
#endif

