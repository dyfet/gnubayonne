// Copyright (C) 2000-2003 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

bool OH323Trunk::flashHandler(TrunkEvent *event)
{
	switch(event->id)
	{
	case TRUNK_CPA_FAILURE:
		setSymbol(SYM_TONE, "fail");
		TimerPort::endTimer();
		if(!trunkSignal(TRUNK_SIGNAL_CANCEL))
			if(!trunkSignal(TRUNK_SIGNAL_TONE))
				trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &OH323Trunk::stepHandler;
		return true;
	case TRUNK_CPA_CONNECT:
		if(data.dialxfer.exit)
			trunkSignal(TRUNK_SIGNAL_HANGUP);
		TimerPort::endTimer();
		if(data.dialxfer.digit)
		{
			TimerPort::endTimer();
			handler = &DialogicTrunk::dialHandler;
			return true;
		}
		setSymbol(SYM_TONE, "dial");
		if(trunkSignal(TRUNK_SIGNAL_TONE))
		{
			TimerPort::endTimer();
			handler = &DialogicTrunk::stepHandler;
			return true;
		}
	case TRUNK_TIMER_EXPIRED:
		if(data.dialxfer.digit)
		{
			TimerPort::endTimer();
			handler = &DialogicTrunk::dialHandler;
			return true;
		}
		trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &DialogicTrunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		setSymbol(SYM_TONE, "none");
		enterState("transfer");
		if(!data.dialxfer.digits || strlen(data.dialxfer.digits) < 1)
		{
			TimerPort::endTimer();
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &OH323Trunk::stepHandler;
			return true;
		}
		transferCall();

		TimerPort::setTimer(data.dialxfer.offhook);
		return true;
	}
	return false;
}

#ifdef CCXX_NAMESPACES
};
#endif

