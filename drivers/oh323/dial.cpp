// Copyright (C) 2000-2003 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

bool OH323Trunk::dialHandler(TrunkEvent *event)
{
        int rtn;
	char dialstr[40];
	const char *dp;

	switch(event->id)
	{
	case TRUNK_CALL_RELEASE:
		setSymbol(SYM_TONE, "fail");
		if(!trunkSignal(TRUNK_SIGNAL_HANGUP))
			if(!trunkSignal(TRUNK_SIGNAL_CANCEL))
				if(!trunkSignal(TRUNK_SIGNAL_TONE))
					trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &OH323Trunk::stepHandler;
		return true;
	case TRUNK_STOP_DISCONNECT:
		_stopping_state = true;
		TimerPort::endTimer();
		Trunk::flags.dsp = DSP_MODE_INACTIVE;
		hangupCall();
		trunkSignal(TRUNK_SIGNAL_HANGUP);
		handler = &OH323Trunk::stepHandler;
		return true;
	case TRUNK_TIMER_EXPIRED:
		hangupCall();
		if(data.dialxfer.exit)
			trunkSignal(TRUNK_SIGNAL_HANGUP);
		else if(!trunkSignal(TRUNK_SIGNAL_TIMEOUT))
			if(!trunkSignal(TRUNK_SIGNAL_CANCEL))
				trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &OH323Trunk::stepHandler;
		return true;
	case TRUNK_CPA_BUSYTONE:
		setSymbol(SYM_TONE, "busy");
		hangupCall();
		if(!trunkEvent("dial:busy"))
			if(!trunkEvent("tone:busy"))
				if(!trunkSignal(TRUNK_SIGNAL_BUSY))
					if(!trunkSignal(TRUNK_SIGNAL_TONE))
					{
						if(data.dialxfer.exit)
							trunkSignal(TRUNK_SIGNAL_HANGUP);
						else
							trunkSignal(TRUNK_SIGNAL_STEP);
					}
		handler = &OH323Trunk::stepHandler;
		return true;		
	case TRUNK_CPA_NOANSWER:
		setSymbol(SYM_TONE, "ring");
		hangupCall();
		if(trunkEvent("dial:noanswer"))
		{
			handler = &OH323Trunk::stepHandler;
			return true;
		}
		if(trunkSignal(TRUNK_SIGNAL_NOANSWER))
		{
			handler = &OH323Trunk::stepHandler;
			return true;
		}
		trunkSignal(TRUNK_SIGNAL_STEP);
		event->id = TRUNK_TIMER_EXPIRED;
		return false;
	case TRUNK_CPA_NORINGBACK:
		setSymbol(SYM_TONE, "silence");
		hangupCall();
		if(trunkEvent("dial:noringback"))
		{
			handler = &OH323Trunk::stepHandler;
			return true;
		}
		if(trunkSignal(TRUNK_SIGNAL_SILENCE))
		{
			handler = &OH323Trunk::stepHandler;
			return true;
		}
		trunkSignal(TRUNK_SIGNAL_STEP);
		event->id = TRUNK_TIMER_EXPIRED;
		return false;
	case TRUNK_CPA_FAILURE:
	case TRUNK_CPA_NODIALTONE:
		setSymbol(SYM_TONE, "fail");
		if(_stopping_state)
			return true;
		if(trunkEvent("dial:failed"))
		{
			handler = &OH323Trunk::stepHandler;
			return true;
		}
		if(trunkSignal(TRUNK_SIGNAL_CANCEL))
		{
			handler = &OH323Trunk::stepHandler;
			return true;
		}
		if(trunkSignal(TRUNK_SIGNAL_TONE))
		{
			handler = &OH323Trunk::stepHandler;
			return true;
		}
		trunkSignal(TRUNK_SIGNAL_STEP);
		event->id = TRUNK_TIMER_EXPIRED;
		return false;
	case TRUNK_TONE_IDLE:
	case TRUNK_CPA_CONNECT:
	case TRUNK_CALL_ANSWERED:
		if(!trunkEvent("dial:answer"))
		{
			if(data.dialxfer.exit)
				trunkSignal(TRUNK_SIGNAL_HANGUP);
			else
				trunkSignal(TRUNK_SIGNAL_STEP);
		}
		handler = &OH323Trunk::stepHandler;
		return true;
	case TRUNK_STOP_STATE:
		TimerPort::endTimer();
		handler = &OH323Trunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		enterState("dial");
		_stopping_state = false;
		TimerPort::endTimer();
		setSymbol(SYM_TONE, "none");
		setDTMFDetect(false);
		status[tsid] = 'd';
		if(strlen(data.dialxfer.digit) < 1)
		{
			setSymbol(SYM_ERROR, "dial-no-number");
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &OH323Trunk::stepHandler;
			return true;
		}
		dp = group->getLast("dialmode");
		if(!dp)
			dp = "T";

		switch(*dp)
		{
		case 't':
		case 'T':
		case 'd':
		case 'D':
			dp = "T";
			break;
		case 'm':
		case 'M':
			dp = "M";
			break;
		case 'p':
		case 'P':
			dp = "P";
			break;
		default:
			dp = "";
		}

		strcpy(dialstr, dp);
		strcat(dialstr, data.dialxfer.digit);

		if(data.dialxfer.timeout)
			setTimer(data.dialxfer.timeout + 120 * strlen(data.dialxfer.digit));

		if(!makeCall(data.dialxfer.digit, data.dialxfer.callingdigit))
		{
                       	setSymbol(SYM_ERROR, "dial-failed");
                       	trunkSignal(TRUNK_SIGNAL_ERROR);
                       	handler = &OH323Trunk::stepHandler;
			return true;
		}
	        return true;
	case TRUNK_LINE_WINK:
	        return true;
	}

	return false;
}

#ifdef CCXX_NAMESPACES
};
#endif

