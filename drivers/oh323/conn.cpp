// Copyright (C) 2000-2003 Open Source Telecom Corporation.
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

OH323Connection::OH323Connection(OH323EndPoint & ep, unsigned ref, unsigned tid) :
H323Connection(ep, ref)
{
	id = tid;
	endpoint = &ep;
	trunk = oh323ivr.ports[tid];

	channel = NULL;
	transmit = NULL;
	receive = NULL;

	transmitUp = receiveUp = false;
}

OH323Connection::~OH323Connection()
{
}

void OH323Connection::CleanUpOnCallEnd()
{
	oh323ivr.ports[id] = NULL;

	H323Connection::CleanUpOnCallEnd();
}

BOOL OH323Connection::OnIncomingCall(const H323SignalPDU & setupPDU, H323SignalPDU &)
{
        const H225_Setup_UUIE & setup = setupPDU.m_h323_uu_pdu.m_h323_message_body;
        const H225_ArrayOf_AliasAddress & adr = setup.m_destinationAddress;
	PString dnid("");
	TrunkEvent event;
        int i;

	//trunk->enterMutex();
	for(i=0; i < adr.GetSize(); i++)
		//if(adr[i].GetTag() == H225_AliasAddress::e_dialedDigits)
			dnid = H323GetAliasAddressString(adr[i]);

	slog(Slog::levelDebug) << "OH323Connection::OnIncomingCall(): H.323 Call From " <<
		GetRemotePartyName() << " to " << GetLocalPartyName() << endl;

	if(setupPDU.m_h323_uu_pdu.HasOptionalField(H225_H323_UU_PDU::e_nonStandardData))
	{
		PString param = setupPDU.m_h323_uu_pdu.m_nonStandardData.m_data.AsString();
		if(!param)
			slog(Slog::levelDebug) << "Non standard param in setup PDU: " << param << endl;
	}

	trunk->setConst(SYM_CLID, GetControlChannel().GetRemoteAddress());
	trunk->setConst(SYM_NAME, GetRemotePartyName());
	trunk->setConst(SYM_DNID, dnid);
	callToken = GetCallToken();
	//event.id = TRUNK_CALL_OFFER;
	//trunk->postEvent(&event);
	
	//trunk->leaveMutex();

	return true;
}

H323Connection::AnswerCallResponse OH323Connection::OnAnswerCall(const PString & callerName,
				const H323SignalPDU & setupPDU,
				H323SignalPDU &)
{
	TrunkEvent event;
	slog(Slog::levelDebug) << "OH323Connection::OnAnswerCall(): Call Token " << callToken << endl;
	event.id = TRUNK_CALL_OFFER;
	event.parm.data = (void *)(const unsigned char*)callToken;
	trunk->postEvent(&event);
	return H323Connection::AnswerCallPending;
}

void OH323Connection::OnEstablished()
{
	slog(Slog::levelDebug) << "OH323Connection::OnEstablished(): In call with " <<
		GetRemotePartyName() << endl;

	TrunkEvent event;
	event.id = TRUNK_CALL_ANSWERED;
	trunk->postEvent(&event);
}

void OH323Connection::OnCleared()
{
	TrunkEvent event;

	if(GetCallEndReason() != H323Connection::EndedByCallForwarded)
	{
		event.id = TRUNK_STOP_DISCONNECT;
		trunk->postEvent(&event);
	}

	//slog(Slog::levelDebug) << "OH323Connection::OnCleared(): Call Duration: " <<
	//	(PTime() - GetConnectionStartTime()) << " Disconnect Cause " <<
	//	GetCallEndReason() << endl;
}

BOOL OH323Connection::OpenAudioChannel(BOOL isEncoding,
                unsigned bufferSize,
                H323AudioCodec & codec)
{
	BOOL rtn;

	if(isEncoding)
		outCodec = &codec;
	else
		inCodec = &codec;

	slog(Slog::levelDebug) << "OH323Connection::OpenAudioChannel(" << 
		(isEncoding ? "record)" : "play)") << endl;

	if(!channel)
		channel = new OH323Channel(trunk);

	rtn = codec.AttachChannel(channel, FALSE);

	return TRUE;
}

BOOL OH323Connection::OnSendSignalSetup(H323SignalPDU & setupPDU)
{
	H225_Setup_UUIE &setup = setupPDU.m_h323_uu_pdu.m_h323_message_body;
	H225_ArrayOf_AliasAddress &addr = setup.m_sourceAddress;
	
	setupPDU.m_h323_uu_pdu.IncludeOptionalField(H225_H323_UU_PDU::e_nonStandardData);
	setupPDU.m_h323_uu_pdu.m_nonStandardData.m_nonStandardIdentifier.SetTag(H225_NonStandardIdentifier::e_h221NonStandard);
	endpoint->SetH221NonStandardInfo(setupPDU.m_h323_uu_pdu.m_nonStandardData.m_nonStandardIdentifier);
	return TRUE;
}

BOOL OH323Connection::OnStartLogicalChannel(H323Channel & channel)
{
	PString dir;
	TrunkEvent event;

	switch(channel.GetDirection())
	{
	case H323Channel::IsTransmitter:
		transmitUp = true;
		dir = "sending";
		break;
	case H323Channel::IsReceiver:
		receiveUp = true;
		dir = "receiving";
		break;
	default:
		break;
	}

	slog(Slog::levelDebug) << "OH323Connection::OnStartLogicalChannel(): Started " << dir << ": " << 
		channel.GetCapability() << endl;

	if(transmitUp && receiveUp)
	{
        	//event.id = TRUNK_CALL_ANSWERED;
        	//trunk->postEvent(&event);
	}

	return true;
}

void OH323Connection::OnUserInputString(const PString & value)
{
	TrunkEvent event;

	const char *digit = (const char *)value;
	int i;

	slog(Slog::levelDebug) << "OH323Connection::OnUserInputString(" << value << ")" << endl;

	if(value.Left(3) == "MSG")
	{
		trunk->setSymbol(SYM_NOTIFYTEXT, value.Mid(3));
		trunk->setSymbol(SYM_NOTIFYTYPE, "user");
		event.id = TRUNK_CALL_INFO;
		if(trunk->postEvent(&event))
			return;
	}

	for(i = 0; i < value.GetLength(); i++)
	{
		switch(digit[i])
		{
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			event.id = TRUNK_DTMF_KEYUP;
			event.parm.dtmf.digit = (int)(digit[i] - '0');
			event.parm.dtmf.duration = 60;
			trunk->postEvent(&event);
			break;
		case '*':
			event.id = TRUNK_DTMF_KEYUP;
			event.parm.dtmf.digit = 10;
			event.parm.dtmf.duration = 60;
			trunk->postEvent(&event);
			break;
		case '#':
			event.id = TRUNK_DTMF_KEYUP;
			event.parm.dtmf.digit = 11;
			event.parm.dtmf.duration = 60;
			trunk->postEvent(&event);
			break;
		}
	}
}

void OH323Connection::OnUserInputTone(char tone, unsigned duration, unsigned channel, unsigned rtpts)
{
	slog(Slog::levelDebug) << "OH323Connection::OnUserInputTone(): tone=" << tone << 
		" duration=" << duration << endl;
	OnUserInputString(tone);
}

void OH323Connection::muteTransmit(BOOL mute)
{
	H323Channel *channel = FindChannel(RTP_Session::DefaultAudioSessionID, FALSE);

	if(channel != NULL)
		channel->SetPause(mute);
}

void OH323Connection::muteReceive(BOOL mute)
{
	H323Channel *channel = FindChannel(RTP_Session::DefaultAudioSessionID, TRUE);

	if(channel != NULL)
		channel->SetPause(mute);
}

#ifdef	CCXX_NAMESPACES
};
#endif
