// Copyright (C) 2000-2003 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

bool OH323Trunk::ringHandler(TrunkEvent *event)
{
	char script[256];
	int argc = 0;
	char *args[2];
	char **start = NULL;
	const char *data;
	const char *value;
	TrunkGroup *grp = NULL;

	//slog(Slog::levelDebug) << event->id << endl;
	switch(event->id)
	{
	case TRUNK_ENTER_STATE:
		enterState("ring");
		status[tsid] = '!';

	//	getCallerId();

	/*	if(group->getAccept())
		{
			if(!acceptCall())
			{
				endTimer();
				slog(Slog::levelError) << "dx(" << id << "): error accepting" << endl;
				handler = &OH323Trunk::hangupHandler;
			}
			return true;
		}*/
		/*if(rings > 1)
			Trunk::flags.dsp = DSP_MODE_INACTIVE;
		if(rings < group->getAnswer())
		{
			setTimer(group->getRingTime() * 1000);
			return true;
		} */
		//sprintf(script, "ring%d", event->parm.ring.digit);
		if(!grp)
			grp = group;
		start = getInitial(args);
		if(attach(*start))
		{
			setList(++start);
			handler = &OH323Trunk::stepHandler;
			return true;
		}
		rings = 0;
		getName(script);
		slog(Slog::levelError) << script<< ": cannot answer call" << endl;	
		return true;
	case TRUNK_START_SCRIPT:
		return false;
	case TRUNK_STOP_DISCONNECT:
	case TRUNK_TIMER_EXPIRED:
		Trunk::flags.dsp = DSP_MODE_INACTIVE;
		handler = &OH323Trunk::hangupHandler;
		return true;
	}
	return false;
}

#ifdef CCXX_NAMESPACES
};
#endif

