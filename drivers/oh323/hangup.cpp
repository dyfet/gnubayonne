// Copyright (C) 2000-2003 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

bool OH323Trunk::hangupHandler(TrunkEvent *event)
{
	timeout_t reset;

	//slog(Slog::levelDebug) << "OH323Trunk::hangupHandler(event->id=" << event->id << ")" << endl;
	switch(event->id)
	{
	case TRUNK_STOP_DISCONNECT:
	case TRUNK_CALL_DISCONNECT:
		TimerPort::endTimer();
		stopServices();
		Trunk::flags.reset = false;
		handler = &OH323Trunk::idleHandler;
		return true;
	case TRUNK_ENTER_STATE:
		enterState("hangup");
		status[tsid] = 'h';
		synctimer = exittimer = 0;
		TimerPort::endTimer();
		if(joined)
			Part();
		if(tgi.pid)
			::kill(tgi.pid, SIGHUP);
		if(thread)
			reset = thread->stop();
		else
			reset = 0;
		Trunk::flags.dsp = DSP_MODE_INACTIVE;
		stopChannel(AUDIO_BOTH);

		hangupCall();

		Trunk::detach();
		if(!isConnected())
			handler = &OH323Trunk::idleHandler;

		return true;
	case TRUNK_TIMER_EXPIRED:
		stopServices();
		Trunk::flags.reset = false;
		handler = &OH323Trunk::idleHandler;
		return true;
	case TRUNK_MAKE_IDLE:
		return true;
	}
	return false;
}

#ifdef CCXX_NAMESPACES
};
#endif

