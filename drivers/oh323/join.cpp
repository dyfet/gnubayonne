// Copyright (C) 2000-2003 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

bool OH323Trunk::joinHandler(TrunkEvent *event)
{
	TrunkEvent jevent;
	struct tm *dt, tbuf;
	struct stat ino;
	const char *cp;
	char buffer[32];
	const char *ext;
	char *fn;
	Audio::Info info;

	slog(Slog::levelDebug) << "OH323Trunk::joinHandler(event->id=" << event->id << ")" << endl;

	switch(event->id)
	{
	case TRUNK_STOP_STATE:
		data.join.count = 0;
		if(joined && joined->getJoined())
			Part();
		TimerPort::endTimer();
		//stopChannel
		_stopping_state = true;
		//delete softJoin;
		//softJoin = NULL;
		handler = &OH323Trunk::stepHandler;
		return true;
	case TRUNK_TIMER_EXPIRED:
		if(data.join.count)
		{
			--data.join.count;
			if(data.join.trunk->getSequence() != data.join.seq)
				goto failed;
			goto retry;
		}
		if(joined && joined->getJoined())
			Part();
		//stopChannel(EV_ASYNC);
		if(!trunkSignal(TRUNK_SIGNAL_TIMEOUT))
			trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &OH323Trunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
retry:
		TimerPort::endTimer();
		//if(softJoin == NULL)
		//	softJoin = new AudioBuffer();

		if(data.join.trunk)
		{
			slog(Slog::levelDebug) << "data join trunk is there" << endl;
			jevent.id = TRUNK_JOIN_TRUNKS;
			jevent.parm.trunk = this;
			jevent.parm.ok = data.join.local;
				
			data.join.trunk->postEvent(&jevent);
			if(!joined && data.join.count)
			{
	                      	enterState("sync");
                               	status[tsid] = 's';
                               	setTimer(keythreads.getResetDelay());
                                	return true;
			}
			if(!joined && jevent.parm.trunk == NULL)
			{
failed:
				if(!trunkSignal(TRUNK_SIGNAL_ERROR))
					trunkSignal(TRUNK_SIGNAL_STEP);
				setSymbol(SYM_ERROR, "join-failed");
				handler = &OH323Trunk::stepHandler;
				return true;
			}
			data.join.count = 0;
			enterState("join");
			status[tsid] = 'j';
			Join((Trunk*)joined);
			return true;
		}
		else
		{
			data.join.count = 0;
			Trunk::setDTMFDetect();
			enterState("wait");
			status[tsid] = 'w';
		}
		if(data.join.wakeup)
			TimerPort::setTimer(data.join.wakeup);
		return true;
	case TRUNK_AUDIO_IDLE:
		return true;
	case TRUNK_PART_TRUNKS:
		stopChannel(AUDIO_BOTH);
		TimerPort::endTimer();
		if(data.join.hangup)
		{
			if(!trunkSignal(TRUNK_SIGNAL_CANCEL))
				trunkSignal(TRUNK_SIGNAL_HANGUP);
		}
		else
			if(!trunkSignal(TRUNK_SIGNAL_CANCEL))
				Trunk::error("join-parted");
		handler = &OH323Trunk::stepHandler;
		return true;
	case TRUNK_SIGNAL_JOIN:
		if(joined)
			return false;

		if(event->parm.error)
			setSymbol(SYM_ERROR, event->parm.error);
		else
			event->parm.error = "";

		if(!trunkSignal(TRUNK_SIGNAL_EVENT))
			trunkSignal(TRUNK_SIGNAL_HANGUP);
		else
		{
			setSymbol(SYM_EVENTID, "join");
			setSymbol(SYM_EVENTMSG, event->parm.error);
		}
		handler = &OH323Trunk::stepHandler;
		return true;
	case TRUNK_JOIN_TRUNKS:
		slog(Slog::levelDebug) << id << ": TRUNK_JOIN_TRUNKS" << endl;
		if(joined)
			return false;

		if(data.join.waiting)
		{
			if(event->parm.trunk != data.join.waiting)
				return false;
		}

		data.join.count = 0;
		//stopChannel(EV_ASYNC);
		TimerPort::endTimer();
		
		if(Join(event->parm.trunk))
		{
			((OH323Trunk *)event->parm.trunk)->Join((Trunk*)this);
			cp = joined->getSymbol(SYM_GID);
			setSymbol(SYM_JOINID, strchr(cp, '-'));
			return true;
		}

		trunkSignal(TRUNK_SIGNAL_ERROR);
		setSymbol(SYM_ERROR, "join-failed");
		handler = &OH323Trunk::stepHandler;
		return false;
	}
	return false;
}

#ifdef CCXX_NAMESPACES
};
#endif

