// Copyright (C) 2000-2003 Open Source Telecom Corporation.
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

OH323Timer::OH323Timer(OH323Trunk *parent) : Thread(keythreads.priService()), TimerPort(), Mutex()
{
	trunk = parent;
}

OH323Timer::~OH323Timer()
{
}

void OH323Timer::run(void)
{
	int timeout;
	TrunkEvent event;
	struct pollfd pfd;

	for(;;)
	{
		//slog(Slog::levelDebug) << "OH323Timer::run()" << endl;
		Thread::sleep(10);
		enterMutex();
		timeout = TimerPort::getTimer();
		if(!timeout)
		{
			slog(Slog::levelDebug) << "OH323Timer::run(): Timer Expired" << endl;
			event.id = TRUNK_TIMER_EXPIRED;
			TimerPort::endTimer();
			//trunk->postEvent(&event);
			//leaveMutex();
			continue;
		}

		leaveMutex();

		/*if(timeout > 0)
		{
			pfd.fd = evbuf[0];
			pfd.events = POLLIN | POLLRDNORM;
			if(::poll(&pfd, 1, timeout) < 1)
			{
				event.id = TRUNK_TIMER_EXPIRED;
				//trunk->postEvent(&event);
				continue;
			}
		} */
	}
}

#ifdef	CCXX_NAMESPACES
};
#endif     
