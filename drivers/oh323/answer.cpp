// Copyright (C) 2000-2003 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

bool OH323Trunk::answerHandler(TrunkEvent *event)
{
	//slog(Slog::levelDebug) << "OH323Trunk::answerHandler(event->id=" <<
	//	event->id << ")" << endl;
	switch(event->id)
	{
	case TRUNK_STOP_DISCONNECT:
	case TRUNK_CALL_RESTART:
	case TRUNK_TIMER_EXPIRED:
		endTimer();
		Trunk::flags.dsp = DSP_MODE_INACTIVE;
		if(!trunkEvent("answer:failed"))
			trunkSignal(TRUNK_SIGNAL_HANGUP);
		handler = &OH323Trunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		setSymbol(SYM_TONE, "none");
		if(Trunk::flags.offhook)
		{
			trunkSignal(TRUNK_SIGNAL_STEP);
			handler = &OH323Trunk::stepHandler;
			return true;
		}
		status[tsid] = 'a';
		enterState("answer");
		//endTimer();
		Trunk::flags.dsp = DSP_MODE_INACTIVE;

		if(!answerCall())
		{
                	endTimer();
                	Trunk::flags.dsp = DSP_MODE_INACTIVE;
                	trunkSignal(TRUNK_SIGNAL_HANGUP);
                	handler = &OH323Trunk::stepHandler;
		}
		return true;
	case TRUNK_CALL_ANSWERED:
		Trunk::flags.offhook = true;
		trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &OH323Trunk::stepHandler;
		status[tsid] = 'i';
		return true;
	}
	return false;
}

#ifdef CCXX_NAMESPACES
};
#endif

