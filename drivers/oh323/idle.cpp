// Copyright (C) 2000-2003 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

bool OH323Trunk::idleHandler(TrunkEvent *event)
{
	char script[256];
	int argc = 0;
	const char *start = NULL;
	const char *data, *value;
	TrunkGroup *grp = NULL;
	char *sp;
	int ctrl;
	unsigned chk = 0;
	char **argv;
	Request *req;

	switch(event->id)
	{
	case TRUNK_ENTER_STATE:
		synctimer = exittimer = 0;
		enterState("idle");
		ScriptSymbol::purge();
		Trunk::flags.dsp = DSP_MODE_INACTIVE;
		tgi.pid = 0;
		digits = 0;
		switch(Trunk::flags.trunk)
		{
		case TRUNK_MODE_OUTGOING:
			group->decOutgoing();
			break;
		case TRUNK_MODE_INCOMING:
			group->decIncoming();
		}
		Trunk::flags.trunk = TRUNK_MODE_INACTIVE;
		Trunk::flags.ready = true;
		Trunk::flags.offhook = false;
		status[tsid] = '-';
		time(&idletime);
		rings = 0;
		endTimer();
		value = group->chkRequest();
		if(value)
			chk = atoi(value);
		if(!chk && stricmp(value, "hangup"))
			return true;
		if(chk < group->getReady())
			chk = group->getReady();
		setTimer(chk);
		endTimer();
		return true;
	case TRUNK_STOP_DISCONNECT:
		return true;
	case TRUNK_TIMER_EXPIRED:
		Trunk::flags.ready = true;
		req = group->getRequest();
		if(req)
		{
			setConst(SYM_PARENT, req->getParent());
			argv = req->getList();
			start = *(argv++);
			if(attach(start))
			{
				Trunk::flags.trunk = TRUNK_MODE_OUTGOING;
				setList(argv);
				endTimer();
				Trunk::flags.ready = false;
				syncParent("start:running");
				handler = &OH323Trunk::stepHandler;
				group->incOutgoing();
				delete req;
				return true;
			}
			syncParent("start:failed");
			ScriptSymbol::purge();
			delete req;
		}
		chk = atoi(group->chkRequest());
		if(chk)
			setTimer(chk);
		return true;
	case TRUNK_START_SCRIPT:
		Trunk::flags.trunk = TRUNK_MODE_OUTGOING;
	case TRUNK_RING_START:
		start = event->parm.argv[0];
		if(start)
		{
			if(strchr(start, '='))
				start = group->getSchedule(script);
			else
				++argc;
		}
		if(Trunk::flags.trunk == TRUNK_MODE_INACTIVE)
			Trunk::flags.trunk = TRUNK_MODE_INCOMING;
		rings = 0;
		if(!start)
			start = group->getSchedule(script);
		if(attach(start))
		{
			setList(&event->parm.argv[argc]);
			Trunk::flags.ready = false;
			endTimer();
			syncParent("start:running");
			if(Trunk::flags.trunk == TRUNK_MODE_OUTGOING)
			{
				handler = &OH323Trunk::stepHandler;
				group->incOutgoing();
			}
			else
			{
				handler = &OH323Trunk::stepHandler;
				group->incIncoming();
			}
			return true;
		}
		syncParent("start:failed");
		ScriptSymbol::purge();
		Trunk::flags.trunk = TRUNK_MODE_INACTIVE;
		getName(script);
		slog(Slog::levelError) << script << ": " << start << ": cannot start" << endl;
		return true;
	case TRUNK_RING_REDIRECT:
		start = group->getRedirect(event->parm.argv[0], script);
		rings = 0;
		if(attach(start))
		{
			setConst(SYM_REDIRECT, event->parm.argv[0]);
			setList(&event->parm.argv[1]);
			Trunk::flags.ready = false;
			Trunk::flags.trunk = TRUNK_MODE_INCOMING;
			endTimer();
			handler = &OH323Trunk::stepHandler;
			group->incIncoming();		
			return true;
		}
		ScriptInterp::purge();
		getName(script);
		slog(Slog::levelError) << script << ": " << event->parm.argv[0] << ": unable to redirect" << endl;
		return true;
	case TRUNK_RINGING_ON:
	case TRUNK_CALL_OFFER:
		if(event->parm.data)
			callToken = (char *)event->parm.data;

		if(!group->getAnswer())
			return true;
		rings = group->getAnswer();
		Trunk::flags.trunk = TRUNK_MODE_INCOMING;
		group->incIncoming();
		Trunk::flags.ready = false;
		endTimer();
		handler = &OH323Trunk::ringHandler;
		return true;
	//case TRUNK_MAKE_BUSY:
	//case TRUNK_MAKE_STANDBY:
	//	handler = &DialogicTrunk::busyHandler;
	//	return true;
	//case TRUNK_MAKE_IDLE:
	//	return true;
	//case TRUNK_DEVICE_BLOCKED:
	//	return true;
	//case TRUNK_DEVICE_UNBLOCKED:
	//	return waitCall();
	}
	return false;
}

#ifdef CCXX_NAMESPACES
};
#endif

