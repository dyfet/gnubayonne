// Copyright (C) 2000-2002 Open Source Telecom Corporation.
// 
// Author: Mark Lipscombe
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <ivrconfig.h>
#include <server.h>
#include <cc++/audio.h>

#define P_HAS_SEMAPHORES
#define P_USE_PRAGMA
#define P_SSL 1
#define P_PTHREADS 1
#define PHAS_TEMPLATES
#define PTRACING 1
#define HAS_OSS

#if __BYTE_ORDER == __LITTLE_ENDIAN
  #define PBYTE_ORDER PLITTLE_ENDIAN
#else
  #define PBYTE_ORDER PBIG_ENDIAN
#endif

//typedef unsigned int PUInt32b;
//typedef unsigned short PUInt16b;

#define	P_USE_PRAGRMA
#define	PHAS_TEMPLATES
#define	P_PLATFORM_HAS_THREADS
#define	P_PTHREADS 1
#define	P_HAS_SEMAPHORES
#define	NDEBUG

#include <ptlib.h>
#include <h323.h>
#include <h323pdu.h>

#include <ptclib/pwavfile.h>
#include <ptclib/delaychan.h>

#define	NDEBUG

#ifdef	CCXX_NAMESPACES
namespace ost {
#endif

#define	AUDIO_TRANSMIT	0
#define	AUDIO_RECEIVE	1
#define AUDIO_BOTH	2

class OH323Trunk;
class OH323Driver;
extern OH323Driver oh323ivr;

typedef	bool (OH323Trunk::*trunkhandler_t)(TrunkEvent *event);

class OH323EndPoint : public H323EndPoint
{
	PCLASSINFO(OH323EndPoint, H323EndPoint);

public:
	OH323EndPoint();
	~OH323EndPoint();

	BOOL Initialise();

	// Connection establishment overrides
	virtual H323Connection * CreateConnection(unsigned callRef, void *userData, H323Transport *transport, H323SignalPDU *setupPDU);
	virtual void OnConnectionCleared(H323Connection & connection, const PString & token);

	// Setup overrides
	virtual void SetEndpointTypeInfo(H225_EndpointType & info) const;
	virtual void SetVendorIdentifierInfo(H225_VendorIdentifier & info) const;
	virtual void SetH221NonStandardInfo(H225_H221NonStandard & info) const;
	virtual H323Gatekeeper *CreateGatekeeper(H323Transport *);

	// Call control
	virtual BOOL OnConnectionForwarded(H323Connection & connection, const PString & forwardParty,
					   const H323SignalPDU &);

	bool MakeCall(const PString & dest, PString & token, unsigned int *callReference,
			H323Capability *cap, char *callerId, void *userData);

	const char *callToken;
	//PString callToken;
private:
	void SetCapabilities(void);

	unsigned trunks;
};

class OH323Audio
{
public:
	OH323Audio() { };
	~OH323Audio() { };

	virtual int getBuffer(void *buf, unsigned len)
		{ return -1; };
	virtual int putBuffer(const void *buf, unsigned len)
		{ return -1; };
};

class URLAudioChannel : public OH323Audio
{
public:
	URLAudioChannel(URLAudio *audio);
	~URLAudioChannel();

	int getBuffer(void *buf, unsigned len);
	int putBuffer(const void *buf, unsigned len);

private:
	URLAudio *audio;
};

class ToneChannel : public OH323Audio
{
public:
	ToneChannel(phTone *t);
	~ToneChannel();

	int getBuffer(void *buf, unsigned len);

private:
	phTone *tone;
	unsigned char *buffer;
	unsigned long samples;
};

class JoinChannel : public OH323Audio
{
public:
	JoinChannel(Trunk *srctrk, Trunk *dsttrk);
	~JoinChannel();

	int getBuffer(void *buf, unsigned len);
	int putBuffer(const void *buf, unsigned len);

private:
	Trunk *src;
	Trunk *dst;
};

class ASRChannel : public OH323Audio
{
public:
	ASRChannel(int asrfd);
	~ASRChannel();

	int putBuffer(const void *buf, unsigned len);

private:
	int fd;
};

class OH323Channel : public PChannel, private Mutex
{
	PCLASSINFO(OH323Channel, PChannel);
public:
	friend class OH323Trunk;
	friend class OH323Connection;

	OH323Channel(OH323Trunk *parent);
	~OH323Channel();

	DTMFDetect *dtmf;

	BOOL Read(void *buf, PINDEX len);
	BOOL Write(const void *buf, PINDEX len);
	BOOL Close();
	BOOL IsOpen();

	bool hasTransmit(void)
		{ if(transmit)
		 	return true;
		  else
			return false;
		};

	bool hasReceive(void)
		{ if(receive)
			return true;
		  else
			return false;
		};

protected:
	void attachTransmitter(OH323Audio *channel)
		{ transmit = channel; };
	void attachReceiver(OH323Audio *channel)
		{ receive = channel; };
	void stop(int channelType);

private:
	OH323Trunk *trunk;
	OH323Audio *transmit;
	OH323Audio *receive;
	bool stopTransmit, stopReceive;
	BOOL closed;
	PAdaptiveDelay readDelay;
	PAdaptiveDelay writeDelay;
};

class OH323Timer : private Thread, private Mutex, public TimerPort
{
public:
	OH323Timer(OH323Trunk *parent);
	~OH323Timer();

	void run();

private:
	friend class OH323Trunk;
	int evbuf[2];
	OH323Trunk *trunk;
};

class OH323Connection : public H323Connection
{
	PCLASSINFO(OH323Connection, H323Connection);
	friend class OH323Trunk;
	friend class OH323EndPoint;
	friend class OH323Driver;

public:
	OH323Connection(OH323EndPoint & ep, unsigned ref, unsigned tid);
	~OH323Connection();

	virtual BOOL OnIncomingCall(const H323SignalPDU &, H323SignalPDU &);
	virtual H323Connection::AnswerCallResponse OnAnswerCall(const PString &,
							const H323SignalPDU &,
							H323SignalPDU &);
	virtual void OnEstablished();
	virtual void OnCleared();
	virtual BOOL OpenAudioChannel(BOOL isEncoding, unsigned bufferSize, H323AudioCodec & codec);
	virtual BOOL OnStartLogicalChannel(H323Channel &);
	virtual BOOL OnSendSignalSetup(H323SignalPDU & setupPDU);

	virtual void OnUserInputString(const PString &);
	virtual void OnUserInputTone(char tone, unsigned duration, unsigned channel, unsigned rtpts);
	virtual void CleanUpOnCallEnd();

	// new functions
	void muteTransmit(BOOL mute);
	void muteReceive(BOOL mute);

	void setForward(void)
		{ isForwarding = true; };
	unsigned id;

private:
	OH323EndPoint *endpoint;
	H323AudioCodec *outCodec, *inCodec;
	OH323Channel *channel;
	OH323Audio *transmit;
	OH323Audio *receive;
	OH323Trunk *trunk;

	bool isForwarding;

	bool transmitUp, receiveUp;
};

class OH323Trunk : private TimerPort,
	private Trunk,
	public AudioService,
	public URLAudio
{
public:
	OH323Trunk(OH323EndPoint & ep, unsigned ref, unsigned tid);
	~OH323Trunk();

protected:
	const char *getDefaultEncoding(void)
		{return "pcm";};

private:
	friend class OH323Driver;
	friend class OH323EndPoint;
	friend class OH323Channel;
	friend class OH323Connection;

	OH323EndPoint endpoint;
	OH323Connection *conn;
	PString callToken;

	trunkhandler_t handler;
	time_t lastring;
	int ts;

	bool _stopping_state;
	bool t_active;
	unsigned frames;
	bool isOutgoing;

	void initSyms(void);
	bool postEvent(TrunkEvent *evt);
	void putEvent(TrunkEvent *evt);
	void timerTick(void);
	void exit(void);
	void getName(char *buffer);
	unsigned long getIdleTime(void);
	void trunkStep(trunkstep_t);

	bool scrJoin(void);
	bool scrWait(void);

	bool lockConnection(void);
	void releaseConnection(void);
	bool isConnected(void);

	//void run(void);

	// Audio controls
	char *getContinuation(void);
	void endRecord();
	void endPlay();
	bool Join(Trunk *trk);
	void Part();
	bool stopChannel(int channelType);

	// Call control
	void attachConnection(OH323Connection *connection);
	OH323Connection *attachConnection(OH323EndPoint & ep, unsigned ref);
	bool answerCall(void);
	bool hangupCall(void);
	bool makeCall(char *digit, char *clid);

	// State handlers
	bool idleHandler(TrunkEvent *event);
	bool stepHandler(TrunkEvent *event);
	bool ringHandler(TrunkEvent *event);
	bool answerHandler(TrunkEvent *event);
	bool playHandler(TrunkEvent *event);
	bool recordHandler(TrunkEvent *event);
	bool sleepHandler(TrunkEvent *event);
	bool threadHandler(TrunkEvent *event);
	bool hangupHandler(TrunkEvent *event);
	bool collectHandler(TrunkEvent *event);
	bool loadHandler(TrunkEvent *event);
	bool joinHandler(TrunkEvent *event);
	bool dialHandler(TrunkEvent *event);
	bool toneHandler(TrunkEvent *event);
	bool listenHandler(TrunkEvent *event);
};

class OH323Config : public Keydata
{
public:
	OH323Config();

	tpport_t getFirstPort(void)
		{return atoi(getLast("first"));};

	unsigned getRTPCount(void)
		{return atoi(getLast("count"));};

	unsigned getRTPInc(void)
		{return atoi(getLast("inc"));};

	unsigned getPort(void)
		{return atoi(getLast("port"));};

	InetAddress getBindAddress(void);

	WORD getSignalPort(void)
		{return atoi(getLast("port"));};

	bool getBool(const char *name);
};

class OH323GatekeeperNAT : public H323Gatekeeper
{
	PCLASSINFO(OH323GatekeeperNAT, H323Gatekeeper);

public:
	OH323GatekeeperNAT(H323EndPoint &ep, H323Transport *trans);
	~OH323GatekeeperNAT();

	// overrides from H323Gatekeeper
	virtual BOOL OnReceiveRegistrationConfirm(const H225_RegistrationConfirm &rcf);
	virtual BOOL OnReceiveUnregistrationRequest(const H225_UnregistrationRequest &);
	virtual void OnSendRegistrationRequest(H225_RegistrationRequest &);
	virtual void OnSendUnregistrationRequest(H225_UnregistrationRequest &);

	virtual BOOL MakeRequest(Request &);
	virtual void DetectIncomingCall();

protected:
	virtual void StopDetecting();
	bool SendInfo(int state);

	class DetectIncomingCallThread : public PThread
	{
		PCLASSINFO(DetectIncomingCallThread, PThread);

	public:
		DetectIncomingCallThread(OH323GatekeeperNAT *gk)
			: PThread(1000, NoAutoDeleteThread), gatekeeper(gk) { Resume(); };
		void Main() { gatekeeper->DetectIncomingCall(); };
	private:
		OH323GatekeeperNAT *gatekeeper;
	};

	bool isMakeRequestCalled;
	DetectIncomingCallThread *detectorThread;
	PTCPSocket *incomingTCP, *outgoingTCP;
	PMutex threadMutex, socketMutex;
	PIPSocket::Address gkip;
	WORD gkport;
	bool isDetecting;

private:
	static bool SendTPKT(PTCPSocket *sender, const PBYTEArray &buf);
	static bool ForwardMesg(PTCPSocket *receiver, PTCPSocket *sender);
	static void DoForwarding(PTCPSocket *incomingTCP, PTCPSocket *outgoingTCP);
};

class OH323Driver : public Driver, public OH323Config, public Thread, public Mutex
{

	//PWLib entry point
	class PWLibProcess : public PProcess
	{
		PCLASSINFO(PWLibProcess, PProcess)

		PWLibProcess() : PProcess("GNU", "Bayonne", 1, 3, ReleaseCode, 0)
		{
		};

		public:
			void Main() { }
	
	} pwlibProcess;

protected:
	OH323EndPoint * endpoint;

private:
	friend class OH323EndPoint;
	friend class OH323Connection;
	friend class OH323Trunk;
	OH323Trunk **ports;
	//OH323Trunk **trunks;
	int port_count;

public:
	OH323Driver();
	~OH323Driver();

	int start(void);
	void stop(void);
	void run(void);

	unsigned getTrunkCount(void)
		{return port_count;};

	Trunk *getTrunkPort(int id);
	Trunk *getOutboundTrunk(int id);
	aaScript *getScript(void);

	unsigned getCaps(void)
		{return capDaemon | capIP | capJoin | capListen;};

	char *getName(void)
		{return "H323";};
};

#ifdef	CCXX_NAMESPACES
};
#endif
