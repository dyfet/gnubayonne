// Copyright (C) 2000-2003 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

bool OH323Trunk::recordHandler(TrunkEvent *event)
{
	char *fn;
	struct tm *dt, tbuf;
	struct stat ino;
	char buffer[32];
	const char *ext;
	const char *fmt = data.record.encoding;
	Audio::Info info;
	unsigned mask;
	int rtn;

	slog(Slog::levelDebug) << "OH323Trunk::recordHandler(event->id=" << event->id << ")" << endl;	
	switch(event->id)
	{
	case TRUNK_STOP_STATE:
		_stopping_state = true;
	case TRUNK_TIMER_EXPIRED:
		endTimer();
		if(!stopChannel(AUDIO_RECEIVE))
			handler = &OH323Trunk::stepHandler;
		return true;
	case TRUNK_AUDIO_IDLE:
		endTimer();
		handler = &OH323Trunk::stepHandler;
		if(_stopping_state == false)
		{
			trunkSignal(TRUNK_SIGNAL_STEP);
		}
		endRecord();
		return true;
	case TRUNK_DTMF_KEYUP:
		mask = (1 << event->parm.dtmf.digit);
		if(!(mask & data.record.term))
			return false;
		if(!stopChannel(AUDIO_RECEIVE))
			handler = &OH323Trunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		enterState("record");
		status[tsid] = 'r';
		_stopping_state = false;
		setTimer(data.record.timeout);
		
		Trunk::setDTMFDetect();
		getName(buffer);

		ext = strrchr(data.record.name, '/');
		if(!ext)
			ext = data.record.name;

		ext = strrchr(ext, '.');
		
		info.format = riff;
		info.order = __LITTLE_ENDIAN;
		info.encoding = pcm16Mono;
		info.order = 0;
		info.annotation = (char *)data.record.annotation;
		info.rate = 8000;

		if(!ext)
		{
			ext = data.record.extension;
			strcat(data.record.name, ext);
		}

		if(!fmt)
			fmt = "pcm";

		URLAudio::close();

		if(stricmp(fmt, "pcm") && stricmp(fmt, "raw") && stricmp(fmt, "linear"))
		{
			setSymbol(SYM_RECORDED, "0");
			setSymbol(SYM_OFFSET, "0");
			setSymbol(SYM_ERROR, "record-failed");
			slog(Slog::levelError) << buffer << ": unsupported encoding requested" << endl;			
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &OH323Trunk::stepHandler;
			return true;			
		}

		if(data.record.offset != (unsigned long)-1)
		{
			open(data.record.name);
			setPosition(data.record.offset);
		}
		else if(data.record.append)
			open(data.record.name);
		else
			create(data.record.name, &info);

		setSymbol(SYM_RECORDED, "0");

		if(!isOpen())
		{
			slog(Slog::levelError) << data.record.name << ": cannot open" << endl;
			setSymbol(SYM_OFFSET, "0");
			setSymbol(SYM_ERROR, "record-failed");
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &OH323Trunk::stepHandler;
			return true;
		}

		if(data.record.append)
			setPosition();

		dt = localtime_r(&ino.st_ctime, &tbuf);
		sprintf(buffer, "%04d%02d%02d,%02d%02d%02d",
			dt->tm_year + 1900, dt->tm_mon + 1, dt->tm_mday,
			dt->tm_hour, dt->tm_min, dt->tm_sec);
		setSymbol(SYM_CREATED, buffer);
		fn = getAnnotation();
		if(fn)
			setSymbol(SYM_ANNOTATION, fn);
		else
			setSymbol(SYM_ANNOTATION, "");

		if(data.record.info)
		{
			sprintf(buffer, "%ld", getPosition());
			setSymbol(SYM_OFFSET, buffer);
		}

		if(lockConnection())
		{
			conn->receive = new URLAudioChannel((URLAudio *)this);
			conn->channel->attachReceiver(conn->receive);
			releaseConnection();
		}
		else
		{
			setSymbol(SYM_ERROR, "record-failed");
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &OH323Trunk::stepHandler;
		}
		return true;
	}
	return false;
}

#ifdef CCXX_NAMESPACES
};
#endif

