// Copyright (C) 2000-2003 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

void OH323Trunk::trunkStep(trunkstep_t step)
{
	TrunkEvent event;

	if(handler != &OH323Trunk::stepHandler)
		return;

	event.id = TRUNK_MAKE_STEP;
	event.parm.step = step;
	stepHandler(&event);
}

bool OH323Trunk::stepHandler(TrunkEvent *event)
{
	Trunk *alt = ctx;

	slog(Slog::levelDebug) << "OH323Trunk::stepHandler(event->id=" << 
		event->id << ")" << endl;
	switch(event->id)
	{
	case TRUNK_MAKE_STEP:
		if(idleHangup())
			return true;
		slog(Slog::levelDebug) << "OH323Trunk::stepHandler(event->parm.step=" <<
			event->parm.step << ")" << endl;
		switch(event->parm.step)
		{
		case TRUNK_STEP_PLAY:
			handler = &OH323Trunk::playHandler;
			return true;
		case TRUNK_STEP_THREAD:
			handler = &OH323Trunk::threadHandler;
			return true;
		case TRUNK_STEP_SLEEP:
			handler = &OH323Trunk::sleepHandler;
			return true;
		case TRUNK_STEP_ANSWER:
			handler = &OH323Trunk::answerHandler;
			return true;
		case TRUNK_STEP_HANGUP:
			handler = &OH323Trunk::hangupHandler;
			return true;
		case TRUNK_STEP_COLLECT:
			handler = &OH323Trunk::collectHandler;
			return true;
		case TRUNK_STEP_RECORD:
			handler = &OH323Trunk::recordHandler;
			return true;
		case TRUNK_STEP_JOIN:
		case TRUNK_STEP_SOFTJOIN:
			handler = &OH323Trunk::joinHandler;
			return true;
		case TRUNK_STEP_DIALXFER:
			handler = &OH323Trunk::dialHandler;
			return true;
		case TRUNK_STEP_TONE:
			handler = &OH323Trunk::toneHandler;
			return true;
		case TRUNK_STEP_LISTEN:
			handler = &OH323Trunk::listenHandler;
			return true;
		/*case TRUNK_STEP_PLAYWAIT:
			handler = &DialogicTrunk::playwaitHandler;
			return true;
		case TRUNK_STEP_ACCEPT:
			handler = &DialogicTrunk::acceptHandler;
			return true;
		case TRUNK_STEP_REJECT:
			handler = &DialogicTrunk::rejectHandler;
			return true;
		case TRUNK_STEP_FLASH:
			handler = &DialogicTrunk::flashonHandler;
			return true;*/
#ifdef	XML_SCRIPTS
		case TRUNK_STEP_LOADER:
			handler = &OH323Trunk::loadHandler;
			return true;
#endif
		}
		//slog(Slog::levelDebug) << "OH323Trunk::stepHandler()" << endl;
		return false;
	case TRUNK_SERVICE_SUCCESS:
	case TRUNK_SERVICE_FAILURE:
		endTimer();
		//setTimer(keythreads.getResetDelay());
		return true;
	case TRUNK_STOP_STATE:
		//stopChannel(EV_ASYNC);
	case TRUNK_TIMER_EXPIRED:
		Trunk::flags.reset = false;
		stopServices();
	case TRUNK_ENTER_STATE:
		if(event->id == TRUNK_ENTER_STATE)
		{
			//conn->channel->stop(AUDIO_BOTH);
			//URLAudio::close();
		}
		if(Trunk::flags.offhook)
		{
			if(Trunk::flags.trunk == TRUNK_MODE_INCOMING)
				status[tsid] = 'i';
			else
				status[tsid] = 'o';
		}
		else
			status[tsid] = 's';
		Trunk::setDTMFDetect();
		endTimer();
		/*if(Trunk::flags.reset || thread)
		{
			enterState("reset");
			if(thread)
				setTimer(thread->stop());
			else
				setTimer(keythreads.getResetDelay());
			return true;
		}*/
		strcpy(numbers[5].sym.data, "step");
		debug->debugStep(this, getScript());
		monitor->monitorStep(this, getScript());
		if(alt != this)
			alt->enterMutex();
		scriptStep();
		if(alt != this)
			alt->leaveMutex();

		if(handler == &OH323Trunk::stepHandler)
		{
			//slog(Slog::levelDebug) << "OH323Trunk::stepHandler(): Next Step at " << 
			//	keythreads.getStepDelay() << endl;
			setTimer(keythreads.getStepDelay());
		}

		return true;
	}
	return false;
}

#ifdef CCXX_NAMESPACES
};
#endif
