// Copyright (C) 2000-2003 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

bool OH323Trunk::toneHandler(TrunkEvent *event)
{
	switch(event->id)
	{
	case TRUNK_STOP_STATE:
		TimerPort::endTimer();
		_stopping_state = true;
		if(!stopChannel(AUDIO_TRANSMIT))
			handler = &OH323Trunk::stepHandler;
		return true;
	case TRUNK_AUDIO_IDLE:
	case TRUNK_TONE_IDLE:
		if(--data.tone.loops && !_stopping_state)
		{
			TimerPort::setTimer(data.tone.wakeup);
			if(lockConnection())
			{
				conn->transmit = new ToneChannel(getTone());
				conn->channel->attachTransmitter(conn->transmit);
				releaseConnection();
				return true;
			}
		}
		TimerPort::endTimer();
		if(!_stopping_state)
			trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &OH323Trunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		enterState("tone");
		status[tsid] = 't';
		_stopping_state = false;
		if(data.tone.wakeup)
			TimerPort::setTimer(data.tone.wakeup);

		if(lockConnection())
		{
			conn->transmit = new ToneChannel(getTone());
			conn->channel->attachTransmitter(conn->transmit);
			releaseConnection();
		}
		else
		{
			setSymbol(SYM_ERROR, "tone-failed");
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &OH323Trunk::stepHandler;
		}

		return true;
	case TRUNK_TIMER_EXPIRED:
		if(--data.tone.loops)
		{
			TimerPort::setTimer(data.tone.wakeup);
			return true;
		}
		TimerPort::endTimer();
		Trunk::setDTMFDetect();
		trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &OH323Trunk::stepHandler;
		return true;
	}
	return false;
}

#ifdef CCXX_NAMESPACES
};
#endif

