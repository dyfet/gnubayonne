// Copyright (C) 2000-2003 Open Source Telecom Corporation.
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

OH323Config::OH323Config() :
Keydata("/bayonne/h323")
{
	static Keydata::Define defkeys[] = {
	{"first", "3128"},
	{"count", "32"},
	{"inc", "4"},
	{"interface", "*"},
	{"port","1720"},
	{"usegk","0"},
	{"gatekeeperaddress",""},
	{"gatekeeperid",""},
	{"gatekeeperpassword",""},
	{"gatewayprefixes",""},
	{"username","Bayonne"},
	{"uimode","rfc2833"},
	{"faststart","1"},
	{"h245tunneling","1"},
	{"h245insetup","1"},
	{"forwarding","1"},
	{"inbanddtmf","1"},
	{"rtpmin","0"},
	{"rtpmax","0"},
	{"tcpmin","0"},
	{"tcpmax","0"},
	{"udpmin","0"},
	{"udpmax","0"},
	{NULL, NULL}};

	if(isFHS())
		load("/drivers/h323");

	load(defkeys);
}

InetAddress OH323Config::getBindAddress(void)
{
	const char *cp = getLast("bind");
	if(!cp)
		cp = getLast("address");
	if(!cp)
		cp = getLast("interface");

	return InetAddress(cp);
}

bool OH323Config::getBool(const char *name)
{
	const char *cp = getLast(name);
	if(atoi(cp) == 1 || !stricmp(cp, "on"))
		return true;
	else
		return false;
}

OH323Driver::OH323Driver() :
Driver()
{
	port_count = oh323ivr.getRTPCount();

	ports = NULL;
	groups = NULL;
	
	endpoint = new OH323EndPoint;

	ports = new OH323Trunk *[port_count];
	groups = new TrunkGroup *[port_count];

	if(ports)
		memset(ports, 0, sizeof(OH323Trunk *) * port_count);

	if(groups)
		memset(groups, 0, sizeof(TrunkGroup *) * port_count);

#ifdef PTRACING
	//PTrace::Initialise(1, NULL, PTrace::Blocks | PTrace::Timestamp | PTrace::Thread |
	//			PTrace::FileAndLine);
	//slog(Slog::levelInfo) << "H323 tracing enabled" << endl;
#endif
	
	slog(Slog::levelInfo) << "H323 trunk driver loaded; capacity=" << port_count << endl;
}

OH323Driver::~OH323Driver()
{
	stop();
	if(ports)
		delete[] ports;

	if(groups)
		delete[] groups;
}

int OH323Driver::start(void)
{
	TrunkGroup *grp = getGroup("internet");
	unsigned idx = 0;

	int count = oh323ivr.getRTPCount();

	if(active)
	{
		slog(Slog::levelError) << "oh323 driver already started" << endl;
		return 0;
	}

	slog(Slog::levelInfo) << "oh323 driver starting..." << endl;

	while(idx < port_count)
	{
		if(grp && groups[idx] == getGroup(NULL))
			groups[idx] = grp;
		++idx;
	}	

	if (!endpoint->Initialise())
	{
		slog(Slog::levelError) << "oh323: the endpoint failed to initialise" << endl;
		return 0;
	}
	
	active = true;

	Thread::start();
	return count;
}

void OH323Driver::stop(void)
{
	if(!active)
		return;

	if(ports)
		memset(ports, 0, sizeof(OH323Trunk *) * port_count);

	active = false;
	slog(Slog::levelInfo) << "oh323 driver stopping..." << endl;
}

void OH323Driver::run(void)
{
	int i;
	int timer;
	TrunkEvent event;

	if(!active)
		return;

	setCancel(cancelImmediate);

	for(;;)
	{
		//Thread::yield();
		sleep(35);
		for(i = 0; i < port_count; i++)
		{
			if(ports[i] == NULL)
				continue;

			if(!ports[i]->getTimer())
			{
				event.id = TRUNK_TIMER_EXPIRED;
				ports[i]->postEvent(&event);
			}
		}
	}
}

Trunk *OH323Driver::getTrunkPort(int id)
{
	if(id < 0 || id >= port_count)
		return NULL;

	if(!ports)
		return NULL;

	if(ports[id] == NULL)
	{
		// make new trunk
		return NULL;
	}

	return (Trunk *)ports[id];
}

Trunk *OH323Driver::getOutboundTrunk(int id)
{
	if(id < 0 || id >= port_count)
		return NULL;

	if(!ports)
		return NULL;

	if(ports[id] != NULL)
		return NULL;

	ports[id] = new OH323Trunk(*endpoint, NULL, id);

	return (Trunk *)ports[id];
}

OH323Driver oh323ivr;

#ifdef	CCXX_NAMESPACES
};
#endif
