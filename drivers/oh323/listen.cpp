// Copyright (C) 2000-2003 Open Source Telecom Corporation.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

bool OH323Trunk::listenHandler(TrunkEvent *event)
{
	unsigned short mask;
	unsigned len, wc = 1;
	int argc = 0;
	Symbol *sym = (Symbol *)data.listen.save;
	char *fmt, *cp, *wl;

	slog(Slog::levelDebug) << "OH323Trunk::listenHandler(event->id=" << event->id << ")" << endl;

	switch(event->id)
	{
	case TRUNK_ASR_PARTIAL:
		if(asrPartial(event->parm.send.msg))
			goto stop;
		if(!asr.partial)
			return true;
		if((cp = strrchr(event->parm.send.msg, ' ')) != NULL)
		{
			*cp = 0;
			cp = strrchr(event->parm.send.msg, ' ');
		}
		cp = (char *)event->parm.send.msg;
		if(cp && *cp)
		{
			if(*cp == ' ' || *cp == ',')
				++wc;
			++cp;
		}
		if(data.listen.count && wc >= data.listen.count)
			goto setsymbol;
		if((cp = strrchr(event->parm.send.msg, ' ')) != NULL)
			cp++;
		else
			cp = (char*)event->parm.send.msg;

		if(!cp)
		{
			slog(Slog::levelWarning) << "ASR string was unparseable" << endl;
			return true;
		}

		while(data.listen.wordlist[argc])
		{
			if(!stricmp(cp, data.listen.wordlist[argc]))
				goto setsymbol;
			argc++;
		}
		return true;
	case TRUNK_ASR_TEXT:
		if(asrPartial(event->parm.send.msg))
			goto stop;
setsymbol:
		if(sym)
		{
			if(sym->data[0])
				fmt = ",%s";
			else
				fmt = "%s";
			len = sym->flags.size - strlen(sym->data);
		}
		else
			len = 0;
		if(len)
		{
			sym->flags.initial = false;
			snprintf(sym->data + strlen(sym->data), len + 1, 
				fmt, event->parm.send.msg);
			if(sym->flags.commit)
				commit(sym);
		}
		trunkSignal(TRUNK_SIGNAL_STEP);
	case TRUNK_STOP_STATE:
stop:
		endTimer();
		stopChannel(AUDIO_RECEIVE);
		_stopping_state = true;
		return true;
	case TRUNK_AUDIO_IDLE:
		//endTimer();
		//if(!_stopping_state)
		//	trunkSignal(TRUNK_SIGNAL_STEP);
		//handler = &OH323Trunk::stepHandler;
		return true;
	case TRUNK_DTMF_KEYUP:
		mask = (1 << event->parm.dtmf.digit);
		if(!(mask & data.listen.term)) 
			return false;
		if(!stopChannel(AUDIO_RECEIVE))
			handler = &OH323Trunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		_stopping_state = false;
		enterState("listen");
		//flags.dsp = DSP_MODE_VOICE;
		status[tsid] = 'l';
		if(data.listen.term)
			setDTMFDetect(true);
		else
			setDTMFDetect();
		setTimer(data.listen.first);
		if(lockConnection())
		{
			conn->receive = new ASRChannel(asr.fd);
			conn->channel->attachReceiver(conn->receive);
			releaseConnection();
		}
		else
		{
			setSymbol(SYM_ERROR, "listen-failed");
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &OH323Trunk::stepHandler;
		}
		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
