// Copyright (C) 2000-2003 Open Source Telecom Corporation.
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

OH323Channel::OH323Channel(OH323Trunk *parent) : 
	trunk(parent)
{
	receive = NULL;
	transmit = NULL;

	//dtmf = new DTMFDetect();
	dtmf = NULL;

	if(oh323ivr.getBool("inbanddtmf"))
		dtmf = new DTMFDetect();
	slog(Slog::levelDebug) << "OH323AudioChannel::c'tor" << endl;
}

OH323Channel::~OH323Channel()
{
	if(receive)
		delete receive;
	if(transmit)
		delete transmit;
	if(dtmf)
		delete dtmf;
}

BOOL OH323Channel::IsOpen()
{
	slog(Slog::levelDebug) << "OH323Audio::IsOpen()" << endl;
	return TRUE;
}

void OH323Channel::stop(int channelType)
{
	TrunkEvent event;
	bool stopped = false;

	enterMutex();
	if(channelType == AUDIO_TRANSMIT && transmit)
	{
		stopped = true;
		delete transmit;
		transmit = NULL;
	}
	else if(channelType == AUDIO_RECEIVE && receive)
	{
		stopped = true;
		delete receive;
		receive = NULL;
	}
	else
	{
		if(receive)
		{
			stopped = true;
			delete receive;
			receive = NULL;
		}
		if(transmit)
		{
			stopped = true;
			delete transmit;
			transmit = NULL;
		}
	}

	if(stopped)
	{
		event.id = TRUNK_AUDIO_IDLE;
		trunk->postEvent(&event);
	}
	leaveMutex();
}

BOOL OH323Channel::Write(const void *buf, PINDEX len)
{
	TrunkEvent event;
	static unsigned lastDtmf = 0;
	int dtmfFound = 0, i;
	char digits[128] = {0};
	enterMutex();

	if(dtmf)
	{
		lastDtmf += len;
		dtmfFound = dtmf->putSamples((int16_t*)buf, len/2);
		if(dtmfFound && lastDtmf > 1000)
		{
			dtmf->getResult(digits, 64);
			if(digits)
			{
				for(i = 0; i < strlen(digits); i++)
				{
					event.id = TRUNK_DTMF_KEYUP;
					event.parm.dtmf.digit = (int)(digits[i] - '0');
					event.parm.dtmf.duration = 60;
					trunk->postEvent(&event);
				}
				lastDtmf = 0;
			}
		}
	}

	if(receive)
		receive->putBuffer(buf, len);
	leaveMutex();

	lastWriteCount = len;
	writeDelay.Delay(lastWriteCount/16);

	return TRUE;
}

BOOL OH323Channel::Read(void *buf, PINDEX len)
{
	TrunkEvent event;
	int count = 0;

	enterMutex();
	if(transmit)
	{
		count = transmit->getBuffer(buf, len);
		if(count < 1)
			stop(AUDIO_TRANSMIT);
	}
	leaveMutex();

	if(count < len)
	{
		memset((char*)buf+count, 0, len - count);
	}

	lastReadCount = len;
	readDelay.Delay(lastReadCount/16);

	return TRUE;
}

BOOL OH323Channel::Close()
{
	closed = true;
	//if(trunk->isOpen())
	//	trunk->close();
	PChannel::Close();
	return true;
}

URLAudioChannel::URLAudioChannel(URLAudio *audio)
{
	this->audio = audio;
}

URLAudioChannel::~URLAudioChannel()
{
}

int URLAudioChannel::getBuffer(void *buf, unsigned len)
{
	return audio->getLinear((short *)buf, len/2) * 2;
}

int URLAudioChannel::putBuffer(const void *buf, unsigned len)
{
	return audio->putBuffer((void *)buf, len);
}

ToneChannel::ToneChannel(phTone *t)
{
	int i;

	this->tone = t;
	buffer = tone->getSamples();
	samples = tone->getDuration() * 8;

	for(i = 0; i < samples; i++)
		buffer[i] = phTone::ulaw2linear(buffer[i]);
}

ToneChannel::~ToneChannel()
{
}

int ToneChannel::getBuffer(void *buf, unsigned len)
{
	int read = len;
	if(samples < read)
		read = samples;

	if(read > 0)
	{
		memcpy(buf, buffer, read);
		samples -= read;
		buffer += read;
	}
	return read;
}

JoinChannel::JoinChannel(Trunk *srctrk, Trunk *dsttrk)
{
	src = srctrk;
	dst = dsttrk;
}

JoinChannel::~JoinChannel()
{
}

int JoinChannel::getBuffer(void *buf, unsigned len)
{
	unsigned amount = len;
	dst->softJoin->getBuffer((char *)buf, len);
	return amount;
}

int JoinChannel::putBuffer(const void *buf, unsigned len)
{
	unsigned amount = len;
	src->softJoin->putBuffer((char *)buf, len);
	return amount;
}

ASRChannel::ASRChannel(int asrfd)
{
	fd = asrfd;
}

ASRChannel::~ASRChannel()
{
}

int ASRChannel::putBuffer(const void *buf, unsigned len)
{
	return ::write(fd, buf, len);
}

#ifdef	CCXX_NAMESPACES
};
#endif     
