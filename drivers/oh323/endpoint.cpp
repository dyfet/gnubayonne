// Copyright (C) 2000-2003 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

OH323EndPoint::OH323EndPoint()
{
	terminalType = e_GatewayOnly;
	trunks = 0;
}
          
        
OH323EndPoint::~OH323EndPoint()
{
	ClearAllCalls(H323Connection::EndedByLocalUser, true);
}

BOOL OH323EndPoint::Initialise()
{
	PString UserName = oh323ivr.getLast("username");
	int usegk = atoi(oh323ivr.getLast("usegk"));
	unsigned rtpmin = atoi(oh323ivr.getLast("rtpmin"));
	unsigned rtpmax = atoi(oh323ivr.getLast("rtpmax"));
	unsigned tcpmin = atoi(oh323ivr.getLast("tcpmin"));
	unsigned tcpmax = atoi(oh323ivr.getLast("tcpmax"));
	unsigned udpmin = atoi(oh323ivr.getLast("udpmin"));
	unsigned udpmax = atoi(oh323ivr.getLast("udpmax"));
	const char *interface = oh323ivr.getLast("interface");
	H323Gatekeeper *gatekeeper = NULL;

	SetLocalUserName(UserName);

	SetCapabilities();

	if(!oh323ivr.getBool("faststart"))
		DisableFastStart(true);
	if(!oh323ivr.getBool("h245tunneling"))
		DisableH245Tunneling(true);
	if(!oh323ivr.getBool("h245insetup"))
		DisableH245inSetup(true);

	if(rtpmin && rtpmin <= rtpmax)
		SetRtpIpPorts(rtpmin, rtpmax);
	if(tcpmin && tcpmin <= tcpmax)
		SetTCPPorts(tcpmin, tcpmax);
	if(udpmin && udpmin <= udpmax)
		SetUDPPorts(udpmin, udpmax);

	H323ListenerTCP * listener;

	PIPSocket::Address interfaceAddress(interface);
	WORD listenPort = oh323ivr.getSignalPort();

	listener = new H323ListenerTCP(*this, interfaceAddress, (WORD)oh323ivr.getPort());

	if (!StartListener(listener)) {
		slog(Slog::levelError) <<  "Could not open H.323 listener port on " << listener->GetListenerPort() << endl;
		delete listener;
		return false;
	}

	slog(Slog::levelInfo) << "Waiting for incoming calls for \"" << GetLocalUserName() << '"' << endl;

	if(usegk)
	{
		const char *gkpw = oh323ivr.getLast("gatekeeperpassword");
		const char *gkaddr = oh323ivr.getLast("gatekeeperaddress");
		const char *gkid = oh323ivr.getLast("gatekeeperid");
		bool rtn;

		if(gkpw && strlen(gkpw))
		{
			slog(Slog::levelInfo) << "oh323: H.235 security enabled for gatekeeper" << endl;
			SetGatekeeperPassword(gkpw);
		}

		if(!UseGatekeeper(gkaddr, gkid))
		{
			slog(Slog::levelError) << "oh323: gatekeeper registration failed: " << endl;;
			gatekeeper = GetGatekeeper();
			if(gatekeeper)
			{
				switch(gatekeeper->GetRegistrationFailReason())
				{
				case H323Gatekeeper::DuplicateAlias:
					slog(Slog::levelError) << "\tduplicate alias" << endl;
					break;
				case H323Gatekeeper::SecurityDenied:
					slog(Slog::levelError) << "\tnot authorised to connect" << endl;
					break;
				case H323Gatekeeper::TransportError:
					slog(Slog::levelError) << "\ttransport error" << endl;
					break;
				default:
					slog(Slog::levelError) << "\tnot found" << endl;
					break;
				}
			}
			else
				slog(Slog::levelError) << "\tunknown error" << endl;
		}
		else
		{
			gatekeeper = GetGatekeeper();
			if(gatekeeper)
				slog(Slog::levelInfo) << "oh323: successfully registered with " << 
					gatekeeper->GetName() << endl;
		}
	}

	return true;
}

H323Gatekeeper * OH323EndPoint::CreateGatekeeper(H323Transport *transport)
{
	return new OH323GatekeeperNAT(*this, transport);
}

H323Connection * OH323EndPoint::CreateConnection(unsigned callRef, void *userData,
						 H323Transport *transport, H323SignalPDU *setupPDU)
{
	int i;
	OH323Trunk *trk = (OH323Trunk *)userData;

	if(trk)
		return (H323Connection *)trk->attachConnection(*this, callRef);

	for(i = 0; i < oh323ivr.getRTPCount(); i++)
	{
		if(oh323ivr.ports[i] != NULL)
			continue;
		oh323ivr.ports[i] = new OH323Trunk(*this, callRef, i);
		return (H323Connection*)oh323ivr.ports[i]->attachConnection(*this, callRef);
	}
}

bool OH323EndPoint::MakeCall(const PString & dest, PString & token, unsigned int *callReference,
				H323Capability *cap, char *callerId, void *userData)
{
	PString finalDest = dest;

	if(GetGatekeeper() == NULL && strrchr(finalDest, ':') == NULL)
	{
		finalDest += psprintf(":%i", H323EndPoint::DefaultTcpPort);
	}

	if(callerId)
		SetLocalUserName(PString(callerId));

	SetCapabilities();;

	H323EndPoint::MakeCall(finalDest, token, userData);
	return true;
}

BOOL OH323EndPoint::OnConnectionForwarded(H323Connection & connection, const PString &forwardParty,
					  const H323SignalPDU &)
{
	OH323Connection & conn = (OH323Connection &)connection;
	PString currentToken = connection.GetCallToken();
	PString newToken;

	if(MakeCall(forwardParty, newToken, NULL, NULL, NULL, (void*)conn.trunk))
	{
		conn.setForward();
		return TRUE;
	}
	else
		return FALSE;
}
	
void OH323EndPoint::OnConnectionCleared(H323Connection & connection, const PString & token)
{
	slog(Slog::levelDebug) << "OH323EndPoint::OnConnectionCleared()" <<
		((OH323Connection &)connection).id << endl;
	int i = ((OH323Connection &)connection).id;
	oh323ivr.ports[i] = NULL;
}

void OH323EndPoint::SetCapabilities(void)
{
	const char *cp = oh323ivr.getLast("uimode");

	H323Capability *gsm;
	H323Capability *msgsm;
	H323Capability *ulaw;
	H323Capability *alaw;

	SetCapability(0, 0, gsm = new H323_GSM0610Capability);
	SetCapability(0, 0, msgsm = new MicrosoftGSMAudioCapability);
	SetCapability(0, 0, ulaw = new H323_G711Capability(H323_G711Capability::muLaw, H323_G711Capability::At64k));
	SetCapability(0, 0, alaw = new H323_G711Capability(H323_G711Capability::ALaw, H323_G711Capability::At64k));

	gsm->SetTxFramesInPacket(4);
	msgsm->SetTxFramesInPacket(4);
	ulaw->SetTxFramesInPacket(30);
	alaw->SetTxFramesInPacket(30);

	SetCapability(0, 0, new SpeexNarrow2AudioCapability());
	SetCapability(0, 0, new SpeexNarrow3AudioCapability());
	SetCapability(0, 0, new SpeexNarrow4AudioCapability());
	SetCapability(0, 0, new SpeexNarrow5AudioCapability());
	SetCapability(0, 0, new SpeexNarrow6AudioCapability());

	SetCapability(0, 0, new H323_G726_Capability(*this, H323_G726_Capability::e_16k));
	SetCapability(0, 0, new H323_G726_Capability(*this, H323_G726_Capability::e_24k));
	SetCapability(0, 0, new H323_G726_Capability(*this, H323_G726_Capability::e_32k));
	SetCapability(0, 0, new H323_G726_Capability(*this, H323_G726_Capability::e_40k));

	SetCapability(0, 0, new H323_LPC10Capability(*this));

	AddAllUserInputCapabilities(0, 1);
	AddAllUserInputCapabilities(0, 2);

        if(!stricmp(cp, "q931"))
                SetSendUserInputMode(H323Connection::SendUserInputAsQ931);
        else if(!stricmp(cp, "signal"))
                SetSendUserInputMode(H323Connection::SendUserInputAsTone);
        else if(!stricmp(cp, "rfc2833"))
                SetSendUserInputMode(H323Connection::SendUserInputAsInlineRFC2833);
        else
                SetSendUserInputMode(H323Connection::SendUserInputAsString);

	SetAudioJitterDelay(GetMinAudioJitterDelay(), GetMinAudioJitterDelay());
}

void OH323EndPoint::SetEndpointTypeInfo(H225_EndpointType & info) const
{
	const char *pfxcfg = oh323ivr.getLast("prefixes");
	char *prefixes[65];
	char *opt;
	char *cp;
	char *sp;
	int pcount = 0;

	H323EndPoint::SetEndpointTypeInfo(info);

	info.m_gateway.IncludeOptionalField(H225_GatewayInfo::e_protocol);
	info.m_gateway.m_protocol.SetSize(1);
	H225_SupportedProtocols &protocol = info.m_gateway.m_protocol[0];
	protocol.SetTag(H225_SupportedProtocols::e_voice);

	if(pfxcfg)
	{
		opt = strdup(pfxcfg);
		cp = strtok_r(opt, " ,;\t\n", &sp);
		while(cp)
		{
			prefixes[pcount++] = cp;
			cp = strtok_r(NULL, " ,;\t\n", &sp);
		}
		((H225_VoiceCaps &)protocol).m_supportedPrefixes.SetSize(pcount);
		while(pcount--)
			H323SetAliasAddress(PString(prefixes[pcount]),
				((H225_VoiceCaps &)protocol).m_supportedPrefixes[pcount].m_prefix);
		free(opt);
	}
}

void OH323EndPoint::SetVendorIdentifierInfo(H225_VendorIdentifier & info) const
{
	H323EndPoint::SetVendorIdentifierInfo(info);

	info.IncludeOptionalField(H225_VendorIdentifier::e_productId);
	info.m_productId = PString("GNU Bayonne");
	info.IncludeOptionalField(H225_VendorIdentifier::e_versionId);
	info.m_productId = VERSION;
}

void OH323EndPoint::SetH221NonStandardInfo(H225_H221NonStandard & info) const
{
	H323EndPoint::SetH221NonStandardInfo(info);
}

#ifdef CCXX_NAMESPACES
};
#endif

