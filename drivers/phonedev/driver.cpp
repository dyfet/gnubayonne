// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

PhonedevConfig::PhonedevConfig() :
Keydata("/bayonne/phone")
{
	static Keydata::Define defkeys[] = {
	{"devices", "16"},
	{"wink", "100"},
	{"buffers", "1"},
	{"echo", "low"},
	{NULL, NULL}};

	if(isFHS())
		load("/drivers/phone");

	load(defkeys);
}

size_t PhonedevConfig::getStack(void)
{
	const char *cp = getLast("stack");

	if(!cp)
		return keythreads.getStack();

	return atoi(cp) * 1024;
}

PhonedevDriver::PhonedevDriver() :
Driver()
{
	port_count = getDevices();
	ports = new PhonedevTrunk *[port_count];
	groups = new TrunkGroup *[port_count];
	memset(ports, 0, sizeof(PhonedevTrunk *) * port_count);
	memset(groups, 0, sizeof(TrunkGroup *) * port_count);

	thread_count = thread_select = 0;
	
	slog(Slog::levelInfo) << "Generic Phone Device (phonedev) driver loaded; capacity=" << port_count << endl;
}

PhonedevDriver::~PhonedevDriver()
{
	stop();
	delete ports;
	delete groups;
}

int PhonedevDriver::start(void)
{
	char path[32];
	int fd, ts;
	int count = 0;
	int scount = keythreads.getServices();

	if(active)
	{
		slog(Slog::levelError) << "driver already started" << endl;
		return 0;
	}

	slog(Slog::levelInfo) << "driver starting " << scount << " service thread(s)" << endl;
	threads = new PhonedevService[scount];

	for(ts = 0; ts < port_count; ++ts)
	{
		sprintf(path, "/dev/phone%d", ts);
		fd = open(path, O_RDWR);
		if(fd < 0)
			continue;
		if(!getuid())
			fchown(fd, keyserver.getUid(), keyserver.getGid());
		ports[ts] = new PhonedevTrunk(fd, ts);
		++count;
	}

	active = true;
	return count;
}

void PhonedevDriver::stop(void)
{
	int id;
	if(!active)
		return;

	if(ports)
	{
		for(id = 0; id < port_count; ++id)
		{
			if(ports[id])
				delete ports[id];
		}
	}
	memset(ports, 0, sizeof(PhonedevTrunk *) * port_count);
	delete[] threads;
	
	active = false;
	slog(Slog::levelInfo) << "driver stopping service thread(s)" << endl;
}

Trunk *PhonedevDriver::getTrunkPort(int id)
{
	if(id < 0 || id >= port_count)
		return NULL;

	return (Trunk *)ports[id];
}

PhonedevService *PhonedevDriver::getService(void)
{
	PhonedevService *svc = &threads[thread_select++];
	if(thread_select >= thread_count)
		thread_select = 0;
	return svc;
}

void PhonedevDriver::notify(unsigned char id)
{
	TrunkEvent event;
	PhonedevTrunk *trunk;

	if(id < 1 || id > port_count)
		return;

	trunk = ports[--id];
	if(!trunk)
		return;

	event.id = TRUNK_NOTIFICATION;
	trunk->postEvent(&event);
}

PhonedevDriver phivr;

#ifdef	CCXX_NAMESPACES
};
#endif
