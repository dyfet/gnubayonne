// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

bool PhonedevTrunk::answerHandler(TrunkEvent *event)
{
	timeout_t wakeup;

	switch(event->id)
	{
	case TRUNK_LINE_WINK:
		if(!flags.offhook)
			return true;
	case TRUNK_CPA_DIALTONE:
		setSymbol(SYM_TONE, "dial");
	case TRUNK_STOP_DISCONNECT:
	case TRUNK_TIMER_EXPIRED:
		flags.dsp = DSP_MODE_INACTIVE;
		if(!trunkEvent("answer:failed"))
			trunkSignal(TRUNK_SIGNAL_HANGUP);
		handler = &PhonedevTrunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		setSymbol(SYM_TONE, "none");
		if(flags.offhook)
		{
			trunkSignal(TRUNK_SIGNAL_STEP);
			handler = &PhonedevTrunk::stepHandler;
			return true;
		}
		status[tsid] = 'a';
		enterState("answer");
	case TRUNK_RINGING_OFF:
		if(rings > 1)
			flags.dsp = DSP_MODE_INACTIVE;

		if(data.answer.rings > rings)
		{
			setTimer(data.answer.timeout);
			return true;
		}
		endTimer();
		flags.dsp = DSP_MODE_INACTIVE;
		setHookState(true);
		setCPADetect(CPA_DIALTONE, true);
		trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &PhonedevTrunk::stepHandler;
		status[tsid] = 'i';
		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
