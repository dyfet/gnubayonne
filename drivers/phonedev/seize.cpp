// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

bool PhonedevTrunk::seizeHandler(TrunkEvent *event)
{
	char script[256];
	const char *start = NULL;

	switch(event->id)
	{
	case TRUNK_ENTER_STATE:
		enterState("seize");
		status[tsid] = 's';
		flags.dsp = DSP_MODE_VOICE;
		setHookState(true);
		setCPADetect(CPA_DIALTONE, true);
		setTimer(group->getDialtone());	
		return true;
	case TRUNK_CPA_DIALTONE:
		syncParent("start:running");
success:
		status[tsid] = 'o';
		handler = &PhonedevTrunk::stepHandler;
		return true;
	case TRUNK_TIMER_EXPIRED:
		syncParent("start:failed");
failed:
		if(trunkEvent("pickup:failed"))
			goto success;

		flags.dsp = DSP_MODE_INACTIVE;
		slog(Slog::levelWarning) << "phone" << id << ": sieze failed; ringing line" << endl;
		detach();
//		start = group->getSchedule(script);
//		if(attach(start))
//		{
//			handler = &PhonedevTrunk::stepHandler;
//			return true;
//		}
		handler = &PhonedevTrunk::hangupHandler;
		slog(Slog::levelError) << "phone" << id << ": " << script << ": cannot start" << endl;
		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
