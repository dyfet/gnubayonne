// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"
#include <sys/ioctl.h>

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

void PhonedevTrunk::initSyms(void)
{
	char buf[4];
	snprintf(buf, sizeof(buf), "%d", id + 1);
	setConst(SYM_CARD, buf);
	setConst(SYM_INTERFACE, "analog");
	setConst(SYM_NETWORK, "none");
}

bool PhonedevTrunk::idleHandler(TrunkEvent *event)
{
	char script[256];
	int argc = 0;
	const char *start = NULL;
	const char *data;
	const char *value;
	TrunkGroup *grp = NULL;
	char *sp;
	int ctrl;
	unsigned chk;
	char **argv;
	Request *req;

	switch(event->id)
	{
	case TRUNK_ENTER_STATE:
		synctimer = exittimer = 0;
		enterState("idle");
		setIdle(true);
		ScriptSymbols::purge();
		setEcho(ECHO_OFF);
		setCPADetect(CPA_DIALTONE, false);
		setCPADetect(CPA_BUSYTONE, false);
		flags.dsp = DSP_MODE_INACTIVE;
		tgi.pid = 0;
		digits = 0;
		switch(flags.trunk)
		{
		case TRUNK_MODE_OUTGOING:
			group->decOutgoing();
			break;
		case TRUNK_MODE_INCOMING:
			group->decIncoming();
		}

		flags.trunk = TRUNK_MODE_INACTIVE;
		status[tsid] = '-';
		time(&idletime);
		purge();
		rings = 0;
		switch(driver)
		{
#ifdef	IXJ_PHONEDEV
		case ixj_driver:
			::close(dev);
			strcpy(script, "/dev/");
			getName(script + 5);
			dev = open(script, O_RDWR);
			if(dev < 0)
			{
				slog(Slog::levelCritical) << script + 5 << ": device reset failed" << endl;
				raise(SIGINT);
			}
#ifdef	POSIX_PHONEDEV
			ctrl = PORT_PSTN;
			ioctl(dev, IXJCTL_PORT, &ctrl);
#else
			ioctl(dev, IXJCTL_PORT, PORT_PSTN);
#endif
#endif
		}
		value = group->chkRequest();
		chk = atoi(value);
		if(!chk && stricmp(value, "hangup"))
			return true;
		if(chk < group->getReady())
			chk = group->getReady();
		setTimer(chk);
		return true;
	case TRUNK_LINE_WINK:
	case TRUNK_CPA_DIALTONE:
	case TRUNK_MAKE_IDLE:
	case TRUNK_STOP_DISCONNECT:
		return true;
	case TRUNK_MAKE_BUSY:
		endTimer();
		handler = &PhonedevTrunk::busyHandler;
		return true;
	case TRUNK_TIMER_EXPIRED:
		flags.ready = true;
		req = group->getRequest();
		if(req)
		{
			setConst(SYM_PARENT, req->getParent());
			argv = req->getList();
			start = *(argv++);
			if(attach(start))
			{
				flags.trunk = TRUNK_MODE_OUTGOING;
				setList(argv);
				endTimer();
				flags.ready = false;
				handler = &PhonedevTrunk::seizeHandler;
				group->incOutgoing();
				delete req;
				return true;
			}
			ScriptSymbols::purge();
			delete req;
		}
		chk = atoi(group->chkRequest());
		if(chk)
			setTimer(chk);
		return true;		
	case TRUNK_START_SCRIPT:
		flags.trunk = TRUNK_MODE_OUTGOING;
	case TRUNK_RING_START:
		start = event->parm.argv[0];
		if(start)
		{
			if(strchr(start, '='))
				start = group->getSchedule(script);
			else
				++argc;
		}
		if(flags.trunk == TRUNK_MODE_INACTIVE)
			flags.trunk = TRUNK_MODE_INCOMING;
		rings = 0;
		if(!start)
			start = group->getSchedule(script);
		if(attach(start))
		{
			setList(&event->parm.argv[argc]);
			flags.ready = false;
			endTimer();		
			if(flags.trunk == TRUNK_MODE_OUTGOING)
			{
				handler = &PhonedevTrunk::seizeHandler;
				group->incOutgoing();
			}
			else
			{
				syncParent("start:running");
				handler = &PhonedevTrunk::stepHandler;
				group->incIncoming();
			}
			return true;
		}
		syncParent("start:failed");
		ScriptSymbols::purge();
		flags.trunk = TRUNK_MODE_INACTIVE;
		slog(Slog::levelError) << "phone" << id << ": " << start << ": cannot start" << endl;
		return true;
	case TRUNK_RING_REDIRECT:
		start = group->getRedirect(event->parm.argv[0], script);
		rings = 0;
		if(attach(start))
		{
			setConst(SYM_REDIRECT, event->parm.argv[0]);
			setList(&event->parm.argv[1]);
			flags.ready = false;
			flags.trunk = TRUNK_MODE_INCOMING;
			endTimer();
			handler = &PhonedevTrunk::stepHandler;
			group->incIncoming();		
			return true;
		}
		ScriptInterp::purge();
		slog(Slog::levelError) << "phone" << id << ": " << event->parm.argv[0] << ": unable to redirect" << endl;
		return true;
	case TRUNK_RINGING_ON:
		if(!group->getAnswer())
			return true;
		rings = 1;
		flags.trunk = TRUNK_MODE_INCOMING;
		group->incIncoming();
		flags.ready = false;
		endTimer();
		handler = &PhonedevTrunk::ringHandler;
		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
