// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"
#include <sys/ioctl.h>
#include <ctype.h>

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

bool PhonedevTrunk::waitHandler(TrunkEvent *event)
{
	switch(event->id)
	{
	case TRUNK_TIMER_EXPIRED:
		setSymbol(SYM_TONE, "fail");
		if(!trunkEvent("dial:failed"))
			if(!trunkSignal(TRUNK_SIGNAL_CANCEL))
				if(!trunkSignal(TRUNK_SIGNAL_TONE))
					return false;
		handler = &PhonedevTrunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		setCPADetect(CPA_DIALTONE, true);
		setDTMFDetect(false);
		setHookState(true);
		setTimer(1000);
		return true;
	case TRUNK_CPA_BUSYTONE:
		return dialHandler(event);
	case TRUNK_CPA_DIALTONE:
		setCPADetect(CPA_DIALTONE, false);
		handler = &PhonedevTrunk::dialHandler;
		return true;
	}
	return false;		
}

bool PhonedevTrunk::dialHandler(TrunkEvent *event)
{
	unsigned short mask;
	int dial;
	int ctrl;

	switch(event->id)
	{
	case TRUNK_CPA_BUSYTONE:
		setSymbol(SYM_TONE, "busy");
		if(trunkEvent("dial:busy"))
		{
			handler = &PhonedevTrunk::stepHandler;
			return true;
		}
		if(trunkEvent("tone:busy"))
		{
			handler = &PhonedevTrunk::stepHandler;
			return true;
		}
		if(trunkSignal(TRUNK_SIGNAL_BUSY))
		{
			handler = &PhonedevTrunk::stepHandler;
			return true;
		}
		if(trunkSignal(TRUNK_SIGNAL_TONE))
		{
			handler = &PhonedevTrunk::stepHandler;
			return true;
		}
		return true;
	case TRUNK_ENTER_STATE:
		enterState("dial");
		flags.dsp = DSP_MODE_VOICE;
		setSymbol(SYM_TONE, "none");
		setCPADetect(CPA_DIALTONE, false);
		setCPADetect(CPA_BUSYTONE, true);
		setDTMFDetect(false);
		status[tsid] = 'd';
	case TRUNK_TIMER_EXPIRED:
		if(!flags.offhook)
			setHookState(true);
#ifdef	POSIX_PHONEDEV
		ioctl(dev, PHONE_GET_TONE_STATE, &ctrl);
#else
		ctrl = ioctl(dev, PHONE_GET_TONE_STATE);
#endif
		if(ctrl)
		{
			setTimer(36);
			return true;
		}
		switch(*data.dialxfer.digit)
		{
		case 0:
			setCPADetect(CPA_BUSYTONE, false);
			if(trunkEvent("dial:expired"))
			{
			}
			else if(data.dialxfer.exit)
				trunkSignal(TRUNK_SIGNAL_HANGUP);
			else
				trunkSignal(TRUNK_SIGNAL_STEP);
			handler = &PhonedevTrunk::stepHandler;
			return true;
		case 'w':
		case 'W':
			handler = &PhonedevTrunk::waitHandler;
			++data.dialxfer.digit;
			return true;
		case 'f':
		case 'F':
			setTimer(group->getFlash());
			++data.dialxfer.digit;
			return true;
		case 'A':
		case 'B':
		case 'C':
		case 'D':
			*data.dialxfer.digit = tolower(*data.dialxfer.digit);
		case 'a':
		case 'b':
		case 'c':
		case 'd':
			dial = 28 + *data.dialxfer.digit - 'a';
			break;					
		case '0':
			dial = 10;
			break;
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			dial = *data.dialxfer.digit - '0';
			break;
		case '*':
			dial = 11;
			break;
		case '#':
			dial = 12;
			break;
		default:
			++data.dialxfer.digit;
			setTimer(1000);
			return true;	
		}
		++data.dialxfer.digit;
		setTimer(data.dialxfer.interdigit);
#ifdef	POSIX_PHONEDEV
		ioctl(dev, PHONE_PLAY_TONE, &dial);
#else
		ioctl(dev, PHONE_PLAY_TONE, dial);
#endif
		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
