// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

void PhonedevTrunk::trunkStep(trunkstep_t step)
{
	TrunkEvent event;
	
	if(handler != &PhonedevTrunk::stepHandler)
		return;

	event.id = TRUNK_MAKE_STEP;
	event.parm.step = step;
	stepHandler(&event);
}

bool PhonedevTrunk::stepHandler(TrunkEvent *event)
{
	Trunk *alt = ctx;
	switch(event->id)
	{
	case TRUNK_MAKE_STEP:
		if(idleHangup())
			return true;
		switch(event->parm.step)
		{
		case TRUNK_STEP_THREAD:
			handler = &PhonedevTrunk::threadHandler;
			return true;
		case TRUNK_STEP_SLEEP:
			handler = &PhonedevTrunk::sleepHandler;
			return true;
		case TRUNK_STEP_HANGUP:
			handler = &PhonedevTrunk::hangupHandler;
			return true;
		case TRUNK_STEP_ANSWER:
			handler = &PhonedevTrunk::answerHandler;
			return true;
		case TRUNK_STEP_COLLECT:
			handler = &PhonedevTrunk::collectHandler;
			return true;
		case TRUNK_STEP_FLASH:
			handler = &PhonedevTrunk::flashonHandler;
			return true;
		case TRUNK_STEP_PLAY:
			handler = &PhonedevTrunk::playHandler;
			return true;
		case TRUNK_STEP_RECORD:
			handler = &PhonedevTrunk::recordHandler;
			return true;
		case TRUNK_STEP_SOFTDIAL:
		case TRUNK_STEP_DIALXFER:
			handler = &PhonedevTrunk::dialHandler;
			return true;
		case TRUNK_STEP_PLAYWAIT:
			handler = &PhonedevTrunk::playwaitHandler;
			return true;
		}
		return false;
	case TRUNK_SERVICE_SUCCESS:
	case TRUNK_SERVICE_FAILURE:
		endTimer();
		setTimer(keythreads.getResetDelay());
		return true;
	case TRUNK_STOP_STATE:
	case TRUNK_TIMER_EXPIRED:
		flags.reset = false;
		stopServices();
	case TRUNK_ENTER_STATE:
		if(flags.offhook)
		{
			if(flags.trunk == TRUNK_MODE_OUTGOING)
				status[tsid] = 'o';
			else
				status[tsid] = 'i';
			setCPADetect(CPA_DIALTONE, true);
		}
		else
			setCPADetect(CPA_DIALTONE, false);
		setEcho(ECHO_OFF);
		setCPADetect(CPA_BUSYTONE, false);
                if(tgi.fd > -1 && digits)
                {
                        snprintf(buffer, sizeof(buffer), "%s\n", dtmf.bin.data);
                        writeShell(buffer);
                        digits = 0;
                }
 
                setDTMFDetect();
		endTimer();

		// if a dsp reset has occured, then we add a delay
		// for the dsp to settle before stepping
		if(flags.reset || thread)
		{
			enterState("reset");
			if(thread)
				setTimer(thread->stop());
			else
				setTimer(keythreads.getResetDelay());
			return true;
		}

                if(tgi.fd > -1)
		{
                        enterState("shell");
                        return true;
		}

		debug->debugStep(this, getLine());
		monitor->monitorStep(this, getLine());

		// step and delay if still in same handler

		if(alt != this)
			alt->enterMutex();
		scriptStep();
		if(alt != this)
			alt->leaveMutex();

		if(handler == &PhonedevTrunk::stepHandler)
			setTimer(keythreads.getStepDelay());

		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
