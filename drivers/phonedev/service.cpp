// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace ost;
#endif

PhonedevService::PhonedevService() :
Thread(keythreads.priService(), phivr.getStack()), Mutex()
{
	long opt;

	first = last = NULL;
	FD_ZERO(&connect);
	::pipe(iosync);
	hiwater = iosync[0] + 1;

	opt = fcntl(iosync[0], F_GETFL);
	fcntl(iosync[0], F_SETFL, opt | O_NDELAY);
}

PhonedevService::~PhonedevService()
{
	update(0);
	terminate();

	while(first)
		delete first;
}

void PhonedevService::attach(PhonedevTrunk *trunk)
{
	int dev;

	enterMutex();
	if(last)
		last->next = trunk;

	trunk->prev = last;
	last = trunk;
	dev = trunk->dev;
	FD_SET(dev, &connect);
	if(dev >= hiwater)
		hiwater = ++dev;

	if(!first)
	{
		first = trunk;
		leaveMutex();
		start();
	}
	else
	{
		leaveMutex();
		update();
	}
	++count;
}

void PhonedevService::detach(PhonedevTrunk *trunk)
{
	enterMutex();
	FD_CLR(trunk->dev, &connect);
	if(trunk == first && trunk == last)
	{
		first = last = NULL;
		leaveMutex();
		return;
	}
	if(trunk->prev)
		trunk->prev->next = trunk->next;

	if(trunk->next)
		trunk->next->prev = trunk->prev;

	if(trunk == first)
		first = trunk->next;

	if(trunk == last)
		last = trunk->prev;

	leaveMutex();
	update();
	--count;
}		

void PhonedevService::update(unsigned char flag)
{
	::write(iosync[1], (char *)&flag, 1);
}

void PhonedevService::run(void)
{
	TrunkEvent event;
	struct timeval timeout, *tvp;
	timeout_t timer, expires;
	PhonedevTrunk *trunk;
	fd_set err;
	fd_set inp;
	unsigned char buf;
	int dev;
	char tmp[33];

	FD_ZERO(&err);

	setCancel(cancelDeferred);
	for(;;)
	{
		timer = ~0;
		while(1 == ::read(iosync[0], (char *)&buf, 1))
		{
			if(buf && buf != 0xff)
			{ 
				debug->debugService(phivr.getTrunkPort(buf - 1), "notify");
				phivr.notify(buf);
				continue;
			}
			if(buf)
			{
				debug->debugService(NULL, "updating");
				continue;
			}
			debug->debugService(NULL, "exiting");
			setCancel(cancelDeferred);
			Thread::sleep(~0);
			exit();
		}

		yield();
		enterMutex();
		trunk = first;
		while(trunk)
		{
			dev = trunk->dev;

			if(FD_ISSET(dev, &err))
			{
				debug->debugService(trunk, "event");
				trunk->getEvents();
			}

retry:
			expires = trunk->getTimer();
			if(expires > 0)
				if(expires < timer)
					timer = expires;

			if(!expires)
			{
				debug->debugService(trunk, "expires");
				event.id = TRUNK_TIMER_EXPIRED;
				trunk->endTimer();
				trunk->postEvent(&event);
				goto retry;
			}

			trunk = trunk->next;
		}
		leaveMutex();
		memcpy(&err, &connect, sizeof(err));
		FD_ZERO(&inp);
		FD_SET(iosync[0], &inp);
		if(timer == ~0)
			tvp = NULL;
		else
		{
			tvp = &timeout;
			timeout.tv_sec = timer / 1000;
			timeout.tv_usec = (timer % 1000) * 1000;
		}
		sprintf(tmp, "delay %d", timer);
		debug->debugService(NULL, tmp);
		select(hiwater, &inp, NULL, &err, tvp);
		debug->debugService(NULL, "wakeup");
	}
}

#ifdef	CCXX_NAMESPACES
};
#endif
