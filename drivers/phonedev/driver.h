// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <ivrconfig.h>
#include <server.h>
#include <cc++/audio.h>

#ifdef	HAVE_LINUX_TELEPHONY_H
#include <linux/telephony.h>
#ifdef	HAVE_LINUX_IXJUSER_H
#include <linux/ixjuser.h>
#endif
#endif

#ifdef HAVE_SYS_TELEPHONY_H
#include <sys/telephony.h>
#ifdef HAVE_SYS_IXJUSER_H
#include <sys/ixjuser.h>
#endif
#endif

#ifdef	CCXX_NAMESPACES
namespace ost {
#endif

typedef	enum
{
	ECHO_OFF = 0,
	ECHO_LOW,
	ECHO_MEDIUM,
	ECHO_HIGH
} aeclevel_t;

typedef	enum
{
	CPA_DIALTONE = 0,
	CPA_BUSYTONE
} cpatone_t;

class PhonedevTrunk;
class PhonedevDriver;
class PhonedevService;

extern PhonedevDriver phivr;

typedef union
{
#ifdef	IXJ_PHONEDEV
	PHONE_CID	ixjcid;
#endif
}	cid_t;

typedef enum
{
	generic_driver,
	ixj_driver
}	driver_t;

typedef	bool (PhonedevTrunk::*trunkhandler_t)(TrunkEvent *event);

class PhonedevTrunk : private TimerPort, private Trunk
{
private:
	friend class PhonedevDriver;
	friend class PhonedevService;
	friend class PhonedevPlay;
	
	time_t lastring;
	trunkhandler_t handler;
	struct phone_capability *cap_list;
	PhonedevTrunk *next, *prev;
	PhonedevService *service;
	driver_t driver;
	int cap_count;
	int dev;

#ifdef	IXJ_PHONEDEV
	IXJ_FILTER cpatone[4];
#endif

	PhonedevTrunk(int fd, int ts);
	~PhonedevTrunk();

	void initSyms(void);
	void setEcho(aeclevel_t level);
	void setCPADetect(cpatone_t tone, bool flag);
	void getName(char *buffer);
	unsigned long getIdleTime(void);
	void setHookState(bool offhook);

	bool stepHandler(TrunkEvent *event);
	bool busyHandler(TrunkEvent *event);
	bool idleHandler(TrunkEvent *event);
	bool seizeHandler(TrunkEvent *event);
	bool ringHandler(TrunkEvent *event);
	bool waitHandler(TrunkEvent *event);
	bool dialHandler(TrunkEvent *event);
	bool playHandler(TrunkEvent *event);
	bool playwaitHandler(TrunkEvent *event);
	bool recordHandler(TrunkEvent *event);
	bool threadHandler(TrunkEvent *event);
	bool sleepHandler(TrunkEvent *event);
	bool hangupHandler(TrunkEvent *event);
	bool answerHandler(TrunkEvent *event);
	bool collectHandler(TrunkEvent *event);
	bool flashonHandler(TrunkEvent *event);
	bool flashoffHandler(TrunkEvent *event);

	struct phone_capability *getCapability(phone_cap id, int sub = -1);
	bool postEvent(TrunkEvent *event);
	void getEvents(void);
	void setTimer(timeout_t timeout = 0);
	void incTimer(timeout_t timeout);
	bool exit(void);
	void trunkStep(trunkstep_t step);

public:
	inline int getDevice(void)
		{return dev;};

	inline driver_t getDriverName(void)
		{return driver;};

	Driver *getDriver(void)
		{return (Driver*)&phivr;};
};

class PhonedevRecord : private AudioFile, public Service
{
private:
	int dev;
	bool reset;
	driver_t driver;
	size_t bufsize, samples;

public:
	PhonedevRecord(PhonedevTrunk *trunk);
	~PhonedevRecord();
	void initial(void);
	void run(void);
};

class PhonedevPlay : private URLAudio, public Service
{
private:
	int dev, id;
	bool reset;
	driver_t driver;
	size_t bufsize, samples;
	void initial(void);
	void run(void);
	char *getContinuation(void);

public:
	PhonedevPlay(PhonedevTrunk *trunk);
	~PhonedevPlay();
};

class PhonedevService : public Thread, private Mutex
{
private:
	friend class PhonedevDriver;
	friend class PhonedevTrunk;

	fd_set connect;
	int iosync[2];
	int hiwater;
	int count;
	PhonedevTrunk *first, *last;

	void attach(PhonedevTrunk *trunk);
	void detach(PhonedevTrunk *trunk);
	void run(void);
	void update(unsigned char id = 0xff);

	PhonedevService();
	~PhonedevService();
};	

class PhonedevConfig : public Keydata
{
public:
	PhonedevConfig();

	inline int getDevices(void)
		{return atoi(getLast("devices"));};

	inline int getWinkDuration(void)
		{return atoi(getLast("wink"));};

	inline int getAudioBuffers(void)
		{return atoi(getLast("buffers"));};

	size_t getStack(void);
};

class PhonedevDriver : public Driver, public PhonedevConfig
{
private:
	PhonedevService *threads;
	int thread_count;
	int thread_select;

	PhonedevTrunk **ports;
	int port_count;

	int start(void);
	void stop(void);

public:
	PhonedevDriver();
	~PhonedevDriver();

	unsigned getTrunkCount(void)
		{return port_count;};

	Trunk *getTrunkPort(int id);
	PhonedevService *getService(void);
	aaScript *getScript(void);
	void notify(unsigned char id);    // ALWAYS trunk + 1

	Driver *getDriver(void)
		{return (Driver *)&phivr;};

	char *getName(void)
		{return "Phone";};
};

#ifdef	CCXX_NAMESPACES
};
#endif
