// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

bool PhonedevTrunk::flashoffHandler(TrunkEvent *event)
{
	switch(event->id)
	{
	case TRUNK_CPA_DIALTONE:
		endTimer();
		setSymbol(SYM_TONE, "dial");
		if(data.dialxfer.digit)
		{
			endTimer();
			handler = &PhonedevTrunk::dialHandler;
			return true;
		}
		if(trunkSignal(TRUNK_SIGNAL_TONE))
		{
			endTimer();
			handler = &PhonedevTrunk::stepHandler;
			return true;
		}
	case TRUNK_TIMER_EXPIRED:
		if(data.dialxfer.digit)
		{
			endTimer();
			handler = &PhonedevTrunk::dialHandler;
			return true;
		}
		trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &PhonedevTrunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		setSymbol(SYM_TONE, "none");
		enterState("flashoff");
		setHookState(true);
		setTimer(data.dialxfer.offhook);
		return true;
	}
	return false;
}

bool PhonedevTrunk::flashonHandler(TrunkEvent *event)
{
	switch(event->id)
	{
	case TRUNK_TIMER_EXPIRED:
		handler = &PhonedevTrunk::flashoffHandler;
		return true;
	case TRUNK_ENTER_STATE:
		enterState("flashon");
		setDTMFDetect(false);
		flags.dsp = DSP_MODE_VOICE;
		status[tsid] = 'f';
		if(!flags.offhook)
		{
			handler = &PhonedevTrunk::flashoffHandler;
			return true;
		}
		setHookState(false);
		setTimer(data.dialxfer.onhook);
		return true;
	}
	return false;
}		

#ifdef	CCXX_NAMESPACES
};
#endif
