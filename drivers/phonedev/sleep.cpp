// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

bool PhonedevTrunk::sleepHandler(TrunkEvent *event)
{
	timeout_t wakeup;
	char buffer[16];
	const char *cp;
	char *sp, *value;

	switch(event->id)
	{
        case TRUNK_SHELL_START:
                if(!tgi.pid)
                        return false;
                tgi.fd = event->parm.fd;
                handler = &PhonedevTrunk::stepHandler;
                return true;
        case TRUNK_WAIT_SHELL:
                if(tgi.seq != event->parm.waitpid.seq)
                        return false;

                if(tgi.pid)
                        return false;

                tgi.pid = event->parm.waitpid.pid;
                return true;

	case TRUNK_EXIT_SHELL:
                if(tgi.seq != event->parm.exitpid.seq)
                        return false;

		if(!tgi.pid)
			return true;

		tgi.pid = 0;
		endTimer();
		sprintf(buffer, "exit-%d", event->parm.exitpid.status);
		setSymbol(SYM_ERROR, buffer);
		if(event->parm.exitpid.status)
			trunkSignal(TRUNK_SIGNAL_ERROR);
		else
			trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &PhonedevTrunk::stepHandler;
		return true;	
	case TRUNK_DTMF_KEYUP:
		if(tgi.pid)
		{
			::kill(tgi.pid, SIGINT);
			tgi.pid = 0;
		}
		return false;
	case TRUNK_RINGING_OFF:
		if(rings > 1)
			flags.dsp = DSP_MODE_INACTIVE;
		if(!data.sleep.rings)
			return true;
		if(rings < data.sleep.rings)
			return true;
		endTimer();
	case TRUNK_TIMER_EXPIRED:
		if(--data.sleep.loops)
		{
			setTimer(data.sleep.wakeup);
			return true;
		}
		if(tgi.pid || data.sleep.save)
		{
			if(tgi.pid)
				::kill(tgi.pid, SIGALRM);
			tgi.pid = 0;
			if(!trunkSignal(TRUNK_SIGNAL_TIMEOUT))
				trunkSignal(TRUNK_SIGNAL_STEP);
		}
		else
			trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &PhonedevTrunk::stepHandler;
		return true;
        case TRUNK_CHILD_FAIL:
                setSymbol(SYM_ERROR, "start-failed");
                if(!trunkSignal(TRUNK_SIGNAL_FAIL))
                        trunkSignal(TRUNK_SIGNAL_STEP);
                handler = &PhonedevTrunk::stepHandler;
                return true;
        case TRUNK_CHILD_START:
		cp = event->parm.trunk->getSymbol(SYM_GID);
		if(cp)
			cp = strchr(cp, '-');
		if(cp)
			setSymbol(data.sleep.save, cp, 16);
                trunkSignal(TRUNK_SIGNAL_STEP);
                handler = &PhonedevTrunk::stepHandler;
                return true;

	case TRUNK_ENTER_STATE:
		enterState("sleep");
		endTimer();
		setDTMFDetect();
		wakeup = data.sleep.wakeup;
		if(data.sleep.rings)
			if(rings >= data.sleep.rings)
				wakeup = 0;

	
		if(!wakeup)
		{
			trunkSignal(TRUNK_SIGNAL_STEP);
			handler = &PhonedevTrunk::stepHandler;
			return true;
		}

		setTimer(wakeup);
		return true;
	}
	return false;
}

bool PhonedevTrunk::threadHandler(TrunkEvent *event)
{
	switch(event->id)
	{
        case TRUNK_TIMER_EXPIRED:
                stopServices();
                endTimer();
                setSymbol(SYM_ERROR, "thread-timeout");
                if(!trunkSignal(TRUNK_SIGNAL_TIMEOUT))
                        trunkSignal(TRUNK_SIGNAL_ERROR);
                handler = &PhonedevTrunk::stepHandler;
                return true;
        case TRUNK_SERVICE_FAILURE:
                flags.reset = true;
                endTimer();
                setSymbol(SYM_ERROR, "thread-error");
                trunkSignal(TRUNK_SIGNAL_ERROR);
                handler = &PhonedevTrunk::stepHandler;
                return true;
        case TRUNK_STOP_STATE:
                stopServices();
                endTimer();
                handler = &PhonedevTrunk::stepHandler;
                return true;
        case TRUNK_SERVICE_SUCCESS:
                flags.reset = true;
		endTimer();
		trunkSignal(TRUNK_SIGNAL_STEP);
                handler = &PhonedevTrunk::stepHandler;
                return true;
        case TRUNK_ENTER_STATE:
                enterState("thread");
                endTimer();
                Trunk::setDTMFDetect();
                setTimer(data.sleep.wakeup);
                thread->start();
                return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
