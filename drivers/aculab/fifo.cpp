// Copyright (C) 2002 David Kerry.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne
// with the Aculab telephony libraries to produce a executable image
// without requiring Aculab's libraries to be supplied in a free software
// license as long as each source file so linked contains this exclusion
// and the unalrtered Aculab source files are made available.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

// 
// $Id: fifo.cpp,v 1.4 2002/10/22 16:49:13 dyfet Exp $
// 
#include "driver.h"

#ifdef    CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif


AculabFifo::AculabFifo() : Mutex()
{

	cond=new Conditional();
	active=true;
	head=NULL;
	tail=NULL;
}

/*
 * Remove all exisiting events and disable further
 * pushes onto queue.
 */
void AculabFifo::disable()
{
	AculabTrunkEvent *evt,*next;

	cond->enterMutex();
	evt=head;
	while (evt != NULL) {
		next=evt->next;
		delete evt;
		evt=next;
	}
	cond->signal(false);
	cond->leaveMutex();
}

/* Re-enable queueing */
void AculabFifo::enable()
{
	active=true;
}

/*
 * Push a new trunk event on to the head of the queue
 */
void AculabFifo::push(AculabTrunkEvent *evt)
{
	if (!active) return;

	cond->enterMutex();
	if (head == NULL) {
		head=tail=evt;
		evt->next=NULL;
		evt->prev=NULL;
	}
	else {
		head->prev=evt;
		evt->next=head;
		evt->prev=NULL;
		head=evt;
	}
	cond->signal(true);
	cond->leaveMutex();
}

/*
 * Pull a new trunk event off of the tail of the queue
 *
 * BLOCKS until queue has something in it!
 */
AculabTrunkEvent *AculabFifo::pullBlock()
{
	AculabTrunkEvent *e=NULL;

	while (1) {
		if (!active) {
			break;
		}

		cond->enterMutex();
		if (tail != NULL) {
			e=tail;
			tail=tail->prev;
			if (tail == NULL) {
				head=NULL;
			}
			else {
				tail->next=NULL;
			}
			cond->leaveMutex();
			break;
		}
		else {
			cond->wait(0);
			cond->leaveMutex();
		}
	}
	return e;
}


/*
 * Pull a new trunk event off of the queue (if
 * there's one available).
 *
 * Does not block.
 */
AculabTrunkEvent *AculabFifo::pull()
{
	AculabTrunkEvent *e=NULL;

	if (!active) return NULL;

	enterMutex();
	if (tail != NULL) {
		e=tail;
		tail=tail->prev;
		if (tail == NULL) {
			head=NULL;
		}
		else {
			tail->next=NULL;
		}
	}
	leaveMutex();
	return e;
}

#ifdef    CCXX_NAMESPACES
};
#endif
