// Copyright (C) 2002 David Kerry.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne
// with the Aculab telephony libraries to produce a executable image
// without requiring Aculab's libraries to be supplied in a free software
// license as long as each source file so linked contains this exclusion
// and the unalrtered Aculab source files are made available.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".


#include "driver.h"

#ifdef    CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

bool AculabTrunk::joinHandler(TrunkEvent *event)
{
	TrunkEvent jevent;

	switch(event->id)
	{
	case TRUNK_DTMF_KEYUP:
		if(join)
			return true;

		return false;

	case TRUNK_TIMER_EXPIRED:
		if(data.join.count)
		{
			--data.join.count;
			if(data.join.trunk->getSequence() != data.join.seq)
				goto failed;
			goto retry;
		}
		Part();
		if(!trunkSignal(TRUNK_SIGNAL_TIMEOUT))
			trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &AculabTrunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		_stopping_state = false;
retry:
		TimerPort::endTimer();
		if(data.join.trunk)
		{
			jevent.id = TRUNK_JOIN_TRUNKS;
			jevent.parm.trunk = this;
			data.join.trunk->postEvent(&jevent);
			if(!join && data.join.count)
			{
                        	enterState("sync");
                                status[tsid] = 's';
				TimerPort::setTimer(keythreads.getResetDelay());
                                return true;
			}
			if(!join)
			{
failed:
				if(!trunkSignal(TRUNK_SIGNAL_ERROR))
					trunkSignal(TRUNK_SIGNAL_STEP);
				setSymbol(SYM_ERROR, "join-failed");
				handler = &AculabTrunk::stepHandler;
				return true;
			}
			enterState("join");
			status[tsid] = 'j';
		}
		else
		{
			tSMConf=sm_conference_create();
			Trunk::setDTMFDetect();
			enterState("wait");
			status[tsid] = 'w';
		}
		if(data.join.wakeup)
			TimerPort::setTimer(data.join.wakeup);
		data.join.count = 0;
		return true;
	case TRUNK_PART_TRUNKS:
		TimerPort::endTimer();
		if(data.join.hangup)
		{
			if(!trunkSignal(TRUNK_SIGNAL_CANCEL))
				trunkSignal(TRUNK_SIGNAL_HANGUP);
		}
		else
		{
			if(!trunkSignal(TRUNK_SIGNAL_CANCEL))
			{
				setSymbol(SYM_ERROR, "join-parted");
				trunkSignal(TRUNK_SIGNAL_ERROR);
			}
		}
		handler = &AculabTrunk::stepHandler;
		return true;
	case TRUNK_SIGNAL_JOIN:
		if(join)
			return false;

		if(event->parm.error)
			setSymbol(SYM_ERROR, event->parm.error);
		else
			event->parm.error = "";

		if(!trunkSignal(TRUNK_SIGNAL_EVENT))
			trunkSignal(TRUNK_SIGNAL_HANGUP);
		else
		{
			setSymbol(SYM_EVENTID, "join");
			setSymbol(SYM_EVENTMSG, event->parm.error);
		}
		handler = &AculabTrunk::stepHandler;
		return true;
	case TRUNK_JOIN_TRUNKS:
		if(join)
			return false;

		if(data.join.waiting)
		{
			if(event->parm.trunk != data.join.waiting)
				return false;
		}

		data.join.count = 0;
		TimerPort::endTimer();
		if(Join((AculabTrunk *)event->parm.trunk))
			return true;

		trunkSignal(TRUNK_SIGNAL_ERROR);
		setSymbol(SYM_ERROR, "join-failed");
		handler = &AculabTrunk::stepHandler;
		return false;
	}
	return false;
}

#ifdef    CCXX_NAMESPACES
};
#endif
