// Copyright (C) 2002 David Kerry.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne
// with the Aculab telephony libraries to produce a executable image
// without requiring Aculab's libraries to be supplied in a free software
// license as long as each source file so linked contains this exclusion
// and the unalrtered Aculab source files are made available.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".


#include "driver.h"

#ifdef    CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

bool AculabTrunk::playHandler(TrunkEvent *event)
{
	char *fn;
	struct tm *dt, tbuf;
	struct stat ino;
	char buffer[32];
	
slog(Slog::levelDebug)<<name<<": playHandler - got event "<<event->id<<endl;

	switch(event->id)
	{
	case TRUNK_STOP_STATE:
		_stopping_state = true;
		endTimer();
		StopPlayFile();
		//Trunk::flags.dsp=DSP_MODE_INACTIVE;
		//handler = &AculabTrunk::stepHandler;
		return true;
	case TRUNK_AUDIO_IDLE:
		endTimer();
		endPlay();
		Trunk::flags.dsp=DSP_MODE_INACTIVE;

		Trunk::flags.audio=false;
		if(!_stopping_state)
			trunkSignal(TRUNK_SIGNAL_STEP);

		handler = &AculabTrunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		_stopping_state = false;
		enterState("play");
		status[tsid] = 'p';

		if(!Trunk::flags.offhook) {
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler=&AculabTrunk::stepHandler;
			return true;
		}

		setSymbol(SYM_PLAYED, "0");
		setSymbol(SYM_OFFSET, "0");
		Trunk::setDTMFDetect();

		if(tts)
			if(!tts->synth(id, &data))
			{
				trunkSignal(TRUNK_SIGNAL_ERROR);
				handler = &AculabTrunk::stepHandler;
				return true;
			}

		if(data.play.maxtime)
			setTimer(data.play.maxtime);
retry:
		filename = getPlayfile();
		if(!filename && data.play.lock)
		{
			cachelock.unlock();
			data.play.lock = false;
		}
		if(!filename && data.play.mode != PLAY_MODE_ANY) {
			slog(Slog::levelError) <<name<< ": no file to play" << endl;
			setSymbol(SYM_ERROR, "play-failed");
			trunkSignal(TRUNK_SIGNAL_ERROR);
		}
		if(!filename && data.play.mode == PLAY_MODE_ANY) {
			trunkSignal(TRUNK_SIGNAL_STEP);
		}

		if(!filename) {
			handler = &AculabTrunk::stepHandler;
			return true;
		}

		stat(filename, &ino);
		/* Make sure we only open files (ie: no directories) */
		//if (!S_ISREG(ino.st_mode) && !S_ISFIFO(ino.st_mode)) {
		//	slog(Slog::levelError) <<name<<": " << filename << ": cannot open - not a file" << endl;
		//	setSymbol(SYM_ERROR, "play-failed");
		//	trunkSignal(TRUNK_SIGNAL_ERROR);
		//	handler = &AculabTrunk::stepHandler;
		//	return true;
		//}

		close();
		open(filename);
		if(data.play.lock)
		{
			cachelock.unlock();
			data.play.lock = false;
		}
		if(!isOpen()) {
			if((data.play.mode == PLAY_MODE_ANY) ||
			   (data.play.mode == PLAY_MODE_ONE)) {
				goto retry;
			}
			errlog("missing", "Prompt=%s", filename);
			slog(Slog::levelError) <<name<<": " << filename << ": cannot open" << endl;
			setSymbol(SYM_ERROR, "play-failed");
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &AculabTrunk::stepHandler;
			return true;
		}
		if(data.play.mode == PLAY_MODE_TEMP) {
			remove(filename);
		}
		dt = localtime_r(&ino.st_ctime, &tbuf);
		sprintf(buffer, "%04d%02d%02d,%02d%02d%02d",
			dt->tm_year + 1900, dt->tm_mon + 1, dt->tm_mday,
			dt->tm_hour, dt->tm_min, dt->tm_sec);
		setSymbol(SYM_CREATED, buffer);
		fn = getAnnotation();
		if(fn)
			setSymbol(SYM_ANNOTATION, fn);
		else
			setSymbol(SYM_ANNOTATION, "");

		StartPlayFile();
		Trunk::flags.dsp=DSP_MODE_VOICE;
		return true;
	}
	return false;
}

bool AculabTrunk::playwaitHandler(TrunkEvent *event)
{
	char buffer[32];

	switch(event->id)
	{
	case TRUNK_EXIT_SHELL:
		if(!tgi.pid)
			return true;
		tgi.pid = 0;
		endTimer();
		if(event->parm.status)
                {
                        trunkSignal(TRUNK_SIGNAL_ERROR);
			sprintf(buffer, "play-failed-exit-%d",
				event->parm.status);
			setSymbol(SYM_ERROR, buffer);
			handler = &AculabTrunk::stepHandler;
			return true;
		}
		handler = &AculabTrunk::playHandler;
                return true;
	case TRUNK_TIMER_EXPIRED:
		if(tgi.pid)
		{
			kill(tgi.pid, SIGTERM);
			tgi.pid = 0;
		}
		sprintf(buffer, "play-failed-timeout");
		setSymbol(SYM_ERROR, buffer);
		trunkSignal(TRUNK_SIGNAL_ERROR);
		handler = &AculabTrunk::stepHandler;
		return true;                                                  
	case TRUNK_ENTER_STATE:
		enterState("playwait");
		endTimer();
		Trunk::setDTMFDetect();
		setTimer(data.play.timeout);
		return true;
        }
        return false;                                                            
}

#ifdef    CCXX_NAMESPACES
};
#endif
