// Copyright (C) 2000 Open Source Telecom Corporation.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

bool AculabTrunk::listenHandler(TrunkEvent *event)
{
#ifdef HAVE_ACULAB_ASR
	Symbol *sym = (Symbol *)acuasr.result;
	char result[512];
	char *tmpres = result;
	char intres[512];
	float confidence;
	char confstr[12];
	int i = 0;

	switch(event->id)
	{
	case TRUNK_ASR_VOICE:
		return true;
	case TRUNK_ASR_TEXT:
		try
		{
			asrSession->getResults(result, &confidence);
		}
		catch(AculabASRException e)
		{
			e.reportError();
			setSymbol(SYM_ERROR, "listed-failed");
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &AculabTrunk::stepHandler;
			return true;
		}
		if(acuasr.tmpltype == TEMPLATE_DIGITS)
		{
			while(*tmpres)
			{
				if(isdigit(*tmpres))
					intres[i++] = *tmpres;
				*tmpres++;
			}
			intres[i] = '\0';
			trunk->setSymbol(SYM_ASRRESULT, intres);
		}
		else
			trunk->setSymbol(SYM_ASRRESULT, result);

		snprintf(confstr, sizeof(confstr), "%f", confidence);
		trunk->setSymbol(SYM_ASRCONF, confstr);
		if(!trunkSignal(TRUNK_SIGNAL_EVENT))
			trunkSignal(TRUNK_SIGNAL_STEP);
	case TRUNK_STOP_STATE:
		endTimer();
		stopASR();
		handler = &AculabTrunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		_stopping_state = false;
		enterState("listen");
		status[tsid] = 'l';
		try
		{
			asrSession = aculabivr.getASRSession();
			asrSession->start(this, acuasr.tmpl, acuasr.on_frames, acuasr.timeout,
				acuasr.pause_frames, acuasr.hold_frames);
		}
		catch(AculabASRException e)
		{
			asrSession = NULL;
			e.reportError();
			setSymbol(SYM_ERROR, "listen-failed-no-session");
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &AculabTrunk::stepHandler;
			return true;
		}

		if(acuasr.barge_in)
		{
			// We're going to operate async for barge in support
			trunkSignal(TRUNK_SIGNAL_STEP);
			handler = &AculabTrunk::stepHandler;
			return true;
		}
		return true;
	case TRUNK_TIMER_EXPIRED:
		break;
	}
	return false;
#endif
}

bool AculabTrunk::bargeinHandler(TrunkEvent *event)
{
#ifdef HAVE_ACULAB_ASR
	Symbol *sym = (Symbol *)acuasr.result;
	char result[512];
	char *tmpres = result;
	char intres[512];
	float confidence;
	char confstr[16];
	int i = 0;

	slog(Slog::levelDebug) << name << ": BARGE IN " << event->id << endl;
	switch(event->id)
	{
	case TRUNK_ASR_VOICE:
		return true;
	case TRUNK_ASR_TEXT:
		try
		{
			asrSession->getResults(result, &confidence);
		}
		catch(AculabASRException e)
		{
			e.reportError();
			setSymbol(SYM_ERROR, "listen-failed");
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &AculabTrunk::stepHandler;
			return true;
		}
                if(acuasr.tmpltype == TEMPLATE_DIGITS)
                {
                        while(*tmpres)
                        {
                                if(!isspace(*tmpres))
                                        intres[i++] = *tmpres;
                                *tmpres++;
                        }
			intres[i] = '\0';
			printf("************* INT RES %s\n", intres);
                        trunk->setSymbol(SYM_ASRRESULT, intres);
                }
                else
                        trunk->setSymbol(SYM_ASRRESULT, result);

		snprintf(confstr, sizeof(confstr), "%f", confidence);
		slog(Slog::levelDebug) << name << ": asr result=" << result << 
			", confidence=" << confidence << endl;
		trunk->setSymbol(SYM_ASRCONF, confstr);
		if(!trunkSignal(TRUNK_SIGNAL_EVENT))
			trunkSignal(TRUNK_SIGNAL_STEP);
	case TRUNK_STOP_STATE:
		endTimer();
		stopASR();
		handler = &AculabTrunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		_stopping_state = false;
		enterState("listen");
		status[tsid] = 'l';
		return true;
	}
	return false;
#endif
}

#ifdef	CCXX_NAMESPACES
};
#endif
