// Copyright (C) 2002 David Kerry.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne
// with the Aculab telephony libraries to produce a executable image
// without requiring Aculab's libraries to be supplied in a free software
// license as long as each source file so linked contains this exclusion
// and the unalrtered Aculab source files are made available.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

// 
// $Id: atimer.cpp,v 1.8 2002/12/11 04:38:16 dyfet Exp $
// 
// Background thread to fire off timer expiration events to
// individual trunk members.
// 
// TODO/FIXME - Should this use a separate 'timer' queue to
// 		bypass the normal command/event queue?  This
// 		would give more timely notifcation of timer
// 		expiration...
// 

#include "driver.h"

#ifdef    CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

AculabTimer::AculabTimer() :
Thread(keythreads.priService()), Mutex()
{
	cond=new Conditional();
	queue=aculabivr.queue;
	active=false;
	bypass=false;
}

AculabTimer::~AculabTimer()
{
	if(!active)
		return;

	slog(Slog::levelDebug) << "timer thread exiting"<<endl;

	active = false;
	terminate();
	delete cond;
	cond=NULL;
}

void AculabTimer::notify()
{
	cond->enterMutex();
	bypass=true;
	cond->signal(false);
	cond->leaveMutex();
}

/*
 * Iterate through the entire list of trunks looking for
 * those with a timer set.
 *
 * Of those, post a TIMER_EXPIRED event to the trunk if the
 * timer has expired.
 */
void AculabTimer::run(void)
{
	AculabTrunkEvent *aevent;
        AculabTrunk *trunk;
	int n;
	int max_trunks;
	int delay;
	long maxtime;
	int expires;

	slog(Slog::levelInfo) << "timer thread running..." << endl;

	active = true;

	max_trunks = aculabivr.port_count * MAXTIMESLOTS;

	while(active) {
		delay=30000;

		for (n=0; n < max_trunks; n++) {
			trunk=aculabivr.trunks[n];
			if ((trunk) && trunk->isActive()) {
				maxtime=trunk->getMaxTime() * 1000;
				expires=trunk->getTimer();
				if (expires == 0) {
//					slog(Slog::levelDebug)<<"timer expired on trunk"<<endl;
					trunk->endTimer();
					aevent=new AculabTrunkEvent();
					aevent->trunk=trunk;
					aevent->event.id=TRUNK_TIMER_EXPIRED;
					queue->push(aevent);
				}
				if (maxtime == 0) {
					slog(Slog::levelDebug)<<"max-script-life timer expired on trunk"<<endl;
					aevent=new AculabTrunkEvent();
					aevent->trunk=trunk;
					aevent->event.id=TRUNK_TIMER_EXIT;
					queue->push(aevent);
				}
				else if ((expires > 0) && (expires < delay)) {
					delay=expires;
				}

				if ((maxtime > 0) && (maxtime < delay)) {
					delay=maxtime;
				}
			}
		}
		cond->enterMutex();
		if (!bypass) {
			cond->wait(delay);
			cond->leaveMutex();
		}
		else {
			bypass=false;
			cond->leaveMutex();
		}
	}
	Thread::sleep(50);
}



#ifdef    CCXX_NAMESPACES
};
#endif
