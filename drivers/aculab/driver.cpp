// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne 
// with the Aculab telephony libraries to produce a executable image
// without requiring Aculab's sources to be supplied in a free software
// license long as each source file so linked contains this exclusion
// and the unalrtered Aculab source files are also provided.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

//
// General Aculab driver overview
// ------------------------------
//
// At this time, I only have Prosody PCI/T1/PRI cards to work with
// so some assumptions may be incorrect for other hardware produced
// by Aculab and supported by their drivers.
//
// Each 'port' on an Aculab card is a T1 interface.  Each T1/E1
// interface can support 24 (T1) or 30 (E1) simultaneous phone
// calls (channels).  Currently the highest density PCI Aculab card
// is the Prosody with a PM4 module which gives 4 ports, for a total
// capacity of 96 T1 or 120 E1 channels per board.  There can be multiple
// Prosody cards in one chassis.  I don't know what the limit is at
// this time, but I suspect it is limited by available PCI slots
// more than anything else.  My development system has 2 4-port
// cards installed, with room for 2 more.
//
// Because of the very high density involved with these cards,
// doing a simple thread-per-trunk(channel) mapping here in the
// driver rapidly gets out of hand as you add more cards.  Take
// a 16-port machine for example.  That would give 384(T1) or
// 480(E1) threads!  The machine will rapidly be running out of
// resources at that point.  Also, in the case of Compact-PCI systems,
// it's possible to cram the machine full of ports, which could
// easily double this number.
//
// To economize on resources, the main driver thread is responsible
// for receiving events from the Aculab driver and dispatching
// them to worker threads through a small queue class.  The optimal
// number of worker threads has yet to be determined, but I imagine
// one per port should suffice.
//
// There are 3 additional threads used by this driver:
//
// o An audio processing thread.  There is one of these per driver
//   and is responsible for feeding audio to/from the hardware
// o A dsp event monitor thread.  This pretty much only listens for
//   DTMF events at the moment.  There is one of these per driver.
// o A timer event thread.  Any time a timer is set, this thread
//   sits and waits for it to expire, posting an event when it does.
//
// See thread.c and atimer.c for more details.
//
// NOTE: The Aculab driver can operate in either state-driven
//       mode or event-driven.  For simplicity of the code, the
//       event driven model is being used.  At some point it may
//       be worth re-examining the issue to see if state-driven
//       provides any advantages.

#include "driver.h"

#ifdef    CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

AculabConfig::AculabConfig() :
Keydata("/bayonne/aculab")
{
	static Keydata::Define defkeys[] = {
	{"prifirmware", "ni2_usr.p4r"},
	{"brifirmware", "ets_bnet.ram"},
	{"prosody", "/home/aculab/dtk111/speech/download/speech/sp30a.smf"},
	{"briconfig", ""},
	{"priconfig", ""},
	{"netformat", "alaw"},
	{"buffersize","256"},
	{"asr","0"},
	{"asrusecontroller","0"},
	{"asrhost","127.0.0.1"},
	{"asrport","2201"},
	{"asrlexicon","eng-us-full.rlf"},
	{"asrmodelset","eng-us-enhanced.rmf"},
	{"asrtriphoneset","eng-us-enhanced.rtf"},
	{"asrrules","eng-us-full.rrf"},
	{"apitrace","0"},
	{NULL, NULL}};

	if(isFHS())
		load("/drivers/aculab");

	load(defkeys);
}

unsigned AculabConfig::getMaxPorts(void)
{
	const char *val = getLast("maxports");
	int v;
	if(!val)
		return MAXPORT;

	v = atoi(val);
	if(v > MAXPORT)
		return MAXPORT;

	return v;
}

kSMTimeslotType AculabConfig::getNetFormat(void)
{
	const char *val = getLast("netformat");

	if(!val)
		return kSMTimeslotTypeMuLaw;

	if(!stricmp(val, "alaw"))
		return kSMTimeslotTypeALaw;
	else
		return kSMTimeslotTypeMuLaw;
}

AculabDriver::AculabDriver() :
Driver(), AculabConfig(), Thread(keythreads.priService())
{
	running = false;
	port_count = 0;
	unsigned long mask;
	unsigned port, ts;
//	AculabTrunk *trunk;

	if(!stricmp(getLast("apitrace"), "1") || !stricmp(getLast("apitrace"), "on"))
		TiNGtrace = 3;
#if 0
	int rc, x;
	char *mykey[128];
	rc=keytones.getIndex(mykey,127);

	/* Setup custom-defined tones */
	for (x=0; x < rc; x++) {
		if (!stricmp(mykey[x],"tones")) {
			continue;
		}
		slog(Slog::levelDebug)<<"GETTING tone for KEY="<<mykey[x]<<endl;
		/*
		 * TODO TODO - convert phTone into an Aculab custom tone
		 * parameter and let DSP do tone generation...
		 */
	}
#endif

	// V6FIXME
	//if(InitNetworks()) {
	//	if(InitProsody()) {
	//		// V6FIXME
	//		//port_count = call_nports();
	//		port_count = 1;
	//	}
	//}
	port_count = mapCards();	

#ifdef HAVE_ACULAB_ASR
	if(!stricmp(getLast("asr"), "1") || !stricmp(getLast("asr"), "on"))
	{
		try
		{
			recogniser = new AculabASR();
			recogniser->start();
		}
		catch(AculabASRException e)
		{
			e.reportError();
			recogniser = NULL;
		}
	}
	else
		recogniser = NULL;
#endif

	if(port_count < 0)
		port_count = 0;

	if(!port_count) {
		slog(Slog::levelCritical) << "Aculab driver failure; no ports" << endl; 
		trunks = NULL;
		groups = NULL;
		imaps = NULL;
		omaps = NULL;
		qthreads = NULL;
		throw((Driver *)this);
	}

	qthreads = NULL;
	audiothread = NULL;

	imaps = new AculabTrunk *[port_count * MAXCHANNELS];
	omaps = new AculabTrunk *[port_count * MAXCHANNELS];
	ixmaps = new AculabTrunk *[MAX_CH_IDX];
	tsmap = new char[port_count * MAXTIMESLOTS];
	groups = new TrunkGroup *[port_count * MAXTIMESLOTS];
	trunks = new AculabTrunk *[port_count * MAXTIMESLOTS];

	memset(imaps, 0, sizeof(AculabTrunk *) * port_count * MAXCHANNELS);
	memset(omaps, 0, sizeof(AculabTrunk *) * port_count * MAXCHANNELS);
	memset(ixmaps, 0, sizeof(AculabTrunk *) * MAX_CH_IDX);
	memset(tsmap, 0, sizeof(char) * port_count * MAXTIMESLOTS);
	memset(groups, 0, sizeof(TrunkGroup *) * port_count * MAXTIMESLOTS);
	memset(trunks, 0, sizeof(AculabTrunk *) * port_count * MAXTIMESLOTS);
	memset(MVIP,0,sizeof(MVIP));

	mvipMutex=new Mutex();

	queue = new AculabFifo();

	// V6FIXME
	// call_signal_info() is replaced with call_port_info(), but must be
	// called on a per port basis, not a whole system basis
	//call_signal_info(&siginfo[0]);

	/*
	 * Determine what timeslots are available
	 */

	int c, t=0;
	for(c = 0; c < total_cards; c++)
	{
		for(port = 0; port < cards[c]->ports_count; port++)
		{
			++t;
			slog(Slog::levelInfo) << "aculab(" <<port<<"): signalling system: "
				<< cards[c]->ports[port]->port_info.sig_sys << endl;
			mask = 1L;
			for(ts = 0; ts < MAXTIMESLOTS; ++ts) {
				if(mask & cards[c]->ports[port]->port_info.valid_vector)
				{
					tsmap[t * MAXTIMESLOTS + ts] = 1;
				}
				mask <<= 1L;
			}
		}
	}
	// V6FIXME
	// needs to be created after fixing the fixme above
	/*
	for(port = 0; port < port_count; ++port) {
		slog(Slog::levelInfo) << "aculab("<<port<<",) signalling system: "
				<< siginfo[port].sigsys <<endl;
		mask = 1L;
		for(ts = 0; ts < MAXTIMESLOTS; ++ts) {
			if(mask & siginfo[port].validvector) {
				tsmap[port * MAXTIMESLOTS + ts] = 1;
			}
			mask <<=1L;
		}
	}*/

	slog(Slog::levelInfo) << "Aculab driver loaded; capacity="
			<< port_count * MAXCHANNELS << " channels" <<  endl;
}

AculabDriver::~AculabDriver()
{
	if(running)
		terminate();

	if(tsmap)
		delete[] tsmap;

	if(trunks)
		delete[] trunks;

	if(groups)
		delete[] groups;

	if(imaps)
		delete[] imaps;

	if(omaps)
		delete[] omaps;

}

interface_t AculabDriver::getInterface(unsigned port)
{
	if(port >= port_count)
		return INVALID_PORT;

	return interfaces[port];
}

void AculabDriver::setChannel(AculabTrunk *trunk)
{
	if(trunk->channel >= MAXCHANNELS)
		return;

	if (call_handle_2_io(trunk->handle) == 0) { /* Incoming call */
		imaps[trunk->port * MAXCHANNELS + trunk->channel] = trunk;
	}
	else {
		omaps[trunk->port * MAXCHANNELS + trunk->channel] = trunk;
	}
	if (trunk->dspChannel != kSMNullChannelId) {
		ixmaps[sm_get_channel_ix(trunk->dspChannel)] = trunk;
	}
}

void AculabDriver::clrChannel(AculabTrunk *trunk)
{
	if(trunk->channel >= MAXCHANNELS)
		return;

	if (call_handle_2_io(trunk->handle) == 0) { /* Incoming call */
		imaps[trunk->port * MAXCHANNELS + trunk->channel] = NULL;
	}
	else {
		omaps[trunk->port * MAXCHANNELS + trunk->channel] = NULL;
	}
	if(trunk->dspChannel != kSMNullChannelId)
		ixmaps[sm_get_channel_ix(trunk->dspChannel)] = NULL;
}

bool AculabDriver::getMVIPslot(int *stream, int *slot)
{
	int mvip_stream=0;
	int mvip_slot=0;

	mvipMutex->enterMutex();
	while(MVIP[(mvip_stream * MVIP_FD_STREAMS) + mvip_slot]) {
		if(mvip_slot==(MVIP_TIMESLOTS-1)) {
			mvip_slot=0;
			if(mvip_stream==(MVIP_FD_STREAMS-1)) {
				return false;
			}
			else {
				mvip_stream++;
			}
		}
		else {
			mvip_slot++;
		}
	}
	MVIP[(mvip_stream * MVIP_FD_STREAMS) + mvip_slot]=1;
	mvipMutex->leaveMutex();

	*stream=mvip_stream;
	*slot=mvip_slot;

	return true;
}


void AculabDriver::freeMVIPslot(int stream, int slot)
{
	mvipMutex->enterMutex();
	MVIP[(stream * MVIP_FD_STREAMS) + slot]=0;
	mvipMutex->leaveMutex();
}

bool AculabDriver::InitProsody(void)
{
	/*ACU_MODULE_ID module;
	int rc;
	ACU_OPEN_CARD_PARMS card_parms;
	ACU_OPEN_PROSODY_PARMS prosody_parms;
	ACU_CARD_INFO_PARMS card_info;
	SM_OPEN_MODULE_PARMS module_parms;

	INIT_ACU_STRUCT(&card_parms);
	rc = acu_open_card(&card_parms);
	if(rc)
	{
		slog(Slog::levelCritical) << "Failed to open card: " << rc << endl;
		return false;
	}

	INIT_ACU_STRUCT(&prosody_parms);
	prosody_parms.card_id = card_parms.card_id;
	rc = acu_open_prosody(&prosody_parms);
	if(rc)
	{
		slog(Slog::levelCritical) << "Failed to open prosody: " << rc << endl;
		return false;
	}

	INIT_ACU_CL_STRUCT(&card_info);
	card_info.card_id = card_parms.card_id;
	rc = call_get_card_info(&card_info);
	if(rc)
	{
		slog(Slog::levelCritical) << "Failed to get card info: " << rc << endl;
		return false;
	}

	memset(&module_parms, 0, sizeof(module_parms));
	module_parms.card_id = card_parms.card_id;
	rc = sm_open_module(&module_parms);
	if(!rc)
	{
		slog(Slog::levelCritical) << "Failed to open module: " << rc << endl;
		return false;
	}*/

	return true;
}

unsigned int AculabDriver::mapCards(void)
{
	unsigned total_switches = 0, total_ports = 0;
	ACU_UINT c;
	ACU_SNAPSHOT_PARMS snapshot_parms;
	ACU_OPEN_CARD_PARMS open_card;
	ACU_APP_CONTEXT_TOKEN_PARMS token_parms;
	ACU_CARD_INFO_PARMS card_info;
	ACU_ERR result, r;

	unsigned int this_card = 0;
	unsigned int this_module = 0;

	INIT_ACU_STRUCT(&snapshot_parms);
	result = acu_get_system_snapshot(&snapshot_parms);
	if(result != 0)
	{
		slog(Slog::levelError) << "Aculab failure: unable to get system snapshot!" << endl;
		return false;
	}

	total_cards = snapshot_parms.count;

	cards = new AculabCardData *[snapshot_parms.count];
	modules = new AculabModuleData *[MAXMODULES];

	for(c = 0; c < snapshot_parms.count; c++)
	{
		cards[this_card] = new AculabCardData;
		cards[this_card]->ports_count = 0;

		slog(Slog::levelInfo) << "Aculab: found card " << snapshot_parms.serial_no[c] << endl;
		cards[this_card]->serial = snapshot_parms.serial_no[c];
		INIT_ACU_STRUCT(&open_card);
		strcpy(open_card.serial_no, snapshot_parms.serial_no[c]);
		result = acu_open_card(&open_card);
		if(result != 0 || open_card.card_id < 0)
		{
			slog(Slog::levelError) << "Aculab failure: unable to open card " <<
				snapshot_parms.serial_no[c] << endl;
			return 0;
		}
		cards[this_card]->card_id = open_card.card_id;
		
		INIT_ACU_STRUCT(&token_parms);

		token_parms.resource_id = cards[this_card]->card_id;
		token_parms.app_context_token = (ACU_ACT)cards[this_card];

		result = acu_set_card_app_context_token(&token_parms);
		if(result != 0)
		{
			slog(Slog::levelError) << "Aculab failure: unable to set app context token on card " <<
				snapshot_parms.serial_no[c] << endl;
			return 0;
		}

		INIT_ACU_STRUCT(&card_info);
		card_info.card_id = cards[this_card]->card_id;
		result = acu_get_card_info(&card_info);
		if(result != 0)
		{
			slog(Slog::levelError) << "Aculab failure: unable to get card info for card " <<
				snapshot_parms.serial_no[c] << endl;
			return 0;
		}
		if(card_info.resources_available & ACU_RESOURCE_CALL)
		{
			ACU_OPEN_CALL_PARMS open_call;
			INIT_ACU_STRUCT(&open_call);
			open_call.card_id = cards[this_card]->card_id;
			result = acu_open_call(&open_call);
			if(result != 0)
			{
				slog(Slog::levelError) << "Aculab failure: unable to open call control on card " <<
					snapshot_parms.serial_no[c] << endl;
				return 0;
			}

			CARD_INFO_PARMS cdi_parm;
			INIT_ACU_STRUCT(&cdi_parm);
			cdi_parm.card_id = cards[this_card]->card_id;
			result = call_get_card_info(&cdi_parm);
			if(result != 0)
			{
				slog(Slog::levelError) << "Aculab failure: call_get_card_info failed on card " <<
					snapshot_parms.serial_no[c] << endl;
				return 0;
			}
			cards[this_card]->ports_count = cdi_parm.ports;
			cards[this_card]->ports = new AculabPortData *[cdi_parm.ports];

			int p;
			for(p = 0; p < cdi_parm.ports; p++)
			{
				OPEN_PORT_PARMS port_parm;
				INIT_ACU_CL_STRUCT(&port_parm);
				port_parm.card_id = cards[this_card]->card_id;
				port_parm.port_ix = p;
				result = call_open_port(&port_parm);
				if(result != 0)
				{
					slog(Slog::levelError) << "Aculab failure: call_open_port failed on card " <<
						snapshot_parms.serial_no[c] << ", port " << p << endl;
					return 0;
				}
				cards[this_card]->ports[p] = new AculabPortData;
				cards[this_card]->ports[p]->port_ix = p;
				cards[this_card]->ports[p]->port_id = port_parm.port_id;
				cards[this_card]->ports[p]->system_ix = p + total_ports;

				PORT_INFO_PARMS pi_parm;
				INIT_ACU_CL_STRUCT(&pi_parm);
				pi_parm.port_id = port_parm.port_id;
				result = call_port_info(&pi_parm);
				if(result != 0)
				{
					slog(Slog::levelError) << "Aculab failure: call_port_info failed on card " <<
						snapshot_parms.serial_no[c] << ", port " << p << endl;
					return 0;
				}
				memcpy(&cards[this_card]->ports[p]->port_info, &pi_parm, sizeof(PORT_INFO_PARMS));
			}
		}
		if(card_info.resources_available & ACU_RESOURCE_SWITCH)
		{
			ACU_OPEN_SWITCH_PARMS open_switch;
			INIT_ACU_STRUCT(&open_switch);
			open_switch.card_id = cards[this_card]->card_id;
			result = acu_open_switch(&open_switch);
			if(result != 0)
			{
				slog(Slog::levelError) << "Aculab failure: unable to open switch control on card " <<
					snapshot_parms.serial_no[c] << endl;
				return 0;
			}
		}
		if(card_info.resources_available & ACU_RESOURCE_SPEECH)
		{
			ACU_OPEN_PROSODY_PARMS open_prosody;
			INIT_ACU_STRUCT(&open_prosody);
			open_prosody.card_id = cards[this_card]->card_id;
			result = acu_open_prosody(&open_prosody);
			if(result != 0)
			{
				slog(Slog::levelError) << "Aculab failure: unable to open Prosody on card " <<
					snapshot_parms.serial_no[c] << endl;
				return 0;
			}

			SM_CARD_INFO_PARMS card_info_parm;
			SM_OPEN_MODULE_PARMS open_module_parm;
			int m;

			INIT_ACU_SM_STRUCT(&card_info_parm);
			//card_info_parm.card = cards[this_card]->card_id;
			card_info_parm.card = open_card.card_id;
			result = sm_get_card_info(&card_info_parm);
			if(result != 0)
			{
				slog(Slog::levelError) << "Aculab failure: unable to get Prosody module info on card " <<
					snapshot_parms.serial_no[c] << " rc=" << result << endl;
				return 0;
			}
			modules[this_module] = new AculabModuleData;
			for(m = 0; m < card_info_parm.module_count; m++)
			{
				INIT_ACU_SM_STRUCT(&open_module_parm);
				open_module_parm.module_ix = m;
				open_module_parm.card_id = cards[this_card]->card_id;
				result = sm_open_module(&open_module_parm);
				if(result != 0)
				{
					slog(Slog::levelError) << "Aculab failure: unable to open Prosody module " <<
						m << " on card " << snapshot_parms.serial_no[c] << endl;
					return 0;
				}
				modules[this_module]->module = m;
				modules[this_module]->module_id = open_module_parm.module_id;
			}
			slog(Slog::levelInfo) << "Aculab: " << snapshot_parms.serial_no[c] << " has " << m << 
				" Prosody modules"  << endl;
		}
		
		ACU_CARD_INFO_PARMS aci_parm;
		INIT_ACU_STRUCT(&aci_parm);
		aci_parm.card_id = cards[this_card]->card_id;
		result = acu_get_card_info(&aci_parm);
		if(result != 0)
		{
			slog(Slog::levelError) << "Aculab failure: acu_get_card_info failed on card " <<
				snapshot_parms.serial_no[c] << endl;
			return 0;
		}
		switch(aci_parm.card_type)
		{
		case ACU_PROSODY_PCI_CARD:
			slog(Slog::levelInfo) << "Aculab: detected Aculab Prosody PCI card [serial=" <<
				snapshot_parms.serial_no[c] << "]" << endl;
			break;
		case ACU_PROSODY_CPCI_CARD:
			slog(Slog::levelInfo) << "Aculab: detected Aculab Prosody cPCI card [serial=" <<
				snapshot_parms.serial_no[c] << "]" << endl;
			break;
		case ACU_PROSODY_S_CARD:
			slog(Slog::levelInfo) << "Aculab: detected Aculab Prosody S software [serial=" <<
				snapshot_parms.serial_no[c] << "]" << endl;
			break;
		case ACU_PROSODY_X_CARD:
			slog(Slog::levelInfo) << "Aculab: detected Aculab Prosody X card [serial=" <<
				snapshot_parms.serial_no[c] << "] [ip=" << aci_parm.ip_address << "]" << endl;
			break;
		default:
			slog(Slog::levelError) << "Aculab: unknown card type " << aci_parm.card_type << endl;
			return 0;
		}
		slog(Slog::levelInfo) << "Aculab: " << snapshot_parms.serial_no[c] << " has " << 
			cards[this_card]->ports_count << " network ports" << endl;
		total_ports += cards[this_card]->ports_count;
	}

	return total_ports;
}

bool AculabDriver::InitNetworks(void)
{
	int total_switches;
	unsigned this_port, total_ports = 0;

	// V6NEW
	ACU_SNAPSHOT_PARMS snapshot_parms;
	ACU_ERR result, r;
	ACU_UINT c;

	INIT_ACU_STRUCT(&snapshot_parms);
	result = acu_get_system_snapshot(&snapshot_parms);
	if(result == 0)
	{
		for(c = 0; c < snapshot_parms.count; c++)
		{
			slog(Slog::levelInfo) << "Aculab: found card serial " <<
				snapshot_parms.serial_no[c] << endl;

			ACU_OPEN_CARD_PARMS open_card_parms;

			INIT_ACU_STRUCT(&open_card_parms);
			strncpy(open_card_parms.serial_no, snapshot_parms.serial_no[c], ACU_MAX_SERIAL);

			r = acu_open_card(&open_card_parms);
			if(r != 0)
			{
				slog(Slog::levelError) << "Aculab Failure: unable to open card " << 
					open_card_parms.serial_no << endl;
				return false;
			}
			
			ACU_CARD_INFO_PARMS card_info_parms;
			card_info_parms.card_id = open_card_parms.card_id;
			r = acu_get_card_info(&card_info_parms);

			if(r == 0)
			{
				if(card_info_parms.resources_available & ACU_RESOURCE_SWITCH)
				{
					total_switches++;
				}
				if(card_info_parms.resources_available & ACU_RESOURCE_CALL)
				{
					ACU_OPEN_CALL_PARMS open_call_parms;
					CARD_INFO_PARMS info_parms;
					
					INIT_ACU_CL_STRUCT(&open_call_parms);
					open_call_parms.card_id = card_info_parms.card_id;
					r = acu_open_call(&open_call_parms);
					if(r != 0)
					{
						slog(Slog::levelError) << "Aculab failure: error opening card " <<
							open_call_parms.card_id << " for call control" << endl;
						return false;
					}

					INIT_ACU_CL_STRUCT(&info_parms);

					info_parms.card_id = card_info_parms.card_id;
					r = call_get_card_info(&info_parms);
					if(r != 0)
					{
						slog(Slog::levelError) << "Aculab failure: error getting card info for " <<
							card_info_parms.card_id << endl;
						return false;
					}

					total_ports += info_parms.ports;

				}
				if(card_info_parms.resources_available & ACU_RESOURCE_SPEECH)
				{
				}
			}
		}
	}
				
		
//	struct sysinfo_xparms sysinfo;
	struct restart_xparms restart_xparms;

	int rc;

	pri_count = bri_count = 0;

	if(total_switches < 1) {
		slog(Slog::levelError) << "Aculab Failure: no switch drivers" << endl;
		return false;
	}

	slog(Slog::levelInfo) << "Aculab: " << total_ports
			<< " network ports available" << endl;

	for(this_port = 0; this_port < getMaxPorts(); ++this_port) {
		interfaces[this_port] = INVALID_PORT;
	}

	for(this_port = 0; this_port < total_ports; ++this_port) {
		
		if(call_line(this_port) == L_BASIC_RATE) {
			interfaces[this_port] = BRI_PORT;
			++bri_count;
			restart_xparms.filenamep = (char *)getBriFirmware();
			restart_xparms.config_stringp = (char *)getBriConfig();
		}
		else {
			interfaces[this_port] = PRI_PORT;
			++pri_count;

			char *firmware=(char *)getPriFirmware(this_port);
			if (firmware == NULL) {
				restart_xparms.filenamep = (char *)getPriFirmware();
			}
			else {
				restart_xparms.filenamep = firmware;
			}
			restart_xparms.config_stringp = (char *)getPriConfig();
			if (restart_xparms.config_stringp == NULL) {
				restart_xparms.config_stringp="";
			}
		}

		if(call_is_download(this_port)) {
			slog(Slog::levelInfo) << "Aculab(" << this_port
				<< "): loading '" << restart_xparms.filenamep
				<< "'/'"<<restart_xparms.config_stringp
				<< "' to port  " << this_port << endl;

			restart_xparms.net = this_port;
			rc = call_restart_fmw(&restart_xparms);
			if(rc) {
				slog(Slog::levelError) << "Aculab(" << this_port
					<< "): firmware load failure: " << rc << endl;
				interfaces[this_port] = FAILED_PORT;
				return false;
			}
		}
	}

	//if(system_init()) {
	//	slog(Slog::levelError) << "Aculab: system init failure" << endl;
	//	return false;
	//}
	return true;
}

int AculabDriver::start(void)
{
	int count = 0;
	unsigned port, ts, id;
	int n;
	int c,p,ix;

	if(active) {
		slog(Slog::levelError) << "driver already started" << endl;
		return 0;
	}

	slog(Slog::levelDebug) << "starting " << WORKER_THREADS << " worker threads..." << endl;

	queue->enable();

	qthreads = new AculabQueueThread *[WORKER_THREADS];

	for(c = 0; c < total_cards; c++)
	{
		for(p = 0; p < cards[c]->ports_count; p++)
		{
			port = cards[c]->ports[p]->system_ix;
			for(ts = 0; ts < MAXTIMESLOTS; ++ts) {
				id = port * MAXTIMESLOTS + ts;
				if(!tsmap[port * MAXTIMESLOTS + ts])
					continue;

				trunks[id] = new AculabTrunk(cards[c], cards[c]->ports[p], ts);
				++count;
			}
		}
	}

	/*
	 * start the consumer threads - just one per port for now.
	 */
	for (n=0; n < WORKER_THREADS; n++) {
		qthreads[n]=new AculabQueueThread(queue);
		qthreads[n]->start();
	}

	/*
	 * Start the dsp event thread (must be done after all
	 * trunks are initialized).
	 */
	dspthread=new AculabDSPEventThread(queue, ixmaps);
	dspthread->start();

	/*
	 * start the audio processing thread
	 */
	audiothread=new AculabAudioThread(queue, ixmaps);
	audiothread->start();

	/*
	 * start the layer-1 monitoring thread
	 */
	l1thread=new AculabMonitorThread();
	l1thread->start();

	timer=new AculabTimer();
	timer->start();

	if (!running) {
		Thread::start();
	}
	active=true;

	slog(Slog::levelInfo) << "driver started..." << endl;
	return count;
}

void AculabDriver::stop(void)
{
	if(!active)
		return;

#ifdef HAVE_ACULAB_ASR
	if(recogniser)
	{
		slog(Slog::levelInfo) << "asr stopping..." << endl;
		try
		{
			recogniser->stop();
		}
		catch(AculabASRException e)
		{
			e.reportError();
		}
		delete recogniser;
		recogniser = NULL;
	}
#endif

	if(trunks)
		memset(trunks, 0, sizeof(AculabTrunk *) * port_count * MAXTIMESLOTS);

	active = false;
	slog(Slog::levelInfo) << "driver stopping..." << endl;

	queue->disable();

	delete[] qthreads;
	qthreads = NULL;

	delete timer;
	timer=NULL;

	delete dspthread;
	dspthread=NULL;

	delete l1thread;
	l1thread=NULL;
}

Trunk *AculabDriver::getTrunkPort(int id)
{
	if(id < 0 || id >= (int)port_count * MAXTIMESLOTS)
		return NULL;

	if(!trunks)
		return NULL;

	return (Trunk *)trunks[id];
}

// this should dispatch events thru putEvent!
void AculabDriver::run(void)
{
	struct state_xparms event_xparms;
//	struct detail_xparms detail_xparms;
//	struct cause_xparms cause;
	unsigned port, channel;
	int rc;
	AculabTrunk *trunk;

	while(active) {
		event_xparms.handle = 0;
		event_xparms.timeout = 500L;

		rc=call_event(&event_xparms);
		if(rc != 0) {
			slog(Slog::levelError) << "Aculab event failure, rc="<<rc<< endl;
			Thread::sleep(100);
			continue;
		}

		if(!event_xparms.handle)
			continue;

		port = call_handle_2_port(event_xparms.handle);
		channel = call_handle_2_chan(event_xparms.handle);

		if(port >= MAXPORT)
			continue;

		if(channel >= MAXCHANNELS)
			continue;

		slog(Slog::levelDebug)<<"got aculab event "<<event_xparms.state<<
				" on port "<<port<<",chan "<<channel<<endl;

		if (call_handle_2_io(event_xparms.handle) == 0) {
			trunk = imaps[port * MAXCHANNELS + channel];
		}
		else {
			trunk = omaps[port * MAXCHANNELS + channel];
		}
		if(!trunk) {
			slog(Slog::levelWarning)<<": NO TRUNK FOR CHANNEL!"<<endl;
			continue;
		}

		switch(event_xparms.state) {
		case EV_WAIT_FOR_INCOMING:
		case EV_WAIT_FOR_OUTGOING:
		case EV_OUTGOING_PROCEEDING:
			break;
		case EV_DETAILS:
			postEvent(trunk,TRUNK_CALL_INFO);
			break;
		case EV_INCOMING_CALL_DET:
			postEvent(trunk,TRUNK_CALL_DETECT);
			break;
		case EV_CALL_CONNECTED:
			postEvent(trunk,TRUNK_CALL_CONNECT);
			break;
		case EV_PROGRESS:
		case EV_OUTGOING_RINGING:
			postEvent(trunk,TRUNK_CPA_RINGBACK);
			break;
		case EV_HOLD:
			postEvent(trunk,TRUNK_CALL_HOLD);
			break;
		case EV_HOLD_REJECT:
			postEvent(trunk,TRUNK_CALL_NOHOLD);
			break;
		case EV_TRANSFER_REJECT: /* Need better value here? */
			postEvent(trunk,TRUNK_CALL_FAILURE);
			break;
		case EV_IDLE:
			postEvent(trunk,TRUNK_CALL_RELEASE);
			break;
		default:
			slog(Slog::levelInfo) << "Aculab("<<port<<","<<trunk->ts<<") "
					<< "Unknown event type received: "
					<< event_xparms.state << endl;
			break;
		}
	}
}

/*
 * NOTE: This must _NOT_ block for very long!!!!
 * (or we will lose hardware events - driver doesn't queue events!)
 */
void AculabDriver::postEvent(AculabTrunk *trunk, trunkevent_t id)
{
	AculabTrunkEvent *aevent;

	aevent=new AculabTrunkEvent();
	aevent->trunk=trunk;
	aevent->event.id=id;
	queue->push(aevent);
}

/*
 * Dump out relavent information about an incoming call
 */
void AculabDriver::dumpCallDetail(struct detail_xparms *details)
{
	slog(Slog::levelInfo) << "  " << ((details->calltype == INCOMING) ? "INCOMING" : "OUTGOING")
			<< " call initiated" <<endl;

	slog(Slog::levelInfo) << "  sending_complete="<<details->sending_complete<<endl;
	slog(Slog::levelInfo) << "  destination_addr='"<<details->destination_addr<<"'"<<endl;
	slog(Slog::levelInfo) << "  originating_addr='"<<details->originating_addr<<"'"<<endl;
	slog(Slog::levelInfo) << "  connected_addr  ='"<<details->connected_addr<<"'"<<endl;
	slog(Slog::levelInfo) << "  feature_info    ="<<details->feature_information<<endl;
}

#ifdef	HAVE_ACULAB_ASR
AculabASRSession *AculabDriver::getASRSession(void)
{
	if(!recogniser)
		throw(AculabASRException("no asr sessions available"));

	return recogniser->getSession();
}
#endif

AculabDriver aculabivr;

#ifdef    CCXX_NAMESPACES
};
#endif
