// Copyright (C) 2002 David Kerry.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne
// with the Aculab telephony libraries to produce a executable image
// without requiring Aculab's libraries to be supplied in a free software
// license as long as each source file so linked contains this exclusion
// and the unalrtered Aculab source files are made available.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".


#include "driver.h"

#ifdef    CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

bool AculabTrunk::dialHandler(TrunkEvent *event)
{
	struct out_xparms outparms;
	struct cause_xparms causeparms;
        int rc;
	bool rtn;

	slog(Slog::levelDebug) <<name<<":dialHandler event "<<event->id<<endl;

	switch(event->id)
	{
	case TRUNK_TIMER_EXPIRED:
		endTimer();
		trunkSignal(TRUNK_SIGNAL_HANGUP);
		handler = &AculabTrunk::stepHandler;
		return true;
#if 0
	case TRUNK_CPA_NOANSWER:
		setSymbol(SYM_TONE, "ring");
		rtn = trunkEvent("dial:noanswer");
		if(!rtn)
			rtn = trunkSignal(TRUNK_SIGNAL_NOANSWER);
		if(rtn)
		{
			handler = &AculabTrunk::stepHandler;
			return true;
		}
		event->id = TRUNK_TIMER_EXPIRED;
		return false;
#endif
	case TRUNK_CALL_RELEASE:
		endTimer();
		memset(&causeparms,0,sizeof(causeparms));
		causeparms.handle=handle;
		call_getcause(&causeparms);
		slog(Slog::levelDebug)<<name<<": clearing cause="<<causeparms.cause<<endl;
		if (causeparms.cause == LC_NUMBER_BUSY) {
			slog(Slog::levelDebug)<<name<<"line is busy"<<endl;
			setSymbol(SYM_TONE, "busy");
			if(!trunkSignal(TRUNK_SIGNAL_BUSY)) {
				if(data.dialxfer.exit) {
					trunkSignal(TRUNK_SIGNAL_HANGUP);
				}
				else {
					trunkSignal(TRUNK_SIGNAL_STEP);
				}
			}
		}
		else {
			trunkSignal(TRUNK_SIGNAL_HANGUP);
		}
		handler = &AculabTrunk::stepHandler;
		return true;
	case TRUNK_CALL_INFO:
		rc=call_details(&detail_xparms);
		if (rc != 0) {
			setSymbol(SYM_ERROR, "isdn detail error");
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &AculabTrunk::stepHandler;
		}
		return true;
	case TRUNK_CPA_RINGBACK:
		setSymbol(SYM_TONE, "ring");
		rtn = trunkEvent("dial:ringback");
		if(!rtn)
			rtn = trunkEvent("tone:ringback");
		if(rtn)
		{
			event->id = TRUNK_STOP_STATE;
			return false;
		}
	case TRUNK_CALL_CONNECT:
		detail_xparms.valid=0;
		detail_xparms.handle=handle;
		detail_xparms.timeout=500;
		if (call_details(&detail_xparms)) {
			slog(Slog::levelError) <<name<<": error retrieving outbound call details"
					<< endl;
			return false;
		}
		
		/* Connect port/timeslot with DSP channel/resources */
		if (!allocateDSP(kSMChannelTypeHalfDuplex)) {
			slog(Slog::levelError) <<name<<": couldn't allocate dsp for outgoing call"<<endl;
			setSymbol(SYM_ERROR, "dsp resource allocation error");
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &AculabTrunk::stepHandler;
			return true;
		}

		if (data.dialxfer.exit) {
			trunkSignal(TRUNK_SIGNAL_HANGUP);
		}
		else {
			trunkSignal(TRUNK_SIGNAL_STEP);
		}
		handler = &AculabTrunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		_stopping_state;
		enterState("dial");
		endTimer();
		setSymbol(SYM_TONE, "none");
		Trunk::flags.offhook = true;
		setDTMFDetect(false);
		status[tsid] = 'd';

		/*
		 * Make the outgoing isdn call.
		 */

		while((*data.dialxfer.digit=='F') ||
		      (*data.dialxfer.digit == 'W')) {
                        ++data.dialxfer.digit;
		}

		setTimer(data.dialxfer.timeout);

		memset(&outparms,0,sizeof(outparms));

		// V6FIXME: does this still have any applicability?
		/*
		switch (nettype) {
		case S_1TR6:
		case S_1TR6NET:
			outparms.unique_xparms.sig_1tr6.numbering_type=NT_NATIONAL;
			outparms.unique_xparms.sig_1tr6.numbering_plan=NP_ISDN;
			outparms.unique_xparms.sig_1tr6.service_octet =0;
			outparms.unique_xparms.sig_1tr6.add_info_octet=0;
			break;
		case S_ETS300:
		case S_ETSNET:
		case S_SWETS300:
		case S_SWETSNET:
		case S_VN3:
		case S_VN3NET:
		case S_QSIG:
		case S_AUSTEL:
		case S_AUSTNET:
		case S_TNA_NZ:
		case S_TNANET:
		case S_FETEX_150:
		case S_FETEXNET:
		case S_ATT:
		case S_ATTNET:
		case S_ATT_T1:
		case S_ATTNET_T1:
		case S_NI2:
		case S_NI2NET:
		case S_IDAP:
		case S_IDAPNET:
			outparms.unique_xparms.sig_q931.service_octet  = 0;
			outparms.unique_xparms.sig_q931.add_info_octet = 0;
			outparms.unique_xparms.sig_q931.bearer.ie[0] = 0;
			outparms.unique_xparms.sig_q931.display.ie[0]=0;
			break;
		case S_DASS:
		case S_DASSNET:
			outparms.unique_xparms.sig_dass.sic1=0x10;
			outparms.unique_xparms.sig_dass.sic2=0xff;
			break;
		case S_DPNSS:
			outparms.unique_xparms.sig_dpnss.sic1=0x10;
			outparms.unique_xparms.sig_dpnss.sic2=0xff;
			break;
		case S_CAS:
		case S_CAS_TONE:
		case S_T1CAS:
		case S_T1CAS_TONE:
			break;
		}
		*/

		outparms.net=port;
		outparms.cnf=0;
		outparms.ts=ts;
		outparms.sending_complete=1;
		strncpy(outparms.destination_addr,data.dialxfer.digit,
			sizeof(outparms.destination_addr)-1);

		if (data.dialxfer.callingdigit) {
			sprintf(outparms.originating_addr,data.dialxfer.callingdigit);
		}
		else {
			outparms.originating_addr[0]=0;
		}
		slog(Slog::levelDebug)<<name<<": originating addr='"<<outparms.originating_addr<<"'"<<endl;

		detail_xparms.valid=0;

		rc=call_openout(&outparms);
		if (rc != 0) {
			slog(Slog::levelError) <<name<<": failed opening outgoing call: "<<rc<<endl;
			setSymbol(SYM_ERROR, "no dsp resources");
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &AculabTrunk::stepHandler;
			return true;
		}

		/*
		 * NOTE:  It's possible for an event to come through from
		 * the driver for this channel before we've set it up in
		 * in the channel->trunk mapping array!  ie: between the
		 * call_openout() call and the setChannel() call.
		 */
		handle=outparms.handle;
		channel=call_handle_2_chan(outparms.handle);
		slog(Slog::levelDebug)<<name<<": assigned channel "<<channel<<endl;
		aculabivr.setChannel(this);

		return true;

	case TRUNK_LINE_WINK:
	        return true;
	}

	return false;
}

#ifdef    CCXX_NAMESPACES
};
#endif
