// Copyright (C) 2002 David Kerry.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne
// with the Aculab telephony libraries to produce a executable image
// without requiring Aculab's libraries to be supplied in a free software
// license as long as each source file so linked contains this exclusion
// and the unaltered Aculab source files are made available.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".


#include "driver.h"

#ifdef    CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

/*
 * We were either requested to hangup this trunk/line/channel, or
 * the remote party hung up on us and we now have to do the clean
 * up.
 */
bool AculabTrunk::hangupHandler(TrunkEvent *event)
{
	timeout_t reset = 0;
	struct cause_xparms cause;
	struct in_xparms in_xparms;

	slog(Slog::levelDebug) <<name<<": hangupHandler event "<<event->id<<endl;

	switch(event->id)
	{
	case TRUNK_ENTER_STATE:
		_stopping_state = false;
		enterState("hangup");
		status[tsid] = 'h';
		exittimer=0;
		endTimer();
		Part();

		if(tgi.pid)
			kill(tgi.pid, SIGHUP);

		if (thread) {
			reset = thread->stop();
		}
		else {
			reset = 0;
		}

		Trunk::flags.dsp = DSP_MODE_INACTIVE;

		if (!(Trunk::flags.offhook || _call_setup)) {
			handler = &AculabTrunk::idleHandler;
//			return true;
		}

		/*
		 * Release driver resources
		 */
		endTimer();
		status[tsid] = '#';

		aculabivr.clrChannel(this);

		memset(&cause,0,sizeof(cause));

		cause.handle = handle;
		cause.cause  = 0;
		cause.raw    = 0;
		if (call_release(&cause)) {
			slog(Slog::levelError) <<name<<": call release FAILED"<<endl;
		}

		/*
		 * Put an idle pattern onto network to please the exchange
		 */
		idle_net_ts(port, ts);
//		return true;

	case TRUNK_CALL_RELEASE:
		/*
		 * Free up DSP resources
		 */
		stopASR();
		//resetDSPChannel();
		//freeDSP();

		/*
		 * Reset channel and begin listening for incoming calls
		 * again.
		 */
		in_xparms.net=port;
		in_xparms.ts=ts;
		in_xparms.cnf=0;
		if (call_openin(&in_xparms) == 0) {
			/* channel # may change between calls! */
			handle=in_xparms.handle;
			channel=call_handle_2_chan(in_xparms.handle);
		}
		else {
			slog(Slog::levelError) <<name<<": call openin FAILED"<<endl;
		}

		aculabivr.setChannel(this);

		Trunk::flags.offhook=false;

		if((timeout_t)group->getHangup() > reset) {
			setTimer(group->getHangup());
		}
		else {
			setTimer(reset);
		}

		exittimer=0;

		detach();

 		handler = &AculabTrunk::idleHandler;

		return true;
	case TRUNK_TIMER_EXPIRED:
		endTimer();
		stopServices();
		Trunk::flags.reset = false;
		handler = &AculabTrunk::idleHandler;
		return true;
	case TRUNK_MAKE_IDLE:
	case TRUNK_STOP_DISCONNECT:
	case TRUNK_LINE_WINK:
	case TRUNK_CPA_DIALTONE:
		return true;
	case TRUNK_RINGING_ON:
		endTimer();
		Trunk::flags.reset = false;
		handler = &AculabTrunk::idleHandler;
		return idleHandler(event);
	}
	return false;
}

#ifdef    CCXX_NAMESPACES
};
#endif
