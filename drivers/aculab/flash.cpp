// Copyright (C) 2002 David Kerry.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne
// with the Aculab telephony libraries to produce a executable image
// without requiring Aculab's libraries to be supplied in a free software
// license as long as each source file so linked contains this exclusion
// and the unalrtered Aculab source files are made available.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".


#include "driver.h"

#ifdef    CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

/*
 * $Id: flash.cpp,v 1.6 2003/08/31 18:28:20 dyfet Exp $
 * 
 * WARNING! This code is broken currently - do not use
 *          unless you want to fix it!
 */
bool AculabTrunk::flashHandler(TrunkEvent *event)
{
	struct out_xparms outparms;
	struct cause_xparms cause;
	struct transfer_xparms txferparms;
	int rc;

slog(Slog::levelDebug) <<name<<": flashHandler event "<<event->id<<endl;

	switch(event->id)
	{
	case TRUNK_TIMER_EXPIRED:
		endTimer();
		slog(Slog::levelError) <<name<<": TIMER EXPIRED??"<<endl;
		return true;

	case TRUNK_CALL_FAILURE:
		endTimer();
		slog(Slog::levelError) <<name<<": call transfer REJECTED"<<endl;
		trunkSignal(TRUNK_SIGNAL_ERROR);
		handler = &AculabTrunk::stepHandler;
		return true;

	case TRUNK_CALL_NOHOLD:
		endTimer();
		/*
		 * Hold request was rejected.
		 */
		slog(Slog::levelError) <<name<<": call hold REJECTED"<<endl;
		setSymbol(SYM_ERROR, "no dsp resources");
		rc=call_reconnect(handle);
		if (rc != 0) {
			slog(Slog::levelError) <<name<<": call reconnect FAILED: "<<rc<<endl;
			/* What to do here?? */
		}
		trunkSignal(TRUNK_SIGNAL_ERROR);
		handler = &AculabTrunk::stepHandler;
		return true;

	case TRUNK_CALL_HOLD:
		/*
		 * Now make the outgoing isdn call.
		 */

		while((*data.dialxfer.digit=='F') ||
		      (*data.dialxfer.digit == 'W')) {
                        ++data.dialxfer.digit;
		}

		setTimer(data.dialxfer.timeout);

		memset(&outparms,0,sizeof(outparms));

		// V6FIXME: does this still have any applicability?
		/*
		switch (nettype) {
		case S_1TR6:
		case S_1TR6NET:
			outparms.unique_xparms.sig_1tr6.numbering_type=NT_NATIONAL;
			outparms.unique_xparms.sig_1tr6.numbering_plan=NP_ISDN;
			outparms.unique_xparms.sig_1tr6.service_octet =0;
			outparms.unique_xparms.sig_1tr6.add_info_octet=0;
		break;
		case S_ETS300:
		case S_ETSNET:
		case S_SWETS300:
		case S_SWETSNET:
		case S_VN3:
		case S_VN3NET:
		case S_QSIG:
		case S_AUSTEL:
		case S_AUSTNET:
		case S_TNA_NZ:
		case S_TNANET:
		case S_FETEX_150:
		case S_FETEXNET:
		case S_ATT:
		case S_ATTNET:
		case S_ATT_T1:
		case S_ATTNET_T1:
		case S_NI2:
		case S_NI2NET:
		case S_IDAP:
		case S_IDAPNET:
			outparms.unique_xparms.sig_q931.service_octet  = 0;
			outparms.unique_xparms.sig_q931.add_info_octet = 0;
			outparms.unique_xparms.sig_q931.bearer.ie[0] = 0;

			outparms.unique_xparms.sig_q931.display.ie[0]=0x07;
			outparms.unique_xparms.sig_q931.display.ie[1]='B';
			outparms.unique_xparms.sig_q931.display.ie[2]='A';
			outparms.unique_xparms.sig_q931.display.ie[3]='Y';
			outparms.unique_xparms.sig_q931.display.ie[4]='O';
			outparms.unique_xparms.sig_q931.display.ie[5]='N';
			outparms.unique_xparms.sig_q931.display.ie[6]='N';
			outparms.unique_xparms.sig_q931.display.ie[7]='E';
			break;
		case S_DASS:
		case S_DASSNET:
			outparms.unique_xparms.sig_dass.sic1=0x10;
			outparms.unique_xparms.sig_dass.sic2=0xff;
			break;
		case S_DPNSS:
			outparms.unique_xparms.sig_dpnss.sic1=0x10;
			outparms.unique_xparms.sig_dpnss.sic2=0xff;
			break;
		case S_CAS:
		case S_CAS_TONE:
		case S_T1CAS:
		case S_T1CAS_TONE:
			break;
		}
		*/

		outparms.net=_port->port_id;
		outparms.cnf=0;
		outparms.ts=-1;
		outparms.sending_complete=1;
		outparms.originating_addr[0]=0;
		strncpy(outparms.destination_addr, data.dialxfer.digit,
			sizeof(outparms.destination_addr)-1);

		detail_xparms.valid=0;

		rc=call_enquiry(&outparms);
		if (rc != 0) {
			slog(Slog::levelError) <<name<<": failed making enquiry call: "<<rc<<endl;
			setSymbol(SYM_ERROR, "no dsp resources");
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &AculabTrunk::stepHandler;
			return true;
		}

slog(Slog::levelDebug)<<name<<": current ts="<<ts<<",chan="<<channel<<".  Enquire ts="<<outparms.ts<<",chan="<<call_handle_2_chan(outparms.handle)<<endl;

		hold_handle=handle;
		handle=outparms.handle;
		channel=call_handle_2_chan(handle);
		aculabivr.setChannel(this);
		return true;

	case TRUNK_CALL_CONNECT:
		return true; /* Ignore for now */
	case TRUNK_CPA_RINGBACK:
		/*
		 * Connect the enquiry call and the
		 * held call together to transfer the call.
		 */
		endTimer();

		txferparms.handlea=hold_handle;
		txferparms.handlec=handle;
		rc=call_transfer(&txferparms);
		if (rc != 0) {
			slog(Slog::levelError) <<name<<": call transfer FAILED: "<<rc<<endl;
		}
		trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &AculabTrunk::stepHandler;
		return true;
	case TRUNK_CALL_RELEASE:
		/*
		 * Failed to make the outgoing enquiry call,
		 * release the handle.
		 */
		endTimer();

		cause.handle = handle;
		cause.cause  = 0;
		cause.raw    = 0;
		rc=call_release(&cause);
		if (rc != 0) {
			slog(Slog::levelError) <<name<<": call enquiry release FAILED: "<<rc<<endl;
		}
//		aculabivr.clrChannel(this);
		handle=hold_handle;
		hold_handle=0;
		aculabivr.setChannel(this);

		trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &AculabTrunk::stepHandler;
		return true;

	case TRUNK_ENTER_STATE:
		_stopping_state = false;
		enterState("flash");
		setDTMFDetect(false);
		status[tsid] = 'f';

		rc=call_hold(handle);
		if (rc != 0) {
			slog(Slog::levelError) <<name<<": call hold FAILED: "<<rc<<endl;
			
			setSymbol(SYM_ERROR, "no dsp resources");
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &AculabTrunk::stepHandler;
		}
		return true;
	}
	return false;
}		

#ifdef    CCXX_NAMESPACES
};
#endif
