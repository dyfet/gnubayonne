// Copyright (C) 2002 David Kerry.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne
// with the Aculab telephony libraries to produce a executable image
// without requiring Aculab's libraries to be supplied in a free software
// license as long as each source file so linked contains this exclusion
// and the unalrtered Aculab source files are made available.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#include "driver.h"

#ifdef    CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

bool AculabTrunk::collectHandler(TrunkEvent *event)
{
	unsigned short mask;
        unsigned copy = 0;
        Symbol *sym = (Symbol *)data.collect.var;
	char evt[65];

	switch(event->id)
	{
	case TRUNK_ENTER_STATE:
		_stopping_state = false;
		enterState("collect");
		if(digits >= data.collect.count)
		{
			trunkSignal(TRUNK_SIGNAL_STEP);
			handler = &AculabTrunk::stepHandler;
			return true;
		}
		Trunk::flags.dsp = DSP_MODE_VOICE;
		setDTMFDetect(true);
		if(digits)
			setTimer(data.collect.timeout);
		else
			setTimer(data.collect.first);
		return true;
	case TRUNK_TIMER_EXPIRED:
		if(!trunkEvent("digits:expired"))
			if(!trunkSignal(TRUNK_SIGNAL_TIMEOUT))
				trunkSignal(TRUNK_SIGNAL_STEP);
	
		handler = &AculabTrunk::stepHandler;
		setDTMFDetect(false);
		return true;
	case TRUNK_DTMF_KEYUP:
		mask = (1 << event->parm.dtmf.digit);
		if(mask & data.collect.ignore) {
			return true;
		}
		if(!(mask & data.collect.term))
		{
			dtmf.bin.data[digits++] = digit[event->parm.dtmf.digit];
			dtmf.bin.data[digits] = 0;
			snprintf(evt, sizeof(evt), "digits:%s", dtmf.bin.data);
			if(trunkEvent(evt))
			{
				handler = &AculabTrunk::stepHandler;
				return true;
			}
		}				
		if(digits >= data.collect.count || (mask & data.collect.term))
		{
                        if(sym)
                        {
                                copy = sym->size;
                                if(copy > digits)
                                        copy = digits;
                                if(copy)
                                        strncpy(sym->data, dtmf.bin.data, copy);
                                sym->data[copy] = 0;
				digits = 0;
				dtmf.bin.data[0] = 0;
                        }

			endTimer();
			trunkSignal(TRUNK_SIGNAL_STEP);
			handler = &AculabTrunk::stepHandler;
		}
		else {
			setTimer(data.collect.timeout);
		}
		return true;
	}
	return false;
}

#ifdef    CCXX_NAMESPACES
};
#endif
