// Copyright (C) 2002 David Kerry.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne
// with the Aculab telephony libraries to produce a executable image
// without requiring Aculab's libraries to be supplied in a free software
// license as long as each source file so linked contains this exclusion
// and the unalrtered Aculab source files are made available.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#include "driver.h"

#ifdef    CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

bool AculabTrunk::answerHandler(TrunkEvent *event)
{
//	timeout_t wakeup;
	int rc;

slog(Slog::levelDebug) <<name<<": answerHandler event "<<event->id<<endl;

	switch(event->id) {
	case TRUNK_LINE_WINK:
		if(!Trunk::flags.offhook)
			return true;
	case TRUNK_STOP_DISCONNECT:
	case TRUNK_TIMER_EXPIRED:
		endTimer();
		Trunk::flags.dsp = DSP_MODE_INACTIVE;
		if(!trunkEvent("answer:failed"))
			trunkSignal(TRUNK_SIGNAL_HANGUP);
		handler = &AculabTrunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		_stopping_state = false;
		if(Trunk::flags.offhook) {
			trunkSignal(TRUNK_SIGNAL_STEP);
			handler = &AculabTrunk::stepHandler;
			return true;
		}
		status[tsid] = 'a';
		enterState("answer");
	case TRUNK_RINGING_OFF:
		if(rings > 1)
			Trunk::Trunk::flags.dsp = DSP_MODE_INACTIVE;

		Trunk::flags.dsp = DSP_MODE_INACTIVE;

		debug->debugState(this, "accepting incoming call");

		setTimer(data.answer.timeout);
		rc = call_accept(handle);
		if(rc) {
			slog(Slog::levelError) <<name<<": accept call FAILED, rc = " << rc <<endl;
		}
		return true;

	case TRUNK_CALL_CONNECT:
	case TRUNK_OFF_HOOK:
		/*
		 * ISDN call is fully connected now
		 */
		endTimer();
		_call_setup=false;
		Trunk::flags.offhook=true;

		/* Connect port/timeslot with DSP channel/resources */
		if (!allocateDSP(kSMChannelTypeFullDuplex)) {
			slog(Slog::levelError) <<name<<": allocate DSP resources failed!"<<endl;
			trunkSignal(TRUNK_SIGNAL_HANGUP);
			handler = &AculabTrunk::stepHandler;
			return true;
		}

		aculabivr.setChannel(this);

		setDTMFDetect(true);

		trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &AculabTrunk::stepHandler;
		status[tsid] = 'i';
		return true;
	}
	return false;
}

#ifdef    CCXX_NAMESPACES
};
#endif
