// Copyright (C) 2002 David Kerry.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne
// with the Aculab telephony libraries to produce a executable image
// without requiring Aculab's libraries to be supplied in a free software
// license as long as each source file so linked contains this exclusion
// and the unalrtered Aculab source files are made available.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".


#include "driver.h"

#ifdef    CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

#ifdef HAVE_ACULAB_ASR

AculabASR::AculabASR()
{
	memset(&sessionParms, 0, sizeof(sessionParms));
	memset(&controllerParms, 0, sizeof(controllerParms));
	memset(&lexParms, 0, sizeof(lexParms));
	memset(&modelParms, 0, sizeof(modelParms));
	memset(&triParms, 0, sizeof(triParms));
	memset(&ruleParms, 0, sizeof(ruleParms));
	useController = false;
}

AculabASR::~AculabASR()
{
}

bool AculabASR::start()
{
	const char *opt = aculabivr.getLast("asrhost");
	const char *use = aculabivr.getLast("asrusecontroller");
	unsigned int port = atoi(aculabivr.getLast("asrport"));
	if(!opt)
		throw(AculabASRException("unable to connect to engine, specify asrhost in bayonne.conf"));
	else
		slog(Slog::levelInfo) << "aculab: connecting to asr engine" << endl;

	try {
		if(!stricmp(use, "1") || !stricmp(use, "yes"))
		{
			useController = true;
			controllerConnect(opt, port);
		}
		else
		{
			strncpy(sessionParms.rec_addr, opt, sizeof(sessionParms.rec_addr));
			sessionParms.rec_port = port;
		}
		sessionOpen();

		opt = aculabivr.getLast("asrmodelset");
		modelLoad(opt);
		opt = aculabivr.getLast("asrtriphoneset");
		triphoneLoad(opt);
		opt = aculabivr.getLast("asrlexicon");
		lexiconLoad(opt);
		opt = aculabivr.getLast("asrrules");
		rulesLoad(opt);
	}
	catch(AculabASRException e)
	{
		throw;
	}
	catch(...)
	{
		throw(AculabASRException("unknown internal exception"));
	}
	return true;
}

void AculabASR::stop()
{
	slog(Slog::levelInfo) << "aculab: asr engine shutting down" << endl;
	try {
		rulesUnload();
		lexiconUnload();
		triphoneUnload();
		modelUnload();
		sessionClose();
		if(useController)
			controllerDisconnect();
	}
	catch(AculabASRException e)
	{
		throw;
	}
	catch(...)
	{
		throw(AculabASRException("unknown internal exception"));
	}
}

AculabASRSession *AculabASR::getSession()
{
	asrconfig_t config;
	memset(&config, 0, sizeof(config));
	config.controller = controllerParms;
	config.session = sessionParms;
	config.lexicon = lexParms;
	config.model = modelParms;
	config.triphone = triParms;
	config.rules = ruleParms;
	return new AculabASRSession(config);
}

void AculabASR::controllerConnect(const char *ipAddr, const unsigned short port)
{
	int rc = 0;
	strcpy(controllerParms.cont_addr, ipAddr);
	controllerParms.cont_port = port;
	rc = smcwr_connect_to_controller(&controllerParms);
	if(rc != 0)
		throw(AculabASRException("connection to asr controller failed", rc));
}

void AculabASR::controllerDisconnect()
{
	int rc = 0;
	rc = smcwr_disconnect_from_controller();
	if(rc != 0)
		throw(AculabASRException("disconnection from asr controller failed", rc));
}

void AculabASR::modelLoad(const char *model)
{
	int rc = 0;
	modelParms.name = model;
	rc = smcwr_model_set_load(sessionId, &modelParms);
	if(rc != 0)
		throw(AculabASRException("smcwr_model_set_load failed", rc));
}

void AculabASR::modelUnload()
{
	int rc = 0;
	rc = smcwr_model_set_unload(sessionId, &modelParms);
	if(rc != 0)
		throw(AculabASRException("smcwr_model_set_unload", rc));
}

void AculabASR::triphoneLoad(const char *triphone)
{
	int rc = 0;
	triParms.name = triphone;
	rc = smcwr_tri_load(sessionId, &triParms);
	if(rc != 0)
		throw(AculabASRException("smcwr_tri_load", rc));
}

void AculabASR::triphoneUnload()
{
	int rc = 0;
	rc = smcwr_tri_unload(sessionId, &triParms);
	if(rc != 0)
		throw(AculabASRException("smcwr_tri_unload", rc));
}

void AculabASR::lexiconLoad(const char *lexicon)
{
	int rc = 0;
	lexParms.name = lexicon;
	rc = smcwr_lex_load(sessionId, &lexParms);
	if(rc != 0)
		throw(AculabASRException("smcwr_lex_load", rc));
}

void AculabASR::lexiconUnload()
{
	int rc = 0;
	rc = smcwr_lex_unload(sessionId, &lexParms);
	if(rc != 0)
		throw(AculabASRException("smcwr_lex_unload", rc));
}

void AculabASR::rulesLoad(const char *rul)
{
	int rc = 0;
	ruleParms.name = rul;
	rc = smcwr_rul_load(sessionId, &ruleParms);
	if(rc != 0)
		throw(AculabASRException("smcwr_rul_load", rc));
}

void AculabASR::rulesUnload()
{
	int rc = 0;
	rc = smcwr_rul_unload(sessionId, &ruleParms);
	if(rc != 0)
		throw(AculabASRException("smcwr_rul_unload", rc));
}

void AculabASR::sessionOpen()
{
	int rc = 0;
	rc = smcwr_open_session(&sessionParms);
	if(rc < 0)
		throw(AculabASRException("smcwr_open_session", rc));
	sessionId = rc;
}

void AculabASR::sessionClose()
{
	int rc = 0;
	if(sessionId > -1)
	{
		rc = smcwr_close_session(sessionId);
		if(rc != 0)
			throw(AculabASRException("smcwr_close_session", rc));
	}
}

AculabASRSession::AculabASRSession(const asrconfig_t &config)
{
	asrconfig = config;
	memset(&sessionParms, 0, sizeof(sessionParms));
	memset(&listenForParms, 0, sizeof(listenForParms));
	memset(&networkInfo, 0, sizeof(networkInfo));
	memset(&netParms, 0, sizeof(netParms));
	memset(&gruntParms, 0, sizeof(gruntParms));
	networkInfo.top_n = 1;
	memset(&configureParms, 0, sizeof(configureParms));
	memset(&configureInfo, 0, sizeof(configureInfo));
	configureInfo.model_set_id = asrconfig.model.model_set_id;
	configureInfo.net_info = &networkInfo;
	configureParms.vfr = NULL;
	configureParms.grunt = &gruntParms;
	configureParms.cwr_conf_info = &configureInfo;
	//recogParms = NULL;
	sessionId = 0;
	_listening = false;
	trunk = NULL;
}

AculabASRSession::~AculabASRSession()
{
	if(eventHandler)
		delete eventHandler;
}

void AculabASRSession::start(AculabTrunk *trk, const char *asgf, unsigned initial, unsigned maxtime, unsigned pause, unsigned buffer)
{
	trunk = trk;
	gruntParms.on_frames = initial;
	gruntParms.rec_frames = maxtime;
	gruntParms.pause_frames = pause;
	gruntParms.hold_frames = buffer;
	slog(Slog::levelDebug) << "aculab: asr enabled: initial=" << initial << 
		", duration=" << maxtime << ", interword=" << pause << ", buffer=" <<
		buffer << endl;
	configureParms.grunt = &gruntParms;
	sessionOpen();
	networkBuild(asgf, kSMCWR_ProTypeLex);
	sessionConfigure();
	eventSet();
	listen(trk->dspChannel);
	eventHandler = new AculabASREvents(this, trk, eventIds);
	eventHandler->start();
}

AculabASREvents::AculabASREvents(AculabASRSession *sess, AculabTrunk *trk, tSMCWREventId *eventIds)
{
	session = sess;
	trunk = trk;
	events = eventIds;
	active = false;
}

AculabASREvents::~AculabASREvents()
{
	if(!active)
		return;

	active = false;
	terminate();
}

void AculabASREvents::run(void)
{
	pollfd fd[3];
	int y;

	active = true;

	while(active)
	{
		AculabTrunkEvent *aevent;
		for(y = 0; y < 3; y++)
		{
			fd[y].fd = events[y].read;
			fd[y].events = events[y].mode;
		}
		poll(fd, 3, 1000);

		if(fd[2].revents)
		{
			smcwr_reset_event(events[2]);
			aevent = new AculabTrunkEvent();
			aevent->trunk = trunk;
			aevent->event.id = TRUNK_ASR_VOICE;
			aculabivr.getQueue()->push(aevent);
		}
		if(fd[1].revents)
		{
			smcwr_reset_event(events[1]);
			slog(Slog::levelDebug) << "aculab: asr error event" << endl;
			aevent = new AculabTrunkEvent();
			aevent->trunk = trunk;
			aevent->event.id = TRUNK_ASR_TEXT;
			aculabivr.getQueue()->push(aevent);
		}
		if(fd[0].revents)
		{
			smcwr_reset_event(events[0]);
			slog(Slog::levelDebug) << "aculab: asr recognition event" << endl;
			// You must stop listening before retrieving the recognised text.
			session->unlisten();
			session->getRecognised();
			aevent = new AculabTrunkEvent();
			aevent->trunk = trunk;
			aevent->event.id = TRUNK_ASR_TEXT;
			aculabivr.getQueue()->push(aevent);
		}
	}
}

void AculabASRSession::stop()
{
	delete eventHandler;
	eventHandler = NULL;

	if(_listening)
		unlisten();

	eventUnset();
	//getRecognised();
	sessionUnconfigure();
	networkUnload();
	sessionClose();
}

void AculabASRSession::sessionOpen()
{
	int rc = 0;
	rc = smcwr_open_session(&asrconfig.session);
	if(rc < 0)
		throw(AculabASRException("smcwr_open_session", rc));
	sessionId = rc;
}

void AculabASRSession::sessionClose()
{
	int rc = 0;
	if(sessionId > -1)
	{
		rc = smcwr_close_session(sessionId);
		if(rc != 0)
			throw(AculabASRException("smcwr_close_session", rc));
	}
}

void AculabASRSession::sessionConfigure()
{
	int rc = 0;
	configureInfo.voc_net_id = netParms.net_id;
	rc = smcwr_configure(sessionId, &configureParms);
	if(rc != 0)
		throw(AculabASRException("smcwr_configure", rc));
}

void AculabASRSession::sessionUnconfigure()
{
	int rc = 0;
	rc = smcwr_unconfigure(sessionId, &configureParms);
	if(rc != 0)
		throw(AculabASRException("smcwr_unconfigure", rc));
}

void AculabASRSession::networkBuild(const char *asrstring, int pt)
{
	int rc = 0;
	netParms.asgf = asrstring;
	netParms.pro_id = &asrconfig.lexicon.lex_id;
	netParms.pro_type = &pt;
	netParms.num_pro_ids = 1;
	netParms.tri_id = asrconfig.triphone.tri_id;
	netParms.pro_method = kSMCWR_ProModeOnePro;
	netParms.build_tri = 1;

	rc = smcwr_net_build(sessionId, &netParms);
	if(rc != 0)
		throw(AculabASRException("smcwr_net_build", rc));
}

void AculabASRSession::networkUnload()
{
	int rc = 0;
	rc = smcwr_net_unload(sessionId, &netParms);
	if(rc != 0)
		throw(AculabASRException("smcwr_net_unload", rc));
}

void AculabASRSession::listen(tSMChannelId ch, int rm)
{
	int rc = 0;
	memset(&listenForParms, 0, sizeof(listenForParms));
	listenForParms.channel = ch;
	listenForParms.cwr_mode = kSMCWRModeOneShot;
	//listenForParms.file = NULL;
	rc = smcwr_listen_for(sessionId, &listenForParms);
	if(rc != 0)
		throw(AculabASRException("smcwr_listen_for", rc));
	else
		_listening = true;
}

void AculabASRSession::unlisten(int rm)
{
	int rc = 0;
	if(!_listening)
		return;
	listenForParms.cwr_mode = kSMCWRModeDisable;
	rc = smcwr_listen_for(sessionId, &listenForParms);
	if(rc != 0)
		throw(AculabASRException("smcwr_listen_for [unlisten]", rc));
	else
		_listening = false;

	// Fix for SCR18608, reregister events after calling smcwr_listen_for to disable
	// ASR because it removes all events.
	trunk->registerEvents();
}

void AculabASRSession::eventSet()
{
	int rc = 0;
	smcwr_ev_create(&eventIds[0]);
	smcwr_ev_create(&eventIds[1]);
	smcwr_ev_create(&eventIds[2]);

	// an event to indicate we have a result
	rc = smcwr_set_event(sessionId, kSMEventTypeCWRResult, kSMEvent, eventIds[0]);
	if(rc != 0)
		throw(AculabASRException("smcwr_set_event [results]", rc));

	// an event to indicate an error
	rc = smcwr_set_event(sessionId, kSMEventTypeCWRError, kSMEvent, eventIds[1]);
	if(rc != 0)
	{
		smcwr_ev_free(eventIds[0]);
		throw(AculabASRException("smcwr_set_event [errors]", rc));
	}

	// an event to indicate that voice activity has been detected
	rc = smcwr_set_event(sessionId, kSMEventTypeCWRVAD, kSMEvent, eventIds[2]);
	if(rc != 0)
	{
		smcwr_ev_free(eventIds[0]);
		smcwr_ev_free(eventIds[1]);
		throw(AculabASRException("smcwr_set_event [vad]", rc));
	}
}

void AculabASRSession::eventUnset()
{
	int rc = 0;
	rc = smcwr_set_event(sessionId, kSMEventTypeCWRResult, kSMNoEvent, eventIds[0]);
	if(rc != 0)
	{
		smcwr_ev_free(eventIds[1]);
		smcwr_ev_free(eventIds[2]);
		throw(AculabASRException("smcwr_ev_free [results]", rc));
	}

	rc = smcwr_set_event(sessionId, kSMEventTypeCWRError, kSMNoEvent, eventIds[1]);
	if(rc != 0)
	{
		smcwr_ev_free(eventIds[2]);
		throw(AculabASRException("smcwr_ev_free [errors]", rc));
	}

	rc = smcwr_set_event(sessionId, kSMEventTypeCWRVAD, kSMNoEvent, eventIds[2]);
	if(rc != 0)
		throw(AculabASRException("smcwr_ev_free [vad]", rc));
}

void AculabASRSession::getRecognised()
{
	int rc = 0;
	rc = smcwr_get_recognised(sessionId, &recogParms);
	if(rc != 0)
		throw(AculabASRException("smcwr_get_recognised", rc));
	rc = smcwr_print_recognised(recogParms);
	if(rc != 0)
		throw(AculabASRException("smcwr_print_recognised", rc));
}

bool AculabASRSession::getResults(char *result, float *confidence)
{
	std::string s;
	s = "";
	int i;

	memset(result, 0, sizeof(result));

	if(recogParms->confidence == 0 || 
		!recogParms->nth_result)
	{
		strcpy(result, s.c_str());
		*confidence = 0.00;
		return false;
	}

	*confidence = recogParms->confidence;
	if(recogParms->nth_result[0].num_tags > 0)
	{
		s += recogParms->nth_result[0].tag[0];
		for(i = 1; i < recogParms->nth_result[0].num_tags; i++)
		{
			s += " ";
			s += recogParms->nth_result[0].tag[i];
		}
	}
	else if(recogParms->nth_result[0].num_tokens > 0)
	{
		s += recogParms->nth_result[0].token[0];
		for(i = 1; i < recogParms->nth_result[0].num_tokens; i++)
		{
			s += " ";
			s += recogParms->nth_result[0].token[i];
		}
	}
	strcpy(result, s.c_str());
	return true;
}

#endif // HAVE_ACULAB_ASR

#ifdef    CCXX_NAMESPACES
};
#endif
