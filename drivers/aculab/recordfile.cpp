// Copyright (C) 2002 David Kerry.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne
// with the Aculab telephony libraries to produce a executable image
// without requiring Aculab's libraries to be supplied in a free software
// license as long as each source file so linked contains this exclusion
// and the unaltered Aculab source files are made available.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".


#include "driver.h"
#include <sys/ioctl.h>
#include <sys/stat.h>

#ifdef    CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

void AculabTrunk::StopRecordFile(void)
{
	int rc;
	SM_RECORD_ABORT_PARMS abort_parms;
	memset(&abort_parms, 0, sizeof(abort_parms));

	abort_parms.channel=dspChannel;
	abort_parms.discard=0;
	rc = sm_record_abort(&abort_parms);
	if(rc != 0)
		slog(Slog::levelError) << name << ": unable to abort record, rc = " << rc << endl;
}

timeout_t AculabTrunk::endRecord(void)
{
	char buffer[12];
	struct stat ino;
	int trim;

	sprintf(buffer, "%ld", getPosition());
	setSymbol(SYM_OFFSET, buffer);

	stat(data.record.name, &ino);
	if(data.record.minsize)
	{
		trim = toBytes(getEncoding(), data.record.minsize);
		if(ino.st_size < trim)
		{
			setSymbol(SYM_RECORDED, "0");
			remove(data.record.name);
			close();
			return 0;
		}
	}


	trim = toBytes(getEncoding(), data.record.trim);
	if(ino.st_size <= trim || data.record.frames)
	{
		remove(data.record.name);
		setSymbol(SYM_RECORDED, "0");
	}
	else
	{
		sprintf(buffer, "%ld",
			toSamples(getEncoding(), ino.st_size - trim));
		setSymbol(SYM_RECORDED, buffer);
               	truncate(data.record.name, ino.st_size - trim);
               	if(!data.record.append)
                       	chown(data.record.name, keyserver.getUid(), keyserver.getGid());
		if(data.record.save)
			rename(data.record.name, data.record.save);
	}
	close();

	return 0;
}

bool AculabTrunk::StartRecordFile(void)
{
	SM_RECORD_PARMS record_parms;
	kSMDataFormat format = kSMDataFormat8KHzULawPCM;
	int rc;

	SM_RECORD_ABORT_PARMS abort_parms;
	memset(&abort_parms, 0, sizeof(abort_parms));
	abort_parms.channel = dspChannel;
	abort_parms.discard = 1;
	rc = sm_record_abort(&abort_parms);
	if(rc != 0)
		slog(Slog::levelError) << "************************ Abort not successful" << endl;

	Trunk::flags.audio=true;

	switch(getEncoding()) {
	case mulawAudio:
		format = kSMDataFormat8KHzULawPCM;
		break;
	case alawAudio:
		format = kSMDataFormat8KHzALawPCM;
		break;
	default:
		/* Eh? */
		slog(Slog::levelError) <<name<< ": unsupported audio format format" << endl;
		return false;
	}

	frames = 0;

	memset(&record_parms, 0, sizeof(record_parms));
	record_parms.channel=dspChannel;
	record_parms.alt_data_source=kSMNullChannelId;
	record_parms.type=format;
	// FIXME
	//record_parms.elimination=kSMDRecordNoElimination;
	record_parms.volume=0;
	record_parms.agc=0;
	record_parms.max_octets=0;
	record_parms.max_elapsed_time=0;
	record_parms.max_silence=0;
	record_parms.tone_elimination_mode = kSMToneDetectionNone;

	slog(Slog::levelDebug)<<name<<": starting record of file '"<<filename
			<<"', bufsize="<<recordaudiobufsize<<",format="<<format<<endl;

	rc=sm_record_start(&record_parms);
	if (rc != 0) {
		slog(Slog::levelError) <<name<<": cannot start record of data: "<<rc<<endl;
		return false;
	}

	/*
	 * The aculab driver will take care of notifying the
	 * audio thread that there's work to do...
	 */
	 return true;
}

/*
 * Retrieve next block of audio from DSP
 */
bool AculabTrunk::RecordNext(void)
{
	SM_TS_DATA_PARMS ts_data;
	int rc;

	ts_data.channel=dspChannel;
	ts_data.data=recordaudiobuffer;
	ts_data.length=recordaudiobufsize;
	rc=sm_get_recorded_data(&ts_data);
	if (rc != 0) {
		slog(Slog::levelError) <<name<<": cannot record audio data: "<<rc<<endl;
		return false;
	}

	if(data.record.frames && (int)frames++ >= data.record.frames)
	{
		frames = 0;
		setPosition(0);
	}

	rc=putBuffer((unsigned char *)recordaudiobuffer,ts_data.length);
	if (rc == -1) {
		slog(Slog::levelError) << name<<": error writing to file: "<<rc<<endl;
		return false;
	}
	return true;
}

#ifdef    CCXX_NAMESPACES
};
#endif
