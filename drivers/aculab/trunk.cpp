// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne 
// with the Aculab telephony libraries to produce a executable image
// without requiring Aculab's sources to be supplied in a free software
// license long as each source file so linked contains this exclusion
// and the unalrtered Aculab source files are also provided.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#include "driver.h"

#ifdef    CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

#define	EVENT_BUFFER_SIZE	16

AculabTrunk::AculabTrunk(AculabCardData *c, AculabPortData *p, int t) :
Trunk(p->system_ix * MAXTIMESLOTS + t, &aculabivr), TimerPort(), AudioService(), URLAudio()
{
	struct in_xparms in_xparms;
	int rc;

	_card = c;
	_port = p;

	trunk = (Trunk *)this;
	handler = NULL;
	join = NULL;
	lastring = 0;
	exittimer = 0;
	port = p->system_ix;
	ts = t;
	dspChannel=kSMNullChannelId;
	mvip_stream=-1;
	mvip_slot=-1;
	stream=-1;
	Trunk::flags.offhook=false;
	_call_setup=false;
	_bargein = false;
#ifdef HAVE_ACULAB_ASR
	asrSession = NULL;
#endif

	memset(joinees, 0, sizeof(joinees));

	sprintf(name,"aculab/%d", id);

	playaudiobufsize=kSMMaxReplayDataBufferSize;
	recordaudiobufsize=kSMMaxRecordDataBufferSize;

	slog(Slog::levelDebug)<<name<<": initializing"<<endl;

	in_xparms.net = _port->port_id;
	in_xparms.ts = ts;
	in_xparms.cnf = 0;
	rc=call_openin(&in_xparms);
	if (rc) {
		errlog("failed", "Device=%s", name);
		slog(Slog::levelCritical) << name<<": open failed: " <<rc << endl;
		throw((Trunk *)this);
	}

	/* get network/switch type */
	nettype=call_type(_port->port_id);

	handle = in_xparms.handle;
	channel = call_handle_2_chan(in_xparms.handle);

	aculabivr.setChannel(this);

	slog(Slog::levelDebug)<<name<<": initialized with channel="<<channel<<endl;

	handler=&AculabTrunk::idleHandler;
}

AculabTrunk::~AculabTrunk()
{
  /* XXX TODO XXX - close files, cleanup! */
}

void AculabTrunk::setHandle(int h)
{
	handle=h;
}

void AculabTrunk::setChannel(int c)
{
	channel=c;
}

void AculabTrunk::setTimer(timeout_t t)
{
	TimerPort::setTimer(t);
	aculabivr.timer->notify();
}

void AculabTrunk::getName(char *buffer)
{
	strcpy(buffer,name);
}

bool AculabTrunk::exit(void)
{
	slog(Slog::levelDebug) << name << ": trunk exit called"<<endl;

	if(!exiting)
		if(ScriptInterp::exit()) {
			return true;
		}

	handler = &AculabTrunk::hangupHandler;

	return false;
}

bool AculabTrunk::postEvent(TrunkEvent *event)
{
	bool rtn = true;
	trunkhandler_t prior;
	char evt[65];

	enterMutex();
	slog(Slog::levelDebug) <<name<<": event "<< event->id << " received"<<endl;

	switch(event->id)
	{
	case TRUNK_TIMER_SYNC:
		if(!synctimer)
			rtn = false;
		synctimer = 0;
		break;
	case TRUNK_TIMER_EXIT:
		if(!exittimer)
			rtn = false;
		exittimer = 0;
		break;
	case TRUNK_TIMER_EXPIRED:
		if(getTimer() < 0)
			rtn = false;

		break;
	case TRUNK_DTMF_KEYUP:
		if(Trunk::flags.offhook)
			time(&idletime);
		if(!Trunk::flags.dtmf)
			rtn = false;
		break;
	}
	if(!rtn)
	{
		leaveMutex();
		return false;
	}

	if(!handler)
	{
		slog(Slog::levelWarning) <<name << ": no handler active; event=" << event->id << endl;
		leaveMutex();
		return false;
	}

retry:
	debug->debugEvent(this, event);
	prior = handler;
	rtn = (this->*handler)(event);
	if(rtn)
	{
		if(handler != prior)
		{
			enterMutex();
			if(prior == &AculabTrunk::idleHandler)
				setIdle(false);
			else if(_bargein)
			{
				_bargein = false;
				handler = &AculabTrunk::bargeinHandler;
			}
			event->id = TRUNK_ENTER_STATE;
			leaveMutex();
			goto retry;
		}
		leaveMutex();
		return true;
	}

	// default handler

	rtn = true;
	switch(event->id)
	{
	case TRUNK_RINGING_ON:
	case TRUNK_CALL_DETECT:
		++rings;
		snprintf(evt, sizeof(evt), "ring:%d", rings);
		if(trunkEvent(evt))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		break;
	case TRUNK_ENTER_STATE:
		if(Trunk::flags.offhook)
			Trunk::setDTMFDetect();
		else
			setDTMFDetect(false);
		endTimer();
		break;
	case TRUNK_CPA_DIALTONE:
		if(trunkEvent("tone:dialtone"))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		goto drop;
	case TRUNK_LINE_WINK:
		if(trunkEvent("line:wink"))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		goto drop;
	case TRUNK_CALL_RELEASE:
	case TRUNK_STOP_DISCONNECT:
drop:
		if(Trunk::flags.onexit)
			break;

                if (!isRunning()) 
		{
                        slog(Slog::levelDebug) << name << ": disconnect AFTER SCRIPT EXIT" << endl;
	                handler = &AculabTrunk::hangupHandler;
                        break;
                }

		if(trunkSignal(TRUNK_SIGNAL_HANGUP))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		break;
	case TRUNK_TIMER_EXPIRED:
		if(!trunkSignal(TRUNK_SIGNAL_TIMEOUT))
			trunkSignal(TRUNK_SIGNAL_STEP);
		event->id = TRUNK_STOP_STATE;
		goto retry;
        case TRUNK_TIMER_SYNC:
                if(trunkSignal(TRUNK_SIGNAL_TIME))
                {
                        event->id = TRUNK_STOP_STATE;
                        goto retry;
                }
                break;
        case TRUNK_TIMER_EXIT:
                if(trunkSignal(TRUNK_SIGNAL_TIME))
                        event->id = TRUNK_STOP_STATE;
                else
                        event->id = TRUNK_STOP_DISCONNECT;
                goto retry;
	case TRUNK_SEND_MESSAGE:
		if(recvEvent(event))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		break;
	case TRUNK_SYNC_PARENT:
		if(trunkEvent(event->parm.sync.msg))
		{
			setSymbol(SYM_STARTID, event->parm.sync.id);
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		rtn = false;
		break;
	case TRUNK_CHILD_EXIT:
		if(!isActive())
			break;
		if(trunkSignal(TRUNK_SIGNAL_CHILD))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		break;
	case TRUNK_DTMF_KEYUP:
		if(digits < MAX_DIGITS)
			dtmf.bin.data[digits++] = digit[event->parm.dtmf.digit];
		dtmf.bin.data[digits] = 0;
		snprintf(evt, sizeof(evt), "digits:%s", dtmf.bin.data);
		if(trunkEvent(evt))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		if(trunkSignal((trunksignal_t)(event->parm.dtmf.digit + TRUNK_SIGNAL_0)))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		break;
	case TRUNK_ASR_VOICE:
		if(handler != &AculabTrunk::listenHandler)
		{
			_bargein = true;
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		rtn = false;
		break;
	case TRUNK_EXIT_SHELL:
		if(event->parm.exitpid.seq != tgi.seq)
			break;
		tgi.pid = 0;
		if(tgi.fd > -1)
		{
			::close(tgi.fd);
			tgi.fd = -1;
			snprintf(evt, sizeof(evt), "exit:%d", 
				event->parm.exitpid.status);
			if(!trunkEvent(evt))
				trunkSignal(TRUNK_SIGNAL_STEP);
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		break;
	case TRUNK_STOP_STATE:
		endTimer();
		handler = &AculabTrunk::stepHandler;
		break;
	case TRUNK_EXIT_STATE:
		break;
	case TRUNK_MAKE_BUSY:
//		handler = &AculabTrunk::busyHandler;
		break;
	case TRUNK_MAKE_IDLE:
		handler = &AculabTrunk::idleHandler;
		break;
	default:
		rtn = false;
	}
	if(handler != prior)
	{
		event->id = TRUNK_ENTER_STATE;
		goto retry;
	}
	leaveMutex();
	return rtn;
}

unsigned long AculabTrunk::getIdleTime(void)
{
	time_t now;

	time(&now);
	if(handler == &AculabTrunk::idleHandler)
		return now - idletime;

	return 0;
}

/**
 * This is used to determine if the maximum script run-time
 * has been exceeded.
 *
 * @return number of seconds remaining, zero if exceeded, -1 if disabled.
 */
long AculabTrunk::getMaxTime(void)
{
	time_t now;
	if (exittimer == 0) {
		return -1;
	}

	time(&now);
	if (now >= exittimer) {
		exittimer = 0;
		return 0;
	}
	else {
		return exittimer - now;
	}
}

bool AculabTrunk::scrJoin(void)
{
	Trunk *trunk = aculabivr.getTrunkId(getValue(getKeyword("id")));
	const char *mem = getMember();

	if(!trunk) 
	{
		Trunk::error("join-no-port");
		return true;
	}

	if(!mem)
		mem = "duplex";

	data.join.hangup = false;
	data.join.waiting = NULL;

	if(!stricmp(mem, "hangup"))
		data.join.hangup = true;

	mem = getKeyword("count");
	if(mem)
		data.join.count = atoi(mem);
	else
		data.join.count = 0;

	mem = getKeyword("waitTime");
	if(mem)
               data.join.count = getSecTimeout(mem) /
                        keythreads.getResetDelay() + 1;

	data.join.waiting = data.join.trunk = trunk;
	data.join.seq = trunk->getSequence();
	data.join.wakeup = getTimeout("maxTime");
	trunkStep(TRUNK_STEP_JOIN);
	return false;
}

bool AculabTrunk::scrWait(void)
{
	const char *mem = getMember();

	if(mem)
	{
		data.join.waiting = NULL;
		data.join.hangup = false;
		if(!stricmp(mem, "hangup"))
			data.join.hangup = true;
		else if(!stricmp(mem, "parent"))
			data.join.waiting = aculabivr.getTrunkId(getSymbol(SYM_PARENT));
	}
	else
	{
		data.join.waiting = NULL;
		data.join.hangup = false;
	}
	data.join.count = 0;
	data.join.trunk = NULL;
	if(getKeyword("maxTime"))
		data.join.wakeup = getTimeout("maxTime");
	else
		data.join.wakeup = getTimeout("waitTime");
	trunkStep(TRUNK_STEP_JOIN);
	return false;
}

void AculabTrunk::stopASR(void)
{
	enterMutex();
	_bargein = false;
#ifdef HAVE_ACULAB_ASR
	if(asrSession)
	{
		try
		{
			asrSession->stop();
		}
		catch(AculabASRException e)
		{
			e.reportError();
		}
		delete asrSession;
		asrSession = NULL;
	}
#endif
	leaveMutex();
}

#ifdef    CCXX_NAMESPACES
};
#endif
