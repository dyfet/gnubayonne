// Copyright (C) 2000 Open Source Telecom Corporation.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

VPBPlay::VPBPlay(VPBTrunk *trunk, int h) :
URLAudio(), Service((Trunk *)trunk, keythreads.priAudio())
{
	handle = h;
	id = trunk->id;
	reset = false;
	vpbtr = trunk; // DR
}

VPBPlay::~VPBPlay()
{
	char buffer[12];

	// DR - make sure Run() thread has finished before we delete object
	if(reset)
	{
		vpb_play_terminate(handle);		
		yield();
	}

	terminate();
	sprintf(buffer, "%ld", getTransfered());
	trunk->setSymbol(SYM_PLAYED, buffer);
	sprintf(buffer, "%ld", getPosition());
	trunk->setSymbol(SYM_OFFSET, buffer);
	URLAudio::close();
}

char *VPBPlay::getContinuation(void)
{
	char *fn;

	if(data->play.mode == PLAY_MODE_ONE || data->play.mode == PLAY_MODE_TEMP)
		return NULL;
retry:
	fn = getPlayfile();
	if(fn && (data->play.mode == PLAY_MODE_ANY || data->play.mode == PLAY_MODE_MOH))
		if(!canAccess(fn))
			goto retry;
	return fn;
}

void VPBPlay::initial(void)
{
	int codec = VPB_MULAW;
	char buffer[32];
	char *fn;
	struct stat ino;
	struct tm *dt;
	struct tm tbuf;
	char *ann;

	trunk->getName(buffer);

	if(tts)
		if(!tts->synth(id, data))
			Service::failure();

retry:
	fn = getPlayfile();
	if(!fn)
	{
		if(data->play.lock)
		{
			cachelock.unlock();
			data->play.lock = false;
		}
		if(data->play.mode == PLAY_MODE_ANY || data->play.mode == PLAY_MODE_MOH)
			success();
		slog(Slog::levelError) << "no file" << endl;
		Service::failure();
	}
	stat(fn, &ino);
	open(fn);
	if(data->play.lock)
	{
		cachelock.unlock();
		data->play.lock = false;
	}
	if(!isOpen())
	{
		if(data->play.mode == PLAY_MODE_ANY || data->play.mode == PLAY_MODE_ONE)
			goto retry;

		if(data->play.mode == PLAY_MODE_MOH)
			goto retry;

		errlog("missing", "Prompt=%s", fn);
		slog(Slog::levelError) << fn << ": cannot open" << endl;
		Service::failure();
	}
	if(data->play.mode == PLAY_MODE_TEMP)
		remove(fn);
	if(data->play.offset)
		setPosition(data->play.offset);
	if(data->play.limit)
		setLimit(data->play.limit);
	dt = localtime_r(&ino.st_ctime, &tbuf);
	sprintf(buffer, "%04d%02d%02d,%02d%02d%02d",
		dt->tm_year + 1900, dt->tm_mon + 1, dt->tm_mday,
		dt->tm_hour, dt->tm_min, dt->tm_sec);
	trunk->setSymbol(SYM_CREATED, buffer);
	ann = getAnnotation();
	if(ann)
		trunk->setSymbol(SYM_ANNOTATION, ann);
	else
		trunk->setSymbol(SYM_ANNOTATION, "");

	switch(getEncoding())
	{
	case g721ADPCM:
	case okiADPCM:
		codec = VPB_OKIADPCM;
		bufsize = 80;
		samples = 160;
		break;
	case g723_3bit:
		codec = VPB_OKIADPCM24;
		samples = 160;
		bufsize = 60;
		break;
	case pcm16Mono:
		codec = VPB_LINEAR;
		bufsize = 320;
		samples = 160;
		break;
	case mulawAudio:
		codec = VPB_MULAW;
		bufsize = 160;
		samples = 160;
		break;
	case alawAudio:
		codec = VPB_ALAW;
		bufsize = 160;
		samples = 160;
		break;
	default:
		slog(Slog::levelError) << buffer << ": unsupported codec required" << endl;
		Service::failure();
	}

	reset = true;
	vpb_play_buf_start(handle, codec);
}

void VPBPlay::run(void)
{
	char name[33];
	char buffer[bufsize];
	Audio::Error status = Audio::errSuccess;
	float gain;

	if(data->play.gain != 0.0)
	{
		vpb_play_get_gain(handle, &gain);
		gain += data->play.gain;

		if(gain > 12.0)
			gain = 12.0;

		if(gain < -12.0)
			gain = -12.0;

		vpb_play_set_gain(handle, gain);
	}

	trunk->getName(name);

	setCancel(cancelDeferred);
	while(status == Audio::errSuccess)
	{	
		status = getSamples(buffer, samples);
		if(status == errReadIncomplete && data->play.maxtime)
		{
			setPosition(0);
			Thread::yield();
			status = Audio::errSuccess;
			continue;
		}
		if(status == errReadIncomplete || status == Audio::errSuccess)
			if(vpb_play_buf_sync(handle, buffer, bufsize) != VPB_OK)
				status = errWriteIncomplete;
		Thread::yield();
	}
	vpb_play_buf_finish(handle);
	reset = false;
	if(status == errReadFailure)
		slog(Slog::levelError) << name << ": failed playback" << endl;
	success();
}

bool VPBTrunk::playHandler(TrunkEvent *event)
{
	char *tag;

	switch(event->id)
	{
	case TRUNK_STOP_STATE:
		if(thread)
		{
			stopped = true;
			vpb_play_terminate(handle);	
			return true;
		}
		endTimer();
		vpb_play_set_gain(handle, outgain);
		handler = &VPBTrunk::stepHandler;
		return true;
	case TRUNK_EXIT_STATE:
		if(!thread)
			return true;
		endTimer();
		stopServices();
		vpb_play_set_gain(handle, outgain);
		handler = &VPBTrunk::stepHandler;
		return true;
	case TRUNK_SERVICE_SUCCESS:
		if(!stopped)
			trunkSignal(TRUNK_SIGNAL_STEP);
		exitThread();
		return true;
	case TRUNK_SERVICE_FAILURE:
		if(!stopped)
		{
			setSymbol(SYM_ERROR, "play-failed");
			trunkSignal(TRUNK_SIGNAL_ERROR);
		}
		exitThread();
		return true;
	case TRUNK_ENTER_STATE:
		stopped = false;
		enterState("play");
		flags.dsp = DSP_MODE_VOICE;
		status[tsid] = 'p';
		if(!flags.offhook)
		{
			flags.offhook = true;
			vpb_sethook_async(handle, VPB_OFFHOOK);
			setTimer(getPickupTimer());
			return true;
		}
	case TRUNK_TIMER_EXPIRED:
		if(thread)
			return false;

		if(data.play.maxtime)
			setTimer(data.play.maxtime);

		setDTMFDetect();
		thread = new VPBPlay(this, handle);
		thread->start();
		return true;
	}
	return false;
}

bool VPBTrunk::playwaitHandler(TrunkEvent *event)
{
        char buffer[65];

        switch(event->id)
        {
        case TRUNK_EXIT_SHELL:
                if(!tgi.pid)
                        return true;
                tgi.pid = 0;
                endTimer();
                if(event->parm.status)
                {
                        trunkSignal(TRUNK_SIGNAL_ERROR);
                        sprintf(buffer, "play-failed-exit-%d",
                                event->parm.status);
                        setSymbol(SYM_ERROR, buffer);
                        handler = &VPBTrunk::stepHandler;
                        return true;
                }
                handler = &VPBTrunk::playHandler;
                return true;
        case TRUNK_TIMER_EXPIRED:
                if(tgi.pid)
                {
                        kill(tgi.pid, SIGTERM);
                        tgi.pid = 0;
                }
                sprintf(buffer, "play-failed-timeout");
                setSymbol(SYM_ERROR, buffer);
                trunkSignal(TRUNK_SIGNAL_ERROR);
                handler = &VPBTrunk::stepHandler;
                return true;                                                   
        case TRUNK_ENTER_STATE:
		stopped = false;
		enterState("playwait");
                endTimer();
                Trunk::setDTMFDetect();
                setTimer(data.play.timeout);
                return true;
        }
        return false;                                                          
}


#ifdef	CCXX_NAMESPACES
};
#endif
