// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

bool VPBTrunk::seizeHandler(TrunkEvent *event)
{
	char script[256];
	const char *start = NULL;
	const char *cp;

	switch(event->id)
	{
	case TRUNK_ENTER_STATE:
		enterState("seize");
		status[tsid] = 's';
		flags.dsp = DSP_MODE_VOICE;
		flags.offhook = true;
		vpb_sethook_async(handle, VPB_OFFHOOK);

		setTimer(group->getDialtone());	
	case TRUNK_STOP_DISCONNECT:
		return true;
	case TRUNK_CPA_REORDER:
		syncParent("start:reorder");
		if(!trunkEvent("pickup:reorder"))
			goto failed;
		goto success;
	case TRUNK_CPA_BUSYTONE:
		syncParent("start:busy");
		if(!trunkEvent("pickup:busy"))
			goto failed;
		goto success;
			
	case TRUNK_CPA_DIALTONE:
		cp = getSymbol(SYM_DIALER);
		if(cp && *cp)
		{
			dialRewrite(cp);	// rewrite by rules
			data.dialxfer.offhook = 0;
			data.dialxfer.onhook = 0;
			data.dialxfer.sync = true;
			data.dialxfer.dialer = DTMF_DIALER;
//			snprintf(data.dialxfer.digits, sizeof(data.dialxfer.digits), "%s", cp);
//			data.dialxfer.digit = data.dialxfer.digits;
			data.dialxfer.exit = false;
			data.dialxfer.interdigit = group->getDialspeed();
			data.dialxfer.digittimer = data.dialxfer.interdigit / 2;
			data.dialxfer.timeout = 0;
			handler = &VPBTrunk::digitDialer;
			syncParent("start:dialing");
			return true;
		}
		syncParent("start:running");
success:
		status[tsid] = 'o';
		handler = &VPBTrunk::stepHandler;
		return true;
	case TRUNK_TIMER_EXPIRED:
		syncParent("start:failed");
failed:
		if(trunkEvent("pickup:failed"))
			goto success;

		flags.dsp = DSP_MODE_INACTIVE;
		slog(Slog::levelWarning) << name << ": sieze failed; no dialtone in " << group->getDialtone() << "ms" << endl;
		detach();
//		start = group->getSchedule(script);
//		if(attach(start))
//		{
//			handler = &VPBTrunk::stepHandler;
//			return true;
//		}
		handler = &VPBTrunk::hangupHandler;
		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
