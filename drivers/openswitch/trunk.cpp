// Copyright (C) 2000 Open Source Telecom Corporation.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

unsigned VPBTrunk::_trkcount = 0;

VPBTrunk::VPBTrunk(int card, int ts, bool sta) :
Trunk((card - 1) * vpbivr.getCardSize() + ts, &vpbivr, card)
{
	VPB_DETECT reorder;
	const char *cp;
	char cbuf[65];
	char *tok;
	unsigned pid;
	TrunkEvent event;
	ringIndex = NULL;
	handler = NULL;
	joined = NULL;
	timer = NULL;
	listen = NULL;
	unsigned f1, f2;
	lastring = 0;
	inTimer = isRinging = false;
	isStation = sta;
	_bank = ((card - 1) * (vpbivr.getCardSize() / 4) + ts / 4) + 1;
	_card = card;
	_port = ts;

	softJoin = new AudioBuffer();

	if(vpbivr.getExtCount())
	{
		if(isStation)
			snprintf(name, sizeof(name), "sta(%d,%d)", card, ts);
		else
		{
			_trk = _trkcount ++;
			dialgroup[9] = true;
			snprintf(name, sizeof(name), "trk(%d,%d)", card, ts);
		}
	}
	else
	{
		_trk = _trkcount++;
		snprintf(name, sizeof(name), "vpb(%d,%d)", card, ts);
	}

	snprintf(name, sizeof(name), "vpb/%d", id);

	if(isStation)
	{
		ringIndex = new bool[vpbivr.getTrunkCount()];
		for(pid = 0; pid < (unsigned)vpbivr.getTrunkCount(); ++pid)
			ringIndex[pid] = false;
	}

	handle = vpb_open(card, ts + 1);
	vpb_timer_open(&timer, handle, id, 0);
	

#ifdef	VPB_V12PCI
	if(isStation)
	{
		vpb_disable_event(handle, VPB_MRING);
		cp = NULL;
	}
	else
	{
		cp = vpbivr.VPBConfig::getLast("reorder");
	}
#else
	cp = vpbivr.VPBConfig::getLast("reorder");
#endif

	if(cp)
	{
		reorder.stran[0].type = reorder.stran[2].type = VPB_RISING;
		reorder.stran[1].type = VPB_FALLING;
		reorder.stran[0].tfire = reorder.stran[1].tfire =  reorder.stran[2].tfire = 0;
		reorder.stran[0].tmin = reorder.stran[0].tmax = 0;
		reorder.stran[1].tmin = reorder.stran[2].tmin = 220;
		reorder.stran[1].tmax = reorder.stran[2].tmax = 280;

		reorder.nstates = 3;
		reorder.tone_id = VPB_GRUNT + 1;

		strcpy(cbuf, cp);
		cp = strtok_r(cbuf, " \t\r\n,;:", &tok);
		f1 = atoi(cp);
		cp = strtok_r(NULL, " \t\r\n,;:", &tok);
		if(cp)
			f2 = atoi(cp);
		else
			f2 = 0;

		reorder.glitch = 40;
		reorder.snr = 10;

		reorder.freq1 = f1;
		reorder.bandwidth1 = 100;
		reorder.minlevel1 = -20;

		if(f2)
		{
			reorder.ntones = 2;
			reorder.twist = 10;
			reorder.freq2 = f2;
			reorder.bandwidth2 = 100;
			reorder.minlevel2 = -20;
		}
		else
		{
			reorder.ntones = 1;
			reorder.twist = 0;
			reorder.freq2 = 0;
			reorder.bandwidth2 = 0;
			reorder.minlevel2 = 0;
		}
		vpb_settonedet(handle, &reorder);
	}

	vpb_play_get_gain(handle, &outgain);
	vpb_record_get_gain(handle, &inpgain);

	event.id = TRUNK_ENTER_STATE;
	handler = &VPBTrunk::idleHandler;
	postEvent(&event);
}

VPBTrunk::~VPBTrunk()
{
	handler = NULL;
	endTimer();

	if(listen)
	{
		delete listen;
		listen = NULL;
	}

#ifdef	VPB_V12PCI
	if(isStation && rings)
		vpb_ring_station_sync(handle, VPB_RING_STATION_OFF);
#endif		

	if(timer)
		vpb_timer_close(timer);
	if(ringIndex)
		delete[] ringIndex;
	vpb_sethook_sync(handle, VPB_ONHOOK);
	vpb_close(handle);
	slog(Slog::levelInfo) << name << ": device stopped" << endl;
}

bool VPBTrunk::setListen(bool start)
{
	if(start)
	{
		if(listen)
			listen->setListen(false);
		else
		{
			listen = new VPBListen(this, handle, 0.0, false);
			thread = NULL;
			listen->start();
		}
		flags.listen = true;
		return true;
	}
	if(!listen)
		return false;

	if(listen)
	{
		vpb_record_terminate(handle);
		Thread::yield();
		delete listen;
	}
	listen = NULL;
	flags.listen = false;
	return true;
}

void VPBTrunk::stopThreads(void)
{
	stopServices();
	if(listen)
	{
		vpb_record_terminate(handle);
		Thread::yield();
		delete listen;
	}
	listen = NULL;
	flags.listen = false;
}

void VPBTrunk::clearRinging(void)
{
	unsigned pid, max = vpbivr.getTrunkCount();
	TrunkEvent event;
	VPBTrunk *trunk;

	if(!isRinging)
		return;

	isRinging = false;

	if(ringIndex)
	{
		if(ringIndex[id])
		{
			--rings;
			ringIndex[id] = false;
		}
	}

	for(pid = 0; pid < max; ++pid)
	{
		trunk = (VPBTrunk *)vpbivr.getTrunkPort(pid);
		if(!trunk)
			continue;

		if(trunk == this)
			continue;

		if(!trunk->isStation)
			continue;

		if(!trunk->ringIndex[id])
			continue;

		event.id = TRUNK_STOP_RINGING;
		event.parm.tid = id;
		trunk->postEvent(&event);		
	}
}

unsigned long VPBTrunk::getCapabilities(void)
{
	if(isStation)
		return TRUNK_CAP_VOICE | TRUNK_CAP_DIAL | TRUNK_CAP_STATION;
	
	return TRUNK_CAP_VOICE | TRUNK_CAP_DIAL;
}

void VPBTrunk::exitThread(void)
{
        VPB_EVENT e;

        e.handle = handle;
        e.data = 0;
        e.type = VPB_DIALEND;           // user defined
        vpb_put_event(&e);
}

unsigned VPBTrunk::getPickupTimer(void)
{
	unsigned min = vpbivr.getHookTimer();
	unsigned pu = group->getPickup();
	if(pu < min)
		return min;

	return pu;
}

void VPBTrunk::getName(char *buffer)
{
	if(isStation && extNumber[0])
	{
		sprintf(buffer, "ext/%s", extNumber);
		return;
	}
	strcpy(buffer, name);
}

void VPBTrunk::exit(void)
{
	if(!flags.onexit)
		if(redirect("::exit"))
		{
			autoloop(false);
			flags.onexit = true;
			return;
		}

	handler = &VPBTrunk::hangupHandler;
}

void VPBTrunk::setTimer(timeout_t timeout)
{

	if(inTimer)
		vpb_timer_stop(timer);

	vpb_timer_change_period(timer, timeout);
        vpb_timer_start(timer);
	inTimer = true;
}

void VPBTrunk::endTimer(void)
{
	if(inTimer)
		vpb_timer_stop(timer);

	inTimer = false;
}

void VPBTrunk::disjoin(trunkevent_t reason)
{
        TrunkEvent event;

        if(!joined)
                return;

	if(data.join.local)
	{
        	if(bridge < 1 || bridge > 2)
		{
			slog(Slog::levelWarning) << name << ": disjoin called with invalid bridge resource " << bridge << endl;
			return;
		}

		vpbivr.freeBridge(_card, bridge);
       	 	vpb_bridge(handle, ((VPBTrunk *)joined)->handle, VPB_BRIDGE_OFF, bridge);
		bridge = 0;
		((VPBTrunk *)joined)->bridge = 0;
	}
	
        joined->setJoined(NULL);
	event.id = TRUNK_PART_TRUNKS;
	event.parm.reason = reason;
	joined->postEvent(&event);
        joined = NULL;
}

void VPBTrunk::setOffhook(void)
{
	timeout_t timer;
	if(!flags.offhook)
	{
		if(!answered)
			exittimer = 0;
		answered = true;
		vpb_sethook_async(handle, VPB_OFFHOOK);
		timer = vpbivr.getHookTimer();
	}
	flags.offhook = true;
}

bool VPBTrunk::postEvent(TrunkEvent *event)
{
	bool rtn = true;
	trunkhandler_t prior;
	char *tag;
	char *dtargs[2];
	char evt[65];

	enterMutex();
	switch(event->id)
	{
	case TRUNK_TIMER_SYNC:
		if(!synctimer)
			rtn = false;
		synctimer = 0;
		break;
	case TRUNK_TIMER_EXIT:
		if(!exittimer)
			rtn = false;
		exittimer = 0;
		break;
	case TRUNK_TIMER_EXPIRED:
		if(!inTimer)
			rtn = false;
		break;
	case TRUNK_START_INTERCOM:
		if(handler != &VPBTrunk::idleHandler && handler != &VPBTrunk::ringStation)
			rtn = false;
	case TRUNK_START_RINGING:
	case TRUNK_STOP_RINGING:
		if(!isStation)
			rtn = false;
		break;
	case TRUNK_STATION_PICKUP:
		if(isActive())
			break;
		if(handler != &VPBTrunk::idleHandler || isStation)
		{
			rtn = false;
			break;
		}
//		dtargs[1] = NULL;
//		dtargs[0] = (char *)group->getLast("dialing");
//		event->parm.argv = dtargs;
		break;
	case TRUNK_STATION_ANSWER:
		if(handler != &VPBTrunk::toneHandler)
			rtn = false;
		break;
	case TRUNK_DTMF_KEYUP:
		if(flags.offhook)
			time(&idletime);
		if(!flags.dtmf)
			rtn = false;
		break;
	}	
	if(!rtn)
	{
		leaveMutex();
		return false;
	}

	if(!handler)
	{
		slog(Slog::levelWarning) << name;
		slog() << ": no handler active; event=" << event->id << endl;
		leaveMutex();
		return false;
	}

retry:
	debug->debugEvent(this, event);
	prior = handler;
	rtn = (this->*handler)(event);
	if(rtn)
	{
		if(handler != prior)
		{
			if(prior == &VPBTrunk::idleHandler)
				setIdle(false);
			event->id = TRUNK_ENTER_STATE;
			goto retry;
		}
		leaveMutex();
		return true;
	}

	// default handler

	rtn = true;
	switch(event->id)
	{
	case TRUNK_RINGING_ON:
		if(flags.offhook)
		{
			if(trunkSignal(TRUNK_SIGNAL_RING))
			{
				event->id = TRUNK_STOP_STATE;
				goto retry;
			}
			else
				event->id = TRUNK_STOP_DISCONNECT;
		}
		else if(!answered && exittimer)
		{
			time(&exittimer);
			exittimer += 6;			
		}
		++rings;
		snprintf(evt, sizeof(evt), "ring:%d", rings);
		if(trunkEvent(evt))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		break;
	case TRUNK_ENTER_STATE:
		if(flags.offhook)
			setDTMFDetect();
		else
			setDTMFDetect(false);
		endTimer();
		break;
	case TRUNK_CPA_GRUNT:
		if(listen)
		{
			// if we are already in asr, no need to use event
			if(listen->getListen())
			{
				leaveMutex();
				return true;
			}

			// we heard grunt, have asr active, see if can barge
			setSymbol(SYM_TONE, "grunt");
			if(trunkEvent("tone:grunt"))
			{
				event->id = TRUNK_STOP_STATE;
				goto retry;
			}
			if(trunkEvent("tone:voice"))
			{
				event->id = TRUNK_STOP_STATE;
				goto retry;
			}
		}
		break;
	case TRUNK_CPA_REORDER:
		if(vpbivr.getExtCount() > 0)
			break;
		event->parm.tone.name = "reorder";
		if(trunkEvent("tone:reorder"))
		{
			setSymbol(SYM_TONE, "reorder");
			event->id = TRUNK_STOP_STATE;
		}
		else if(trunkSignal(TRUNK_SIGNAL_TONE))
		{
			setSymbol(SYM_TONE, "reorder");
			event->id = TRUNK_STOP_STATE;
		}
		else
			event->id = TRUNK_STOP_DISCONNECT;
		goto retry;
	case TRUNK_LINE_WINK:
		if(trunkEvent("line:wink"))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		if(!flags.offhook)
			break;
		goto drop;
	case TRUNK_CPA_DIALTONE:
		if(isStation)
			break;
		if(trunkEvent("tone:dialtone"))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		goto drop;
	case TRUNK_STOP_DISCONNECT:
	case TRUNK_STATION_ONHOOK:
drop:
		if(flags.onexit)
			break;
		if(trunkSignal(TRUNK_SIGNAL_HANGUP))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		break;
	case TRUNK_ASR_TEXT:
		setSymbol(SYM_NOTIFYTYPE, "asr");
		setSymbol(SYM_NOTIFYTEXT, event->parm.send.msg);
		if(trunkSignal(TRUNK_SIGNAL_NOTIFY))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		break;
	case TRUNK_SEND_MESSAGE:
		if(recvEvent(event))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		break;
	case TRUNK_STATION_FLASH:
		if(trunkSignal(TRUNK_SIGNAL_FLASH))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		break;
        case TRUNK_TIMER_SYNC:
                if(trunkSignal(TRUNK_SIGNAL_TIME))
                {
                        event->id = TRUNK_STOP_STATE;
                        goto retry;
                }
                break;
        case TRUNK_TIMER_EXIT:
		if(!answered)
		{
			answered = true;
			event->id = TRUNK_STOP_DISCONNECT;
			goto retry;
		}
                if(trunkSignal(TRUNK_SIGNAL_TIME))
                        event->id = TRUNK_STOP_STATE;
                else
                        event->id = TRUNK_STOP_DISCONNECT;
                goto retry;
	case TRUNK_TIMER_EXPIRED:
		if(!trunkSignal(TRUNK_SIGNAL_TIMEOUT))
			trunkSignal(TRUNK_SIGNAL_STEP);
		event->id = TRUNK_STOP_STATE;
		goto retry;
		break;
	case TRUNK_TONE_START:
		setSymbol(SYM_TONE, event->parm.tone.name);
		snprintf(evt, sizeof(evt), "tone:5s", event->parm.tone.name);
		if(trunkEvent(evt))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		if(trunkSignal(TRUNK_SIGNAL_TONE))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		break;
	case TRUNK_SYNC_PARENT:
		if(trunkEvent(event->parm.sync.msg))
		{
			setSymbol(SYM_STARTID, event->parm.sync.id);
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		rtn = false;
		break;
	case TRUNK_CHILD_EXIT:
		if(!isActive())
			break;
		if(trunkSignal(TRUNK_SIGNAL_CHILD))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		break;
	case TRUNK_STATION_PICKUP:
		if(!isActive())
			break;
		if(trunkSignal(TRUNK_SIGNAL_PICKUP))
		{
			tag = event->parm.trunk->getSymbol(SYM_GID);
			if(!tag)
				tag = "";
			else
				tag = strchr(tag, '-');
			setSymbol(SYM_RECALL, tag);
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		rtn = false;
		break;
	case TRUNK_DTMF_KEYUP:
		if(digits < MAX_DIGITS)
			dtmf.bin.data[digits++] = digit[event->parm.dtmf.digit];
		dtmf.bin.data[digits] = 0;
		snprintf(evt, sizeof(evt), "digits:%s", dtmf.bin.data);
		if(trunkEvent(evt))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		if(trunkSignal((trunksignal_t)(event->parm.dtmf.digit + TRUNK_SIGNAL_0)))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		break;
	case TRUNK_EXIT_SHELL:
                if(event->parm.exitpid.seq != tgi.seq)
                        break;
                tgi.pid = 0;
                if(tgi.fd > -1)
                {
                        close(tgi.fd);
                        tgi.fd = -1;
			snprintf(evt, sizeof(evt), "exit:%d",
				event->parm.exitpid.status);
			if(!trunkEvent(evt))
	                        trunkSignal(TRUNK_SIGNAL_STEP);
                        event->id = TRUNK_STOP_STATE;
                        goto retry;
                }
                break;
	case TRUNK_STOP_STATE:
		endTimer();
		handler = &VPBTrunk::stepHandler;
		break;
	case TRUNK_EXIT_STATE:
		break;
	case TRUNK_MAKE_STANDBY:
	case TRUNK_MAKE_BUSY:
		handler = &VPBTrunk::busyHandler;
		break;
	case TRUNK_START_RINGING:
		if(!isStation)
		{
			rtn = false;
			break;
		}
		if(!ringIndex[event->parm.tid])
		{
			++rings;
			ringIndex[event->parm.tid] = true;
		}		
		break;
	case TRUNK_STOP_RINGING:
		if(!isStation)
		{
			rtn = false;
			break;
		}
		if(ringIndex[event->parm.tid])
		{
			--rings;
			ringIndex[event->parm.tid] = false;
		}
		break;
	case TRUNK_MAKE_IDLE:
		handler = &VPBTrunk::idleHandler;
		break;
	default:
		rtn = false;
	}
	if(handler != prior)
	{
		event->id = TRUNK_ENTER_STATE;
		goto retry;
	}
	leaveMutex();
	return rtn;
}

unsigned long VPBTrunk::getIdleTime(void)
{
	time_t now;

	time(&now);
	if(handler == &VPBTrunk::idleHandler)
		return now - idletime;

	return 0;
}

#ifdef	CCXX_NAMESPACES
};
#endif
