// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

void VPBTrunk::initSyms(void)
{
	char buf[4];

	snprintf(buf, sizeof(buf), "%d", _card);
	setConst(SYM_CARD, buf);
	snprintf(buf, sizeof(buf), "%d", _bank);
	setConst(SYM_BANK, buf);
	setConst(SYM_NETWORK, "soft");
	if(isStation)
	{
		setConst(SYM_TRUNKID, "0");
		setConst(SYM_INTERFACE, "station");
	}
	else
	{
		snprintf(buf, sizeof(buf), "%d", _trk);
		setConst(SYM_TRUNKID, buf);
		setConst(SYM_INTERFACE, "analog");
	}
	setSymbol(SYM_BUFFER, "12000");
}

bool VPBTrunk::idleHandler(TrunkEvent *event)
{
	char script[256];
	int argc = 0;
	const char *start = NULL;
	const char *value;
	unsigned chk;
	Request *req;
	char **argv;
	char *stargs[2];
#ifdef	VPB_CID_MAX_DLP
	VPB_CID cid;
	char dlp[VPB_CID_MAX_DLP];
	unsigned len;
#endif
	VPBTrunk *trunk;

	switch(event->id)
	{
	case TRUNK_ENTER_STATE:
		flags.cid = flags.sent = false;
		synctimer = exittimer = 0;
		enterState("idle");
		setIdle(true);
		tgi.pid = 0;
		digits = 0;
		answered = true;
		disjoin(TRUNK_STOP_DISCONNECT);

#ifdef	VPB_CID_MAX_DLP
		vpb_cid_set(&cid, VPB_CID_EMPTY, NULL);
		if(extNumber[0])
		{
			snprintf(script, sizeof(script), "EXT %s", extNumber);
			vpb_cid_set(&cid, VPB_CID_CALLING_LINE_DN, extNumber);
			vpb_cid_set(&cid, VPB_CID_CALLER_NAME, script);
			len = vpb_cid_compose_dlp(&cid, dlp);
			cidlen = 0;
			vpb_cid_compose_wav(dlp, len, (short *)cidbuf, &cidlen);
			flags.cid = true;
		}
		else if(!isStation)
		{
                        snprintf(script, sizeof(script), "CO %d", _trk);
			vpb_cid_set(&cid, VPB_CID_CALLER_NAME, script);
                        len = vpb_cid_compose_dlp(&cid, dlp);
			cidlen = 0;
                        vpb_cid_compose_wav(dlp, len, (short *)cidbuf, &cidlen);
                        flags.cid = true;
		}	
#endif

		vpb_play_set_gain(handle, outgain);
		vpb_record_set_gain(handle, inpgain);

		vpb_sethook_async(handle, VPB_ONHOOK);
		setDTMFDetect(false);
		flags.dsp = DSP_MODE_INACTIVE;
		flags.offhook = false;

		switch(flags.trunk)
		{
		case TRUNK_MODE_OUTGOING:
			group->decOutgoing();
			break;
		case TRUNK_MODE_INCOMING:
			group->decIncoming();
		}

		flags.trunk = TRUNK_MODE_INACTIVE;
		time(&idletime);
		ScriptSymbol::purge();
		clearRinging();

#ifdef	VPB_V12PCI
		if(isStation && rings)
		{
			handler = &VPBTrunk::ringStation;
			return true;
		}
#endif

		status[tsid] = '-';
		rings = 0;
		value = group->chkRequest();
		if(!atoi(value) && stricmp(value, "hangup"))
			return true;
		chk = atoi(value);
		if(chk < group->getReady())
			chk = group->getReady();
		setTimer(chk);
		return true;
#ifdef	VPB_V12PCI
	case TRUNK_START_INTERCOM:
		if(!isStation)
			return false;

		if(!ringIndex[event->parm.intercom.tid])
		{
			ringIndex[event->parm.intercom.tid] = true;
			++rings;
		}
		Trunk::data.answer.intercom = vpbivr.getTrunkPort(event->parm.intercom.tid);
		Trunk::data.answer.transfer = event->parm.intercom.transfer;
		handler = &VPBTrunk::ringIntercom;
		return true;
	
	case TRUNK_START_RINGING:
		if(!isStation)
			return false;

		if(ringIndex[event->parm.tid])
			return true;

		ringIndex[event->parm.tid] = true;
		++rings;
		handler = &VPBTrunk::ringStation;
		return true;
#endif
	case TRUNK_STATION_FLASH:
	case TRUNK_STATION_ONHOOK:
	case TRUNK_STOP_DISCONNECT:
	case TRUNK_LINE_WINK:
	case TRUNK_CPA_DIALTONE:
	case TRUNK_MAKE_IDLE:
		return true;
	case TRUNK_MAKE_BUSY:
		endTimer();
		handler = &VPBTrunk::busyHandler;
		return true;
	case TRUNK_TIMER_EXPIRED:	
		flags.ready = true;
		req = group->getRequest();
		if(req)
		{
			setConst(SYM_PARENT, req->getParent());
			argv = req->getList();
			start = *(argv++);
			if(attach(start))
			{
				flags.trunk = TRUNK_MODE_OUTGOING;
				setList(argv);
				flags.ready = false;
				endTimer();
				handler = &VPBTrunk::seizeHandler;
				group->incOutgoing();
				delete req;
				return true;
			}
			syncParent("start:failed");
			ScriptSymbol::purge();
			delete req;
		}
		chk = atoi(group->chkRequest());
		if(chk)
			setTimer(chk);
		return true;
#ifdef	VPB_V12PCI
	case TRUNK_STATION_OFFHOOK:
#ifdef	VPB_CID_MAX_DLP
		if(extNumber[0])
		{
	                vpb_cid_set(&cid, VPB_CID_EMPTY, NULL);
                       	snprintf(script, sizeof(script), "EXT %s", extNumber);
			vpb_cid_set(&cid, VPB_CID_CALLING_LINE_DN, extNumber);
			vpb_cid_set(&cid, VPB_CID_CALLER_NAME, script);
                        len = vpb_cid_compose_dlp(&cid, dlp);
			cidlen = 0;
                        vpb_cid_compose_wav(dlp, len, (short *)cidbuf, &cidlen);
                        flags.cid = true;
		}
		else
			flags.cid = false;
#endif
		stargs[1] = NULL;
		if(!extNumber[0])
		{
			setConst(SYM_CALLTYPE, "service");
			stargs[0] = (char *)group->getLast("stationnoservice");
		}
		else if(handler == &VPBTrunk::idleHandler)
		{
			setConst(SYM_CALLTYPE, "station");
			stargs[0] = (char *)group->getLast("stationoffhook");
		}
		else if(handler == &VPBTrunk::ringIntercom)
		{
			setConst(SYM_CALLTYPE, "intercom");
			stargs[0] = (char *)group->getLast("stationanswering");
			if(!stargs[0])
			{
				trunk = (VPBTrunk *)data.answer.intercom;
				if(trunk)
					stargs[0] = (char *)trunk->group->getSchedule(script);
			}
		}
		else
		{
			setConst(SYM_CALLTYPE, "ringing");
			stargs[0] = (char *)group->getLast("stationringing");
		}

		flags.offhook = true;
	case TRUNK_STATION_PICKUP:
		if(event->id == TRUNK_STATION_PICKUP)
		{
			flags.offhook = false;
			setConst(SYM_CALLTYPE, "dialing");		
			trunk = (VPBTrunk *)event->parm.trunk;
			stargs[0] = (char *)group->getLast("stationdialing");
			if(!stargs[0] && trunk)
				stargs[0] = (char *)trunk->group->getSchedule(script);
			stargs[1] = NULL;
		}

                if(stargs[0])
                        if(!stricmp(stargs[0], "*"))
                                stargs[0] = NULL;
                event->parm.argv = stargs;

#endif
	case TRUNK_START_SCRIPT:
		flags.trunk = TRUNK_MODE_OUTGOING;

	case TRUNK_RING_START:
		start = event->parm.argv[0];
		if(start)
		{
			if(strchr(start, '='))
				start = group->getSchedule(script);
			else
				++argc;
		}
		if(flags.trunk == TRUNK_MODE_INACTIVE)
			flags.trunk = TRUNK_MODE_INCOMING;
		if(!isStation)
			rings = 0;
		if(!start)
			start = group->getSchedule(script);
		if(attach(start))
		{
			setList(&event->parm.argv[argc]);
			flags.ready = false;
			endTimer();		
			if(flags.trunk == TRUNK_MODE_OUTGOING)
			{
				if(event->id == TRUNK_STATION_OFFHOOK || event->id == TRUNK_STATION_PICKUP)
				{
					syncParent("start:running");
					handler = &VPBTrunk::stepHandler;
				}
				else
					handler = &VPBTrunk::seizeHandler;
				group->incOutgoing();
			}
			else
			{
				handler = &VPBTrunk::stepHandler;
				group->incIncoming();
			}
			return true;
		}
		syncParent("start:failed");
		ScriptSymbol::purge();
		flags.trunk = TRUNK_MODE_INACTIVE;
	
		return true;		
	case TRUNK_RING_REDIRECT:
		start = group->getRedirect(event->parm.argv[0], script);
		if(!isStation)
			rings = 0;
		if(attach(start))
		{
			setConst(SYM_REDIRECT, event->parm.argv[0]);
			setList(&event->parm.argv[1]);
			flags.ready = false;
			flags.trunk = TRUNK_MODE_INCOMING;
			endTimer();
			handler = &VPBTrunk::stepHandler;
			group->incIncoming();		
			return true;
		}
		ScriptInterp::purge();
		slog(Slog::levelError) << name << ": " << event->parm.argv[0] << ": unable to redirect" << endl;
		return true;
	case TRUNK_RINGING_ON:
		if(!group->getAnswer())
			return true;
		rings = 1;
		flags.trunk = TRUNK_MODE_INCOMING;
		flags.ready = false;
		endTimer();
		group->incIncoming();
		handler = &VPBTrunk::ringHandler;
		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
