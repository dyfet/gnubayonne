// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

VPBTone::VPBTone(VPBTrunk *trunk, int h) :
Service((Trunk *)trunk, keythreads.priAudio()), TimerPort()
{
	handle = h;
	reset = false;
	trk = trunk; // DR
}

VPBTone::~VPBTone()
{
	// DR - make sure Run() thread has finished before we delete object
	if(reset)
	{
		vpb_play_terminate(handle);		
		yield();
	}

	terminate();
}

void VPBTone::run(void)
{
	char name[33];
	phTone *tone = trk->getTone();
// data.tone.tone;
	timeout_t sync, max = keythreads.getStepInterval();
	unsigned save = tone->getDuration() * 8;
	char *ptr, *psave = (char *)tone->getSamples();
	unsigned len, size;

	trunk->getName(name);
	vpb_play_buf_start(handle, VPB_MULAW);

	setCancel(cancelDeferred);
	while(trk->data.tone.loops-- && !trk->stopped)
	{	
		ptr = psave;
		size = save;
		setTimer(trk->data.tone.wakeup);
		if(size > trk->data.tone.wakeup * 8 && trk->data.tone.wakeup)
			size = trk->data.tone.wakeup * 8;

		while(size && !trk->stopped)
		{
			if(size > 1024)
				len = 1024;
			else
				len = size;
			
			if(vpb_play_buf_sync(handle, ptr, len) != VPB_OK)
				trk->stopped = true;
			
			size -= len;
			ptr += len;
		}
		while((sync = getTimer()) && !trk->stopped)
		{
			if(sync > max)
				sync = max;
			Thread::sleep(sync);
		}
		Thread::yield();
	}
	vpb_play_buf_finish(handle);
	reset = false;
	success();
}

bool VPBTrunk::toneHandler(TrunkEvent *event)
{
	const char *tag;

	TrunkEvent evt;
	switch(event->id)
	{
	case TRUNK_STATION_ANSWER:
		if(!data.tone.dialing)
			return false;
		if(event->parm.trunk != data.tone.dialing)
			return false;
		if(!trunkSignal(TRUNK_SIGNAL_ANSWER))
			trunkSignal(TRUNK_SIGNAL_STEP);
	case TRUNK_STOP_STATE:
		if(data.tone.dialing)
		{
			evt.id = TRUNK_STOP_RINGING;
			evt.parm.tid = id;
			data.tone.dialing->postEvent(&evt);
		}
		if(thread)
		{
			stopped = true;
			vpb_play_terminate(handle);	
			return true;
		}
		stopServices();
		endTimer();
		handler = &VPBTrunk::stepHandler;
		return true;
	case TRUNK_EXIT_STATE:
		if(!thread)
			return true;
		stopServices();
		endTimer();
		handler = &VPBTrunk::stepHandler;
		return true;
	case TRUNK_SERVICE_SUCCESS:
	case TRUNK_SERVICE_FAILURE:
		if(data.tone.dialing)
		{
			evt.id = TRUNK_STOP_RINGING;
			evt.parm.tid = id;
			data.tone.dialing->postEvent(&evt);
			if(!stopped)
			{
				if(!trunkSignal(TRUNK_SIGNAL_NOANSWER))
					error("dial-noanswer");
			}
		}
		else if(!stopped)
			trunkSignal(TRUNK_SIGNAL_STEP);
		exitThread();
		return true;
	case TRUNK_ENTER_STATE:
		stopped = false;
		enterState("tone");
		flags.dsp = DSP_MODE_VOICE;
		status[tsid] = 't';
		setDTMFDetect();
		thread = new VPBTone(this, handle);
		thread->start();
		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
