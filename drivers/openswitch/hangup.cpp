// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

bool VPBTrunk::hangupHandler(TrunkEvent *event)
{
	timeout_t reset;

	switch(event->id)
	{
	case TRUNK_ENTER_STATE:
		synctimer = exittimer = 0;
		enterState("hangup");
		status[tsid] = 'h';
		endTimer();
		disjoin(TRUNK_STOP_DISCONNECT);
		if(tgi.pid)
			kill(tgi.pid, SIGHUP);
		if(thread)
			reset = thread->stop();
		else
			reset = 0;
		setDTMFDetect(false);
		flags.dsp = DSP_MODE_INACTIVE;
		flags.offhook = false;
		vpb_sethook_async(handle, VPB_ONHOOK);
		detach();
		clearRinging();
		if(isStation)
			goto done;
		if(group->getHangup() > reset)
			setTimer(group->getHangup());
		else
			setTimer(reset);
		return true;
	case TRUNK_TIMER_EXPIRED:
done:
		stopThreads();
		flags.reset = false;
		handler = &VPBTrunk::idleHandler;
		return true;
	case TRUNK_MAKE_IDLE:
	case TRUNK_STOP_DISCONNECT:
	case TRUNK_LINE_WINK:
	case TRUNK_STATION_FLASH:
	case TRUNK_STATION_ONHOOK:
	case TRUNK_CPA_DIALTONE:
		return true;
	case TRUNK_RINGING_ON:
		endTimer();
		flags.reset = false;
		handler = &VPBTrunk::idleHandler;
		return idleHandler(event);
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
