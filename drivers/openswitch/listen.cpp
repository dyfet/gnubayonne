// Copyright (C) 2000 Open Source Telecom Corporation.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

VPBListen::VPBListen(VPBTrunk *trunk, int h, float g, bool start) :
Service((Trunk *)trunk, keythreads.priAudio())
{
	handle = h;
	reset = false;
	vpbtr = trunk; // DR
	gain = g;
	if(start)
		level = 2;
	else
		level = 0;
}

VPBListen::~VPBListen()
{
	char buffer[12];
	struct stat ino;
	int trim;

	// DR - make sure Run() thread has finised before we delete object
	if(reset)
	{
		vpb_record_terminate(handle);
		yield();
	}
	terminate();
}

bool VPBListen::getListen(void)
{
	if(level)
		return true;

	return false;
}

void VPBListen::setListen(bool start)
{
	if(start && level)
		return;

	if(start && trunk->getASR() < 0)
		return;

	if(start)
		level = 1;
	else
		level = 0;
}

void VPBListen::initial(void)
{
	int codec = VPB_LINEAR;
	char buffer[32];
	bufsize = 1024;

	reset = true;
	vpb_record_buf_start(handle, codec);
}

void VPBListen::run(void)
{
	unsigned bufcount = atoi(vpbivr.getLast("listen"));
	char name[33];
	char buffer[bufsize * bufcount];
	float newgain;
	char *bufpos;
	unsigned head = 0, tail = 0;

	if(gain != 0.0)
	{
		vpb_record_get_gain(handle, &newgain);
		newgain += gain;
		if(newgain < -12.0)
			newgain = -12.0;

		if(newgain > 12.0)
			newgain = 12.0;

		vpb_record_set_gain(handle, newgain);
	}
	
	trunk->getName(name);

	setCancel(cancelDeferred);
	for(;;)
	{
		bufpos = buffer + bufsize * tail;

		if(++tail >= bufcount)
			tail = 0;

		if(tail == head)
		{
			if(level)
				slog(Slog::levelError) <<
					name << ": listen overrun" << endl;
			if(++head >= bufcount)
				head = 0;
		}

		if(vpb_record_buf_sync(handle, bufpos, bufsize) != VPB_OK)
			break;

		if(!level || trunk->getASR() < 0)
			continue;

		while(head != tail)
		{
			level = 2;
			bufpos = buffer + bufsize * head;
			if(::write(trunk->getASR(), bufpos, bufsize) < 1)
				break;

			if(++head >= bufcount)
				head = 0;
		}
	}
	vpb_record_buf_finish(handle);
	reset = false;
	slog(Slog::levelError) << name << ": listen failed" << endl;
	success();
}

bool VPBTrunk::listenHandler(TrunkEvent *event)
{
	unsigned short mask;
	unsigned len, wc = 1;
	int argc = 0;
	Symbol *sym = (Symbol *)data.listen.save;
	char *fmt, *cp, *wl;

	switch(event->id)
	{
	case TRUNK_ASR_PARTIAL:
		if(asrPartial(event->parm.send.msg))
			goto stop;
		if(!asr.partial)
			return true;
		cp = (char *)event->parm.send.msg;
		while(cp && *cp)
		{
			if(*cp == ',')
				++wc;
			++cp;
		}
		if(data.listen.count && wc >= data.listen.count)
			goto setsymbol;

		if((cp = strrchr(event->parm.send.msg, ',')) != NULL)
			cp++;
		else
			cp = (char *)event->parm.send.msg;

		if(!cp)
		{
			slog(Slog::levelWarning) << "ASR string was unparseable" << endl;
			return true;
		}

		while(data.listen.wordlist[argc])
		{
			if(!stricmp(cp, data.listen.wordlist[argc]))
				goto setsymbol;
			argc++;
		}
		return true;
	case TRUNK_ASR_TEXT:
		if(asrPartial(event->parm.send.msg))
			goto stop;
setsymbol:
		if(sym)
		{
			if(sym->data[0])
				fmt = ",%s";
			else
				fmt = "%s";
			len = sym->flags.size - strlen(sym->data);
		}
		else
			len = 0;
		if(len)
		{
			sym->flags.initial = false;
			snprintf(sym->data + strlen(sym->data), len + 1, 
				fmt, event->parm.send.msg);
			if(sym->flags.commit)
				commit(sym);
		}
		setTimer(data.listen.next);
		trunkSignal(TRUNK_SIGNAL_STEP);
	case TRUNK_STOP_STATE:
stop:
		endTimer();
		stopped = true;
		if(listen)
		{
			listen->setListen(false);
			handler = &VPBTrunk::stepHandler;
			return true;
		}
		if(thread)
		{
			vpb_record_terminate(handle);
			return true;
		}
		vpb_record_set_gain(handle, inpgain);
		handler = &VPBTrunk::stepHandler;
		return true;
	case TRUNK_EXIT_STATE:
		if(listen)
		{
			listen->setListen(false);
			handler = &VPBTrunk::stepHandler;
			return true;
		}
		if(!thread)
			return true;
		stopServices();
		vpb_record_set_gain(handle, inpgain);
		handler = &VPBTrunk::stepHandler;
		return true;
	case TRUNK_DTMF_KEYUP:
		mask = (1 << event->parm.dtmf.digit);
		if(!(mask & data.listen.term)) 
			return false;
		if(listen)
		{
			listen->setListen(false);
			trunkSignal(TRUNK_SIGNAL_STEP);
			handler = &VPBTrunk::stepHandler;
			return true;
		}
		if(thread)
		{
			stopped = true;
			trunkSignal(TRUNK_SIGNAL_STEP);
			vpb_record_terminate(handle);
			return true;
		}
		return true;
	case TRUNK_SERVICE_SUCCESS:
		if(!stopped)
			trunkSignal(TRUNK_SIGNAL_STEP);
		exitThread();
		return true;
	case TRUNK_SERVICE_FAILURE:
		if(!stopped)
		{
			setSymbol(SYM_ERROR, "listen-failed");
			trunkSignal(TRUNK_SIGNAL_ERROR);
		}
		exitThread();
		return true;
	case TRUNK_ENTER_STATE:
		stopped = false;
		enterState("listen");
		flags.dsp = DSP_MODE_VOICE;
		status[tsid] = 'l';
		if(!flags.offhook)
		{
			flags.offhook = true;
			vpb_sethook_async(handle, VPB_OFFHOOK);
			setTimer(getPickupTimer());
			return true;
		}
		if(listen)
		{
			if(data.listen.term)
				setDTMFDetect(true);
			else
				setDTMFDetect();
			setTimer(data.listen.first);
			listen->setListen(true);
			return true;
		}
	case TRUNK_TIMER_EXPIRED:
		if(listen || thread)	// default timeout handler
			return false;

		if(data.listen.term)
			setDTMFDetect(true);
		else
			setDTMFDetect();
		thread = new VPBListen(this, handle, data.listen.gain, true);
		thread->start();
		setTimer(data.listen.first);
		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
