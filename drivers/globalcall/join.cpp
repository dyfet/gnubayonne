// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne 
// with the Dialogic runtime libraries to produce a executable image
// without requiring Dialogic's sources to be supplied so long as each
// each source file so linked contains this exclusion.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

bool DialogicTrunk::joinHandler(TrunkEvent *event)
{
	TrunkEvent jevent;
	struct tm *dt, tbuf;
	struct stat ino;
	char buffer[32];
	const char *cp;
	const char *ext;
	char *fn;
	Audio::Info info;

	switch(event->id)
	{
	case TRUNK_STOP_STATE:
		data.join.count = 0;
		if(join && join->join)
			Part();
		TimerPort::endTimer();
		stopChannel(EV_ASYNC);
		_stopping_state = true;
		handler = &DialogicTrunk::stepHandler;
		return true;
	case TRUNK_TIMER_EXPIRED:
		if(data.join.count)
		{
			--data.join.count;
			if(data.join.trunk->getSequence() != data.join.seq)
				goto failed;
			goto retry;
		}
		if(join && join->join)
			Part();
		stopChannel(EV_ASYNC);
		if(!trunkSignal(TRUNK_SIGNAL_TIMEOUT))
			trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &DialogicTrunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
retry:
		TimerPort::endTimer();
		if(data.join.trunk)
		{
			switch(data.join.direction)
			{
			case JOIN_FULL:
				jevent.id = TRUNK_JOIN_TRUNKS;
				jevent.parm.trunk = this;
				data.join.trunk->postEvent(&jevent);
				if(!join && data.join.count)
				{
	                               	enterState("sync");
                                	status[tsid] = 's';
                                	setTimer(keythreads.getResetDelay());
                                	return true;
				}
				if(!join)
				{
failed:
					if(!trunkSignal(TRUNK_SIGNAL_ERROR))
						trunkSignal(TRUNK_SIGNAL_STEP);
					setSymbol(SYM_ERROR, "join-failed");
					handler = &DialogicTrunk::stepHandler;
					return true;
				}
				if(data.join.recfn)
				{
					ext = strrchr(data.join.recfn, '.');
					info.format = raw;
					info.encoding = mulawAudio;
					info.order = 0;
					info.annotation = (char *)data.join.annotation;
					info.rate = 8000;

					if(!ext)
					{
						ext = data.join.extension;
						strcat(data.join.recfn, ext);
					}

					if(!stricmp(ext, ".al"))
						info.encoding = alawAudio;
					else if(!stricmp(ext, ".au"))
					{
						info.format = sun;
						info.order = __BIG_ENDIAN;
					}
					else if(!stricmp(ext, ".wav"))
					{
						info.format = riff;
						info.order = __LITTLE_ENDIAN;
					}
					else if(!stricmp(ext, ".vox"))
						info.encoding = voxADPCM;
					URLAudio::close();
					create(data.join.recfn, &info);
					if(!isOpen())
					{
						slog(Slog::levelError) << data.join.recfn << ": cannot open" << endl;
						setSymbol(SYM_ERROR, "join-record-failed");
						trunkSignal(TRUNK_SIGNAL_ERROR);
						handler = &DialogicTrunk::stepHandler;
						return true;
					}
					dt = localtime_r(&ino.st_ctime, &tbuf);
					sprintf(buffer, "%04d%02d%02d,%02d%02d%02d",
						dt->tm_year + 1900, dt->tm_mon + 1, dt->tm_mday,
						dt->tm_hour, dt->tm_min, dt->tm_sec);
					setSymbol(SYM_CREATED, buffer);
					fn = getAnnotation();
					if(fn)
						setSymbol(SYM_ANNOTATION, fn);
					else
						setSymbol(SYM_ANNOTATION, "");
				}	
				data.join.count = 0;
				enterState("join");
				status[tsid] = 'j';
				return true;
			case JOIN_XMIT:
			case JOIN_RECV:
				data.join.count = 0;
               			if(Listen((DialogicTrunk *)data.join.trunk))
				{
					enterState("listen");
					status[tsid] = 'l';
				}
				else
				{
               				trunkSignal(TRUNK_SIGNAL_ERROR);
               				setSymbol(SYM_ERROR, "join-failed");
               				handler = &DialogicTrunk::stepHandler;
               				return true;
				}
				break;
			}
		}
		else
		{
			data.join.count = 0;
			Trunk::setDTMFDetect();
			enterState("wait");
			status[tsid] = 'w';
		}
		if(data.join.wakeup)
			TimerPort::setTimer(data.join.wakeup);
		return true;
	case TRUNK_PART_TRUNKS:
		TimerPort::endTimer();
		if(data.join.hangup)
		{
			if(!trunkSignal(TRUNK_SIGNAL_CANCEL))
				trunkSignal(TRUNK_SIGNAL_HANGUP);
		}
		else if(!trunkSignal(TRUNK_SIGNAL_CANCEL))
			Trunk::error("join-parted");
		handler = &DialogicTrunk::stepHandler;
		return true;
	case TRUNK_SIGNAL_JOIN:
		if(join)
			return false;

		if(event->parm.error)
			setSymbol(SYM_ERROR, event->parm.error);
		else
			event->parm.error = "";

		if(!trunkSignal(TRUNK_SIGNAL_EVENT))
			trunkSignal(TRUNK_SIGNAL_HANGUP);
		else
		{
			setSymbol(SYM_EVENTID, "join");
			setSymbol(SYM_EVENTMSG, event->parm.error);
		}
		handler = &DialogicTrunk::stepHandler;
		return true;
	case TRUNK_JOIN_TRUNKS:
		if(join)
			return false;

		if(data.join.waiting)
		{
			if(event->parm.trunk != data.join.waiting)
				return false;
		}

		data.join.count = 0;
		stopChannel(EV_ASYNC);
		TimerPort::endTimer();
		
		if(Join((DialogicTrunk *)event->parm.trunk))
		{
			cp = join->getSymbol(SYM_GID);
			setSymbol(SYM_JOINID, strchr(cp, '-'));
			return true;
		}

		trunkSignal(TRUNK_SIGNAL_ERROR);
		setSymbol(SYM_ERROR, "join-failed");
		handler = &DialogicTrunk::stepHandler;
		return false;
	}
	return false;
}

#ifdef CCXX_NAMESPACES
};
#endif

