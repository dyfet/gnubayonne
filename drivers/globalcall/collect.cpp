// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

bool DialogicTrunk::collectHandler(TrunkEvent *event)
{
	unsigned short mask;
	Symbol *sym = (Symbol *)data.collect.var;
	unsigned copy = 0;
	char evt[65];

	switch(event->id)
	{
	case TRUNK_ENTER_STATE:
		enterState("collect");
		if(digits >= data.collect.count)
		{
			trunkSignal(TRUNK_SIGNAL_STEP);
			handler = &DialogicTrunk::stepHandler;
			return true;
		}
		Trunk::flags.dsp = DSP_MODE_VOICE;
		setDTMFDetect(true);
		if(digits)
			TimerPort::setTimer(data.collect.timeout);
		else
			TimerPort::setTimer(data.collect.first);
		return true;
	case TRUNK_TIMER_EXPIRED:
		if(!trunkEvent("digits:expired"))
			if(!trunkSignal(TRUNK_SIGNAL_TIMEOUT))
				trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &DialogicTrunk::stepHandler;
		setDTMFDetect(false);
		return true;
	case TRUNK_DTMF_KEYUP:
		mask = (1 << event->parm.dtmf.digit);
		if(mask & data.collect.ignore)
			return true;
		if(!(mask & data.collect.term))
		{
			dtmf.bin.data[digits++] = digit[event->parm.dtmf.digit];
			dtmf.bin.data[digits] = 0;
			snprintf(evt, sizeof(evt), "digits:%s", dtmf.bin.data);
			if(trunkEvent(evt))
			{
				handler = &DialogicTrunk::stepHandler;
				return true;
			}
		}				
		if(digits >= data.collect.count || (mask & data.collect.term))
		{
                        if(sym)
                        {
                                copy = sym->flags.size;
                                if(copy > digits)
                                        copy = digits;
                                if(copy)
                                        strncpy(sym->data, dtmf.bin.data, copy);
				sym->flags.initial = false;
                                sym->data[copy] = 0;
                                if(sym->flags.commit)
                                        commit(sym);
				digits = 0;
				dtmf.bin.data[0] = 0;
                        }

			TimerPort::endTimer();
			trunkSignal(TRUNK_SIGNAL_STEP);
			handler = &DialogicTrunk::stepHandler;
		}
		else
			TimerPort::setTimer(data.collect.timeout);
		return true;
	}
	return false;
}

#ifdef CCXX_NAMESPACES
};
#endif

