// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne 
// with the Dialogic runtime libraries to produce a executable image
// without requiring Dialogic's sources to be supplied so long as each
// each source file so linked contains this exclusion.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

void DialogicTrunk::trunkStep(trunkstep_t step)
{
	TrunkEvent event;

	if(handler != &DialogicTrunk::stepHandler)
		return;

	event.id = TRUNK_MAKE_STEP;
	event.parm.step = step;
	stepHandler(&event);
	putEvent(NULL);
}

bool DialogicTrunk::stepHandler(TrunkEvent *event)
{
	Trunk *alt = ctx;
	switch(event->id)
	{
	case TRUNK_MAKE_STEP:
		if(idleHangup())
			return true;
		switch(event->parm.step)
		{
		case TRUNK_STEP_THREAD:
			handler = &DialogicTrunk::threadHandler;
			return true;
		case TRUNK_STEP_SOFTDIAL:
		case TRUNK_STEP_DIALXFER:
			handler = &DialogicTrunk::dialHandler;
			return true;
		case TRUNK_STEP_COLLECT:
			handler = &DialogicTrunk::collectHandler;
			return true;
		case TRUNK_STEP_SLEEP:
			handler = &DialogicTrunk::sleepHandler;
			return true;
		case TRUNK_STEP_HANGUP:
			handler = &DialogicTrunk::hangupHandler;
			return true;
		case TRUNK_STEP_TONE:
			handler = &DialogicTrunk::toneHandler;
			return true;
		case TRUNK_STEP_RECORD:
			handler = &DialogicTrunk::recordHandler;
			return true;
		case TRUNK_STEP_PLAYWAIT:
			handler = &DialogicTrunk::playwaitHandler;
			return true;
		case TRUNK_STEP_PLAY:
			handler = &DialogicTrunk::playHandler;
			return true;
		case TRUNK_STEP_ACCEPT:
			handler = &DialogicTrunk::acceptHandler;
			return true;
		case TRUNK_STEP_REJECT:
			handler = &DialogicTrunk::rejectHandler;
			return true;
		case TRUNK_STEP_ANSWER:
			handler = &DialogicTrunk::answerHandler;
			return true;
		case TRUNK_STEP_JOIN:
			handler = &DialogicTrunk::joinHandler;
			return true;
		case TRUNK_STEP_FLASH:
			handler = &DialogicTrunk::flashonHandler;
			return true;
#ifdef	XML_SCRIPTS
		case TRUNK_STEP_LOADER:
			handler = &DialogicTrunk::loadHandler;
			return true;
#endif
		}
		return false;
	case TRUNK_SERVICE_SUCCESS:
	case TRUNK_SERVICE_FAILURE:
		TimerPort::endTimer();
		TimerPort::setTimer(keythreads.getResetDelay());
		return true;
	case TRUNK_STOP_STATE:
		stopChannel(EV_ASYNC);
	case TRUNK_TIMER_EXPIRED:
		Trunk::flags.reset = false;
		stopServices();
	case TRUNK_ENTER_STATE:
		if(event->id == TRUNK_ENTER_STATE)
		{
			URLAudio::close();
			stopChannel(EV_ASYNC);
		}
		if(Trunk::flags.offhook)
		{
			if(Trunk::flags.trunk == TRUNK_MODE_INCOMING)
				status[tsid] = 'i';
			else
				status[tsid] = 'o';
		}
		else
			status[tsid] = 's';

                if(tgi.fd > -1 && digits)
                {
                        snprintf(buffer, sizeof(buffer), "%s\n", dtmf.bin.data);
                        writeShell(buffer);
                        digits = 0;
                }

                Trunk::setDTMFDetect();
		TimerPort::endTimer();
		if(Trunk::flags.reset || thread)
		{
			enterState("reset");
			if(thread)
				TimerPort::setTimer(thread->stop());
			else
				TimerPort::setTimer(keythreads.getResetDelay());
			return true;
		}

                if(tgi.fd > -1)
		{
                        enterState("shell");
			return true;
		}

		strcpy(numbers[5].sym.data, "step");
		debug->debugStep(this, getScript());
		monitor->monitorStep(this, getScript());
		if(alt != this)
			alt->enterMutex();
		scriptStep();
		if(alt != this)
			alt->leaveMutex();

		if(handler == &DialogicTrunk::stepHandler)
			TimerPort::setTimer(keythreads.getStepDelay());

		return true;
	}
	return false;
}

#ifdef CCXX_NAMESPACES
};
#endif
