// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne
// with the Dialogic runtime libraries to produce a executable image
// without requiring Dialogic's sources to be supplied so long as each
// each source file so linked contains this exclusion.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

bool DialogicTrunk::sleepHandler(TrunkEvent *event)
{
	timeout_t wakeup;
	char buffer[16];
	char *cp;
	char *value;
	char *sp;

	switch(event->id)
	{
        case TRUNK_SHELL_START:
                if(!tgi.pid)
                        return false;
                tgi.fd = event->parm.fd;
                handler = &DialogicTrunk::stepHandler;
                return true;
        case TRUNK_WAIT_SHELL:
                if(tgi.seq != event->parm.waitpid.seq)
                        return false;

                if(tgi.pid)
                        return false;

                tgi.pid = event->parm.waitpid.pid;
                return true;
	case TRUNK_EXIT_SHELL:
		if(tgi.seq != event->parm.exitpid.seq)
			return false;

		if(!tgi.pid)
			return true;

		tgi.pid = 0;
		TimerPort::endTimer();
		sprintf(buffer, "exit-%d", event->parm.exitpid.status);
		setSymbol(SYM_ERROR, buffer);
		if(event->parm.exitpid.status)
			trunkSignal(TRUNK_SIGNAL_ERROR);
		else
			trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &DialogicTrunk::stepHandler;
		return true;	
	case TRUNK_DTMF_KEYUP:
		if(tgi.pid)
		{
			::kill(tgi.pid, SIGINT);
			tgi.pid = 0;
		}
		return false;
	case TRUNK_RINGING_OFF:
		if(rings > 1)
			stopChannel(EV_ASYNC);
		if(!data.sleep.rings)
			return true;
		if(rings < data.sleep.rings)
			return true;
		TimerPort::endTimer();
	case TRUNK_TIMER_EXPIRED:
		if(--data.sleep.loops)
		{
			TimerPort::setTimer(data.sleep.wakeup);
			return true;
		}
		if(tgi.pid || data.sleep.save)
		{
			if(tgi.pid)
				::kill(tgi.pid, SIGALRM);
			tgi.pid = 0;
			if(!trunkSignal(TRUNK_SIGNAL_TIMEOUT))
				trunkSignal(TRUNK_SIGNAL_STEP);
		}
		else
			trunkSignal(TRUNK_SIGNAL_STEP);

		handler = &DialogicTrunk::stepHandler;
		return true;
	case TRUNK_CHILD_FAIL:
		setSymbol(SYM_ERROR, "start-failed");
		if(!trunkSignal(TRUNK_SIGNAL_FAIL))
			trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &DialogicTrunk::stepHandler;
		return true;
	case TRUNK_CHILD_START:
		cp = event->parm.trunk->getSymbol(SYM_GID);
		if(cp)
			cp = strchr(cp, '-');
		if(cp)
			setVariable(data.sleep.save, 16, cp);
		trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &DialogicTrunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		enterState("sleep");
		TimerPort::endTimer();
		Trunk::setDTMFDetect();
		wakeup = data.sleep.wakeup;
		if(data.sleep.rings)
			if(rings >= data.sleep.rings)
				wakeup = 0;

		if(!wakeup)
		{
			trunkSignal(TRUNK_SIGNAL_STEP);
			handler = &DialogicTrunk::stepHandler;
			return true;
		}

		TimerPort::setTimer(wakeup);
		return true;
	}
	return false;
}

bool DialogicTrunk::threadHandler(TrunkEvent *event)
{
        switch(event->id)
        {
        case TRUNK_STOP_STATE:
                stopServices();
                TimerPort::endTimer();
                handler = &DialogicTrunk::stepHandler;
                return true;
        case TRUNK_SERVICE_SUCCESS:
                Trunk::flags.reset = true;
                TimerPort::endTimer();
                trunkSignal(TRUNK_SIGNAL_STEP);
                handler = &DialogicTrunk::stepHandler;
                return true;
        case TRUNK_SERVICE_FAILURE:
                Trunk::flags.reset = true;
                TimerPort::endTimer();
                setSymbol(SYM_ERROR, "thread-error");
                trunkSignal(TRUNK_SIGNAL_ERROR);
                handler = &DialogicTrunk::stepHandler;
                return true;
        case TRUNK_TIMER_EXPIRED:
                stopServices();
                TimerPort::endTimer();
                setSymbol(SYM_ERROR, "thread-timeout");
                if(!trunkSignal(TRUNK_SIGNAL_TIMEOUT))
                        trunkSignal(TRUNK_SIGNAL_ERROR);
                handler = &DialogicTrunk::stepHandler;
                return true;
        case TRUNK_ENTER_STATE:
                enterState("thread");
                endTimer();
                Trunk::setDTMFDetect();
                TimerPort::setTimer(data.sleep.wakeup);
                thread->start();
                return true;

        }
        return false;
}

#ifdef CCXX_NAMESPACES
};
#endif

