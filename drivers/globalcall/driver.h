// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne 
// with the Dialogic runtime libraries to produce a executable image
// without requiring Dialogic's sources to be supplied so long as each
// source file so linked contains this exclusion.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#define	USE_OLD_CONSTS	1

//	some proposed conditional control flags:
//
//	DIALOGIC_MT 	- threadsafe api
//	DIALOGIC_GC	- build for globalcall support

#include <ivrconfig.h>
#include <server.h>
#include <cc++/audio.h>

#define DIALOGIC_MT

#include <gclib.h>
#include <anapi.h>
#include <gcisdn.h>
#include <srllib.h>
#include <devmapr4.h>
#include <dtilib.h>
#include <sctools.h>

#ifdef  CCXX_NAMESPACES
namespace ost {
#endif

extern "C" {
	int uio_read(int fd, char *buffer, unsigned len);
	int uio_write(int fd, char *buffer, unsigned len);
	long uio_seek(int fd, long offset, int mode);
};

#define MAX_DEVNAME 64

typedef enum
{
	E1ORT1,
	ISDN,
	ANALOG,
	SS7,
	IP
} interface_t;

typedef enum
{
	SPRINGWARE,
	DM3
} boardtype_t;

typedef	struct
{
	interface_t iface;
	LINEDEV linedev;
	char netdev[MAX_DEVNAME];
	char protocol[MAX_DEVNAME];
	char voicedev[MAX_DEVNAME];
	char devname[MAX_DEVNAME];
	int chdev;
	int tsdev;
	unsigned span;
}	devices_t;

// typedef struct
// {
//	char devicename[MAX_DEVNAME];
//	unsigned int devcount;
// }	subboards_t;

// typedef struct
// {
//	int boardtype;
//	unsigned int dtibdcount;
//	unsigned int voxbdcount;
//	unsigned int unknownbdcount;
//	int dticount;
//	int voxcount;
//	SRLDEVICEINFO *voxboards;
//	SRLDEVICEINFO *dtiboards;
//}	boards_t;

class DialogicTrunk;
class DialogicDriver;

extern DialogicDriver dialogicivr;

typedef	bool (DialogicTrunk::*trunkhandler_t)(TrunkEvent *event);

class DialogicISDN
{
private:
	friend class DialogicDriver;
	int isdev, isspan;
	interface_t interface;

	void spanEvent(TrunkEvent *evt)
		{return;};

	interface_t getInterface(void)
		{return interface;}

	DialogicISDN(int is, int span, interface_t iface);
	~DialogicISDN();
};

class DialogicTrunk : private TimerPort, private Trunk, private Thread, private URLAudio, private AudioService
{
private:
	friend class DialogicDriver;

	DialogicISDN *isdn;

	DialogicTrunk *join;
	interface_t interface;
	trunkhandler_t handler;
	unsigned frames;
	time_t lastring;
	bool routing;
	LINEDEV linedev;
	int chdev, tsdev, isdev, faxdev;
	int iocnt;
	CRN crn;
	WAITCALL_BLK wtcall;
	long tsinfo[3];
	SC_TSINFO voxts, intts, faxts;
	DV_DIGIT digbuf[33];
	DV_TPT digtpt[5];
	DX_IOTT iofile, iotone;
	DX_CAP dxcap;
	int evbuf[2];
	bool _stopping_state, _disconnecting, _blocked, _waiting, _accepted;
	int callstate;

	bool Join(DialogicTrunk *trunk);
	bool Listen(DialogicTrunk *trunk);
	void Part(void);
	void tsConnect(void);
	void tsRelease(void);

	bool postEvent(TrunkEvent *event);
	void postError(int dev, const char *msg = "");
	void postGCError(const char *msg = "");
	void putEvent(TrunkEvent *evt);
	void run(void);
	void exit(void);
	void trunkStep(trunkstep_t step);
	void getName(char *buffer);
	void putISDNError(const char *msg = "");

	void setDTMFDetect(bool flag);
	unsigned long getIdleTime(void);
	bool waitCall(void);
	bool acceptCall(void);
	bool answerCall(void);
	bool hangupCall(void);
	void releaseCall(void);
	void getCallerId(void);
	void resetChannel(void);

	void initSyms(void);
	void stopChannel(int mode);
	bool playAudio(void);
	bool recordAudio(void);
	bool playTone(phTone *tone);
	bool playTone(unsigned f1, unsigned f2, int a1, int a2, timeout_t dur);
	bool makeCall(char digit[16], int timeout);
	char *getContinuation(void);

	bool releaseHandler(TrunkEvent *event);
	bool resetHandler(TrunkEvent *event);
	bool acceptHandler(TrunkEvent *event);
	bool rejectHandler(TrunkEvent *event);
	bool answerHandler(TrunkEvent *event);
	bool stepHandler(TrunkEvent *event);
	bool idleHandler(TrunkEvent *event);
	bool loadHandler(TrunkEvent *event);
	bool dialHandler(TrunkEvent *event);
	bool sleepHandler(TrunkEvent *event);
	bool threadHandler(TrunkEvent *event);
	bool collectHandler(TrunkEvent *event);
	bool ringHandler(TrunkEvent *event);
	bool hangupHandler(TrunkEvent *event);
	bool playHandler(TrunkEvent *event);
	bool toneHandler(TrunkEvent *event);
	bool playwaitHandler(TrunkEvent *event);
	bool recordHandler(TrunkEvent *event);
	bool joinHandler(TrunkEvent *event);
	bool flashonHandler(TrunkEvent *event);
	bool flashoffHandler(TrunkEvent *event);
	bool busyHandler(TrunkEvent *event);

	bool scrJoin(void);
	bool scrWait(void);

	void endRecord(void);
	void endPlay(void);

	DialogicTrunk(int id, devices_t *devif);
	~DialogicTrunk();
	
	friend int uio_read(int fd, char *buffer, unsigned len);
	friend int uio_write(int fd, char *buffer, unsigned len);
	friend long uio_seek(int fd, long offset, int mode);	
public:
	Driver *getDriver(void)
		{return (Driver*)&dialogicivr;};
};

class DialogicConfig : public Keydata
{
private:
	static class Span
	{
	public:
		const char *gcname;
		const char *protocol;
		const char *span;
		unsigned ports;
	}	span[];

public:
	DialogicConfig();

	bool isAnalog(int card = 0);
	bool getRouting();
	int getNumberPlan();
	int getNumberType();
	int getL1Protocol();
	int getBoardType();

	inline const char *getDebounce(void)
		{return getLast("debounce");};

	inline const char *getTalkback(void)
		{return getLast("talkback");};

	inline int getInterval(void)
		{return atoi(getLast("interval"));};

	inline unsigned getSpanCount(void);

	inline unsigned getDelay(void)
		{return atoi(getLast("delay"));};

	const char *getSpanProtocol(void);
	unsigned getSpanPorts(void);
};

class DialogicDriver : public Driver, public DialogicConfig, public Thread
{
private:
	DialogicTrunk **ports;
	DialogicTrunk **maps;
	DialogicISDN **ismaps;
	DialogicISDN **isdn, **spanmaps;
	devices_t *devmap;
	int *voicemaps;
	int mode;
	int port_count, port_used, isdn_count, level;
	volatile bool running;
	DX_UIO uio;
	DV_TPT tpt[2], dm3tpt[1];

	void run(void);

	bool autoDetect(void);

	bool spanEvent(unsigned span, TrunkEvent *event);

	friend int uio_read(int fd, char *buffer, unsigned len);
	friend int uio_write(int fd, char *buffer, unsigned len);
	friend long uio_seek(int fd, long offset, int mode);	
	
public:
	DialogicDriver();
	~DialogicDriver();

        unsigned getCaps(void)
                {return capDaemon | capPSTN | capJoin | capSpans;};

	int start(void);
	void stop(void);

	unsigned getTrunkCount(void)
		{return port_count;};

	DialogicISDN *getSpan(int span);
	DialogicISDN *getISDN(int fd);
	Trunk *getTrunkPort(int id);
	aaScript *getScript(void);
	DV_TPT *getPlayTPT(void)
		{if(getBoardType() == DM3) return NULL;
		 else return tpt;};
	DV_TPT *getRecordTPT(void)
		{if(getBoardType() == DM3) return dm3tpt;
		 else return tpt;};

	char *getName(void)
		{return "Dialogic";};
};

#ifdef  CCXX_NAMESPACES
};
#endif

