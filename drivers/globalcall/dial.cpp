// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne 
// with the Dialogic runtime libraries to produce a executable image
// without requiring Dialogic's sources to be supplied so long as each
// source file so linked contains this exclusion.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

bool DialogicTrunk::dialHandler(TrunkEvent *event)
{
	char dialstr[40];
	const char *dp;

	switch(event->id)
	{
	case TRUNK_CALL_RELEASE:
		setSymbol(SYM_TONE, "fail");
		if(!trunkEvent("dial:failed"))
			if(!trunkSignal(TRUNK_SIGNAL_HANGUP))
				if(!trunkSignal(TRUNK_SIGNAL_CANCEL))
					if(!trunkSignal(TRUNK_SIGNAL_TONE))
						trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &DialogicTrunk::stepHandler;
		return true;
	case TRUNK_TIMER_EXPIRED:
		stopChannel(EV_ASYNC);
		hangupCall();
		if(trunkEvent("dial:expired"))
		{
		}
		else if(data.dialxfer.exit)
			trunkSignal(TRUNK_SIGNAL_HANGUP);
		else if(!trunkSignal(TRUNK_SIGNAL_TIMEOUT))
			if(!trunkSignal(TRUNK_SIGNAL_CANCEL))
				trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &DialogicTrunk::stepHandler;
		return true;
	case TRUNK_CPA_BUSYTONE:
		setSymbol(SYM_TONE, "busy");
		stopChannel(EV_ASYNC);
		releaseCall();
		if(!trunkEvent("dial:busy"))
			if(!trunkEvent("tone:busy"))
				if(!trunkSignal(TRUNK_SIGNAL_BUSY))
					if(!trunkSignal(TRUNK_SIGNAL_TONE))
					{
						if(data.dialxfer.exit)
							trunkSignal(TRUNK_SIGNAL_HANGUP);
						else
							trunkSignal(TRUNK_SIGNAL_STEP);
					}
		handler = &DialogicTrunk::stepHandler;
		return true;		
	case TRUNK_CPA_NOANSWER:
		setSymbol(SYM_TONE, "ring");
		releaseCall();
		if(trunkEvent("dial:noanswer"))
		{
			handler = &DialogicTrunk::stepHandler;
			return true;
		}
		if(trunkSignal(TRUNK_SIGNAL_NOANSWER))
		{
			handler = &DialogicTrunk::stepHandler;
			return true;
		}
		trunkSignal(TRUNK_SIGNAL_STEP);
		event->id = TRUNK_TIMER_EXPIRED;
		return false;
	case TRUNK_CPA_NORINGBACK:
		setSymbol(SYM_TONE, "silence");
		stopChannel(EV_ASYNC);
		releaseCall();
		if(trunkEvent("dial:noringback"))
		{
			handler = &DialogicTrunk::stepHandler;
			return true;
		}
		if(trunkSignal(TRUNK_SIGNAL_SILENCE))
		{
			handler = &DialogicTrunk::stepHandler;
			return true;
		}
		trunkSignal(TRUNK_SIGNAL_STEP);
		event->id = TRUNK_TIMER_EXPIRED;
		return false;
	case TRUNK_CPA_FAILURE:
	case TRUNK_CPA_NODIALTONE:
		setSymbol(SYM_TONE, "fail");
		if(_stopping_state)
		{
			handler = &DialogicTrunk::stepHandler;
			return true;
		}
		releaseCall();
		stopChannel(EV_ASYNC);
		if(trunkEvent("dial:fail"))
		{
			handler = &DialogicTrunk::stepHandler;
			return true;
		}
		if(trunkSignal(TRUNK_SIGNAL_CANCEL))
		{
			handler = &DialogicTrunk::stepHandler;
			return true;
		}
		if(trunkSignal(TRUNK_SIGNAL_TONE))
		{
			handler = &DialogicTrunk::stepHandler;
			return true;
		}
		trunkSignal(TRUNK_SIGNAL_STEP);
		event->id = TRUNK_TIMER_EXPIRED;
		return false;
	case TRUNK_CALL_CONNECT:
		if(dxcap.ca_intflg == DX_PAMDOPTEN)
			return true;
		stopChannel(EV_ASYNC);
		return true;
	case TRUNK_CPA_STOPPED:
	case TRUNK_CPA_CONNECT:
		if(_stopping_state)
		{
			handler = &DialogicTrunk::stepHandler;
			return true;
		}
		if(trunkEvent("dial:answer"))
		{
		}
		else if(data.dialxfer.exit)
			trunkSignal(TRUNK_SIGNAL_HANGUP);
		else
			trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &DialogicTrunk::stepHandler;
		return true;
	case TRUNK_STOP_STATE:
		_stopping_state = true;
		stopChannel(EV_ASYNC);
		TimerPort::endTimer();
		return true;
	case TRUNK_ENTER_STATE:
		enterState("dial");
		_stopping_state = false;
		TimerPort::endTimer();
		setSymbol(SYM_TONE, "none");
		Trunk::flags.offhook = true;

		if(status[tsid] == 'd')
			return true;
		status[tsid] = 'd';
		if(strlen(data.dialxfer.digit) < 1)
		{
			setSymbol(SYM_ERROR, "dial-no-number");
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &DialogicTrunk::stepHandler;
			return true;
		}
		dp = group->getLast("dialmode");
		if(!dp)
			dp = "T";

		switch(*dp)
		{
		case 't':
		case 'T':
		case 'd':
		case 'D':
			dp = "T";
			break;
		case 'm':
		case 'M':
			dp = "M";
			break;
		case 'p':
		case 'P':
			dp = "P";
			break;
		default:
			dp = "";
		}

		strcpy(dialstr, dp);
		strcat(dialstr, data.dialxfer.digit);

                if(data.dialxfer.timeout)
                {
                        dx_clrcap(&dxcap);

			// Enable only basic CPA, not PVD & PAMD, by default.
			// PVD & PAMD introduce a silent delay in dialing after
			// answer.
			dxcap.ca_intflg = DX_OPTEN;

                        // rings before concluding no answer

                        dp = group->getLast("cprings");
                        if(dp)
                                dxcap.ca_nbrdna = atoi(dp);

                        dp = group->getLast("noanswer");
                        if(dp)
                                dxcap.ca_noanswer = getMSTimeout(dp) / 10;

                        // silence on ringback

                        dp = group->getLast("nosignal");
                        if(dp)
                                dxcap.ca_cnosig = getMSTimeout(dp) / 10;

                        dp = group->getLast("nosilence");
                        if(dp)
                                dxcap.ca_cnosil = getMSTimeout(dp) / 10;

			dp = group->getLast("intercept");
			if(dp)
			{
				if(!strnicmp(dp, "pam", 3))
					dxcap.ca_intflg = DX_PAMDOPTEN;
			}
                }
		if(!makeCall(data.dialxfer.digit, data.dialxfer.timeout))
		{
                       	setSymbol(SYM_ERROR, "dial-failed");
                       	trunkSignal(TRUNK_SIGNAL_ERROR);
                       	handler = &DialogicTrunk::stepHandler;
			return true;
		}

		if(dx_dial(chdev, "", &dxcap, EV_ASYNC | DX_CALLP) < 0)
			postError(chdev, "dx_dial");

		return true;

	case TRUNK_LINE_WINK:
	        return true;
	}

	return false;
}

#ifdef CCXX_NAMESPACES
};
#endif

