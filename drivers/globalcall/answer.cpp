// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne 
// with the Dialogic runtime libraries to produce a executable image
// without requiring Dialogic's sources to be supplied so long as each
// source file so linked contains this exclusion.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

bool DialogicTrunk::answerHandler(TrunkEvent *event)
{
	switch(event->id)
	{
	case TRUNK_LINE_WINK:
		if(!Trunk::flags.offhook)
			return true;
	case TRUNK_CPA_DIALTONE:
		setSymbol(SYM_TONE, "dial");
	case TRUNK_STOP_DISCONNECT:
	case TRUNK_CALL_RESTART:
	case TRUNK_TIMER_EXPIRED:
		TimerPort::endTimer();
		Trunk::flags.dsp = DSP_MODE_INACTIVE;
		if(!trunkEvent("answer:failed"))
			trunkSignal(TRUNK_SIGNAL_HANGUP);
		handler = &DialogicTrunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		setSymbol(SYM_TONE, "none");
		if(Trunk::flags.offhook)
		{
			trunkSignal(TRUNK_SIGNAL_STEP);
			handler = &DialogicTrunk::stepHandler;
			return true;
		}
		status[tsid] = 'a';
		enterState("answer");
	case TRUNK_RINGING_OFF:
		if(rings > 1)
			Trunk::flags.dsp = DSP_MODE_INACTIVE;

		if(data.answer.rings > rings)
		{
			TimerPort::setTimer(data.answer.timeout);
			return true;
		}
		TimerPort::endTimer();
		Trunk::flags.dsp = DSP_MODE_INACTIVE;

		if(!answerCall())
		{
                	TimerPort::endTimer();
                	Trunk::flags.dsp = DSP_MODE_INACTIVE;
                	trunkSignal(TRUNK_SIGNAL_HANGUP);
                	handler = &DialogicTrunk::stepHandler;
		}
		return true;
	case TRUNK_OFF_HOOK:
		trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &DialogicTrunk::stepHandler;
		status[tsid] = 'i';
		return true;
	}
	return false;
}

#ifdef CCXX_NAMESPACES
};
#endif

