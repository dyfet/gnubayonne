// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne
// with the Dialogic runtime libraries to produce a executable image
// without requiring Dialogic's sources to be supplied so long as each
// each source file so linked contains this exclusion.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

bool DialogicTrunk::recordHandler(TrunkEvent *event)
{
	struct tm *dt, tbuf;
	struct stat ino;
	char buffer[32];
	const char *ext;
	const char *fmt = data.record.encoding;
	char *fn;
	Audio::Info info;
	unsigned mask;
	slog(Slog::levelDebug) << event->id << endl;	
	switch(event->id)
	{
	case TRUNK_STOP_STATE:
		TimerPort::endTimer();
		_stopping_state = true;
		stopChannel(EV_ASYNC);
		return true;
	case TRUNK_DTMF_KEYUP:
		mask = (1 << event->parm.dtmf.digit);
		if(!(mask & data.record.term))
			return false;
		TimerPort::endTimer();
	case TRUNK_TIMER_EXPIRED:
		stopChannel(EV_ASYNC);
		return true;
	case TRUNK_AUDIO_IDLE:
		TimerPort::endTimer();
		handler = &DialogicTrunk::stepHandler;
		if(_stopping_state == false)
		{
			trunkSignal(TRUNK_SIGNAL_STEP);
		}
		endRecord();
		return true;
	case TRUNK_ENTER_STATE:
		enterState("record");
		status[tsid] = 'r';
		_stopping_state = false;
		if(!Trunk::flags.offhook && interface == ANALOG)
		{
			//setHook(true);
			Trunk::flags.offhook = true;
			return true;
		}
	case TRUNK_OFF_HOOK:
		TimerPort::setTimer(data.record.timeout);
		if(data.record.term)
			setDTMFDetect(true);
		else
			Trunk::setDTMFDetect();
		getName(buffer);

		ext = strrchr(data.record.name, '/');
		if(!ext)
			ext = data.record.name;

		if(!fmt)
			fmt = "raw";

		ext = strrchr(ext, '.');

		info.format = raw;
		info.encoding = mulawAudio;
		info.order = 0;
		info.annotation = (char *)data.record.annotation;
		info.rate = 8000;

		if(!ext)
		{
			ext = data.record.extension;
			strcat(data.record.name, ext);
		}

		if(!stricmp(ext, ".al"))
			fmt = "alaw";
		else if(!stricmp(ext, ".ul"))
			fmt = "mulaw";
		else if(!stricmp(ext, ".vox"))
			fmt = "vox";
		else if(!stricmp(ext, ".au") || !stricmp(ext, ".snd"))
		{
			info.format = sun;
			info.order = __BIG_ENDIAN;
		}
		else if(!stricmp(ext, ".wav"))
		{
			info.format = riff;
			info.order = __LITTLE_ENDIAN;
		}

		if(!stricmp(fmt, "alaw"))
                        info.encoding = alawAudio;
                else if(!stricmp(fmt, "vox") || !stricmp(fmt, "adpcm"))
                        info.encoding = voxADPCM;
                else if(!stricmp(fmt, "ulaw") || !stricmp(fmt, "mulaw"))
                        info.encoding = mulawAudio;
                else if(!stricmp(fmt, "g.711") || !stricmp(fmt, "g711"))
                {
                        if(info.encoding != alawAudio)
                        info.encoding = mulawAudio;   
                }
                else if(stricmp(fmt, "raw"))
                {
                        slog(Slog::levelError) << buffer
                                << ": unsupported encoding requested" << endl;
                        setSymbol(SYM_OFFSET, "0");
                        setSymbol(SYM_RECORDED, "0");
                        setSymbol(SYM_ERROR, "record-failed");
                        trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &DialogicTrunk::stepHandler;
			return true;
		}

		URLAudio::close();
                if(data.record.offset != (unsigned long)-1)
                {
                        open(data.record.name);
                        setPosition(data.record.offset);
                }

		else if(data.record.append)
			open(data.record.name);
		else
		{
			remove(data.record.name);
			create(data.record.name, &info);
		}

                setSymbol(SYM_RECORDED, "0");

		if(!isOpen())
		{
			slog(Slog::levelError) << data.record.name << ": cannot open" << endl;
			setSymbol(SYM_OFFSET, "0");
			setSymbol(SYM_ERROR, "record-failed");
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &DialogicTrunk::stepHandler;
			return true;
		}
		if(data.record.append)
			setPosition();

		dt = localtime_r(&ino.st_ctime, &tbuf);
		sprintf(buffer, "%04d%02d%02d,%02d%02d%02d",
			dt->tm_year + 1900, dt->tm_mon + 1, dt->tm_mday,
			dt->tm_hour, dt->tm_min, dt->tm_sec);
		setSymbol(SYM_CREATED, buffer);
		fn = getAnnotation();
		if(fn)
			setSymbol(SYM_ANNOTATION, fn);
		else
			setSymbol(SYM_ANNOTATION, "");

                if(data.record.info)
                {
                        sprintf(buffer, "%ld", getPosition());
                        setSymbol(SYM_OFFSET, buffer);
                        return true;
                }

		if(!recordAudio())
		{
			setSymbol(SYM_ERROR, "record-failed");
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &DialogicTrunk::stepHandler;
		}
		return true;
	}
	return false;
}

#ifdef CCXX_NAMESPACES
};
#endif

