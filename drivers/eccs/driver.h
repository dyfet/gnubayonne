// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <ivrconfig.h>
#include <server.h>
#include <cc++/audio.h>

class ECCSTrunk;

class ECCSConfig : public Keydata
{
public:
	ECCSConfig();
};

class ECCSDriver : public Driver, public ECCSConfig
{
private:
	ECCSTrunk **ports;
	int port_count;

public:
	ECCSDriver();
	~ECCSDriver();

	int Start(void);
	void Stop(void);

	int getTrunkCount(void)
		{return port_count;};

	Trunk *getTrunkPort(int id);
	aaScript *getScript(void);
};

extern ECCSDriver eccsivr;
