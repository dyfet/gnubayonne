// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

ECCSConfig::ECCSConfig() :
Keydata("/bayonne/eccs")
{
	static KEYDEF defkeys[] = {
	{NULL, NULL}};

	if(chkHosting(NULL))
		Load("/hosting/eccs");

	Load(defkeys);
}

ECCSDriver::ECCSDriver() :
Driver()
{
	ports = NULL;
	groups = NULL;
	port_count = 0;

	if(ports)
		memset(ports, 0, sizeof(ECCSTrunk *) * port_count);

	if(groups)
		memset(groups, 0, sizeof(TrunkGroup *) * port_count);

	slog(SLOG_INFO) << "ECCS free dialogic driver loaded; capacity=" << port_count << endl;
}

ECCSDriver::~ECCSDriver()
{
	Stop();
	if(ports)
		delete ports;

	if(groups)
		delete groups;
}

int ECCSDriver::Start(void)
{
	int count = 0;

	if(active)
	{
		slog(SLOG_ERROR) << "driver already started" << endl;
		return 0;
	}

	slog(SLOG_INFO) << "driver starting..." << endl;

	active = true;
	return count;
}

void ECCSDriver::Stop(void)
{
	if(!active)
		return;

	if(ports)
		memset(ports, 0, sizeof(ECCSTrunk *) * port_count);

	active = false;
	slog(SLOG_INFO) << "driver stopping..." << endl;
}

Trunk *ECCSDriver::getTrunkPort(int id)
{
	if(id < 0 || id >= port_count)
		return NULL;

	if(!ports)
		return NULL;

	return (Trunk *)ports[id];
}

ECCSDriver eccsivr;
