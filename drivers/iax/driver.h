// Copyright (C) 2000-2002 Open Source Telecom Corporation.
// 
// Author: Mark Lipscombe
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <ivrconfig.h>
#include <server.h>
#include <cc++/audio.h>

extern "C" {
#include <iax-client.h>
#include <gsm.h>
}

#ifdef	CCXX_NAMESPACES
namespace ost {
#endif

class IAXTrunk;
class IAXDriver;
extern IAXDriver iaxivr;

typedef	bool (IAXTrunk::*trunkhandler_t)(TrunkEvent *event);

class IAXTimer : private Thread, private Mutex, public TimerPort
{
public:
	IAXTimer(IAXTrunk *parent);
	~IAXTimer();

	void run();

private:
	friend class IAXTrunk;
	int evbuf[2];
	IAXTrunk *trunk;
};

class IAXTrunk : private TimerPort,
	private Trunk,
	public AudioService,
	public URLAudio
{
public:
	IAXTrunk(unsigned ref, unsigned tid);
	~IAXTrunk();

private:
	friend class IAXDriver;
	//friend class IAXEndPoint;
	//friend class IAXAudioChannel;
	//friend class IAXConnection;

	//IAXEndPoint endpoint;
	//IAXConnection *conn;
	//PString callToken;

	IAXTrunk *join;

	trunkhandler_t handler;
	time_t lastring;
	int ts;

	bool _stopping_state;
	bool t_active;
	unsigned frames;
	bool isOutgoing;

	void initSyms(void);
	bool postEvent(TrunkEvent *evt);
	void putEvent(TrunkEvent *evt);
	void timerTick(void);
	void exit(void);
	void getName(char *buffer);
	unsigned long getIdleTime(void);
	void trunkStep(trunkstep_t) {return;};

	bool scrJoin(void);
	bool scrWait(void);

	//void run(void);

	// Audio controls
	bool playAudio(char *fn);
	char *getContinuation(void);
	void endRecord();
	void endPlay();
	bool Join(IAXTrunk *trk);
	bool Part();

	// Call control
	//void attachConnection(IAXConnection *connection);
	//IAXConnection *attachConnection(IAXEndPoint & ep, unsigned ref);
	bool answerCall(void);
	bool hangupCall(void);
	bool makeCall(char *digit, char *clid);

	// State handlers
	/*bool idleHandler(TrunkEvent *event);
	bool stepHandler(TrunkEvent *event);
	bool ringHandler(TrunkEvent *event);
	bool answerHandler(TrunkEvent *event);
	bool playHandler(TrunkEvent *event);
	bool recordHandler(TrunkEvent *event);
	bool sleepHandler(TrunkEvent *event);
	bool threadHandler(TrunkEvent *event);
	bool hangupHandler(TrunkEvent *event);
	bool collectHandler(TrunkEvent *event);
	bool loadHandler(TrunkEvent *event);
	bool joinHandler(TrunkEvent *event);
	bool dialHandler(TrunkEvent *event);
	bool toneHandler(TrunkEvent *event);
	bool listenHandler(TrunkEvent *event);*/
};

class IAXConfig : public Keydata
{
public:
	IAXConfig();

	tpport_t getFirstPort(void)
		{return atoi(getLast("first"));};

	unsigned getRTPCount(void)
		{return atoi(getLast("count"));};

	unsigned getRTPInc(void)
		{return atoi(getLast("inc"));};

	unsigned getPort(void)
		{return atoi(getLast("port"));};

	InetAddress getBindAddress(void);

	unsigned getSignalPort(void)
		{return atoi(getLast("port"));};
};

class IAXDriver : public Driver, public IAXConfig, public Thread
{
protected:
	//IAXEndPoint * endpoint;

private:
	//friend class IAXEndPoint;
	//friend class IAXConnection;
	friend class IAXTrunk;
	IAXTrunk **ports;
	//IAXTrunk **trunks;
	int port_count;

	int registration(void);

	struct iax_session *registry;
	struct timeval regtime;

	int netfd;

	struct iaxpeer_t {
		int time;
		gsm gsmin;
		gsm gsmout;

		struct iax_session *session;
		struct iaxpeer_t *next;
	};

	static struct iaxpeer_t *iaxpeers;
public:
	IAXDriver();
	~IAXDriver();

	int start(void);
	void stop(void);
	void run(void);

	unsigned getTrunkCount(void)
		{return port_count;};

	Trunk *getTrunkPort(int id);
	Trunk *getOutboundTrunk(int id);
	aaScript *getScript(void);

	unsigned getCaps(void)
		{return capDaemon | capIP | capJoin | capListen;};

	char *getName(void)
		{return "IAX";};
};

#ifdef	CCXX_NAMESPACES
};
#endif
