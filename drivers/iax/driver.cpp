// Copyright (C) 2000-2002 Open Source Telecom Corporation.
// 
// Author: Jeremy J. McNamara 
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

struct IAXDriver::iaxpeer_t *IAXDriver::iaxpeers = NULL;

IAXConfig::IAXConfig() :
Keydata("/bayonne/h323")
{
	static Keydata::Define defkeys[] = {
	{"first", "3128"},
	{"count", "32"},
	{"inc", "4"},
	{"interface", "*"},
	{"port","1720"},
	{"usegk","0"},
	{"gatekeeper",""},
	{"gkid",""},
	{"username","Bayonne"},
	{"uimode","rfc2833"},
	{"peer",""},
	{"secret",""},
	{"server",""},
	{"context",""},
	{"language",""},
	{"refresh",""},
	{NULL, NULL}};

	if(isFHS())
		load("/drivers/iax");

	load(defkeys);
}

InetAddress IAXConfig::getBindAddress(void)
{
	const char *cp = getLast("bind");
	if(!cp)
		cp = getLast("address");
	if(!cp)
		cp = getLast("interface");

	return InetAddress(cp);
}

IAXDriver::IAXDriver() :
Driver()
{
	port_count = iaxivr.getRTPCount();

	ports = NULL;
	groups = NULL;
	
	//endpoint = new IAXEndPoint;

	ports = new IAXTrunk *[port_count];
	groups = new TrunkGroup *[port_count];

	if(ports)
		memset(ports, 0, sizeof(IAXTrunk *) * port_count);

	if(groups)
		memset(groups, 0, sizeof(TrunkGroup *) * port_count);

	slog(Slog::levelInfo) << "IAX trunk driver loaded; capacity=" << port_count << endl;
}

IAXDriver::~IAXDriver()
{
	stop();
	if(ports)
		delete[] ports;

	if(groups)
		delete[] groups;
}

int IAXDriver::registration(void)
{
	char *peer = (char *)getLast("peer");
	char *server = (char *)getLast("server");
	char *secret = (char *)getLast("secret");
	char *context = (char *)getLast("context");
	char *language = (char *)getLast("language");
	char *ref = (char *)getLast("refresh");
	int refresh, rc;

	if(ref)
		refresh = atoi(ref);

	if(peer && strlen(peer) > 0)
	{
		registry = iax_session_new();
		rc = iax_register(registry, server, peer, secret, refresh);
		if(rc)
		{
			slog(Slog::levelError) << "IAX registration failed: " << iax_errstr << endl;
			return -1;
		}
		//iax_regtimeout(5 * refresh / 6);
	}
	else
	{
		//iax_regtimeout(0);
		refresh = 60;
	}
	return 0;
}

int IAXDriver::start(void)
{
	int count = iaxivr.getRTPCount();
	int port;

	if(active)
	{
		slog(Slog::levelError) << "IAX driver already started" << endl;
		return 0;
	}

	slog(Slog::levelInfo) << "IAX driver starting..." << endl;

	// init here
	port = iax_init(4329);
	if(port < 0)
	{
		slog(Slog::levelDebug) << "Failed to start IAX" << endl;
		throw((Driver *)this);
	}
	iax_set_formats(AST_FORMAT_GSM);
	netfd = iax_get_fd();
	registration();

	slog(Slog::levelDebug) << "IAX initialised and listening on port " << port << endl;
	active = true;

	Thread::start();
	return count;
}

void IAXDriver::stop(void)
{
	if(!active)
		return;

	if(ports)
		memset(ports, 0, sizeof(IAXTrunk *) * port_count);

	active = false;
	slog(Slog::levelInfo) << "IAX driver stopping..." << endl;
}

void IAXDriver::run(void)
{
	int i, c;
	TrunkEvent event;
	fd_set readfd;
	struct timeval timer;
	struct timeval *timerptr = NULL;
	int sessions = 0;
	struct iax_event *e = 0;
	struct iaxpeer_t *iaxpeer;
	int ms2;

	if(!active)
		return;

	setCancel(cancelImmediate);

	timer.tv_sec = 0;
	timer.tv_usec = 0;

	for(;;)
	{
		do {
			ms2 = iax_time_to_next_event();
			if(!ms2)
			{
				while((e = iax_get_event(0))) {
					slog(Slog::levelDebug) << "event" << endl;
					iax_event_free(e);
				}
			}
		} while (!ms2);
		FD_ZERO(&readfd);
		FD_SET(netfd, &readfd);

		c = select(netfd+1, &readfd, 0, 0, &timer);

		if(c == EINTR || !FD_ISSET(netfd, &readfd))
			continue;

		while(e = iax_get_event(0))
		{
			slog(Slog::levelDebug) << "Event recieved" << endl;
			//peer = find_peer(e->session);
			//if(peer)
			//	handle_event(e, peer);
			if(e->session == registry)
			{
				slog(Slog::levelDebug) << "IAX registration complete: " << endl;
				//	(e->event.regreply.status == IAX_REG_SUCCESS) ? "Success" : "Fail" <<
				//	" " << e->event.regreply.status);
				registry = NULL;
				continue;
			}
			switch(e->etype)
			{
			case IAX_EVENT_CONNECT:
				slog(Slog::levelDebug) << "Connect" << endl;
				iaxpeer = (struct iaxpeer_t *)malloc(sizeof(struct iaxpeer_t));
				if(!iaxpeer)
					return;
				iaxpeer->time = time(0);
				iaxpeer->session = e->session;
				iaxpeer->gsmin = 0;
				iaxpeer->gsmout = 0;
				iaxpeer->next = iaxpeers;
				iaxpeers = iaxpeer;

				iax_accept(iaxpeer->session);
				iax_ring_announce(iaxpeer->session);
				break;
			case IAX_EVENT_ACCEPT:
				slog(Slog::levelDebug) << "Accept" << endl;
				break;
			case IAX_EVENT_HANGUP:
				slog(Slog::levelDebug) << "Hangup" << endl;
				break;
			case IAX_EVENT_REJECT:
				slog(Slog::levelDebug) << "Reject" << endl;
				break;
			case IAX_EVENT_VOICE:
				slog(Slog::levelDebug) << "Voice" << endl;
				break;
			case IAX_EVENT_DTMF:
				slog(Slog::levelDebug) << "DTMF" << endl;
				break;
			case IAX_EVENT_TIMEOUT:
				slog(Slog::levelDebug) << "Timeout" << endl;
				break;
			case IAX_EVENT_LAGRQ:
				slog(Slog::levelDebug) << "LagRQ" << endl;
				break;
			case IAX_EVENT_LAGRP:
				slog(Slog::levelDebug) << "LagRP" << endl;
				break;
			case IAX_EVENT_RINGA:
				slog(Slog::levelDebug) << "Ring Announce" << endl;
				break;
			case IAX_EVENT_PING:
			case IAX_EVENT_PONG:
				slog(Slog::levelDebug) << "Ping/Pong" << endl;
				break;
			case IAX_EVENT_BUSY:
				slog(Slog::levelDebug) << "Busy" << endl;
				break;
			case IAX_EVENT_ANSWER:
				slog(Slog::levelDebug) << "Answer" << endl;
				break;
			case IAX_EVENT_IMAGE:
				slog(Slog::levelDebug) << "Image" << endl;
				break;
			case IAX_EVENT_AUTHRQ:
				slog(Slog::levelDebug) << "Authentication Request" << endl;
				break;
			case IAX_EVENT_AUTHRP:
				slog(Slog::levelDebug) << "Authentication Reply" << endl;
				break;
			case IAX_EVENT_REGREQ:
				slog(Slog::levelDebug) << "Registration Request" << endl;
				break;
			//case IAX_EVENT_REGACK:
			//	slog(Slog::levelDebug) << "Registration Ack" << endl;
			//	break;
			case IAX_EVENT_URL:
				slog(Slog::levelDebug) << "URL" << endl;
				break;
			case IAX_EVENT_LDCOMPLETE:
				slog(Slog::levelDebug) << "URL Load Complete" << endl;
				break;
			case IAX_EVENT_TRANSFER:
				slog(Slog::levelDebug) << "Transfer" << endl;
				break;
			case IAX_EVENT_DPREQ:
				slog(Slog::levelDebug) << "Dialplan Request" << endl;
				break;
			case IAX_EVENT_DPREP:
				slog(Slog::levelDebug) << "Dialplan Reply" << endl;
				break;
			case IAX_EVENT_DIAL:
				slog(Slog::levelDebug) << "Dial" << endl;
				break;
			case IAX_EVENT_QUELCH:
			case IAX_EVENT_UNQUELCH:
				slog(Slog::levelDebug) << "Audio Quelch/Unquelch" << endl;
				break;
			case IAX_EVENT_UNLINK:
				slog(Slog::levelDebug) << "Unlink" << endl;
				break;
			case IAX_EVENT_LINKREJECT:
				slog(Slog::levelDebug) << "Link Reject" << endl;
				break;
			case IAX_EVENT_TEXT:
				slog(Slog::levelDebug) << "Text Frame" << endl;
				break;
			case IAX_EVENT_REGREJ:
				slog(Slog::levelDebug) << "Registration Rejected" << endl;
				break;
			case IAX_EVENT_LINKURL:
				slog(Slog::levelDebug) << "Link URL" << endl;
				break;
			default:
				slog(Slog::levelDebug) << "Unknown packet" << endl;
				break;
			}
			if(e->etype != IAX_EVENT_CONNECT)
			{
				slog(Slog::levelDebug) << "Event for non-existant session" << endl;
				continue;
			}
			else
			{
				/*if(e->event.connect.callerid && e->event.connect.dnid)
					slog(Slog::levelDebug) << "Call from " << e->event.connect.callerid << " for " << e->event.connect.dnid << endl;
				else if(e->event.connect.dnid)
					slog(Slog::levelDebug) << "Call from " << e->event.connect.callerid << endl;
				else*/
				slog(Slog::levelDebug) << "Call from " << inet_ntoa(iax_get_peer_addr(e->session).sin_addr) << endl;
			}
			iax_event_free(e);
		}

		int m = iax_time_to_next_event();
		if(m > -1)
		{
			timer.tv_sec = m / 1000;
			timer.tv_usec = (m % 1000) * 1000;
		}
		int regm = 6000; // registration();
		if(!timer.tv_sec || (m > regm))
		{
			timer.tv_sec = regm / 1000;
			timer.tv_usec = (regm % 1000) * 1000;
		}
		/*for(i = 0; i < port_count; i++)
		{
			if(ports[i] == NULL)
				continue;

			if(!ports[i]->getTimer())
			{
				event.id = TRUNK_TIMER_EXPIRED;
				ports[i]->postEvent(&event);
			}
		}*/
	}
}

Trunk *IAXDriver::getTrunkPort(int id)
{
	if(id < 0 || id >= port_count)
		return NULL;

	if(!ports)
		return NULL;

	if(ports[id] == NULL)
	{
		// make new trunk
		return NULL;
	}

	return (Trunk *)ports[id];
}

Trunk *IAXDriver::getOutboundTrunk(int id)
{
	if(id < 0 || id >= port_count)
		return NULL;

	if(!ports)
		return NULL;

	if(ports[id] != NULL)
		return NULL;

	ports[id] = new IAXTrunk(0, id);

	return (Trunk *)ports[id];
}

IAXDriver iaxivr;

#ifdef	CCXX_NAMESPACES
};
#endif
