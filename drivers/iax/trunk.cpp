// Copyright (C) 2000-2002 Open Source Telecom Corporation.
// 
// Author: Mark Lipscombe
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

char IAXTrunk::status[250];

IAXTrunk::IAXTrunk(unsigned ref, unsigned tid) :
Trunk(tid, &iaxivr), AudioService()
{
	trunk = (Trunk *)this;

	t_active = false;
	lastring = 0;
	id = ts = tid;

	//conn = NULL;

	isOutgoing = false;

	slog(Slog::levelDebug) << "IAXTrunk::IAXTrunk()" << endl;
	//handler = &IAXTrunk::idleHandler;
	slog(Slog::levelDebug) << "IAXTrunk::IAXTrunk() END" << endl;
}

IAXTrunk::~IAXTrunk()
{
	t_active = false;
	//delete conn;
	//sleep(400);
	//endTimer();
	//terminate();
	//terminate();
	//slog(Slog::levelDebug) << "d'tor" << endl;
}

void IAXTrunk::initSyms(void)
{
	setConst(SYM_NETWORK, "none");
	setConst(SYM_INTERFACE, "iax");
}

bool IAXTrunk::postEvent(TrunkEvent *event)
{
	slog(Slog::levelDebug) << "IAXTrunk(" << id << ")::postEvent(event->id=" << event->id << ")" << endl;
	bool rtn = true;
	trunkhandler_t prior;
	char evt[65];

	Trunk::enterMutex();
	switch(event->id)
	{
	case TRUNK_TIMER_SYNC:
		if(!synctimer)
			rtn = false;
		synctimer = 0;
		break;
	case TRUNK_TIMER_EXIT:
		if(!exittimer)
			rtn = false;
		exittimer = 0;
		break;
	case TRUNK_TIMER_EXPIRED:
		if(getTimer() < 0)
			rtn = false;
		endTimer();
		break;
	case TRUNK_DTMF_KEYUP:
		if(Trunk::flags.offhook)
			time(&idletime);
		if(!Trunk::flags.dtmf)
			rtn = false;
		break;
	default:
		break;
	}

	if(!rtn)
	{
		Trunk::leaveMutex();
		return false;
	}

	if(!handler)
	{
		Trunk::leaveMutex();
		return false;
	}
retry:
	debug->debugEvent(this, event);
	prior = handler;
	rtn = (this->*handler)(event);
	if(rtn)
	{
		if(handler != prior)
		{
			slog(Slog::levelDebug) << "handler != prior" << endl;
			event->id = TRUNK_ENTER_STATE;
			goto retry;
		}
		Trunk::leaveMutex();
		return true;
	}

	rtn = true;
	switch(event->id)
	{
	case TRUNK_MAKE_IDLE:
		//handler = &IAXTrunk::idleHandler;
		break;
	case TRUNK_MAKE_BUSY:
	case TRUNK_MAKE_STANDBY:
		//handler = &IAXTrunk::busyHandler;
		break;
	case TRUNK_STOP_STATE:
		endTimer();
		//handler = &IAXTrunk::stepHandler;
		break;
	case TRUNK_EXIT_STATE:
		break;
	case TRUNK_EXIT_SHELL:
		tgi.pid = 0;
		break;
	//case TRUNK_RINGING_ON:
	//	++rings;
	//	break;
	case TRUNK_ENTER_STATE:
		endTimer();
		break;
	case TRUNK_STOP_DISCONNECT:
		if(Trunk::flags.onexit)
			break;
		trunkSignal(TRUNK_SIGNAL_HANGUP);
		event->id = TRUNK_STOP_STATE;
		goto retry;
		break;
	case TRUNK_SEND_MESSAGE:
		if(recvEvent(event))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		break;
	case TRUNK_TIMER_EXPIRED:
		trunkSignal(TRUNK_SIGNAL_TIMEOUT);
		event->id = TRUNK_STOP_STATE;
		goto retry;
		break;
	case TRUNK_TIMER_SYNC:
		if(trunkSignal(TRUNK_SIGNAL_TIME))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		break;
	case TRUNK_TIMER_EXIT:
		if(trunkSignal(TRUNK_SIGNAL_TIME))
			event->id = TRUNK_STOP_STATE;
		else
			event->id = TRUNK_STOP_DISCONNECT;
		goto retry;
	case TRUNK_CHILD_EXIT:
		if(!ScriptInterp::isActive())
			break;
		if(trunkSignal(TRUNK_SIGNAL_CHILD))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		break;
	case TRUNK_CALL_INFO:
		if(trunkSignal(TRUNK_SIGNAL_NOTIFY))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		rtn = false;
		break;
	case TRUNK_DTMF_KEYUP:
		if(digits < 32)
			dtmf.bin.data[digits++] = digit[event->parm.dtmf.digit];
		dtmf.bin.data[digits] = 0;
		if(trunkSignal((trunksignal_t)(event->parm.dtmf.digit + TRUNK_SIGNAL_0)))
		{
			event->id = TRUNK_STOP_STATE;
			goto retry;
		}
		break;
	default:
		rtn = false;
	}
	if(handler != prior)
	{
		event->id = TRUNK_ENTER_STATE;
		goto retry;
	}
	Trunk::leaveMutex();
	return rtn;
}

void IAXTrunk::getName(char *buffer)
{
	sprintf(buffer, "iax/%d", id);
}

void IAXTrunk::exit(void)
{
	if(!Trunk::flags.onexit)
		if(redirect("::exit"))
		{
			Trunk::flags.onexit = true;
			return;
		}

	char buffer[33];
	getName(buffer);
	slog(Slog::levelDebug) << buffer << ": script exiting" << endl;
	//handler = &IAXTrunk::hangupHandler;
}

unsigned long IAXTrunk::getIdleTime(void) 
{
        time_t now;
 

	time(&now); 
// 	if(handler == &IAXTrunk::idleHandler) 
		// return now - idle;
 
        return 0; 
}
 
void IAXTrunk::timerTick(void)
{
	TrunkEvent event;

	Trunk::enterMutex();
	if(getTimer())
	{
		Trunk::leaveMutex();
		return;
	}
	endTimer();
	event.id = TRUNK_TIMER_EXPIRED;
	postEvent(&event);
	Trunk::leaveMutex();
}

bool IAXTrunk::playAudio(char *fn)
{
	//return false;
	//Trunk::enterMutex();
	
	//playChannel = new IAXPlay(fn, this);

	/*if(!playChannel->isOpen())
	{
		slog(Slog::levelDebug) << "IAXTrunk::playAudio(): File is not opened" << endl;
		return false;
	}

	if(!outCodec)
	{
		slog(Slog::levelDebug) << "IAXTrunk::playAudio(): outCodec is NULL!" << endl;
		return false;
	}

	outCodec->AttachChannel(playChannel, TRUE);*/
	//Trunk::leaveMutex();
	//return playChannel->OpenFile(fn);
	return true;
}

bool IAXTrunk::Join(IAXTrunk *trk)
{
	slog(Slog::levelDebug) << id << ": Join()" << endl;
	//joined = trk;
	//joined->joined = this;
	//conn->playChannel->Play();
	return true;
}

bool IAXTrunk::Part(void)
{
	TrunkEvent event;

	slog(Slog::levelDebug) << id << ": Part()" << endl;
	//conn->playChannel->Stop();
	//joined->joined = NULL;
	event.id = TRUNK_PART_TRUNKS;
	event.parm.ok = false;
	//joined->postEvent(&event);
	//joined = NULL;
	delete softJoin;
	softJoin = NULL;
	return false;
}

void IAXTrunk::endRecord(void)
{
	char buffer[12];
	struct stat ino;
	int trim;

	sprintf(buffer, "%ld", getPosition());
	setSymbol(SYM_OFFSET, buffer);

	if(data.record.minsize)
	{
		if(getPosition() < data.record.minsize)
		{
			setSymbol(SYM_RECORDED, "0");
			remove(data.record.name);
			URLAudio::close();
			return;
		}
	}

	trim = toBytes(getEncoding(), data.record.trim);
	stat(data.record.name, &ino);
	if(ino.st_size <= trim || data.record.frames)
	{
		setSymbol(SYM_RECORDED, "0");
		remove(data.record.name);
	}
	else
	{
		sprintf(buffer, "%ld",
			getPosition() - data.record.trim);
		setSymbol(SYM_RECORDED, buffer);
		truncate(data.record.name, ino.st_size - trim);
		if(!data.record.append)
			chown(data.record.name, keyserver.getUid(), keyserver.getGid());
		if(data.record.save)
			rename(data.record.name, data.record.save);
	}
	URLAudio::close();
}

void IAXTrunk::endPlay(void)
{
	char buffer[12];
	sprintf(buffer, "%ld", getTransfered());
	setSymbol(SYM_PLAYED, buffer);
	sprintf(buffer, "%ld", getPosition());
	setSymbol(SYM_OFFSET, buffer);
	URLAudio::close();
}

bool IAXTrunk::answerCall(void)
{
	//if(conn)
	//	conn->AnsweringCall(H323Connection::AnswerCallNow);
	return true;
}

bool IAXTrunk::hangupCall(void)
{
	//if(conn)
	//	conn->ClearCall();
	return true;
}

bool IAXTrunk::makeCall(char *digit, char *clid)
{
	unsigned int callref;

	isOutgoing = true;
	//endpoint.MakeCall(digit, callToken, &callref, NULL, 
	//	clid, (void *)this);
	
	return true;
}

char *IAXTrunk::getContinuation(void)
{
	char *fn;

	if(data.play.mode == PLAY_MODE_ONE || data.play.mode == PLAY_MODE_TEMP)
		return NULL;

retry:
	fn = getPlayfile();
	if(fn && data.play.mode == PLAY_MODE_ANY)
	{
		if(!canAccess(fn))
			goto retry;
	}
	return fn;
}

#ifdef	CCXX_NAMESPACES
};
#endif
