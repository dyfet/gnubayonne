// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

ZapataConfig::ZapataConfig() :
Keydata("/bayonne/zapata")
{
	static Keydata::Define defkeys[] = {
	{NULL, NULL}};

	if(isFHS())
		load("/drivers/zapata");

	load(defkeys);
}

ZapataDriver::ZapataDriver() :
Driver()
{
	ports = NULL;
	groups = NULL;
	port_count = 0;

	if(ports)
		memset(ports, 0, sizeof(ZapataTrunk *) * port_count);

	if(groups)
		memset(groups, 0, sizeof(TrunkGroup *) * port_count);

	slog(Slog::levelInfo) << "Zapata driver loaded; capacity=" << port_count << endl;
}

ZapataDriver::~ZapataDriver()
{
	stop();
	if(ports)
		delete ports;

	if(groups)
		delete groups;
}

int ZapataDriver::start(void)
{
	int count = 0;

	if(active)
	{
		slog(Slog::levelError) << "driver already started" << endl;
		return 0;
	}

	slog(Slog::levelInfo) << "driver starting..." << endl;

	active = true;
	return count;
}

void ZapataDriver::stop(void)
{
	if(!active)
		return;

	if(ports)
		memset(ports, 0, sizeof(ZapataTrunk *) * port_count);

	active = false;
	slog(Slog::levelInfo) << "driver stopping..." << endl;
}

Trunk *ZapataDriver::getTrunkPort(int id)
{
	if(id < 0 || id >= port_count)
		return NULL;

	if(!ports)
		return NULL;

	return (Trunk *)ports[id];
}

ZapataDriver zapataivr;

#ifdef	CCXX_NAMESPACES
};
#endif
