// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <ivrconfig.h>
#include <server.h>
#include <zap.h>

#ifdef	CCXX_NAMESPACES
namespace ost {
#endif

class ZapataTrunk;

typedef bool (ZapataTrunk::*trunkhandler_t)(TrunkEvent *event);

class ZapataTrunk : private TimerPort, private Trunk
{
private:
	friend class ZapataDriver;
	friend class ZapataService;

	static char status[240];

	time_t lastring;
	trunkhandler_t handler;
	ZapataTrunk *next, *prev;
	ZapataService *service;

	int dev;

	ZapataTrunk(char *devname, int ts);
	~ZapataTrunk();

	void initSyms(void);
	void getName(char *buffer);
	unsigned long getIdleTime(void);

        bool stepHandler(TrunkEvent *event);
        bool busyHandler(TrunkEvent *event);
        bool idleHandler(TrunkEvent *event);
        bool seizeHandler(TrunkEvent *event);
        bool ringHandler(TrunkEvent *event);
        bool waitHandler(TrunkEvent *event);
        bool loadHandler(TrunkEvent *event);
        bool dialHandler(TrunkEvent *event);
        bool playHandler(TrunkEvent *event);
        bool playwaitHandler(TrunkEvent *event);
        bool recordHandler(TrunkEvent *event);
        bool sleepHandler(TrunkEvent *event);
        bool hangupHandler(TrunkEvent *event);
        bool answerHandler(TrunkEvent *event);
        bool collectHandler(TrunkEvent *event);
        bool flashonHandler(TrunkEvent *event);

	bool postEvent(TrunkEvent *event);
        void setTimer(timeout_t timeout = 0);
        void incTimer(timeout_t timeout);
        void exit(void);
        void trunkStep(trunkstep_t step);

};

class ZapataService : public Thread, private Mutex
{
private:
        friend class ZapataDriver;
        friend class ZapataTrunk;

        fd_set connect;
        int iosync[2];
        int hiwater;
        int count;
        ZapataTrunk *first, *last;
        void attach(ZapataTrunk *trunk);
        void detach(ZapataTrunk *trunk);
        void run(void);
        void update(unsigned char id = 0xff);

        ZapataService();
        ~ZapataService();
};

class ZapataConfig : public Keydata
{
public:
	ZapataConfig();
};

class ZapataDriver : public Driver, public ZapataConfig
{
private:
	
	ZapataTrunk **ports;
	ZapataService *threads;
	int port_count;
	int thread_count;
	int thread_select;

public:
	ZapataDriver();
	~ZapataDriver();

	int start(void);
	void stop(void);

	unsigned getTrunkCount(void)
		{return port_count;};

	Trunk *getTrunkPort(int id);
	ZapataService *getService(void);
	aaScript *getScript(void);
	void notify(unsigned char id);
};

extern ZapataDriver zapataivr;

#ifdef	CCXX_NAMESPACES
};
#endif
