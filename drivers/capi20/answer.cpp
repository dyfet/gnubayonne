// Copyright (C) 2001 Kai Germaschewski
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

bool CapiTrunk::answerHandler(TrunkEvent *event)
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << " "
			 << event->id << endl;

	_cmsg cmsg;
	unsigned err;
	CapiDriver *drv = (CapiDriver *) driver;

	switch(event->id) {
	case TRUNK_ENTER_STATE:
		if(flags.offhook) {
			trunkSignal(TRUNK_SIGNAL_STEP);
			handler = &CapiTrunk::stepHandler;
			return true;
		}
		enterState("answer");
		status[tsid] = 'a';
		rings--;
		// fall through
	case TRUNK_TIMER_EXPIRED:
		++rings;
		endTimer();
		if (rings < data.answer.rings) {
			// simulate ringing
			setTimer(group->getRingTime() * 1000);
			return true;
		}

		if (plci_handler != &CapiTrunk::p2Handler) {
			slog(Slog::levelError) << "CAPI: unexpected state" << endl;
			return true;
		}

		err = CONNECT_RESP(&cmsg, drv->appl_id, 
				   connect_ind_msg_id++, plci,
				   0, // accept
				   1, // B1 transp
				   1, // B2 transp
				   0, // B3 transp
				   (unsigned char *)"", // B1configuration
				   (unsigned char *)"", // B2configuration
				   (unsigned char *)"", // B3configuration
				   (unsigned char *)"", // ConnectedNumber
				   (unsigned char *)"", // ConnectedSubaddress
				   (unsigned char *)"", // LLC
				   (unsigned char *)"", // BChannelinformation
				   (unsigned char *)"", // Keypadfacility
				   (unsigned char *)"", // Useruserdata
				   (unsigned char *)"");// Facilitydataarray
		if (err != CapiNoError) {
			slog(Slog::levelError) << "CAPI: CONNECT_RESP err = "
					 << err << endl;
			return true;
		}
		plci_handler = &CapiTrunk::p4Handler;
		return true;
	case TRUNK_CALL_CONNECT:
		trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &CapiTrunk::stepHandler;
		status[tsid] = 'i';
		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
