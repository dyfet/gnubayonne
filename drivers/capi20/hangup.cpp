// Copyright (C) 2001 Kai Germaschewski
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"
#include <capi20.h>

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

bool CapiTrunk::hangupHandler(TrunkEvent *event)
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << " " 
			 << event->id << endl;

	_cmsg cmsg;
	timeout_t reset;
	unsigned int err;
	CapiDriver *drv = (CapiDriver *) driver;

	switch(event->id) {
#if 0
	case TRUNK_LINE_WINK:
	case TRUNK_STOP_DISCONNECT:
	case TRUNK_CPA_DIALTONE:
		return true;
#endif
	case TRUNK_ENTER_STATE:
		exittimer = synctimer = 0;
		enterState("hangup");
		status[tsid] = 'h';
		disjoin();
		endTimer();
		if (tgi.pid)
			kill(tgi.pid, SIGHUP);
		if (thread)
			reset = thread->stop();
		else
			reset = 0;
		setDTMFDetect(false);
		detach();
		if((timeout_t)group->getHangup() > reset)
			setTimer(group->getHangup());
		else
			setTimer(reset);
		return true;
	case TRUNK_TIMER_EXPIRED:
		endTimer();
		stopServices();
		flags.reset = false;
		if (plci_handler == &CapiTrunk::pActHandler) {
			err = DISCONNECT_REQ(&cmsg, drv->appl_id, 
					     drv->msg_id++, plci,
					     (unsigned char *)"", // BChannelinformation
					     (unsigned char *)"", // Keypadfacility
					     (unsigned char *)"", // Useruserdata
					     (unsigned char *)"");// Facilitydataarray
			if (err != CapiNoError) {
				slog(Slog::levelError) << "CAPI: DISCONNECT_REQ err = "
						 << err << endl;
				return true;
			}
			plci_handler = &CapiTrunk::p5Handler;
 		} else if (plci_handler == &CapiTrunk::p0Handler) {
			handler = &CapiTrunk::idleHandler;
		}
		return true;
	case TRUNK_CALL_RELEASE:
		endTimer();
		handler = &CapiTrunk::idleHandler;
		return true;
	case TRUNK_MAKE_IDLE:
		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
