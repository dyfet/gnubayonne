// Copyright (C) 2001 Kai Germaschewski
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

CapiRecord::CapiRecord(CapiTrunk *trunk)
	: URLAudio(), Service((Trunk *)trunk, keythreads.priAudio())
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << endl;
}

CapiRecord::~CapiRecord()
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << endl;

	struct stat ino;
	char buffer[12];
	int trim;
	CapiTrunk *trk = (CapiTrunk *) trunk;

	trk->record_flag = false;
	if (isThread()) 
		slog(Slog::levelWarning) << "CAPI: called from thread ctx" << endl;
	terminate();

	sprintf(buffer, "%ld", getPosition());
	trunk->setSymbol(SYM_OFFSET, buffer);

	if(data->record.minsize)
	{
		if(getPosition() < data->record.minsize)
		{
			trunk->setSymbol(SYM_RECORDED, "0");
			remove(data->record.name);
			URLAudio::close();
			return;
		}
	}


	URLAudio::close();

	stat(data->record.name, &ino);
	trim = toBytes(getEncoding(), data->record.trim);
	if(ino.st_size <= trim || data->record.frames)
	{
		remove(data->record.name);
		trunk->setSymbol(SYM_RECORDED, "0");
	}
	else
	{
		sprintf(buffer, "%ld",
			getPosition() - data->record.trim);
		trunk->setSymbol(SYM_RECORDED, buffer);
		truncate(data->record.name, ino.st_size - trim);
		chown(data->record.name, keyserver.getUid(), keyserver.getGid());
		if(data->record.save)
			rename(data->record.name, data->record.save);
	}
}

void CapiRecord::initial(void)
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ 
			 << " " << data->record.name << endl;

	unsigned int rate;
	char buffer[32];
	const char *ext;
	const char *fmt = data->record.encoding;
	Info recinfo;

	ext = strrchr(data->record.name, '/');
	if(!ext)
		ext = data->record.name;

	ext = strrchr(ext, '.');

	trunk->getName(buffer);
	recinfo.format = raw;
	recinfo.encoding = alawAudio;
	recinfo.order = 0;
	recinfo.annotation = (char *)data->record.annotation;
	recinfo.rate = 8000;

	if(!ext)
	{
		ext = data->record.extension;
		strcat(data->record.name, ext);
	}

	if(!fmt)
		fmt = "raw";

	if(!stricmp(ext, ".ul"))
	{
		fmt = "mulaw";
		recinfo.encoding = mulawAudio;
	}
	else if(!stricmp(ext, ".al"))
	{
		fmt = "alaw";
		recinfo.encoding = alawAudio;
	}
	else if(!stricmp(ext, ".au") || !stricmp(ext, ".snd")) {
		recinfo.format = sun;
		recinfo.order = __BIG_ENDIAN;
	}
	else if(!stricmp(ext, ".wav")) {
		recinfo.format = riff;
		recinfo.order = __LITTLE_ENDIAN;
	}

	if(!stricmp(fmt, "alaw"))
		recinfo.encoding = alawAudio;
	else if(!stricmp(fmt, "ulaw") || !stricmp(fmt, "mulaw"))
		recinfo.encoding = mulawAudio;
	else if(!stricmp(fmt, "g.711") || !stricmp(fmt, "g711"))
	{
		if(recinfo.encoding != mulawAudio)
			recinfo.encoding = alawAudio;
	}
	else if(stricmp(fmt, "raw"))
	{
		slog(Slog::levelError) << buffer << ": unsupported encoding requested" << endl;
		Service::failure();
	}
	
	if(data->record.offset != (unsigned long)-1)
	{
		open(data->record.name);
		setPosition(data->record.offset);
	}
	else if(data->record.append)
		open(data->record.name);
	else
	{
		remove(data->record.name);
		create(data->record.name, &recinfo);
	}

	if(!isOpen()) {
		slog(Slog::levelError) << data->record.name << ": cannot open" << endl;
		Service::failure();
	}
	if(data->record.append)
		setPosition();

	rate = getSampleRate();
	switch(getEncoding())	{
	case mulawAudio:
	case alawAudio:
		rate = 8000;
		break;
	default:
		slog(Slog::levelError) << buffer << ": unsupported codec required" << endl;
		Service::failure();
	}
}

void CapiRecord::run()
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << endl;
	unsigned frames = 0;

	char name[33];
	Audio::Error status = Audio::errSuccess;
	CapiTrunk *trk = (CapiTrunk *) trunk;
	trk->getName(name);

	if(data->record.info)
	{
		Service::success();
		return;
	}

	trk->record_queue.purge();
	trk->record_flag = true;

	while (status == Audio::errSuccess && !stopped) {
		Semaphore::wait();

		if(data->record.frames && (int)frames++ >= data->record.frames)
		{
			frames = 0;
			setPosition(0);
		}

		while (status == Audio::errSuccess) {
			CapiBuffer *buf = trk->record_queue.dequeue();
			if (!buf) 
				break;

			buf->bitReverse();
			status = putSamples(buf->data, buf->len);
			delete buf;
		}
	}
	trk->record_flag = false;
	trk->record_queue.purge();

	if(status != Audio::errSuccess) {
		slog(Slog::levelError) << name << ": failed recording" << endl;
		Service::failure();
	}

	Service::success();
}

timeout_t CapiRecord::stop(void)
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << endl;

	CapiTrunk *trk = (CapiTrunk *) trunk;

	trk->record_flag = false;
	stopped = true;
	Semaphore::post();

	return 10;
}

bool CapiTrunk::recordHandler(TrunkEvent *event)
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << " " 
			 << event->id << endl;

	unsigned long mask;

	switch(event->id) {
	case TRUNK_SERVICE_SUCCESS:
		trunkSignal(TRUNK_SIGNAL_STEP);
		goto stop;
	case TRUNK_SERVICE_FAILURE:
		setSymbol(SYM_ERROR, "record-failed");
		trunkSignal(TRUNK_SIGNAL_ERROR);
		goto stop;
	case TRUNK_STOP_STATE:
	stop:
		handler = &CapiTrunk::stepHandler;
		return true;
	case TRUNK_DTMF_KEYUP:
		mask = (1 << event->parm.dtmf.digit);
		if(mask & data.record.term) {
			trunkSignal(TRUNK_SIGNAL_STEP);
			event->id = TRUNK_STOP_STATE;
		}
		return false;
	case TRUNK_ENTER_STATE:
		endTimer();
		if (!flags.offhook) {
			trunkSignal(TRUNK_SIGNAL_ERROR);
			setSymbol(SYM_ERROR, "record-on-hook");
			handler = &CapiTrunk::stepHandler;
			return true;
		}
		enterState("record");
		status[tsid] = 'r';
		if (data.record.term)
			setDTMFDetect(true);
		else
			Trunk::setDTMFDetect();
		thread = new CapiRecord(this);
		thread->start();
		setTimer(data.record.timeout);
		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
