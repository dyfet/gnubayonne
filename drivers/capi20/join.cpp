// Copyright (C) 2001 Kai Germaschewski
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

#if 0
CapiSwitch::CapiSwitch(CapiTrunk *trunk)
	: Service((Trunk *)trunk, keythreads.priAudio())
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << endl;

	clrAudio();
}

CapiSwitch::~CapiSwitch()
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << endl;

	CapiTrunk *trk = (CapiTrunk *) trunk;

	clrAudio();
	trk->record_flag = false;
	terminate();
}

void CapiSwitch::initial(void)
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << endl;

}

void CapiSwitch::run()
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << endl;

	setCancel(THREAD_CANCEL_IMMEDIATE);
	char name[33];
	Audio::Error status = Audio::errSuccess;
	CapiTrunk *trk = (CapiTrunk *) trunk;
	CapiDriver *drv = (CapiDriver *) driver;
	trk->getName(name);
	_cmsg cmsg;
	unsigned int err;

	setAudio();
	trk->record_queue.purge();
	trk->record_flag = true;

	while (status == Audio::errSuccess && !stopped) {
		Wait();

		while (status == Audio::errSuccess) {
			CapiBuffer *buf = trk->record_queue.dequeue();
			if (!buf) 
				break;

			drv->enterMutex();
			if (trk->joined->ncci) {
				err = DATA_B3_REQ(&cmsg, 
						  drv->appl_id, 
						  drv->msg_id++,
						  trk->joined->ncci,
						  buf->data,
						  buf->len,
						  0,
						  0);
			}
			drv->leaveMutex();
			if (err != CapiNoError) {
				slog(Slog::levelError) << "CAPI: DATA_REQ err = "
						 << err << endl;
			}
			delete buf;
		}
	}
	clrAudio();
	trk->record_flag = false;
	trk->record_queue.purge();

	if(status != Audio::errSuccess) {
		slog(Slog::levelError) << name << ": failed recording" << endl;
		Service::failure();
	}

	Service::success();
}

timeout_t CapiSwitch::stop(void)
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << endl;

	CapiTrunk *trk = (CapiTrunk *) trunk;

	clrAudio();
	trk->record_flag = false;
	stopped = true;
	Semaphore::post();

	return 10;
}
#endif

void CapiTrunk::disjoin()
{
	TrunkEvent event;

	if (!joined)
		return;

	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << endl;

	event.id = TRUNK_PART_TRUNKS;
	joined->postEvent(&event);
	joined = NULL;
}
		
bool CapiTrunk::joinHandler(TrunkEvent *event)
{
	const char *cp;

	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << " " 
			 << event->id << endl;

	TrunkEvent ev;

	switch(event->id) {
	case TRUNK_TIMER_EXPIRED:
		disjoin();
		if(!trunkSignal(TRUNK_SIGNAL_TIMEOUT))
			trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &CapiTrunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		endTimer();
		if (data.join.trunk) {
			// join
			enterState("join");
			status[tsid] = 'j';
			ev.id = TRUNK_JOIN_TRUNKS;
			ev.parm.trunk = this;
			data.join.trunk->postEvent(&ev);
			if(!joined) {
				if(!trunkSignal(TRUNK_SIGNAL_ERROR))
					trunkSignal(TRUNK_SIGNAL_STEP);
				setSymbol(SYM_ERROR, "join-failed");
				handler = &CapiTrunk::stepHandler;
				return true;
			}
		} else {
			// wait
			enterState("wait");
			status[tsid] = 'w';
			Trunk::setDTMFDetect();
		}
		if (data.join.wakeup)
			setTimer(data.join.wakeup);
		return true;
	case TRUNK_PART_TRUNKS:
		if (!joined)
			return true;

		joined = NULL;
		endTimer();
		if (!trunkSignal(TRUNK_SIGNAL_CANCEL))
			trunkSignal(TRUNK_SIGNAL_HANGUP);
		handler = &CapiTrunk::stepHandler;
		return true;
	case TRUNK_SIGNAL_JOIN:
		if (joined)
			return false;

		if (event->parm.error)
			setSymbol(SYM_ERROR, event->parm.error);
		else
			event->parm.error = "";
		if (!trunkSignal(TRUNK_SIGNAL_EVENT))
			trunkSignal(TRUNK_SIGNAL_HANGUP);
		else
		{
			setSymbol(SYM_EVENTID, "join");
			setSymbol(SYM_EVENTMSG, event->parm.error);
		}
		handler = &CapiTrunk::stepHandler;
		return true;
	case TRUNK_JOIN_TRUNKS:
		if(joined)
			return false;

		endTimer();
		joined = event->parm.trunk;
		joined->setJoined(this);
	
		cp = joined->getSymbol(SYM_GID);
		setSymbol(SYM_JOINID, strchr(cp, '-'));		

#if 0
		thread = new CapiSwitch(this);
		thread->start();

		joined->thread = new CapiSwitch(joined);
		joined->thread->start();
#endif
		return true;
	}
	return false;
}


#ifdef	CCXX_NAMESPACES
};
#endif
