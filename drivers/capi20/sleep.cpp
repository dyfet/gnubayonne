// Copyright (C) 2001 Kai Germaschewski
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"
#include <capi20.h>

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

bool CapiTrunk::sleepHandler(TrunkEvent *event)
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << " " 
			 << event->id << endl;

	timeout_t wakeup;
	char buffer[16];
	char *cp;

	switch(event->id) {
	case TRUNK_ENTER_STATE:
		enterState("sleep");
		Trunk::setDTMFDetect();
		wakeup = data.sleep.wakeup;
		if(data.sleep.rings) // FIXME
			if(rings >= data.sleep.rings)
				wakeup = 0;

	
		if (wakeup == 0) {
			trunkSignal(TRUNK_SIGNAL_STEP);
			handler = &CapiTrunk::stepHandler;
			return true;
		}

		setTimer(wakeup);
		return true;
	case TRUNK_TIMER_EXPIRED:
		if (--data.sleep.loops)	{
			setTimer(data.sleep.wakeup);
			return true;
		}
		if(tgi.pid || data.sleep.save) {
			if(tgi.pid)
				kill(tgi.pid, SIGALRM);
			tgi.pid = 0;
			if (!trunkSignal(TRUNK_SIGNAL_TIMEOUT))
				trunkSignal(TRUNK_SIGNAL_STEP);
		} else {
			trunkSignal(TRUNK_SIGNAL_STEP);
		}
		handler = &CapiTrunk::stepHandler;
		return true;
        case TRUNK_CHILD_FAIL:
                setSymbol(SYM_ERROR, "start-failed");
                if(!trunkSignal(TRUNK_SIGNAL_FAIL))
                        trunkSignal(TRUNK_SIGNAL_STEP);
                handler = &CapiTrunk::stepHandler;
                return true;
        case TRUNK_CHILD_START:
		cp = event->parm.trunk->getSymbol(SYM_GID);
		if(cp)
			cp = strchr(cp, '-');
		if(cp)
			setVariable(data.sleep.save, 16, cp);
                trunkSignal(TRUNK_SIGNAL_STEP);
                handler = &CapiTrunk::stepHandler;
                return true;

	case TRUNK_DTMF_KEYUP:
		if(tgi.pid) {
			kill(tgi.pid, SIGINT);
			tgi.pid = 0;
		}
		return false;
        case TRUNK_SHELL_START:
                if(!tgi.pid)
                        return false;
                tgi.fd = event->parm.fd;
                handler = &CapiTrunk::stepHandler;
                return true;
        case TRUNK_WAIT_SHELL:
                if(tgi.seq != event->parm.waitpid.seq)
                        return false;

                if(tgi.pid)
                        return false;

                tgi.pid = event->parm.waitpid.pid;
                return true;
	case TRUNK_EXIT_SHELL:
                if(tgi.seq != event->parm.exitpid.seq)
                        return false;

		if(!tgi.pid)
			return true;

		tgi.pid = 0;
		endTimer();
		sprintf(buffer, "exit-%d", event->parm.exitpid.status);
		setSymbol(SYM_ERROR, buffer);
		if(event->parm.exitpid.status)
			trunkSignal(TRUNK_SIGNAL_ERROR);
		else
			trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &CapiTrunk::stepHandler;
		return true;	
#if 0
	case TRUNK_RINGING_OFF:
		if(rings > 1)
			stopDSP();
		if(!data.sleep.rings)
			return true;
		if(rings < data.sleep.rings)
			return true;
		endTimer();
		return true;
#endif
	}
	return false;
}

bool CapiTrunk::threadHandler(TrunkEvent *event)
{
	switch(event->id)
	{
        case TRUNK_ENTER_STATE:
                enterState("thread");
                endTimer();
                Trunk::setDTMFDetect();
                TimerPort::setTimer(data.sleep.wakeup);
                thread->start();
                return true;
	case TRUNK_TIMER_EXPIRED:
		setSymbol(SYM_ERROR, "thread-timeout");
		if(!trunkSignal(TRUNK_SIGNAL_TIMEOUT))
			trunkSignal(TRUNK_SIGNAL_ERROR);
		goto stop;
        case TRUNK_SERVICE_SUCCESS:
                trunkSignal(TRUNK_SIGNAL_STEP);
                goto stop;
        case TRUNK_SERVICE_FAILURE:
                setSymbol(SYM_ERROR, "thread-error");
                trunkSignal(TRUNK_SIGNAL_ERROR);
                goto stop;
        case TRUNK_STOP_STATE:
        stop:
                handler = &CapiTrunk::stepHandler;
                return true;
        }
        return false;
}



#ifdef	CCXX_NAMESPACES
};
#endif
