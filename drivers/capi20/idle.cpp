// Copyright (C) 2001 Kai Germaschewski
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"
#include <capi20.h>

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

void CapiTrunk::initSyms(void)
{
	setConst(SYM_NETWORK, "none");
	setConst(SYM_INTERFACE, "isdn");
}

bool CapiTrunk::idleHandler(TrunkEvent *event)
{
  // 	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << " "
  //			 << event->id << endl;

	const char *start = NULL;
	const char *value;
	int argc = 0;
	char script[256];
	unsigned err, chk;
	Request *req;
	char **argv;

	switch(event->id) {
	case TRUNK_ENTER_STATE:
		synctimer = exittimer = 0;
		enterState("idle");
		setIdle(true);
		status[tsid] = '-';
		ScriptSymbol::purge();
		switch(flags.trunk) {
		case TRUNK_MODE_OUTGOING:
			group->decOutgoing();
			break;
		case TRUNK_MODE_INCOMING:
			group->decIncoming();
		}
		tgi.pid = 0;
		disjoin();
		flags.trunk = TRUNK_MODE_INACTIVE;
		flags.ready = true;
		flags.offhook = false;
		endTimer();
		rings = 0;
                value = group->chkRequest();
                if(!atoi(value) && stricmp(value, "hangup"))
                        return true;
                chk = atoi(value);
                if(chk < group->getReady())
                        chk = group->getReady();
                setTimer(chk);
		return true;
        case TRUNK_TIMER_EXPIRED:
                flags.ready = true;
                req = group->getRequest();
                if(req)
                {
                        setConst(SYM_PARENT, req->getParent());
                        argv = req->getList();
                        start = *(argv++);
                        if(attach(start))
                        {
                                flags.trunk = TRUNK_MODE_OUTGOING;
                                setList(argv);
                                flags.ready = false;
                                endTimer();
				syncParent("start:running");
                                handler = &CapiTrunk::stepHandler;
                                group->incOutgoing();
                                delete req;
                                return true;
                        }
			syncParent("start:failed");
                        ScriptSymbol::purge();
                        delete req;
                }
                chk = atoi(group->chkRequest());
                if(chk)
                        setTimer(chk);
                return true;
        case TRUNK_RING_REDIRECT:
                start = group->getRedirect(event->parm.argv[0], script);
                rings = 0;
                if(attach(start))
                {
                        setConst(SYM_REDIRECT, event->parm.argv[0]);
                        setList(&event->parm.argv[1]);
                        flags.ready = false;
                        flags.trunk = TRUNK_MODE_INCOMING;
                        endTimer();
                        handler = &CapiTrunk::stepHandler;
                        group->incIncoming();
                        return true;
                }
		ScriptInterp::purge();
                slog(Slog::levelError) << "CAPI: " << id << ": " << 
			event->parm.argv[0] << "unable to redirect" << endl;
                return true;

	case TRUNK_START_SCRIPT:
		flags.trunk = TRUNK_MODE_OUTGOING;

	case TRUNK_RING_START:
		start = event->parm.argv[0];
		++argc;
		
		if (attach(start)) {
			syncParent("start:running");
			setList(&event->parm.argv[argc]);
			endTimer();
			handler = &CapiTrunk::stepHandler;
			flags.trunk = TRUNK_MODE_OUTGOING;
			group->incOutgoing();
			return true;
		}
		syncParent("start:failed");
		ScriptSymbol::purge();

		slog(Slog::levelError) << "CAPI: " << id << ": " << start 
				 << ": cannot start" << endl;
		return true;		
	case TRUNK_CALL_DETECT:
	case TRUNK_RINGING_ON:
		_cmsg cmsg;

		rings = 0;
		if(!group->getAnswer())
			return true;
		
		CapiDriver *drv = (CapiDriver *) driver;
		
		group->incIncoming();
		endTimer();
		err = ALERT_REQ(&cmsg, drv->appl_id, drv->msg_id++,
				plci,
				(unsigned char *)"",
				(unsigned char *)"",
				(unsigned char *)"",
				(unsigned char *)"");
		if (err != CapiNoError) {
			slog(Slog::levelError) << "CAPI: ALERT_REQ err = "
					 << err << endl;
		}
		handler = &CapiTrunk::ringHandler;
		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
