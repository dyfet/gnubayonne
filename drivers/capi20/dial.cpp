// Copyright (C) 2001 Kai Germaschewski
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

bool CapiTrunk::dialHandler(TrunkEvent *event)
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << " " 
			 << event->id << endl;

	unsigned char called_party_number[70];
	unsigned int err;
	_cmsg cmsg;
	CapiDriver *drv = (CapiDriver *) driver;

	switch(event->id) {
	case TRUNK_ENTER_STATE:
		enterState("dial");
		endTimer();

		if (plci_handler != &CapiTrunk::p0Handler) {
			slog(Slog::levelError) << "CAPI: unexpected state" << endl;
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &CapiTrunk::stepHandler;
			return true;
		}

		connect_req_msg_id = drv->msg_id++;
		called_party_number[0] = 1 + strlen(data.dialxfer.digit);
		called_party_number[1] = 0x80;
		strcpy((char *) &called_party_number[2], data.dialxfer.digit);
					
		err = CONNECT_REQ(&cmsg, drv->appl_id, 
				  connect_req_msg_id, 
				  contr,
				  16, // CIPValue
				  called_party_number,
				   (unsigned char *)"", // CallingPartyNumber
				   (unsigned char *)"", // CalledPartySubaddress
				   (unsigned char *)"", // CallingPartySubaddress,
				   1, // B1 transp
				   1, // B2 transp
				   0, // B3 transp
				   (unsigned char *)"", // B1configuration
				   (unsigned char *)"", // B2configuration
				   (unsigned char *)"", // B3configuration
				   (unsigned char *)"", // BC
				   (unsigned char *)"", // HLC
				   (unsigned char *)"", // LLC
				   (unsigned char *)"", // BChannelinformation
				   (unsigned char *)"", // Keypadfacility
				   (unsigned char *)"", // Useruserdata
				   (unsigned char *)"");// Facilitydataarray
		if (err != CapiNoError) {
			slog(Slog::levelError) << "CAPI: CONNECT_REQ err = "
					 << err << endl;
			return true;
		}
		plci_handler = &CapiTrunk::p01Handler;
#if 0
		setTimer(data.dialxfer.timeout);
#endif
		return true;
	case TRUNK_CALL_CONNECT:
		if(data.dialxfer.exit)
			trunkSignal(TRUNK_SIGNAL_HANGUP);
		else
			trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &CapiTrunk::stepHandler;
		return true;
#if 0
	case TRUNK_TIMER_EXPIRED:
		handler = &CapiTrunk::hangupHandler;
		return true;
#endif
	}

	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
