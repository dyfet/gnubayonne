// Copyright (C) 2001 Kai Germaschewski
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"
#include <capi20.h>

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

bool CapiTrunk::ringHandler(TrunkEvent *event)
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << " "
			 << event->id << endl;

	char *args[2];
	char **start = NULL;

	switch(event->id) {
	case TRUNK_ENTER_STATE:
		enterState("ring");
		status[tsid] = '!';
		// fall through
	case TRUNK_TIMER_EXPIRED:
		++rings;
		if (rings < group->getAnswer()) {
			// simulate ringing
			setTimer(group->getRingTime() * 1000);
			return true;
		}
		start = getInitial(args);
		slog(Slog::levelDebug) << "starting script " << *start << endl;

		if(attach(*start)) {
			setList(++start);
			handler = &CapiTrunk::stepHandler;
			return true;
		}
		rings = 0;
		slog(Slog::levelError) << "CAPI: " << id 
				 <<": cannot answer call" << endl;	
		return true;
	case TRUNK_CALL_RELEASE:
		endTimer();
		handler = &CapiTrunk::idleHandler;
		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
