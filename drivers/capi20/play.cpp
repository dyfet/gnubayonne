// Copyright (C) 2001 Kai Germaschewski
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

CapiPlay::CapiPlay(CapiTrunk *trunk)
	: URLAudio(), Service((Trunk *)trunk, keythreads.priAudio())
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << endl;

	num_frames = 2;
	frame_size = capiivr.getBufferSize();
	id = trunk->id;
}

CapiPlay::~CapiPlay()
{
	char buffer[12];

	slog(Slog::levelDebug) << "CAPI: ~" << __FUNCTION__ << endl;
	
	if (isThread()) 
		slog(Slog::levelWarning) << "CAPI: called from thread ctx" << endl;
	terminate();

	sprintf(buffer, "%ld", getTransfered());
	trunk->setSymbol(SYM_PLAYED, buffer);
	sprintf(buffer, "%ld", getPosition());
	trunk->setSymbol(SYM_OFFSET, buffer);
	URLAudio::close();
}

void CapiPlay::initial()
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << endl;

	CapiTrunk *trk = (CapiTrunk *) trunk;
	char name[32];
	char buffer[32];
	char *fn;
	struct stat ino;
	char *ann;
	struct tm *dt;
	struct tm tbuf;

	trunk->getName(name);

	if(tts)
		if(!tts->synth(id, data))
			Service::failure();

retry:
	fn = getPlayfile();
	if(!fn) {
		if (data->play.lock)
		{
			cachelock.unlock();
			data->play.lock = false;
		}
		if (data->play.mode == PLAY_MODE_ANY)
			Service::success();
		slog(Slog::levelError) << name << ": no file to play" << endl; 
		Service::failure();
	}
	
	slog(Slog::levelDebug) << "CAPI: " << fn << endl;
	stat(fn, &ino);
	open(fn);	
	if(data->play.lock)
	{
		cachelock.unlock();
		data->play.lock = false;
	}
	if(!isOpen()) {
		if(data->play.mode == PLAY_MODE_ANY || 
		   data->play.mode == PLAY_MODE_ONE)
			goto retry;

		slog(Slog::levelError) << name << ": " << fn << ": cannot open" << endl;
		Service::failure();
	}

	if (data->play.mode == PLAY_MODE_TEMP)
		remove(fn);

	if(data->play.offset)
		setPosition(data->play.offset);

	if(data->play.limit)
		setLimit(data->play.limit);

	dt = localtime_r(&ino.st_ctime, &tbuf);
	sprintf(buffer, "%04d%02d%02d,%02d%02d%02d",
		dt->tm_year + 1900, dt->tm_mon + 1, dt->tm_mday,
		dt->tm_hour, dt->tm_min, dt->tm_sec);
	trunk->setSymbol(SYM_CREATED, buffer);

	ann = getAnnotation();
	if (ann)
		trunk->setSymbol(SYM_ANNOTATION, ann);
	else
		trunk->setSymbol(SYM_ANNOTATION, "");

	if (getEncoding() != alawAudio &&
	    getEncoding() != mulawAudio) {
		slog(Slog::levelError) << name << ": unsupported codec required" << endl;
		Service::failure();
	}
	trk->play_frames = 0;
}

void CapiPlay::run()
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << endl;

	char name[33];
	CapiTrunk *trk = (CapiTrunk *) trunk;
	CapiDriver *drv = (CapiDriver *) trunk->getDriver();
	Audio::Error status = Audio::errSuccess;
	CapiBuffer buf(frame_size);
	unsigned int err;
	_cmsg cmsg;

	trunk->getName(name);

	while (!stopped) {
		if (trk->play_frames >= (int)num_frames) {
			Semaphore::wait();
			continue;
		}
		status = getSamples(buf.data, buf.len);
		if(status == errReadIncomplete && data->play.maxtime)
		{
			status = Audio::errSuccess;
			setPosition(0);
			continue;
		}
		if (status == errReadFailure) 
			break;
		buf.bitReverse();
		err = DATA_B3_REQ(&cmsg, drv->appl_id, drv->msg_id++,
				  trk->ncci,
				  buf.data,
				  buf.len,
				  0,
				  0);
		if (err != CapiNoError) {
				slog(Slog::levelError) << "CAPI: DATA_REQ err = "
						 << err << endl;
				break;
		}
		++trk->play_frames;
		if (status == errReadIncomplete) 
			break;
	}
	if (status == errReadFailure) {
		slog(Slog::levelError) << name << ": failed playback" << endl;
		Service::failure();
	}
	while (trk->play_frames > 0)
		Semaphore::wait();

	Service::success();
}

char *CapiPlay::getContinuation()
{
	char *fn;

	if (data->play.mode == PLAY_MODE_ONE || 
            data->play.mode == PLAY_MODE_TEMP)
		return NULL;
retry:
	fn = getPlayfile();

	if(fn && data->play.mode == PLAY_MODE_ANY) {
		slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << fn << endl;
		if(!canAccess(fn))
			goto retry;
	}
	return fn;
}

timeout_t CapiPlay::stop(void)
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << endl;

	CapiTrunk *trk = (CapiTrunk *) trunk;

	stopped = true;

	return ~0;
	return trk->play_frames * frame_size * 1000/8000;
}

bool CapiTrunk::playHandler(TrunkEvent *event)
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << " " 
			 << event->id << endl;

	switch(event->id) {
	case TRUNK_ENTER_STATE:
		endTimer();
		if (!flags.offhook) {
			trunkSignal(TRUNK_SIGNAL_ERROR);
			setSymbol(SYM_ERROR, "play-on-hook");
			handler = &CapiTrunk::stepHandler;
			return true;
		}
		enterState("play");
		status[tsid] = 'p';
		Trunk::setDTMFDetect();
		thread = new CapiPlay(this);
		thread->start();

		if(data.play.maxtime)
			setTimer(data.play.maxtime);

		return true;
	case TRUNK_SERVICE_SUCCESS:
		trunkSignal(TRUNK_SIGNAL_STEP);
		goto stop;
	case TRUNK_SERVICE_FAILURE:
		setSymbol(SYM_ERROR, "play-failed");
		trunkSignal(TRUNK_SIGNAL_ERROR);
		goto stop;
	case TRUNK_STOP_STATE:
	stop:
		handler = &CapiTrunk::stepHandler;
		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
