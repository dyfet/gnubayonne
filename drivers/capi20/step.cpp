// Copyright (C) 2001 Kai Germaschewski
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

void CapiTrunk::trunkStep(trunkstep_t step)
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << endl;

	TrunkEvent event;

	if(handler != &CapiTrunk::stepHandler)
		return;

	event.id = TRUNK_MAKE_STEP;
	event.parm.step = step;
	stepHandler(&event);
}

bool CapiTrunk::stepHandler(TrunkEvent *event)
{
	Trunk *alt = ctx;
	if (event->id != 401)
		slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << " "
				 << event->id << endl;

	switch(event->id) {
	case TRUNK_MAKE_STEP:
		if(idleHangup())
			return true;
		switch(event->parm.step) {
		case TRUNK_STEP_THREAD:
			handler = &CapiTrunk::threadHandler;
			return true;
		case TRUNK_STEP_ANSWER:
			handler = &CapiTrunk::answerHandler;
			return true;
		case TRUNK_STEP_PLAY:
			handler = &CapiTrunk::playHandler;
			return true;
		case TRUNK_STEP_SLEEP:
			handler = &CapiTrunk::sleepHandler;
			return true;
		case TRUNK_STEP_COLLECT:
			handler = &CapiTrunk::collectHandler;
			return true;
		case TRUNK_STEP_RECORD:
			handler = &CapiTrunk::recordHandler;
			return true;
		case TRUNK_STEP_TONE:
			handler = &CapiTrunk::toneHandler;
			return true;
		case TRUNK_STEP_HANGUP:
			handler = &CapiTrunk::hangupHandler;
			return true;
		case TRUNK_STEP_SOFTDIAL:
		case TRUNK_STEP_DIALXFER:
			handler = &CapiTrunk::dialHandler;
			return true;
		case TRUNK_STEP_JOIN:
			handler = &CapiTrunk::joinHandler;
			return true;
#if 0
		case TRUNK_STEP_FLASH:
			handler = &PikaTrunk::flashonHandler;
			return true;
		case TRUNK_STEP_PLAYWAIT:
			handler = &PikaTrunk::playwaitHandler;
			return true;
		case TRUNK_STEP_DETECT:
			handler = &PikaTrunk::detectHandler;
			return true;
		case TRUNK_STEP_DUPLEX:
			handler = &PikaTrunk::duplexHandler;
			return true;
#endif
#ifdef	XML_SCRIPTS
		case TRUNK_STEP_LOADER:
			handler = &CapiTrunk::loadHandler;
			return true;
#endif
		}
		slog(Slog::levelDebug) << "CAPI: " << event->parm.step
				 << "not handled" << endl;
		return false;
	case TRUNK_TIMER_EXPIRED:
		endTimer();
		flags.reset = false;
		stopServices();
		// fall through
	case TRUNK_ENTER_STATE:
		endTimer();
                if(flags.offhook)
                {
                        if(flags.trunk == TRUNK_MODE_OUTGOING)
                                status[tsid] = 'o';
                        else
                                status[tsid] = 'i';
                }

                if(tgi.fd > -1 && digits)
                {
                        snprintf(buffer, sizeof(buffer), "%s\n", dtmf.bin.data);
                        writeShell(buffer);
                        digits = 0;
                }

		// if a dsp reset has occured, then we add a delay
		// for the dsp to settle before stepping
		if (thread) {
			enterState("reset");
			setTimer(thread->stop());
			return true;
		}

		Trunk::setDTMFDetect();
                if(tgi.fd > -1)
		{
                        enterState("shell");
                        return true;
		}

		strcpy(numbers[5].sym.data, "step");
		debug->debugStep(this, getScript());
		monitor->monitorStep(this, getScript());
		// step and delay if still in same handler

		if(alt != this)
			alt->enterMutex();
		scriptStep();
		if(alt != this)
			alt->leaveMutex();

		if(handler == &CapiTrunk::stepHandler)
			setTimer(keythreads.getStepDelay());

		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
