// Copyright (C) 2001 Kai Germaschewski
// Copyright (C) 2002/2003 Peter Krapfl
//
// Version 1.2.0.2
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <ivrconfig.h>
#include <server.h>
#include <cc++/audio.h>
#include <cc++/strchar.h>
#include <capi20.h>

#ifdef	CCXX_NAMESPACES
namespace ost {
#endif

#define MAX(a,b) ((a)>(b)?(a):(b))

#define BUG(a) slog(Slog::levelError) << __FILE__ << ":" << __LINE__ << " " << __FUNCTION__ << endl

class CapiTrunk;
class CapiDriver;

extern CapiDriver capiivr;

typedef	bool (CapiTrunk::*trunkhandler_t)(TrunkEvent *event);
typedef	bool (CapiTrunk::*plcihandler_t)(_cmsg *cmsg);
typedef	bool (CapiTrunk::*nccihandler_t)(_cmsg *cmsg);

class CapiBufferBase
{
 public:
	CapiBufferBase *next;
	CapiBufferBase *prev;
};

class CapiBuffer : public CapiBufferBase
{
 public:
	unsigned char *data;
	unsigned int len;

	CapiBuffer(unsigned int len);
	~CapiBuffer();

	void bitReverse();
};

class CapiBufferQueue : protected CapiBufferBase, Mutex
{
 public:
	unsigned int qlen;

	CapiBufferQueue();
	~CapiBufferQueue();

	void queueTail(CapiBuffer *buf);
	CapiBuffer *dequeue();
	void purge();
};

class CapiPlay : private URLAudio, public Service
{
private:
	unsigned int num_frames;
	unsigned int frame_size;
	int id;

	void initial();
	void run();
	char *getContinuation();

public:
	CapiPlay(CapiTrunk *trunk);
	~CapiPlay();
	timeout_t stop();
};

class CapiTone : public Service
{
private:
	unsigned int num_frames;
	unsigned int frame_size;
	unsigned char *buffer;
	unsigned int len;

	void initial();
	void run();

public:
	CapiTone(CapiTrunk *trunk);
	~CapiTone();
	timeout_t stop();
};

#if 0
class CapiSwitch : public Service
{
private:
	void initial();
	void run();

public:
	CapiSwitch(CapiTrunk *trunk);
	~CapiSwitch();
	timeout_t stop();
};
#endif

class CapiRecord : private URLAudio, public Service
{
private:
	void initial();
	void run();

public:
	CapiRecord(CapiTrunk *trunk);
	~CapiRecord();
	timeout_t stop();
	void DataB3Ind(_cmsg *cmsg);
};

class CapiTrunk : private TimerPort, private Trunk
{
private:
	friend class CapiDriver;
	friend class CapiConf;
	friend class CapiPlay;
	friend class CapiTone;
	friend class CapiRecord;
	friend class CapiSwitch;

	unsigned contr;
	unsigned plci;
	unsigned ncci;
	plcihandler_t plci_handler;
	nccihandler_t ncci_handler;
 	unsigned connect_ind_msg_id;
 	unsigned connect_req_msg_id;
	trunkhandler_t handler;

	AtomicCounter play_frames;

	CapiBufferQueue record_queue;
	bool record_flag;

	CapiTrunk(unsigned b_channel, int controller);
	~CapiTrunk();

	bool idleHandler(TrunkEvent *event);
	bool loadHandler(TrunkEvent *event);
	bool ringHandler(TrunkEvent *event);
	bool stepHandler(TrunkEvent *event);
	bool hangupHandler(TrunkEvent *event);
	bool answerHandler(TrunkEvent *event);
	bool playHandler(TrunkEvent *event);
	bool recordHandler(TrunkEvent *event);
	bool sleepHandler(TrunkEvent *event);
	bool threadHandler(TrunkEvent *event);
	bool collectHandler(TrunkEvent *event);
	bool toneHandler(TrunkEvent *event);
	bool dialHandler(TrunkEvent *event);
	bool joinHandler(TrunkEvent *event);

	bool p0Handler(_cmsg *cmsg);
	bool p01Handler(_cmsg *cmsg);
	bool p1Handler(_cmsg *cmsg);
	bool p2Handler(_cmsg *cmsg);
	bool p4Handler(_cmsg *cmsg);
	bool pActHandler(_cmsg *cmsg);
	bool p5Handler(_cmsg *cmsg);

	bool n2Handler(_cmsg *cmsg);
	bool nActHandler(_cmsg *cmsg);

	bool recvCapiMsg(_cmsg *cmsg);
	void getName(char *buffer);
	void trunkStep(trunkstep_t);
	bool postEvent(TrunkEvent *evt);
	void putEvent(TrunkEvent *evt);
	void setDTMFDetect(bool flag);
	unsigned long getIdleTime();
	void exit(void);
	void setTimer(timeout_t time);
	void endTimer(void);
	void disjoin(void);
	void initSyms(void);

	bool scrJoin(void);
	bool scrWait(void);

protected:
	const char *getDefaultEncoding()
		{return "alaw";};
public:
	Driver *getDriver(void)
		{return (Driver*)&capiivr;};
};


class CapiConfig : public Keydata
{
public:
	CapiConfig();

	inline unsigned getBufferSize(void)
		{return atoi(getLast("buffersize"));};

};


class CapiDriver : public Driver, public CapiConfig, public Thread, public Mutex
{
	friend class CapiTrunk;
	friend class CapiPlay;
	friend class CapiTone;
	friend class CapiSwitch;

private:
	unsigned contr_count;
	unsigned b_chan[255];
	unsigned port_count;
	unsigned appl_id;
	unsigned msg_id;
	CapiTrunk **ports;

	bool running;
	int evbuf[30];

public:
	CapiDriver();
	~CapiDriver();

	int start(void);
	void stop(void);
	unsigned getTrunkCount(void);
	Trunk *getTrunkPort(int id);

	void run(void);
	void handleCapiMessage(void);

	char *getName(void)
		{return "capi";};
};

#ifdef	CCXX_NAMESPACES
};
#endif
