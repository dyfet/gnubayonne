// Copyright (C) 2001 Kai Germaschewski
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

CapiRecord::CapiRecord(CapiTrunk *trunk)
	: URLAudio(), Service((Trunk *)trunk, keythreads.priAudio())
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << endl;

	clrAudio();
}

CapiRecord::~CapiRecord()
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ << endl;

	struct stat ino;
	char buffer[12];
	int trim;
	CapiTrunk *trk = (CapiTrunk *) trunk;

	clrAudio();
	trk->record_flag = false;
	terminate();

	sprintf(buffer, "%ld", getPosition());
	trunk->setSymbol(SYM_RECORDED, buffer);
	trim = toBytes(getEncoding(), atoi(trunk->getSymbol(SYM_TRIM)));
	URLAudio::close();

	stat(data->record.name, &ino);
	truncate(data->record.name, ino.st_size - trim);
	chown(data->record.name, keyserver.getUid(), keyserver.getGid());
}

void CapiRecord::initial(void)
{
	slog(Slog::levelDebug) << "CAPI: " << __FUNCTION__ 
			 << " " << data->record.name << endl;

	unsigned int rate;
	char buffer[32];
	char *ext = strrchr(data->record.name, '.');
	char *fmt = trunk->getSymbol(SYM_FORMAT);
	audioinfo_t recinfo;

	trunk->getName(buffer);
	recinfo.format = AUDIO_FORMAT_RAW;
	recinfo.encoding = MULAW_AUDIO_ENCODING;
	recinfo.order = 0;
	recinfo.annotation = trunk->getSymbol(SYM_ANNOTATION);
	recinfo.rate = 8000;

	if(!ext)
		ext = trunk->getSymbol(SYM_EXTENSION);

	if(!fmt)
		fmt = "raw";

	if(!stricmp(ext, ".al"))
		recinfo.encoding = ALAW_AUDIO_ENCODING;
	else if(!stricmp(ext, ".au")) {
		recinfo.format = AUDIO_FORMAT_SUN;
		recinfo.order = __BIG_ENDIAN;
	}
	else if(!stricmp(ext, ".wav")) {
		recinfo.format = AUDIO_FORMAT_RIFF;
		recinfo.order = __LITTLE_ENDIAN;
	}

	if(!stricmp(fmt, "alaw"))
		recinfo.encoding = ALAW_AUDIO_ENCODING;
	
	if(data->record.append)
		Open(data->record.name);
	else
		Create(data->record.name, &recinfo);	
	if(!isOpen()) {
		slog(SLOG_ERROR) << data->record.name << ": cannot open" << endl;
		Failure();
	}
	if(data->record.append)
		setPosition();

	rate = getSampleRate();
	switch(getEncoding())	{
	case MULAW_AUDIO_ENCODING:
	case ALAW_AUDIO_ENCODING:
		rate = 8000;
		break;
	default:
		slog(SLOG_ERROR) << buffer << ": unsupported codec required" << endl;
		Failure();
	}
}

void CapiRecord::Run()
{
	slog(SLOG_DEBUG) << "CAPI: " << __FUNCTION__ << endl;

	char name[33];
	audioerror_t status = AUDIO_SUCCESS;
	CapiTrunk *trk = (CapiTrunk *) trunk;
	trk->getName(name);

	setAudio();
	trk->record_queue.purge();
	trk->record_flag = true;

	while (status == AUDIO_SUCCESS && !stopped) {
		Wait();

		while (status == AUDIO_SUCCESS) {
			CapiBuffer *buf = trk->record_queue.dequeue();
			if (!buf) 
				break;

			status = putSamples(buf->data, buf->len);
			delete buf;
		}
	}
	clrAudio();
	trk->record_flag = false;
	trk->record_queue.purge();

	if(status != AUDIO_SUCCESS) {
		slog(SLOG_ERROR) << name << ": failed recording" << endl;
		Failure();
	}

	Success();
}

timeout_t CapiRecord::Stop(void)
{
	slog(SLOG_DEBUG) << "CAPI: " << __FUNCTION__ << endl;

	CapiTrunk *trk = (CapiTrunk *) trunk;

	clrAudio();
	trk->record_flag = false;
	stopped = true;
	Semaphore::Post();

	return 10;
}

bool CapiTrunk::recordHandler(TrunkEvent *event)
{
	slog(SLOG_DEBUG) << "CAPI: " << __FUNCTION__ << " " 
			 << event->id << endl;

	unsigned long mask;

	switch(event->id) {
	case TRUNK_SERVICE_SUCCESS:
		TrunkSignal(TRUNK_SIGNAL_STEP);
		goto stop;
	case TRUNK_SERVICE_FAILURE:
		setSymbol(SYM_ERROR, "record-failed");
		TrunkSignal(TRUNK_SIGNAL_ERROR);
		goto stop;
	case TRUNK_STOP_STATE:
	stop:
		handler = &CapiTrunk::stepHandler;
		return true;
	case TRUNK_DTMF_KEYUP:
		mask = (1 << event->parm.dtmf.digit);
		if(mask & data.record.term) {
			TrunkSignal(TRUNK_SIGNAL_STEP);
			event->id = TRUNK_STOP_STATE;
		}
		return false;
	case TRUNK_ENTER_STATE:
		endTimer();
		debug->DebugState(this, "record");
		if (data.record.term)
			setDTMFDetect(true);
		else
			Trunk::setDTMFDetect();
		thread = new CapiRecord(this);
		thread->Start();
		setTimer(data.record.timeout);
		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
