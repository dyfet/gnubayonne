// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

void VPBTrunk::trunkStep(trunkstep_t step)
{
	TrunkEvent event;
	
	if(handler != &VPBTrunk::stepHandler)
		return;

	event.id = TRUNK_MAKE_STEP;
	event.parm.step = step;
	stepHandler(&event);
}

bool VPBTrunk::stepHandler(TrunkEvent *event)
{
	Trunk *alt = ctx;

	switch(event->id)
	{
	case TRUNK_MAKE_STEP:
		if(idleHangup())
			return true;
		switch(event->parm.step)
		{
		case TRUNK_STEP_THREAD:
			handler = &VPBTrunk::threadHandler;
			return true;
		case TRUNK_STEP_SLEEP:
			handler = &VPBTrunk::sleepHandler;
			return true;
		case TRUNK_STEP_TONE:
			handler = &VPBTrunk::toneHandler;
			return true;
		case TRUNK_STEP_HANGUP:
			handler = &VPBTrunk::hangupHandler;
			return true;
		case TRUNK_STEP_ANSWER:
			handler = &VPBTrunk::answerHandler;
			return true;
		case TRUNK_STEP_COLLECT:
			handler = &VPBTrunk::collectHandler;
			return true;
		case TRUNK_STEP_FLASH:
			handler = &VPBTrunk::flashonHandler;
			return true;
		case TRUNK_STEP_PLAYWAIT:
			handler = &VPBTrunk::playwaitHandler;
			return true;
		case TRUNK_STEP_PLAY:
			handler = &VPBTrunk::playHandler;
			return true;
		case TRUNK_STEP_RECORD:
			handler = &VPBTrunk::recordHandler;
			return true;
#ifdef	XML_SCRIPTS
		case TRUNK_STEP_LOADER:
			handler = &VPBTrunk::loadHandler;
			return true;
#endif
		case TRUNK_STEP_SOFTDIAL:
			data.dialxfer.sync = false;
			handler = &VPBTrunk::digitDialer;
			return true;
		case TRUNK_STEP_DIALXFER:
		  //  ===== not to comment out
			data.dialxfer.sync = false;
	      		handler = &VPBTrunk::dialHandler;
			return true;
		case TRUNK_STEP_JOIN:
			handler = &VPBTrunk::joinHandler;
			return true;
		case TRUNK_STEP_LISTEN:
			handler = &VPBTrunk::listenHandler;
			return true;
		}
		return false;
	case TRUNK_SERVICE_SUCCESS:
	case TRUNK_SERVICE_FAILURE:
//		endTimer();
//		setTimer(keythreads.getResetDelay());
		if(thread)
			exitThread();
		return true;
	case TRUNK_EXIT_STATE:
		if(!thread)
			return true;
		flags.reset = false;
		stopServices();
		return true;
	case TRUNK_TIMER_EXPIRED:
	case TRUNK_STOP_STATE:
		flags.reset = false;
		stopServices();
	case TRUNK_ENTER_STATE:
		if(flags.offhook)
		{
			if(flags.trunk == TRUNK_MODE_OUTGOING)
				status[tsid] = 'o';
			else
				status[tsid] = 'i';
		}
                if(tgi.fd > -1 && digits)
                {
                        snprintf(buffer, sizeof(buffer), "%s\n", dtmf.bin.data);
                        writeShell(buffer);
                        digits = 0;
                }
                Trunk::setDTMFDetect();
		endTimer();

		// if a dsp reset has occured, then we add a delay
		// for the dsp to settle before stepping
		if(flags.reset || thread)
		{
			enterState("reset");
			if(thread)
				setTimer(thread->stop());
			else
				setTimer(keythreads.getResetDelay());
			return true;
		}

		if(tgi.fd > -1)
		{
			enterState("shell");
			return true;
		}
	
		strcpy(numbers[5].sym.data, "step");
		debug->debugStep(this, getScript());
		monitor->monitorStep(this, getScript());

		// step and delay if still in same handler

		if(alt != this)
			alt->enterMutex();

		scriptStep();

		if(alt != this)
			alt->leaveMutex();

		if(handler == &VPBTrunk::stepHandler)
			setTimer(vpbivr.stepTimer);

		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
