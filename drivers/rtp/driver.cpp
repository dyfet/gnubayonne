// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

RTPConfig::RTPConfig() :
Keydata("/bayonne/rtp")
{
	static KEYDEF defkeys[] = {
	{"first", "3128"},
	{"count", "32"},
	{"inc", "4"},
	{"interface", "*"},
	{NULL, NULL}};

	Load(defkeys);
}

InetHostAddress RTPConfig::getBindAddress(void)
{
	const char *cp = getLast("bind");
	if(!cp)
		cp = getLast("address");
	if(!cp)
		cp = getLast("interface");

	return InetHostAddress(cp);
}

RTPDriver::RTPDriver() :
Driver()
{
	int count = rtpivr.getRTPCount();

	ports = NULL;
	groups = NULL;
	port_count = 0;

	ports = new RTPTrunk *[count];
	groups = new TrunkGroup *[count];

	if(ports)
		memset(ports, 0, sizeof(RTPTrunk *) * port_count);

	if(groups)
		memset(groups, 0, sizeof(TrunkGroup *) * port_count);

	slog(SLOG_INFO) << "RTP trunk driver loaded; capacity=" << port_count << endl;
}

RTPDriver::~RTPDriver()
{
	Stop();
	if(ports)
		delete[] ports;

	if(groups)
		delete[] groups;
}

int RTPDriver::Start(void)
{
	int count = 0;

	if(active)
	{
		slog(SLOG_ERROR) << "driver already started" << endl;
		return 0;
	}

	slog(SLOG_INFO) << "driver starting..." << endl;

	active = true;
	return count;
}

void RTPDriver::Stop(void)
{
	if(!active)
		return;

	if(ports)
		memset(ports, 0, sizeof(RTPTrunk *) * port_count);

	active = false;
	slog(SLOG_INFO) << "driver stopping..." << endl;
}

Trunk *RTPDriver::getTrunkPort(int id)
{
	if(id < 0 || id >= port_count)
		return NULL;

	if(!ports)
		return NULL;

	return (Trunk *)ports[id];
}

RTPDriver rtpivr;
