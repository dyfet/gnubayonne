// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <ivrconfig.h>
#include <server.h>
#include <cc++/audio.h>
#include <cc++/rtp.h>

class RTPTrunk;
class RTPDriver;

extern RTPDriver rtpivr;

typedef	bool (RTPTrunk::*trunkhandler_t)(TrunkEvent *event);

class RTPTrunk : private RTPSocket, private Trunk, private TimerPort
{
private:
	friend class RTPDriver;
	static char status[250];

	trunkhandler_t handler;
	time_t lastring;
	int ts;

	bool postEvent(TrunkEvent *evt);
	void putEvent(TrunkEvent *evt);
	void timerTick(void);
	void Exit(void);
	void getName(char *buffer);
	unsigned long getIdleTime(void);

	void TrunkStep(trunkstep_t step)
		{return;};

	RTPTrunk(tpport_t port, int ts);
	~RTPTrunk();
public:
	Driver *getDriver(void)
		{return (Driver*)&rtpivr;};
};

class RTPConfig : public Keydata
{
public:
	RTPConfig();

	tpport_t getFirstPort(void)
		{return atoi(getLast("first"));};

	unsigned getRTPCount(void)
		{return atoi(getLast("count"));};

	unsigned getRTPInc(void)
		{return atoi(getLast("inc"));};

	InetHostAddress getBindAddress(void);
};

class RTPDriver : public Driver, public RTPConfig
{
private:
	RTPTrunk **ports;
	int port_count;

public:
	RTPDriver();
	~RTPDriver();

	int Start(void);
	void Stop(void);

	int getTrunkCount(void)
		{return port_count;};

	Trunk *getTrunkPort(int id);
	aaScript *getScript(void);
};

