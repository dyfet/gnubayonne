// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

char RTPTrunk::status[250];

RTPTrunk::RTPTrunk(tpport_t port, int ts) :
Trunk(ts, &rtpivr), RTPSocket(rtpivr.getBindAddress(), port, keythreads.priAudio()), TimerPort()
{
	handler = NULL;
	lastring = 0;
}

RTPTrunk::~RTPTrunk()
{
	endSocket();
}

bool RTPTrunk::postEvent(TrunkEvent *event)
{
	bool rtn = true;
	trunkhandler_t prior;

	EnterMutex();

	switch(event->id)
	{
	case TRUNK_TIMER_EXPIRED:
		if(!TimerPort::getTimer())
			rtn = false;
		break;
	case TRUNK_DTMF_KEYUP:
		if(Trunk::flags.offhook)
			time(&idle);
		if(!Trunk::flags.dtmf)
			rtn = false;
		break;
	}

	if(!rtn)
	{
		LeaveMutex();
		return false;
	}

	if(!handler)
	{
		LeaveMutex();
		return false;
	}

	return true;
}

void RTPTrunk::getName(char *buffer)
{
	sprintf(buffer, "rtp(%d)", id);
}

void RTPTrunk::Exit(void)
{
	char buffer[33];
	getName(buffer);
	slog(SLOG_DEBUG) << buffer << ": script exiting" << endl;
//	handler = &RTPTrunk::hangupHandler;
}

unsigned long RTPTrunk::getIdleTime(void) 
{
        time_t now;
 

	time(&now); 
// 	if(handler == &RTPTrunk::idleHandler) 
		// return now - idle;
 
        return 0; 
}
 
void RTPTrunk::timerTick(void)
{
	TrunkEvent event;

	EnterMutex();
	if(TimerPort::getTimer())
	{
		LeaveMutex();
		return;
	}
	TimerPort::endTimer();
	event.id = TRUNK_TIMER_EXPIRED;
	postEvent(&event);
	LeaveMutex();
}
		 
     
