// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

bool DialogicTrunk::busyHandler(TrunkEvent *event)
{
	switch(event->id)
	{
	case TRUNK_ENTER_STATE:
		debug->debugState(this, "busy");
		synctimer = exittimer = 0;
		switch(Trunk::flags.trunk)
		{
		case TRUNK_MODE_OUTGOING:
			group->decOutgoing();
			break;
		case TRUNK_MODE_INCOMING:
			group->decIncoming();
		}
		Trunk::flags.trunk = TRUNK_MODE_UNAVAILABLE;
		status[tsid] = '*';
		TimerPort::endTimer();
		if(thread)
		{
			TimerPort::setTimer(thread->stop());
			return true;
		}
	case TRUNK_TIMER_EXPIRED:
		stopServices();
		Trunk::detach();
		Trunk::flags.reset = false;
		setDTMFDetect(false);
		Trunk::flags.dsp = DSP_MODE_INACTIVE;
		Trunk::flags.offhook = true;
		return true;
	case TRUNK_STOP_DISCONNECT:
	case TRUNK_LINE_WINK:
	case TRUNK_CPA_DIALTONE:
	case TRUNK_MAKE_BUSY:
	case TRUNK_MAKE_STANDBY:
		return true;
	}
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
