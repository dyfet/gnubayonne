// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne 
// with the Dialogic runtime libraries to produce a executable image
// without requiring Dialogic's sources to be supplied so long as each
// each source file so linked contains this exclusion.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

extern "C" {
	// "null" operation from our point of view
	
	long uio_seek(int fd, long offset, int wence)
	{
		return offset;
	}

	// read audio buffer

	int uio_read(int fd, char *buffer, unsigned len)
	{
		int rtn;
		int offset = 0;
		bool linear = false;
		DialogicTrunk *trunk = dialogicivr.ports[fd];
		if(!trunk)
			return -1;

                switch(trunk->getEncoding())
                {
                case Audio::pcm16Mono:
                        linear = true;
                }



		while(offset < (int)len && trunk->data.play.maxtime)
		{
			rtn = trunk->getBuffer(buffer + offset, len - offset);
			if(rtn < 0)
			{
				if(offset)
					return offset;
				return rtn;
			}
			if(rtn < (int)len - offset)
			{
				trunk->setPosition(0);
				continue;
			}
			offset += rtn;
		}
		if(offset)
			return offset;

		if(linear)
		{
			short *lbuffer = new short[len];
			rtn = trunk->getBuffer(lbuffer, len * 2) / 2;
			while(offset < rtn)
			{
				buffer[offset] = phTone::linear2ulaw(lbuffer[offset]);
				++offset;
			}
			delete[] lbuffer;			
			return rtn;
		}
		else
			return trunk->getBuffer(buffer, len);
	}

	// write audio buffer

	int uio_write(int fd, char *buffer, unsigned len)
	{
		DialogicTrunk *trunk = dialogicivr.ports[fd];
		if(!trunk)
			return -1;

		if(trunk->data.record.frames && (int)trunk->frames++ >= trunk->data.record.frames)
		{
			trunk->frames = 0;
			trunk->setPosition(0);
		}

		return trunk->putBuffer(buffer, len);
	}
};

void DialogicTrunk::endPlay(void)
{
	char buffer[12];

	stopChannel(EV_ASYNC);
	sprintf(buffer, "%ld", getTransfered());
	trunk->setSymbol(SYM_PLAYED, buffer);
	sprintf(buffer, "%ld", getPosition());
	trunk->setSymbol(SYM_OFFSET, buffer);
	URLAudio::close();
}

void DialogicTrunk::endRecord(void)
{
	char buffer[12];
	struct stat ino;
	int trim;

	stopChannel(EV_ASYNC);
	sprintf(buffer, "%ld", getPosition());
	setSymbol(SYM_OFFSET, buffer);

	if(data.record.minsize)
	{
		if(getPosition() < data.record.minsize)
		{
			setSymbol(SYM_RECORDED, "0");
			remove(data.record.name);
			URLAudio::close();
			return;
		}
	}

	trim = toBytes(getEncoding(), data.record.trim);
	stat(data.record.name, &ino);
	if(ino.st_size <= trim || data.record.frames)
	{
		setSymbol(SYM_RECORDED, "0");
		remove(data.record.name);
	}
	else
	{
		sprintf(buffer, "%ld", 
			getPosition() - data.record.trim);
		setSymbol(SYM_RECORDED, buffer);
		truncate(data.record.name, ino.st_size - trim);
		if(!data.record.append)
			chown(data.record.name, keyserver.getUid(), keyserver.getGid());
		if(data.record.save)
			rename(data.record.name, data.record.save);
	}
	URLAudio::close();
}

#ifdef CCXX_NAMESPACES
};
#endif

