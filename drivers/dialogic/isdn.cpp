// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne 
// with the Dialogic runtime libraries to produce a executable image
// without requiring Dialogic's sources to be supplied so long as each
// each source file so linked contains this exclusion.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

DialogicISDN::DialogicISDN(int dev, int span, interface_t iface)
{
	isdev = dev;
	isspan = span;
	interface = iface;
}

DialogicISDN::~DialogicISDN()
{
	if(isdev > -1)
		cc_Close(isdev);
}

void DialogicTrunk::putISDNError(const char *msg)
{
	char nbuf[32];
	char *rbuf;
	int reason;


	getName(nbuf);

	HOLD("isdn error")	
		reason = cc_CauseValue(tsdev);
		cc_ResultMsg(tsdev, reason, &rbuf);
		cc_Restart(tsdev, EV_SYNC);
	RELEASE

	slog(Slog::levelError) << nbuf << "; " << msg << rbuf << endl;
} 

#ifdef CCXX_NAMESPACES
};
#endif /* CCXX_NAMESPACES */

