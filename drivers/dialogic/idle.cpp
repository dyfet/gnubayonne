// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne 
// with the Dialogic runtime libraries to produce a executable image
// without requiring Dialogic's sources to be supplied so long as each
// each source file so linked contains this exclusion.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

void DialogicTrunk::initSyms(void)
{
	switch(interface)
	{
	case NT_ANALOG:
		setConst(SYM_INTERFACE, "analog");
		break;
	//case NT_ISDN;
	//	setConst(SYM_INTERFACE, "isdn");
	//	break;
	default:
		setConst(SYM_INTERFACE, "digital");
		break;
	}
	setConst(SYM_NETWORK, "tdm");
}

bool DialogicTrunk::releaseHandler(TrunkEvent *event)
{
	int chk;
	
	switch(event->id)
	{
	case TRUNK_ENTER_STATE:
		enterState("release");
		status[tsid] = '#';
		HOLD("release")
			chk = cc_ReleaseCallEx(crn, EV_ASYNC);
		RELEASE

		_disconnecting = false;

		if (chk < 0)
			putISDNError("cc_ReleaseCallEx");
		TimerPort::setTimer(60000);
		return true;
	case TRUNK_TIMER_EXPIRED:
		slog(Slog::levelError) << "release timeout failure" << endl;
	case TRUNK_CALL_RELEASE:
		TimerPort::endTimer();
		crn = -1;
		handler = &DialogicTrunk::idleHandler;
		return true;
	}
	return true;
}

#ifndef	CC_ADDRSIZE
#define	CC_ADDRSIZE	16
#endif

bool DialogicTrunk::idleHandler(TrunkEvent *event)
{
	char script[256];
	int argc = 0;
	const char *start = NULL;
	const char *value;
	CRN lcrn;
	unsigned chk;
	char **argv;
	Request *req;

	switch(event->id)
	{
	case TRUNK_ENTER_STATE:
		synctimer = exittimer = 0;
		if(crn != -1)
			releaseISDN();
		enterState("idle");
		setIdle(true);
		Part();
		ScriptSymbol::purge();
		Trunk::flags.dsp = DSP_MODE_INACTIVE;
		tgi.pid = 0;
		digits = 0;
		switch(Trunk::flags.trunk)
		{
		case TRUNK_MODE_OUTGOING:
			group->decOutgoing();
			break;
		case TRUNK_MODE_INCOMING:
			group->decIncoming();
		}
		Trunk::flags.trunk = TRUNK_MODE_INACTIVE;
		Trunk::flags.ready = true;
		Trunk::flags.offhook = false;
		status[tsid] = '-';
		time(&idletime);
		rings = 0;
		TimerPort::endTimer();
		value = group->chkRequest();
		chk = atoi(value);
		if(!chk && stricmp(value, "hangup"))
			return true;
		if(chk < group->getReady())
			chk = group->getReady();
		TimerPort::setTimer(chk);

		// TEMP KILL
		TimerPort::endTimer();
		
		getName(script);
		slog(Slog::levelDebug) << script << ": setup waiting" << endl;
		if(isdn)
		{
			slog(Slog::levelDebug) << script << ": isdn restart" << endl;
			HOLD("isdn restart")
			chk = cc_Restart(tsdev, EV_ASYNC);
			RELEASE
			if(chk < 0)
				putISDNError("cc_Restart");
			slog(Slog::levelDebug) << script << ": isdn waiting" << endl;
			_disconnecting = false;
			HOLD("isdn waitcall")
			chk = cc_WaitCall(tsdev, &lcrn, &wtcall, -1, EV_ASYNC);
			RELEASE
			if(chk < 0)
				putISDNError("cc_WaitCall");
		}

		return true;
	case TRUNK_LINE_WINK:
	case TRUNK_CPA_DIALTONE:
	case TRUNK_STOP_DISCONNECT:
		return true;
	case TRUNK_TIMER_EXPIRED:
		Trunk::flags.ready = true;
		req = group->getRequest();
		if(req)
		{
			setConst(SYM_PARENT, req->getParent());
			argv = req->getList();
			start = *(argv++);
			if(attach(start))
			{
				Trunk::flags.trunk = TRUNK_MODE_OUTGOING;
				setList(argv);
				TimerPort::endTimer();
				Trunk::flags.ready = false;
				handler = &DialogicTrunk::seizeHandler;
				group->incOutgoing();
				delete req;
				return true;
			}
			syncParent("start:failed");
			ScriptSymbol::purge();
			delete req;
		}
		chk = atoi(group->chkRequest());
		if(chk)
			TimerPort::setTimer(chk);
		return true;
	case TRUNK_START_SCRIPT:
		Trunk::flags.trunk = TRUNK_MODE_OUTGOING;
	case TRUNK_RING_START:
		start = event->parm.argv[0];
		if(start)
		{
			if(strchr(start, '='))
				start = group->getSchedule(script);
			else
				++argc;
		}
		if(Trunk::flags.trunk == TRUNK_MODE_INACTIVE)
			Trunk::flags.trunk = TRUNK_MODE_INCOMING;
		rings = 0;
		if(!start)
			start = group->getSchedule(script);
		if(attach(start))
		{
			setList(&event->parm.argv[argc]);
			Trunk::flags.ready = false;
			TimerPort::endTimer();
			if(Trunk::flags.trunk == TRUNK_MODE_OUTGOING)
			{
				handler = &DialogicTrunk::seizeHandler;
				group->incOutgoing();
			}
			else
			{
				syncParent("start:running");
				handler = &DialogicTrunk::stepHandler;
				group->incIncoming();
			}
			return true;
		}
		ScriptSymbol::purge();
		Trunk::flags.trunk = TRUNK_MODE_INACTIVE;
		getName(script);
		slog(Slog::levelError) << script << ": " << start << ": cannot start" << endl;
		return true;
	case TRUNK_RING_REDIRECT:
		start = group->getRedirect(event->parm.argv[0], script);
		rings = 0;
		if(attach(start))
		{
			setConst(SYM_REDIRECT, event->parm.argv[0]);
			setList(&event->parm.argv[1]);
			Trunk::flags.ready = false;
			Trunk::flags.trunk = TRUNK_MODE_INCOMING;
			TimerPort::endTimer();
			handler = &DialogicTrunk::stepHandler;
			group->incIncoming();		
			return true;
		}
		ScriptInterp::purge();
		getName(script);
		slog(Slog::levelError) << script << ": " << event->parm.argv[0] << ": unable to redirect" << endl;
		return true;
	case TRUNK_RINGING_ON:
		if(!group->getAnswer())
			return true;
		rings = group->getAnswer();
		Trunk::flags.trunk = TRUNK_MODE_INCOMING;
		group->incIncoming();
		Trunk::flags.ready = false;
		TimerPort::endTimer();
		handler = &DialogicTrunk::ringHandler;
		return true;
	case TRUNK_CALLER_ID:
		return ringHandler(event);
		return true;			
	case TRUNK_MAKE_BUSY:
	case TRUNK_MAKE_STANDBY:
		handler = &DialogicTrunk::busyHandler;
		return true;
	case TRUNK_MAKE_IDLE:
		return true;
	}
	return false;
}

#ifdef CCXX_NAMESPACES
};
#endif

