// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne
// with the Dialogic runtime libraries to produce a executable image
// without requiring Dialogic's sources to be supplied so long as each
// each source file so linked contains this exclusion.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

bool DialogicTrunk::playHandler(TrunkEvent *event)
{
	char *fn;
	struct tm *dt, tbuf;
	struct stat ino;
	char buffer[32];

	switch(event->id)
	{
	case TRUNK_STOP_STATE:
		TimerPort::endTimer();
		stopChannel(EV_ASYNC);
		_stopping_state = true;
		return true;
	case TRUNK_AUDIO_IDLE:
		TimerPort::endTimer();
		if (!_stopping_state) {
		trunkSignal(TRUNK_SIGNAL_STEP);
		}
		handler = &DialogicTrunk::stepHandler;
		endPlay();
		return true;
	case TRUNK_ENTER_STATE:
		enterState("play");
		status[tsid] = 'p';
		_stopping_state = false;
		if(!Trunk::flags.offhook && interface == NT_ANALOG)
		{
			setHook(true);
			Trunk::flags.offhook = true;
			TimerPort::setTimer(group->getPickup());
			return true;
		}
	case TRUNK_OFF_HOOK:
	case TRUNK_TIMER_EXPIRED:
		if(thread)
			return false;

		if(data.play.maxtime)
			TimerPort::setTimer(data.play.maxtime);

		TimerPort::endTimer();
		Trunk::setDTMFDetect();
		getName(buffer);
		setSymbol(SYM_PLAYED, "0");
		setSymbol(SYM_OFFSET, "0");
	
		if(tts)
			if(!tts->synth(id, &data))
			{
				trunkSignal(TRUNK_SIGNAL_ERROR);
				handler = &DialogicTrunk::stepHandler;
				return true;
			}

retry:
		fn = getPlayfile();
		if(!fn && data.play.lock)
		{
			cachelock.unlock();
			data.play.lock = false;
		}
		if(!fn && data.play.mode != PLAY_MODE_ANY)
		{
			slog(Slog::levelError) << buffer << ": no file to play" << endl;
			setSymbol(SYM_ERROR, "play-failed");
			trunkSignal(TRUNK_SIGNAL_ERROR);
		}
		if(!fn && data.play.mode == PLAY_MODE_ANY)
			trunkSignal(TRUNK_SIGNAL_STEP);
		if(!fn)
		{
			handler = &DialogicTrunk::stepHandler;
			return true;
		}
		stat(fn, &ino);
		URLAudio::close();
		open(fn);
		if(data.play.lock)
		{
			cachelock.unlock();
			data.play.lock = false;
		}
		if(!isOpen())
		{
			if(data.play.mode == PLAY_MODE_ANY || data.play.mode == PLAY_MODE_ONE)
				goto retry;
			slog(Slog::levelError) << buffer << ": " << fn << ": cannot open" << endl;
			setSymbol(SYM_ERROR, "play-failed");
			trunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &DialogicTrunk::stepHandler;
			return true;
		}
		if(data.play.mode == PLAY_MODE_TEMP)
			remove(fn);
		if(data.play.offset)
			setPosition(data.play.offset);
		if(data.play.limit)
			setLimit(data.play.limit);
		dt = localtime_r(&ino.st_ctime, &tbuf);
		sprintf(buffer, "%04d%02d%02d,%02d%02d%02d",
			dt->tm_year + 1900, dt->tm_mon + 1, dt->tm_mday,
			dt->tm_hour, dt->tm_min, dt->tm_sec);
		setSymbol(SYM_CREATED, buffer);
		fn = getAnnotation();
		if(fn)
			setSymbol(SYM_ANNOTATION, fn);
		else
			setSymbol(SYM_ANNOTATION, "");
		playAudio();	
		return true;
	}
	return false;
}

bool DialogicTrunk::playwaitHandler(TrunkEvent *event)
{
	switch(event->id)
	{
        case TRUNK_EXIT_SHELL:
                if(!tgi.pid)
                        return true;
                tgi.pid = 0;
                TimerPort::endTimer();
                if(event->parm.status)
                {
                        trunkSignal(TRUNK_SIGNAL_ERROR);
                        sprintf(buffer, "play-failed-exit-%d",
				event->parm.status);
			setSymbol(SYM_ERROR, buffer);
                        handler = &DialogicTrunk::stepHandler;
                        return true;
                }
                handler = &DialogicTrunk::playHandler;
                return true;                                                     
        case TRUNK_TIMER_EXPIRED:
                if(tgi.pid)
                {
                        ::kill(tgi.pid, SIGTERM);
                        tgi.pid = 0;
                }
                sprintf(buffer, "play-failed-timeout");
                setSymbol(SYM_ERROR, buffer);
                trunkSignal(TRUNK_SIGNAL_ERROR);
                handler = &DialogicTrunk::stepHandler;
                return true;                                                  
        case TRUNK_ENTER_STATE:
		enterState("play");
                TimerPort::endTimer();
                Trunk::setDTMFDetect();
                TimerPort::setTimer(data.play.timeout);
                return true;
        }
        return false;                                                            
}

#ifdef CCXX_NAMESPACES
};
#endif

