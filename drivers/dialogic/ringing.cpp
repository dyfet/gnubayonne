// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifndef	CC_ADDRSIZE
#define	CC_ADDRSIZE 41
#endif

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

bool DialogicTrunk::ringHandler(TrunkEvent *event)
{
	char script[256];
	char *args[2];
	char **start = NULL;
	TrunkGroup *grp = NULL;
	unsigned char cid[CC_ADDRSIZE];

	switch(event->id)
	{
	case TRUNK_CALLER_ID:
		strcpy((char *)cid, "pstn:");

		if(isdn)
		{
			if(!getISDNInfo(MCLASS_DN, cid + 5))
			{
				setConst(SYM_CALLER, (char *)cid);
				setConst(SYM_CLID, (char *)cid + 5);
			}
			if(!getISDNInfo(MCLASS_DDN, cid + 5))
			{
				setConst(SYM_DIALED, (char *)cid);
				setConst(SYM_DNID, (char *)cid + 5);
			}
			return true;
		}

		if(!getCallerId(MCLASS_DN, cid + 5))
		{
			setConst(SYM_CALLER, (char *)cid);	
			setConst(SYM_CLID, (char *)cid + 5);
		}
		if(!getCallerId(MCLASS_NAME, cid))
			setConst(SYM_NAME, (char *)cid);
		strcpy((char *)cid, "pstn:");
		if(!getCallerId(MCLASS_DDN, cid + 5))
		{
			setConst(SYM_DIALED, (char *)cid);
			setConst(SYM_DNID, (char *)cid + 5);
		}
		return true;
	case TRUNK_ENTER_STATE:
		enterState("ring");
		status[tsid] = '!';
		setRing(1);
		if(isdn && group->getAccept())
		{
			if(!acceptISDN())
			{
				TimerPort::endTimer();
				Trunk::flags.dsp = DSP_MODE_INACTIVE;
				slog(Slog::levelError) << "dx(" << id << "): error accepting" << endl;
				//trunkSignal(TRUNK_SIGNAL_HANGUP);
				handler = &DialogicTrunk::idleHandler;
			}
		}
		Trunk::flags.dsp = DSP_MODE_CALLERID;
		return true;
	case TRUNK_START_SCRIPT:
		return false;
	case TRUNK_RING_START:
		Trunk::flags.dsp = DSP_MODE_INACTIVE;
		return idleHandler(event);
	case TRUNK_RINGING_ON:
		++rings;
		TimerPort::endTimer();
		return true;
	case TRUNK_CALL_RINGING:
	case TRUNK_RINGING_OFF:
		if(rings > 1)
			Trunk::flags.dsp = DSP_MODE_INACTIVE;
		if(rings < group->getAnswer())
		{
			TimerPort::setTimer(group->getRingTime() * 1000);
			return true;
		}
		sprintf(script, "ring%d", event->parm.ring.digit);
		if(!grp)
			grp = group;
		start = getInitial(args);
		if(attach(*start))
		{
			setList(++start);
			handler = &DialogicTrunk::stepHandler;
			return true;
		}
		rings = 0;
		getName(script);
		slog(Slog::levelError) << script<< ": cannot answer call" << endl;	
		return true;
	case TRUNK_LINE_WINK:
		if(!Trunk::flags.offhook)
			return true;
	case TRUNK_STOP_DISCONNECT:
	case TRUNK_CPA_DIALTONE:
	case TRUNK_TIMER_EXPIRED:
		Trunk::flags.dsp = DSP_MODE_INACTIVE;
		handler = &DialogicTrunk::idleHandler;
		return true;
	}
	return false;
}

#ifdef CCXX_NAMESPACES
};
#endif

