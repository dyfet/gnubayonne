// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne
// with the Dialogic runtime libraries to produce a executable image
// without requiring Dialogic's sources to be supplied so long as each
// each source file so linked contains this exclusion.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#include "driver.h"

#ifdef CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

bool DialogicTrunk::toneHandler(TrunkEvent *event)
{
	switch(event->id)
	{
	case TRUNK_AUDIO_IDLE:
	case TRUNK_TONE_IDLE:
		if(--data.tone.loops)
		{
			TimerPort::setTimer(data.tone.wakeup);
			if(!data.tone.tone)
				playTone(data.tone.freq1, data.tone.freq2, data.tone.ampl1,
					 data.tone.ampl2, data.tone.duration);
			else
				playTone(getTone());
			return true;
		}
		TimerPort::endTimer();
		trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &DialogicTrunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		enterState("tone");
		status[tsid] = 't';
		_stopping_state = false;
		if(!Trunk::flags.offhook && (interface == NT_ANALOG && !isdn))
		{
			setHook(true);
			Trunk::flags.offhook = true;
			TimerPort::setTimer(group->getPickup());
			return true;
		}
		if(data.tone.wakeup)
			TimerPort::setTimer(data.tone.wakeup);

		if(!data.tone.tone)
		{
			if(!playTone(data.tone.freq1, data.tone.freq2, data.tone.ampl1,
				data.tone.ampl2, data.tone.duration))
			{
				setSymbol(SYM_ERROR, "tone-failed");
				trunkSignal(TRUNK_SIGNAL_ERROR);
				handler = &DialogicTrunk::stepHandler;
			}
		}
		else
			playTone(getTone());

		return true;
	case TRUNK_TIMER_EXPIRED:
		if(--data.tone.loops)
		{
			TimerPort::setTimer(data.tone.wakeup);
			return true;
		}
		TimerPort::endTimer();
		Trunk::setDTMFDetect();
		trunkSignal(TRUNK_SIGNAL_STEP);
		handler = &DialogicTrunk::stepHandler;
		return true;
	}
	return false;
}

#ifdef CCXX_NAMESPACES
};
#endif

