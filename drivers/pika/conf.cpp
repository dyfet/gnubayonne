// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne 
// with  the Pika MonteCarlo libraries to produce a executable image
// without requiring MonteCarlo itself to be supplied in source form so
// long as each source file so linked contains this exclusion.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#include "driver.h"

PikaMixer::PikaMixer(TResourceHandle Dsp, unsigned f) :
Mixer()
{
	hDsp = Dsp;
	alloc = 0;
	first = f;
	PK_VC_ChangeParameters(hDsp, PK_VC_GAIN_ROBUSTNESS, PK_VC_LONG_DISTANCE_ROBUSTNESS);

	members = avail = pikaivr.getConfMembers();
}

bool PikaMixer::setMixer(int grp, int count)
{
	int used = members - avail;
	bool ret = true;

	count -= used;
	if(count < 0)
		count = 0;

	EnterMutex();
	while(alloc > count)
		PK_DSP_ReleasePort(reserved[--alloc]);
	while(alloc < count && ret)
	{
		reserved[alloc] = PK_DSP_SeizePort(hDsp, PK_CF_RESOURCES);
		if(reserved[alloc])
			++alloc;
		else
			ret = false;
	}
	LeaveMutex();
	return ret;
}

Conference *PikaMixer::getConference(int id)
{
	if(id < 0 || id >= groups)
		return NULL;

	return pikaivr.getConference(id + first);
}
	
TResourceHandle PikaMixer::getResource(void)
{
	TResourceHandle hRes;
	EnterMutex();
	if(!avail)
	{
		LeaveMutex();
		return 0;
	}

	if(alloc)
	{
		hRes = reserved[--alloc];
		--avail;
		LeaveMutex();
		return hRes;
	}

	hRes = PK_DSP_SeizePort(hDsp, PK_CF_RESOURCES);
	if(hRes)
	{
		--avail;
	}
	LeaveMutex();
	return hRes;
}

void PikaMixer::endResource(TResourceHandle hRes)
{
	PK_DSP_ReleasePort(hRes);
	EnterMutex();
	++avail;
	LeaveMutex();
}

PikaConf::PikaConf(PikaMixer *mixer, TResourceHandle hGrp) :
Conference(mixer)
{
	hConf = hGrp;
}

PikaConf::~PikaConf()
{
	PK_VC_DeleteGroup(hConf);
}		

TResourceHandle PikaConf::getMember(int id)
{
	PikaMixer *mixer = getResources();
	TResourceHandle hDsp;

	EnterMutex();

	if(limit)
		if(members >= limit)
		{
			LeaveMutex();
			return 0;
		}

	hDsp = mixer->getResource();

	if(hDsp)
	{
		membership[members++] = id;
		PK_VC_AddMember(hConf, hDsp, 0, 0, PK_FALSE);
	}
	LeaveMutex();
	return hDsp;
}

void PikaConf::endMember(TResourceHandle hDsp, int id)
{
	int mem, offset = 0;
	PikaMixer *mixer = getResources();
	EnterMutex();
	for(mem = 0; mem < members; ++mem)
	{
		if(membership[mem] == id)
			continue;
		membership[mem] = membership[offset++];
	}
	PK_VC_RemoveMember(hConf, hDsp);
	mixer->endResource(hDsp);
	--members;
	LeaveMutex();
}

bool PikaConf::setConference(unsigned max)
{
	EnterMutex();
	limit = max;
	LeaveMutex();
	return true;
}

