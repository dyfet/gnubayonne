// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne 
// with  the Pika MonteCarlo libraries to produce a executable image
// without requiring MonteCarlo itself to be supplied in source form so
// long as each source file so linked contains this exclusion.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#include "driver.h"

bool PikaTrunk::sleepHandler(TrunkEvent *event)
{
	timeout_t wakeup;
	char buffer[16];
	char *cp;
	char *value;
	char *sp;

	switch(event->id)
	{
	case TRUNK_EXIT_SHELL:
		if(!tgi.pid)
			return true;
		tgi.pid = 0;
		endTimer();
		sprintf(buffer, "exit-%d", event->parm.status);
		setSymbol(SYM_ERROR, buffer);
		if(event->parm.status)
			TrunkSignal(TRUNK_SIGNAL_ERROR);
		else
			TrunkSignal(TRUNK_SIGNAL_STEP);
		handler = &PikaTrunk::stepHandler;
		return true;	
	case TRUNK_DTMF_KEYUP:
		if(tgi.pid)
		{
			kill(tgi.pid, SIGINT);
			tgi.pid = 0;
		}
		return false;
	case TRUNK_RINGING_OFF:
		if(rings > 1)
			stopDSP();
		if(!data.sleep.rings)
			return true;
		if(rings < data.sleep.rings)
			return true;
		endTimer();
	case TRUNK_SERVICE_LOOKUP:
		if(event->parm.lookup.seq != tgi.seq)
			return false;
		if(event->parm.lookup.result)
		{
			tgi.data = event->parm.lookup.data;
			cp = strtok_r(event->parm.lookup.data, "&", &sp);
			while(cp)
			{
				value = strchr(cp, '=');
				if(value)
				{
					*(value++) = 0;
					setSymbol(cp, getSymbolSize());
					setSymbol(cp, value);
				}
				cp = strtok_r(NULL, "&", &sp);
			}
			TrunkSignal(TRUNK_SIGNAL_STEP);
		}
		else
		{
			setSymbol(SYM_ERROR, event->parm.lookup.data);
			TrunkSignal(TRUNK_SIGNAL_ERROR);
		}
		handler = &PikaTrunk::stepHandler;
		return true;
	case TRUNK_TIMER_EXPIRED:
		if(--data.sleep.loops)
		{
			setTimer(data.sleep.wakeup);
			return true;
		}
		if(tgi.pid)
		{
			kill(tgi.pid, SIGALRM);
			tgi.pid = 0;
			if(!TrunkSignal(TRUNK_SIGNAL_TIMEOUT))
				TrunkSignal(TRUNK_SIGNAL_STEP);
		}
		else
			TrunkSignal(TRUNK_SIGNAL_STEP);
		handler = &PikaTrunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		debug->DebugState(this, "sleep");
		endTimer();
		Trunk::setDTMFDetect();
		wakeup = data.sleep.wakeup;
		if(data.sleep.rings)
			if(rings >= data.sleep.rings)
				wakeup = 0;

	
		if(!wakeup)
		{
			TrunkSignal(TRUNK_SIGNAL_STEP);
			handler = &PikaTrunk::stepHandler;
			return true;
		}

		setTimer(wakeup);
		return true;
	}
	return false;
}
