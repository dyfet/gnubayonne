// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne 
// with  the Pika MonteCarlo libraries to produce a executable image
// without requiring MonteCarlo itself to be supplied in source form so
// long as each source file so linked contains this exclusion.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#include "driver.h"

bool PikaTrunk::detectHandler(TrunkEvent *event)
{
        int rtn;

	switch(event->id)
	{
	case TRUNK_CPA_BUSYTONE:
		stopDSP();
		handler = &PikaTrunk::stepHandler;
		if(!TrunkSignal(TRUNK_SIGNAL_BUSY))
			TrunkSignal(TRUNK_SIGNAL_STEP);
		return true;		
	case TRUNK_CPA_FAILURE:
		stopDSP();
		handler = &PikaTrunk::stepHandler;
		if(!TrunkSignal(TRUNK_SIGNAL_CANCEL))
			TrunkSignal(TRUNK_SIGNAL_STEP);
		return true;
	case TRUNK_CPA_DIALTONE:
		stopDSP();
		handler = &PikaTrunk::stepHandler;
		if(!TrunkSignal(TRUNK_SIGNAL_DIALTONE))
			TrunkSignal(TRUNK_SIGNAL_STEP);
		return true;
	case TRUNK_CPA_RINGBACK:
		stopDSP();
		handler = &PikaTrunk::stepHandler;
		return true;
	case TRUNK_TIMER_EXPIRED:
		stopDSP();
		if(!TrunkSignal(TRUNK_SIGNAL_TIMEOUT))
			TrunkSignal(TRUNK_SIGNAL_STEP);
		handler = &PikaTrunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		debug->DebugState(this, "detect");
		if(!getCPResource())
		{
			setSymbol(SYM_ERROR, "dsp-unavailable");
			TrunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &PikaTrunk::stepHandler;
			return true;
		}
		if(!flags.offhook)
		{
			PK_TRUNK_OffHook(hTrk);
			flags.offhook = true;
		}
		setTimer(data.dialxfer.timeout);
		return true;	
	}

	return false;
}
