// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne 
// with  the Pika MonteCarlo libraries to produce a executable image
// without requiring MonteCarlo itself to be supplied in source form so
// long as each source file so linked contains this exclusion.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#include "driver.h"
#include <sys/ioctl.h>

PikaRecord::PikaRecord(PikaTrunk *trunk) :
Service((Trunk *)trunk, keythreads.priAudio()), AudioFile()
{
	stopped = false;
	reset = false;
	hDsp = trunk->getDevice();
	clrAudio();

	control = trunk->getHeaders();
	buffer = trunk->getBuffers();
	buflimit = trunk->getBufferSize();
}

PikaRecord::~PikaRecord()
{
	struct stat ino;
	char buffer[12];
	int trim;

	clrAudio();
	Terminate();
	sprintf(buffer, "%ld", getPosition());
	trunk->setSymbol(SYM_RECORDED, buffer);
	trim = tobytes(getEncoding(), atoi(trunk->getSymbol(SYM_TRIM)));
	AudioFile::Close();

	stat(data->record.name, &ino);
	truncate(data->record.name, ino.st_size - trim);
	chown(data->record.name, keyserver.getUid(), keyserver.getGid());

	if(reset)
	{
		PK_AUDIO_Reset(hDsp);
		dspReset();
	}
}

timeout_t PikaRecord::Stop(void)
{
	clrAudio();
	stopped = true;
	if(reset)
	{
		reset = false;
		PK_AUDIO_Reset(hDsp);
	}
	return 120 * pikaivr.getAudioBuffers() + keythreads.getResetDelay();
}

void PikaRecord::Initial(void)
{
	unsigned codec, speed, rate, len, count = 0;
	char buffer[32];
	char *ext = strrchr(data->record.name, '.');
	char *fmt = trunk->getSymbol(SYM_FORMAT);
	audioinfo_t recinfo;

	trunk->getName(buffer);
	recinfo.format = AUDIO_FORMAT_RAW;
	recinfo.encoding = MULAW_AUDIO_ENCODING;
	recinfo.order = 0;
	recinfo.annotation = trunk->getSymbol(SYM_ANNOTATION);
	recinfo.rate = 8000;

	if(!ext)
		ext = trunk->getSymbol(SYM_EXTENSION);

	if(!fmt)
		fmt = "raw";

	if(!stricmp(ext, ".al"))
		recinfo.encoding = ALAW_AUDIO_ENCODING;
	else if(!stricmp(ext, ".au"))
	{
		recinfo.format = AUDIO_FORMAT_SUN;
		recinfo.order = __BIG_ENDIAN;
	}
	else if(!stricmp(ext, ".wav"))
	{
		recinfo.format = AUDIO_FORMAT_RIFF;
		recinfo.order = __LITTLE_ENDIAN;
	}

	if(!stricmp(fmt, "adpcm") || !stricmp(fmt, "g721"))
		recinfo.encoding = G721_ADPCM_ENCODING;
	else if(!stricmp(fmt, "alaw"))
		recinfo.encoding = ALAW_AUDIO_ENCODING;
	else if(!stricmp(fmt, "pcm8"))
		recinfo.encoding = PCM8_AUDIO_ENCODING;
	else if(!stricmp(fmt, "pcm"))
		recinfo.encoding = PCM16_AUDIO_ENCODING;
	else if(!stricmp(fmt, "g723"))
		recinfo.encoding = G723_3BIT_ENCODING;
	
	if(data->record.append)
		Open(data->record.name);
	else
		Create(data->record.name, &recinfo);	
	if(!isOpen())
	{
		slog(SLOG_ERROR) << data->record.name << ": cannot open" << endl;
		Failure();
	}
	if(data->record.append)
		setPosition();

	rate = getSampleRate();
	switch(getEncoding())
	{
	case MULAW_AUDIO_ENCODING:
		codec = PK_COMPOUND_MU_LAW;
		rate = 8000;
		break;
	case ALAW_AUDIO_ENCODING:
		codec = PK_COMPOUND_A_LAW;
		rate = 8000;
		break;
	case G723_3BIT_ENCODING:
		codec = PK_ADPCM_3_BIT;
		rate = 8000;
		break;
	case G721_ADPCM_ENCODING:
		codec = PK_ADPCM_4_BIT;
		rate = 8000;
		break;
	case PCM8_AUDIO_ENCODING:
		codec = PK_PCM_8_BIT;
		if(!rate)
			rate = 8000;
		break;
	case PCM16_AUDIO_ENCODING:
		codec = PK_PCM_16_BIT;
		if(!rate)
			rate = 8000;
		break;
	default:
		slog(SLOG_ERROR) << buffer << ": unsupported codec required" << endl;
		Failure();
	}

	if(!rate)
		rate = (unsigned)samplerate(getEncoding());

	switch(rate)
	{
	case 8000:
	case 8012:
		speed = PK_SAMPLING_RATE_8_KHz;
		break;
	case 6000:
		speed = PK_SAMPLING_RATE_6_KHz;
		break;
	case 4000:
		speed = PK_SAMPLING_RATE_4_KHz;
		break;
	case 11025:
		speed = PK_SAMPLING_RATE_11_KHz;
		break;
	default:
		slog(SLOG_ERROR) << buffer << ": unsupported sample rate " << getSampleRate() << endl;
		Failure();
	}
	PK_AUDIO_SetFormat(hDsp, codec | speed);
	bufsize = PK_AUDIO_BufferSize(hDsp);
	samples = tosamples(getEncoding(), bufsize);
}

void PikaRecord::Run(void)
{
	int maxbuffers = pikaivr.getAudioBuffers();
	int buffers = buflimit / bufsize;
	int frame = 0;
	unsigned len = samples;
	audioerror_t status = AUDIO_SUCCESS;
	char name[33];

	reset = true;
	trunk->getName(name);

	if(buffers > maxbuffers)
		buffers = maxbuffers;

	// start

	while(frame < buffers)
	{
		control[frame].lpData = (char *)&buffer[frame * bufsize];
		control[frame].dwBufferLength = bufsize;
		control[frame].dwBytesRecorded = 0;
		control[frame].dwLoops = 0;
		control[frame].lpNext = NULL;
		control[frame].dwFlags = 0;
		PK_AUDIO_InputAddBuffer(hDsp, &control[frame++]);
	}

	setAudio();
	PK_AUDIO_InputStart(hDsp);

	frame = 0;
	while(status == AUDIO_SUCCESS && !stopped)
	{
		Wait();
		len = tosamples(getEncoding(), control[frame].dwBytesRecorded);
		if(len) 
			status = putSamples(&buffer[frame * bufsize], len);
		else
			status = AUDIO_WRITE_INCOMPLETE;
		
		if(status == AUDIO_SUCCESS)
		{
                	control[frame].lpData = (char *)&buffer[frame * bufsize];
                	control[frame].dwBufferLength = bufsize;
                	control[frame].dwBytesRecorded = 0;
                	control[frame].dwLoops = 0;
                	control[frame].lpNext = NULL;
			control[frame].dwFlags = 0;
			PK_AUDIO_InputAddBuffer(hDsp, &control[frame++]);
		}
		if(frame >= buffers)
			frame = 0;
	}

	PK_AUDIO_Reset(hDsp);
	reset = false;
	clrAudio();
	if(status != AUDIO_SUCCESS || len < 1)
	{
		slog(SLOG_ERROR) << name << ": failed recording" << endl;
		Failure();
	}
	Success();
}

bool PikaTrunk::recordHandler(TrunkEvent *event)
{
	unsigned long mask;

	switch(event->id)
	{
	case TRUNK_STOP_STATE:
		endTimer();
		stopServices();
		flags.reset = false;
		handler = &PikaTrunk::stepHandler;
		return true;
	case TRUNK_AUDIO_IDLE:
		if(!thread)
		{
			setTimer(32);
			return true;
		}
		return true;
	case TRUNK_SERVICE_SUCCESS:
//		stopServices();
		flags.reset = false;
		TrunkSignal(TRUNK_SIGNAL_STEP);
		handler = &PikaTrunk::exitHandler;
		return true;
	case TRUNK_SERVICE_FAILURE:
//		stopServices();
		flags.reset = false;
		setSymbol(SYM_ERROR, "record-failed");
		TrunkSignal(TRUNK_SIGNAL_ERROR);
		handler = &PikaTrunk::exitHandler;
		return true;
	case TRUNK_DTMF_KEYUP:
		mask = (1 << event->parm.dtmf.digit);
		if(mask & data.record.term)
		{
			TrunkSignal(TRUNK_SIGNAL_STEP);
			event->id = TRUNK_STOP_STATE;
		}
		return false;
	case TRUNK_ENTER_STATE:
		debug->DebugState(this, "record");
		status[id] = 'r';
		if(!flags.offhook)
		{
			flags.offhook = true;
			PK_TRUNK_OffHook(hTrk);
			setTimer(group->getPickup());
		}
		else
			setTimer(125);
		return true;
	case TRUNK_TIMER_EXPIRED:
		if(thread)
			return false;
	case TRUNK_OFF_HOOK:
		endTimer();
		if(!getVPResource())
		{
			setSymbol(SYM_ERROR, "dsp-unavailable");
			TrunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &PikaTrunk::stepHandler;
			return true;
		}
		if(data.record.term)
			setDTMFDetect(true);
		else
			Trunk::setDTMFDetect();
		thread = new PikaRecord(this);
		thread->Start();
		setTimer(data.record.timeout);
		return true;
	}
	return false;
}

