// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne 
// with  the Pika MonteCarlo libraries to produce a executable image
// without requiring MonteCarlo itself to be supplied in source form so
// long as each source file so linked contains this exclusion.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#include "driver.h"
#include <sys/ioctl.h>
#include <sys/stat.h>

PikaPlay::PikaPlay(PikaTrunk *trunk) :
Service((Trunk *)trunk, keythreads.priAudio()), URLAudio()
{
	reset = false;
	hDsp = trunk->getDevice();
	clrAudio();

	control = trunk->getHeaders();
	buffer = trunk->getBuffers();
	buflimit = trunk->getBufferSize();
}

PikaPlay::~PikaPlay()
{
	char buffer[12];
	clrAudio();
	Terminate();

	sprintf(buffer, "%ld", getTransfered());
	trunk->setSymbol(SYM_PLAYED, buffer);
	URLAudio::Close();
	if(reset)
	{
		PK_AUDIO_Reset(hDsp);
		dspReset();
	}
	else
		PK_AUDIO_Stop(hDsp);
}

char *PikaPlay::getContinuation(void)
{
	char *fn;

	if(data->play.mode == PLAY_MODE_ONE || 
           data->play.mode == PLAY_MODE_TEMP)
		return NULL;
retry:
	fn = getPlayfile();
	if(fn && data->play.mode == PLAY_MODE_ANY)
	{
		if(!canAccess(fn))
			goto retry;
	}
	return fn;
}

timeout_t PikaPlay::Stop(void)
{
	clrAudio();
	stopped = true;
	if(reset)
	{
		reset = false;
		PK_AUDIO_Reset(hDsp);
	}
	return 120 * pikaivr.getAudioBuffers() + keythreads.getResetDelay();
}

void PikaPlay::Initial(void)
{
	unsigned codec, speed, rate, len, count = 0;
	char buffer[32];
	char *fn;
	struct stat ino;
	char *ann;
	struct tm *dt;
	struct tm tbuf;

	trunk->getName(buffer);
retry:
	fn = getPlayfile();

	if(!fn)
	{
		if(data->play.mode == PLAY_MODE_ANY)
			Success();
		slog(SLOG_ERROR) << buffer << ": no file to play" << endl; 
		Failure();
	}

	stat(fn, &ino);
	Open(fn);	
	if(!isOpen())
	{
		if(data->play.mode == PLAY_MODE_ANY || 
		   data->play.mode == PLAY_MODE_ONE)
			goto retry;

		slog(SLOG_ERROR) << buffer << ": " << fn << ": cannot open" << endl;
		Failure();
	}

	if(data->play.mode == PLAY_MODE_TEMP)
		remove(fn);

	if(data->play.offset)
		setPosition(data->play.offset);

	if(data->play.limit)
		setLimit(data->play.limit);

	dt = localtime_r(&ino.st_ctime, &tbuf);
	sprintf(buffer, "%04d%02d%02d,%02d%02d%02d",
		dt->tm_year + 1900, dt->tm_mon + 1, dt->tm_mday,
		dt->tm_hour, dt->tm_min, dt->tm_sec);
	trunk->setSymbol(SYM_CREATED, buffer);

	ann = getAnnotation();
	if(ann)
		trunk->setSymbol(SYM_ANNOTATION, ann);
	else
		trunk->setSymbol(SYM_ANNOTATION, "");

	rate = getSampleRate();
	switch(getEncoding())
	{
	case MULAW_AUDIO_ENCODING:
		codec = PK_COMPOUND_MU_LAW;
		rate = 8000;
		break;
	case ALAW_AUDIO_ENCODING:
		codec = PK_COMPOUND_A_LAW;
		rate = 8000;
		break;
	case G723_3BIT_ENCODING:
		codec = PK_ADPCM_3_BIT;
		rate = 8000;
		break;
	case G721_ADPCM_ENCODING:
		codec = PK_ADPCM_4_BIT;
		rate = 8000;
		break;
	case PCM8_AUDIO_ENCODING:
		codec = PK_PCM_8_BIT;
		if(!rate)
			rate = 8000;
		break;
	case PCM16_AUDIO_ENCODING:
		codec = PK_PCM_16_BIT;
		if(!rate)
			rate = 8000;
		break;
	default:
		slog(SLOG_ERROR) << buffer << ": unsupported codec required" << endl;
		Failure();
	}

	if(!rate)
		rate = (unsigned)samplerate(getEncoding());

	switch(rate)
	{
	case 8000:
	case 8012:
		speed = PK_SAMPLING_RATE_8_KHz;
		break;
	case 6000:
		speed = PK_SAMPLING_RATE_6_KHz;
		break;
	case 4000:
		speed = PK_SAMPLING_RATE_4_KHz;
		break;
	case 11025:
		speed = PK_SAMPLING_RATE_11_KHz;
		break;
	default:
		slog(SLOG_ERROR) << buffer << ": unsupported sample rate " << getSampleRate() << endl;
		Failure();
	}
	PK_AUDIO_SetFormat(hDsp, codec | speed);
	bufsize = PK_AUDIO_BufferSize(hDsp);
	samples = tosamples(getEncoding(), bufsize);
}

void PikaPlay::Run(void)
{
	int maxbuffers = pikaivr.getAudioBuffers();
	int buffers = buflimit / bufsize;
	int frame = 0;
	int len = 0;
	audioerror_t status = AUDIO_SUCCESS;
	char name[33];
	int delay;

	reset = true;
	trunk->getName(name);

	if(buffers > maxbuffers)
		buffers = maxbuffers;
	
	// start

	while(!stopped && frame < buffers && status == AUDIO_SUCCESS)
	{	
		
		status = getSamples(&buffer[frame * bufsize], samples);
		control[frame].lpData = (char *)&buffer[frame * bufsize];
		control[frame].dwBufferLength = bufsize;
		control[frame].dwBytesRecorded = bufsize;
		control[frame].dwLoops = 0;
		control[frame].lpNext = NULL;
		if(status != AUDIO_READ_INCOMPLETE && status != AUDIO_SUCCESS)
			break;
		if(status == AUDIO_READ_INCOMPLETE)
			control[frame].dwFlags = PK_AUDIO_LAST_BUFFER;
		else
			control[frame].dwFlags = 0;
		PK_AUDIO_OutputAddBuffer(hDsp, &control[frame++]);		
	}
	
	delay = frame;
	if(frame > 0)
	{
		setAudio();
		PK_AUDIO_OutputStart(hDsp);
	}

	frame = 0;
	status = AUDIO_SUCCESS;
	while(status == AUDIO_SUCCESS && !stopped)
	{
		Wait();
		status = getSamples(&buffer[frame * bufsize], samples);
                control[frame].lpData = (char *)&buffer[frame * bufsize];
                control[frame].dwBufferLength = bufsize;
                control[frame].dwBytesRecorded = bufsize;
                control[frame].dwLoops = 0;
                control[frame].lpNext = NULL;

		if(status == AUDIO_SUCCESS)
			control[frame].dwFlags = 0;
		else
			control[frame].dwFlags = PK_AUDIO_LAST_BUFFER;
		if(status == AUDIO_SUCCESS || status == AUDIO_READ_INCOMPLETE)
			PK_AUDIO_OutputAddBuffer(hDsp, &control[frame++]);
		if(frame >= buffers)
			frame = 0;
	}

	reset = false;
	clrAudio();
	if(status == AUDIO_READ_FAILURE)
	{
		slog(SLOG_ERROR) << name << ": failed playback" << endl;
		Failure();
	}
	Sleep(delay * 120l + 120);
	Success();
}

bool PikaTrunk::exitHandler(TrunkEvent *event)
{
	switch(event->id)
	{
	case TRUNK_STOP_STATE:
		return playHandler(event);
	case TRUNK_ENTER_STATE:
		debug->DebugState(this, "exit");
		setTimer(20);
		return true;
	case TRUNK_TIMER_EXPIRED:
		endTimer();
		event->id = TRUNK_STOP_STATE;
		return playHandler(event);
	}
	return false;
}		

bool PikaTrunk::playHandler(TrunkEvent *event)
{
	switch(event->id)
	{
	case TRUNK_STOP_STATE:
		stopServices();
		flags.reset = false;
		endTimer();
		handler = &PikaTrunk::stepHandler;
		return true;
	case TRUNK_AUDIO_IDLE:
		if(!thread)
		{
			setTimer(32);
			return true;
		}
		return true;
	case TRUNK_SERVICE_SUCCESS:
//		stopServices();
		flags.reset = false;
		TrunkSignal(TRUNK_SIGNAL_STEP);
		handler = &PikaTrunk::exitHandler;
		return true;
	case TRUNK_SERVICE_FAILURE:
//		stopServices();
		flags.reset = false;
		setSymbol(SYM_ERROR, "play-failed");
		TrunkSignal(TRUNK_SIGNAL_ERROR);
		handler = &PikaTrunk::exitHandler;
		return true;
	case TRUNK_ENTER_STATE:
		debug->DebugState(this, "play");
		status[id] = 'p';
		if(!flags.offhook)
		{
			PK_TRUNK_OffHook(hTrk);
			flags.offhook = true;
			setTimer(group->getPickup());
		}
		else
			setTimer(125);
		return true;
	case TRUNK_OFF_HOOK:
		if(thread)
			return true;
	case TRUNK_TIMER_EXPIRED:
		endTimer();
		if(!getVPResource())
		{
			setSymbol(SYM_ERROR, "dsp-unavailable");
			TrunkSignal(TRUNK_SIGNAL_ERROR);
			handler = &PikaTrunk::stepHandler;
			return true;
		}
		Trunk::setDTMFDetect();
		thread = new PikaPlay(this);
		thread->Start();
		return true;
	}
	return false;
}

bool PikaTrunk::playwaitHandler(TrunkEvent *event)
{
	char buffer[65];

	switch(event->id)
	{
	case TRUNK_EXIT_SHELL:
		if(!tgi.pid)
			return true;
		tgi.pid = 0;
		endTimer();
		if(event->parm.status)
		{
			TrunkSignal(TRUNK_SIGNAL_ERROR);
			sprintf(buffer, "play-failed-exit-%d", 
				event->parm.status);
			setSymbol(SYM_ERROR, buffer);
			handler = &PikaTrunk::stepHandler;
			return true;
		}
		handler = &PikaTrunk::playHandler;
		return true;
	case TRUNK_TIMER_EXPIRED:
		if(tgi.pid)
		{
			kill(tgi.pid, SIGTERM);
			tgi.pid = 0;
		}
		sprintf(buffer, "play-failed-timeout");
		setSymbol(SYM_ERROR, buffer);
		TrunkSignal(TRUNK_SIGNAL_ERROR);
		handler = &PikaTrunk::stepHandler;
		return true;
	case TRUNK_ENTER_STATE:
		debug->DebugState(this, "playwait");
		endTimer();
		Trunk::setDTMFDetect();
		setTimer(data.play.timeout);
		return true;
	}
	return false;
}
		
