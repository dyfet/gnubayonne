// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne 
// with  the Pika MonteCarlo libraries to produce a executable image
// without requiring MonteCarlo itself to be supplied in source form so
// long as each source file so linked contains this exclusion.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#include "driver.h"

bool PikaTrunk::ringHandler(TrunkEvent *event)
{
	char script[256];
	char *args[2];
	int argc = 0;
	char **start = NULL;
	const char *data;
	const char *value;
	TrunkGroup *grp = NULL;
	unsigned char msgtype, msgsize, totsize;

	enum
	{
		SMT_CALLING_NUMBER_DELIVERY_INFO = 0x04,
		SMT_MESSAGE_WAITING_INDICATOR = 0x0a,
		SMT_MESSAGE_DESK_INFO = 0x0b,
		MMT_CALL_SETUP=0x80,
		MMT_TEST_CALLING_NUMBER_DELIVERY=0x81,
		MMT_MESSAGE_WAITING_NOTIFICATION=0x82
	};

	enum
	{
		MPT_TIME = 1,
		MPT_CALLING_LINE_DIRECTORY_NUMBER,
		MPT_DIALABLE_DIRECTORY_NUMBER,
		MPT_REASON_FOR_ABSENCE_OF_DIRECTORY_NUMBER,
		MPT_REASON_FOR_REDIRECTION,
		MPT_CALL_QUALIFIER,
		MPT_NAME,
		MPT_REASON_FOR_ABSENCE_OF_NAME
	};	

	switch(event->id)
	{
	case TRUNK_DSP_READY:
		PK_CID_Disable(hDsp);
		return true;
	case TRUNK_ENTER_STATE:
		debug->DebugState(this, "ringing");
		status[id] = 'r';
		if(group->getAnswer() > 1)
			getCIDResource();
		return true;
	case TRUNK_START_SCRIPT:
		return false;
	case TRUNK_RING_START:
		stopDSP();
		return idleHandler(event);
	case TRUNK_RINGING_ON:
		++rings;
		endTimer();
		return true;
	case TRUNK_CALLER_ID:
		data = (const char *)event->parm.data;
		msgtype = data[0];
		msgsize = totsize = data[1];
		data += 2;
		switch(msgtype)
		{
		case SMT_CALLING_NUMBER_DELIVERY_INFO:
			break;
		case MMT_CALL_SETUP:
			while(totsize)
			{
				msgtype = data[0];
				msgsize = data[1];
				if(msgsize > totsize)
					msgsize = totsize;
				data += 2;
				switch(msgtype)
				{
				case MPT_REASON_FOR_ABSENCE_OF_DIRECTORY_NUMBER:
				case MPT_CALLING_LINE_DIRECTORY_NUMBER:
					strcpy(script, "pstn:");
					memcpy(script + 5, data, msgsize);
					script[msgsize + 5] = 0;
					setConstant(SYM_CLID, script + 5);
					setConstant(SYM_CALLER, script);
					break;
				case MPT_REASON_FOR_ABSENCE_OF_NAME:
				case MPT_NAME: 
					memcpy(script, data, msgsize);
					script[msgsize] = 0;
					setConstant(SYM_NAME, script);
				}
				data += msgsize;
				totsize -= (msgsize + 2);
			}
			break;
		default:
			slog(SLOG_DEBUG) << "unsupported cid entity" << endl;
			break;
		}
		stopDSP();
		if(rings < group->getAnswer())
			return true;
	case TRUNK_RINGING_OFF:
		if(rings == 1 && flags.dsp == DSP_MODE_CALLERID)
		{
			if(PK_CID_Enable(hDsp))
				slog(SLOG_DEBUG) << pikaivr.getCardName(card) << id << ": callerid activation failed" << endl;
		} 
		if(rings == group->getAnswer() && flags.dsp == DSP_MODE_CALLERID)
		{
			setTimer(group->getRingTime() * 1000);
			return true;
		}
		if(rings < group->getAnswer())
		{
			setTimer(group->getRingTime() * 1000);
			return true;
		}
		sprintf(script, "ring%d", event->parm.ring.digit);
		if(!grp)
			grp = group;
		start = getInitial(args);
		if(Attach(*start))
		{
			setList(++start);
			handler = &PikaTrunk::stepHandler;
			return true;
		}
		rings = 0;
		slog(SLOG_ERROR) << pikaivr.getCardName(card) << id <<": cannot answer call" << endl;	
		return true;
	case TRUNK_LINE_WINK:
		if(!flags.offhook)
			return true;
	case TRUNK_CPA_DIALTONE:
	case TRUNK_TIMER_EXPIRED:
	case TRUNK_STOP_DISCONNECT:
		stopDSP();
		handler = &PikaTrunk::idleHandler;
		return true;
	}
	return false;
}
