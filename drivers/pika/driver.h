// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne 
// with  the Pika MonteCarlo libraries to produce a executable image
// without requiring MonteCarlo itself to be supplied in source form so
// long as each source file so linked contains this exclusion.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#include <ivrconfig.h>
#include <server.h>
#include <cc++/audio.h>
#include <cc++/strchar.h>

#ifdef	have_montecarlo_h
#include <montecarlo.h>
#else
#ifdef	HAVE_MONTECARLO_H
#include <MonteCarlo.h>
#endif
#endif

#define	PK_VP_RESOURCES (PK_TONE_DETECTION | PK_TONE_GENERATION)
#define	PK_CP_RESOURCES (PK_TONE_DETECTION | PK_TONE_GENERATION)
#define PK_CF_RESOURCES (PK_DTMF_DETECTION | PK_VOICE_CONFERENCE)
#define USE_PK_CALL
#define	MAX_DSP_DEVICES	2
#define DSP
#define FAX
#define DRIVER_TYPE "PIKA"

#include <pk_mvip.h>
#include <pk_misc.h>
#include <pk_dsp.h>
#include <pk_vp.h>
#include <pk_vc.h>
#include <pk_cid.h>
#include <pk_fax.h>

class PikaTrunk;
class PikaDriver;

extern PikaDriver pikaivr;

typedef enum
{
	TRUNK_GS_NTI = PK_DID_PORT,
	TRUNK_LS_NTI = PK_LS_PORT,
	TRUNK_EM_NTI = PK_EM_PORT
} trunknti_t;

typedef struct
{
	TDeviceHandle dsp[MAX_DSP_DEVICES];
} trunkdsp_t;

typedef	bool (PikaTrunk::*trunkhandler_t)(TrunkEvent *event);
typedef void PK_CALLBACK(*pikacall_t)(PK_U32, PK_U32, PK_U32, PK_U32);

class PikaTrunk : private TimerPort, private Trunk
{
private:
	friend class PikaDriver;
	friend class PikaConf;

	static char *status;
	static void callback(PK_U32 id, PK_U32 event, PK_U32 p1, PK_U32 p2);

	PikaConf *conf;
	PikaTrunk *joined;
	TBufferHeader hTone, *headers;
	TResourceHandle hTrk, hDsp, hDsp1, hTimer, hPat;
	unsigned char *buffers;
	unsigned bufsize;
	trunknti_t nti;
	trunkhandler_t handler;
	int card;

	struct
	{
		int count;
		char bin[33];
	} did;

	unsigned char cidbuf[PK_CID_BUFFER_SIZE];

	PikaTrunk(TResourceHandle hRes, trunknti_t nti, int card, int ts);
	~PikaTrunk();

	bool duplexHandler(TrunkEvent *event);
	bool hangupHandler(TrunkEvent *event);
	bool idleHandler(TrunkEvent *event);
	bool busyHandler(TrunkEvent *event);
	bool stepHandler(TrunkEvent *event);
	bool ringHandler(TrunkEvent *event);
	bool siezeHandler(TrunkEvent *event);
	bool detectHandler(TrunkEvent *event);
	bool sleepHandler(TrunkEvent *event);
	bool answerHandler(TrunkEvent *event);
	bool collectHandler(TrunkEvent *event);
	bool flashoffHandler(TrunkEvent *event);
	bool flashonHandler(TrunkEvent *event);
	bool playHandler(TrunkEvent *event);
	bool exitHandler(TrunkEvent *event);
	bool playwaitHandler(TrunkEvent *event);
	bool resetHandler(TrunkEvent *event);
	bool recordHandler(TrunkEvent *event);
	bool loadHandler(TrunkEvent *event);
	bool dialHandler(TrunkEvent *event);
	bool toneHandler(TrunkEvent *event);
	bool joinHandler(TrunkEvent *event);

	bool scrJoin(void);
	bool scrWait(void);
	bool scrConf(void);
	bool scrLeave(void);
	bool scrDetect(void);
	bool scrDuplex(void);

	void setTimer(timeout_t timeout);
	void incTimer(timeout_t timeout);
	void endTimer(void);
	void setDTMFDetect(bool flag);
	bool getDupResource(void);
	bool getVPResource(void);
	bool getCPResource(void);
	bool getCFResource(int id);
	bool getCIDResource(void);
	void disjoin(void);
	void stopDSP(void);
	void getName(char *buffer);
	void TrunkStep(trunkstep_t);
	bool postEvent(TrunkEvent *);
	unsigned long getIdleTime(void);
	void Exit(void);

public:
	inline TResourceHandle getDevice(void)
		{return hDsp;};

	inline TResourceHandle getAltDevice(void)
		{return hDsp1;};

	inline TBufferHeader *getHeaders(void)
		{return headers;};

	inline unsigned char *getBuffers(void)
		{return buffers;};

	inline unsigned getBufferSize(void)
		{return bufsize;};

	Driver *getDriver(void)
		{return (Driver*)&pikaivr;};
};

class PikaRecord : private AudioFile, public Service
{
private:
	TResourceHandle hDsp;
	bool reset;
	size_t bufsize;
	unsigned samples;
	TBufferHeader *control;
	unsigned char *buffer;
	unsigned buflimit;

	void Initial(void);
	void Run(void);

public:
	timeout_t Stop(void);
	PikaRecord(PikaTrunk *trunk);
	~PikaRecord();
};

class PikaPlay : private URLAudio, public Service
{
private:
	TResourceHandle hDsp;
	bool reset;
	size_t bufsize;
	unsigned samples;
	TBufferHeader *control;
	unsigned char *buffer;
	unsigned buflimit;

	void Initial(void);
	void Run(void);
	char *getContinuation(void);

	void Final(void)
		{return;};

public:
	timeout_t Stop(void);
	PikaPlay(PikaTrunk *trunk);
	~PikaPlay();
};

class PikaDuplex : private AudioFile, public Service
{
private:
	TResourceHandle hDsp, hOut;
	bool reset;
	size_t bufsize;
	unsigned samples;
	TBufferHeader *control;
	unsigned char *buffer;
	unsigned buflimit;	
	AudioFile record;

	audioerror_t playAudio(int &frame, int buffers);
	audioerror_t recAudio(int &frame, int buffers);
	void Initial(void);
	void Run(void);
	char *getContinuation(void);

public:
	timeout_t Stop(void);
	PikaDuplex(PikaTrunk *trunk);
	~PikaDuplex();
};

class PikaConfig : public Keydata
{
public:
	PikaConfig();

	inline unsigned getAudioBuffers(void)
		{return atoi(getLast("buffers"));};

	inline unsigned getRingDebounce(void)
		{return atoi(getLast("ringdebounce"));};

	inline unsigned getConfMembers(void)
		{return atoi(getLast("members"));};

	inline unsigned getMixerGroups(void)
		{return atoi(getLast("groups"));};
};

class PikaMixer : public Mixer
{
private:
	TResourceHandle hDsp;
	TResourceHandle reserved[32];
	unsigned alloc;
	unsigned first;

public:
	PikaMixer(TResourceHandle Dsp, unsigned f);
	TResourceHandle getResource(void);
	void endResource(TResourceHandle hRes);
	bool setMixer(int group, int count);
	Conference *getConference(int group);

	inline void setGroups(int grp)
		{groups = grp;};
};

class PikaConf : public Conference
{
private:
	TResourceHandle hConf;

	PikaConf(PikaMixer *mixer, TResourceHandle hGrp);
	~PikaConf();

	friend class PikaDriver;
public:
	TResourceHandle getMember(int id);
	void endMember(TResourceHandle hRes, int id);
	PikaMixer *getResources(void)
		{return (PikaMixer *)mixer;};
	bool setConference(unsigned limit);
};		

class PikaDriver : public Driver, public PikaConfig
{
private:
	static char *names[];

	PikaTrunk **ports;
	PikaConf **conf;
	PikaMixer **mixer;
	trunkdsp_t *dsps;
	TResourceHandle cpTable;

	bool keyflag;
	unsigned mixer_count;
	unsigned conf_count;
	unsigned card_count;
	unsigned port_count;
	unsigned dsp_count;

	void Run(void) {return;};
	void OnSignal(int signo) {return;};

public:
	PikaDriver();
	~PikaDriver();

	void callback(void);
	int Start(void);
	void Stop(void);

	TResourceHandle getCPTable(void)
		{return cpTable;};

	int getTrunkCount(void)
		{return port_count;};

	char *getCardName(int card);
	
	TResourceHandle getDSPResource(unsigned card, unsigned mask);
	Trunk *getTrunkPort(int id);
	Conference *getConference(int id);
	bool isReserved(int id);
	Mixer *getMixer(int id);
	aaScript *getScript(void);

	unsigned getMixers(void)
		{return mixer_count;};

	unsigned getGroups(void)
		{return conf_count;};
};

