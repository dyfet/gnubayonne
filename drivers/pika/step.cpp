// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne 
// with  the Pika MonteCarlo libraries to produce a executable image
// without requiring MonteCarlo itself to be supplied in source form so
// long as each source file so linked contains this exclusion.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#include "driver.h"

void PikaTrunk::TrunkStep(trunkstep_t step)
{
	TrunkEvent event;
	
	if(handler != &PikaTrunk::stepHandler)
		return;

	event.id = TRUNK_MAKE_STEP;
	event.parm.step = step;
	stepHandler(&event);
}

bool PikaTrunk::stepHandler(TrunkEvent *event)
{
	switch(event->id)
	{
	case TRUNK_MAKE_STEP:
		if(idleHangup())
			return true;
		switch(event->parm.step)
		{
		case TRUNK_STEP_SLEEP:
			handler = &PikaTrunk::sleepHandler;
			return true;
		case TRUNK_STEP_HANGUP:
			handler = &PikaTrunk::hangupHandler;
			return true;
		case TRUNK_STEP_ANSWER:
			handler = &PikaTrunk::answerHandler;
			return true;
		case TRUNK_STEP_COLLECT:
			handler = &PikaTrunk::collectHandler;
			return true;
		case TRUNK_STEP_FLASH:
			handler = &PikaTrunk::flashonHandler;
			return true;
		case TRUNK_STEP_PLAYWAIT:
			handler = &PikaTrunk::playwaitHandler;
			return true;
		case TRUNK_STEP_PLAY:
			handler = &PikaTrunk::playHandler;
			return true;
		case TRUNK_STEP_RECORD:
			handler = &PikaTrunk::recordHandler;
			return true;
		case TRUNK_STEP_DIALXFER:
			handler = &PikaTrunk::dialHandler;
			return true;
		case TRUNK_STEP_JOIN:
			handler = &PikaTrunk::joinHandler;
			return true;
		case TRUNK_STEP_TONE:
			handler = &PikaTrunk::toneHandler;
			return true;
		case TRUNK_STEP_DETECT:
			handler = &PikaTrunk::detectHandler;
			return true;
		case TRUNK_STEP_DUPLEX:
			handler = &PikaTrunk::duplexHandler;
			return true;
		case TRUNK_STEP_LOADER:
			handler = &PikaTrunk::loadHandler;
			return true;
		}
		return false;
	case TRUNK_SERVICE_SUCCESS:
	case TRUNK_SERVICE_FAILURE:
		endTimer();
		setTimer(keythreads.getResetDelay());
		return true;
	case TRUNK_STOP_STATE:
	case TRUNK_AUDIO_IDLE:
	case TRUNK_TIMER_EXPIRED:
		endTimer();
		flags.reset = false;
		stopServices();
	case TRUNK_ENTER_STATE:
		if(flags.offhook)
		{
			if(flags.trunk == TRUNK_MODE_OUTGOING)
				status[id] = 'o';
			else
				status[id] = 'i';
		}
		else
			status[id] = 'a';
		endTimer();

		// if a dsp reset has occured, then we add a delay
		// for the dsp to settle before stepping
		if(flags.reset || thread)
		{
			debug->DebugState(this, "reset");
			if(thread)
				setTimer(thread->Stop());
			else
				setTimer(keythreads.getResetDelay());
			return true;
		}
		debug->DebugStep(this, getScript());
		// step and delay if still in same handler
		Trunk::setDTMFDetect();
		ScriptStep();

		if(handler == &PikaTrunk::stepHandler)
			setTimer(keythreads.getStepDelay());

		return true;
	}
	return false;
}

