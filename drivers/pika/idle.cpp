// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// As a special exception to the GNU General Public License, permission is
// granted for additional uses of the text contained in its release
// of Bayonne as noted here.
//
// This exception is that permission is hereby granted to link Bayonne 
// with  the Pika MonteCarlo libraries to produce a executable image
// without requiring MonteCarlo itself to be supplied in source form so
// long as each source file so linked contains this exclusion.
//
// This exception does not however invalidate any other reasons why
// the resulting executable file might be covered by the GNU General
// public license or invalidate the licensing requirements of any
// other component or library.
//
// This exception applies only to the code released by OST under the
// name Bayonne.  If you copy code from other releases into a copy of
// Bayonne, as the General Public License permits, the exception does not
// apply to the code that you add in this way.  To avoid misleading
// anyone as to the status of such modified files, you must delete
// this exception notice from them.
//
// If you write modifications of your own to Bayonne, it is your choice
// whether to permit this exception to apply to your modifications.
// If you do not wish that, delete this exception notice, at which
// point the terms of your modification would be covered under the GPL
// as explicitly stated in "COPYING".

#include "driver.h"
#include <sys/ioctl.h>

bool PikaTrunk::idleHandler(TrunkEvent *event)
{
	char script[256];
	int argc = 0;
	const char *start = NULL;
	const char *data;
	const char *value;
	TrunkGroup *grp = NULL;
	char *sp;
	unsigned chk;
	Request *req;
	char **argv;

	switch(event->id)
	{
	case TRUNK_ENTER_STATE:
		debug->DebugState(this, "idle");
		ScriptSymbol::Purge();
		disjoin();
		stopDSP();
		endTimer();

		digits = 0;
		flags.ready = false;
		flags.offhook = false;
		PK_TRUNK_OnHook(hTrk);

		switch(flags.trunk)
		{
		case TRUNK_MODE_OUTGOING:
			group->decOutgoing();
			break;
		case TRUNK_MODE_INCOMING:
			group->decIncoming();
		}
		tgi.pid = 0;
		flags.trunk = TRUNK_MODE_INACTIVE;
		flags.audio = false;
		status[id] = '-';
		time(&idle);
		Purge();
		rings = 0;
		value = group->chkRequest();
		if(!atoi(value) && stricmp(value, "hangup"))
			return true;
		chk = atoi(value);
		if(chk < group->getReady())
			chk = group->getReady();
		setTimer(chk);
		return true;		
	case TRUNK_STOP_DISCONNECT:
	case TRUNK_LINE_WINK:
	case TRUNK_CPA_DIALTONE:
		return true;
	case TRUNK_MAKE_BUSY:
		endTimer();
		flags.ready = false;
		handler = &PikaTrunk::busyHandler;
		return true;
	case TRUNK_MAKE_IDLE:
		return true;
	case TRUNK_TIMER_EXPIRED:
		flags.ready = true;
		req = group->getRequest();
		if(req)
		{
			argv = req->getList();
			start = *(argv++);
			if(Attach(start))
			{
				flags.trunk = TRUNK_MODE_OUTGOING;
				setList(argv);
				endTimer();
				flags.ready = false;
				handler = &PikaTrunk::siezeHandler;
				group->incOutgoing();
				delete req;
				return true;
			}
			delete req;
		}
		chk = atoi(group->chkRequest());
		if(chk)
			setTimer(chk);
		return true;
	case TRUNK_START_SCRIPT:
		flags.trunk = TRUNK_MODE_OUTGOING;
		start = event->parm.argv[0];
		++argc;
		
		if(Attach(start))
		{
			while(event->parm.argv[argc])
			{
				strcpy(script, event->parm.argv[argc++]);
				data = strtok_r(script, "=", &sp);
				value = strtok_r(NULL, "=", &sp);
				if(data && value)
					setConstant(data, value);
			}		
		
		  endTimer();
		  flags.ready = false;
		  handler = &PikaTrunk::siezeHandler;
		  group->incOutgoing();
			
		  return true;
		}

		flags.trunk = TRUNK_MODE_INACTIVE;
		slog(SLOG_ERROR) << pikaivr.getCardName(card) << id << ": " << start << ": cannot start" << endl;
		return true;		
// Matt B
	case TRUNK_RING_REDIRECT:
		start = group->getRedirect(event->parm.argv[0], script);
		rings = 0;
		if(Attach(start))
		{
			setConstant(SYM_REDIRECT, event->parm.argv[0]);
			setList(&event->parm.argv[1]);
			flags.ready = false;
			flags.trunk = TRUNK_MODE_INCOMING;
			endTimer();
			handler = &PikaTrunk::stepHandler;
			group->incIncoming();		
			return true;
		}
		slog(SLOG_ERROR) << pikaivr.getCardName(card) << id << ": " << event->parm.argv[0] << ": unable to redirect" << endl;
		return true;
	case TRUNK_RING_START:
		start = event->parm.argv[0];
		if(start)
		{
			if(strchr(start, '='))
				start = group->getSchedule(script);
			else
				++argc;
		}
		rings = 0;
		flags.trunk = TRUNK_MODE_INCOMING;
		if(!start)
			start = group->getSchedule(script);

		if(Attach(start))
		{
			while(event->parm.argv[argc])
			{
				strcpy(script, event->parm.argv[argc++]);
				data = strtok_r(script, "=", &sp);
				value = strtok_r(NULL, "=", &sp);
				if(data && value)
					setConstant(data, value);
			}
		 	flags.ready = false;
			endTimer();		
			handler = &PikaTrunk::stepHandler;
			group->incIncoming();
			return true;
		}
		flags.trunk = TRUNK_MODE_INACTIVE;
		slog(SLOG_ERROR) << pikaivr.getCardName(card) << id << ": " << start << ": cannot start" << endl;
		return true;		
	case TRUNK_RINGING_ON:
		if(!group->getAnswer())
			return true;
		rings = 1;
		group->incIncoming();
		flags.ready = false;
		endTimer();
		handler = &PikaTrunk::ringHandler;
		return true;
	}
	return false;
}
