// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <ivrconfig.h>
#include <server.h>
#include <cc++/audio.h>
#include <cc++/serial.h>

#ifdef	CCXX_NAMESPACES
namespace ost {
#endif

#define	MAX_CAPACITY	64

class ModemTrunk;
class ModemDriver;

extern ModemDriver modemivr;

typedef bool (ModemTrunk::*trunkhandler_t)(TrunkEvent *event);

class ModemConfig : public Keydata
{
public:
	ModemConfig();

	inline const char *getDevices(void)
		{return getLast("devices");};

	inline const char *getInitial(void)
		{return getLast("initial");};

	inline const char *getConfigData(const char *str)
		{return getLast(str);};

	unsigned getStack(void);
};

class ModemDriver : public Driver, public ModemConfig
{
private:
	ModemTrunk *ports[MAX_CAPACITY];
	unsigned port_count;

public:
	ModemDriver();
	~ModemDriver();

	int start(void);
	void stop(void);

	unsigned getTrunkCount(void)
		{return port_count;};

	Trunk *getTrunkPort(int id);
	aaScript *getScript(void);
};

class ModemTrunk : private TimerPort, private Trunk, private TTYSession,
	private AudioFile, private AudioService, private Keydata
{
private:
	friend class ModemDriver;

	trunkhandler_t handler;
	time_t lastring;
	int dev;
	int evbuf[2];
	const char *devname;

	void initSyms(void)
		{return;};

	char *getConfigName(const char *str);
	const char *getConfigData(const char *str);
	bool postEvent(TrunkEvent *event) {return false;};
	void putEvent(TrunkEvent *evt);
	void run(void);
	void exit(void);
	void trunkStep(trunkstep_t step) {return;};
	void getName(char *buffer);
//	void setDTMFDetect(bool flag);
	unsigned long getIdleTime(void);

	char *atCommand(const char *cmd);

	ModemTrunk(int id, const char *devname);
	~ModemTrunk();
public:
	Driver *getDriver(void)
		{return (Driver*)&modemivr;};

	char *getName(void)
		{return "Modem";};
};

#ifdef	CCXX_NAMESPACES
};
#endif
