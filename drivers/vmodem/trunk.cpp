// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

ModemTrunk::ModemTrunk(int id, const char *fname) :
TTYSession(fname, 0, modemivr.getStack()), Trunk(id, &modemivr), TimerPort(),
Keydata(getConfigName(fname)), AudioService(), AudioFile()
{
	long opts;

	trunk = (Trunk *)this;
	handler = NULL;
	lastring = 0;
	devname = strrchr(fname, '/') + 1;

	pipe(evbuf);
	opts = fcntl(evbuf[1], F_GETFL);
	fcntl(evbuf[1], F_SETFL, opts | O_NONBLOCK);

	setSpeed(atol(getConfigData("speed")));
	setFlowControl(flowHard);
	setStopBits(1);
	setCharBits(8);
	setParity(parityNone);
	atCommand(getConfigData("init"));
}

ModemTrunk::~ModemTrunk()
{
	terminate();
	toggleDTR(1000);
	::close(evbuf[0]);
	::close(evbuf[1]);
	endSerial();
}
	
char *ModemTrunk::getConfigName(const char *name)
{
	static char buffer[65];
	const char *cp = strrchr(name, '/');

	if(cp)
		++cp;
	else
		cp = name;

	strcpy(buffer, "/bayonne/");
	strcat(buffer, cp);
	return buffer;
}	

const char *ModemTrunk::getConfigData(const char *str)
{
	const char *cp = Keydata::getLast(str);
	if(!cp)
		cp = modemivr.getConfigData(str);
	return cp;
}

unsigned long ModemTrunk::getIdleTime(void)
{
	return 0;
}

char *ModemTrunk::atCommand(const char *optcmd)
{
	char result[65];
	char *cp, *lp;

	if(!optcmd)
		return "NO COMMAND";

	slog(Slog::levelDebug) << "AT COMMAND=" << optcmd << endl;
	flushInput();
	setLineInput('\r', '\n');
	*this << "AT";

	if(!strnicmp(optcmd, "AT", 2))
		optcmd += 2;

	while(*optcmd)
	{
		switch(*optcmd)
		{
		case '<':
			if(!stricmp(optcmd, "<DLE>"))
			{
				optcmd += 5;
				*this << '\020';
			}
			else if(!stricmp(optcmd, "<ETX>"))
			{
				optcmd += 5;
				*this << '\003';
			}
			else if(!stricmp(optcmd, "<DC2>"))
			{
				optcmd += 5;
				*this << '\022';
			}
			else if(!stricmp(optcmd, "<DC4>"))
			{
				optcmd += 5;
				*this << '\024';
			}
			else if(!stricmp(optcmd, "<CAN>"))
			{
				optcmd += 5;
				*this << '\030';
			}
			else if(!stricmp(optcmd, "<ESC>"))
			{
				optcmd += 5;
				*this << '\033';
			}
			else if(!stricmp(optcmd, "<SUB>"))
			{
				optcmd += 5;
				*this << '\032';
			}
			else
				*this << *(optcmd++);
			break;
		default:
			*this << *(optcmd++);
		}
	}

	*this << '\r' << endl;

	flushOutput();
	for(;;)
	{
		if(!isPending(pendingInput, 3000))
			return "TIMEOUT";
		getline(result, 64);
		slog(Slog::levelDebug) << "AT RESULT=" << result << endl;
		lp = result + strlen(result) - 1;
		while(*lp == '\r' || *lp == '\n')
		{
			*(lp--) = 0;
			if(lp < result)
				break;
		}
		cp = result;
		while(*cp == '\r' || *cp == '\n')
			++cp;
	
		if(!strnicmp(cp, "at", 2))
			continue;

		if(*cp)
			break;
	}
	slog(Slog::levelDebug) << "AT RETURNS=" << cp << endl;
	return cp;
}

void ModemTrunk::getName(char *buffer)
{
	sprintf(buffer, "modem/%d", id);
}

void ModemTrunk::putEvent(TrunkEvent *evt)
{
	char buf[sizeof(TrunkEvent)];
	TrunkEvent null;

	if(!evt && isThread())
		return;

	if(!evt)
	{
		null.id = TRUNK_NULL_EVENT;
		evt = &null;
	}
	memcpy(buf, evt, sizeof(TrunkEvent));
	if(::write(evbuf[1], &buf, sizeof(buf)) != sizeof(buf))
		slog(Slog::levelError) << "vmodem(" << devname << "): event overflow" << endl;
}

void ModemTrunk::exit(void)
{
	char buffer[33];
	getName(buffer);
	slog(Slog::levelDebug) << buffer << ": script exiting" << endl;
// 	handler = ...
}

void ModemTrunk::run(void)
{
	Thread::sleep(TIMEOUT_INF);
}

#ifdef	CCXX_NAMESPACES
};
#endif
