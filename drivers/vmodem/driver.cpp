// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "driver.h"

#ifdef	CCXX_NAMESPACES
using namespace std;
namespace ost {
#endif

ModemConfig::ModemConfig() :
Keydata("/bayonne/vmodem")
{
	static Keydata::Define defkeys[] = {
	{"devices", "ttyS1"},
	{"init", "ATZ"},
	{"speed", "115200"},
	{NULL, NULL}};

	if(isFHS())
		load("/drivers/vmodem");

	load(defkeys);
}

unsigned ModemConfig::getStack(void)
{
	const char *cp = getLast("stack");

	if(!cp)
		return keythreads.getStack();

	return atoi(cp);
}

ModemDriver::ModemDriver() :
Driver()
{
	port_count = 0;
	char buffer[256];
	char devname[65];
	char *cp, *dn, *tok;
	int fd;

	groups = new TrunkGroup *[MAX_CAPACITY];
	strcpy(buffer, modemivr.getDevices());
	cp = strtok_r(buffer, " \t\n:,", &tok);

	memset(ports, 0, sizeof(ports));

	while(cp && port_count < MAX_CAPACITY)
	{
		if(*cp == '/')
			devname[0] = 0;
		else
			strcpy(devname, "/dev/");
		strcat(devname, cp);

		dn = strdup(strrchr(devname, '/') + 1);
		cp = strtok_r(NULL, " \t\n:,", &tok);

		slog(Slog::levelDebug) << "vmodem: opening " << dn << endl;
		fd = open(devname, O_RDWR | O_NONBLOCK);
		if(fd < 0)
		{
			slog(Slog::levelError) << "vmodem: " << dn;
			slog() << ": cannot access" << endl;
			continue;
		}
		::close(fd);
		ports[port_count] = new ModemTrunk(port_count, devname);
		++port_count;
	}

	if(groups)
		memset(groups, 0, sizeof(TrunkGroup *) * port_count);

	slog(Slog::levelInfo) << "Voice modem driver loaded; capacity=" << port_count << endl;
}

ModemDriver::~ModemDriver()
{
	stop();

	if(groups)
		delete[] groups;
}

int ModemDriver::start(void)
{
	unsigned port = 0;

	if(active)
	{
		slog(Slog::levelError) << "driver already started" << endl;
		return 0;
	}

	slog(Slog::levelInfo) << "driver starting..." << endl;
	for(port = 0; port < port_count; ++port)
		ports[port]->start();

	active = true;
	return port;
}

void ModemDriver::stop(void)
{
	unsigned port;

	if(!active)
		return;

	for(port = 0; port < port_count; ++port)
		ports[port]->terminate();


	if(ports)
		memset(ports, 0, sizeof(ModemTrunk *) * port_count);

	active = false;
	slog(Slog::levelInfo) << "driver stopping..." << endl;
}

Trunk *ModemDriver::getTrunkPort(int id)
{
	if(id < 0 || id >= (int)port_count)
		return NULL;

	if(!ports)
		return NULL;

	return (Trunk *)ports[id];
}

ModemDriver modemivr;

#ifdef	CCXX_NAMESPACES
};
#endif
