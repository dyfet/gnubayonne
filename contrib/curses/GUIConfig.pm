#!/usr/local/bin/perl 


################################################################
#
# Variable Definition
#
# All parameters that are used throughout the Bayonne Curses Gui
# are defined here. The scripts will call class methods to
# access the information defined in the variables below.
#
################################################################


package GUIConfig;

use strict;

# Set the Client Workstation IP information
my $client_ip = '10.1.1.150';

################################################################

sub new 
{
        my $class = shift;
        return bless {}, $class;
}

################################################################

sub local_ip 
{
        return $client_ip;
}

1;

