#!/usr/local/bin/perl -w

#  Bayonne Multi-Threaded Server Component
#  (Meant to be used with the Bayonne Curses Gui)
#  
#  Copyright (C) 2000, 2001, 2002 by
#
#  Damian Kohlfeld - <damian@kohlfeld.com>
#
#  This file is distributed for use with Bayonne, and may freely be distributed with Bayonne.
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or   
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of 
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  

use strict;
use IO::Socket;
use Carp;
use IO::Select;   
use IO::Handle;
use GUIConfig;

my $GUIConfig = new GUIConfig();


# Host configuration
#
# This is the only line you need to modify.  Simple change the IP to the ip of the interface you'll
# receive Bayonne and Syslog Packets on.
#
#Local Address To Bind To.
my $LOCALIP = $GUIConfig->local_ip();


#
#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#
# YOU DO NOT NEED TO EDIT BELOW THIS LINE #
#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#
#


my $EOL = "\015\012";

sub spawn;  # forward declaration
sub logmsg { print "$0 $$: @_ at ", scalar localtime, "\n" }

my $server = IO::Socket::INET->new  
(
        LocalHost=>"$LOCALIP",
        LocalPort=>'7000',
        Proto=>'udp'
)  or  die("Could not create socket $!\n");
my $selectserver = IO::Select->new($server) || die "select failure: $!";
$server->autoflush(1);

my $server2 = IO::Socket::INET->new  
(
        LocalHost=>"$LOCALIP",
        LocalPort=>'514',
        Proto=>'udp'
)  or  die("Could not create socket $!\n");
my $selectserver2 = IO::Select->new($server) || die "select failure: $!";
$server2->autoflush(1);

my $sender = IO::Socket::INET->new
(
        PeerAddr=>'127.0.0.1',
        PeerPort=>'7020',
        Proto=>'udp'
)  or  die("Could not create socket $!\n");

my $selectsender = IO::Select->new($sender) || die "select failure: $!";
$sender->autoflush(1);

my $waitedpid = 0;
my $paddr;

use POSIX ":sys_wait_h";
sub REAPER 
{
    my $child;
    while (($waitedpid = waitpid(-1,WNOHANG)) > 0) {
        logmsg "reaped $waitedpid" . ($? ? " with exit $?" : '');
    }
    $SIG{CHLD} = \&REAPER;  # loathe sysV
}

$SIG{CHLD} = \&REAPER;

&ConnectServer1;

&ConnectServer2;

while(1) 
{
	print "Process Continuation...\n";
	sleep 5;
}

sub ConnectServer1
{
    print "Beginning UDP Listen on...\n";	

    spawn sub 
    {
        $|=1;
	my $buffer;
	while(1)
	{
		$server->recv($buffer,1024,0);
        	print $buffer."\n";
		if($selectsender->can_write(5)) {
			syswrite($sender,$buffer."\n");
		}

    	}
    };

}

sub ConnectServer2
{
    print "Beginning UDP Listen on...\n";	

    spawn sub 
    {
        $|=1;
	my $buffer;
	while(1)
	{
		$server2->recv($buffer,1024,0);
        	print $buffer."\n";
		if($selectsender->can_write(5)) {
			syswrite($sender,$buffer."\n");
		}

    	}
    };

}

sub spawn 
{
    my $coderef = shift;

    unless (@_ == 0 && $coderef && ref($coderef) eq 'CODE') {
        confess "usage: spawn CODEREF";
    }

    my $pid;
    if (!defined($pid = fork)) 
    {
        logmsg "cannot fork: $!";
        return;
    } 
    elsif ($pid) 
    {
        logmsg "begin $pid";
        return; # I'm the parent
    }
    print "Beginning client connection...\n";
    exit &$coderef();
}


