#!/usr/local/bin/perl -w

#  Bayonne Curses Gui
#  
#  Copyright (C) 2000, 2001, 2002 by
#
#  Damian Kohlfeld - <damian@kohlfeld.com>
#
#  This file is distributed for use with Bayonne, and may freely be distributed with Bayonne.
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

$SIG{INT} = \&Signal_INT_Trap;
$SIG{PIPE} = \&Signal_PIPE_Trap;

use strict;
use Curses;
use IO::Socket;
use IO::Select;
use Socket;
use IO::Handle;
use GUIConfig;


my $GUIConfig = new GUIConfig();


# Host configuration
#
# This is the only line you need to modify.  Simple change the IP to the ip of the interface you'll
# receive Bayonne and Syslog Packets on.
#
#Local Address To Bind To.
my $LOCALIP = $GUIConfig->local_ip();

#
#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#
# YOU DO NOT NEED TO EDIT BELOW THIS LINE #
#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#
#

#
#  Channels Configuration
#
#  Edit the lines below to format the three columns of data.  By default, 92 channels are watched.
#  31 channels in the first and second columns and 30 channels in the third column.  I recommend
#  you leave this as is since the program is intelligent enough to realize this. 
my $C1_CHANNELS = 31;
my $C2_CHANNELS = 31;
my $C3_CHANNELS = 30;

socketpair(CHILD,PARENT,AF_UNIX, SOCK_STREAM, PF_UNSPEC)
	or die("Socketpair creation failed $!\n");

CHILD->autoflush(1);
PARENT->autoflush(1);

my $monitor = new Curses;
initscr;
start_color;

init_pair 1, COLOR_GREEN,   COLOR_BLACK;
init_pair 2, COLOR_RED,     COLOR_BLACK;
init_pair 3, COLOR_YELLOW,  COLOR_BLACK;
init_pair 4, COLOR_BLUE,    COLOR_BLACK;
init_pair 5, COLOR_MAGENTA, COLOR_BLACK;
init_pair 6, COLOR_CYAN,    COLOR_BLACK;
init_pair 7, COLOR_BLACK,   COLOR_WHITE;

$monitor->standout();

my $char = 32|COLOR_PAIR(3)|A_REVERSE;

$monitor->attron(COLOR_PAIR(2));

$monitor->addstr(0,0,'                                 BAYONNE    Real-Time                                      ');
$monitor->addstr(1,0,'                              IVR System Status Monitor                                    ');
$monitor->standend();


$monitor->addstr(4,5,'Channel:');
my $cur_line = 5;
$monitor->attron(COLOR_PAIR(6));

for(my $count=0;$count<$C1_CHANNELS;++$count) {
	$monitor->addstr($cur_line,5,'dx('.$count.')');
	++$cur_line;
}

$cur_line = 5; for(my $count=$C1_CHANNELS;$count<($C2_CHANNELS+$C1_CHANNELS);++$count) {
	$monitor->addstr($cur_line,35,'dx('.$count.')');
	++$cur_line;
}

$cur_line = 5;
for(my $count=($C1_CHANNELS+$C2_CHANNELS);$count<($C1_CHANNELS+$C2_CHANNELS+$C3_CHANNELS);++$count) {
	$monitor->addstr($cur_line,65,'dx('.$count.')');
	++$cur_line;
}
	

$monitor->refresh();

system "stty cbreak </dev/tty >&1";

my $server = IO::Socket::INET->new
(
        LocalHost=>'127.0.0.1',
        LocalPort=>'7020',
        Proto=>'udp'
)  or  die("Could not create socket $!\n");

my $select = IO::Select->new($server) || die "select failure: $!";
my $bcast = sockaddr_in("7000",inet_aton($LOCALIP));
$server->autoflush(1);

my $toggle = 1;
my $logpos = 1;

my @array2;
my @status_array;
my @time_array;

for(my $i=0;$i<100;$i++)
{
	push(@array2,"\n");
	push(@status_array,"\n");
	push(@time_array,"\n");
}
 


while ($select->can_read(15))
{
	my $child_pid;
	my $buff;
	my $rport;
	my $rhost;
	my $raddr;
	my @array;
	my $tempoutput;
	my $channels;
	my $length;
	my $countbytes = 0;
	my $devbytes = 0;

        unless (defined($child_pid = fork())) { die "can not fork ERROR\n" };

        if ($child_pid)
        {
                while ($select->can_read(15))
                {
                        my $remote = $server->recv($buff,1024,0);
			$monitor->refresh();
                        ($rport,$raddr) = sockaddr_in($remote);
                        $rhost = gethostbyaddr($raddr,AF_INET);

			unless ($buff =~ /\*MON/)
			{
				WriteLog($buff,"testdata");
				next;
			}

			$tempoutput=join(",", unpack "c*",$buff);
			@array = split(",",$tempoutput);
			$buff="";

			$countbytes = 0;
			$devbytes = 0;
		        foreach(@array)
		        {
				#Look for the first non-printable and start capturing/filtering after that
				unless ($countbytes > "26")
				{
					++$countbytes;
				}
				else
				{
					#This clears in the misc. bytes in the MON Packet, non-printables
 	                		unless (($_ == "0")	)
         	        		{
							
						$devbytes++;
                 	        		$buff.=pack("c*",$_);
                			}
				}
        		}

			#  TIMING:
			#  This section populates the timing array with the time in seconds a channel has been in it's current state
			#  If the channel becomes "locked" in a state and is not answering calls, this will show since the state
			#  of the channel will not change.

			my @temp_array = @status_array;
			for(my $i=0;($i<($C1_CHANNELS+$C2_CHANNELS+$C3_CHANNELS)) && ($i < length($buff));++$i) 
			{
				$status_array[$i] = substr($buff,$i,1);

				unless ($status_array[$i] =~ m/[a-z]|-/) 
				{
					$time_array[$i] = 0;
					next;
				}

				unless ($temp_array[$i] eq $status_array[$i])
				{
					$time_array[$i] = time();
				}
			}

			if($toggle == 1)
			{
				$monitor->standout;
				$monitor->addstr(40,5,'PACKET');
				$monitor->standend;
				$toggle = $toggle * -1;
			}
			else
			{
				$monitor->addstr(40,5,'PACKET');
				$toggle = $toggle * -1;
			}

			$channels = $buff;
			$length = length($channels);		

			# WriteChannels
			#
			# Now that we have the line status, go ahead and write the status to the screen in a friendly format
			#

			WriteChannels(13,0,$C1_CHANNELS,$channels,$length);
			WriteChannels(43,$C1_CHANNELS,($C2_CHANNELS+$C1_CHANNELS),$channels,$length);
			WriteChannels(73,($C1_CHANNELS+$C2_CHANNELS),($C1_CHANNELS+$C2_CHANNELS+$C3_CHANNELS),$channels,$length);

			$monitor->move(37,5);
			$monitor->refresh();

                }
        }
        else
        {
                while (($char = getc) ne 'q')
                {
			
			$monitor->addstr(37,5,"Command is : $char");
			$monitor->refresh();
                }
                kill INT => $child_pid;
                exit(0);
        }
}

# WriteChannels
#
# This function draws the channel status for each channel character in the MON packet.
# 


sub WriteChannels
{	
	
	my ($column,$countstart,$countend,$localchannels,$locallength) = @_;
	my $ch_status = '';
	my $cur_line = 5;

	for(my $count=$countstart;$count<$countend;++$count) 
	{
		if($count < $locallength) 
		{
			$ch_status = substr($localchannels,$count,1);
			if($ch_status eq '-') 
			{
				$monitor->addstr($cur_line,$column,'ONHOOK ');
			}
			else
			{
				$monitor->standout;
				$monitor->attron(COLOR_PAIR(5));
				if($ch_status eq 'p')
				{
					$monitor->addstr($cur_line,$column,'PLAY   ');
				}
                                else
				{
					if($ch_status eq 'i')                                        
					{
        	                           	$monitor->addstr($cur_line,$column,'IDLE   ');
                                       	}
					else
					{
                                       		if($ch_status eq 'r')
			                        {
                         		                $monitor->addstr($cur_line,$column,'RECORD ');
                                      		}
						else
						{
	                                       		if($ch_status eq 'h')
				                        {
                	        		                       $monitor->addstr($cur_line,$column,'HANGUP ');
                        	               		}
						}

					}
				}
				$monitor->attroff(COLOR_PAIR(5));
				$monitor->standend;
			}

			if($time_array[$count])
			{
				$monitor->addstr($cur_line,($column+9),(time()-$time_array[$count]).'  ');
			}
			++$cur_line;
		}

	}

}


# WriteLog
#
# The WriteLog function prints the syslog data from the Bayonne server.
# To enable this to work you must run edit the syslog.conf file on the
# Bayonne server and make a line entry like:
# *.crit	@ip-of-this-machine
#
# Then use a call like:
# 	slog.crit "Call terminated with duration of %session.duration"
# In your script.  This will cause the UDP packet to be forwarded and 
# displayed via this program, through the multithreaded server.

sub WriteLog
{
	my ($buffer,$data) = @_;
	my @temparray;
	for(my $i=1;$i<100;$i++) 
	{
		$temparray[$i] = $array2[$i-1];
	}
	$temparray[0] = $buffer;
	
	@array2 = @temparray;
	for(my $i=0;$i<15;$i++) 
	{
		$monitor->attron(COLOR_PAIR(3));
		$monitor->addstr((42+$i),5,"LogData: ");
		$monitor->attroff(COLOR_PAIR(3));
		$monitor->attron(COLOR_PAIR(7));
		$monitor->addstr((42+$i),14,"$array2[$i]");
		$monitor->attroff(COLOR_PAIR(7));

	}
	$monitor->refresh();
}


sleep 2;
endwin;

exit(0);

sub Signal_INT_Trap
{
        my $signame = shift;
	`reset && clear`;
        print "Interrupt Signal sent is a SIG$signame";
        exit(0);
}

sub Signal_PIPE_Trap
{
        my $signame = shift;
        print "Signal sent is a SIG$signame";
        return;
}

