%define _release 1
%define _version 1.3snapshot-20030914
%define _dsopath %{_libdir}/bayonne/%{_version}

Summary: bayonne - the telephony server of the GNU project
Name: bayonne
Version: %{_version}
Release: %{_release}
Group: Communications
Source: ftp://www.voxilla.org/pub/bayonne/bayonne-%{PACKAGE_VERSION}.tar.gz
Prefix: %{_prefix}
Vendor: Open Source Telecom
Copyright: GPL
BuildRoot: %{_tmppath}/bayonne-root
Packager: David Sugar <dyfet@ostel.com>
url: http://www.bayonne.cx
BuildRequires: CommonC++ >= 1.4.0, ccscript >= 1.5.1, ccaudio, ccrtp

%description
The bayonne streams subsystem provides a version of the bayonne server
build for streams support and assembly of streams dependent telephony
driver plugins.

%package streams
Summary: Bayonne server with LiS streams support.
Group: communications
Provides: bayonne

%description streams
Bayonne server for use with streams based telephony drivers.

%package dialogic
Summary: Dialogic SDK telephony plugin.
Group: communications
Requires: bayonne bayonne-streams

%description dialogic
Dialogic telephony SDK plugin driver for Bayonne.  This plugin supports
the Unix SDK and those cards under it that support voice resources.

%prep
rm -rf $RPM_BUILD_ROOT

%setup

./configure --prefix=%{_prefix}

%build
(cd server ; make bayonne)
(cd drivers/dialogic ; make )

%install
mkdir -p $RPM_BUILD_ROOT/%{_sbindir}
cp -a server/bayonne $RPM_BUILD_ROOT/%{_sbindir}
mkdir -p $RPM_BUILD_ROOT/%{_dsopath}
cp -a drivers/dialogic/dialogic.ivr $RPM_BUILD_ROOT/%{_dsopath}

%files streams
%defattr(-,root,root)
%{_sbindir}/bayonne
%dir %{_dsopath}

%files dialogic
%defattr(-,root,root)
%{_dsopath}/dialogic.ivr

%clean
rm -rf $RPM_BUILD_DIR/bayonne-%{_version}
rm -rf $RPM_BUILD_ROOT
