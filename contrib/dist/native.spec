%define _release 5
%define _version 1.3snapshot-20030914
%define _dsopath %{_libdir}/bayonne/%{_version}

Summary: bayonne - the telephony server of the GNU project
Name: bayonne
Version: %{_version}
Release: %{_release}
Group: Communications
Source: ftp://www.voxilla.org/pub/bayonne/bayonne-%{PACKAGE_VERSION}.tar.gz
Prefix: %{_prefix}
Vendor: Open Source Telecom
Copyright: GPL
BuildRoot: %{_tmppath}/bayonne-root
Packager: David Sugar <dyfet@ostel.com>
url: http://www.bayonne.cx
BuildRequires: CommonC++ >= 1.4.0, ccscript >= 1.5.1, ccaudio, ccrtp

%description
Bayonne offers a script programmable modular server for deploying
multi-line telephony voice response and computer telephony applications.

%package common
Summary: Common programs, config files, scripts, etc.
Group: Communications
Requires: bayonne

%package native
Summary: Bayonne server image built for native telephony drivers.
Group: Communications
Provides: bayonne

%package pika
Summary: Bayonne telephony plugin for Pika MonteCarlo API.
Group: Communications
Requires: bayonne

%package vpb
Summary: Bayonne voicetronix plugin
Group: Communications
Requires: bayonne

%package UsEngM
Summary: voice library for US English Male speaker
Group: Communications
Requires: bayonne-common

%package FrenchM
Summary: international voice library for French
Group: Communications
Requires: bayonne-common

%package FrenchF
Summary: international voice library for French
Group: Communications
Requires: bayonne-common

%package devel
Summary: bayonne bindings and libs for development use
Group: Communications

%description native
Bayonne server image for use with native drivers that do not require
LiS stream support.  This package also supports native linux
kernel and GNU/Linux telephony interfaces such as "/dev/phone" and vmodem
which have no special API library requirements, and rtp/sip support.

%description pika
Bayonne Pika plugin module to support the Pika MonteCalro API under
GNU/Linux.

%description vpb
Bayonne plugin to support Voicetronix telephony cards such as the VPB4.
You will also need to install the voicetronix api package.

%description common
Plugins, config files, modules, and supporting utilities for Bayonne.  This
includes most core services which do not depend on streams or non-streams
specific linking.

%description devel
This provides the bayonne header file and the sunrpc bindings needed for
building Bayonne modules and applications to remotely call Bayonne
services.

%description UsEngM
The US English "male" speaker library offers default voice prompts
for the Bayonne telephony server.

%description FrenchM
The French international "male" speaker library offers default French 
language voice prompts for the Bayonne telephony server.  Contributed
by Wilane Ousmane.

%description FrenchF
The French international "female" speaker library offers default French
language voice prompts for the Bayonne telephony server.  Contributed
by Mariza Nicole.

%prep
rm -rf $RPM_BUILD_ROOT

%setup

# NOTE: in Common C++, compiler options are retreived from config.def
# and should not be overriden here!

./configure --prefix=%{_prefix} --without-streams

%build
uname -a|grep SMP && make -j 2 || make

%install
mkdir -p $RPM_BUILD_ROOT/var/bayonne
mkdir -p $RPM_BUILD_ROOT/etc/rc.d/init.d
mkdir -p $RPM_BUILD_ROOT/%{_prefix}/bin
mkdir -p $RPM_BUILD_ROOT/%{_mandir}/man3
make prefix=$RPM_BUILD_ROOT/%{_prefix} etc_prefix=$RPM_BUILD_ROOT/etc \
	mandir=$RPM_BUILD_ROOT/%{_mandir} etc_suffix="" install
make prefix=$RPM_BUILD_ROOT/%{_prefix} \
	mandir=$RPM_BUILD_ROOT/%{_mandir} man

%files native
%defattr(-,root,root)
%{_sbindir}/bayonne
%dir %{_dsopath}
%{_dsopath}/vmodem.ivr
%{_dsopath}/phonedev.ivr
%{_dsopath}/rtp.ivr
%{_dsopath}/dummy.ivr

%files pika
%defattr(-,root,root)
%{_dsopath}/pika.ivr

%files vpb
%defattr(-,root,root)
%{_dsopath}/vpb.ivr

%files common
%defattr(-,root,root)
%doc AUTHORS COPYING NEWS README TODO ChangeLog BAYONNE.FAQ 
%doc doc/manual.*
%config %attr(0600,bayonne,bayonne) /etc/bayonne.conf
%config %attr(0600,bayonne,bayonne) /etc/bayonne.sched
%dir %{_datadir}/aaprompts
/etc/rc.d/init.d/bayonne
%{_mandir}/man8/bayonne.8*
%{_prefix}/libexec/tgi
%{_dsopath}/*.cfg
%{_dsopath}/*.xml
%{_dsopath}/*.tgi
%{_dsopath}/*.fun
%{_dsopath}/*.tts
%{_dsopath}/*.dbg
%{_dsopath}/*.svc
%{_dsopath}/*.mod
%{_datadir}/aascripts
%{_prefix}/sbin/bayonne_policy
%{_prefix}/sbin/bayonne_apache
%{_prefix}/sbin/bayonne_setup
%{_prefix}/sbin/bayonne_admin
%{_prefix}/bin/bayonne_install
%{_prefix}/bin/bayonne_control
%{_prefix}/bin/bayonne_down
%{_prefix}/bin/bayonne_start
%{_prefix}/bin/bayonne_status
%{_prefix}/bin/bayonne_update
%{_datadir}/aaprompts/sys
%attr(06755,root,root) %{_prefix}/bin/bayonne_wrapper
%attr(0640,bayonne,bayonne) /var/bayonne

%files devel
%doc doc/html
%{_prefix}/include/cc++/bayonne.h
%{_prefix}/include/rpcsvc/bayonne.x
%{_prefix}/include/rpcsvc/bayonne.h
%{_libdir}/libbayonnerpc.*
%{_mandir}/man3/*3cc*

%files UsEngM
%defattr(-,root,root)
%{_datadir}/aaprompts/UsEngM

%files FrenchM
%defattr(-,root,root)
%{_datadir}/aaprompts/FrenchM

%files FrenchF
%defattr(-,root,root)
%{_datadir}/aaprompts/FrenchF

%clean
rm -rf $RPM_BUILD_ROOT

%pre
grep '^bayonne:' /etc/passwd >/dev/null || /usr/sbin/adduser -r bayonne

%post
/sbin/chkconfig bayonne reset
if test -d /etc/httpd ; then /usr/sbin/bayonne_apache ; fi

%preun
/etc/rc.d/init.d/bayonne stop
/sbin/chkconfig -level 0123456 bayonne off

%changelog
* Sat Apr 21 2001 David Sugar <dyfet@ostel.com> 0.5-5
- merged driver and bayonne package to simplify install.
- bayonne manual

* Tue Jul 25 2000 David Sugar <dyfet@ostel.com> 0.4-3
- added changes suggested by Lenny Cartier <lenny@mandrakesoft.com>.
- made into a relocatable package.

* Wed Mar 27 2000 David Sugar <dyfet@ostel.com> 0.5-4
- added userid management to create Bayonne user.
- changed config file ownership to bayonne.

