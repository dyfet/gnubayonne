#!/bin/sh
# Shell script to detect the presence of Dialogic/Intel *PCI* (only) cards, both
# Springware and DM3, based purely on their PCI device IDs.
# For each type of board found, a separate C program will be called which 
# will prompt the user for configuration specifics and create a working
# config file - dialogic.cfg and/or pyramid.scd
#
# So, Off to the races.........
#
# Variables
DM3=0
SW=0

echo -e "Linux Installation Wizard for Dialogic cards......\n\n"

# Let's look for Springware boards first......
echo -e "Searching for Springware boards......"
SW=`lspci -n | grep '12[cC]7:' | wc -l`
if [ ${SW} -gt 0 ]
then
echo -e "Found ${SW} Springware board(s).....\n\n"
fi

# Now, Let's look for DM3 boards ......
echo -e "Searching for DM3 boards......"
DM3=`lspci -n | grep '10[Bb]5:9054' | wc -l`
if [ ${DM3} -gt 0 ]
then
echo -e "Found ${DM3} DM3 board(s).....\n\n"

# Need to make sure the driver module is loaded.....
lsmod | grep mercd >/dev/null 2>&1
	if [ $? -ne 0 ] # Drivers not loaded!!!!
	then
		if [ ! -f /lib/modules/`uname -r`/misc/*mercd* ]
		then
		echo -e "WARNING!! The DM3 drivers are not yet installed!"
		echo -e "Please install the DM3 drivers and re-run this program."
		exit 1
		fi
	echo -e "Loading DM3 drivers. Please wait....."
	/usr/dialogic/bin/drvload >/dev/null 2>&1
	fi
fi

# Failsafe.....
if [ ${SW} -eq 0 -a ${DM3} -eq 0 ]
then
echo -e "ERROR!! NO Springware OR DM3 boards found! Please check your"
echo -e "        **            **"
echo -e "hardware configuration....Exiting.\n\n"
fi

# Stop the service
echo -e "Stopping the Dialogic Service. Please wait.......\n\n"
/usr/dialogic/bin/dlstop > /dev/null 2>&1

# If we found SW, let's configure.......
if [ ${SW} -gt 0 ]
then
./dlconfig
fi

# If we found DM3, let's configure.......
if [ ${DM3} -gt 0 ]
then
./dm3config
fi

# OK, we should be able to restart the service now
echo -e "Starting the Dialogic Service. Please wait.......\n\n"
/usr/dialogic/bin/dlstart > /dev/null 2>&1

echo -e "Dialogic Service should now be running.....Goodbye.\n\n"


