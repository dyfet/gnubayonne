
/**********************************************************************************
** Copyright (c) 2003 Gerry Gilmore and Eric Lange
** This file is part of DLCONFIG.

    DLCONFIG is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    DLCONFIG is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DLCONFIG; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


***********************************************************************************/
// dlconfig - Program to easily configure Springware family cards under Linux
// This program only requires: curses and that the Intel Dialogic Linux 
// drivers for SR 5.1++ have been installed and configured correctly
//
// Once all of the info has been collected, the program builds what should be
// a working, valid dialogic.cfg file in /usr/dialogic/cfg.
//
// Anyway, here's the code!!!!
//

// Normal includes
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <curses.h>
#include <term.h>
#include <signal.h>
#include <string.h>

// Global defines
#define NUMBRDS 16
// Screen positions
#define BOARD 2
#define SLOT 7
#define MODEL 11
#define NAME 21
// #define MEDIA 36
#define CONFED 41
#define FRONT 53

// This is the overall per-board struct. It's pretty big, but with a 16
// board limit, we won't run out of memory........
typedef struct board {
int brdnum;			// Logical board number
int slot;			// Position (Thumbwheel setting)
int model;			// ModelType - Determines EVERYTHING
int brdtype;			// Board Type-0=Span;1=Resource;2=Analog;3=Fax
				// 4=MSI/other(BRI,etc.)
int fereq;			// NI front-end? 0=No; 1=Yes
int conflag;			// Configed yet?????
int pcmtype;			// 0 = MULAW, 1 = ALAW
int dualspan;			// 0 = Single span; 1=Dual-Span
char name[20];			// Name string for the board
// char filename[40];		// Common name for PCD/FCD file specs
// char mload[4];			// Media Load
char feproto[20];		// Front End protocols (4ess, etc.) 
// char ipaddr[20];		// IP address (if IPLink)
// char subnet[20];		// subnet mask (if IPLink)
// char target[40];		// NIC name   (if IPLink)
// char host_ipaddr[20];		// IP address of host (if IPLink)
// char host_name[40];		// Host sys name   (if IPLink)
// char user_name[40];		// Valid user on sys  (if IPLink) default=root
// char gate_ipaddr[20];		// IP address of gateway (if IPLink) 
} brdinfo ;

// List of currently supported FE protocols
char const *fetypes[] =
{
"CAS - Robbed-bit",
"4ess - ISDN PRI ",
"5ess - ISDN PRI ",
"ntt - ISDN PRI  ",
"dms - ISDN PRI  ",
"ne1 - ISDN PRI (Network)",
"nt1 - ISDN PRI (Network)",
"ni2 - ISDN PRI  ",
"qsig - ISDN PRI ",
"ctr4 - Euro-ISDN PRI ",
"dass2 - BT-ISDN PRI ",
NULL};

char const **feptr;		// Pointer to fetypes

// List of currently supported media loads
// char const *mltypes[] =
// {
// "1 - Basic Voice (Play/Rec/CPA)",
// "2 - Enhanced (Basic+CSP+SCR)",
// "6 - Ultra (Enhanced+TrueSpeech)",
// "9b - Conferencing Only",
// "10 - Enhanced+Conferencing",
// NULL};

// char const **mlptr;		// Pointer to mltypes

struct board binfo[NUMBRDS];
struct board *biptr;

// Some global variables
char buff[80];
char per_board[80];
char *cptr, *tmptr;
int offset = 4;			// Start 4 lines down from top of screen
int brd =0;
int numbrds =0;
char prname[10];		// Protocol name (4ESS, etc. from fetypes)

FILE *popen(), *brdlst;

WINDOW *mwin, *pwin, *lwin;	// Various windows for prompts, etc.

// Function prototypes
void disp_all_brds(void);	
void disp_brd(int brd);
int bld_binfo(void);
void scr_brite(WINDOW *win);
void scr_norm(WINDOW *win);
void config_brd(struct board *biptr);
void config_span_brd(struct board *biptr);
int get_fe_info(struct board *biptr);
int get_brd_id(struct board *biptr);
int save_info(void);

// Main entry point
int main(argc, argv)
int argc;
char argv[];
{
int key;
int rc;
char buff2[80];

// First, let's make sure that we're running in a sane (non-X) environment
// by seeing if the DISPLAY variable is set.
// disp = getenv("DISPLAY");
// if (disp != NULL)
	// {
	// printf("\n\tWARNING!! This program MUST be run from a \n");
	// printf("\n\tconsole screen - not from an X terminal!!!! \n");
	// printf("\n\tExiting..... \n\n");
	// exit(1);
	// }

biptr = binfo;

// Clear our buffers
memset(per_board, 0, 80);
memset(buff, 0, 80);
memset(buff2, 0, 80);

sprintf(buff,"/sbin/lspci -n|grep '12[cC]7'|cut -d':' -d' ' -f4|cut -c6-9");
brdlst = popen(buff,"r");
fgets(per_board, 78, brdlst);
if (strlen(per_board) == 0)	// Drivers loaded??
	{
	printf("\n\tWARNING!! NO PCI Springware cards found!! Exiting!!\n\n");
	exit(1);
	}
pclose(brdlst);			// Clean up.....


// Clear our buffers (again)
memset(per_board, 0, 80);
memset(buff, 0, 80);

// OK, drivers loaded. Proceed.....
printf("\n\n\tGetting board info. Please wait............");
fflush(stdout);

rc = bld_binfo();		// Build up the device structs
				// The initial setup
if( rc == -1)
	{
	printf("\n\n\tWARNING!! No Springware Boards Found! Exiting!! \n");
	exit(1);
	} 

initscr();			// Initialize curses
nonl();
start_color();
mwin = newwin(23,80,0,0);
lwin = newwin(1,80,23,0);

// For those who don't like my default color selections, here's where to
// change them..............
init_pair(1,COLOR_WHITE,COLOR_BLUE);
init_pair(2,COLOR_WHITE,COLOR_RED);
init_pair(3,COLOR_BLUE,COLOR_WHITE);
init_pair(4,COLOR_YELLOW,COLOR_BLUE);

wattrset(mwin,COLOR_PAIR(1));
werase(mwin);

wattrset(lwin,COLOR_PAIR(3));
werase(lwin);

wbkgd(mwin, COLOR_PAIR(1));		// Can be OR'ed with A_BOLD
wbkgd(lwin, COLOR_PAIR(3));		// Can be OR'ed with A_BOLD

// Setup main window
wattrset(mwin,COLOR_PAIR(1)|A_BOLD|A_UNDERLINE);
mvwaddstr(mwin,1,1,"                 Intel - Communications Products Group");
mvwaddstr(mwin,2,1,"                  Linux Springware Configuration Tool ");
wmove(mwin, 3, 0);
hline(0,80);
box(mwin,0,0);

wattrset(mwin,COLOR_PAIR(1)|A_BOLD|A_UNDERLINE);
mvwaddstr(mwin,4,1,"Brd  BLT  PCI-ID       Name         Configured Y/N     Front-End");

wattrset(mwin,COLOR_PAIR(1)|A_BOLD);

// Setup label window
mvwaddstr(lwin,0,1," Arrow-up/down   C - ConfigureBoard  S - SaveConfig  Q - Quit");
wattrset(lwin,COLOR_PAIR(2)|A_BOLD);
mvwaddch(lwin,0,18,'C'); mvwaddch(lwin,0,38,'S'); mvwaddch(lwin,0,54,'Q');
wrefresh(lwin);

disp_all_brds();	// Show all the boards found

scr_brite(mwin);
brd = 1;
disp_brd(brd);		// Show 1st board first
wrefresh(mwin);
wrefresh(lwin);
while (1)		// This is the loop we keep coming back to
{
noecho();
raw();
flushinp();
scrollok(mwin,TRUE);
keypad(mwin, TRUE);
disp_brd(brd);		// Display the currently selected board
touchwin(mwin);
wrefresh(mwin);
wrefresh(lwin);
key = wgetch(mwin);
switch (key)
	{
	case KEY_DOWN:
			scr_norm(mwin);
			disp_brd(brd);
			if (brd == (numbrds -1)) // At bottom of list?
				brd = 1;	// Go to top
			else
				brd++;
			scr_brite(mwin);
			disp_brd(brd);
			wrefresh(mwin);
			wrefresh(lwin);
			break;
	case KEY_UP:
			scr_norm(mwin);
			disp_brd(brd);
			if (brd == 1)		// AT top of list?
				brd = numbrds -1;
			else
				brd--;
			scr_brite(mwin);
			disp_brd(brd);
			wrefresh(mwin);
			wrefresh(lwin);
			break;

	case 'c':	// "C" or Enter will start config of the currently
	case 'C':	// selected board
	case KEY_ENTER:
	case '\n':
	case '\r':
			biptr = &binfo[brd -1];	// Ones based vs zero-based
			rc = get_brd_id(biptr);
			if (rc != 0)
				break;
			if (biptr->fereq == 1)  // If this bd uses a FE
				rc = get_fe_info(biptr);
			if (rc != 0)
				break;
			biptr->conflag = 1;
			break;


	case 'q':		// Quit config tool
	case 'Q':
biptr = binfo;
while (biptr->model != 0)
	{
	if (biptr->conflag == 1) // If quitting before saving (and at 
		{		// least 1 board has been configed)
pwin = newwin(10,50,5,20);
wbkgd(pwin, COLOR_PAIR(1));		// Can be OR'ed with A_BOLD
wattrset(pwin,COLOR_PAIR(1)|A_BOLD);
box(pwin,0,0);
	mvwaddstr(pwin,2,2,"WARNING! You have configured one or more");
	mvwaddstr(pwin,3,2,"boards, but not saved the configuration.");
	mvwaddstr(pwin,4,2,"Enter 'Y' to quit anyway or 'S' to save ");
			wrefresh(pwin);
			key = wgetch(pwin);
			if ((key == 'y') || (key == 'Y'))
				{
				werase(pwin);
				delwin(pwin);
				touchwin(mwin);
				wrefresh(mwin);
				break;
				}
			else
			rc = save_info();  // OK, we'll save the info
			break;
		}
	biptr++;
	}
			endwin();
			exit(0);
			break;

	case 's':
	case 'S':

			rc = save_info();
			break;
	default:
			beep(); beep();
			break;
	}
}
// endwin();
}						// End of main

// Function to display all of the boards found with preliminary info
// No parameters passed - void returned
void disp_all_brds()
{
int i;
biptr = binfo;
wattrset(mwin,COLOR_PAIR(1)| A_BOLD);
for (i = 1; i< NUMBRDS; i++)
	{
	if (biptr->model == 0)
			break;
	sprintf(buff, "%d", biptr->brdnum);
	mvwaddstr(mwin,offset + i,BOARD,buff);

	sprintf(buff, "%c", biptr->slot);
	mvwaddstr(mwin,offset + i,SLOT,buff);

	sprintf(buff, "%0x", biptr->model);
	mvwaddstr(mwin,offset + i,MODEL,buff);

	sprintf(buff, "%s", biptr->name);
	mvwaddstr(mwin,offset + i,NAME,buff);

	// sprintf(buff, "%s", biptr->mload);
	// mvwaddstr(mwin,offset + i,MEDIA,buff);

	if (biptr->conflag == 1)
	mvwaddch(mwin,(offset + i),CONFED,'Y');
		else
	mvwaddch(mwin,(offset + i),CONFED,'N');

	sprintf(buff, "%s", biptr->feproto);
	mvwaddstr(mwin,offset + i,FRONT,buff);

	biptr++;
	}						// Do it again
wrefresh(mwin);
wrefresh(lwin);
}


// Function that cycles through each possible board location, filling in basic
// info on each board. 
// Called at init time and returns 0 on success, -1 on failure
int bld_binfo()
{
char buff2[80];
int i;
brd = 1;				// Initialize to 1
biptr = binfo;

sprintf(buff,"/sbin/lspci -n|grep '12[cC]7'|cut -d':' -d' ' -f4|cut -c6-9");
brdlst = popen(buff,"r");

for (i = 1; i< NUMBRDS; i++)
	{
memset(per_board, 0, 80);
memset(buff, 0, 80);
memset(buff2, 0, 80);

fgets(per_board, 78, brdlst);
if (strlen(per_board) == 0)	// Drivers loaded??
	continue;

biptr->brdnum = brd;
cptr = per_board;
tmptr = buff2;

// OK at this point I've got that string in a buffer
// cptr += 8;
biptr->model = strtol(cptr, NULL, 16);

config_brd(biptr);			// For each board found, fill info
// Fill in other fields
if(strlen(biptr->feproto) == 0)
	sprintf(biptr->feproto, "<NONE SELECTED> ");
// if(strlen(biptr->mload) == 0)
// 	sprintf(biptr->mload, "**");
biptr++;
brd++;
	}				// End of for loop
if (brd == 1)				// No boards found
	return(-1);
pclose(brdlst);
numbrds = brd;				// Save the number of boards found
return(0);
}					// End of bld_binfo()

// Generic routine to display a board's info. 
// Called with a ones-based based board number - void return
void disp_brd(int brd)
{
int i;
i = brd;
biptr = &binfo[brd -1];	// Ones based vs zero-based
wmove(mwin,(offset + i),0);
mvwaddstr(mwin,(offset + i),BOARD,"                                                                           ");

	sprintf(buff, "%d", biptr->brdnum);
	mvwaddstr(mwin,(offset + i),BOARD,buff);

	sprintf(buff, "%c", biptr->slot);
	mvwaddstr(mwin,(offset + i),SLOT,buff);

	sprintf(buff, "%0x", biptr->model);
	mvwaddstr(mwin,(offset + i),MODEL,buff);

	sprintf(buff, "%s", biptr->name);
	mvwaddstr(mwin,(offset + i),NAME,buff);

	// sprintf(buff, "%s", biptr->mload);
	// mvwaddstr(mwin,(offset + i),MEDIA,buff);

	if (biptr->conflag == 1)
	mvwaddch(mwin,(offset + i),CONFED,'Y');
		else
	mvwaddch(mwin,(offset + i),CONFED,'N');

	sprintf(buff, "%s", biptr->feproto);
	mvwaddstr(mwin,(offset + i),FRONT,buff);
}					// End of disp_brd()

// Generic screen high-lighter
void scr_brite(WINDOW *win)
{
wattrset(win,COLOR_PAIR(1)| A_REVERSE);
}

// Generic screen de-high-lighter
void scr_norm(WINDOW *win)
{
wattrset(win,COLOR_PAIR(1)| A_BOLD);
}

// Function to fill in initial, default info for each board found.
// Called with a board pointer - void return
// NOTE: Best guesses are used for filling in this info. MAY CHANGE!!!!!
// For boards that only support 1 media load, it's pre-selected.
void config_brd(struct board *biptr)
{

switch(biptr->model)
	{
// Span family
	case 0x525:			// 
		biptr->brdtype = 0;
		biptr->pcmtype = 0;
		biptr->fereq = 1;
		biptr->slot = '*';
		biptr->dualspan = 0;
		sprintf(biptr->name,"D/240PCI-T1");
		break;
	case 0x526:			// 
		biptr->brdtype = 0;
		biptr->pcmtype = 1;
		biptr->fereq = 1;
		biptr->slot = '*';
		biptr->dualspan = 0;
		sprintf(biptr->name,"D/300PCI-E1");
		break;
	case 0x527:			// 
		biptr->brdtype = 0;
		biptr->pcmtype = 1;
		biptr->fereq = 1;
		biptr->slot = '*';
		biptr->dualspan = 0;
		sprintf(biptr->name,"D/300PCI-E1");
		break;
	case 0x647:			// 
		biptr->brdtype = 0;
		biptr->pcmtype = 0;
		biptr->slot = '*';
		biptr->fereq = 1;
		biptr->dualspan = 0;
		sprintf(biptr->name,"D/240JCT-T1");
		break;
	case 0x648:			// 
		biptr->brdtype = 0;
		biptr->pcmtype = 1;
		biptr->fereq = 1;
		biptr->slot = '*';
		biptr->dualspan = 0;
		sprintf(biptr->name,"D/300JCT-E1");
		break;
	case 0x649:			// 
		biptr->brdtype = 0;
		biptr->pcmtype = 1;
		biptr->fereq = 1;
		biptr->slot = '*';
		biptr->dualspan = 0;
		sprintf(biptr->name,"D/300JCT-E1");
		break;
	case 0x685:			// 
		biptr->brdtype = 0;
		biptr->pcmtype = 0;
		biptr->fereq = 1;
		biptr->slot = '*';
		biptr->dualspan = 1;
		sprintf(biptr->name,"D/480JCT-2T1");
		break;
	case 0x687:			// 
		biptr->brdtype = 0;
		biptr->pcmtype = 1;
		biptr->fereq = 1;
		biptr->dualspan = 1;
		biptr->slot = '*';
		sprintf(biptr->name,"D/600JCT-2E1");
		break;
	case 0x689:			// 
		biptr->brdtype = 0;
		biptr->pcmtype = 1;
		biptr->fereq = 1;
		biptr->slot = '*';
		biptr->dualspan = 1;
		sprintf(biptr->name,"D/600JCT-2E1");
		break;
	case 0x651:			// 
		biptr->brdtype = 4;
		biptr->pcmtype = 0;
		biptr->slot = '*';
		biptr->fereq = 0;
		sprintf(biptr->name,"MSI-PCI Station");
		break;
	case 0x676:			// 
		biptr->brdtype = 2;
		biptr->pcmtype = 0;
		biptr->fereq = 0;
		biptr->slot = '*';
		sprintf(biptr->name,"D/41JCT-LS");
		break;
	case 0x546:			// 
		biptr->brdtype = 2;
		biptr->pcmtype = 0;
		biptr->fereq = 0;
		sprintf(biptr->name,"D/120JCT-LS");
		biptr->slot = '*';
		break;
	case 0x604:			// 
		biptr->brdtype = 1;
		biptr->pcmtype = 0;
		biptr->fereq = 0;
		biptr->slot = '*';
		sprintf(biptr->name,"D/80PCI");
		break;
	case 0x605:			// 
		biptr->brdtype = 1;
		biptr->pcmtype = 0;
		biptr->fereq = 0;
		biptr->slot = '*';
		sprintf(biptr->name,"D/320PCI");
		break;
	case 0x673:			// 
	case 0x674:			// 
	case 0x675:			// 
		biptr->brdtype = 4;
		biptr->pcmtype = 1;
		biptr->fereq = 0;
		biptr->slot = '*';
		sprintf(biptr->name,"BRI/PCI");
		break;
	case 0x619:			// 
		biptr->brdtype = 4;
		biptr->pcmtype = 0;
		biptr->fereq = 0;
		biptr->slot = '*';
		sprintf(biptr->name,"D/82JCT-U");
		break;
	case 0x580:			// 
		biptr->brdtype = 4;
		biptr->pcmtype = 0;
		biptr->fereq = 0;
		biptr->slot = '*';
		sprintf(biptr->name,"D/42NE");
		break;
	case 0x708:			// 
		biptr->brdtype = 1;
		biptr->pcmtype = 0;
		biptr->fereq = 0;
		biptr->slot = '*';
		sprintf(biptr->name,"D/160JCT");
		break;
	case 0x707:			// 
		biptr->brdtype = 1;
		biptr->pcmtype = 0;
		biptr->fereq = 0;
		biptr->slot = '*';
		sprintf(biptr->name,"D/320JCT");
		break;
		break;
	case 0x529:			// 
	case 0x562:			// 
	case 0x561:			// 
		biptr->brdtype = 4;
		biptr->pcmtype = 0;
		biptr->fereq = 0;
		biptr->slot = '*';
		sprintf(biptr->name,"BRI/PCI");
		break;
	default:
			sprintf(biptr->name,"***UNKNOWN***");
			break;
	}
}					// End of config_brd


// Code that pops up a new window and allows the user to select a front-end
// protocol. No sanity checking (other than it's gotta be on the list of FE
// types)
// Called with board pointer and, despite casting, returns void
int get_fe_info(struct board *biptr)
{
int line = 2;
int key;

// Section here to pop-up a new window and get the FE info
pwin = newwin(18,40,5,20);
wbkgd(pwin, COLOR_PAIR(1));		// Can be OR'ed with A_BOLD
wattrset(pwin,COLOR_PAIR(1)|A_BOLD);
box(pwin,0,0);
// Single-key-mode   keypad(pwin, TRUE);

for (feptr = fetypes; *feptr != NULL; feptr++)
{
mvwaddstr(pwin,line,10,*feptr);
line++;
}

mvwaddstr(pwin,14,1, "Use arrow keys to highlight the ");
mvwaddstr(pwin,15,1, "desired telephony front-end protocol ");
mvwaddstr(pwin,16,1, " Press ENTER to select - Q to quit   ");

// Reset to beginning of list
feptr = fetypes;
line = 2;
scr_brite(pwin);
mvwaddstr(pwin,2,10,*feptr);
wrefresh(pwin);
while (1)
{
noecho();
raw();
flushinp();
// typeahead(-1);
scrollok(pwin,TRUE);
keypad(pwin, TRUE);
touchwin(pwin);
wrefresh(pwin);
key = wgetch(pwin);
switch (key)
	{
	case KEY_DOWN:
		if (*feptr++ == NULL)
		{
				beep();
				beep();
				feptr--;
				break;
		}
		feptr--;
		scr_norm(pwin);
		mvwaddstr(pwin,line,10,*feptr);
		feptr++;
		line++;
		scr_brite(pwin);
		mvwaddstr(pwin,line,10,*feptr);
		break;

	case KEY_UP:
		if (feptr == &fetypes[0])
		{
				beep();
				beep();
				break;
		}
		scr_norm(pwin);
		mvwaddstr(pwin,line,10,*feptr);
		line--;
		feptr--;
		scr_brite(pwin);
		mvwaddstr(pwin,line,10,*feptr);
		break;

	case KEY_ENTER:
	case '\n':
	case '\r':
		werase(pwin);
		wrefresh(pwin);
		delwin(pwin);
		memset(biptr->feproto,0,sizeof(biptr->feproto));
		strcpy(biptr->feproto, *feptr);
		return(0);
		break;

	case 'q':
	case 'Q':
		werase(pwin);
		wrefresh(pwin);
		delwin(pwin);
		return(-1);
		break;
	default:
		beep();
		break;
	}

}
// To here
werase(pwin);
wrefresh(pwin);
delwin(pwin);
}

// Function to save the config info and build the pyramid.scd file
int save_info(void)
{
FILE *fp;
int key;
char * delim = " \t\n";
char prot[7];
int law = 0;		// 0 = ULAW; 1 = ALAW



// First, we'll make sure that all boards are configed......
biptr = binfo;
while (biptr->model != 0)
	{
	if (biptr->conflag != 1)
		{
pwin = newwin(10,50,5,20);
wbkgd(pwin, COLOR_PAIR(1));		// Can be OR'ed with A_BOLD
wattrset(pwin,COLOR_PAIR(1)|A_BOLD);
box(pwin,0,0);
	mvwaddstr(pwin,2,2,"WARNING! All boards not configured!!!");
	mvwaddstr(pwin,3,2,"Configuration file NOT written!!!");
	mvwaddstr(pwin,4,2,"Enter 'Y' to save anyway  ");
			wrefresh(pwin);
			key = wgetch(pwin);
			if ((key == 'y') || (key == 'Y'))
				{
				werase(pwin);
				delwin(pwin);
				touchwin(mwin);
				wrefresh(mwin);
				break;
				}
			werase(pwin);
			delwin(pwin);
			return(-1);
		}
	biptr++;
	}

// Set law config
biptr = binfo;
while (biptr->model != 0)
	{
	if (biptr->pcmtype == 1)
		law = 1;
	biptr++;
	}

biptr = binfo;

// Open the config file
fp = fopen("/usr/dialogic/cfg/dialogic.cfg","w");
if ( fp == NULL)
	{
	printf("ERROR opening config file.\n");
	printf("Press ENTER\n");
	getchar();
	return(-1);
	}
// Begin filling in the file.......
fprintf(fp,"\t\t#####################################\n");
fprintf(fp,"\t\t## Dialogic configuration file ######\n");
fprintf(fp,"\t\t##  Automatically created by   ######\n");
fprintf(fp,"\t\t##     dlconfig program        ######\n");
fprintf(fp,"\t\t#####################################\n");

fprintf(fp,"\n\n[Genload - All Boards]\n");
fprintf(fp,"LogFile=genload.log\n");
fprintf(fp,"BusType=SCBus\n");
fprintf(fp,"SCBusClockMaster=AUTOMATIC\n");
fprintf(fp,"SCBusClockMasterSource=AUTOMATIC\n");
if (law)
fprintf(fp,"PCMEncoding=ALAW\n");
else
fprintf(fp,"PCMEncoding=ULAW\n");

fprintf(fp,"\n\n");

while (biptr->model != 0)
	{
	if (biptr->conflag != 1)
		{
		biptr++;
		continue;
		}
fprintf(fp,"[Genload - PCI ID %c] \n",biptr->slot);
if  ((biptr->feproto[0] == 'C') || (biptr->feproto[0] == '<'))
	fprintf(fp,"\n");
	else
		{
		strcpy(prot,strtok(biptr->feproto,delim));
		sprintf(buff,"ISDNProtocol=%s\n",prot);
		fprintf(fp,buff);
		sprintf(buff,"ParameterFile=%s.prm\n",prot);
		fprintf(fp,buff);
		if (biptr->dualspan)
			{
		sprintf(buff,"ISDNProtocol2=%s\n",prot);
		fprintf(fp,buff);
		sprintf(buff,"ParameterFile2=%s.prm\n",prot);
		fprintf(fp,buff);
			}
		}
fprintf(fp,"\n");
biptr++;
	}
// Done going through the list
endwin();
exit(0);
}

int get_brd_id(struct board *biptr)
{
int key;
pwin = newwin(5,45,5,20);
wbkgd(pwin, COLOR_PAIR(1));		// Can be OR'ed with A_BOLD
wattrset(pwin,COLOR_PAIR(1)|A_BOLD);
box(pwin,0,0);
echo();
noraw();
flushinp();
// typeahead(-1);
scrollok(pwin,TRUE);
keypad(pwin, TRUE);
mvwaddstr(pwin,2,2,"Enter the thumbwheel ID for this board: ");
touchwin(pwin);
wrefresh(pwin);
key = wgetch(pwin);
// if ((key < '0') || (key > 'F'))
switch (key)
	{
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
	case 'a':
	case 'A':
	case 'b':
	case 'B':
	case 'c':
	case 'C':
	case 'd':
	case 'D':
	case 'e':
	case 'E':
	case 'f':
	case 'F':
		biptr->slot = key;
		break;
	default:
		beep();beep(); return(-1);
	}
// To here
werase(pwin);
wrefresh(pwin);
delwin(pwin);
return(0);
}
