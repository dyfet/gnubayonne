/**********************************************************************************
*
*	C Source:		gcdiag.c
*	Description:	GlobalCall diagnostics application for Springware and DM3
*	%created_by:	langee and gilmoreg%
*	%date_created:	Wed Nov 20 16:29:13 2002 %
*
* Version History:
*	1.0 - First port from Linux "isdiag"
*	2.0 - Single-source code for Linux and NT. Supports:
*		Springware digital cards
*		DM3 cards (except IPLink)
*	3.0 - Add IPLink support 
*	3.01 - Fix bug in Rec_File and some IPLink issues with HMP under W2K
*	3.02 - Fix bug ISDN/H.323 detect which prevented gc_Attach() from being
*		called for CAS lines. Doh!
***********************************************************************************/
/**********************************************************************************
** Copyright (c) 2003 Gerry Gilmore and Eric Lange
** This file is part of GCDIAG.

    GCDIAG is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GCDIAG is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCDIAG; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


***********************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <malloc.h>
#include <sys/types.h>

#ifdef WIN32
#include "scrnlib.h"		// screen functions library 
#include "scrnlownt.h"		// screen functions library 
#else				// LINUX
#include <curses.h>
#include <sys/wait.h>
#include <unistd.h>
#endif

#include "gcdiag.h"		// This should handle all my specific stuff
			// along with the Dialogic headers in the proper order.

#define max_linfo &linfo[MAXCHAN + 1]	// Define the highest struct element
#define max_vinfo &vinfo[MAXCHAN + 1]	// Define the highest struct element

#ifdef LINUX
typedef long (*HDLR)();		// Basic Handler typedef
#else
typedef long (*HDLR)(unsigned long);	// Basic Handler typedef
#endif

Line_Info *liptr; 		// The structure definition is in the header
Voice_Info *viptr; 		// Same as above, but for the voice resources

//
// Global Variables  (I know, I know.......)
//
int interrupted = 0;
int oofflag;
int redflag;
int aisflag;
int raiflag;
int losflag;
int ferrflag;
int brd;
int chan;
int maxdti;
int voice;
int val_ptr;
int menu_number;
int menu_number2;
int menu_number3;
int menu_number4;
int menu_number5;
int menu_num;
int req_type;
int StartTraceFlag = 0;
int row;
int column;
int main_win;
int alarm_win;
int message_win;
int	normal_color;	
int	reverse_color;	
int box_color;		
int	help_color;		
int title_color;		
int red_alarm_color;
int cmdhelp_color;	
int	yel_alarm_color;
int info_color;
int	blinkrvs_color;
int dm3 = 0;
int isdn = 0;
int ipt = 0;
char filename[20];
char called_num[24] = "9739933000";
char calling_num[24];
char trace_name[60] = {0};
char protocol[40];
char called_sub_buf[80];
char uui_buf[512];
char voicedev[20];
extern int full_scrnh;
unsigned long scrnlib_features;
 
GC_MAKECALL_BLK mkcall;
MAKECALL_BLK my_makecall_blk;

LINEDEV dtidev;

BYTE info_transfer_cap_num;
BYTE usr_info_layer1_proto;
BYTE usr_rate;

DX_IOTT iott[1],*iottp;
DV_TPT tpt[3],*tptp;
CT_DEVINFO ct_devinfo; 			// Device information structure  

#ifdef WIN32
	PROCESS_INFORMATION	pInfo;
	STARTUPINFO			sInfo;
#endif

#ifdef LINUX
	WINDOW *w, *a, *l; 
#endif

#define MAXDIGIT 20      
#define ALARMS (DTEC_RED | DTEC_RYEL | DTEC_RBL | DTEC_LOS | DTEC_ECS | \
		DTEC_BVCS | DTEC_DPM | DTEC_FERR)
#define PROGRAM "GlobalCall DIAG  Version 3.02"
#define BLANK "                                                                           "
//
// Function Prototypes
//
Line_Info *Find_Line_Dev(int hdl);	// Given a handle, return the "Line_Info" pointer.

int get_num( );
int get_str( );
int get_key( );
int Open_Single_Voice_Dev(void);
int Open_Net_Devs(void);
long GC_Hdlr(unsigned long parm);
int GC_Brd_Hdlr(void);
int Net_Board_Hdlr(void);
int Vox_Hdlr(void);
int Play_File(char *fname);
int Rec_File(char *fname);
int Marry_TS(Line_Info *liptr, Voice_Info *viptr);
int Unmarry_TS(Line_Info *liptr, Voice_Info *viptr);

void Exit_Prog(void);
void usage(void);
void chkargs( int argc, char *argv[] );
void dspMainMenu(void);
void dspAlarms(void);
void display_init(void);
void display_exit(void);
void get_user_command(void);
void Intr_Hdlr(void);
void Set_To_Zero(void);
void Open_Net_Board_Devs(void);
void Print_Info(void);
void log_message1(char *str);
void log_message2(char *str);
void log_message3(char *str);
void menu_switch( );
void set_call_parameter_menu( );
void set_call_parameter_menu_switch( );
void ani_menu( );
void ani_switch( );
void presentation_indicator_menu( );
void bearer_capability_ie_menu( );
void display_information_menu( );
void display_information_switch( );
void user_info_layer1_proto( );
void user_info_layer1_proto_switch( );
void user_rate( );
void user_rate_switch( );
void getustr();


/****************************************************************
*        NAME : main(argc,argv)
* DESCRIPTION : main
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS : 
****************************************************************/
int main(int argc, char *argv[])
{
	int mode;
	int retval;
	char buff[80];

	chkargs( argc, argv );

#ifdef WIN32
	mode = SR_STASYNC;   
	CRC(sr_setparm(SRL_DEVICE, SR_MODELTYPE, (void *)&mode));
#else
	mode = SR_POLLMODE;
	CRC(sr_setparm(SRL_DEVICE, SR_MODEID, (void *)&mode));
#endif
	display_init();	//initializes the screen

	dspAlarms();	// displays alarm window

	dspMainMenu();  // displays Main Menu 
	
	sprintf(buff,"Initializing Data Structures......");
	log_message1(buff);
	
	Set_To_Zero();

	if (voice) {
 		sprintf(buff,"Initializing Voice Device.......");
		log_message1(buff);

		if ((Open_Single_Voice_Dev()) != 0)	{
 			sprintf(buff,"Couldn't open voice device!!!! Exiting!");
			log_message3(buff);
			exit(1);
		}
	}
	
	sprintf(buff,"Initializing Global Call Network Line Device.......");
	log_message1(buff);

	if ((Open_Net_Devs()) != 0)	{
	sprintf(buff,"Couldn't open Global Call Network Line Device!!!! Exiting");
		log_message3(buff);
		exit(1);
	}

	sprintf(buff,"Initializing Network Board Devices.......");
	log_message1(buff);
	
	Open_Net_Board_Devs();

	sprintf(buff,"Setting Signal Handlers.......");
	log_message1(buff);

#ifdef WIN32

	signal(SIGINT, (void (__cdecl*)(int)) Intr_Hdlr);
	signal(SIGTERM, (void (__cdecl*)(int)) Intr_Hdlr);
	SRC (scrn_wclrline( message_win, 3, 1, 80 ));

#else

	signal(SIGHUP, (void (*) ()) Intr_Hdlr);
	signal(SIGINT, (void (*) ()) Intr_Hdlr);
	signal(SIGQUIT, (void (*) ()) Intr_Hdlr);
	signal(SIGTERM, (void (*) ()) Intr_Hdlr);
	// wmove(l,2,0);
	// wprintw(l,BLANK);
	wrefresh(l);

#endif

	dspAlarms();	// displays alarm window

	dspMainMenu();  // displays Main Menu 
	
	// Initialize the ISDN-specific MAKECALL block stuff correctly.
	
	mkcall.gclib = NULL;
	mkcall.cclib = (void *)&my_makecall_blk;

	while (1) {
		get_user_command();
	
		retval = sr_waitevt(50); 
		
		if (interrupted == 1)
			Exit_Prog();
	}

} /* End of Main */

/****************************************************************
*        NAME : Line_Info *Find_Line_Dev(int hdl)	
* DESCRIPTION : Function to return a pointer to the line info struct 
*				given a device handle.
*       INPUT : handle
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS : 
****************************************************************/
Line_Info *Find_Line_Dev(int hdl)	
{ 
	for(liptr = linfo; liptr != max_linfo; liptr++)	{
	
		if (liptr->lndev == hdl) 
		break;		
	}
	if (liptr == max_linfo)
		liptr = NULL;		// Or, return(NULL)?

	return(liptr);
} 

/****************************************************************
*        NAME : Open_Single_Voice_Dev(void)	
* DESCRIPTION : Function to open a voice device.
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS : 
****************************************************************/
int Open_Single_Voice_Dev(void)
{
	char buff[80];

	viptr = vinfo;

	if ( ( viptr->vrdev = dx_open( voicedev, 0 ) ) == -1 ) {
		viptr->vrdev = 0;
sprintf(buff,"Unable to open voice device %s, errno = %d",voicedev,errno ); 
		log_message3(buff);
		return(-1);
	}		// End of if
	
	strcpy(viptr->vname, voicedev);

			// Get xmitslot, etc.
	viptr->vtsinfo.sc_numts =1;
	viptr->vtsinfo.sc_tsarrayp = &viptr->vscts;

	if((dx_getxmitslot(viptr->vrdev, &viptr->vtsinfo) ) == -1) {
	sprintf(buff,"Error in dx_getxmitslot. Device: %s", viptr->vname);
			log_message3(buff);
			return(-1);
	}

			// Enable the handler
	if(sr_enbhdlr(viptr->vrdev, EV_ANYEVT,(HDLR)Vox_Hdlr) == -1) {
		sprintf(buff,"Error enabling vox handler");
		log_message3(buff);
		return(-1);
	}

	return(0);
}	// End of function

/****************************************************************
*        NAME : Open_Net_Devs(void)	
* DESCRIPTION : Function to open network (GC) line devices.
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS : 
****************************************************************/
int Open_Net_Devs(void)
{

	char buff[80];
	char buffer[40];
	int channel;
	int brdhdl;

	liptr = linfo;			// Inititalize to the first one.

	if (gc_Start(NULL) == -1) {	// Initialize GC
		log_message3("ERROR starting GC sub-system. Exiting!!!/n");
		exit(1);
	}

											//  Open the GlobalCall *board* device
	if (ipt)
	sprintf(buffer, ":N_iptB%d:P_H323_NTSC", brd);
	else
	sprintf(buffer, ":N_dtiB%d:P_%s", brd,protocol);
	
	GRC(gc_OpenEx(&dtidev,buffer, EV_SYNC, 0));

// HMP	if (!ipt)
	GRC(gc_GetNetworkH(dtidev, &brdhdl));

// HMP	if (!ipt)
// HMP	{
	maxdti = ATDV_SUBDEVS(brdhdl);
	// Bug with HMP in W2K.....
	if (maxdti == -1)
	{
		log_message3("Failure in ATDV_SUBDEVS()!!");
		maxdti=24;
	}
// HMP	}
// HMP	else
// HMP		maxdti=24; 	// HACK!!!!!!
												//	Enable the board (D channel) handler
	if (!ipt)
	{
	if (sr_enbhdlr(dtidev, GCEV_D_CHAN_STATUS, (HDLR) GC_Brd_Hdlr) == -1) {
	sprintf(buff,"Error enabling handler for %s\n",ATDV_NAMEP(dtidev));
		log_message3(buff);
		}
	}
		// Loop through all valid channels....
	for (channel = 1; channel <= maxdti; channel++) {
		strcpy(liptr->protocol,protocol); 
	if (!ipt)
		{
		sprintf(liptr->lname, "dtiB%dT%d",brd,channel);
		liptr->lntype = dtCCDEV;	// ISDN
		sprintf(buffer,":N_%s:P_%s",liptr->lname,liptr->protocol);
		}
	else
		{
		sprintf(liptr->lname, "iptB%dT%d",brd,channel);
		liptr->lntype = dtIPDEV;	// IPT 
if ((voice == 1) && (chan == channel))
sprintf(buffer,":N_%s:P_H323_NTSC:M_ipmB%dC%d:V_%s",liptr->lname,brd,channel,voicedev);
else
sprintf(buffer,":N_%s:P_H323_NTSC:M_ipmB%dC%d",liptr->lname,brd,channel);
		}
												// Now, open the GlobalCall device
		GRC(gc_OpenEx(&liptr->lndev,buffer, EV_SYNC, 0));
													// Put the "raw" network handle into liptr->dtdev
		GRC(gc_GetNetworkH(liptr->lndev,&liptr->dtdev));
													// And fill in the SC Timeslot info
		liptr->ltsinfo.sc_numts =1;
		liptr->ltsinfo.sc_tsarrayp = &liptr->lscts;

	if (ipt)
	{
		GRC(gc_GetXmitSlot(liptr->lndev,&liptr->ltsinfo));
	}
	else
	{
		// NOTE: Springware SR 5.1 doesn't handle gc_GetXmitSlot()
		// Need a DM3/SW test....

		if((dt_getxmitslot(liptr->dtdev, &liptr->ltsinfo) ) == -1) {
	sprintf(buff,"Error in dt_getxmitslot. Device: %s", liptr->lname);
			log_message3(buff);
		}
	}
													// Set the event mask
// GFG Broken in 5.1 non-SP1	GRC(gc_SetEvtMsk(liptr->lndev,(GCMSK_ALERTING|GCMSK_PROCEEDING),GCACT_ADDMSK));
												// Enable the handler
		if (sr_enbhdlr((long)liptr->lndev, (unsigned long)EV_ANYEVT, (HDLR) GC_Hdlr) == -1) {
		sprintf(buff,"Error enabling handler for %s\n",liptr->lname);
		log_message3(buff);
		}
			// Set default ANI if this is the channel selected
		if (channel == chan) {
			GRC(gc_SetCallingNum(liptr->lndev, "9739933000"));
	sprintf(my_makecall_blk.isdn.origination_phone_number,"9739933000");
			GRC(dt_getctinfo(liptr->dtdev, &ct_devinfo));

			if( ct_devinfo.ct_devfamily == CT_DFDM3 )
				dm3 = 1;
		}
											// Only route to voice if specified AND this is the "active" channel
		if ((voice == 1) && (channel == chan)) {
			viptr = vinfo;

		if ((!(isdn)) && (!(ipt)))
			GRC(gc_Attach(liptr->lndev, viptr->vrdev, EV_SYNC));
			Marry_TS(liptr, viptr);
		}
		liptr++;
	}				// End of for loop
	return(0);
}	// End of function

/****************************************************************
*        NAME : Vox_Hdlr()
* DESCRIPTION : Function to handle voice (vox) events.
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS : 
****************************************************************/
int Vox_Hdlr()
{
	char buff[80];
#ifdef LINUX
	int y,x;
#endif

	int type = sr_getevttype();
	viptr = vinfo;

#ifdef LINUX
	getsyx(y,x);
#endif
	
	switch (type) {
		
		case TDX_GETDIG:	 	// Got a digit

			sprintf(buff,"GETDIG event on %s", viptr->vname);
			log_message2(buff);
			viptr->vrstate = vsIDLE;
			break;

		case TDX_PLAY:	 		// Finished playing
		
		sprintf(buff,"PLAY COMPLETED event on %s", viptr->vname);
			log_message2(buff);
			viptr->vrstate = vsIDLE;
	dx_clrdigbuf(viptr->vrdev);	// Get rid of any DTMFs lingering
			break;

	
		case TDX_RECORD: 		// Finished recording
	
		sprintf(buff,"RECORD COMPLETED event on %s", viptr->vname);
			log_message2(buff);
			viptr->vrstate = vsIDLE;
			break;

	
		case TDX_NOSTOP: 		// Channel stopped via dx_stopch
	
			sprintf(buff,"NOSTOP event on %s", viptr->vname);
			log_message2(buff);
			viptr->vrstate = vsIDLE;
			break;

	
		default:

sprintf(buff,"Unexpected vox event 0x%X on device: %s",type,viptr->vname);
			log_message3(buff);
			break;
	}

#ifdef LINUX
	setsyx(y,x);
	doupdate();
#endif
	return (0);
}

/****************************************************************
*        NAME : GC_Hdlr()
* DESCRIPTION : Function to handle GlobalCall events.
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS : 
****************************************************************/
long GC_Hdlr(unsigned long parm)
{
	char buff[80];
#ifdef LINUX
	int y,x;
#endif
	int dev = sr_getevtdev();
	int type = sr_getevttype();
	int *datap = (int *)sr_getevtdatap();

	METAEVENT metaevent;

#ifdef LINUX
	getsyx(y,x);
#endif

	GRC(gc_GetMetaEvent(&metaevent));

	if (metaevent.flags & GCME_GC_EVENT) {	// Should be a valid GC event
		dev = metaevent.evtdev;
		type = metaevent.evttype;
		datap = metaevent.evtdatap;
	}
	else {
	sprintf(buff,"Non-GC event: 0x%x device: %s.",type,ATDV_NAMEP(dev));
		log_message3(buff);
		return(0);
	}

	if ( (liptr = Find_Line_Dev(dev)) == NULL) {
	sprintf(buff, "ERROR: Find_Line_Dev got a NULL pointer. Dev = %d", dev);
		log_message3(buff);
		return(0);
	}

	switch (type) {
		case GCEV_OFFERED:
			
			sprintf(buff,"GCEV_OFFERED event on %s", liptr->lname);
			log_message2(buff);
			
			if ((gc_GetCRN(&liptr->crn, &metaevent)) == -1) {
			sprintf(buff,"gc_GetCRN failed for %s",liptr->lname);
				log_message3(buff);
			return(0);	// Return here since something must be
					// screwed up for this to fail
				}

			if ((gc_GetDNIS(liptr->crn, liptr->dnis_buf)) == -1) {
			sprintf(buff,"gc_GetDNIS failed for %s",liptr->lname);
				log_message3(buff);
				}

			if ((gc_GetANI(liptr->crn, liptr->ani_buf)) == -1) {
			sprintf(buff,"gc_GetANI failed for %s",liptr->lname);
				log_message3(buff);
				}

	sprintf(buff,"ANI is: %s DNIS is: %s",liptr->ani_buf,liptr->dnis_buf);
			log_message1(buff);

			if ((gc_AcceptCall(liptr->crn,0, EV_ASYNC)) == -1) {
			sprintf(buff,"AcceptCall failed for %s",liptr->lname);
				log_message3(buff);
				}
			break;

		case GCEV_ACCEPT:
			
			sprintf(buff,"GCEV_ACCEPTED event on %s", liptr->lname);
			log_message2(buff);
		
			if ((gc_AnswerCall(liptr->crn,0, EV_ASYNC)) == -1) {
			sprintf(buff,"AnswerCall failed for %s",liptr->lname);
				log_message3(buff);
				return(0);
				}
			break;

		case GCEV_ANSWERED:
			
			sprintf(buff,"GCEV_ANSWERED event on %s", liptr->lname);
			log_message2(buff);
			liptr->lnstate = lsCONNECTED;
			break;

	
		case GCEV_DISCONNECTED:
			
		sprintf(buff,"GCEV_DISCONNECTED event on %s", liptr->lname);
			log_message2(buff);
			sprintf(buff,"Dropping Call on %s", liptr->lname);
			log_message1(buff);
		
	if ((gc_DropCall(liptr->crn, GC_NORMAL_CLEARING, EV_ASYNC)) == -1) {
			sprintf(buff,"gc_DropCall failed for %s",liptr->lname);
				log_message3(buff);
				return(0);
				}
			break;

		case GCEV_DROPCALL:
			
			sprintf(buff,"GCEV_DROPPED event on %s", liptr->lname);
			log_message2(buff);
		
		if ((gc_ReleaseCallEx(liptr->crn, EV_ASYNC)) == -1)	{
			sprintf(buff,"ReleaseCall failed for %s",liptr->lname);
				log_message3(buff);
				return(0);
				}
		// Clean up ANI/DNIS buffers
			liptr->dnis_buf[0]='\0';
			liptr->ani_buf[0]='\0';
			
			// Need to stop any associated plays/records in progress
			if (voice){
				viptr = vinfo;
				if ((viptr->vrstate == vsPLAYING) || (viptr->vrstate == vsRECORDING)  || (viptr->vrstate == vsDTMF)) {
			sprintf(buff,"Stopping vox channel: %s.",viptr->vname);
					log_message1(buff);
					dx_stopch(viptr->vrdev, EV_ASYNC);
					} 
				}
			break;
		
		case GCEV_RELEASECALL:

		sprintf(buff,"GCEV_RELEASECALL event on %s", liptr->lname);
			log_message2(buff);

			liptr->crn = 0;
			liptr->lnstate = lsIDLE;
			break;

		case GCEV_PROCEEDING:
			
		sprintf(buff,"GCEV_PROCEEDING event on %s", liptr->lname);
			log_message2(buff);
			break;

		case GCEV_ALERTING:
			
			sprintf(buff,"GCEV_ALERTING event on %s", liptr->lname);
			log_message2(buff);
			break;

		case GCEV_PROGRESSING:
		
		sprintf(buff,"GCEV_PROGRESSING event on %s", liptr->lname);
			log_message2(buff);
			break;

		case GCEV_CONNECTED:
			
		sprintf(buff,"GCEV_CONNECTED event on %s", liptr->lname);
			log_message2(buff);
			liptr->lnstate = lsCONNECTED;
			break;

		case GCEV_BLOCKED:
			
			liptr->lnstate = lsOOS;
			sprintf(buff,"GCEV_BLOCKED Event on %s", liptr->lname);
			log_message2(buff);
			break;

		case GCEV_UNBLOCKED:
		
			liptr->lnstate = lsIDLE;
		sprintf(buff,"GCEV_UNBLOCKED Event on %s", liptr->lname);
			log_message2(buff);
			GRC(gc_ResetLineDev(liptr->lndev,EV_ASYNC));
			break;

		case GCEV_RESETLINEDEV:
			// Handle the case where the line goes: UB->B->RESET
			if (liptr->lnstate == lsOOS) {
sprintf(buff,"GCEV_RESETLINEDEV Event on %s- still BLOCKED", liptr->lname);
				log_message2(buff);
				break;
			}
		
			liptr->lnstate = lsIDLE;
		sprintf(buff,"GCEV_RESETLINEDEV Event on %s", liptr->lname);
			log_message2(buff);
			
			GRC(gc_WaitCall(liptr->lndev,NULL, NULL, 0, EV_ASYNC));
			break;

		case GCEV_TASKFAIL:
			
			sprintf(buff,"GCEV_TASKFAIL Event on %s", liptr->lname);
			log_message2(buff);
			break;

		case GCEV_RESTARTFAIL:
		
		sprintf(buff,"GCEV_RESTARTFAIL Event on %s", liptr->lname);
			log_message2(buff);
			break;

		case GCEV_SETCHANSTATE:
		
		sprintf(buff,"GCEV_SETCHANSTATE Event on %s", liptr->lname);
			log_message2(buff);
			break;

		case 0x864:
			
			sprintf(buff,"MAINTENANCE Event on %s", liptr->lname);
			log_message2(buff);
			break;

		default:
			
		sprintf(buff,"Unknown event 0x%x on %s", type, liptr->lname);
			log_message3(buff);
			break;

	}	// End of switch

#ifdef LINUX
	setsyx(y,x);
	doupdate();
#endif
	return(0);
}

/****************************************************************
*        NAME : GC_Brd_Hdlr()
* DESCRIPTION : Function to handle board level D Channel Status events.
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS : 
****************************************************************/
int GC_Brd_Hdlr()
{
	char buff[80];
#ifdef LINUX
	int y,x;
#endif
	int dev = sr_getevtdev();
	int type = sr_getevttype();
	int *datap = (int *)sr_getevtdatap();

	METAEVENT metaevent;

#ifdef LINUX
	getsyx(y,x);
#endif

	GRC(gc_GetMetaEvent(&metaevent));	// Chk for errors???

	if (metaevent.flags & GCME_GC_EVENT) {	// Should be a valid GC event
	
		dev = metaevent.evtdev;
		type = metaevent.evttype;
		datap = metaevent.evtdatap;
	}
	else {
		sprintf(buff,"Got non-GC event: %d in GC_Hdlr for device: %s.",type,ATDV_NAMEP(dev));
		log_message3(buff);
		return(0);
	}


	switch (type) {

		case GCEV_D_CHAN_STATUS:
			
		sprintf(buff,"DCHAN_STATUS Event on %s", ATDV_NAMEP(dtidev));
			log_message2(buff);
			break;
	
		default:
			
	sprintf(buff,"Unknown event 0x%x on %s", type, ATDV_NAMEP(dtidev));
			log_message3(buff);
			break;
	}
	
	dspAlarms(); 

#ifdef LINUX
	setsyx(y,x);
	doupdate();
#endif
	return(0);
}

/****************************************************************
*        NAME : Marry_TS(Line_Info *liptr, Voice_Info *viptr)
* DESCRIPTION : Function to route voice and network times slots
*				together.
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS : 
****************************************************************/
int Marry_TS(Line_Info *liptr, Voice_Info *viptr)
{

if (liptr->lntype != dtIPDEV)
{
	if(dt_listen(liptr->dtdev, &viptr->vtsinfo) == -1) {
	
		sprintf(buff,"dt_listen failed");
		log_message3(buff);
	
		return(-1);
	}
}
else
{
if (gc_Listen(liptr->lndev, &viptr->vtsinfo, EV_SYNC) == -1)
	{
	sprintf(buff,"gc_Listen failed");
	log_message3(buff);
	return(-1);
	}
}

	if (dx_listen(viptr->vrdev, &liptr->ltsinfo) == -1) {

		sprintf(buff,"dx_listen failed");
		log_message3(buff);
	
		return(-1);
	}

	viptr->vr_inuse = 1;

	return(0);
}

/****************************************************************
*        NAME : Unmarry_TS(Line_Info *liptr, Voice_Info *viptr)
* DESCRIPTION : Function to un-route voice and network times slots
*				together.
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS : 
****************************************************************/
int Unmarry_TS(Line_Info *liptr, Voice_Info *viptr)
{

if (liptr->lntype != dtIPDEV)
{
	if(dt_unlisten(liptr->dtdev) == -1)	{
	
		sprintf(buff,"dt_unlisten failed");
		log_message3(buff);
	
		return(-1);
	}

}
else
{
if (gc_UnListen(liptr->lndev, EV_SYNC) == -1)
	{
	sprintf(buff,"gc_Listen failed");
	log_message3(buff);
	return(-1);
	}
}
	if (dx_unlisten(viptr->vrdev) == -1) {

		sprintf(buff,"dx_unlisten failed");
		log_message3(buff);
	
		return(-1);
	}

	viptr->vr_inuse = 0;

	return(0);
}

/****************************************************************
*        NAME : Intr_Hdlr()
* DESCRIPTION : Handler function to BEGIN app shutdown
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS : 
****************************************************************/
void Intr_Hdlr()
{

	sprintf(buff,"Signal Received. Quitting");
	log_message2(buff);
	
	interrupted = 1;

	Exit_Prog();
}

/****************************************************************
*        NAME : Exit_Prog()
* DESCRIPTION : Function to perform app shutdown
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS : 
****************************************************************/
void Exit_Prog()
{

	int i;

	sprintf(buff,"Closing all GC devices");
	log_message3(buff);

	for(liptr = linfo; liptr != max_linfo; liptr++)	{

		if (liptr->lndev == 0)
			break;
		
		gc_Close(liptr->lndev);
	}

		// Check the state to see if we're playing and do dx_stopch

	sprintf(buff,"Closing all voice devices");
	log_message3(buff);

	
	if (voice)	{
		
		viptr = vinfo;
		
		if (viptr->vr_inuse != 0) {

			sprintf(buff,"Stopping %s",viptr->vname);
			log_message3(buff);
			i = dx_stopch(viptr->vrdev, EV_SYNC);
		}

		i = dx_close(viptr->vrdev);
	}
		

	// Do much more here
	
	gc_Stop();
#ifdef LINUX
	endwin();
#else
	display_exit();
#endif

	exit (0);
}

/****************************************************************
*        NAME : Set_To_Zero
* DESCRIPTION : Function to initialize data structure
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS : 
****************************************************************/
void Set_To_Zero()
{

	for(liptr = linfo; liptr != max_linfo; liptr++)	{
		
		liptr->lndev = 0;
		liptr->vrdev = 0;
		liptr->lntype = dtCOUNT;
		liptr->lnstate = 0;
		liptr->app_state = 0;
		liptr->dtdev = 0;
	}

	viptr = vinfo;
	viptr->vrdev = 0;
	viptr->lndev = 0;
	viptr->vrtype = vtCOUNT;
	viptr->vr_inuse = 0;

}


/****************************************************************
*        NAME : Open_Net_Board_Devs(void)
* DESCRIPTION : Function to open board devices and set masks
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS : 
****************************************************************/
void Open_Net_Board_Devs(void)
{
	int brdhdl;
	char devname[10];

if (ipt)
	return;

	sprintf(devname,"dtiB%d", brd);

	if ((brdhdl = dt_open(devname,0)) == -1) {

		printf("Error opening board %s\n", devname);
		return;
	}

	if (sr_enbhdlr(brdhdl, DTEV_T1ERRC, (HDLR) Net_Board_Hdlr) == -1) {
		
		printf("Error enabling handler on %s\n", devname);
		return;
	}
	
	if (dt_setevtmsk(brdhdl,DTG_T1ERREVT,(DTEC_RLOS|DTEC_LOS|DTEC_DPM|
		DTEC_RED|DTEC_BPVS|DTEC_ECS|DTEC_RYEL|DTEC_RCLX|DTEC_FERR|
		DTEC_B8ZSD|DTEC_RBL|DTEC_OOF), DTA_SETMSK) == -1) {

		printf("Error setting event mask on %s\n", devname);
		return;
	}

}

/****************************************************************
*        NAME : Net_Board_Hdlr()
* DESCRIPTION : Function to report on all board-level T1 error 
*				conditions.
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS : 
****************************************************************/
int Net_Board_Hdlr()
{

	unsigned long int type;
	int dev;
	static int aflag = 0;
	unsigned char *datap;
	char halfblank[40] = "                                   ";
	char *cptr;
#ifdef LINUX
	int y,x;
#endif

						// Gather all of the info about the event

	type = sr_getevttype();
	dev = sr_getevtdev();
	datap = (unsigned char *)sr_getevtdatap();
	cptr = alarmbuff;

#ifdef LINUX
	getsyx(y,x);
	wmove(a,1,0);
	wprintw(a, "                                      ");
#endif

	if ( type != DTEV_T1ERRC ) {

		sprintf(alarmbuff,"Unexpected event: 0x%lX. on device: %s", type, ATDV_NAMEP(dev));
		log_message3(alarmbuff);
	
		return(0);
	}
	else {

		switch ( *datap ) {
			
		case DTT1_RLOS:
			aflag++;
			cptr += sprintf(alarmbuff, "Remote LOS ");
			break;
		case DTT1_RLOSOK:
			aflag--;
			cptr += sprintf(alarmbuff, "Remote LOS OK ");
			break;
		case DTT1_LOS:
			aflag++;
			losflag = 1;
			cptr += sprintf(alarmbuff, "Loss of Synchronization ");
			break;
		case DTT1_LOSOK:
			aflag--;
			losflag = 0;
		cptr += sprintf(alarmbuff, "Loss of Synchronization OK");
			break;
		case DTT1_RED:
			aflag++;
			redflag = 1;
			cptr += sprintf(alarmbuff, "RED Alarm Condition");
			break;
		case DTT1_DPM:
			aflag++;
		cptr += sprintf(alarmbuff,"Driver Performance Monitor Error ");
			break;
		case DTT1_DPMOK:
			aflag--;
		cptr += sprintf(alarmbuff,"Driver Performance Monitor OK");
			break;
		case DTT1_REDOK:
			aflag--;
			redflag = 0;
			cptr += sprintf(alarmbuff, "RED Alarm Condition OK");
			break;
		case DTT1_BPVS:
			aflag++;
		cptr+=sprintf(alarmbuff,"Bipolar violation count saturation ");
			break;
		case DTT1_BPVSOK:
			aflag--;
	cptr+=sprintf(alarmbuff,"Bipolar violation count saturation OK");
			break;
		case DTT1_ECS:
			aflag++;
			cptr += sprintf(alarmbuff, "Error Count Saturation ");
			break;
		case DTT1_ECSOK:
			aflag--;
			cptr += sprintf(alarmbuff, "Error Count Saturation OK");
			break;
		case DTT1_RYEL:
			aflag++;
			raiflag = 1;
			cptr += sprintf(alarmbuff, "Yellow Alarm Condition ");
			break;
		case DTT1_RYELOK:
			aflag--;
			raiflag = 0;
			cptr += sprintf(alarmbuff, "Yellow Alarm Condition OK");
			break;
		case DTT1_RCL:
			aflag++;
			cptr += sprintf(alarmbuff, "Remote Carrier Loss ");
			break;
		case DTT1_RCLOK:
			aflag--;
			cptr += sprintf(alarmbuff, "Remote Carrier Loss OK");
			break;
		case DTT1_FERR:
			aflag++;
			ferrflag = 1;
			cptr += sprintf(alarmbuff, "Frame Bit Error ");
			break;
		case DTT1_FERROK:
			aflag--;
			ferrflag = 0;
			cptr += sprintf(alarmbuff, "Frame Bit Error OK");
			break;
		case DTT1_RBL:
			aflag++;
			aisflag = 1;
			cptr += sprintf(alarmbuff, "Blue Alarm Condition ");
			break;
		case DTT1_RBLOK:
			aflag--;
			aisflag = 0;
			cptr += sprintf(alarmbuff, "Blue Alarm Condition OK");
			break;
		case DTT1_OOF:
			aflag++;
			oofflag = 1;
			cptr += sprintf(alarmbuff, "Out Of Frame Error");
			break;
		case DTT1_OOFOK:
			aflag--;
			oofflag = 0;
			cptr += sprintf(alarmbuff, "Out Of Frame Error OK");
			break;
		default:
		cptr += sprintf(alarmbuff,"Unexpected T1 Error: 0x%X", *datap);
			log_message3(alarmbuff);
			break;
		}	// End switch

#ifdef LINUX
		setsyx(y,x);
		doupdate();
#endif
		dspAlarms();
			 
		if (aflag) {
#ifdef WIN32
			SRC (scrn_fgcolor ( box_color & 0x0F ));
			SRC (scrn_bgcolor ( normal_color & 0xF0 ));
#else
			wattrset(a,COLOR_PAIR(4)|A_BOLD);
#endif
		}
		if (aflag && aisflag) {
#ifdef WIN32

			SRC (scrn_fgcolor ( normal_color & 0x0F ));
			SRC (scrn_bgcolor ( normal_color & 0xF0 ));
#else
			wattrset(a,COLOR_PAIR(2)|A_BOLD);
#endif

		}
		if (aflag && raiflag) {

#ifdef WIN32
		
			SRC (scrn_fgcolor ( box_color & 0x0F ));
			SRC (scrn_bgcolor ( normal_color & 0xF0 ));

#else
			wattrset(a,COLOR_PAIR(5)|A_BOLD);
#endif

		}

		    if (oofflag+aisflag+redflag+raiflag+losflag+ferrflag == 0) {                                     

#ifdef LINUX
				wattrset(a,COLOR_PAIR(1));
#else
				SRC (scrn_fgcolor ( normal_color & 0x0F ));
				SRC (scrn_bgcolor ( normal_color & 0xF0 ));
#endif
	            sprintf(alarmbuff, halfblank);                   
			}
#ifdef WIN32

		SRC (scrn_wclrline( alarm_win, 2, 1, 38 ));	
		SRC (scrn_wqgoprint( alarm_win, 2, 2, ATTR_NORMAL, alarmbuff ));

#else
		wmove(a,1,0);
		waddstr(a, alarmbuff);
		wattrset(a,COLOR_PAIR(1));
		wrefresh(a);
#endif
		
	}		// End if/else

#ifdef WIN32

		SRC (scrn_fgcolor ( normal_color & 0x0F ));
		SRC (scrn_bgcolor ( normal_color & 0xF0 ));
		SRC (scrn_wgocursor( full_scrnh, row, column ));
#else

	setsyx(y,x);
	doupdate();
	touchwin(a);
	touchwin(w);
	wnoutrefresh(a);
	wnoutrefresh(w);

#endif

	return(0);	 

}	/* End of Net_Board_Hdlr function */


/****************************************************************
*        NAME : Play_File(char *fname)
* DESCRIPTION : Function to play a file
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS : 
****************************************************************/
int Play_File(char *fname)
{

	int fd;

	iottp = iott;
	tptp = tpt;

	viptr = vinfo;

#ifdef LINUX
	fd = open(fname, O_RDONLY, 0666);
#endif

#ifdef WIN32
	fd = dx_fileopen(fname, O_RDONLY|O_BINARY);
#endif

	if (fd == -1) {
		sprintf(buff,"ERROR in open of %s in Play_File",fname);
		log_message3(buff);
		return(-1);
	}

// The following demonstrates a very simple preparation for playing a file

	memset(iottp, 0, sizeof (DX_IOTT));

	memset(tptp, 0, sizeof (DV_TPT) * 2);

	iott[0].io_fhandle = fd;
	iott[0].io_offset = 0;
	iott[0].io_length = -1;
	iott[0].io_type = IO_DEV | IO_EOT;

	tpt[0].tp_type = IO_CONT;
	tpt[0].tp_termno = DX_MAXTIME;
	tpt[0].tp_length = -1;
	tpt[0].tp_flags = TF_MAXTIME;

	tpt[1].tp_type =  IO_EOT;
	tpt[1].tp_termno = DX_MAXDTMF;
	tpt[1].tp_length = 1;
	tpt[1].tp_flags = TF_MAXDTMF;

	// Now we're ready to play

	if (dx_play(viptr->vrdev, iottp, tptp , EV_ASYNC) == -1) {
		
		sprintf(buff,"ERROR: %s for dev: %s in Play_File.",ATDV_ERRMSGP(viptr->vrdev),viptr->vname);
	
		log_message3(buff);
	
		return(-1);
	}

	viptr->vrstate = vsPLAYING;

	return(0);

}	/* End of Play_File() */

/****************************************************************
*        NAME : Rec_File(char *fname)
* DESCRIPTION : Function to record to file
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS : 
****************************************************************/
int Rec_File(char *fname)
{
	int fd;

	iottp = iott;
	tptp = tpt;

	viptr = vinfo;

#ifdef LINUX
	fd = open(fname, O_RDWR|O_CREAT|O_TRUNC , 0666);
#endif

#ifdef WIN32
	fd = dx_fileopen(fname, O_RDWR|O_BINARY|O_CREAT);
#endif

	if (fd == -1) {
		sprintf(buff,"ERROR in open of %s in Rec_File",fname);
		log_message3(buff);
		return(-1);
	}

	// The following demonstrates a very simple preparation for recording to a file

	memset(iottp, 0, sizeof (DX_IOTT));
		
	memset(tptp, 0, sizeof (DV_TPT) * 2);

	iott[0].io_fhandle = fd;
	iott[0].io_offset = 0;
	iott[0].io_length = -1;
	iott[0].io_type = IO_DEV | IO_EOT;

	tpt[0].tp_type = IO_CONT;
	tpt[0].tp_termno = DX_MAXTIME;
	tpt[0].tp_length = 300;	// Default to 30 second max time
	tpt[0].tp_flags = TF_MAXTIME;

	tpt[1].tp_type =  IO_EOT;
	tpt[1].tp_termno = DX_MAXDTMF;
	tpt[1].tp_length = 1;
	tpt[1].tp_flags = TF_MAXDTMF;

			// Now we're ready to RECORD
	if (dx_rec(viptr->vrdev, iottp, tptp , EV_ASYNC) == -1) {

		sprintf(buff,"ERROR: %s for dev: %s in Rec_File.",ATDV_ERRMSGP(viptr->vrdev),viptr->vname);
		
		log_message3(buff);
		
		return(-1);
	}

	viptr->vrstate = vsRECORDING;

	return(0);

} /* End of Rec_File() */

/****************************************************************
*        NAME :	usage()
* DESCRIPTION : prints command line usage info to screen
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS : 
****************************************************************/
void usage()
{
	printf("\nUsage: gcdiag <brd> <chn> <prot> <v>\n");
	printf("Where:\n");
	printf("<brd> is the board number. 1-99\n");
	printf("<chn> is the channel number 1-23 for T1; 1-30 for E1\n");
	printf("<prot> is the GC protocol name (pdk_us_mf_io, isdn, etc.) \n");
	printf("<voice> is device name of voice device (dxxxB1C1, etc.)\n");
printf("\n\nNOTE: For non-ISDN protocols, the <v> parameter is required.)\n\n");
}

/****************************************************************
*        NAME :	get_user_command()
* DESCRIPTION : Function to retrieve input from keyboard
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS : 
****************************************************************/
void get_user_command()
{
   /* init menu select to invalid */
	menu_number = 0; 
	menu_number = get_key();
	menu_switch(); 

#ifdef LINUX
	wmove(w,17,23);
	waddstr(w,"        ");
	wmove(w,17,23);
	wrefresh(w);
#endif

} 

/****************************************************************
*        NAME :	atoh(char *p)
* DESCRIPTION : Function to convert ascii to hex
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS : See send_msg()
****************************************************************/
int atoh(char *p)
{
   int i, j;

   while(*p == '0' || *p == 'X' || *p == 'x') p++; 
   for(i=0; (*p>='0'&&*p<='9')||(*p>='a'&&*p<='f')||(*p>='A'&&*p<='F'); p++)
   {
       if(*p >= '0' && *p <= '9')
          j = *p - '0';
       else if(*p >= 'a' && *p <= 'f')
          j = *p - 'a' + 10;
       else if(*p >= 'A' && *p <= 'F')
          j = *p - 'A' + 10;
       i = i * 16 + j;
   }
   return i;
}
    
/****************************************************************
*        NAME :	trace_func(int menu_num)
* DESCRIPTION : Function to kick off ISDN tracing
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
int trace_func(int menu_num)
{
	int rc;

#ifdef LINUX
	char temp_trace[64];
#endif

#ifdef WIN32
	
	int length;
	int i;
	char trace[60]; 
	char newtrace[60] = {0};
	char process[60] = {0};

	DWORD error;
	DWORD exitCode;
	
	sInfo.cb = sizeof(STARTUPINFO);
	sInfo.lpReserved = NULL;
	sInfo.lpReserved2 = NULL;
	sInfo.cbReserved2 = 0;
	sInfo.lpDesktop = NULL;
	sInfo.lpTitle = NULL;
	sInfo.dwFlags = 0;
	sInfo.dwX = 0;
	sInfo.dwY = 0;
	sInfo.dwFillAttribute = 0;
	sInfo.wShowWindow = SW_SHOW;

#endif

	switch(menu_num) {

        case 1:

#ifdef WIN32
			SRC (scrn_wqgoprint( main_win, 16, 4, ATTR_NORMAL, "Please enter the trace filename:" ));
			SRC (scrn_wqgoprint( main_win, 17, 6, ATTR_NORMAL, "Ex: C:\\temp\\trace.log" ));
			SRC (scrn_wgocursor( main_win, 16, 37 ));

#else	/*LINUX*/
			wmove(w,15,4);
			waddstr(w,"Please enter the trace filename:\n");
			wmove(w,16,4);
			waddstr(w,"Ex: /tmp/trace.log");
			wmove(w,15,37);
			wrefresh(w);
#endif

			getustr(trace_name);

			StartTraceFlag = 1;
			
		/* Start Trace */
		if ((rc = gc_StartTrace(dtidev, trace_name)) != GC_SUCCESS) {
				sprintf(buff,"Error in StartTrace, rc = %X: Check to see if directory is valid", rc);
				log_message3(buff);
			
				return 1;
			}

			sprintf(buff,"Tracing started: %s ", trace_name);
			log_message3(buff);
			
			return 0;

        case 2:

			if((rc = gc_StopTrace(dtidev)) != GC_SUCCESS) {

				sprintf(buff,"Error in StopTrace, rc = %X", rc);
				log_message3(buff);
			
				return 1;
			}
			
			sprintf(buff,"Tracing stopped: %s ", trace_name);
			log_message3(buff);
	   
			StartTraceFlag = 0;
           
			return 0;
	 
		case 3:
			
			if(trace_name[0] == '\0') {

				sprintf(buff,"Error StartTrace First !!!");
				log_message3(buff);
               
				return 1;  
			}
			
			if(StartTraceFlag == 1) {
				
				if((rc = gc_StopTrace(dtidev)) != GC_SUCCESS) {

				sprintf(buff,"Error in StopTrace, rc = %X", rc);
				log_message3(buff);
			
				return 1;
				
				}
			
				sprintf(buff,"Tracing stopped: %s ", trace_name);
				log_message3(buff);
	   
				StartTraceFlag = 0;
					
			}
			
#ifdef WIN32		

		sprintf (trace, "C:\\progra~1\\Dialogic\\bin\\pritrace.exe ");
				
		strcat(trace, trace_name);		// Builds command string
 											// Starts pritrace.exe to decode log file
			
			if(!CreateProcess(NULL, trace, NULL, NULL, FALSE, 0, NULL, NULL, &sInfo, &pInfo)) {
				
				error = GetLastError();
				sprintf(buff,"Error CreateProcess for pritrace %d!!!", error);
				log_message3(buff);
				dspMainMenu();
			}
 

			SRC ( scrn_clrwin( alarm_win ));
			SRC ( scrn_clrwin( main_win ));
			SRC ( scrn_clrwin( message_win ));
			
			dspMainMenu();
			dspAlarms();

			sprintf(buff,"Processing log file...");
			log_message3(buff);
			
			
			WaitForSingleObject( pInfo.hProcess, 5000L );

			do {									// polls to wait for pritrace to complete

				if( GetExitCodeProcess( pInfo.hProcess, &exitCode ) == 0 ){
				
					error = GetLastError();
					sprintf(buff,"GetExitCodeProcess: pritrace %x!!!", error);
					log_message3(buff);
					dspMainMenu();
				}
			
			}while ( exitCode != 0 );


			sprintf(buff,"Processing log file...DONE!");
			log_message3(buff);
			
			length = strlen( trace_name );
			
			for(i=0; i<length-4; i++)						// removes extension
				newtrace[i] = trace_name[i];
									 
			newtrace[i] = '\0';								// adds NULL termination
			strcat( newtrace, ".res" );						// adds ".res" extension
			
			sprintf (process, "C:\\Progra~1\\Window~1\\Accessories\\wordpad.exe "); 
			
															
			strcat( process, newtrace );					// builds executable string
															//LAUNCHES Wordpad.exe w/ tracing file
														
			if(!CreateProcess(NULL , process, NULL, NULL, FALSE, 0, NULL, NULL, &sInfo, &pInfo)) {

				error = GetLastError();
				sprintf(buff,"Error CreateProcess Wordpad.exe %d!!!", error);
				log_message3(buff);
				dspMainMenu();
			}


#else	/*LINUX*/		
			if(fork() == 0){    // Child....
			 
		   		endwin();
	       		execl("/usr/dialogic/bin/pritrace","/usr/dialogic/bin/pritrace", trace_name, 0); 
			}
			else
				wait(0);    // Parent.....

			strcpy(temp_trace, trace_name);
			strcat(temp_trace, ".res");
			sprintf(buff,"vi %s", temp_trace);
			system(buff);
			reset_prog_mode();
#endif

           return 0;

    }

	return 0;
}

/****************************************************************
*        NAME :	send_msg(char *s)
* DESCRIPTION : Function to send ISDN message
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
void send_msg(char *s)
{
   unsigned char *msg_ptr;
   int  msg_type;
   IE_BLK ie;

   while(*s == ' ' || *s == '\t') s++;
   if(*s == '#' || *s == '\r' || *s == '\n' || *s == ';') return;
   msg_type = atoh(s);        /* msg_type */
   while(*s != ' ' && *s != '\t' && *s != ',') s++;
   while(*s == ' ' || *s == '\t' || *s == ',') s++;
   ie.length = atoh(s);       /* msg_length */
   while(*s != ' ' && *s != '\t' && *s != ',') s++;
   while(*s == ' ' || *s == '\t' || *s == ',') s++;
   for(msg_ptr=ie.data;*s!='\n'&&*s!='\r'&&*s!=';'&& *s!='#';msg_ptr++)
   {
       *msg_ptr = atoh(s);     /* msg_data */
       while(*s!=' '&&*s!='\t'&&*s!= ','&&*s!=';'&&*s!='\n'&&*s!='\r'&&*s!='#') s++;
       while(*s == ' ' || *s == '\t' || *s == ',') s++;
   }        
   *msg_ptr = '\0'; 
   /* GFG ????????
   if(gc_SndMsg(linfo[(chan-1)].crn, msg_type, &ie) != 0);
	endwin();
	exit(0);
	*/

}

/****************************************************************
*        NAME :	menu_switch()
* DESCRIPTION : Function to switch to next level menu
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
void menu_switch()
{
	char buff[80];
	int rc;	
	int chn_num;
	int ccwc;
	int channel_state;
	char str[260];
#ifdef WIN32		
	int x,y;
#endif

  switch(menu_number) { 
   
	case 1:
	   
#ifdef WIN32		
		SRC (scrn_clrwin( main_win ));
#else /*LINUX*/
		wclear(w);
#endif
		set_call_parameter_menu();
		
		while (menu_number2 != 27) {

#ifdef WIN32
			set_call_parameter_menu_switch();
			SRC (scrn_clrwin( main_win ));
#else
			wclear(w);
#endif
			set_call_parameter_menu();

		}  /* End of while-statement */        

					/* display user menu */
#ifdef LINUX
	   wclear(w);
#endif
	   dspMainMenu();
      
	   break;

	case 2:

		if(!isdn) {

			sprintf(buff,"ISDN only selection. For CAS, ANI is procotol dependent ");
			log_message3(buff);		
			
			break;
			
		}


#ifdef WIN32
		
		SRC (scrn_wgetcursor( main_win, &x, &y ));
		SRC (scrn_wqgoprint( main_win, 19, 35, ATTR_NORMAL, "                     "  ));
		SRC (scrn_wqgoprint( main_win, 19, 35, ATTR_NORMAL, "Get calling party number(ANI) (Y/N)?" ));
		SRC (scrn_wgocursor( main_win, 19, 72 ));
		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));
#else
		wmove(w,17,35);
		waddstr(w,"                     ");
		wmove(w,17,35);
		waddstr(w,"get calling party number(ANI) (Y/N) ");
		wrefresh(w);
#endif		
		
		getustr(str);

#ifdef WIN32
		
		SRC (scrn_wqgoprint( main_win, 18, 35, ATTR_NORMAL, "                        "  ));
		SRC (scrn_wgocursor( main_win, x, y ));
#else
		wmove(w,17,35);
		waddstr(w,"                     ");
		wrefresh(w);
#endif		

		if((strcmp(str,"y") == 0) || (strcmp(str,"Y") == 0)) {
			
			ani_menu();
			ani_switch();
      	}

#ifdef LINUX
		wclear(w);
#endif
		dspMainMenu();
		
		break;

   case 3:
		
	   if(!isdn) {

			sprintf(buff,"Feature not supported with CAS - ISDN ONLY!");
			log_message3(buff);		
			
			break;
			
		}

#ifdef WIN32

	    SRC (scrn_clrwin( main_win ));
		SRC (scrn_wqgoprint( main_win, 3, 2, ATTR_NORMAL, "MAIN MENU/Maintenance Request Menu" ));
		SRC (scrn_wqgoprint( main_win, 5, 2, ATTR_NORMAL, " 0. IN SERVICE" ));
		SRC (scrn_wqgoprint( main_win, 6, 2, ATTR_NORMAL, " 1. MAINTENANCE" ));
		SRC (scrn_wqgoprint( main_win, 7, 2, ATTR_NORMAL, " 2. OUT OF SERVICE" ));
		SRC (scrn_wqgoprint( main_win, 9, 4, ATTR_NORMAL, "Please enter choice: " ));
		SRC (scrn_wgocursor( main_win, 9, 25 ));
		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));

#else /* LINUX */

		wclear(w);
		wmove(w,0,0);
		waddstr(w,"MAIN MENU/Maintenance Request Menu");
		wmove(w,10,20);
		waddstr(w," 0. IN SERVICE");
		wmove(w,11,20);
		waddstr(w," 1. MAINTENANCE");
		wmove(w,12,20);
		waddstr(w," 2. OUT OF SERVICE");
		wmove(w,14,20);
		waddstr(w,"Please enter choice: ");
		wrefresh(w);
#endif		

        channel_state = get_key();

		switch (channel_state) {

			case 0:
			 
				channel_state = GCLS_INSERVICE;
				
				break;
			case 1:
			 
				channel_state = GCLS_MAINTENANCE;
			 
				break;
			case 2:
			 
				channel_state = GCLS_OUT_OF_SERVICE;
			 
				break;
			
			default:
           		
				dspMainMenu();
           		
				break;        
	}


#ifdef LINUX
	 wclear(w);
#endif

//	 dspMainMenu(); //EL to check out
	
	 sprintf(buff,"Sending Maintenance Request on %s",linfo[(chan-1)].lname);
	 log_message1(buff);
	 
	 rc = gc_SetChanState(linfo[(chan -1)].lndev, channel_state, EV_ASYNC);
	 
	 if(rc) {
		
		 sprintf(buff,"Maintenance Request fialed on %s",linfo[(chan-1)].lname);
		
		 log_message3(buff);
	 }

#ifdef LINUX
	 wrefresh(w);
#endif

     dspMainMenu();
	 
	 break;

   case 4:
     
	   display_information_menu( );
	   display_information_switch( );

#ifdef LINUX
	   wclear(w);
#endif

	   dspMainMenu(); //EL to check out
		
		break;

   case 5: /* Drop the call */
       
		if (linfo[(chan -1)].crn != 0){

			sprintf(buff,"Dropping Call on %s", linfo[(chan -1)].lname);
			log_message1(buff);
			GRC(gc_DropCall(linfo[(chan -1)].crn, GC_NORMAL_CLEARING, EV_ASYNC));
		} 
		else {

			sprintf(buff,"No call active to be dropped for %s",linfo[(chan-1)].lname);
			log_message3(buff);
		}
		
		break;

   case 6:
        
	   if (linfo[(chan-1)].lnstate == lsOOS) {
			sprintf(buff,"Line %s in BLOCKED state",linfo[(chan-1)].lname);
			log_message3(buff);
			break;
		}

#ifdef WIN32
	    SRC (scrn_wqgoprint( main_win, 19, 34, ATTR_NORMAL, "Please enter number to call: "  ));
		SRC (scrn_wgocursor( main_win, 19, 64 ));
		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));
#else		
		mvwprintw(w,17,32,"Please enter number to call: ");
		wrefresh(w);
#endif

		getustr(called_num);
		
		dspMainMenu();

		sprintf(buff,"Placing Call on %s", linfo[(chan -1)].lname);
		log_message1(buff);
		
		
		if (isdn)
		{
		GRC(gc_MakeCall(linfo[(chan -1)].lndev,&linfo[(chan-1)].crn,called_num,&mkcall,0, EV_ASYNC));
		}
		else
		{
		GRC(gc_MakeCall(linfo[(chan -1)].lndev,&linfo[(chan-1)].crn,called_num,NULL,0, EV_ASYNC));
		}

   
		break;

   case 7:
		
	   if(voice == 1) {

#ifdef WIN32
		SRC (scrn_wqgoprint( main_win, 19, 32, ATTR_NORMAL, "Please enter file to play: "  ));
		SRC (scrn_wgocursor( main_win, 19, 59 ));
		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));
#else
	    wmove(w,18,20);
	    waddstr(w,BLANK); 
		wmove(w,18,20);
		waddstr(w,"Please enter file name to play: ");
#endif

		getustr(filename);

#ifdef LINUX
		wrefresh(w);
#endif
		
		rc =(Play_File(&filename[0]));
		
		if (rc == 0)
			log_message1("Play initiated......");
		
		}
		else {
          
			log_message3("No voice resource specified on command line");
      
		}

#ifdef LINUX
		wclear(w);
#endif
		dspMainMenu();
      
		break;
				
   case 8:

		if(voice == 1) {

#ifdef WIN32
			SRC (scrn_wqgoprint( main_win, 19, 32, ATTR_NORMAL, "Please enter file to record: "  ));
			SRC (scrn_wgocursor( main_win, 19, 61 ));
			SRC (scrn_wgetcursor( full_scrnh, &row, &column ));
#else			
			wmove(w,18,20);
			waddstr(w,BLANK); 
			wmove(w,18,20);
			waddstr(w,"Please enter file name to record: ");
#endif
			getustr(filename);

#ifdef LINUX
			wrefresh(w);
#endif	

			rc =(Rec_File(&filename[0]));
      
			if (rc == 0)
				log_message1("Record initiated......");
		}
		else {
          
			log_message3("No voice resource specified on command line");
		}

		dspMainMenu();

		break;

   case 9:
      
	   if (voice == 1) {
		
		viptr = vinfo;
		
		if((rc = dx_stopch(viptr->vrdev,EV_ASYNC)) != 0) {
			log_message3("Error in stop_record ");
			break;
		}
		else {
	       log_message1("Stopping record/play");
	    }
      
	   }

      break;
   
   case 10:
      /* Start/stop trace */

	   	 if(!isdn) {

			sprintf(buff,"Feature not currently support on SpringWare - ISDN ONLY!");
			log_message3(buff);		
			
			break;
			
		}

#ifdef WIN32
	    SRC (scrn_clrwin( main_win ));
		SRC (scrn_wqgoprint( main_win, 3, 2, ATTR_NORMAL, "MAIN MENU/Tracing Menu" ));
		SRC (scrn_wqgoprint( main_win, 5, 2, ATTR_NORMAL, "  1. Start Trace" ));
		SRC (scrn_wqgoprint( main_win, 6, 2, ATTR_NORMAL, "  2. Stop Trace" ));
		SRC (scrn_wqgoprint( main_win, 7, 2, ATTR_NORMAL, "  3. Browse Trace" ));
		SRC (scrn_wqgoprint( main_win, 8, 2, ATTR_NORMAL, "  ESC Exit" ));
		SRC (scrn_wqgoprint( main_win, 10, 4, ATTR_NORMAL, "Please enter choice: " ));
		SRC (scrn_wgocursor( main_win, 10, 26 ));
		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));
#else  /* LINUX */
		wclear(w);
		wmove(w,10,4);
		waddstr(w,"1. Start Trace");
		wmove(w,11,4);
		waddstr(w,"2. Stop Trace");
		wmove(w,12,4);
		waddstr(w,"3. Browse Trace");
	    wmove(w,14,4);
		waddstr(w,"Please enter choice: ");
		wrefresh(w);
#endif      
	   menu_num = get_key();

      switch(menu_num) {

		case 1:
            trace_func(1);
			
			break;
	 
		case 2:
            trace_func(2);
	    
			break;
		case 3:
            trace_func(3);
            
			break;
        case 27:
            break;
	 
		default:

#ifdef LINUX
			wmove(w,21,20);
			waddstr(w,"Invalid choice...\n");
			wrefresh(w);
#endif			
			break;
      }
      /* display user menu */

#ifdef LINUX
		wclear(w);
#endif
		dspMainMenu();
		
		break; 

   case 11:

#ifdef WIN32
	   
		SRC (scrn_wqgoprint( main_win, 19, 35, ATTR_NORMAL, "Please enter channel to restart: " ));
		SRC (scrn_wgocursor( main_win, 19, 68 ));
#else
		wmove(w,17,35);
		waddstr(w,"                                   ");
		wmove(w,17,35);
		waddstr(w,"Please enter channel to restart: ");
#endif

	   chn_num = get_key();

#ifdef LINUX
		wmove(w,17,35);
		waddstr(w,"                                   ");
#endif      
	   
		if(chn_num <= maxdti) {
			sprintf(buff,"Restarting device %s",linfo[(chn_num-1)].lname);
			log_message1(buff);
			GRC(gc_ResetLineDev(linfo[(chn_num-1)].lndev, EV_ASYNC));
		}
		else {
#ifdef LINUX
		   wmove(w,18,20);
	       waddstr(w,"Invalid channel number");
		   wrefresh(w);
#endif		
		}
  
      break; 

   case 12:
#ifdef WIN32

		SRC (scrn_wclrline( message_win, 1, 1, 80 ));
	    SRC (scrn_wclrline( message_win, 2, 1, 80 ));
		SRC (scrn_wclrline( message_win, 3, 1, 80 )); 
#else	   
		wmove(l,2, 0);
		waddstr(l,BLANK);
		wmove(l,0, 0);
		waddstr(l,BLANK);
		wmove(l,1, 0);
		waddstr(l,BLANK);
		wrefresh(l);
#endif	  
	  			
		break;

   case 13:
	   	
#ifdef WIN32

	    SRC (scrn_wqgoprint( main_win, 19, 35, ATTR_NORMAL, "Change current channel to: " ));
		SRC (scrn_wgocursor( main_win, 19, 63 ));
#else
		wmove(w,18,35);
		wprintw(w,"Change current channel to: ");
#endif
		
		ccwc = get_key();

		if(ccwc <= maxdti) {

			if (voice) {

				viptr = vinfo;
				Unmarry_TS(&linfo[(chan-1)], viptr);
			if ( (!isdn) && (!ipt) )
		GRC(gc_Detach(linfo[(chan-1)].lndev,viptr->vrdev,EV_SYNC));
        		chan = ccwc;
			if ( (!isdn) && (!ipt) )
		GRC(gc_Attach(linfo[(chan-1)].lndev,viptr->vrdev,EV_SYNC));
				Marry_TS(&linfo[(chan-1)], viptr);
			}
        
			chan = ccwc;

		}
		else {

#ifdef LINUX
			wmove(w,18,0);
			waddstr(w,BLANK);
			wmove(w,18,20);
			wprintw(w,"Invalid channel number");
			wrefresh(w);
#endif
		}
	
				
		if(gc_SetCallingNum(linfo[(chan-1)].lndev,"9739933000") != 0) {
		
			sprintf(buff,"Error setting calling number on new channel");
			log_message3(buff);
		}

#ifdef LINUX
	     wclear(w);
#endif

		sprintf(buff,"Calling Number has been reset to 9739933000");
		log_message3(buff);

		dspMainMenu();
  
		break;
        
   case 27:

#ifdef LINUX

		wclear(w);	
		wrefresh(w);
		endwin();
#endif

      Exit_Prog();
      
	  break;

   case 0:
      break;
      
   default:

#ifdef LINUX
		wmove(w,23,20);
		waddstr(w,"Invalid Menu Number!!  Please pick a valid number...\n");
		wrefresh(w);
#endif
      break;
  } /*End of switch-statement */

#ifdef LINUX
	  wmove(w,17,23);
	  waddstr(w,"      ");  
	  wmove(w,17,23);
	  wrefresh(w);
#endif

}

/****************************************************************
*        NAME :	dspMainMenu()
* DESCRIPTION : Function to display main menu to screen
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
void dspMainMenu( )
{

#ifdef WIN32

		SRC (scrn_fgcolor ( normal_color & 0x0F ));
		SRC (scrn_bgcolor ( normal_color & 0xF0 ));
		SRC (scrn_clrwin( main_win ));
		SRC (scrn_wqgoprint( main_win, 2, 1, ATTR_NORMAL, " Main Menu "  ));
		SRC (scrn_wqgoprint( main_win, 4, 3, ATTR_NORMAL, "1.  set call parameter"  ));
		SRC (scrn_wqgoprint( main_win, 5, 3, ATTR_NORMAL, "2.  request calling party number (ANI)"  ));
		SRC (scrn_wqgoprint( main_win, 6, 3, ATTR_NORMAL, "3.  send maintenance request (ISDN Only)"  ));
		SRC (scrn_wqgoprint( main_win, 7, 3, ATTR_NORMAL, "4.  display information"  ));
		SRC (scrn_wqgoprint( main_win, 8, 3, ATTR_NORMAL, "5.  drop call"  ));
		SRC (scrn_wqgoprint( main_win, 9, 3, ATTR_NORMAL, "6.  make call"  ));
#else
		wclear(w);
		wmove(w,1,0);
		waddstr(w," Main Menu ");
		wmove(w,3,0);
		waddstr(w,"1.  set call parameter\n");
		waddstr(w,"2.  request calling party number (ANI)\n");
		waddstr(w,"3.  send maintenance request (ISDN Only)\n");
		waddstr(w,"4.  display information\n");
		waddstr(w,"5.  drop call\n");
		waddstr(w,"6.  make call\n");
#endif

		if (voice == 1)	{

#ifdef WIN32

			SRC (scrn_wqgoprint( main_win, 10, 3, ATTR_NORMAL, "7.  play"  ));
			SRC (scrn_wqgoprint( main_win, 11, 3, ATTR_NORMAL, "8.  record"  ));
			SRC (scrn_wqgoprint( main_win, 12, 3, ATTR_NORMAL, "9.  stop play/record"  ));
#else
			waddstr(w,"7.  play\n");
			waddstr(w,"8.  record\n"); 
			waddstr(w,"9.  stop play/record\n");
#endif			
		
		}
		else {

#ifdef WIN32
			SRC (scrn_wqgoprint( main_win, 10, 3, ATTR_NORMAL, "7.  play (N/A)"  ));
			SRC (scrn_wqgoprint( main_win, 11, 3, ATTR_NORMAL, "8.  record (N/A)"  ));	
			SRC (scrn_wqgoprint( main_win, 12, 3, ATTR_NORMAL, "9.  stop play/record (N/A)"  ));
#else
			waddstr(w,"7.  play (NA)\n");
			waddstr(w,"8.  record (NA)\n");
			waddstr(w,"9.  stop play/record (NA)\n");
#endif
			
		}

#ifdef WIN32

		SRC (scrn_wqgoprint( main_win, 13, 3, ATTR_NORMAL, "10. start/stop/browse trace"  ));
		SRC (scrn_wqgoprint( main_win, 14, 3, ATTR_NORMAL, "11. restart and waitcall"  ));
		SRC (scrn_wqgoprint( main_win, 15, 3, ATTR_NORMAL, "12. Clear LOG Messages"  ));
		SRC (scrn_wqgoprint( main_win, 16, 3, ATTR_NORMAL, "13. change current channel"  ));
		SRC (scrn_wqgoprint( main_win, 17, 3, ATTR_NORMAL, "ESC exit"  ));
		SRC (scrn_wqgoprint( main_win, 19, 5, ATTR_NORMAL, "  Please enter choice: "  ));
		SRC (scrn_wgocursor( main_win, 19, 29 )); 
		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));
#else

		waddstr(w,"10. start/stop/browse trace\n");
		waddstr(w,"11. restart and waitcall\n");
		waddstr(w,"12. Clear LOG Messages\n");
		waddstr(w,"13. change current channel\n");
		waddstr(w,"ESC exit\n");
		wmove(w,17, 0);
		waddstr(w,"  Please enter choice: ");
		wrefresh(w);
#endif		

} /* end of dspMainMenu() */

/****************************************************************
*        NAME :	set_call_parameter_menu()
* DESCRIPTION : Function to display call parameter menu
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
void set_call_parameter_menu()
{

#ifdef WIN32	
		
		SRC (scrn_clrwin( main_win ));
		SRC (scrn_wqgoprint( main_win, 3, 1, ATTR_NORMAL, " MAIN MENU/Set Call Parameter Menu"  ));
		SRC (scrn_wqgoprint( main_win, 5, 2, ATTR_NORMAL, "   1.  change destination number type"  ));
		SRC (scrn_wqgoprint( main_win, 6, 2, ATTR_NORMAL, "   2.  change destination number plan"  ));
		SRC (scrn_wqgoprint( main_win, 7, 2, ATTR_NORMAL, "   3.  change facility coding value"  ));
		SRC (scrn_wqgoprint( main_win, 8, 2, ATTR_NORMAL, "   4.  change facility feature service"  ));
		SRC (scrn_wqgoprint( main_win, 9, 2, ATTR_NORMAL, "   5.  change presentation indicator"  ));
		SRC (scrn_wqgoprint( main_win, 10, 2, ATTR_NORMAL, "   6.  change calling party number"  ));
		SRC (scrn_wqgoprint( main_win, 11, 2, ATTR_NORMAL, "   7.  do not send calling party number" ));
		SRC (scrn_wqgoprint( main_win, 12, 2, ATTR_NORMAL, "   8.  change info. transfer capability"  ));
		SRC (scrn_wqgoprint( main_win, 13, 2, ATTR_NORMAL, "   9.  change user info. layer 1 protocol" ));
		SRC (scrn_wqgoprint( main_win, 14, 2, ATTR_NORMAL, "   10. change user rate"  ));
		SRC (scrn_wqgoprint( main_win, 15, 2, ATTR_NORMAL, "   11. change called party number"  ));
		SRC (scrn_wqgoprint( main_win, 17, 4, ATTR_NORMAL, "      Please enter choice: "  ));
		SRC (scrn_wgocursor( main_win, 17, 31 ));
		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));
	
#else

		wclear(w);
		wmove(w,2,0);
		waddstr(w,"MAIN MENU/Set Call Parameter Menu");
		wmove(w,4,2);
		waddstr(w,"    1.  change destination number type\n");
		wmove(w,5,2);
		waddstr(w,"    2.  change destination number plan\n");
		wmove(w,6,2);
		waddstr(w,"    3.  change facility coding value\n");
		wmove(w,7,2);
		waddstr(w,"    4.  change facility feature service\n");
		wmove(w,8,2);
		waddstr(w,"    5.  change presentation indicator\n");
		wmove(w,9,2);
		waddstr(w,"    6.  change calling party number\n");
		wmove(w,10,2);
		waddstr(w,"    7.  do not send calling party number\n");
		wmove(w,11,2);                                  
		waddstr(w,"    8.  change info. transfer capability");
		wmove(w,12,2);
		waddstr(w,"    9.  change user info. layer 1 protocol");
		wmove(w,13,2);
		waddstr(w,"    10. change user rate");
		wmove(w,14,2);
		waddstr(w,"    11. change called party number");
		wmove(w,15,2);
		waddstr(w,"       Please enter choice: ");
		
		wrefresh(w);
		
#endif
		
		menu_number2 = get_key();
}

/****************************************************************
*        NAME :	set_call_parameter_menu_switch()
* DESCRIPTION : Function to display and switch call parameters
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
void set_call_parameter_menu_switch()
{
  
	BYTE destination_number_type;
  	BYTE destination_number_plan;
  	BYTE origination_number_type;
  	BYTE origination_number_plan;
  	BYTE facility_coding_value;
  	BYTE facility_feature_service;

#ifdef WIN32
	SRC (scrn_clrwin( main_win ));
#else	
	wclear(w);
#endif	
  
	switch(menu_number2) { 
		
		case 1:
#ifdef WIN32
 
		SRC (scrn_wqgoprint( main_win, 3, 2, ATTR_NORMAL, "MAIN MENU/Set Call Parameter/Destination Number Type Menu"  ));
 		SRC (scrn_wqgoprint( main_win, 6, 10, ATTR_NORMAL, "0.  Unknown"  ));
 		SRC (scrn_wqgoprint( main_win, 7, 10, ATTR_NORMAL, "1.  International Number"  ));
 		SRC (scrn_wqgoprint( main_win, 8, 10, ATTR_NORMAL, "2.  National Number"  ));
 		SRC (scrn_wqgoprint( main_win, 9, 10, ATTR_NORMAL, "4.  Subscriber Number"  ));
 		SRC (scrn_wqgoprint( main_win, 10, 10, ATTR_NORMAL,"6.  Abbreviated Number"  ));
 		SRC (scrn_wqgoprint( main_win, 11, 10, ATTR_NORMAL,"ESC exit"  ));
 		SRC (scrn_wqgoprint( main_win, 13, 12, ATTR_NORMAL, "Please enter a value for Destination Number Type: "  ));
 		SRC (scrn_wgocursor( main_win, 13, 63 ));
		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));
#else
	    wmove(w,2,0);
		waddstr(w,"MAIN MENU/Set Call Parameter/Destination Number Type Menu");
		wmove(w,5,10);
		waddstr(w,"0.  Unknown");
		wmove(w,6,10);
		waddstr(w,"1.  International Number");
		wmove(w,7,10);
		waddstr(w,"2.  National Number");
		wmove(w,8,10);
		waddstr(w,"4.  Subscriber Number");
		wmove(w,9,10);
		waddstr(w,"6.  Abbreviated Number");
		wmove(w,10,10);
		waddstr(w,"ESC exit");
		wmove(w,11,10);
		waddstr(w,"Please enter a value for Destination Number Type: ");
		wrefresh(w);
#endif
		
		destination_number_type = get_key();

		if(destination_number_type == 27) {

			dspMainMenu();
        
		break;
		}

		 my_makecall_blk.isdn.destination_number_type = destination_number_type;

		break;

   case 2:
		


#ifdef WIN32

		SRC (scrn_wqgoprint( main_win, 3, 2, ATTR_NORMAL, "MAIN MENU/Set Call Parameter/Destination Number Plan"  ));
		SRC (scrn_wqgoprint( main_win, 6, 10, ATTR_NORMAL, "0. Unknown"  ));
		SRC (scrn_wqgoprint( main_win, 7, 10, ATTR_NORMAL, "1. ISDN/Telephony number plan(Rec. E.164/E.163)"  ));
		SRC (scrn_wqgoprint( main_win, 8, 10, ATTR_NORMAL, "2. Telephony number plan(E.163)"  ));
		SRC (scrn_wqgoprint( main_win, 9, 10, ATTR_NORMAL, "9. Private number plan"  ));
		SRC (scrn_wqgoprint( main_win, 11, 12, ATTR_NORMAL, "Please enter a value for Destination Number Plan: "  ));
		SRC (scrn_wgocursor( main_win, 11, 62 ));
		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));
#else
		wmove(w,2,0);
		waddstr(w,"MAIN MENU/Set Call Parameter/Destination Number Plan");
		wmove(w,5,10);
		waddstr(w,"0. Unknown");
		wmove(w,6,10);
		waddstr(w,"1. ISDN/Telephony number plan(Rec. E.164/E.163)");
		wmove(w,7,10);
		waddstr(w,"2. Telephony number plan(E.163)");
		wmove(w,8,10);
		waddstr(w,"9. Private number plan");
		wmove(w,10,10);
		waddstr(w,"Please enter a value for Destination Number Plan: ");
		wrefresh(w);
#endif
        
		destination_number_plan = get_key();

		if(destination_number_plan == 27) {
			
			dspMainMenu();
			break;
		}
      
		my_makecall_blk.isdn.destination_number_plan = destination_number_plan;
		
		break;

   case 3:

#ifdef WIN32

		SRC (scrn_wqgoprint( main_win, 3, 2, ATTR_NORMAL, "MAIN MENU/Set Call Pararmeter Menu/Facility Coding Menu"  ));
		SRC (scrn_wqgoprint( main_win, 6, 20, ATTR_NORMAL, " 0. Clear coding value"  ));
		SRC (scrn_wqgoprint( main_win, 7, 20, ATTR_NORMAL, " 1. SDN"  ));
		SRC (scrn_wqgoprint( main_win, 8, 20, ATTR_NORMAL, " 2. MEGACOM 800"  ));
		SRC (scrn_wqgoprint( main_win, 9, 20, ATTR_NORMAL, " 3. MEGACOM"  ));
		SRC (scrn_wqgoprint( main_win, 10, 20, ATTR_NORMAL, " 6. ACCUNET"  ));
		SRC (scrn_wqgoprint( main_win, 11, 20, ATTR_NORMAL, " 8. I800"  ));
		SRC (scrn_wqgoprint( main_win, 12, 20, ATTR_NORMAL, "10. ATT MULTIQUEST"  ));
		SRC (scrn_wqgoprint( main_win, 14, 22, ATTR_NORMAL, "Please enter choice: "  ));
		SRC (scrn_wgocursor( main_win, 14, 43 ));
		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));

#else

		wmove(w,2,0);
		waddstr(w,"MAIN MENU/Set Call Pararmeter Menu/Facility Coding Menu");
		wmove(w,8,30);
		waddstr(w," 0. Clear coding value");
		wmove(w,9,30);
		waddstr(w," 1. SDN");
		wmove(w,10,30);
		waddstr(w," 2. MEGACOM 800");
		wmove(w,11,30);
		waddstr(w," 3. MEGACOM");
		wmove(w,12,30);
		waddstr(w," 6. ACCUNET");
		wmove(w,13,30);
		waddstr(w," 8. I800");
		wmove(w,14,30);
		waddstr(w,"10. ATT MULTIQUEST");
		wmove(w,16,30);
		waddstr(w,"Please enter choice: ");
		wrefresh(w);
		
#endif
		
		facility_coding_value = get_key();

		if(facility_coding_value == 27) {
			
			dspMainMenu();
			break;
		 }

      /* remove network spec. ie */
		if (facility_coding_value == 0) {

			my_makecall_blk.isdn.facility_coding_value = 0xff;
			my_makecall_blk.isdn.facility_feature_service = 0xff;
		}
		else {

			my_makecall_blk.isdn.facility_coding_value = facility_coding_value;
			my_makecall_blk.isdn.facility_feature_service = 1;
		}
      
		break;

   case 4:
	   
#ifdef WIN32

	    SRC (scrn_wqgoprint( main_win, 3, 2, ATTR_NORMAL, "MAIN MENU/Set Call Parameter Menu/Facility Feature Service Menu"  ));
		SRC (scrn_wqgoprint( main_win, 6, 10, ATTR_NORMAL, "0. Feature"  ));
		SRC (scrn_wqgoprint( main_win, 7, 10, ATTR_NORMAL, "1. Service"  ));
		SRC (scrn_wqgoprint( main_win, 9, 12, ATTR_NORMAL, "Please enter choice: "  ));
        SRC (scrn_wgocursor( main_win, 9, 33 ));
		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));

#else

		wmove(w,2,0);
	    waddstr(w,"MAIN MENU/Set Call Parameter Menu/Facility Feature Service Menu");
	    wmove(w,5,10);
	    waddstr(w,"0. Feature");
	    wmove(w,6,10);
	    waddstr(w,"1. Service");
	    wmove(w,8,10);
        waddstr(w, "Please enter choice: ");
	    wrefresh(w);

#endif
		
		facility_feature_service = get_key();

      
		if(facility_feature_service == 27) {
	        dspMainMenu();
		    break;
		}

		my_makecall_blk.isdn.facility_feature_service = facility_feature_service;
      
		break;

   case 5:

		presentation_indicator_menu();
      
		break;

   case 6:	/* Origination Number Type Menu */
	       
#ifdef WIN32

	    SRC (scrn_wqgoprint( main_win, 3, 2, ATTR_NORMAL, "MAIN MENU/Set Call Parameter/Origination Number Type Menu"  ));
		SRC (scrn_wqgoprint( main_win, 5, 10, ATTR_NORMAL, "0. Number is sent en_bloc"  ));
		SRC (scrn_wqgoprint( main_win, 6, 10, ATTR_NORMAL, "1. International number"  ));
		SRC (scrn_wqgoprint( main_win, 7, 10, ATTR_NORMAL, "2. National number"  ));
		SRC (scrn_wqgoprint( main_win, 8, 10, ATTR_NORMAL, "4. Subscriber number"  ));
		SRC (scrn_wqgoprint( main_win, 9, 10, ATTR_NORMAL, "5. Overlap sending"  ));
		SRC (scrn_wqgoprint( main_win, 11, 12, ATTR_NORMAL, "Please enter a value for Origination Number Type: "  ));
		SRC (scrn_wgocursor( main_win, 11, 62 ));
		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));
#else
		wmove(w,2,0);
	    waddstr(w,"MAIN MENU/Set Call Parameter/Origination Number Type Menu");
	    wmove(w,4,10);
		waddstr(w,"0. Number is sent en_bloc");
		wmove(w,5,10);
		waddstr(w,"1. International number");
		wmove(w,6,10);
		waddstr(w,"2. National number");
		wmove(w,7,10);
		waddstr(w,"4. Subscriber number");
		wmove(w,8,10);
		waddstr(w,"5. Overlap sending");
		wmove(w,11,10);
		waddstr(w,"Please enter a value for Origination Number Type: ");
		wrefresh(w);
	
#endif
	   
		origination_number_type = get_key();

		if(origination_number_type == 27) {

			dspMainMenu();
			
			break;
		}

		my_makecall_blk.isdn.origination_number_type = origination_number_type;
		
		/* Origination Number Plan */

#ifdef WIN32

		SRC (scrn_clrwin( main_win ));
		SRC (scrn_wqgoprint( main_win, 3, 2, ATTR_NORMAL, "MAIN MENU/Set Call Parameter/Origination Number Plan"  ));
		SRC (scrn_wqgoprint( main_win, 6, 10, ATTR_NORMAL, "0. Unknown"  ));
		SRC (scrn_wqgoprint( main_win, 7, 10, ATTR_NORMAL, "1. ISDN/Telephony number plan(Rec. E.164/E.163)"  ));
		SRC (scrn_wqgoprint( main_win, 8, 10, ATTR_NORMAL, "2. Telephony number plan(E.163)"  ));
		SRC (scrn_wqgoprint( main_win, 9, 10, ATTR_NORMAL, "9. Private number plan"  ));
		SRC (scrn_wqgoprint( main_win, 11, 12, ATTR_NORMAL, "Please enter a value for Origination Number Plan: "  ));
		SRC (scrn_wgocursor( main_win, 11, 62 ));
		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));

#else
        wclear(w);
	    wmove(w,2,0);
		waddstr(w,"MAIN MENU/Set Call Parameter/Origination Number Plan");
	    wmove(w,5,10);
		waddstr(w,"0. Unknown");
		wmove(w,6,10);
		waddstr(w,"1. ISDN/Telephony number plan(Rec. E.164/E.163)");
	    wmove(w,7,10);
        waddstr(w,"2. Telephony number plan(E.163)");
	    wmove(w,8,10);
		waddstr(w,"9. Private number plan");
	    wmove(w,10,10);
		waddstr(w,"Please enter a value for Origination Number Plan: ");
		wrefresh(w);

#endif
		origination_number_plan = get_key();
      
		if(origination_number_plan == 27) {

			dspMainMenu();
			break;
		}

		my_makecall_blk.isdn.origination_number_plan = origination_number_plan;

#ifdef WIN32

		SRC (scrn_wqgoprint( main_win, 16, 16, ATTR_NORMAL, "Please enter calling party number: "  ));
		SRC (scrn_wgocursor( main_win, 16, 51 ));
		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));
#else
	    wmove(w,15,20);
	    waddstr(w,"Please enter calling party number: ");
#endif

		getustr(calling_num);

		if(isdn) {

			my_makecall_blk.isdn.origination_phone_number[0] = calling_num[0];
			my_makecall_blk.isdn.origination_phone_number[1] = calling_num[1];
			my_makecall_blk.isdn.origination_phone_number[2] = calling_num[2];
			my_makecall_blk.isdn.origination_phone_number[3] = calling_num[3];
			my_makecall_blk.isdn.origination_phone_number[4] = calling_num[4];
			my_makecall_blk.isdn.origination_phone_number[5] = calling_num[5];
			my_makecall_blk.isdn.origination_phone_number[6] = calling_num[6];
			my_makecall_blk.isdn.origination_phone_number[7] = calling_num[7];
			my_makecall_blk.isdn.origination_phone_number[8] = calling_num[8];
			my_makecall_blk.isdn.origination_phone_number[9] = calling_num[9];
		
		}
		else {
		
			GRC(gc_SetCallingNum(liptr->lndev, calling_num));
		}
		
		break;

   case 7:
		
	    my_makecall_blk.isdn.origination_number_type = 0xFF;
		my_makecall_blk.isdn.origination_number_plan = 0xFF;
		my_makecall_blk.isdn.origination_phone_number[0] = '\0';
      
		break;
	    
   case 8:
		
	    bearer_capability_ie_menu();
      
		break;

   case 9:
		
	    user_info_layer1_proto();      
		user_info_layer1_proto_switch();      
      
		break;
   
   case 10:
		
	    user_rate();
		user_rate_switch();
      
		break;

   case 11:

#ifdef WIN32

	    SRC (scrn_wqgoprint( main_win, 17, 16, ATTR_NORMAL, "Please enter called party number: "  ));
		SRC (scrn_wgocursor( main_win, 17, 50 ));
		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));
#else

	    wmove(w,16,0);
	    waddstr(w, "Please enter called party number: ");
#endif

		getustr(called_num);

#ifdef LINUX
		wrefresh(w);
#endif
		break;

   case 12:

	   /* need to look at this */

#ifdef WIN32
	   SRC (scrn_wqgoprint( main_win, 17, 16, ATTR_NORMAL, "Return to Main Menu "  ));
#else
	   waddstr(w,"Return to Main Menu.\n");
#endif
	   break;

	default:
		
		/* need to look at this */
#ifdef WIN32
        SRC (scrn_wqgoprint( main_win, 17, 16, ATTR_NORMAL, "Invalid Menu Number!!  Please pick a valid number..." ));
#else
		waddstr(w,"Invalid Menu Number!!  Please pick a valid number...");
#endif
		break;
  
	} /*End of switch-statement */
  
  return;
}

/****************************************************************
*        NAME :	ani_menu
* DESCRIPTION : Function to display ANI menu
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
void ani_menu()
{

#ifdef WIN32

		SRC (scrn_clrwin( main_win )); 
		SRC (scrn_wqgoprint( main_win, 3, 2, ATTR_NORMAL, "MAIN MENU/Calling Party Number Menu"  ));
		SRC (scrn_wqgoprint( main_win, 8, 10, ATTR_NORMAL, "  1.  CPN (SID) preferred"  ));
		SRC (scrn_wqgoprint( main_win, 9, 10, ATTR_NORMAL, "  2.  BN (ANI) preferred"  ));
		SRC (scrn_wqgoprint( main_win, 10, 10, ATTR_NORMAL,"  3.  CPN (SID) only"  ));
		SRC (scrn_wqgoprint( main_win, 11, 10, ATTR_NORMAL,"  4.  BN (ANI) only"  ));
		SRC (scrn_wqgoprint( main_win, 12, 10, ATTR_NORMAL,"  ESC Exit"  ));
		SRC (scrn_wqgoprint( main_win, 14, 12, ATTR_NORMAL,"Please enter choice: "  ));
		SRC (scrn_wgocursor( main_win, 14, 33 ));
		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));
#else
		wclear(w);
		wmove(w,2,0);
		waddstr(w,"MAIN MENU/Calling Party Number Menu");
		wmove(w,7,24);
		waddstr(w,"  1.  CPN (SID) preferred");
		wmove(w,8,24);
		waddstr(w,"  2.  BN (ANI) preferred");
		wmove(w,9,24);
		waddstr(w,"  3.  CPN (SID) only");
		wmove(w,10,24);
		waddstr(w,"  4.  BN (ANI) only");
		wmove(w,11,24);
		waddstr(w,"  ESC Exit");
		wmove(w,13,24);
		waddstr(w,"Please enter choice: ");
		wrefresh(w);

#endif
		menu_number4 = get_key();

} /* end of ani_menu() */

/****************************************************************
*        NAME :	ani_switch()
* DESCRIPTION : Function to set ANI type
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
void ani_switch()
{
  
	switch(menu_number4) {
		case 1:
		case 2:
		case 3:
		case 4:
			req_type = menu_number4;  
			break;
		case 27:                       /* Exit */
			break;
		default:
#ifdef WIN32
		SRC (scrn_wqgoprint( main_win, 14, 12, ATTR_NORMAL,"Invalid choice!!!"));
#else
		waddstr(w,"Invalid choice!!!");
#endif
      break;
	}
} /* end of ani_switch() */

/****************************************************************
*        NAME :	presentation_indicator_menu()
* DESCRIPTION : Function to display presentation indicator menu
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
void presentation_indicator_menu()
{
  int rc;
//  char str[80];

#ifdef WIN32

		SRC (scrn_clrwin( main_win ));
		SRC (scrn_wqgoprint( main_win, 3, 2, ATTR_NORMAL, "MAIN MENU/Set Call Parameter Menu/Presentation Indicator Menu"  ));
		SRC (scrn_wqgoprint( main_win, 6, 10, ATTR_NORMAL, "  0. Presentation allowed"  ));
		SRC (scrn_wqgoprint( main_win, 7, 10, ATTR_NORMAL, "  1. Presentation restricted"  ));
		SRC (scrn_wqgoprint( main_win, 8, 10, ATTR_NORMAL, "  2. Number not available due to interworking"  ));
		SRC (scrn_wqgoprint( main_win, 9, 10, ATTR_NORMAL, "  3. Reserved"  ));
		SRC (scrn_wqgoprint( main_win, 11, 12, ATTR_NORMAL, "Please enter choice: "  ));
		SRC (scrn_wgocursor( main_win, 11, 33 ));
		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));
#else
		wclear(w);
		wmove(w,2,0);
		waddstr(w,"MAIN MENU/Set Call Parameter Menu/Presentation Indicator Menu");
		wmove(w,10, 20);
		waddstr(w,"  0. Presentation allowed");
		wmove(w,11, 20);
		waddstr(w,"  1. Presentation restricted");
		wmove(w,12, 20);
		waddstr(w,"  2. Number not available due to interworking");
		wmove(w,13, 20);
		waddstr(w,"  3. Reserved");
		wmove(w,15, 20);
		waddstr(w,"Please enter choice: ");
		wrefresh(w);
#endif

		rc = get_key();

		if(rc == 27) { 
    
			dspMainMenu();
		
			return;
		}

		
		/// EAL this must be checked out.
		// may need to add a gc_setparm()		

 
  	return;
}


/****************************************************************
*        NAME :	bearer_capability_ie_menu()
* DESCRIPTION : Function to display bearer_capability_ie_menu
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
void bearer_capability_ie_menu()
{

#ifdef WIN32

		SRC (scrn_clrwin( main_win ));
		SRC (scrn_wqgoprint( main_win, 3, 2, ATTR_NORMAL, "MAIN MENU/Set Call Parameter/Info. Transfer Capability Menu"  ));
		SRC (scrn_wqgoprint( main_win, 6, 6, ATTR_NORMAL, "     00. Speech"  ));
		SRC (scrn_wqgoprint( main_win, 7, 6, ATTR_NORMAL, "     08. Unrestricted digital information"  ));
		SRC (scrn_wqgoprint( main_win, 8, 6, ATTR_NORMAL, "     09. Restricted digital information"  ));
		SRC (scrn_wqgoprint( main_win, 9, 6, ATTR_NORMAL, "     16. 3.1 kHz audio" ));
		SRC (scrn_wqgoprint( main_win, 10, 6, ATTR_NORMAL,"     17. 7 kHz audio"  ));
  		SRC (scrn_wqgoprint( main_win, 11, 6, ATTR_NORMAL,"     24. video"  ));
		SRC (scrn_wqgoprint( main_win, 13, 13, ATTR_NORMAL, "Please enter choice: "  ));
		SRC (scrn_wgocursor( main_win, 13, 34 ));
		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));
#else
		wclear(w);
		wmove(w,2,0);
		waddstr(w,"MAIN MENU/Set Call Parameter/Info. Transfer Capability Menu");
		wmove(w,10,6);
		waddstr(w,"     00. Speech");
		wmove(w,11,6);
		waddstr(w,"     08. Unrestricted digital information");
		wmove(w,12,6);
		waddstr(w,"     09. Restricted digital information");
		wmove(w,13,6);
		waddstr(w,"     16. 3.1 kHz audio");
		wmove(w,14,6);
		waddstr(w,"     17. 7 kHz audio");
		wmove(w,15,6);
		waddstr(w,"     24. video");
		wmove(w,17,6);
		waddstr(w,"Please enter choice: ");
		wrefresh(w);
#endif


		info_transfer_cap_num = get_key();
	
		if(info_transfer_cap_num == 27)
			return;
  
		my_makecall_blk.isdn.BC_xfer_cap=info_transfer_cap_num;

	return;
}

/****************************************************************
*        NAME :	user_info_layer1_proto()
* DESCRIPTION : Function to display user_info_layer1_proto
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
void user_info_layer1_proto()
{

#ifdef WIN32

		SRC (scrn_clrwin( main_win ));
		SRC (scrn_wqgoprint( main_win, 3, 2, ATTR_NORMAL, "MAIN MENU/Set Call Parameter/User Info. Layer 1 Protocol Menu"  ));
		SRC (scrn_wqgoprint( main_win, 6, 6, ATTR_NORMAL, "     1. CCITT standardized rate adaption V.110/X.30"  ));
		SRC (scrn_wqgoprint( main_win, 7, 6, ATTR_NORMAL, "     2. Recommendation G.711 u-law"  ));
		SRC (scrn_wqgoprint( main_win, 8, 6, ATTR_NORMAL, "     4. Recommendation G.721 32kbit/s ADPCM & Recommendation I.460"  ));
		SRC (scrn_wqgoprint( main_win, 9, 6, ATTR_NORMAL, "     7. Non-CCITT standardized rate adaption"  ));
		SRC (scrn_wqgoprint( main_win, 10, 6, ATTR_NORMAL,"     12. 5 frame subrate data"  ));
		SRC (scrn_wqgoprint( main_win, 11, 6, ATTR_NORMAL,"     13. 10 frame subrate data"  ));
		SRC (scrn_wqgoprint( main_win, 12, 6, ATTR_NORMAL,"     14. 20 frame subrate data"  ));
		SRC (scrn_wqgoprint( main_win, 13, 6, ATTR_NORMAL,"     15. enchaned 20 frame subrate data"  ));
		SRC (scrn_wqgoprint( main_win, 14, 6, ATTR_NORMAL,"     16. remove layer 1 protocol field"  ));
  		SRC (scrn_wqgoprint( main_win, 15, 6, ATTR_NORMAL,"     ESC exit"  ));
		SRC (scrn_wqgoprint( main_win, 17, 8, ATTR_NORMAL,"Please enter choice: "  ));
		SRC (scrn_wgocursor( main_win, 17, 29 ));	
		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));

#else

		wclear(w);
		wmove(w,2,0);
		waddstr(w,"MAIN MENU/Set Call Parameter/User Info. Layer 1 Protocol Menu");
		wmove(w,8,6);
		waddstr(w,"     1. CCITT standardized rate adaption V.110/X.30");
		wmove(w,9,6);
		waddstr(w,"     2. Recommendation G.711 u-law");
		wmove(w,10,6);
		waddstr(w,"     4. Recommendation G.721 32kbit/s ADPCM & Recommendation I.460");
		wmove(w,11,6);
		waddstr(w,"     7. Non-CCITT standardized rate adaption");
		wmove(w,12,6);
		waddstr(w,"     12. 5 frame subrate data");
		wmove(w,13,6);
		waddstr(w,"     13. 10 frame subrate data");
		wmove(w,14,6);
		waddstr(w,"     14. 20 frame subrate data");
		wmove(w,15,6);
		waddstr(w,"     15. enchaned 20 frame subrate data");
		wmove(w,16,6);
		waddstr(w,"     16. remove layer 1 protocol field");
		wmove(w,17,6);
		waddstr(w,"     ESC exit");
		wmove(w,19,6);
		waddstr(w,"Please enter choice: ");
		wrefresh(w);

#endif

		usr_info_layer1_proto = get_key();

	return;
}

/****************************************************************
*        NAME : user_info_layer1_proto_switch()
* DESCRIPTION : Function to switch on info layer 1
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
void user_info_layer1_proto_switch()
{
   
	
   switch(usr_info_layer1_proto)
   {
      case 1:
      case 2:
      case 4:
      case 7:
      case 12:
      case 13:
      case 14:
      case 15:
		
		  my_makecall_blk.isdn.usrinfo_layer1_protocol=usr_info_layer1_proto;
		
		  break;
      case 16:
	
		  my_makecall_blk.isdn.usrinfo_layer1_protocol=0xFF;
      
		  break; 
      case 27:
						/* may need to be changed to preserve consistancy */
		  dspMainMenu();
//		  wmove(w,16,0);
//		  waddstr(w,"Exit to previous menu");
//		  wrefresh(w); 

			break;
      default:

			my_makecall_blk.isdn.usrinfo_layer1_protocol=usr_info_layer1_proto;
      
			break;              
   }

   return;
}

/****************************************************************
*        NAME : user_rate()
* DESCRIPTION : Function to display user_rate
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
void user_rate()
{

#ifdef WIN32

		SRC (scrn_clrwin( main_win ));
		SRC (scrn_wqgoprint( main_win, 3, 2, ATTR_NORMAL, "MAIN MENU/Set Call Parameter/User Rate Menu"  ));
		SRC (scrn_wqgoprint( main_win, 6, 6, ATTR_NORMAL, "3.   2.4 kbit/s Recommendation V.6 and X.1" ));
		SRC (scrn_wqgoprint( main_win, 7, 6, ATTR_NORMAL, "5.   4.8 kbit/s Recommendation V.6 and X.1" ));
		SRC (scrn_wqgoprint( main_win, 8, 6, ATTR_NORMAL, "8.   9.6 kbit/s Recommendation V.6 and X.1" ));
		SRC (scrn_wqgoprint( main_win, 9, 6, ATTR_NORMAL, "15.  56 kbit/s Recommendation V.6 and X.1" ));
		SRC (scrn_wqgoprint( main_win, 10, 6, ATTR_NORMAL,"16.  remove user rate field" ));
		SRC (scrn_wqgoprint( main_win, 11, 6, ATTR_NORMAL, "ESC  exit" ));
		SRC (scrn_wqgoprint( main_win, 13, 8, ATTR_NORMAL, "Please enter choice: " ));
		SRC (scrn_wgocursor( main_win, 13, 29 ));
		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));
#else
		wclear(w);
		wmove(w,2,0);
		waddstr(w,"MAIN MENU/Set Call Parameter/User Rate Menu");
		wmove(w,10,6);
		waddstr(w,"3.   2.4 kbit/s Recommendation V.6 and X.1");
		wmove(w,11,6);
		waddstr(w,"5.   4.8 kbit/s Recommendation V.6 and X.1");
		wmove(w,12,6);
		waddstr(w,"8.   9.6 kbit/s Recommendation V.6 and X.1");
		wmove(w,13,6);
		waddstr(w,"15.  56 kbit/s Recommendation V.6 and X.1");
		wmove(w,14,6);
		waddstr(w,"16.  remove user rate field");
		wmove(w,15,6);
		waddstr(w,"ESC  exit");
		wmove(w,17,6);
		waddstr(w,"Please enter choice: ");
		wrefresh(w);
#endif
		usr_rate = get_key();

	return;
}

/****************************************************************
*        NAME : user_rate_switch()
* DESCRIPTION : Function to set user in makecall block
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
void user_rate_switch()
{
   switch(usr_rate)
   {
      case 3:
      case 5:
      case 8:
      case 15:
		my_makecall_blk.isdn.usr_rate = usr_rate;
		break;
	  case 16:
		my_makecall_blk.isdn.usr_rate = 0xFF; 
		break;
      case 27:
						/* may need to be changed to preserve consistancy */
		dspMainMenu();
//		wmove(w,16,0);
//		waddstr(w,"Exit to previous menu");
//		wrefresh(w); 
      
		break;
      default:

		my_makecall_blk.isdn.usr_rate =usr_rate;

      break;
   }
}

/****************************************************************
*        NAME : display_information_menu( )
* DESCRIPTION : Function to display_information_menu
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
void display_information_menu( )
{

#ifdef WIN32

		SRC (scrn_clrwin( main_win ));
		SRC (scrn_wqgoprint( main_win, 3, 2, ATTR_NORMAL, "MAIN MENU/Set Call Parameter/User Rate Menu"  ));	
		SRC (scrn_wqgoprint( main_win, 5, 5, ATTR_NORMAL, " 1.  Display Called Party Subaddress (ISDN Only)"  ));
		SRC (scrn_wqgoprint( main_win, 6, 5, ATTR_NORMAL, " 2.  Display User-User Info. (ISDN Only)"  ));
		SRC (scrn_wqgoprint( main_win, 7, 5, ATTR_NORMAL, " 3.  Display B channel information"  ));
		SRC (scrn_wqgoprint( main_win, 8, 5, ATTR_NORMAL, " 4.  Display Call State information"  ));
		SRC (scrn_wqgoprint( main_win, 9, 5, ATTR_NORMAL, " ESC exit"  ));
		SRC (scrn_wqgoprint( main_win, 11, 7 , ATTR_NORMAL, "Please enter choice: "  ));
		SRC (scrn_wgocursor( main_win, 11, 28 ));
		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));
#else
		wclear(w);
		wmove(w,2,0);
		waddstr(w,"MAIN MENU/Display Information Menu");
		wmove(w,10,20);
		waddstr(w," 1.  Display Called Party Subaddress (ISDN Only)");
		wmove(w,11,20);
		waddstr(w," 2.  Display User-User Info. (ISDN Only)");
		wmove(w,12,20);
		waddstr(w," 3.  Display B channel information");
		wmove(w,13,20);
		waddstr(w," 4.  Display Call State information");
		wmove(w,14,20);
		waddstr(w," ESC exit");
		wmove(w,16, 20);
		waddstr(w,"    Please enter choice: ");
		wrefresh(w);
#endif
	
		menu_number5 = get_key();

		return;
}

/****************************************************************
*        NAME : display_information_switch( )
* DESCRIPTION : Function to display ISDN information, B Channel 
*				information and call state information 
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
void display_information_switch()
{
   int i;
   int chn_state;
   char str[80];

#ifdef WIN32
   int x,y;
   SRC (scrn_wgocursor( main_win, 19, 15 ));
#else
	wmove(w,18,15);
#endif

	switch(menu_number5) {
      
		case 1:

#ifdef WIN32
			SRC (scrn_wqgoprint( main_win, 14, 20, ATTR_NORMAL, "Called Party Subaddress: " ));
#else
			waddstr(w,"Called Party Subaddress: ");
#endif

			for(i = 0; i < 20; i++) {
		
				sprintf(str, "%d", *(called_sub_buf+i));
#ifdef WIN32
				SRC (scrn_wqgoprint( main_win, 14, (45 + i), ATTR_NORMAL, str ));
#else
				waddstr(w,str);
#endif
			} 
	 
#ifdef WIN32

			SRC (scrn_wqgoprint( main_win, 14, 65, ATTR_NORMAL, "******" ));
			SRC (scrn_wqgoprint( main_win, 16, 20, ATTR_NORMAL, "Press RETURN key to return to main menu" ));
			SRC (scrn_wgocursor( main_win, 16, 60 ));
			SRC (scrn_wgetcursor( full_scrnh, &row, &column ));
#else
			waddstr(w,"******");
			wmove(w,17,15);
			waddstr(w,"Press RETURN key to return to main menu");
			wrefresh(w);
#endif 
			getustr(str);
	 
			break;

		case 2:


#ifdef WIN32

			sprintf(str, "User-User Info. Length: %d", ((USRINFO_ELEM *)(uui_buf))->length);
			SRC (scrn_wqgoprint( main_win, 15, 25, ATTR_NORMAL, str ));
			sprintf(str, "User-User Info. Protocol Discriminator: %X", ((USRINFO_ELEM *)(uui_buf))->protocol_discriminator);
			SRC (scrn_wqgoprint( main_win, 16, 25, ATTR_NORMAL, str ));
			sprintf(str, "User-User Info. Message: %s", ((USRINFO_ELEM *)(uui_buf))->usrinformation);
			SRC (scrn_wqgoprint( main_win, 17, 25, ATTR_NORMAL, str ));
			SRC (scrn_wqgoprint( main_win, 19, 25, ATTR_NORMAL, "Press RETURN key to return to main menu" ));
			SRC (scrn_wgocursor( main_win, 19, 65 ));
			SRC (scrn_wgetcursor( full_scrnh, &row, &column ));
#else
			sprintf(str, "User-User Info. Length: %d", ((USRINFO_ELEM *)(uui_buf))->length);
			waddstr(w,str);
			wmove(w,16,10);
			sprintf(str, "User-User Info. Protocol Discriminator: %X", ((USRINFO_ELEM *)(uui_buf))->protocol_discriminator);
			waddstr(w,str);
			wmove(w,17,10);
			sprintf(str, "User-User Info. Message: %s", ((USRINFO_ELEM *)(uui_buf))->usrinformation);
			waddstr(w,str);
			wmove(w,18,9);
			waddstr(w,"Press RETURN key to return main menu");
			wrefresh(w);
#endif
			
			getustr(str);
			
			break;

		case 3:

					
			if((!dm3) && (!isdn)) {

				sprintf(buff,"Feature not supported with SpringWare/CAS combination");
				log_message3(buff);		
			
				break;
			
			}


#ifdef WIN32

			SRC (scrn_clrwin( main_win ));

			sprintf(str,"BOARD: %d", brd);
			SRC (scrn_wqgoprint( main_win, 5, 3, ATTR_NORMAL, str ));
			SRC (scrn_wqgoprint( main_win, 7, 3, ATTR_NORMAL,"CHAN:   " ));
			SRC (scrn_wgocursor( main_win, 7, 11 ));
			SRC (scrn_wgetcursor( full_scrnh, &row, &column ));

			for(i = 0; i < 15; i++) {

				SRC (scrn_wgetcursor( main_win, &x, &y ));
				
				y = y++;
				
				if( i > 9 )
					y = y + 3;
				else
					y = y + 2;

				sprintf(str," %d", i+1);

				SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL, str ));
         
			}

			SRC (scrn_wqgoprint( main_win, 8, 3, ATTR_NORMAL,"STATUS: " ));
			SRC (scrn_wgocursor( main_win, 8, 11 ));
			SRC (scrn_wgetcursor( full_scrnh, &row, &column ));


			for(i = 0; i < 15; i++) {

				GRC(gc_GetLinedevState(linfo[i].lndev, GCGLS_BCHANNEL, &chn_state));


				SRC (scrn_wgetcursor( main_win, &x, &y ));

				y = y++;
				
				if( i > 9 )
					y = y + 3;
				else
					y = y + 2;
								
				sprintf(str," %X", chn_state);


				SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL, str ));
		         
			}

			SRC (scrn_wqgoprint( main_win, 10, 3, ATTR_NORMAL,"CHAN:   " ));
			SRC (scrn_wgocursor( main_win, 10, 10 ));


			for(i = 15; i < maxdti; i++) {

				SRC (scrn_wgetcursor( main_win, &x, &y ));

				y = y++;
				y = y + 3;
						
				sprintf(str," %d", i+1);


				SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL, str ));
         	}

			SRC (scrn_wqgoprint( main_win, 11, 3, ATTR_NORMAL,"STATUS: " ));
			SRC (scrn_wgocursor( main_win, 11, 10 ));
	
			for(i = 15; i < maxdti; i++) {

				GRC(gc_GetLinedevState(linfo[i].lndev,GCGLS_BCHANNEL, &chn_state));
				
				SRC (scrn_wgetcursor( main_win, &x, &y ));

				y = y++;
				y = y + 3;
						
				sprintf(str," %X", chn_state);

				SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL, str ));
							
         	}


			SRC (scrn_wqgoprint( main_win, 14, 17, ATTR_NORMAL, "IN Service=0, Maintenance=1, OUT of Service=2" ));
			SRC (scrn_wqgoprint( main_win, 16, 18, ATTR_NORMAL,"Press RETURN key to return to main menu"));
			SRC (scrn_wgocursor( main_win, 16, 58 ));
			SRC (scrn_wgetcursor( full_scrnh, &row, &column ));
			
#else
			
         wclear(w);
         wmove(w,1,0);
         sprintf(str,"BOARD: %d", brd);
         waddstr(w,str);
         wmove(w,3,0);
         waddstr(w,"CHAN:   ");
         for(i = 0; i < 15; i++)
         {
            sprintf(str,"  %2d", i+1);
            waddstr(w,str);
         }
         wmove(w,4,0);
         waddstr(w,"STATUS: ");
         for(i = 0; i < 15; i++)
         {
            GRC(gc_GetLinedevState(linfo[i].lndev, GCGLS_BCHANNEL, &chn_state));
            sprintf(str,"  %2X", chn_state);
            waddstr(w,str);
         }
         wmove(w,6,0);
         waddstr(w,"CHAN:   ");
         for(i = 15; i < maxdti; i++)
         {
            sprintf(str,"  %2d", i+1);
            waddstr(w,str);
         }
         wmove(w,7,0);
         waddstr(w,"STATUS: ");
         for(i = 15; i < maxdti; i++)
         {
            GRC(gc_GetLinedevState(linfo[i].lndev,GCGLS_BCHANNEL, &chn_state));
            sprintf(str,"  %2X", chn_state);
            waddstr(w,str);
         }
         wmove(w,16,17);
         waddstr(w,"IN Service=0, Maintenance=1, OUT Service=2");
         wmove(w,18,19);
         waddstr(w,"Press RETURN key to return to main menu");
         wrefresh(w);
#endif			

			getustr(str);			
			
			break;

      case 4:

#ifdef WIN32

		  SRC (scrn_clrwin( main_win ));
		  sprintf(str,"BOARD: %d", brd);
		  SRC (scrn_wqgoprint( main_win, 5, 3, ATTR_NORMAL, str ));
		  SRC (scrn_wqgoprint( main_win, 7, 3, ATTR_NORMAL,"CHAN:   " ));
		  SRC (scrn_wgocursor( main_win, 7, 11 ));
		  SRC (scrn_wgetcursor( full_scrnh, &row, &column ));
   
		  for(i = 0; i < 15; i++) {

			  SRC (scrn_wgetcursor( main_win, &x, &y ));

				y = y++;
				
				if( i > 9 )
					y = y + 3;
				else
					y = y + 2;

				sprintf(str," %d", i+1);

 
				SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL, str ));
				
		  }

		  
		  SRC (scrn_wqgoprint( main_win, 8, 3, ATTR_NORMAL,"STATUS: " ));
		  SRC (scrn_wgocursor( main_win, 8, 11 ));
		  
		  for(i = 0; i < 15; i++) {
		
		  SRC (scrn_wgetcursor( main_win, &x, &y ));

			y = y++;
				
			if( i > 9 )
				y = y + 3;
			else
				y = y + 2;			

			  if (linfo[i].crn) {

				GRC(gc_GetCallState(linfo[i].crn, &chn_state));
		
		
				switch(chn_state) {

					case GCST_ALERTING:

						SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL," AL" ));

						break;

					case GCST_IDLE:

						SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL," ID" ));

						break;
			
					case GCST_NULL:

						SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL," NL" ));				

						break;
			
					case GCST_CONNECTED:

						SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL," CN" ));				

						break;

					case GCST_ACCEPTED:

						SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL," AC" ));

						break;
			
					case GCST_OFFERED:

						SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL," OF" ));

						break;
			
					case GCST_DISCONNECTED:

						SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL," DC" ));				

						break;
			
					case GCST_PROCEEDING:

						SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL," PR" ));				

						break;
			
					case GCST_ONHOLD:

						SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL," HL" ));				

						break;
			
					default:

						SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL," NL" ));				

						break;
				}
			}
			else

				SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL," NL" ));

			}

		  SRC (scrn_wqgoprint( main_win, 10, 3, ATTR_NORMAL,"CHAN:   " ));
		  SRC (scrn_wgocursor( main_win, 10, 10 ));

		  for(i = 15; i < maxdti; i++) {


			SRC (scrn_wgetcursor( main_win, &x, &y ));
			
			y = y++;
			y = y + 3;

			sprintf(str," %d", i+1);

			SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL, str ));


		  }

  		  	SRC (scrn_wqgoprint( main_win, 11, 3, ATTR_NORMAL,"STATUS: " ));
			SRC (scrn_wgocursor( main_win, 11, 10 ));
         
		  
		  for(i = 15; i < maxdti; i++) {

			  SRC (scrn_wgetcursor( main_win, &x, &y ));

				y = y++;
				y = y + 3;

				if (linfo[i].crn)	{
            
				  GRC(gc_GetCallState(linfo[i].crn, &chn_state));
			
				  switch(chn_state) {

						case GCST_ALERTING:

							SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL," AL" ));
 
							break;
			
						case GCST_IDLE:

							SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL," ID" ));

							break;
			
						case GCST_NULL:

							SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL," NL" ));

							break;
			
						case GCST_CONNECTED:

							SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL," CN" ));

							break;
			
						case GCST_ACCEPTED:

							SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL," AC" ));

							break;
			
						case GCST_OFFERED:

							SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL," OF" ));

							break;
			
						case GCST_DISCONNECTED:

							SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL," DC" ));

							break;
			
						case GCST_PROCEEDING:

							SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL," PR" ));

							break;
			
						case GCST_ONHOLD:

							SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL," HL" ));

							break;
			
						default:

							SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL," NL" ));

	           				break;
					}

				}
				else


					SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL," NL" ));

		  
		  }


		SRC (scrn_wqgoprint( main_win, 14, 17, ATTR_NORMAL,"NL = Null" ));
		SRC (scrn_wqgoprint( main_win, 15, 17, ATTR_NORMAL,"DC = Disc" ));
		SRC (scrn_wqgoprint( main_win, 16, 17, ATTR_NORMAL,"OF = Offered" ));
		SRC (scrn_wqgoprint( main_win, 14, 33, ATTR_NORMAL,"CN = Connected" ));
		SRC (scrn_wqgoprint( main_win, 15, 33, ATTR_NORMAL,"AL = Alerting" ));
		SRC (scrn_wqgoprint( main_win, 16, 33, ATTR_NORMAL,"ID = Idle" ));
		SRC (scrn_wqgoprint( main_win, 14, 49, ATTR_NORMAL,"AC = Accepted" ));
		SRC (scrn_wqgoprint( main_win, 15, 49, ATTR_NORMAL,"OF = Proceeding" ));
		SRC (scrn_wqgoprint( main_win, 16, 49, ATTR_NORMAL,"HL = On Hold" ));
		SRC (scrn_wqgoprint( main_win, 18, 19, ATTR_NORMAL,"Press RETURN key to return to main menu" ));
		SRC (scrn_wgocursor( main_win, 18, 59 ));
		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));

#else
         wclear(w);
         wmove(w,1,0);
         sprintf(str,"BOARD: %d", brd);
         waddstr(w,str);
         wmove(w,3,0);
         waddstr(w,"CHAN:   ");
         for(i = 0; i < 15; i++)
         {
            sprintf(str,"  %2d", i+1);
            waddstr(w,str);
         }
         wmove(w,4,0);
         waddstr(w,"STATUS: ");
         for(i = 0; i < 15; i++)
         {
		if (linfo[i].crn)
		{
            GRC(gc_GetCallState(linfo[i].crn, &chn_state));
			switch(chn_state)
			{
			case GCST_ALERTING:
            			waddstr(w,"  AL");
				break;
			case GCST_IDLE:
            			waddstr(w,"  ID");
				break;
			case GCST_NULL:
            			waddstr(w,"  NL");
				break;
			case GCST_CONNECTED:
            			waddstr(w,"  CN");
				break;
			case GCST_ACCEPTED:
            			waddstr(w,"  AC");
				break;
			case GCST_OFFERED:
            			waddstr(w,"  OF");
				break;
			case GCST_DISCONNECTED:
            			waddstr(w,"  DC");
				break;
			case GCST_PROCEEDING:
            			waddstr(w,"  PR");
				break;
			case GCST_ONHOLD:
            			waddstr(w,"  HL");
				break;
			default:
            			waddstr(w,"  NL");
				break;
			}
		}
		else
            waddstr(w,"  NL");
         }
         wmove(w,6,0);
         waddstr(w,"CHAN:   ");
         for(i = 15; i < maxdti; i++)
         {
          sprintf(str,"  %2d", i+1);
            waddstr(w,str);
         }
         wmove(w,7,0);
         waddstr(w,"STATUS: ");
         for(i = 15; i < maxdti; i++)
         {
		if (linfo[i].crn)
		{
            GRC(gc_GetCallState(linfo[i].crn, &chn_state));
			switch(chn_state)
			{
			case GCST_ALERTING:
            			waddstr(w,"  AL");
				break;
			case GCST_IDLE:
            			waddstr(w,"  ID");
				break;
			case GCST_NULL:
            			waddstr(w,"  NL");
				break;
			case GCST_CONNECTED:
            			waddstr(w,"  CN");
				break;
			case GCST_ACCEPTED:
            			waddstr(w,"  AC");
				break;
			case GCST_OFFERED:
            			waddstr(w,"  OF");
				break;
			case GCST_DISCONNECTED:
            			waddstr(w,"  DC");
				break;
			case GCST_PROCEEDING:
            			waddstr(w,"  PR");
				break;
			case GCST_ONHOLD:
            			waddstr(w,"  HL");
				break;
			default:
            			waddstr(w,"  NL");
				break;
			}
		}
		else
            waddstr(w,"  NL");
         }
	wmove(w,16,0);
	waddstr(w,"NL=NULL DC=DISC OF=OFFERED CN=CONNECTED AL=ALERT ID=IDLE AC=ACCEPT");
	wmove(w,17,0);
	waddstr(w,"PR=PROCEEDING HL=ONHOLD ");
         wmove(w,18,19);
         waddstr(w,"Press RETURN key to return to main menu");
         wrefresh(w);

#endif 

 	
		getustr(str);
         
		  break;

      case 27:
		  break;

      default:
//	 waddstr(w,"Invalid entry!!!  Please reenter choice.");
//	 wrefresh(w);
	 break;
   
	}  /* End of switch-statement */

}  /* end of display_information_switch */
/****************************************************************
*        NAME : dspAlarms()
* DESCRIPTION : Function to display Alarms
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
void dspAlarms()
{
   int chn_state,j;
   char str[80];
#ifdef LINUX
	int y,x;
#endif

#ifdef WIN32
	
	SRC (scrn_wgetcursor( full_scrnh, &row, &column ));
	SRC (scrn_fgcolor ( normal_color & 0x0F ));
	SRC (scrn_bgcolor ( normal_color & 0xF0 ));
	SRC (scrn_wqgoprint( alarm_win, 1, 1, ATTR_NORMAL, PROGRAM ));
	SRC (scrn_wqgoprint( alarm_win, 1, 42, ATTR_NORMAL, "LOS" ));

#else

	getsyx(y,x);
	wmove(a,0,0);
	wattrset(a,COLOR_PAIR(1));
	waddstr(a,PROGRAM);
	curs_set(0);
	wmove(a,0,41);
	waddstr(a,"LOS");
	wmove(a,1,42);

#endif
   
	sprintf(str, "%d", losflag);
	
	if (losflag) {

#ifdef WIN32
		SRC (scrn_fgcolor ( box_color & 0x0F ));
		SRC (scrn_bgcolor ( normal_color & 0xF0 ));
#else
		wattrset(a,COLOR_PAIR(4)|A_BOLD); /* makes it red*/
#endif
	
	}
   	else {

#ifdef WIN32
		SRC (scrn_fgcolor ( title_color & 0x0F ));
		SRC (scrn_bgcolor ( normal_color & 0xF0 ));
#endif
	
	}
	
#ifdef WIN32

	SRC (scrn_wqgoprint( alarm_win, 2, 43, ATTR_NORMAL, str ));
	SRC (scrn_fgcolor ( normal_color & 0x0F ));
	SRC (scrn_bgcolor ( normal_color & 0xF0 ));
	SRC (scrn_wqgoprint( alarm_win, 1, 47, ATTR_NORMAL, "OOF" ));

#else

	waddstr(a,str);
	wattrset(a,COLOR_PAIR(1));
	wmove(a,0,46);
	waddstr(a,"OOF");
	wmove(a,1,47);

#endif
   
	sprintf(str, "%d", oofflag);
	
	if (oofflag) {

#ifdef WIN32
		SRC (scrn_fgcolor ( box_color & 0x0F ));
		SRC (scrn_bgcolor ( normal_color & 0xF0 ));
#else
		wattrset(a,COLOR_PAIR(4)|A_BOLD); /* makes it red */
#endif

	}
	else {

#ifdef WIN32
		SRC (scrn_fgcolor ( title_color & 0x0F ));
		SRC (scrn_bgcolor ( normal_color & 0xF0 ));
#endif
		
	}

#ifdef WIN32

	SRC (scrn_wqgoprint( alarm_win, 2, 48, ATTR_NORMAL, str ));
	SRC (scrn_fgcolor ( normal_color & 0x0F ));
	SRC (scrn_bgcolor ( normal_color & 0xF0 ));
	SRC (scrn_wqgoprint( alarm_win, 1, 52, ATTR_NORMAL, "RAI" ));

#else
	
	waddstr(a,str);
	wattrset(a,COLOR_PAIR(1));
	wmove(a,0,51);
	waddstr(a,"RAI");
	wmove(a,1,52);

#endif
	
	sprintf(str, "%d", raiflag);
	
	if (raiflag) {

#ifdef WIN32
		SRC (scrn_fgcolor ( box_color & 0x0F ));
		SRC (scrn_bgcolor ( normal_color & 0xF0 ));
#else
		wattrset(a,COLOR_PAIR(4)|A_BOLD);
#endif

	}
	else {

#ifdef WIN32

		SRC (scrn_fgcolor ( title_color & 0x0F ));
		SRC (scrn_bgcolor ( normal_color & 0xF0 ));
#endif
	
	}

#ifdef WIN32

	SRC (scrn_wqgoprint( alarm_win, 2, 53, ATTR_NORMAL, str ));
	SRC (scrn_fgcolor ( normal_color & 0x0F ));
	SRC (scrn_bgcolor ( normal_color & 0xF0 ));	
	SRC (scrn_wqgoprint( alarm_win, 1, 57, ATTR_NORMAL, "AIS" ));	
	
#else

	waddstr(a,str);
	wattrset(a,COLOR_PAIR(1));
	wmove(a,0,56);
	waddstr(a,"AIS");
	wmove(a,1,57);

#endif   

	sprintf(str, "%d", aisflag);
	
	if (aisflag) {

#ifdef WIN32

		SRC (scrn_fgcolor ( box_color & 0x0F ));
		SRC (scrn_bgcolor ( normal_color & 0xF0 ));
#else

		wattrset(a,COLOR_PAIR(4)|A_BOLD); /* makes it red */

#endif
	}
	else {

#ifdef WIN32
		SRC (scrn_fgcolor ( title_color & 0x0F ));
		SRC (scrn_bgcolor ( normal_color & 0xF0 ));
#endif
	}
	
#ifdef WIN32

	SRC (scrn_wqgoprint( alarm_win, 2, 58, ATTR_NORMAL, str ));
	SRC (scrn_fgcolor ( normal_color & 0x0F ));
	SRC (scrn_bgcolor ( normal_color & 0xF0 ));
	SRC (scrn_wqgoprint( alarm_win, 1, 62, ATTR_NORMAL, "CRC" ));

#else

	waddstr(a,str);
	wattrset(a,COLOR_PAIR(1));
	wmove(a,0,61);
	waddstr(a,"CRC");
	wmove(a,1,62); 

#endif


	sprintf(str, "%d", ferrflag);
	
	if (ferrflag) {

#ifdef WIN32

		SRC (scrn_fgcolor ( box_color & 0x0F ));
		SRC (scrn_bgcolor ( normal_color & 0xF0 ))
#else

		wattrset(a,COLOR_PAIR(4)|A_BOLD); /* makes it red */

#endif

	}
	else {

#ifdef WIN32
		SRC (scrn_fgcolor ( title_color & 0x0F ))
		SRC (scrn_bgcolor ( normal_color & 0xF0 ));
#endif
	}

#ifdef WIN32

	SRC (scrn_wqgoprint( alarm_win, 2, 63, ATTR_NORMAL, str ))
	SRC (scrn_fgcolor ( normal_color & 0x0F ))
	SRC (scrn_bgcolor ( normal_color & 0xF0 ))
	SRC (scrn_wqgoprint( alarm_win, 1, 67, ATTR_NORMAL, "R/E" ))
#else

	waddstr(a,str);
	wattrset(a,COLOR_PAIR(1));
	wmove(a,0,66);
	waddstr(a,"R/E");
	wmove(a,1,67);

#endif	
	
	sprintf(str, "%d", redflag);
	
	if (redflag) {

#ifdef WIN32

		SRC (scrn_fgcolor ( box_color & 0x0F ))
		SRC (scrn_bgcolor ( normal_color & 0xF0 ))
#else

		wattrset(a,COLOR_PAIR(4)|A_BOLD);

#endif

	}
	else {

#ifdef WIN32
		SRC (scrn_fgcolor ( title_color & 0x0F ))
		SRC (scrn_bgcolor ( normal_color & 0xF0 ));
#endif
	
	}

#ifdef WIN32

		SRC (scrn_wqgoprint( alarm_win, 2, 68, ATTR_NORMAL, str ))
		SRC (scrn_fgcolor ( normal_color & 0x0F ));
		SRC (scrn_bgcolor ( normal_color & 0xF0 ));
		SRC (scrn_wqgoprint( alarm_win, 1, 72, ATTR_NORMAL, "DCH" ));
#else
		
		waddstr(a,str);
		wattrset(a,COLOR_PAIR(1));
		wmove(a,0,71);
		waddstr(a,"DCH");
		wmove(a,1,72);
		waddstr(a,"   ");
		wmove(a,1,72);
    
#endif

    if((j=gc_GetLinedevState(dtidev, GCGLS_DCHANNEL, &chn_state)) == 0) {
   
		if(chn_state == DATA_LINK_UP)
			 chn_state = 0;
		else
			 chn_state = 1;
     		
		sprintf(str,"%d", chn_state);
     
		if (chn_state) {

#ifdef WIN32

			SRC (scrn_fgcolor ( box_color & 0x0F ));
			SRC (scrn_bgcolor ( normal_color & 0xF0 ));			
#else
			wattrset(a,COLOR_PAIR(4)|A_BOLD);
#endif

		}

		else { 
				
#ifdef WIN32
			SRC (scrn_fgcolor ( title_color & 0x0F ));
			SRC (scrn_bgcolor ( normal_color & 0xF0 ));
#endif
		}

#ifdef WIN32

		SRC (scrn_wqgoprint( alarm_win, 2, 73, ATTR_NORMAL, str ));
		SRC (scrn_fgcolor ( normal_color & 0x0F ));
		SRC (scrn_bgcolor ( normal_color & 0xF0 ));
#else

		waddstr(a,str);
		wattrset(a,COLOR_PAIR(1));
#endif
	
	}
	else {

      sprintf(str, "*");

#ifdef WIN32
	  
	  SRC (scrn_fgcolor ( box_color & 0x0F ));
	  SRC (scrn_bgcolor ( normal_color & 0xF0 ));
	  SRC (scrn_wqgoprint( alarm_win, 2, 73, ATTR_NORMAL, str ));
#else

	  wattrset(a,COLOR_PAIR(4)|A_BOLD);
	  waddstr(a,str);
	  wattrset(a,COLOR_PAIR(1));

#endif

	}

#ifdef WIN32

		SRC (scrn_wgocursor( full_scrnh, row, column ));
#else

		setsyx(y,x);
		touchwin(w);
		touchwin(a);
		wnoutrefresh(a);
		wnoutrefresh(w);
		doupdate();
		curs_set(1);

#endif

	return;
}

/****************************************************************
*        NAME : getustr(tmp_buf)
* DESCRIPTION : intermiate function that calls get_str() but 
*				checks for Dialogic event.
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
void getustr(char *tmp_buf)
{
   int retval;
	*tmp_buf = 0;
   	while( get_str(tmp_buf) == -1)
     retval = sr_waitevt(50);
	
}

/****************************************************************
*        NAME : get_key()
* DESCRIPTION : intermiate function that calls get_num() but 
*				checks for Dialogic event.
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
int get_key()
{
	int ret;
	char key_buf[50];
	int retval;
   
  while((ret = get_num(key_buf)) == -1)
	{
	   retval = sr_waitevt(50);
	   		
	}
  if ( ret == 0 )     
     return(0x1B);
  return(atoi(key_buf)); 
}

/****************************************************************
*        NAME : get_num(str)
* DESCRIPTION : Function to get numbers from kybd and 
*				display to screen.
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
int get_num(char *str)
{
#ifdef WIN32
    unsigned short ch;
    int y = 0;
	int x = 0;

         
	if( (ch = scrn_keychk()) <= 0 )			/*No Kbhit */	
		return -1;
    if(ch == 0x1B)                             /* ESC */
    { 
       if((ch = scrn_keychk()) == 0x5B)        /* Function Key */
       {
          ch = scrn_keychk();                  /* Data */
       }
       else
       {
         *str = '\0';
          return 0;
       }
    }
    else if(ch == 8/*0x7F*/)        /* Backspace */
    {
         if(val_ptr > 0)
         {
           val_ptr--;
           SRC (scrn_wgetcursor( main_win, &x, &y ));
		   SRC (scrn_wqgoprint( main_win, x, (y-1) , ATTR_NORMAL, " " ));
    	   SRC (scrn_wgetcursor( full_scrnh, &row, &column ));
		   	
		 }  
    }
    else if(ch >= '0' && ch <= '9')
    {
		SRC (scrn_wgetcursor( main_win, &x, &y ));
		SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL, &ch ));
		SRC (scrn_wgocursor( main_win, x, (y + 1) ));
		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));

        *(str+val_ptr++) = (char) ch;
    }
    else if(ch == '\n' || ch == '\r')
    {
         *(str+val_ptr) = '\0';
         SRC (scrn_wqgoprint( main_win, 19, 29, ATTR_NORMAL, BLANK ));
		 SRC (scrn_wgocursor( main_win, 19, 29 ));
		 SRC (scrn_wgetcursor( full_scrnh, &row, &column ));

		 val_ptr = 0;
         return 1;
    }
    return -1; 
#else
    char ch;
    int y = 0;
	int x = 0;
  
    if( (ch = wgetch(w)) <= 0 )                       /* No Kbhit */
       return -1;
    if(ch == 0x1B)                                      /* ESC */
    { 
       if((ch = wgetch(w)) == 0x5B)        /* Function Key */
       {
          ch = wgetch(w);                  /* Data */
       }
       else
       {
         *str = '\0';
          return 0;
       }
    }
    else if(ch == 0x7F)        /* Backspace */
    {
         if(val_ptr > 0)
         {
             val_ptr--;
	     getyx(w,y,x);
	     wmove(w,y,(x-1));
             waddch(w, ' ');
	     wmove(w,y,(x-1));
             wrefresh(w);
         }  
    }
    else if(ch >= '0' && ch <= '9')
    {
         waddch(w, ch);
         wrefresh(w);
         *(str+val_ptr++) = ch;
    }
    else if(ch == '\n' || ch == '\r')
    {
         *(str+val_ptr) = '\0';
         val_ptr = 0;
         return 1;
    }
    return -1; 
#endif
}

/****************************************************************
*        NAME : get_str(str)
* DESCRIPTION : Function to get input string from kybd and 
*				display to screen
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
int get_str(char *str)
{
#ifdef WIN32
    unsigned short ch;
    int y,x;
  
	if( (ch = scrn_keychk()) <= 0 )				/*No Kbhit */	
       return -1;
    if(ch == 0x1B)                           /* ESC */
    { 

       if((ch = scrn_keychk()) == 0x5B)        /* Function Key */
       {
          ch = scrn_keychk();                  /* Data */
       }
       else
       {
         *str = '\0';
          return 0;
       }
    }
    else if(ch == 8/*0x7F*/)        /* Backspace */
    {
         if(val_ptr > 0)
         {
             val_ptr--;
			 SRC (scrn_wgetcursor( main_win, &x, &y ));
			 SRC (scrn_wqgoprint( main_win, x, (y-1) , ATTR_NORMAL, " " ));
             SRC (scrn_wgetcursor( full_scrnh, &row, &column ));

         }  
    }
    else if(ch == '\n' || ch == '\r')
    {
         *(str+val_ptr) = '\0';
         val_ptr = 0;
         return 1;
    }
    else 
    {
		SRC (scrn_wgetcursor( main_win, &x, &y ));
		SRC (scrn_wqgoprint( main_win, x, y, ATTR_NORMAL, &ch ));
		SRC (scrn_wgocursor( main_win, x, (y + 1) ));
 		SRC (scrn_wgetcursor( full_scrnh, &row, &column ));

        *(str+val_ptr++) = (char) ch;

    }
    return -1; 
#else

    char ch;
    int y = 0;
	int x = 0;
  
    if( (ch = wgetch(w)) <= 0 )                       /* No Kbhit */
       return -1;
    if(ch == 0x1B)                           /* ESC */
    { 
       if((ch = wgetch(w)) == 0x5B)        /* Function Key */
       {
          ch = wgetch(w);                  /* Data */
       }
       else
       {
         *str = '\0';
          return 0;
       }
    }
    else if(ch == 0x7F)        /* Backspace */
    {
         if(val_ptr > 0)
         {
             val_ptr--;
	     getyx(w,y,x);
	     wmove(w,y,(x-1));
             waddch(w, ' ');
	     wmove(w,y,(x-1));
             wrefresh(w);
         }  
    }
    else if(ch == '\n' || ch == '\r')
    {
         *(str+val_ptr) = '\0';
         val_ptr = 0;
         return 1;
    }
    else 
    {
         waddch(w, ch);
         wrefresh(w);
         *(str+val_ptr++) = ch;
    }
    return -1; 
#endif
}

/****************************************************************
*        NAME : log_message1(message_string)
* DESCRIPTION : Function to display LOG1 messages
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
void log_message1(char *message_string)
{
	char str[80];
#ifdef LINUX
// GFG	int y,x;
#endif

#ifdef WIN32

	SRC (scrn_fgcolor ( normal_color & 0x0F ));
	SRC (scrn_bgcolor ( normal_color & 0xF0 ));
	SRC (scrn_wclrline( message_win, 1, 1, 80 ));
	sprintf(str,"LOG1: %s", message_string);
	SRC (scrn_wqgoprint( message_win, 1, 1, ATTR_NORMAL, str));
	SRC (scrn_wgocursor( full_scrnh, row, column ));
//	SRC (scrn_wrefresh( full_scrnh);

#else

// GFG	getsyx(y,x);
	wmove(l,0, 0);
	waddstr(l,BLANK);
	wmove(l,0, 0);
	sprintf(str,"LOG1: %s", message_string);
	// fprintf(stderr, str);
	// fprintf(stderr, "\n");
	waddstr(l,str);
	wrefresh(l);
// GFG	setsyx(y,x);
// GFG	doupdate();
	// wmove(w,y,x);
	touchwin(w);
	touchwin(l);
	wnoutrefresh(l);
	wnoutrefresh(w);
	wrefresh(l);
	curs_set(1);

#endif

}
/****************************************************************
*        NAME : log_message2(message_string)
* DESCRIPTION : Function to display LOG2 messages
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
void log_message2(char *message_string)
{
   char str[80];
#ifdef LINUX
	// GFGint y,x;
#endif

#ifdef WIN32

	SRC (scrn_fgcolor ( normal_color & 0x0F ));
	SRC (scrn_bgcolor ( normal_color & 0xF0 ));   
	SRC (scrn_wclrline( message_win, 2, 1, 80 ));
	sprintf(str,"LOG2: %s", message_string);
	SRC (scrn_wqgoprint( message_win, 2, 1, ATTR_NORMAL, str));
	SRC (scrn_wgocursor( full_scrnh, row, column ));

#else

// GFG	getsyx(y,x);
	wmove(l,1, 0);
	waddstr(l,BLANK);
	wmove(l,1, 0);
	sprintf(str,"LOG2: %s", message_string);
	// fprintf(stderr, str);
	// fprintf(stderr, "\n");
	waddstr(l,str);
	wrefresh(l);
// GFG	setsyx(y,x);
// GFG	doupdate();
	// wmove(w,y,x);
	touchwin(w);
	touchwin(l);
	wnoutrefresh(l);
	wnoutrefresh(w);
	wrefresh(l);
	curs_set(1);

#endif
}

/****************************************************************
*        NAME : log_message3(message_string)
* DESCRIPTION : Function to display LOG3 messages
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
void log_message3(char *message_string)
{
	char str[80];
#ifdef LINUX
// GFG	int y,x;
#endif

#ifdef WIN32

	SRC (scrn_fgcolor ( normal_color & 0x0F ));
	SRC (scrn_bgcolor ( normal_color & 0xF0 ));
	SRC (scrn_wclrline( message_win, 3, 1, 80 ));
	sprintf(str,"LOG3: %s", message_string);
	SRC (scrn_wqgoprint( message_win, 3, 1, ATTR_NORMAL, str ));
	SRC (scrn_wgocursor( full_scrnh, row, column ));

#else

// GFG	getsyx(y,x);
	wmove(l,2, 0);
	waddstr(l,BLANK);
	wmove(l,2, 0);
	sprintf(str,"LOG3: %s", message_string);
	// fprintf(stderr, str);
	// fprintf(stderr, "\n");
	waddstr(l,str);
	wrefresh(l);
	// wmove(w,y,x);
// GFG	setsyx(y,x);
// GFG	doupdate();
	touchwin(w);
	touchwin(l);
	wnoutrefresh(l);
	wnoutrefresh(w);
	wrefresh(l);
	curs_set(1);

#endif 
}
/****************************************************************
*        NAME : display_exit()
* DESCRIPTION : Function to reformat to DOS command window
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
void display_exit()
{
#ifdef WIN32

	SRC (scrn_gocursor( 2, 1));
	SRC (scrn_fgcolor ( reverse_color & 0x0F ));
	SRC (scrn_bgcolor ( reverse_color & 0xF0 ));
	SRC (scrn_endwin( alarm_win ));
	SRC (scrn_endwin( main_win ));
	SRC (scrn_endwin( message_win ));
	SRC (scrn_endwin( full_scrnh ));

#endif
}
/****************************************************************
*        NAME : display_init()
* DESCRIPTION : Function to initialize display
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
void display_init()
{

// Do basic screen setup, etc. and then go into main loop.....

#ifdef WIN32

	scrn_getfeatmask(&scrnlib_features);

	if ((scrnlib_features & SCRN_FEAT_COLOR) == 0) {

	/* monochome display */
	
		normal_color	= FG_LTWHITE	| BG_BLACK;
		reverse_color	= FG_WHITE		| BG_BLACK;					
		box_color		= FG_LTWHITE	| BG_BLACK;	
		help_color		= FG_WHITE		| BG_BLACK;	
		title_color		= FG_LTWHITE	| BG_BLACK;	
		blinkrvs_color	= FG_WHITE		| BG_BLACK;
		cmdhelp_color	= FG_WHITE		| BG_BLACK;
	    info_color		= FG_WHITE		| BG_BLACK;
	
	}
	else {

	/* color display */

		normal_color	= FG_LTWHITE	| BG_BLUE;
		reverse_color	= FG_WHITE		| BG_BLACK;
		box_color		= FG_LTYELLOW	| BG_BLUE;
		help_color		= FG_LTWHITE	| BG_CYAN;
		title_color		= FG_LTGREEN	| FG_BLUE;
		red_alarm_color	= BG_BLUE		| FG_RED;
		cmdhelp_color	= BG_WHITE		| FG_CYAN;
		yel_alarm_color	= FG_LTWHITE	| BG_YELLOW;
	
	}

	if ( scrn_init() == SCRN_FAIL ) {
		printf("ERROR: Unable to Initialize Screen: %s (0x%X)\n",
		scrn_errmsg(scrn_errno), scrn_errno);
		printf("Exiting\n!");
		exit(1);
	}

	SRC ( alarm_win = scrn_makewin ( 1, 1, 2, 80, NO_BORDER ));
	SRC ( main_win = scrn_makewin ( 3, 1, 22, 80, NO_BORDER ));
	SRC ( message_win = scrn_makewin ( 23, 1, 25, 80, NO_BORDER ));

	SRC ( scrn_fgcolor ( normal_color & 0x0F ));
	SRC ( scrn_bgcolor ( normal_color & 0xF0 ));

	SRC ( scrn_clrwin( alarm_win ));
	SRC ( scrn_clrwin( main_win ));
	SRC ( scrn_clrwin( message_win ));

#else

	initscr(); 
	
	halfdelay(1);
	noecho();

	start_color();  

	a = newwin(2,80,0,0);	// Alarm window  
	w = newwin(20,80,2,0);	// Main window  
	l = newwin(3,80,22,0);	// Log messages window  

	init_pair(1,COLOR_WHITE,COLOR_BLUE);	// Default screen colors
	init_pair(2,COLOR_WHITE,COLOR_BLUE);	// Possible new default 
	init_pair(3,COLOR_BLUE,COLOR_WHITE);	// Reverse of 2           
	init_pair(4,COLOR_WHITE,COLOR_RED);		// Alarm notice colors    
	init_pair(5,COLOR_WHITE,COLOR_YELLOW);	// Yellow Alarm notice color    

	wattrset(w, COLOR_PAIR(2)|A_BOLD);
	wattrset(a, COLOR_PAIR(2)|A_BOLD);
	wattrset(l, COLOR_PAIR(2)|A_BOLD);
	werase(w);
	werase(a);
	werase(l);

	wbkgd(w, COLOR_PAIR(2)|A_BOLD);
	wbkgd(a, COLOR_PAIR(2)|A_BOLD);
	wbkgd(l, COLOR_PAIR(2)|A_BOLD);
	wrefresh(w);
	wrefresh(a);
	wrefresh(l);

#endif
}

/****************************************************************
*        NAME : chkargs()
* DESCRIPTION : Function to parse command line arguments and set
*				global variables
*       INPUT : none
*      OUTPUT : none
*     RETURNS : none
*    CAUTIONS :  
****************************************************************/
void chkargs( int argc, char *argv[] )
{

					// Check number of args passed

	if ((argc < 4) || (argc > 5)) {
	
		usage();
		exit(1);
	}
					// Get the board number

	brd = atoi(argv[1]);

	if ((brd < 1) || (brd > 99)) {
	usage();
	exit(1);
	}

					// Get the channel number

	chan = atoi(argv[2]);

	if ((chan < 1) || (chan > 30)) {
		usage();
		exit(1);
	}

					// Copy the protocol arg

	strcpy(protocol, argv[3]);

					// If we also specify the voice dev....

	if (argc == 5) {

		strcpy(voicedev, argv[4]);
					// Minimally check the voice device name
		if (strncmp(voicedev,"dxxxB",5) != 0)
		{
		usage();
		exit(1);
		}
		voice = 1;
	}
	else
		voice = 0;
	
// Sanity test for non-ISDN protocol and voice device specified...

	if (!strcmp("isdn", protocol))
		isdn = 1;

	if (!strncmp("H323", protocol,4))
		ipt = 1;

// Handle variants of the name....
	if (!strncmp("h323", protocol,4))
		ipt = 1;

	if (!strncmp("h.323", protocol,5))
		ipt = 1;

	if ( (argc == 4) && (isdn == 0) && (ipt == 0) ) {
		usage();
		exit(1);
	}

} /* END OF GCdiag.c */
