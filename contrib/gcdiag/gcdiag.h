/**********************************************************************************
** Copyright 2003 Gerry Gilmore and Eric Lange
** This file is part of GCDIAG.

    GCDIAG is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GCDIAG is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCDIAG; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


***********************************************************************************/

// Generic Dialogic includes
#include <isdnerr.h>
#include <srllib.h> 
#include <dtilib.h> 
#include <dxxxlib.h>
#include <dxcallp.h>

#ifndef GLOBAL_CALL
#include <cclib.h>  
#include <isdncmd.h>
#include <isdnlib.h>
#endif

#ifdef GLOBAL_CALL
#include <gclib.h>
#include <gcerr.h>
#include <gcisdn.h>
METAEVENT metaevent;
#endif

// Global Variables
#define MAXCHAN 1024		// Maximum number of channels supported
#define MAXBRDS 32			// Maximum number of boards supported
#define MAXDNIS 32			// Maximum number of DNIS digits 
#define MAXANIS 32			// Maximum number of ANI digits 
#define MAXNAME 12			// Maximum length of a device name
#define BUFFER_LEN 512		// Maximum length of log string

char buff [BUFFER_LEN];			// Global buffer for Print_Log to use
char alarmbuff [BUFFER_LEN];	// Global buffer for alarms

void Print_Log(char *str);		// Prototype for Print_Log function
// void Print_GC_Log(char *str);		// Prototype for GC Print_Log function
void log_message3(char *str);
typedef long int(*EVTHDLRTYP) (); 	// Event handler prototype

// Defines for line state
// I changed this so that the line state will be more primitive. It's 
// the voice resource that does plays, records, etc. 
typedef enum Line_State_Def
{
lsOOS,					// Out Of Service (GC BLOCKED)
lsIDLE,					// Idle, not doing anything
lsDIALING,				// Making outbound call
lsCONNECTED,				// Connected
lsANSWERING,				// Waiting for an inbound call
// lsPLAYING,				// Playing a file (or from memory)
// lsRECORDING,				// Recording a file (or to memory)
lsHANGUP,				// Hanging up the line
lsANIDNIS, 				// Gathering ANI/DNIS
lsCOUNT
} Line_State;

// Defines for device type
typedef enum Device_Type_Def
{
dtCCDEV,				// ISDN Device
dtDTDEV, 				// Robbed-bit T1 Device
dtAGDEV, 				// Analog line Device
dtIPDEV,				// IPT Device
dtCOUNT
} Device_Type;

// Defines for device type
typedef enum Voice_Type_Def
{
vtAGDEV,				// VR from an analog line card
vtTDDEV, 				// DTMF-only from DTI/241 card
vtVRDEV, 				// Full blown VR from HD-type card
vtCOUNT
} Voice_Type;

// Defines for voice state
typedef enum Voice_State_Def
{
vsIDLE,					// Idle, not doing anything
vsDIALING,				// Making outbound call
vsCPA,					// Call Progress (same as dialing?)
vsPLAYING,				// Playing a file (or from memory)
vsRECORDING,				// Recording a file (or to memory)
vsDTMF, 				// Gathering DTMF digits    
vsCOUNT
} Voice_State;

typedef struct LINFO {
LINEDEV lndev;				// Line device network handle
Device_Type lntype;			// Type (ISDN, Robbed bit, etc.)
char protocol[20];			// Protocol name for GC
int dtdev;				// DTI device handle
int vrdev;				// Associated VR device handle
Line_State lnstate;			// State of the line 
int app_state;				// State of the application
char lname[MAXNAME];			// Device name for line I/F device
CRN crn;				// CRN (ISDN/GC Call Reference Number)
MAKECALL_BLK *mkcall;			// ISDN/GC Makecall block pointer
SC_TSINFO ltsinfo;			// SCBus timeslot info for line I/F
long lscts;				// TS number
char dnis_buf[MAXDNIS];			// Buffer for DNIS digits
char ani_buf[MAXANIS];			// Buffer for ANI digits
} Line_Info;				// Struct for channel pairs

struct LINFO linfo[MAXCHAN + 1];	// Declare array of these structs

typedef struct VINFO {
int vrdev;				// Voice resource device handle
LINEDEV lndev;				// Line device handle
Voice_State vrstate;			// State of the voice resource
char vname[MAXNAME];			// Device name for voice res device
SC_TSINFO vtsinfo;			// SCBus timeslot info for voice I/F
long vscts;				// TS number
Voice_Type vrtype;			// Info for type of voice resource
DV_DIGIT digbuf;			// buffer for DTMFs
int vr_inuse;				// 0=NotInUse, 1=InUse
} Voice_Info;				// Struct for voice resource info

struct VINFO vinfo[MAXCHAN + 1];	// Declare array of these structs
                                                                            
#define E_FUNC_FAIL     ( 6 )		// generic failure 
#define E_FUNC_FAILCODE ( 7 )		// generic failure error code 

//
// Definitions of error severities
//
#define E_SUCCESS    ( 0 )
#define E_WARNING    ( 1 )
#define E_ERROR      ( 2 )
#define E_INFO       ( 3 )
#define E_SEVERE     ( 4 )
 
// Standard function call error checking macro
#define CRC( func_call )  \
                { int ec; \
                  if( (ec = func_call ) == -1 ) \
                      sys_error ( E_FUNC_FAIL, #func_call, ec, E_ERROR ); \
                }

#define GRC( func_call )  \
                { int ec; \
                  if( (ec = func_call ) != EGC_NOERR ) \
                      GRC_sys_error ( E_FUNC_FAIL, #func_call, ec, E_ERROR ); \
                }

#ifdef LINUX
#define SRC( func_call ) \
		{ int ec; \
			(ec = func_call); \
		}
#else
#define SRC( func_call )  \
                { int ec; \
                  if( (ec = func_call ) == -1 ) \
                      sys_error ( E_FUNC_FAIL, #func_call, ec, E_ERROR ); \
                }
#endif

// Function prototypes
void sys_error( int mtype, char *message, int data, int status );

void GRC_sys_error( int mtype, char *message, int data, int status );


//
// Function:    sys_error( mtype, message, data, status )
//
// Purpose:     Print out a error/warning messages and terminate processing
//              if necessary. (all messages go to stderr.)
//
// Input:       etype   - a integer indicating message to be output
//              message - additional info to be printed with error
//              status  - exit status
//
// Returned:    Nothing.
//
//
void sys_error( int mtype, char *message, int data, int status )
{
    switch ( mtype ){                   // check error type 
        //
        // assign pointer to message string to be printed
        //
        case E_FUNC_FAIL:
            sprintf(buff, "Function \"%s\" returned %d \n", message, data );
	    log_message3(buff);
//			Print_Log(buff);
/********
	    sprintf(buff,"for device \"%s\" ", ATDV_NAMEP() );
	    Print_Log(buff);
	    sprintf(buff,"Error message is \"%s\" ", ATDV_ERRMSGP() );
	    Print_Log(buff);
***************/
            break;

        default:
            sprintf(buff, "Unidentified message \n" );
			log_message3(buff);
//	    Print_Log(buff);
            break;

    } // end switch 

    //
    // If status is other than zero (warning) then end execution
    // returning status as the exit status
    //
    switch ( status ) {
        case E_WARNING:
            //
            // Just a warning ..... continue processing
            //
            break;

        case E_ERROR:
        case E_SEVERE:
            //
            // Fatal error as defined within this program. Return error
            // status and stop execution.
            //
            // Not implemented yet.  sys_shutdown();
            break;

    } // end switch 
} // end sys_error() 

//
// Function:    GRC_sys_error( mtype, message, data, status )
//
// Purpose:     Print out a error/warning messages and terminate processing
//              if necessary. (all messages go to stderr.)
//
// Input:       etype   - a integer indicating message to be output
//              message - additional info to be printed with error
//              status  - exit status
//
// Returned:    Nothing.
//
//
void GRC_sys_error( int mtype, char *message, int data, int status )
{
int            cclibid;          /* cclib id for gc_ErrorValue() */ 
int            gc_error;         /* GlobalCall error code */ 
long           cc_error;         /* Call Control Library error code */  
char           *msg;             /* points to the error message string */ 
char           *lib_name;        /* library name for cclibid */    

switch ( mtype ){                   // check error type 
        //
        // assign pointer to message string to be printed
        //
        case E_FUNC_FAIL:
		sprintf(buff, "Function \"%s\" returned %d \n", message, data );
		log_message3(buff);
//		Print_Log(buff);
		gc_ErrorValue( &gc_error, &cclibid, &cc_error);  
		gc_ResultMsg( LIBID_GC, (long) gc_error, &msg); 
			/* GlobalCall errors will return an integer for  
			the print error value. */ 
		sprintf(buff,"GlobalCall error 0x%x - %s\n", gc_error, msg); 
		log_message3(buff);
//		Print_Log(buff);
		gc_ResultMsg( cclibid, cc_error, &msg); 
		gc_CCLibIDToName(cclibid, &lib_name); 
		sprintf(buff,"%s library had error 0x%lx - %s\n",lib_name,cc_error,msg);
		log_message3(buff);	
// 		Print_Log(buff);
              break;

        default:
            sprintf(buff, "Unidentified message \n" );
			log_message3(buff);		    
//			Print_Log(buff);
            break;

    } // end switch 

    //
    // If status is other than zero (warning) then end execution
    // returning status as the exit status
    //
    switch ( status ) {
        case E_WARNING:
            //
            // Just a warning ..... continue processing
            //
            break;

        case E_ERROR:
        case E_SEVERE:
            //
            // Fatal error as defined within this program. Return error
            // status and stop execution.
            //
            // Not implemented yet.  sys_shutdown();
            break;

    } // end switch 
} // end sys_error() 
