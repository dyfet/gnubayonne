
/**********************************************************************************
** Copyright (c) 2003 Gerry Gilmore and Eric Lange
** This file is part of DM3CONFIG.

    DM3CONFIG is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    DM3CONFIG is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DM3CONFIG; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


***********************************************************************************/
// dm3config - Program to easily configure DM3 family cards under Linux
// This program only requires: curses and that the Intel Dialogic Linux 
// drivers for SR 5.1++ have been installed and configured correctly
//
// The main flow here is that, as the program starts, it will first check
// that the drivers are actually up and running. Then, it will cycle through
// each possible board ID (1-16), grabbing the output from the onboard
// configuration ROM. This info is taken by calling the "listboards" utility
// and grepping it's output via a popen() call.
//
// The key piece of info gleaned from this is the "Model Type". This is a 
// unique model number burned into ROM for each type of board. Therefore, we
// pivot around this number to fill in appropriate info and determine what 
// info is needed by the user. 
//
// Currently, there are 2 switch tables that are used in configuring the boards.
// The first is used as each board is queried during initialization to determine
// some of the basic info. (do I need a network front-end? Am I an IPLink?,etc)
// Then, as each board is selected for configuration, another switch table is
// used to get more specific info.
// Ideally, I'd like to combine these tables to reduce future maintenance and
// generally keep things cleaner. Probably, I'll just use a separate parameter
// to pass to one config function that will drive how it works.....
//
// Once all of the info has been collected, the program builds what should be
// a working, valid pyramid.scd file in /usr/dialogic/cfg.
//
// Anyway, here's the code!!!!
//

// Normal includes
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <curses.h>
#include <term.h>
#include <signal.h>
#include <string.h>

// Global defines
#define NUMBRDS 16
// Screen positions
#define BOARD 2
#define SLOT 6
#define MODEL 10
#define NAME 16
#define MEDIA 36
#define CONFED 50
#define FRONT 62

// This is the overall per-board struct. It's pretty big, but with a 16
// board limit, we won't run out of memory........
typedef struct board {
int brdnum;			// Logical board number
int slot;			// Position (slot number - IMPORTANT)
int model;			// ModelType - Determines EVERYTHING
int bustype;			// Bus Type - 0 = PCI; 1 = cPCI
int brdtype;			// Board Type - 0 = QVS; 1 = IPLink
int fereq;			// NI front-end? 0=No; 1=Yes
int conflag;			// Configed yet?????
int pcmtype;			// 0 = MULAW, 1 = ALAW
char name[20];			// Name string for the board
char filename[40];		// Common name for PCD/FCD file specs
char mload[4];			// Media Load
char feproto[20];		// Front End protocols (4ess, etc.) 
char ipaddr[20];		// IP address (if IPLink)
char subnet[20];		// subnet mask (if IPLink)
char target[40];		// NIC name   (if IPLink)
char host_ipaddr[20];		// IP address of host (if IPLink)
char host_name[40];		// Host sys name   (if IPLink)
char user_name[40];		// Valid user on sys  (if IPLink) default=root
char gate_ipaddr[20];		// IP address of gateway (if IPLink) 
} brdinfo;

// List of currently supported FE protocols
char const *fetypes[] =
{
"CAS - Robbed-bit",
"4ESS - ISDN PRI ",
"5ESS - ISDN PRI ",
"NTT - ISDN PRI  ",
"DMS - ISDN PRI  ",
"NET5 - ISDN PRI ",
"NI2 - ISDN PRI  ",
"QSIG - ISDN PRI ",
NULL};

char const **feptr;		// Pointer to fetypes

// List of currently supported media loads
char const *mltypes[] =
{
"1 - Basic Voice (Play/Rec/CPA)",
"2 - Enhanced (Basic+CSP+SCR)",
"6 - Ultra (Enhanced+TrueSpeech)",
"9b - Conferencing Only",
"10 - Enhanced+Conferencing",
NULL};

char const **mlptr;		// Pointer to mltypes

struct board binfo[NUMBRDS];
struct board *biptr;

// Some global variables
char buff[80];
char per_board[80];
char *cptr, *tmptr;
int offset = 4;			// Start 4 lines down from top of screen
int brd =0;
int numbrds =0;
char prname[10];		// Protocol name (4ESS, etc. from fetypes)

FILE *popen(), *brdlst;

WINDOW *mwin, *pwin, *lwin;	// Various windows for prompts, etc.

// Function prototypes
void disp_all_brds(void);	
void disp_brd(int brd);
int bld_binfo(void);
void scr_brite(WINDOW *win);
void scr_norm(WINDOW *win);
void config_brd(struct board *biptr);
void config_span_brd(struct board *biptr);
void config_ipt_brd(struct board *biptr);
int get_fe_info(struct board *biptr);
int get_ipt_info(struct board *biptr);
int get_ml_info(struct board *biptr);
int save_info(void);
void reset_ml(void);
void bld_prname_t1(struct board *biptr);
void bld_prname_e1(struct board *biptr);

// Main entry point
int main(argc, argv)
int argc;
char argv[];
{
int key;
int rc;
char buff2[80];
char *disp;

// First, let's make sure that we're running in a sane (non-X) environment
// by seeing if the DISPLAY variable is set.
disp = getenv("DISPLAY");
if (disp != NULL)
	{
	printf("\n\tWARNING!! This program MUST be run from a \n");
	printf("\n\tconsole screen - not from an X terminal!!!! \n");
	printf("\n\tExiting..... \n\n");
	exit(1);
	}

biptr = binfo;

// Clear our buffers
memset(per_board, 0, 80);
memset(buff, 0, 80);
memset(buff2, 0, 80);

// Next, let's make sure the drivers are loaded
sprintf(buff, "/sbin/lsmod | grep mercd");
brdlst = popen(buff,"r");
fgets(per_board, 78, brdlst);
if (strlen(per_board) == 0)	// Drivers loaded??
	{
	printf("\n\tWARNING!! DM3 drivers not loaded!! Exiting!!\n\n");
	exit(1);
	}
pclose(brdlst);			// Clean up.....

// Clear our buffers (again)
memset(per_board, 0, 80);
memset(buff, 0, 80);

// Section to test for boards at ID 0
sprintf(buff, "/usr/dialogic/bin/listboards -c 0 | grep MODEL 2>/dev/null");
brdlst = popen(buff,"r");
fgets(per_board, 78, brdlst);
if (strlen(per_board) != 0)	// Anything here???
	{			// Yep - dammit
	printf("\n\tWARNING!! Detected boards set at ID 0!    \n");
	printf("\n\tAll DM3 boards MUST be set at non-zero, unique \n");
	printf("\n\tvalues. (i.e. 1, 2, 3, etc.) \n");
	printf("\n\tShut down the system and re-configure the boards. \n");
	printf("\n\tExiting..... \n\n");
	exit(1);
	}


// OK, drivers loaded and board IDs are sane. Proceed.....
printf("\n\n\tGetting board info. Please wait............");
fflush(stdout);

rc = bld_binfo();		// Build up the device structs
				// The initial setup
if( rc == -1)
	{
	printf("\n\n\tWARNING!! No DM3 Boards Found! Exiting!! \n");
	exit(1);
	} 

initscr();			// Initialize curses
nonl();
start_color();
mwin = newwin(23,80,0,0);
lwin = newwin(1,80,23,0);

// For those who don't like my default color selections, here's where to
// change them..............
init_pair(1,COLOR_WHITE,COLOR_BLUE);
init_pair(2,COLOR_WHITE,COLOR_RED);
init_pair(3,COLOR_BLUE,COLOR_WHITE);
init_pair(4,COLOR_YELLOW,COLOR_BLUE);

wattrset(mwin,COLOR_PAIR(1));
werase(mwin);

wattrset(lwin,COLOR_PAIR(3));
werase(lwin);

wbkgd(mwin, COLOR_PAIR(1));		// Can be OR'ed with A_BOLD
wbkgd(lwin, COLOR_PAIR(3));		// Can be OR'ed with A_BOLD

// Setup main window
wattrset(mwin,COLOR_PAIR(1)|A_BOLD|A_UNDERLINE);
mvwaddstr(mwin,1,1,"                 Intel - Communications Products Group");
mvwaddstr(mwin,2,1,"                      Linux DM3 Configuration Tool ");
wmove(mwin, 3, 0);
hline(0,80);
box(mwin,0,0);

wattrset(mwin,COLOR_PAIR(1)|A_BOLD|A_UNDERLINE);
mvwaddstr(mwin,4,1,"Brd Slot Model   Name          MediaLoad    Configured Y/N     Front-End");

wattrset(mwin,COLOR_PAIR(1)|A_BOLD);

// Setup label window
mvwaddstr(lwin,0,1," Arrow-up/down   C - ConfigureBoard  S - SaveConfig  Q - Quit");
wattrset(lwin,COLOR_PAIR(2)|A_BOLD);
mvwaddch(lwin,0,18,'C'); mvwaddch(lwin,0,38,'S'); mvwaddch(lwin,0,54,'Q');
wrefresh(lwin);

disp_all_brds();	// Show all the boards found

scr_brite(mwin);
brd = 1;
disp_brd(brd);		// Show 1st board first
wrefresh(mwin);
wrefresh(lwin);
while (1)		// This is the loop we keep coming back to
{
noecho();
raw();
flushinp();
scrollok(mwin,TRUE);
keypad(mwin, TRUE);
disp_brd(brd);		// Display the currently selected board
touchwin(mwin);
wrefresh(mwin);
wrefresh(lwin);
key = wgetch(mwin);
switch (key)
	{
	case KEY_DOWN:
			scr_norm(mwin);
			disp_brd(brd);
			if (brd == (numbrds -1)) // At bottom of list?
				brd = 1;	// Go to top
			else
				brd++;
			scr_brite(mwin);
			disp_brd(brd);
			wrefresh(mwin);
			wrefresh(lwin);
			break;
	case KEY_UP:
			scr_norm(mwin);
			disp_brd(brd);
			if (brd == 1)		// AT top of list?
				brd = numbrds -1;
			else
				brd--;
			scr_brite(mwin);
			disp_brd(brd);
			wrefresh(mwin);
			wrefresh(lwin);
			break;

	case 'c':	// "C" or Enter will start config of the currently
	case 'C':	// selected board
	case KEY_ENTER:
	case '\n':
	case '\r':
			biptr = &binfo[brd -1];	// Ones based vs zero-based
			if (biptr->fereq == 1)  // If this bd uses a FE
				rc = get_fe_info(biptr);
			if (rc == -1)
				break;
			if (biptr->brdtype == 1) // If IPLink board
				{
				get_ipt_info(biptr);
				config_ipt_brd(biptr);
				}
				else 	// It must be a span board
				{
				config_span_brd(biptr);
				}
			break;


	case 'q':		// Quit config tool
	case 'Q':
biptr = binfo;
while (biptr->model != 0)
	{
	if (biptr->conflag == 1) // If quitting before saving (and at 
		{		// least 1 board has been configed)
pwin = newwin(10,50,5,20);
wbkgd(pwin, COLOR_PAIR(1));		// Can be OR'ed with A_BOLD
wattrset(pwin,COLOR_PAIR(1)|A_BOLD);
box(pwin,0,0);
	mvwaddstr(pwin,2,2,"WARNING! You have configured one or more");
	mvwaddstr(pwin,3,2,"boards, but not saved the configuration.");
	mvwaddstr(pwin,4,2,"Enter 'Y' to quit anyway or 'S' to save ");
			wrefresh(pwin);
			key = wgetch(pwin);
			if ((key == 'y') || (key == 'Y'))
				{
				werase(pwin);
				delwin(pwin);
				touchwin(mwin);
				wrefresh(mwin);
				break;
				}
			else
			rc = save_info();  // OK, we'll save the info
			break;
		}
	biptr++;
	}
			endwin();
			exit(0);
			break;

	case 's':
	case 'S':

			rc = save_info();
			break;
	default:
			beep(); beep();
			break;
	}
}
// endwin();
}						// End of main

// Function to display all of the boards found with preliminary info
// No parameters passed - void returned
void disp_all_brds()
{
int i;
biptr = binfo;
wattrset(mwin,COLOR_PAIR(1)| A_BOLD);
for (i = 1; i< NUMBRDS; i++)
	{
	if (biptr->model == 0)
			break;
	sprintf(buff, "%d", biptr->brdnum);
	mvwaddstr(mwin,offset + i,BOARD,buff);

	sprintf(buff, "%d", biptr->slot);
	mvwaddstr(mwin,offset + i,SLOT,buff);

	sprintf(buff, "%d", biptr->model);
	mvwaddstr(mwin,offset + i,MODEL,buff);

	sprintf(buff, "%s", biptr->name);
	mvwaddstr(mwin,offset + i,NAME,buff);

	sprintf(buff, "%s", biptr->mload);
	mvwaddstr(mwin,offset + i,MEDIA,buff);

	if (biptr->conflag == 1)
	mvwaddch(mwin,(offset + i),CONFED,'Y');
		else
	mvwaddch(mwin,(offset + i),CONFED,'N');

	sprintf(buff, "%s", biptr->feproto);
	mvwaddstr(mwin,offset + i,FRONT,buff);

	biptr++;
	}						// Do it again
wrefresh(mwin);
wrefresh(lwin);
}


// Function that cycles through each possible board location, filling in basic
// info on each board. 
// Called at init time and returns 0 on success, -1 on failure
int bld_binfo()
{
char buff2[80];
int i;
brd = 1;				// Initialize to 1
biptr = binfo;


for (i = 1; i< NUMBRDS; i++)
	{
memset(per_board, 0, 80);
memset(buff, 0, 80);
memset(buff2, 0, 80);
sprintf(buff, "/usr/dialogic/bin/listboards -c %d | grep MODEL 2>/dev/null", i);
brdlst = popen(buff,"r");
fgets(per_board, 78, brdlst);
if (strlen(per_board) == 0)	// Anything here???
			continue;	// Nope, loop again
biptr->slot = i;			// Yes! Save the slot number
biptr->brdnum = brd;
cptr = per_board;
tmptr = buff2;

// OK at this point I've got that string in a buffer
cptr += 8;
biptr->model = strtol(cptr, NULL, 16);

config_brd(biptr);			// For each board found, fill info
pclose(brdlst);
// Fill in other fields
if(strlen(biptr->feproto) == 0)
	sprintf(biptr->feproto, "<NONE SELECTED> ");
if(strlen(biptr->mload) == 0)
	sprintf(biptr->mload, "**");
biptr++;
brd++;
	}				// End of for loop
if (brd == 1)				// No boards found
	return(-1);
numbrds = brd;				// Save the number of boards found
return(0);
}					// End of bld_binfo()

// Generic routine to display a board's info. 
// Called with a ones-based based board number - void return
void disp_brd(int brd)
{
int i;
i = brd;
biptr = &binfo[brd -1];	// Ones based vs zero-based
wmove(mwin,(offset + i),0);
mvwaddstr(mwin,(offset + i),BOARD,"                                                                           ");

	sprintf(buff, "%d", biptr->brdnum);
	mvwaddstr(mwin,(offset + i),BOARD,buff);

	sprintf(buff, "%d", biptr->slot);
	mvwaddstr(mwin,(offset + i),SLOT,buff);

	sprintf(buff, "%d", biptr->model);
	mvwaddstr(mwin,(offset + i),MODEL,buff);

	sprintf(buff, "%s", biptr->name);
	mvwaddstr(mwin,(offset + i),NAME,buff);

	sprintf(buff, "%s", biptr->mload);
	mvwaddstr(mwin,(offset + i),MEDIA,buff);

	if (biptr->conflag == 1)
	mvwaddch(mwin,(offset + i),CONFED,'Y');
		else
	mvwaddch(mwin,(offset + i),CONFED,'N');

	sprintf(buff, "%s", biptr->feproto);
	mvwaddstr(mwin,(offset + i),FRONT,buff);
}					// End of disp_brd()

// Generic screen high-lighter
void scr_brite(WINDOW *win)
{
wattrset(win,COLOR_PAIR(1)| A_REVERSE);
}

// Generic screen de-high-lighter
void scr_norm(WINDOW *win)
{
wattrset(win,COLOR_PAIR(1)| A_BOLD);
}

// Function to fill in initial, default info for each board found.
// Called with a board pointer - void return
// NOTE: Best guesses are used for filling in this info. MAY CHANGE!!!!!
// For boards that only support 1 media load, it's pre-selected.
void config_brd(struct board *biptr)
{

switch(biptr->model)
	{
						// Begin IPLink section
	case 1024:			// 
		biptr->bustype = 0;
		biptr->pcmtype = 0;
		biptr->brdtype = 1;
		biptr->fereq = 0;
		sprintf(biptr->name,"DM/IP0810");
		break;
	case 1025:			// 
		biptr->bustype = 0;
		biptr->brdtype = 1;
		biptr->pcmtype = 0;
		biptr->fereq = 0;
		sprintf(biptr->name,"DM/IP2420");
		break;
	case 1280:			// 
		biptr->bustype = 0;
		biptr->brdtype = 1;
		biptr->pcmtype = 1;
		biptr->fereq = 0;
		sprintf(biptr->name,"DM/IP0810A E1");
		break;
	case 1281:			// 
		biptr->bustype = 0;
		biptr->brdtype = 1;
		biptr->pcmtype = 1;
		biptr->fereq = 0;
		sprintf(biptr->name,"DM/IP3020A E1");
		break;
	case 1536:			// 
		biptr->bustype = 0;
		biptr->brdtype = 1;
		biptr->pcmtype = 0;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/IP2431A-T1");
		break;
	case 1539:			// 
		biptr->bustype = 1;
		biptr->brdtype = 1;
		biptr->pcmtype = 0;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/IP481A-2T1 cPCI");
		break;
	case 1792:			// 
		biptr->bustype = 0;
		biptr->brdtype = 1;
		biptr->pcmtype = 1;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/IP3031A-E1");
		break;
	case 1794:			// 
		biptr->bustype = 1;
		biptr->brdtype = 1;
		biptr->pcmtype = 1;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/IP3031A-E1 cPCI");
		break;
	case 4096:			// 
		biptr->bustype = 0;
		biptr->brdtype = 1;
		biptr->pcmtype = 1;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/IP301-1E1");
		break;
	case 8192:			// 
		biptr->bustype = 0;
		biptr->brdtype = 1;
		biptr->pcmtype = 0;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/IP241-1T1");
		break;
	case 1283:			// 
		biptr->bustype = 0;
		biptr->brdtype = 1;
		biptr->pcmtype = 0;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/IP481-2T1-100");
		break;
						// End IPLink section
						// Quad -span family
// NOTE: Correlate new model numbers to these entries above
	case 256:			// 
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 0;
		biptr->fereq = 1;
// This board only allows ML 1, therefore no need to call get_ml_info()
		sprintf(biptr->mload,"1");
		sprintf(biptr->name,"DM/V960-4T1");
		break;
	case 257:			// 
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 0;
		biptr->fereq = 1;
// This board only allows ML 1, therefore no need to call get_ml_info()
		sprintf(biptr->mload,"1");
		sprintf(biptr->name,"DM/V480-4T1");
		break;
	case 258:			// 
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 0;
		biptr->fereq = 1;
		sprintf(biptr->mload,"NA");
		sprintf(biptr->name,"DM/T960-4T1");
		break;
	case 259:			// 
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 0;
		biptr->fereq = 1;
		sprintf(biptr->mload,"NA");
		sprintf(biptr->name,"DM/N960-4T1");
		break;
	case 260:			// 
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 0;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/V960A-4T1");
		break;
	case 261:			// 
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 1;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/V1200A-4E1");
		break;
	case 512:			// 
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 1;
		biptr->fereq = 1;
// This board only allows ML 1, therefore no need to call get_ml_info()
		sprintf(biptr->mload,"1");
		sprintf(biptr->name,"DM/V1200-4E1");
		break;
	case 513:			// 
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 1;
		biptr->fereq = 1;
// This board only allows ML 1, therefore no need to call get_ml_info()
		sprintf(biptr->mload,"1");
		sprintf(biptr->name,"DM/V600-4E1");
		break;
	case 514:			// 
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 1;
		biptr->fereq = 1;
		sprintf(biptr->mload,"NA");
		sprintf(biptr->name,"DM/T600-2E1");
		break;
	case 515:			// 
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 1;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/N1200-4E1");
		break;
	case 768:			// 
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 0;
		biptr->fereq = 0;
		sprintf(biptr->name,"DM/F240");
		sprintf(biptr->mload,"FAX");
		break;
// NOTE Missing 1027, 1028, 1283, 1284, 1539, etc.
	case 2048:			// 
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 0;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/F240-1T1");
		sprintf(biptr->mload,"FAX");
		break;
	case 2049:			// 
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 0;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/VF240-1T1");
		sprintf(biptr->mload,"FAX");
		break;
// NOTE: Missing 2052
	case 2304:			// 
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 1;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/F300-1E1");
		sprintf(biptr->mload,"FAX");
		break;
	case 2305:			// 
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 1;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/VF300-1E1");
		sprintf(biptr->mload,"FAX");
		break;
	case 2560:			// 
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 1;
		biptr->fereq = 0;
		sprintf(biptr->name,"DM/F300");
		sprintf(biptr->mload,"FAX");
		break;
	case 3075:			// 
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 0;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/V480-2T1 (Hi Z)");
		break;
	case 3076:			// 
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 1;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/V600-2E1 (Hi Z)");
		break;
	case 3584:			// 
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 1;
		biptr->fereq = 0;
		sprintf(biptr->name,"DM/V2400A PCI");
		sprintf(biptr->feproto,"NOT APPLICABLE");
		break;
/********* OBSOLETE 
	case 4097:			// 
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 1;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/V600-2E1");
		break;
*******************************/
	case 4098:			// 
		biptr->bustype = 1;
		biptr->brdtype = 0;
		biptr->pcmtype = 1;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/V600A-2E1 cPCI");
		sprintf(biptr->mload,"10");
		break;
	case 4352:			// 
		biptr->bustype = 1;
		biptr->brdtype = 0;
		biptr->pcmtype = 1;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/V1200A-4E1 cPCI");
		break;
	case 4353:			// 
		biptr->bustype = 1;
		biptr->brdtype = 0;
		biptr->pcmtype = 0;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/V960A-4T1 cPCI");
		break;
	case 4608:			// 
		biptr->bustype = 1;
		biptr->brdtype = 0;
		biptr->pcmtype = 0;
		biptr->fereq = 1;
		sprintf(biptr->name,"DTI-16 cPCI");
		break;
	case 7936:			// 
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 0;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/V480A-2T1");
		break;
	case 7937:			// 
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 1;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/V600A-2E1");
		break;
	case 8194:			// 
		biptr->bustype = 1;
		biptr->brdtype = 0;
		biptr->pcmtype = 0;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/V480A-2T1 cPCI");
		sprintf(biptr->mload,"10");
		break;
/*********** OBSOLETE
	case 8193:			// 
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 0;
		biptr->fereq = 1;
		sprintf(biptr->name,"DM/V480-2T1");
		break;
***************************/
	case 28675:			// HDSI
		biptr->bustype = 0;
		biptr->brdtype = 0;
		biptr->pcmtype = 1;
		biptr->conflag = 1;
		biptr->fereq = 0;
		sprintf(biptr->filename,"us_hdsi");	
		sprintf(biptr->mload,"STA");
		sprintf(biptr->name,"HDSI Station");
		sprintf(biptr->feproto,"NOT APPLICABLE");
		break;
	default:
			sprintf(biptr->name,"***UNKNOWN***");
			break;
	}
if (biptr->brdtype == 1)
	sprintf(biptr->mload,"IP");
}					// End of config_brd


// Gathers more info for span cards
// Called with board pointer - void return
// NOTE: This is the hairy part. Basically, the FE info and, if applicable,
// media load info need to be collected and pieced together  to create 
// valid PCD anf FCD file names. This is hairy because there's no clear
// consistent naming convention used for the filenames. Case by case for now...
void config_span_brd(struct board *biptr)
{
int rc;

if (biptr->model == 0)		// If it's non-valid
	return;

if (biptr->name[0] == '*')		// If it's non-valid
	return;

switch(biptr->feproto[0])
	{
	case '4':
		sprintf(prname,"4ess");
		break;
	case '5':
		sprintf(prname,"5ess");
		break;
	case 'D':
		sprintf(prname,"dms");
		break;
	case 'Q':
	case 'C':
// Will get filled in later..............
		break;
	case 'N':
		switch(biptr->feproto[1])
			{
			case 'T':
			sprintf(prname,"ntt");
			case 'I':
			sprintf(prname,"ni2");
			case 'E':
			sprintf(prname,"net5");
			break;
			}
		break;
	default:
//		fprintf(stdout,"No protocol selected in config_span_brd()!!\n");
//		fflush(stdout);
		break;
	}

switch(biptr->model)
	{
						// Quad -span family
	case 256:			// 4x4 T1
		bld_prname_t1(biptr);
		sprintf(biptr->filename,"ml1_qs_%s",prname);	
			biptr->conflag = 1;
			break;
	case 257:			// 4x2 T1
		bld_prname_t1(biptr);
		sprintf(biptr->filename,"ml1_4x2_%s",prname);	
			biptr->conflag = 1;
			break;
	case 258:			// 4xt T1
		bld_prname_t1(biptr);
		sprintf(biptr->filename,"mn_4xt_%s",prname);	
			biptr->conflag = 1;
			break;
	case 259:			// 4x0 T1
		bld_prname_t1(biptr);
		sprintf(biptr->filename,"4x0_isdn_%s",prname);	
			biptr->conflag = 1;
			break;

	case 260:			// 4x4 T1 QSA (Supports ML > 1)
		// Get the media load info first.
		rc = get_ml_info(biptr);
		if(biptr->mload[0] == '1')
			{
			sprintf(biptr->mload,"2");
			reset_ml();
			}
		if((biptr->mload[0] == '9') && (biptr->feproto[0] != 'C'))
			{
			sprintf(biptr->mload,"2");
			reset_ml();
			}
		bld_prname_t1(biptr);
		sprintf(biptr->filename,"ml%s_qsa_%s",biptr->mload,prname);	
			biptr->conflag = 1;
			break;
	case 7936:			// 2x2 T1 DSA (Supports ML > 1)
		// Get the media load info first.
		rc = get_ml_info(biptr);
		if(biptr->mload[0] == '1') 
			{
			sprintf(biptr->mload,"10");
			}
		if(biptr->mload[0] == '9') 
			{
			sprintf(biptr->mload,"10");
			reset_ml();
			}
		bld_prname_t1(biptr);
		sprintf(biptr->filename,"ml%s_dsa_%s",biptr->mload,prname);	
			biptr->conflag = 1;
			break;
	case 8194:			// 2x2 T1 DSA (Supports ML > 1)
		bld_prname_t1(biptr);
		sprintf(biptr->filename,"ml%s_ds2_%s",biptr->mload,prname);	
			biptr->conflag = 1;
			break;
	case 4098:			// 2x2 T1 DSA (Supports ML > 1)
		bld_prname_e1(biptr);
		sprintf(biptr->filename,"ml%s_ds2_%s",biptr->mload,prname);	
			biptr->conflag = 1;
			break;
	case 7937:			// 2x2 E1 DSA (Supports ML > 1)
		// Get the media load info first.
		rc = get_ml_info(biptr);
		if(biptr->mload[0] == '1') 
			{
			sprintf(biptr->mload,"10");
			}
		if(biptr->mload[0] == '9') 
			{
			sprintf(biptr->mload,"10");
			reset_ml();
			}
		bld_prname_e1(biptr);
		sprintf(biptr->filename,"ml%s_dsa_%s",biptr->mload,prname);	
			biptr->conflag = 1;
			break;
	case 261:			// 4x4 E1 QSA (Supports ML > 1)
		// Get the media load info first.
		rc = get_ml_info(biptr);
		if((biptr->mload[0] == '9') && (biptr->feproto[0] != 'C'))
			{
			sprintf(biptr->mload,"2");
			reset_ml();
			}
		if(biptr->mload[0] == '1')
			{
			sprintf(biptr->mload,"2");
			reset_ml();
			}
		bld_prname_e1(biptr);
		if(biptr->feproto[0] == 'C')
			{
			if (biptr->mload[0] == '9')
				sprintf(prname,"e1");
			else
				sprintf(prname,"r2mf");
			}
		sprintf(biptr->filename,"ml%s_qsa_%s",biptr->mload,prname);	
			biptr->conflag = 1;
			break;
	case 512:			// 4x4 E1
		bld_prname_e1(biptr);
		sprintf(biptr->filename,"ml1_qs_%s",prname);	
			biptr->conflag = 1;
			break;
			break;
	case 513:			// 4x2 E1
		bld_prname_e1(biptr);
		sprintf(biptr->filename,"ml1_4x2_%s",prname);	
			biptr->conflag = 1;
			break;
	case 514:			// 4xt E1
		bld_prname_e1(biptr);
		sprintf(biptr->filename,"mn_4xt_%s",prname);	
			biptr->conflag = 1;
			break;
	case 515:			// 4x0 E1
		// Get the media load info first.
		rc = get_ml_info(biptr);
			break;
	case 768:			// DM/F240 (Fax resource only)
			break;
	case 2048:			// DM/F240-1T1
		if (biptr->feproto[0] == 'C')
			{
			sprintf(biptr->filename,"fn3_t1");	
			biptr->conflag = 1;
			break;
			}
			else
			{
		if(biptr->feproto[0] == 'Q')
			sprintf(biptr->filename,"fn3_isdn_qsigt1");	
			else
			sprintf(biptr->filename,"fn3_isdn_%s",prname);	
			biptr->conflag = 1;
			break;
			}
	case 2049:			// DM/VF240-1T1
		if (biptr->feproto[0] == 'C')
			{
			sprintf(biptr->filename,"vfn3_t1");	
			biptr->conflag = 1;
			break;
			}
			else
			{
		if(biptr->feproto[0] == 'Q')
			sprintf(biptr->filename,"vfn3_isdn_qsigt1");	
			else
		bld_prname_t1(biptr);
			sprintf(biptr->filename,"vfn3_isdn_%s",prname);	
			biptr->conflag = 1;
			break;
			}
	case 4608:			// DTI-16 cPCI 
		if (biptr->feproto[0] == 'C')
			{
			sprintf(biptr->filename,"dti16_t1cc");	
			biptr->conflag = 1;
			break;
			}
			else
			{
			sprintf(biptr->filename,"dti16_isdn_%s",prname);	
			biptr->conflag = 1;
			break;
			}
	case 2304:			// DM/F300-1E1
		if (biptr->feproto[0] == 'C')
			{
			sprintf(biptr->filename,"fn3_t1");	
			biptr->conflag = 1;
			break;
			}
			else
			{
		if(biptr->feproto[0] == 'Q')
			sprintf(biptr->filename,"fn3_isdn_qsigt1");	
			else
			sprintf(biptr->filename,"fn3_isdn_%s",prname);	
			biptr->conflag = 1;
			break;
			}
	case 2305:			// DM/VF300-1E1
		if (biptr->feproto[0] == 'C')
			{
			sprintf(biptr->filename,"vfn3_r2mf");	
			biptr->conflag = 1;
			break;
			}
			else
			{
			sprintf(biptr->filename,"vfn3_isdn_%s",prname);	
			biptr->conflag = 1;
			break;
			}
	case 2560:			// DM/F300 (Fax resource only)
			break;
	case 3075:			// DM/V480-T1 (Hi Z)
		// Get the media load info first.
		rc = get_ml_info(biptr);
			break;
	case 3076:			// DM/V600-E1 (Hi Z)
		// Get the media load info first.
		rc = get_ml_info(biptr);
			break;
	case 3584:			// DM/V2400A Resource 
		// Get the media load info first.
		rc = get_ml_info(biptr);
		if((biptr->mload[0] != '9') && (biptr->mload[0] != '1'))
			{
			sprintf(biptr->mload,"1");
			reset_ml();
			}
		sprintf(biptr->filename,"ml%s_pcires",biptr->mload);	
		biptr->conflag = 1;
			break;
	case 4097:			// DM/V600-E1
		// Get the media load info first.
		rc = get_ml_info(biptr);
			break;
	case 4352:			// DM/V1200-4E1 cPCI
		// Get the media load info first.
		rc = get_ml_info(biptr);
		if((biptr->mload[0] == '9') && (biptr->feproto[0] != 'C'))
			{
			sprintf(biptr->mload,"2");
			reset_ml();
			}
		if(biptr->mload[0] == '1')
			{
			sprintf(biptr->mload,"2");
			reset_ml();
			}
		bld_prname_e1(biptr);
		if(biptr->feproto[0] == 'C')
			{
			if (biptr->mload[0] == '9')
				sprintf(prname,"e1");
			else
				sprintf(prname,"r2mf");
			}
		sprintf(biptr->filename,"ml%s_qs2_%s",biptr->mload,prname);	
			biptr->conflag = 1;
			break;
	case 4353:			// DM/V960-4T1 cPCI
		// Get the media load info first.
		rc = get_ml_info(biptr);
		if(biptr->mload[0] == '1')
			{
			sprintf(biptr->mload,"2");
			reset_ml();
			}
		if((biptr->mload[0] == '9') && (biptr->feproto[0] != 'C'))
			{
			sprintf(biptr->mload,"2");
			reset_ml();
			}
		bld_prname_t1(biptr);
		sprintf(biptr->filename,"ml%s_qs2_%s",biptr->mload,prname);	
			biptr->conflag = 1;
			break;
	case 8193:			// 2x2 T1
		// Get the media load info first.
		rc = get_ml_info(biptr);
			break;
	case 28675:			// HDSI
		sprintf(biptr->filename,"us_hdsi");	
			break;
	default:
			break;
		
	}
}						// End of config_span_brd


// Gathers more info for ipt cards
// Called with board pointer - void return
// NOTE: This is the hairy part. Basically, the FE info and, if applicable,
// media load info need to be collected and pieced together  to create 
// valid PCD anf FCD file names. This is hairy because there's no clear
// consistent naming convention used for the filenames. Case by case for now...
// NOTE2: This section is quite sparse as the IPLink boards which actually 
// work and which I have access to are also quite sparse. Of course,
// that's the reason I'm furnishing this code.........
void config_ipt_brd(struct board *biptr)
{

switch(biptr->feproto[0])
	{
	case '4':
		sprintf(prname,"4ess");
		break;
	case '5':
		sprintf(prname,"5ess");
		break;
	case 'C':
		sprintf(prname,"t1");
		break;
	case 'D':
		sprintf(prname,"dms");
		break;
	case 'N':
		switch(biptr->feproto[1])
			{
			case 'T':
			sprintf(prname,"ntt");
			case 'I':
			sprintf(prname,"ni2");
			case 'E':
			sprintf(prname,"net5");
			break;
			}
	break;
	}


switch(biptr->model)
	{

						// Begin IPLink section
	case 1024:			// DM/IP0810 (no network interface)
	case 1025:			// DM/IP2420 (no network interface)
	case 1280:			// DM/IP0810A E1 (no network interface??)
	case 1281:			// DM/IP3020A E1 (no network interface??)
	case 1536:			// DM/IP2431A-T1
	case 1538:			// DM/IP2431A-T1 cPCI
	case 1792:			// DM/IP3031A-E1
	case 1794:			// DM/IP3031A-E1 cPCI
	case 1539:			// DM/IP481-T1 cPCI
		if(prname[0] == 't')
			sprintf(biptr->filename,"ipvs_evr_2cas_311c");
			else
		sprintf(biptr->filename,"ipvs_evr_2isdn_%s_311c",prname);
			biptr->conflag = 1;
	break;
	case 8192:			// DM/IP241-T1
		if(prname[0] == 't')
			sprintf(biptr->filename,"iptk2_t1_307");
			else
			sprintf(biptr->filename,"ipt_isdn_%s_307",prname);
			biptr->conflag = 1;
	break;
	case 1283:			// DM/IP481-T1
		if(prname[0] == 't')
			sprintf(biptr->filename,"ipt_evr_2cas_311");
			else
			sprintf(biptr->filename,"ipt_evr_2isdn_%s_311",prname);
			biptr->conflag = 1;
	break;
	}
}
						// End IPLink section

// Code that pops up a new window and allows the user to select a front-end
// protocol. No sanity checking (other than it's gotta be on the list of FE
// types)
// Called with board pointer and, despite casting, returns void
int get_fe_info(struct board *biptr)
{
int line = 2;
int key;

// Section here to pop-up a new window and get the FE info
pwin = newwin(15,40,5,20);
wbkgd(pwin, COLOR_PAIR(1));		// Can be OR'ed with A_BOLD
wattrset(pwin,COLOR_PAIR(1)|A_BOLD);
box(pwin,0,0);
// Single-key-mode   keypad(pwin, TRUE);

for (feptr = fetypes; *feptr != NULL; feptr++)
{
mvwaddstr(pwin,line,10,*feptr);
line++;
}

mvwaddstr(pwin,11,1, "Use arrow keys to highlight the ");
mvwaddstr(pwin,12,1, "desired telephony front-end protocol ");
mvwaddstr(pwin,13,1, " Press ENTER to select - Q to quit   ");

// Reset to beginning of list
feptr = fetypes;
line = 2;
scr_brite(pwin);
mvwaddstr(pwin,2,10,*feptr);
wrefresh(pwin);
while (1)
{
noecho();
raw();
flushinp();
// typeahead(-1);
scrollok(pwin,TRUE);
keypad(pwin, TRUE);
touchwin(pwin);
wrefresh(pwin);
key = wgetch(pwin);
switch (key)
	{
	case KEY_DOWN:
		if (*feptr++ == NULL)
		{
				beep();
				beep();
				feptr--;
				break;
		}
		feptr--;
		scr_norm(pwin);
		mvwaddstr(pwin,line,10,*feptr);
		feptr++;
		line++;
		scr_brite(pwin);
		mvwaddstr(pwin,line,10,*feptr);
		break;

	case KEY_UP:
		if (feptr == &fetypes[0])
		{
				beep();
				beep();
				break;
		}
		scr_norm(pwin);
		mvwaddstr(pwin,line,10,*feptr);
		line--;
		feptr--;
		scr_brite(pwin);
		mvwaddstr(pwin,line,10,*feptr);
		break;

	case KEY_ENTER:
	case '\n':
	case '\r':
		werase(pwin);
		wrefresh(pwin);
		delwin(pwin);
		memset(biptr->feproto,0,sizeof(biptr->feproto));
		strcpy(biptr->feproto, *feptr);
		return(0);
		break;

	case 'q':
	case 'Q':
		werase(pwin);
		wrefresh(pwin);
		delwin(pwin);
		return(-1);
		break;
	default:
		beep();
		break;
	}

}
// To here
werase(pwin);
wrefresh(pwin);
delwin(pwin);
}

// Code that pops up a new window and allows the user to select IPLink     
// info. No sanity checking
// Called with board pointer and returns int
int get_ipt_info(struct board *biptr)
{
echo();
pwin = newwin(23,80,0,0);
wbkgd(pwin, COLOR_PAIR(1));		// Can be OR'ed with A_BOLD
// wattrset(pwin,COLOR_PAIR(1));
wattrset(pwin,COLOR_PAIR(1)|A_BOLD);
box(pwin,0,0);
// IP Address
mvwaddstr(pwin,2,1,"Using the form: 192.168.124.219       ");
mvwaddstr(pwin,3,1,"Enter IP address of the on-board NIC: ");
wrefresh(pwin);
wscanw(pwin,"%s",biptr->ipaddr);

// Subnet Mask
mvwaddstr(pwin,5,1,"Using the form: ffffff00               ");
mvwaddstr(pwin,6,1,"Enter subnet mask of the on-board NIC: ");
wrefresh(pwin);
wscanw(pwin,"%s",biptr->subnet);

// Target Name
mvwaddstr(pwin,8,1,"Using the form: sysname.hostname.com ");
mvwaddstr(pwin,9,1,"Enter host name of this IPLink board's NIC: ");
wrefresh(pwin);
wscanw(pwin,"%s",biptr->target);

// Host IP Address
mvwaddstr(pwin,11,1,"Using the form: 192.168.124.219      ");
mvwaddstr(pwin,12,1,"Enter IP address of the HOST system: ");
wrefresh(pwin);
wscanw(pwin,"%s",biptr->host_ipaddr);

// HostName      
// Should be able to do a 'uname -n' call here via popen()
// TEST below and include............
sprintf(buff, "uname -n  2>/dev/null");
brdlst = popen(buff,"r");
fgets(biptr->host_name, 78, brdlst);
pclose(brdlst);
if (strlen(biptr->host_name) == 0)	// Anything here???
	{		// Include the below if nothing found
mvwaddstr(pwin,14,1,"Using the form: sysname.dialogic.com ");
mvwaddstr(pwin,15,1,"Enter host name of the HOST system:  ");
wrefresh(pwin);
wscanw(pwin,"%s",biptr->host_name);
	}

// Host UserName
mvwaddstr(pwin,17,1,"Using the form: guestuser               ");
mvwaddstr(pwin,18,1,"Enter a valid user name on this system: ");
wrefresh(pwin);
wscanw(pwin,"%s",biptr->user_name);

// Gateway Address
mvwaddstr(pwin,20,1,"Using the form: 192.168.124.219      ");
mvwaddstr(pwin,21,1,"Enter a gateway address to use: ");
wrefresh(pwin);
wscanw(pwin,"%s",biptr->gate_ipaddr);

// 
delwin(pwin);
return(0);
}

// Function to save the config info and build the pyramid.scd file
int save_info(void)
{
FILE *fp;
int key;
int nicnt = 1;



// First, we'll make sure that all boards are configed......
biptr = binfo;
while (biptr->model != 0)
	{
	if (biptr->conflag != 1)
		{
pwin = newwin(10,50,5,20);
wbkgd(pwin, COLOR_PAIR(1));		// Can be OR'ed with A_BOLD
wattrset(pwin,COLOR_PAIR(1)|A_BOLD);
box(pwin,0,0);
	mvwaddstr(pwin,2,2,"WARNING! All boards not configured!!!");
	mvwaddstr(pwin,3,2,"Configuration file NOT written!!!");
	mvwaddstr(pwin,4,2,"Enter 'Y' to save anyway  ");
			wrefresh(pwin);
			key = wgetch(pwin);
			if ((key == 'y') || (key == 'Y'))
				{
				werase(pwin);
				delwin(pwin);
				touchwin(mwin);
				wrefresh(mwin);
				break;
				}
			werase(pwin);
			delwin(pwin);
			return(-1);
		}
	biptr++;
	}


biptr = binfo;

// Open the config file
fp = fopen("/usr/dialogic/cfg/pyramid.scd","w");
if ( fp == NULL)
	{
	printf("ERROR opening config file.\n");
	printf("Press ENTER to continue.\n");
	getchar();
	return(-1);
	}
// Begin filling in the file.......
fprintf(fp,"NumStreams		: 4000  \n");
fprintf(fp,"NumBindHandles		: 4000  \n\n");

while (biptr->model != 0)
	{
	if (biptr->conflag != 1)
		{
		biptr++;
		continue;
		}
fprintf(fp,"[Board %d] {\n",biptr->brdnum);
fprintf(fp,"PCDName			: %s.pcd\n",biptr->filename);
fprintf(fp,"FCDName			: %s.fcd\n",biptr->filename);
if  (biptr->pcmtype == 0)
	fprintf(fp,"PCMEncoding		: MULAW \n");
	else
	fprintf(fp,"PCMEncoding		: ALAW \n");
fprintf(fp,"SlotNumber		: %d \n",biptr->slot);
	fprintf(fp,"BusType			: PCI \n");
fprintf(fp,"LogFile			: board%d.log\n",biptr->brdnum);
fprintf(fp,"TimeToSendMsg		: 50 \n");
fprintf(fp,"MasterStatus		: ");
	if(biptr->brdnum == 1)	// First board - primary, etc.
		fprintf(fp,"PRIMARY \n");
	else
		fprintf(fp,"SLAVE \n");
		if (biptr->bustype == 0)
			fprintf(fp,"TDMBusType		: H100 \n");
			else
			fprintf(fp,"TDMBusType		: H110 \n");
// TEST 	if(biptr->brdnum == 1)	// First board - primary, etc.
// TEST 		{
// TEST		fprintf(fp,"DeriveClockFrom		: OSC \n");
		fprintf(fp,"BusCR			: 8 \n");
		fprintf(fp,"Group1CR		: 8 \n");
		fprintf(fp,"Group2CR		: 8 \n");
		fprintf(fp,"Group3CR		: 8 \n");
		fprintf(fp,"Group4CR		: 8 \n");
		fprintf(fp,"DeriveClockFrom		: NETREF_1 \n");
		fprintf(fp,"PrimaryLines		: CT_A \n");
// TEST 		}
// If IPLink, fill in the NIC info
if (biptr->brdtype == 1)	// We're an IPLink
		{
		fprintf(fp,"[NIC %d] {\n", nicnt);
		fprintf(fp,"  IPAddress		: %s \n",biptr->ipaddr);
		fprintf(fp,"  SubnetMask		: %s \n",biptr->subnet);
		fprintf(fp,"  TargetName		: %s \n",biptr->target);
		fprintf(fp,"  HostIPAddress		: %s \n",biptr->host_ipaddr);
		fprintf(fp,"  HostName		: %s \n", biptr->host_name);
		fprintf(fp,"  UserName		: %s \n", biptr->user_name);
		fprintf(fp,"  GatewayIPAddress		: %s \n",biptr->gate_ipaddr);
		fprintf(fp, "\t}\n");
		}

fprintf(fp,"}\n\n");

biptr++;
	}
// Done going through the list
endwin();
exit(0);
}

int get_ml_info(struct board *biptr)
{
int line = 2;
int key;

// Section here to pop-up a new window and get the ML info
pwin = newwin(15,40,5,20);
wbkgd(pwin, COLOR_PAIR(1));		// Can be OR'ed with A_BOLD
wattrset(pwin,COLOR_PAIR(1)|A_BOLD);
box(pwin,0,0);
// Single-key-mode   keypad(pwin, TRUE);

for (mlptr = mltypes; *mlptr != NULL; mlptr++)
{
mvwaddstr(pwin,line,5,*mlptr);
line++;
}

mvwaddstr(pwin,11,1, "Use arrow keys to highlight the ");
mvwaddstr(pwin,12,1, "desired Media Load for this board    ");
mvwaddstr(pwin,13,1, " Press ENTER to select - Q to quit   ");

// Reset to beginning of list
mlptr = mltypes;
line = 2;
scr_brite(pwin);
mvwaddstr(pwin,2,5,*mlptr);
wrefresh(pwin);
while (1)
{
noecho();
raw();
flushinp();
// typeahead(-1);
scrollok(pwin,TRUE);
keypad(pwin, TRUE);
touchwin(pwin);
wrefresh(pwin);
key = wgetch(pwin);
switch (key)
	{
	case KEY_DOWN:
		if (*mlptr++ == NULL)
		{
				beep();
				beep();
				mlptr--;
				break;
		}
		mlptr--;
		scr_norm(pwin);
		mvwaddstr(pwin,line,5,*mlptr);
		mlptr++;
		line++;
		scr_brite(pwin);
		mvwaddstr(pwin,line,5,*mlptr);
		break;

	case KEY_UP:
		if (mlptr == &mltypes[0])
		{
				beep();
				beep();
				break;
		}
		scr_norm(pwin);
		mvwaddstr(pwin,line,5,*mlptr);
		line--;
		mlptr--;
		scr_brite(pwin);
		mvwaddstr(pwin,line,5,*mlptr);
		break;

	case KEY_ENTER:
	case '\n':
	case '\r':
		werase(pwin);
		wrefresh(pwin);
		delwin(pwin);
// GFG Add here to select correctly
		strncpy(biptr->mload, *mlptr,2);
		if (biptr->mload[1] == ' ')
			biptr->mload[1] = '\0';
		return(0);
		break;

	case 'q':
	case 'Q':
		werase(pwin);
		wrefresh(pwin);
		delwin(pwin);
		return(-1);
		break;
	default:
		beep();
		break;
	}

}
// To here
werase(pwin);
wrefresh(pwin);
delwin(pwin);
}

void reset_ml()
{

pwin = newwin(10,50,5,20);
wbkgd(pwin, COLOR_PAIR(1));		// Can be OR'ed with A_BOLD
wattrset(pwin,COLOR_PAIR(1)|A_BOLD);
box(pwin,0,0);
	mvwaddstr(pwin,2,7,"NOTICE! The Media Load that you chose");
	mvwaddstr(pwin,3,7,"is not valid for this board. ");
	mvwaddstr(pwin,4,7,"The best alternative has been" );
	mvwaddstr(pwin,5,7,"selcted for you.             " );
			mvwaddstr(pwin,7,10,"Press ENTER to return ");
			wrefresh(pwin);
			wgetch(pwin);
			werase(pwin);
			delwin(pwin);
}


void bld_prname_t1(struct board *biptr)
{
if(biptr->feproto[0] == 'Q')
	sprintf(prname,"qsigt1");
if(biptr->feproto[0] == 'C')
	sprintf(prname,"cas");
return;
}

void bld_prname_e1(struct board *biptr)
{
if(biptr->feproto[0] == 'Q')
	sprintf(prname,"qsige1");
if(biptr->feproto[0] == 'C')
	sprintf(prname,"r2mf");
return;
}
