/*
 * tgitest.c - Test the TGI environment for ACS/Bayonne.
 *
 * Purpose: Log (to syslog) the environment sent to this script by ACS/Bayonne.
 *  Author:	Jay Vaughan (TekLab Custom Software - http://www.teklab.com/)
 *    Date:	20 April 2000 (v1.0)
 * License:
 *
 *   This source is covered by the GNU GPL, see COPYING for details.
 *
 *   Please let me know if you make any changes/updates to this code, also
 *   if you use it for any projects I'd be happy to hear about it!)
 *
 *														Jay 
 *   													- jv@teklab.com
 *
 *   Usage:
 *
 * 	 To use this script, you must first have ACS/Bayonne installed and 
 * 	 working properly on your system.  An example aascript to execute
 * 	 this application would look something like this:
 *
 * 	 			libexec tgitest hello there tgiapp
 *
 * 	 Put this command in a file called "tgitester" in your aascripts dir
 * 	 and then call it from your other ACS/Bayonne scripts (for example, 
 * 	 when the caller hits '9' or something).  Check the logfile (see notes
 * 	 below) and observe the output from tgitest as executed by ACS/Bayonne.
 * 	 
 *   For more details on how TGI script execution works, please refer to
 *   the ACS/Bayonne documentation at http://www.bayonne.cx/
 */

#include <stdio.h>
#include <syslog.h>

/* 
 * The environment variables set by ACS/Bayonne for this libexec call. These
 * are *separate* from the actual arguments passed to the script, which are
 * encoded in HTTP form, and stored in "PORT_QUERY" environment variable.
 * 
 */
char srvenv[6][20] = { "SERVER_VERSION", "SERVER_PLATFORM",
	"SERVER_SOFTWARE", "SERVER_RUNTIME",
	"SERVER_PROMPTS", "SERVER_LIBEXEC"
};

char prtenv[6][20] = { "PORT_NUMBER", "PORT_QUERY",
	"PORT_DIGITS", "PORT_DNID",
	"PORT_ANI", "PORT_CID"
};

/* 
 * Arguments are passed to TGI scripts in HTTP form.  tgiarg's are used
 * in decoding these arguments for use by the script.  For the purposes of
 * this demonstration, assume that the max # of args being passed is 10.
 */
typedef struct {
	char name[128];
	char val[128];
} tgiarg;

#define NUMTGIARGS 10
tgiarg tgiargs[NUMTGIARGS];

/*
 * tgiopts sends all of its output to syslog.  See your /etc/syslog.conf
 * file for details on where local0/info messages are being sent. (Most
 * likely to /var/log/messages ...
 */
#define TGILOGOPTS (LOG_LOCAL0 | LOG_INFO)

/* 
 * Some helper functions for use in decoding the HTTP ARGS passed in PORT_QUERY
 */

void getword(char *word, char *line, char stop);
char x2c(char *what);
void unescape_url(char *url);
void plustospace(char *str);
char strrrepl(char *dst, char *src, char *pat, char *rep);

/*
 * main() 
 */
int main()
{
	int i;
	char envstr[128];
	int x, m = 0;
	char *cl;

	/* Show the environment that tgitest was passed from ACS/Bayonne */
	syslog(TGILOGOPTS, "tgitest startup.  Here's some info:");
	for (i = 0; i <= 5; i++) {
		sprintf(envstr, "%s = %s\n", srvenv[i], getenv(srvenv[i]));
		//printf(envstr);
		syslog(TGILOGOPTS, envstr);
	}

	for (i = 0; i <= 5; i++) {
		sprintf(envstr, "%s = %s\n", prtenv[i], getenv(prtenv[i]));
		//printf(envstr);
		syslog(TGILOGOPTS, envstr);
	}

	/* Now break down the PORT_QUERY variable to get the arg list */
	cl = (char *) getenv("PORT_QUERY");
	if (cl == NULL) {
		syslog(TGILOGOPTS, "No TGI ARGS information to decode.\n");
	} else {

		for (x = 0; cl[0] != '\0'; x++) {
			m = x;
			getword(tgiargs[x].val, cl, '&');
			plustospace(tgiargs[x].val);
			unescape_url(tgiargs[x].val);
			getword(tgiargs[x].name, tgiargs[x].val, '=');
		}

		for (i = 0; i < NUMTGIARGS; i++) {
			sprintf(envstr, "arg %d (%s) = \"%s\"",
					i, tgiargs[i].name, tgiargs[i].val);
			syslog(TGILOGOPTS, envstr);
		}

	}

	/* Now send something back to the ACS/Bayonne script */
	syslog(TGILOGOPTS, "setting myvar to 'hello_world'\n");
	fprintf(stdout, "SET %s myvar hello_world\n", getenv("PORT_NUMBER"));

	syslog(TGILOGOPTS, "tgitest finished.  Goodbye!\n");

	/* 
	 * And now tell ACS/Bayonne that we're done on this port, with an
	 * exit code of 0.  Your TGI script should set an exit code accordingly.
	 */
	fprintf(stdout, "EXIT %s d\n", getenv("PORT_NUMBER"), 0);
}


/*
 *
 * The rest of these func()'s are just for parsing the PORT_QUERY http-style
 * arguments passed to us from ACS/Bayonne.  Some of these aren't used by
 * tgitest.c, but are here for reference anyway... (JAY)
 *
 */

#define LF 10
#define CR 13

void getword(char *word, char *line, char stop)
{
	int x = 0, y;

	for (x = 0; ((line[x]) && (line[x] != stop)); x++)
		word[x] = line[x];

	word[x] = '\0';
	if (line[x])
		++x;
	y = 0;

	while (line[y++] = line[x++]);
}

char *makeword(char *line, char stop)
{
	int x = 0, y;
	char *word = (char *) malloc(sizeof(char) * (strlen(line) + 1));

	for (x = 0; ((line[x]) && (line[x] != stop)); x++)
		word[x] = line[x];

	word[x] = '\0';
	if (line[x])
		++x;
	y = 0;

	while (line[y++] = line[x++]);
	return word;
}

char *fmakeword(FILE * f, char stop, int *cl)
{
	int wsize;
	char *word;
	int ll;

	wsize = 102400;
	ll = 0;
	word = (char *) malloc(sizeof(char) * (wsize + 1));

	while (1) {
		word[ll] = (char) fgetc(f);
		if (ll == wsize) {
			word[ll + 1] = '\0';
			wsize += 102400;
			word = (char *) realloc(word, sizeof(char) * (wsize + 1));
		}
		--(*cl);
		if ((word[ll] == stop) || (feof(f)) || (!(*cl))) {
			if (word[ll] != stop)
				ll++;
			word[ll] = '\0';
			return word;
		}
		++ll;
	}
}

char x2c(char *what)
{
	register char digit;

	digit =
		(what[0] >= 'A' ? ((what[0] & 0xdf) - 'A') + 10 : (what[0] - '0'));
	digit *= 16;
	digit +=
		(what[1] >= 'A' ? ((what[1] & 0xdf) - 'A') + 10 : (what[1] - '0'));
	return (digit);
}

void unescape_url(char *url)
{
	register int x, y;

	for (x = 0, y = 0; url[y]; ++x, ++y) {
		if ((url[x] = url[y]) == '%') {
			url[x] = x2c(&url[y + 1]);
			y += 2;
		}
	}
	url[x] = '\0';
}

void plustospace(char *str)
{
	register int x;

	for (x = 0; str[x]; x++)
		if (str[x] == '+')
			str[x] = ' ';
}

int rind(char *s, char c)
{
	register int x;
	for (x = strlen(s) - 1; x != -1; x--)
		if (s[x] == c)
			return x;
	return -1;
}

int getline(char *s, int n, FILE * f)
{
	register int i = 0;

	while (1) {
		s[i] = (char) fgetc(f);
		s[i] = (char) fgetc(f);

		if (s[i] == CR)
			s[i] = fgetc(f);

		if ((s[i] == 0x4) || (s[i] == LF) || (i == (n - 1))) {
			s[i] = '\0';
			return (feof(f) ? 1 : 0);
		}
		++i;
	}
}

void send_fd(FILE * f, FILE * fd)
{
	int num_chars = 0;
	char c;

	while (1) {
		c = fgetc(f);
		if (feof(f))
			return;
		fputc(c, fd);
	}
}

int ind(char *s, char c)
{
	register int x;

	for (x = 0; s[x]; x++)
		if (s[x] == c)
			return x;

	return -1;
}

void escape_shell_cmd(char *cmd)
{
	register int x, y, l;

	l = strlen(cmd);
	for (x = 0; cmd[x]; x++) {
		if (ind("&;`'\"|*?~<>^()[]{}$\\", cmd[x]) != -1) {
			for (y = l + 1; y > x; y--)
				cmd[y] = cmd[y - 1];
			l++;				/* length has been increased */
			cmd[x] = '\\';
			x++;				/* skip the character */
		}
	}
}

void escape_html(char *buf1, char *buf2)
{

	register int x, y, l;

	for (x = 0, y = 0; buf1[x]; x++) {
		switch (buf1[x]) {
		case '<':
			buf2[y] = '&';
			buf2[y + 1] = 'l';
			buf2[y + 2] = 't';
			y += 3;
			break;
		case '>':
			buf2[y] = '&';
			buf2[y + 1] = 'g';
			buf2[y + 2] = 't';
			y += 3;
			break;
		default:
			buf2[y++] = buf1[x];
			break;
		};
		buf2[y] = '\0';
	}
}
