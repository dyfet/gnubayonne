// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#undef	PACKAGE
#undef	VERSION
#undef	bool

#include "bayonne.h"

#ifdef	CCXX_NAMESPACES
using namespace ost;
using namespace std;
#endif

extern "C" {
void mkinterp(const char *imp);
void rminterp(void);
void execinterp(char *cmd);
};

class TGIPython : protected TGI
{
protected:
	void script(char *cmd, char **args);

public:
	TGIPython();
	virtual ~TGIPython();
} python;

TGIPython::TGIPython() :
TGI()
{
	mkinterp(keyimports.getLast("python"));
	slog(Slog::levelDebug) << "tgi: python interpreter loaded" << endl;
}

TGIPython::~TGIPython()
{
	rminterp();
}

void TGIPython::script(char *cmd, char **args)
{
	char *ext = strrchr(cmd, '.');
	if (ext == NULL)
		return;

	if(strcmp(ext, ".py"))
		if(strcmp(ext, ".python"))
			return;

	execinterp(cmd);
	slog(Slog::levelError) << "tgi: " << cmd << ": failed to parse" << endl;
	exit(-1);
}
	
