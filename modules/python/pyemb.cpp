// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#undef	PACKAGE
#undef	VERSION

extern "C" {
#include <Python.h>

void mkinterp(const char *imp)
{
	char *cp;
	char imports[1024];
	char string[512];

	if(!imp)
		imp = "";
	Py_Initialize();
	strncpy(imports, imp, 1023);
	imports[1023] = 0;
	cp = strtok(imports, " ,;:\t");
	while(cp)
	{	
		sprintf(string, "import %s\n", cp);
		PyRun_SimpleString(string);
		cp = strtok(NULL, " ,;:\t");
	}
}

void rminterp(void)
{
	Py_Exit(0);
}

void execinterp(char *cmd)
{
	static char *argv[3];
	int argc = 3;
	FILE	*fp;
	
	argv[0] = "-";
	argv[1] = cmd;
	argv[2] = NULL;

	PySys_SetArgv(argc, argv);
	fp = fopen(cmd, "r");
	if(!fp)
		argc = -1;
	else
	{
		argc = PyRun_SimpleFile(fp, cmd);
		fclose(fp);
	}
	Py_Exit(argc);
}
	
}
