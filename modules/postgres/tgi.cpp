// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <server.h>
#include <cc++/process.h>

#ifdef	HAVE_PGSQL_POSTGRES
#define	DLLIMPORT
#define	HAVE_NAMESPACE_STD
#define	HAVE_CXX_STRING_HEADER
#include <pgsql/libpq++.h>
#else
#include <libpq++.h>
#endif

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

#define	SYM_SQLDRIVER	"sql.driver"
#define	SYM_ROWS	"sql.rows"
#define	SYM_COLS	"sql.cols"
#define	SYM_DATABASE	"sql.database"
#define	SYM_SQLERROR	"sql.error"

class TGIPostgres : private TGI, public Keydata
{
private:
        virtual int parse(int fd, char port[64], char *cmd);
        virtual bool getExtension(const char *ext);
	void script(char *cmd, char **args);

public:
	TGIPostgres();
} pgtgi;

TGIPostgres::TGIPostgres() :
TGI(), Keydata("/bayonne/sql")
{
	static Keydata::Define keydefs[] = {
	{"database", "bayonne"},
	{NULL, NULL}};

	const char *cp;

	load(keydefs);

	cp = getLast("host");
	if(cp)
		Process::setEnv("PGHOST", cp, true);

	cp = getLast("port");
	if(cp)
		Process::setEnv("PGPORT", cp, true);

	slog(Slog::levelDebug) << "tgi: postgres interpreter loaded" << endl;
}

bool TGIPostgres::getExtension(const char *ext)
{
	if(!stricmp(ext, "sql"))
		return true;

	return false;
}

int TGIPostgres::parse(int fd, char port[64], char *cmd)
{
        const char *dbname, *user, *password;
	char buf[256];
	PgDatabase *dbase;
	ExecStatusType status;
	const char *errmsg;

	if(strnicmp(cmd, "sql ", 4))
		return -1;

        dbname = getLast("database");
        user = getLast("user");
        password = getLast("password");

	if(password)
		snprintf(buf, sizeof(buf), "dbname=%s user=%s password=%s",
			dbname, user, password);
	else if(user)
		snprintf(buf, sizeof(buf), "dbname=%s user=%s",
			dbname, user);
	else
		snprintf(buf, sizeof(buf), "dbname=%s", dbname);

	dbase = new PgDatabase(buf);
	if(dbase)
		slog(Slog::levelDebug) << "tgi: connecting database " << dbname << endl;
	else
	{
		slog(Slog::levelDebug) << "tgi: cannot connect " << dbname << endl;
		return -1;
	}

	status = dbase->Exec(cmd + 4);
	
	switch(status)
	{
	case PGRES_FATAL_ERROR:
		errmsg = dbase->ErrorMessage();
		slog(Slog::levelCritical) << "tgi: " << errmsg << endl;
		break;
	case PGRES_NONFATAL_ERROR:
	case PGRES_BAD_RESPONSE:
		errmsg = dbase->ErrorMessage();
		slog(Slog::levelError) << "tgi: " << errmsg << endl;
		break;
	case PGRES_EMPTY_QUERY:
	case PGRES_COMMAND_OK:
	case PGRES_COPY_OUT:
	case PGRES_COPY_IN:
		break;
	}

	delete dbase;
	return 0;
}

#ifdef	CCXX_NAMESPACES
};
#endif
