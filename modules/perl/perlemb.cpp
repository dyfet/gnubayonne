// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#undef	PACKAGE
#undef	VERSION

extern "C" {
#include <EXTERN.h>
#include <perl.h>

#ifdef	XS_INIT
EXTERN_C void xs_init(pTHXo);
#endif

void *mkinterp(void)
{
	PerlInterpreter *interp = perl_alloc();
	perl_construct(interp);
	return interp;
}

void rminterp(void *interp)
{
	perl_destruct((PerlInterpreter *)interp);
	perl_free((PerlInterpreter *)interp);
}

void execinterp(void *interp, char *cmd)
{
	static char *argv[3];
	int argc;
	
	argv[0] = "-";
	argv[1] = cmd;
	argv[2] = NULL;

#ifdef	XS_INIT
	if(perl_parse((PerlInterpreter *)interp, xs_init, 2, argv, (char **)NULL))
		return;
#else
	if(perl_parse((PerlInterpreter *)interp, NULL, 2, argv, (char **)NULL))
		return;
#endif

	argc = perl_run((PerlInterpreter *)interp);
	perl_destruct((PerlInterpreter *)interp);
	perl_free((PerlInterpreter *)interp);
	exit(argc);
}
	
}
