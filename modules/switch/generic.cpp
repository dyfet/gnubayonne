// Copyright (C) 2000 Open Source Telecom Corporation.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
// The generic switch integration module supports setting and clearing of
// message waiting status by dialing extension numbers directly through 
// the bayonne server.  Many PBX's have the ability to set and clear mwi
// status by such dialing codes.  A mwi.mod PBX integration script is also
// used with entries for common pbx vendors.

#include <server.h>
#include <cc++/serial.h>

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

class MWI : public Module, public Keydata
{
public:
	MWI();

private:
	modtype_t getType(void)
		{return MODULE_SWITCH;};

	char *getName(void)
		{return "mwi";};

	char *dispatch(Trunk *trunk);

} mwi;

MWI::MWI() : Module(), Keydata("/bayonne/switch")
{
	static Keydata::Define keys[] = {
		{"protocol", "none"},
		{"group", "*"},
		{"timeout", "30s"},
		{NULL, NULL}};

	load(keys);

	aascript->addModule(this);
}

char* MWI::dispatch(Trunk *trunk)
{
	TrunkGroup *grp = getGroup(getLast("group"));
	const char *mem = trunk->getMember();
	const char *swi = getLast("protocol");
	const char *value;
	char *argv[8];
	char name[65];
	char extensions[256];
	int len = 0;

	if(!stricmp(swi, "none"))
		return NULL;

	if(!grp)
		return "mwi-invalid-group";

	if(!mem)
		mem = "mwi.mode=on";
	else
		mem = "mwi.mode=off";

	extensions[0] = 0;
	snprintf(name, sizeof(name), "mwi::%s", swi);

	while(NULL != (value = trunk->getValue(NULL)))
	{
		if(!len)
		{
			sprintf(extensions, "mwi.extensions=%s", value);
			len = strlen(extensions);
			continue;
		}
		extensions[len++] = ',';
		if(len + strlen(value) >= sizeof(extensions))
			break;
		strcpy(extensions + len, value);
		len += strlen(value);
	}
	if(!extensions[0])
		return NULL;

	argv[0] = name;
	argv[1] = extensions;
	argv[2] = (char *)mem;
	argv[3] = NULL;

	request(grp, argv, getSecTimeout(getLast("timeout")), "mwi");	
	
	return NULL;			
}

#ifdef	CCXX_NAMESPACES
};
#endif
