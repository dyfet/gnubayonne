// Copyright (C) 2000 Open Source Telecom Corporation.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <server.h>
#include <cc++/serial.h>

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

class SMDI : public TTYStream, public Module, public Server, public Keydata, public Mutex
{
public:
	SMDI();

private:
	void stop(void);
	void run(void);
	modtype_t getType(void)
		{return MODULE_SWITCH;};

	char *getName(void)
		{return "mwi";};

	char *dispatch(Trunk *trunk);

	void setMsgWaiting(const char *target);
	void clrMsgWaiting(const char *target);

} smdi;

SMDI::SMDI() : TTYStream(), Module(), Server(keythreads.priSwitch()), Keydata("/bayonne/switch"), Mutex()
{
	static Keydata::Define keys[] = {
		{"device", "/dev/ttyS0"},
		{"speed", "1200"},
		{"desk", "0"},
		{NULL, NULL}};

	load(keys);
	aascript->addModule(this);
}

void SMDI::stop(void)
{
	terminate();
	endSerial();
}

void SMDI::run(void)
{
	char cidbuf[65];
	char dnisbuf[65];
	char clid[65];
	char dnid[65];
	char *argv[8];
	int argc = 1;
	TrunkEvent event;
	Trunk *trunk;
	unsigned desk, port;
	char *forward, *token;
	unsigned fdesk = atoi(getLast("desk"));

	char buffer[128];
	char *cp, *p1, *p2;

	setCancel(cancelImmediate);
	open(getLast("device"));

	setSpeed(atol(getLast("speed")));
	setCharBits(7);
	setParity(parityEven);
	setFlowControl(flowBoth);
	setLineInput('\r', '\n');

	for(;;)
	{
		if(!isPending(pendingInput, 3000))
		{
			Thread::yield();
			continue;
		}
		getline(buffer, sizeof(buffer) - 1);
		cp = buffer;
		while(*cp == ' ' || *cp == '\t' || *cp == '\n')
                        ++cp;

                if(!*cp)
                        continue;

		if(strncmp(cp, "MD ", 3))
			continue;

		desk = atoi(cp + 3);
		if(fdesk && desk != fdesk)
			continue;

		cp = strchr(cp + 3, ' ');
		port = atoi(++cp);
		trunk = Driver::drvFirst->getTrunkPort(port);
		if(!trunk)
			continue;

		cp = strchr(cp, ' ');
		p1 = strtok_r(cp + 2, " \t\n", &token);
		p2 = strtok_r(NULL, " \t\n", &token);
		if(p1)
			if(!*p1)
				p1 = NULL;
		if(p2)
			if(!*p2)
				p2 = NULL;

		cidbuf[0] = 0;
		dnisbuf[0] = 0;
		clid[0] = 0;
		dnid[0] = 0;			
	
		switch(*++cp)
		{
		case 'A':
			forward = "session.callfwd=all";
			break;
		case 'B':
			forward = "session.callfwd=busy";
			break;
		case 'N':
			forward = "session.callfwd=na";
			break;
		default:
			forward = "session.callfwd=none";	
		}

		switch(*cp)
		{
		case 'A':
		case 'B':
		case 'N':
			if(p1)
			{
				snprintf(dnisbuf, sizeof(dnisbuf),
					"%s=pstn:%s", SYM_DIALED, p1);
				snprintf(dnid, sizeof(dnid),
					"%s=%s", SYM_DNID, p1);
			}
			p1 = p2;	
		default:	
			if(p1)
			{
				snprintf(cidbuf, sizeof(cidbuf),
					"%s=pstn:%s", SYM_CALLER, p1);
				snprintf(clid, sizeof(clid),
					"%s=%s", SYM_CLID, p1);
			}
		}
		
		argv[0] = forward;
		if(dnisbuf[0])
			argv[argc++] = dnisbuf;
		if(dnid[0])
			argv[argc++] = dnid;
		if(clid[0])
			argv[argc++] = clid;
		if(cidbuf[0])
			argv[argc++] = cidbuf;
		argv[argc] = NULL;		
		event.id = TRUNK_RING_START;
		event.parm.argv = argv;
		trunk->postEvent(&event);
	}
}

void SMDI::setMsgWaiting(const char *target)
{
	enterMutex();
	*this << "OP:MWI " << target << "!\004";
	flush();
	flushOutput();
	leaveMutex();
}

void SMDI::clrMsgWaiting(const char *target)
{
	enterMutex();
	*this << "RMV:MWI " << target << "!\004";
	flush();
	flushOutput();
	leaveMutex();
}

char* SMDI::dispatch(Trunk *trunk)
{
	const char *mem = trunk->getMember();
	const char *value;

	if(!mem)
		mem = "on";

	while(NULL != (value = trunk->getValue(NULL)))
	{

		if(!stricmp(mem, "off"))
			clrMsgWaiting(value);
		else
			setMsgWaiting(value);
	}
	return NULL;			
}

#ifdef	CCXX_NAMESPACES
};
#endif
