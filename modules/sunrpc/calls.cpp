// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <server.h>
#include "bayonne_rpc.h"

#ifdef	CCXX_NAMESPACES
using namespace ost;
using namespace std;
#endif

bayonne_error *bayonne_command_1_svc(bayonne_command *cmd, struct svc_req *svc)
{
	static bayonne_error result;
	Module *mod = getModule(MODULE_FIFO, cmd->mod_name);
	if(!mod)
	{
		result = BAYONNE_INVALID_MODULE;
		return &result;
	}

	if(!mod->command(cmd->argv.argv_val, NULL))
		result = BAYONNE_FAILURE;
	
	result = BAYONNE_SUCCESS;
}

bayonne_error *bayonne_module_1_svc(char **name, struct svc_req *svc)
{
	static bayonne_error result;

	result = BAYONNE_SUCCESS;
	if(!getModule(MODULE_ANY, *name))
		result = BAYONNE_INVALID_MODULE;

	return &result;
}

bayonne_error *bayonne_reserve_1_svc(bayonne_reserve *res, struct svc_req *svc)
{
	static bayonne_error error = BAYONNE_SUCCESS;
	Mixer *mix = Driver::drvFirst->getMixer(res->conf_mixer);
	Conference *cf;
	int group;

	if(!mix)
	{
		error = BAYONNE_INVALID_MIXER;
		return &error;
	}

	if(!mix->setMixer(res->conf_groups, res->conf_alloc))
	{
		error = BAYONNE_FAILURE;
		return &error;
	}		
	if(!res->conf_groups)
		return &error;

	for(group = 0; group < mix->getGroups(); ++group)
	{
		cf = mix->getConference(group);
		if(!cf)
		{
			error = BAYONNE_INVALID_CONFERENCE;
			return &error;
		}
		if(group < res->conf_groups)
			cf->setConference(res->conf_limits.conf_limits_val[group]);
		else
			cf->setConference(0);
	}
	return &error;
}

bayonne_conf *bayonne_conference_1_svc(int *id, struct svc_req *svc)
{
	static bayonne_conf conf;
	Conference *cf = Driver::drvFirst->getConference(*id);
	memset(&conf, 0, sizeof(conf));
	if(cf)
	{
		conf.count = cf->getMembers();
		conf.limit = cf->getLimit();
		conf.members.members_len = conf.count;
		conf.members.members_val = cf->getMembership();
	}
	return &conf;
}

bayonne_mixer *bayonne_mixer_1_svc(int *id, struct svc_req *svc)
{
	static bayonne_mixer mixer;
	static bayonne_conf conf[128];
	Mixer *mix = Driver::drvFirst->getMixer(*id);
	Conference *cf;
	int grp = 0;
	int groups;

	memset(&mixer, 0, sizeof(mixer));
	if(mix)
	{
		groups = mix->getGroups();
		mixer.mixer_avail = mix->getAvail();
		mixer.mixer_used = mix->getMembers();
		mixer.mixer_groups = groups;
		mixer.mixer_conf.mixer_conf_len = groups;
		mixer.mixer_conf.mixer_conf_val = conf;
		while(grp < groups)
		{
			cf = mix->getConference(grp);
			if(!cf)
			{
				++grp;
				continue;
			}
			conf[grp].count = cf->getMembers();
			conf[grp].limit = cf->getLimit();
			conf[grp].members.members_len = conf[grp].count;
			conf[grp].members.members_val = cf->getMembership();
			++grp;
		}
	}
	return &mixer;
}

bayonne_info *bayonne_query_1_svc(void *, struct svc_req *reqsvc)
{
	TrunkGroup *grp = getGroup("*");
	static bayonne_info info;
	if(grp)
		strcpy(info.tgi_policy, grp->getLast("groups"));
	strcpy(info.tgi_node, keyserver.getNode());
	strcpy(info.tgi_user, keyserver.getLast("user"));
	strcpy(info.tgi_token , keyserver.getToken());
	strcpy(info.tgi_driver, plugins.getDriverName());
	strcpy(info.tgi_version, getenv("SERVER_VERSION"));
	info.ports = Driver::drvFirst->getTrunkCount();
	info.used = Driver::drvFirst->getTrunkUsed();
	info.mixers = Driver::drvFirst->getMixers();
	info.conferences = Driver::drvFirst->getGroups();
	info.uid = keyserver.getUid();
	info.gid = keyserver.getGid();
	info.nodes = keyserver.getNodeCount();
	
	return(&info);
}

char **bayonne_status_1_svc(void *, struct svc_req *req)
{
	char status[256];
	static char *stat = status;
	Driver::drvFirst->getStatus(status);
	return(&stat);
}

bayonne_error *bayonne_down_1_svc(void *, struct svc_req *req)
{
	static bayonne_error result = BAYONNE_SUCCESS;

	raise(SIGINT);
	return &result;
}

bayonne_error *bayonne_compile_1_svc(void *, struct svc_req *req)
{
	static bayonne_error result = BAYONNE_SUCCESS;
	Driver *drv = Driver::drvFirst;
	while(drv)
	{
		drv->getImage();
		drv = drv->drvNext;
	}
	return &result;
}

bayonne_error *bayonne_schedule_1_svc(char **sched, struct svc_req *req)
{
	static bayonne_error result = BAYONNE_SUCCESS;
	if(!stricmp(*sched, "*"))
		scheduler.altSchedule(NULL);
	else
		scheduler.altSchedule(*sched);
	return &result;
}	

bayonne_error *bayonne_reload_1_svc(char **name, struct svc_req *req)
{
	static bayonne_error result;
	Module *mod;

	result = BAYONNE_SUCCESS;
	
	mod = getModule(MODULE_FIFO, *name);
	if(mod)
		mod->reload();
	else
		result = BAYONNE_INVALID_MODULE;
	return &result;
}

bayonne_nodes *bayonne_network_1_svc(char **name, struct svc_req *req)
{
	static bayonne_nodes nodes;
	nodes.bayonne_nodes_len = 0;
	nodes.bayonne_nodes_val = (bayonne_node *)getNodes(*name);

	if(nodes.bayonne_nodes_val)
	{
		if(*name)
			nodes.bayonne_nodes_len = 1;
		else
			nodes.bayonne_nodes_len = keyserver.getNodeCount();
	}
	return &nodes;
}

bayonne_error *bayonne_disconnect_1_svc(int *port, struct svc_req *req)
{
	static bayonne_error result;
	Trunk *trunk = Driver::drvFirst->getTrunkPort(*port);
	TrunkEvent event;

	result = BAYONNE_SUCCESS;
	if(!trunk)
	{
		result = BAYONNE_INVALID_PORT;
		return &result;
	}

	event.id = TRUNK_STOP_DISCONNECT;
	if(!trunk->postEvent(&event))
		result = BAYONNE_FAILURE;

	return &result;
}
	
bayonne_error *bayonne_busy_1_svc(int *port, struct svc_req *req)
{
	static bayonne_error result;
	Trunk *trunk = Driver::drvFirst->getTrunkPort(*port);
	TrunkEvent event;

	result = BAYONNE_SUCCESS;
	if(!trunk)
	{
		result = BAYONNE_INVALID_PORT;
		return &result;
	}

	event.id = TRUNK_MAKE_BUSY;
	if(!trunk->postEvent(&event))
		result = BAYONNE_FAILURE;

	return &result;
}
	
bayonne_error *bayonne_idle_1_svc(int *port, struct svc_req *req)
{
	static bayonne_error result;
	Trunk *trunk = Driver::drvFirst->getTrunkPort(*port);
	TrunkEvent event;

	result = BAYONNE_SUCCESS;
	if(!trunk)
	{
		result = BAYONNE_INVALID_PORT;
		return &result;
	}

	event.id = TRUNK_MAKE_IDLE;
	if(!trunk->postEvent(&event))
		result = BAYONNE_FAILURE;

	return &result;
}

bayonne_error *bayonne_start_1_svc(bayonne_start *start, struct svc_req *req)
{
	static bayonne_error result;
	Trunk *trunk;
	TrunkEvent event;
	int port = Driver::drvFirst->getTrunkCount();
	TrunkGroup *pol, *my = getGroup(start->policy);

	result = BAYONNE_SUCCESS;
	if(start->port > -1)
	{
		if(!trunk)
		{	
			result = BAYONNE_INVALID_PORT;
			return &result;
		}
		event.parm.argv = start->argv.argv_val;
		event.id = TRUNK_START_SCRIPT;
		if(!trunk->postEvent(&event))
			result = BAYONNE_FAILURE;
		return &result;
	}	
	while(port)
	{
		--port;
		pol = Driver::drvFirst->getTrunkGroup(port);
		if(!pol)
			continue;
		if(pol != my)
			continue;
		event.id = TRUNK_START_SCRIPT;
		event.parm.argv = start->argv.argv_val;
		if(trunk->postEvent(&event))
			return &result;
	}
	result = BAYONNE_FAILURE;
	return &result;
}			

bayonne_error *bayonne_ring_1_svc(int *port, struct svc_req *req)
{
	static char *argv[] = {NULL};
	static bayonne_error result;
	Trunk *trunk = Driver::drvFirst->getTrunkPort(*port);
	TrunkEvent event;

	result = BAYONNE_SUCCESS;
	if(!trunk)
	{
		result = BAYONNE_INVALID_PORT;
		return &result;
	}

	event.parm.argv = argv;
	event.id = TRUNK_RING_START;
	if(!trunk->postEvent(&event))
		result = BAYONNE_FAILURE;

	return &result;
}


bayonne_error *bayonne_request_1_svc(bayonne_request *req, struct svc_req *svc)
{
	static bayonne_error result;
	TrunkGroup *pol = getGroup(req->req_policy);

	if(!pol)
	{
		result = BAYONNE_INVALID_POLICY;
		return &result;
	}
	result = BAYONNE_SUCCESS;
	request(pol, req->argv.argv_val, req->req_timeout);
	return &result;
}

bayonne_policy *bayonne_policy_1_svc(char **name, struct svc_req *req)
{
	static char schedule[32];
	static bayonne_policy policy;
	static int *ports = NULL;
	TrunkGroup *pol = getGroup(*name);
	int count = Driver::drvFirst->getTrunkCount();
	int id;

	if(!ports)
		ports = new int[count];

	policy.pol_members = 0;
	policy.pol_ports.pol_ports_len = 0;
	policy.pol_ports.pol_ports_val = ports;
	policy.pol_sched = "";
	policy.pol_name = "";
	policy.pol_number = "";
	policy.pol_active = 0;
	policy.max_incoming = policy.max_outgoing = 0;
	policy.tot_incoming = policy.tot_outgoing = 0;

	if(!pol)
		return &policy;

	pol->getSchedule(schedule);
	policy.pol_sched = schedule;
	policy.pol_name = (char *)pol->getLast("name");
	policy.pol_number = (char *)pol->getLast("number");

	for(id = 0; id < count; ++id)
	{
		if(Driver::drvFirst->getTrunkGroup(id) != pol)
			continue;

		ports[policy.pol_members++] = id;
	}
	policy.pol_active = pol->getStat(STAT_ACTIVE_CALLS);
	policy.max_incoming = pol->getStat(STAT_MAX_INCOMING);
	policy.max_outgoing = pol->getStat(STAT_MAX_OUTGOING);
	policy.tot_incoming = pol->getStat(STAT_TOT_INCOMING);
	policy.tot_outgoing = pol->getStat(STAT_TOT_OUTGOING);
	policy.pol_ports.pol_ports_len = policy.pol_members;			
	return &policy;
}
bayonne_port *bayonne_port_1_svc(int *id, struct svc_req *req)
{
	static bayonne_port port;
	Trunk *trunk = Driver::drvFirst->getTrunkPort(*id);
	char *cp;
	
	port.port_caller = "";
	port.port_dialed = "";
	port.port_name = "";
	port.port_user = "";
	port.port_info = "";
	port.port_lang = "";
	port.port_gid = "";
	port.port_policy = "";
	port.port_caps = 0;
	port.port_duration = 0;

	if(!trunk)
		return &port;

	cp = trunk->getSymbol(SYM_CALLER);
	if(cp)
		port.port_caller = cp;

	cp = trunk->getSymbol(SYM_DIALED);
	if(cp)
		port.port_dialed = cp;

	cp = trunk->getSymbol(SYM_NAME);
	if(cp)
		port.port_name = cp;

	cp = trunk->getSymbol(SYM_USER);
	if(cp)
		port.port_user = cp;

	cp = trunk->getSymbol(SYM_INFODIGITS);
	if(cp)
		port.port_info = cp;

	cp = trunk->getSymbol(SYM_LANGUAGE);
	if(cp)
		port.port_lang = cp;

	cp = trunk->getSymbol(SYM_GID);
	if(cp)
		port.port_gid = cp;

	cp = trunk->getSymbol(SYM_POLICY);
	if(cp)
		port.port_policy = cp;

	cp = trunk->getSymbol(SYM_DURATION);
	if(cp)
		port.port_duration = atol(cp);

	port.port_caps = trunk->getCapabilities();

	return &port;
}
