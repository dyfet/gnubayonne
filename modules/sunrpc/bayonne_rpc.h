/*
 * Please do not edit this file.
 * It was generated using rpcgen.
 */

#ifndef _BAYONNE_RPC_H_RPCGEN
#define _BAYONNE_RPC_H_RPCGEN

#include <rpc/rpc.h>


#ifdef __cplusplus
extern "C" {
#endif

/* Copyright (C) 2001 Open Source Telecom Corporation.
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License version 2 as published by the Free Software Foundation.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.                                                 
 */
#pragma pack(1)

struct bayonne_conf {
	int count;
	int limit;
	struct {
		u_int members_len;
		int *members_val;
	} members;
};
typedef struct bayonne_conf bayonne_conf;

struct bayonne_policy {
	char *pol_name;
	char *pol_sched;
	char *pol_number;
	int pol_active;
	int max_incoming;
	int max_outgoing;
	long tot_incoming;
	long tot_outgoing;
	int pol_members;
	struct {
		u_int pol_ports_len;
		int *pol_ports_val;
	} pol_ports;
};
typedef struct bayonne_policy bayonne_policy;

struct bayonne_mixer {
	int mixer_avail;
	int mixer_used;
	int mixer_groups;
	struct {
		u_int mixer_conf_len;
		bayonne_conf *mixer_conf_val;
	} mixer_conf;
};
typedef struct bayonne_mixer bayonne_mixer;

struct bayonne_node {
	long node_update;
	char node_name[16];
	long node_addr;
	char node_version;
	char node_buddies;
	char node_ports;
	char node_stat[255];
};
typedef struct bayonne_node bayonne_node;

struct bayonne_port {
	char *port_caller;
	char *port_dialed;
	char *port_name;
	char *port_user;
	char *port_info;
	char *port_lang;
	char *port_gid;
	char *port_policy;
	long port_caps;
	long port_duration;
};
typedef struct bayonne_port bayonne_port;

enum bayonne_error {
	BAYONNE_SUCCESS = 0,
	BAYONNE_FAILURE = 0 + 1,
	BAYONNE_INVALID_VALUES = 0 + 2,
	BAYONNE_INVALID_MIXER = 0 + 3,
	BAYONNE_INVALID_CONFERENCE = 0 + 4,
	BAYONNE_INVALID_MODULE = 0 + 5,
	BAYONNE_INVALID_COMMAND = 0 + 6,
	BAYONNE_INVALID_PORT = 0 + 7,
	BAYONNE_INVALID_POLICY = 0 + 8,
};
typedef enum bayonne_error bayonne_error;
#pragma pack()

typedef struct {
	u_int bayonne_nodes_len;
	bayonne_node *bayonne_nodes_val;
} bayonne_nodes;

struct bayonne_reserve {
	int conf_mixer;
	int conf_alloc;
	int conf_groups;
	struct {
		u_int conf_limits_len;
		int *conf_limits_val;
	} conf_limits;
};
typedef struct bayonne_reserve bayonne_reserve;

typedef char *argv_string;

struct bayonne_command {
	char *mod_name;
	struct {
		u_int argv_len;
		argv_string *argv_val;
	} argv;
};
typedef struct bayonne_command bayonne_command;

struct bayonne_start {
	int port;
	char *policy;
	struct {
		u_int argv_len;
		argv_string *argv_val;
	} argv;
};
typedef struct bayonne_start bayonne_start;

struct bayonne_request {
	char *req_policy;
	int req_timeout;
	struct {
		u_int argv_len;
		argv_string *argv_val;
	} argv;
};
typedef struct bayonne_request bayonne_request;

struct bayonne_info {
	char *tgi_user;
	char *tgi_node;
	char *tgi_version;
	char *tgi_driver;
	char *tgi_token;
	char *tgi_policy;
	u_int nodes;
	u_int ports;
	u_int used;
	u_int mixers;
	u_int conferences;
	long uid;
	long gid;
};
typedef struct bayonne_info bayonne_info;

#define BAYONNE_PROGRAM 0x29000001
#define BAYONNE_VERSION 1

#if defined(__STDC__) || defined(__cplusplus)
#define BAYONNE_COMMAND 1
extern  bayonne_error * bayonne_command_1(bayonne_command *, CLIENT *);
extern  bayonne_error * bayonne_command_1_svc(bayonne_command *, struct svc_req *);
#define BAYONNE_QUERY 2
extern  bayonne_info * bayonne_query_1(void *, CLIENT *);
extern  bayonne_info * bayonne_query_1_svc(void *, struct svc_req *);
#define BAYONNE_STATUS 3
extern  char ** bayonne_status_1(void *, CLIENT *);
extern  char ** bayonne_status_1_svc(void *, struct svc_req *);
#define BAYONNE_NETWORK 4
extern  bayonne_nodes * bayonne_network_1(char **, CLIENT *);
extern  bayonne_nodes * bayonne_network_1_svc(char **, struct svc_req *);
#define BAYONNE_MODULE 5
extern  bayonne_error * bayonne_module_1(char **, CLIENT *);
extern  bayonne_error * bayonne_module_1_svc(char **, struct svc_req *);
#define BAYONNE_MIXER 6
extern  bayonne_mixer * bayonne_mixer_1(int *, CLIENT *);
extern  bayonne_mixer * bayonne_mixer_1_svc(int *, struct svc_req *);
#define BAYONNE_CONFERENCE 7
extern  bayonne_conf * bayonne_conference_1(int *, CLIENT *);
extern  bayonne_conf * bayonne_conference_1_svc(int *, struct svc_req *);
#define BAYONNE_RESERVE 8
extern  bayonne_error * bayonne_reserve_1(bayonne_reserve *, CLIENT *);
extern  bayonne_error * bayonne_reserve_1_svc(bayonne_reserve *, struct svc_req *);
#define BAYONNE_RELOAD 9
extern  bayonne_error * bayonne_reload_1(char **, CLIENT *);
extern  bayonne_error * bayonne_reload_1_svc(char **, struct svc_req *);
#define BAYONNE_COMPILE 10
extern  bayonne_error * bayonne_compile_1(void *, CLIENT *);
extern  bayonne_error * bayonne_compile_1_svc(void *, struct svc_req *);
#define BAYONNE_DOWN 11
extern  bayonne_error * bayonne_down_1(void *, CLIENT *);
extern  bayonne_error * bayonne_down_1_svc(void *, struct svc_req *);
#define BAYONNE_SCHEDULE 12
extern  bayonne_error * bayonne_schedule_1(char **, CLIENT *);
extern  bayonne_error * bayonne_schedule_1_svc(char **, struct svc_req *);
#define BAYONNE_DISCONNECT 13
extern  bayonne_error * bayonne_disconnect_1(int *, CLIENT *);
extern  bayonne_error * bayonne_disconnect_1_svc(int *, struct svc_req *);
#define BAYONNE_BUSY 14
extern  bayonne_error * bayonne_busy_1(int *, CLIENT *);
extern  bayonne_error * bayonne_busy_1_svc(int *, struct svc_req *);
#define BAYONNE_IDLE 15
extern  bayonne_error * bayonne_idle_1(int *, CLIENT *);
extern  bayonne_error * bayonne_idle_1_svc(int *, struct svc_req *);
#define BAYONNE_RING 16
extern  bayonne_error * bayonne_ring_1(int *, CLIENT *);
extern  bayonne_error * bayonne_ring_1_svc(int *, struct svc_req *);
#define BAYONNE_PORT 18
extern  bayonne_port * bayonne_port_1(int *, CLIENT *);
extern  bayonne_port * bayonne_port_1_svc(int *, struct svc_req *);
#define BAYONNE_POLICY 19
extern  bayonne_policy * bayonne_policy_1(char **, CLIENT *);
extern  bayonne_policy * bayonne_policy_1_svc(char **, struct svc_req *);
#define BAYONNE_START 20
extern  bayonne_error * bayonne_start_1(bayonne_start *, CLIENT *);
extern  bayonne_error * bayonne_start_1_svc(bayonne_start *, struct svc_req *);
#define BAYONNE_REQUEST 21
extern  bayonne_error * bayonne_request_1(bayonne_request *, CLIENT *);
extern  bayonne_error * bayonne_request_1_svc(bayonne_request *, struct svc_req *);
extern int bayonne_program_1_freeresult (SVCXPRT *, xdrproc_t, caddr_t);

#else /* K&R C */
#define BAYONNE_COMMAND 1
extern  bayonne_error * bayonne_command_1();
extern  bayonne_error * bayonne_command_1_svc();
#define BAYONNE_QUERY 2
extern  bayonne_info * bayonne_query_1();
extern  bayonne_info * bayonne_query_1_svc();
#define BAYONNE_STATUS 3
extern  char ** bayonne_status_1();
extern  char ** bayonne_status_1_svc();
#define BAYONNE_NETWORK 4
extern  bayonne_nodes * bayonne_network_1();
extern  bayonne_nodes * bayonne_network_1_svc();
#define BAYONNE_MODULE 5
extern  bayonne_error * bayonne_module_1();
extern  bayonne_error * bayonne_module_1_svc();
#define BAYONNE_MIXER 6
extern  bayonne_mixer * bayonne_mixer_1();
extern  bayonne_mixer * bayonne_mixer_1_svc();
#define BAYONNE_CONFERENCE 7
extern  bayonne_conf * bayonne_conference_1();
extern  bayonne_conf * bayonne_conference_1_svc();
#define BAYONNE_RESERVE 8
extern  bayonne_error * bayonne_reserve_1();
extern  bayonne_error * bayonne_reserve_1_svc();
#define BAYONNE_RELOAD 9
extern  bayonne_error * bayonne_reload_1();
extern  bayonne_error * bayonne_reload_1_svc();
#define BAYONNE_COMPILE 10
extern  bayonne_error * bayonne_compile_1();
extern  bayonne_error * bayonne_compile_1_svc();
#define BAYONNE_DOWN 11
extern  bayonne_error * bayonne_down_1();
extern  bayonne_error * bayonne_down_1_svc();
#define BAYONNE_SCHEDULE 12
extern  bayonne_error * bayonne_schedule_1();
extern  bayonne_error * bayonne_schedule_1_svc();
#define BAYONNE_DISCONNECT 13
extern  bayonne_error * bayonne_disconnect_1();
extern  bayonne_error * bayonne_disconnect_1_svc();
#define BAYONNE_BUSY 14
extern  bayonne_error * bayonne_busy_1();
extern  bayonne_error * bayonne_busy_1_svc();
#define BAYONNE_IDLE 15
extern  bayonne_error * bayonne_idle_1();
extern  bayonne_error * bayonne_idle_1_svc();
#define BAYONNE_RING 16
extern  bayonne_error * bayonne_ring_1();
extern  bayonne_error * bayonne_ring_1_svc();
#define BAYONNE_PORT 18
extern  bayonne_port * bayonne_port_1();
extern  bayonne_port * bayonne_port_1_svc();
#define BAYONNE_POLICY 19
extern  bayonne_policy * bayonne_policy_1();
extern  bayonne_policy * bayonne_policy_1_svc();
#define BAYONNE_START 20
extern  bayonne_error * bayonne_start_1();
extern  bayonne_error * bayonne_start_1_svc();
#define BAYONNE_REQUEST 21
extern  bayonne_error * bayonne_request_1();
extern  bayonne_error * bayonne_request_1_svc();
extern int bayonne_program_1_freeresult ();
#endif /* K&R C */

/* the xdr functions */

#if defined(__STDC__) || defined(__cplusplus)
extern  bool_t xdr_bayonne_conf (XDR *, bayonne_conf*);
extern  bool_t xdr_bayonne_policy (XDR *, bayonne_policy*);
extern  bool_t xdr_bayonne_mixer (XDR *, bayonne_mixer*);
extern  bool_t xdr_bayonne_node (XDR *, bayonne_node*);
extern  bool_t xdr_bayonne_port (XDR *, bayonne_port*);
extern  bool_t xdr_bayonne_error (XDR *, bayonne_error*);
extern  bool_t xdr_bayonne_nodes (XDR *, bayonne_nodes*);
extern  bool_t xdr_bayonne_reserve (XDR *, bayonne_reserve*);
extern  bool_t xdr_argv_string (XDR *, argv_string*);
extern  bool_t xdr_bayonne_command (XDR *, bayonne_command*);
extern  bool_t xdr_bayonne_start (XDR *, bayonne_start*);
extern  bool_t xdr_bayonne_request (XDR *, bayonne_request*);
extern  bool_t xdr_bayonne_info (XDR *, bayonne_info*);

#else /* K&R C */
extern bool_t xdr_bayonne_conf ();
extern bool_t xdr_bayonne_policy ();
extern bool_t xdr_bayonne_mixer ();
extern bool_t xdr_bayonne_node ();
extern bool_t xdr_bayonne_port ();
extern bool_t xdr_bayonne_error ();
extern bool_t xdr_bayonne_nodes ();
extern bool_t xdr_bayonne_reserve ();
extern bool_t xdr_argv_string ();
extern bool_t xdr_bayonne_command ();
extern bool_t xdr_bayonne_start ();
extern bool_t xdr_bayonne_request ();
extern bool_t xdr_bayonne_info ();

#endif /* K&R C */

#ifdef __cplusplus
}
#endif

#endif /* !_BAYONNE_RPC_H_RPCGEN */
