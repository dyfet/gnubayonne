%/* Copyright (C) 2001 Open Source Telecom Corporation.
% *
% *
% * This program is free software; you can redistribute it and/or
% * modify it under the terms of the GNU Library General Public
% * License version 2 as published by the Free Software Foundation.
% * 
% * This library is distributed in the hope that it will be useful,
% * but WITHOUT ANY WARRANTY; without even the implied warranty of
% * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% * Library General Public License for more details.
% * 
% * You should have received a copy of the GNU Library General Public License
% * along with this library; see the file COPYING.LIB.  If not, write to
% * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
% * Boston, MA 02111-1307, USA.                                                 
% */

#ifdef	RPC_HDR
%#pragma pack(1)
#endif

struct bayonne_conf
{
	int count;
	int limit;
	int members<>;
};

struct bayonne_policy
{
	string pol_name<32>;
	string pol_sched<32>;
	string pol_number<16>;
	int pol_active;
	int max_incoming;
	int max_outgoing;
	long tot_incoming;
	long tot_outgoing;
	int pol_members;
	int pol_ports<>;
};	

struct bayonne_mixer
{
	int mixer_avail;
	int mixer_used;
	int mixer_groups;
	bayonne_conf mixer_conf<>;
};

struct bayonne_node
{
	long node_update;
	char node_name[16];
	long node_addr;
	char node_version;
	char node_buddies;
	char node_ports;
	char node_stat[255];
};

struct bayonne_port
{
	string port_caller<16>;
	string port_dialed<16>;
	string port_name<64>;
	string port_user<32>;
	string port_info<8>;
	string port_lang<32>;
	string port_gid<64>;
	string port_policy<32>;
	long port_caps;
	long port_duration;
};

enum bayonne_error
{
	BAYONNE_SUCCESS = 0,
	BAYONNE_FAILURE,
	BAYONNE_INVALID_VALUES,
	BAYONNE_INVALID_MIXER,
	BAYONNE_INVALID_CONFERENCE,
	BAYONNE_INVALID_MODULE,
	BAYONNE_INVALID_COMMAND,
	BAYONNE_INVALID_PORT,
	BAYONNE_INVALID_POLICY
};

#ifdef	RPC_HDR
%#pragma pack()
#endif

typedef bayonne_node bayonne_nodes<>;

struct bayonne_reserve {
	int conf_mixer;
	int conf_alloc;
	int conf_groups;
	int conf_limits<>;
};

typedef string argv_string<>;

struct bayonne_command {
	string mod_name<32>;
	argv_string argv<>;
};

struct bayonne_start {
	int port;
	string policy<32>;
	argv_string argv<>;
};

struct bayonne_request {
	string req_policy<32>;
	int req_timeout;
	argv_string argv<>;
};

struct bayonne_info {
	string tgi_user<32>;
	string tgi_node<32>;
	string tgi_version<16>;
	string tgi_driver<16>;
	string tgi_token<2>;
	string tgi_policy<256>;
	unsigned nodes;
	unsigned ports;
	unsigned used;
	unsigned mixers;
	unsigned conferences;
	long uid;
	long gid;
};

program BAYONNE_PROGRAM
{
	version BAYONNE_VERSION
	{
		bayonne_error BAYONNE_COMMAND(bayonne_command) = 1;
		bayonne_info BAYONNE_QUERY(void) = 2;
		string BAYONNE_STATUS(void) = 3;
		bayonne_nodes BAYONNE_NETWORK(string) = 4;
		bayonne_error BAYONNE_MODULE(string) = 5;
		bayonne_mixer BAYONNE_MIXER(int) = 6;
		bayonne_conf BAYONNE_CONFERENCE(int) = 7;
		bayonne_error BAYONNE_RESERVE(bayonne_reserve) = 8;
		bayonne_error BAYONNE_RELOAD(string) = 9;
		bayonne_error BAYONNE_COMPILE(void) = 10;
		bayonne_error BAYONNE_DOWN(void) = 11;
		bayonne_error BAYONNE_SCHEDULE(string) = 12;
		bayonne_error BAYONNE_DISCONNECT(int) = 13;
		bayonne_error BAYONNE_BUSY(int) = 14;
		bayonne_error BAYONNE_IDLE(int) = 15;
		bayonne_error BAYONNE_RING(int) = 16;
		bayonne_port BAYONNE_PORT(int) = 18;
		bayonne_policy BAYONNE_POLICY(string) = 19;
		bayonne_error BAYONNE_START(bayonne_start) = 20;
		bayonne_error BAYONNE_REQUEST(bayonne_request) = 21;
	} = 1;
} = 0x29000001;

	
