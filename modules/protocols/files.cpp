// Copyright (C) 2000 Open Source Telecom Corporation.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <server.h>
#include <cc++/strchar.h>
#include <cc++/audio.h>

#ifdef	HAVE_SSTREAM
#include <sstream>
#else
#include <strstream>
#endif

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

class CountThread : public Service
{
private:
        Dir *dir;

        void run(void);
public:
        CountThread(Trunk *trk);
        ~CountThread();
};

class ScanThread : public Service
{
private:
        Dir *dir;

        void run(void);
public:
        ScanThread(Trunk *trk);
        ~ScanThread();
};

class StatThread : public Service, public Audio
{
private:
        AudioFile au;
        char buffer[65];
        void run(void);

public:
        StatThread(Trunk *trk, char *path);
        ~StatThread();
};

ScanThread::ScanThread(Trunk *trk) :
Service(trk, 0)
{
        dir = NULL;
}

ScanThread::~ScanThread()
{
        terminate();
        if(dir)
                delete dir;
        dir = NULL;
}

void ScanThread::run(void)
{
        char buffer[6];
        unsigned count = 0;
        const char *prefix = trunk->getKeyword("prefix");
        const char *match = trunk->getKeyword("match");
        const char *ext;
        const char *file = trunk->getKeyword("file");
        const char *prev = trunk->getKeyword("prev");
        const char *next = trunk->getKeyword("next");
        const char *e, *n;
        char *cp;
        unsigned mlen;

        if(!prefix)
                prefix = trunk->getValue(NULL);

        if(!prefix)
        {
                trunk->setSymbol(SYM_ERROR, "no-prefix-dir");
                Service::failure();
        }
        if(!isDir(prefix))
        {
                trunk->setSymbol(SYM_ERROR, "not-directory");
                Service::failure();
        }

        if(!match)
                match = "";

        mlen = strlen(match);

	ext = strrchr(file, '.');

        if(prev)
                trunk->setSymbol(prev, "", 32);

        if(next)
		trunk->setSymbol(next, "", 32);
	
        dir = new Dir(prefix);
        while(NULL != (n = dir->getName()))
        {
                if(*n == '.')
                        continue;

                if(*match)
                        if(strnicmp(match, n, mlen))
                                continue;

                e = strrchr(n, '.');
                if(!e)
                        continue;

		if(ext)
	                if(stricmp(e, ext))
        	                continue;

		if(prev)
			e = trunk->getValue(prev);
		else
			e = NULL;
		if(e && *e)
		{
			if(stricmp(e, n) < 0)
				if(stricmp(file, n) > 0)
					trunk->setSymbol(prev, n, 32);
 		}
		else if(stricmp(file, n) > 0)
			trunk->setSymbol(prev, n, 32);

		if(next)
	                e = trunk->getValue(next);
		else
			e = NULL;
                if(e && *e)
                {
                        if(stricmp(e, n) > 0)
                                if(stricmp(file, n) < 0)
                                        trunk->setSymbol(next, n, 32);
                }
                else if(stricmp(file, n) < 0)
                        trunk->setSymbol(next, n, 32);

	}
	Service::success();
}

CountThread::CountThread(Trunk *trk) :
Service(trk, 0)
{
        dir = NULL;
}

CountThread::~CountThread()
{
        terminate();
        if(dir)
                delete dir;
        dir = NULL;
}

void CountThread::run(void)
{
        char buffer[6];
        unsigned count = 0;
        const char *prefix = trunk->getKeyword("prefix");
        const char *match = trunk->getKeyword("match");
        const char *ext = trunk->getKeyword("extension");
        const char *cvar = trunk->getKeyword("count");
        const char *first = trunk->getKeyword("first");
        const char *last = trunk->getKeyword("last");
        const char *e, *n;
        char *cp;
        unsigned mlen;

	if(!cvar)
		cvar = trunk->getKeyword("var");

       if(!prefix)
                prefix = trunk->getValue(NULL);

        if(!prefix)
        {
                trunk->setSymbol(SYM_ERROR, "no-prefix-dir");
                Service::failure();
        }

        if(!isDir(prefix))
        {
                trunk->setSymbol(SYM_ERROR, "not-directory");
                Service::failure();
        }

        if(!match)
                match = "";

        mlen = strlen(match);

       if(!ext)
                ext = trunk->getSymbol(SYM_EXTENSION);


        if(cvar)
                trunk->setSymbol(cvar, "0", 5);

        if(first)
        	trunk->setSymbol(first, "", 32);

        if(last)
		trunk->setSymbol(last, "", 32);

        dir = new Dir(prefix);
        while(NULL != (n = dir->getName()))
        {
                if(*n == '.')
                        continue;

                if(*match)
                        if(strnicmp(match, n, mlen))
                                continue;

                e = strrchr(n, '.');
                if(!e)
                        continue;

                if(stricmp(e, ext))
                        continue;

		if(first)
	                e = trunk->getValue(first);
		else
			e = NULL;
                if(e && *e)
                {
                        if(stricmp(e, n) > 0)
                                trunk->setSymbol(first, n, 32);
                }
                else
                        trunk->setSymbol(first, n, 32);

		if(last)
	                e = trunk->getValue(last);
		else
			e = NULL;
                if(e && *e)
                {
                        if(stricmp(e, n) < 0)
                                trunk->setSymbol(last, n, 32);
                }
                else
                        trunk->setSymbol(last, n, 32);

                ++count;
        }
        snprintf(buffer, sizeof(buffer), "%d", count);
	if(cvar)
	        trunk->setSymbol(cvar, buffer, 5);
        Service::success();
}

StatThread::StatThread(Trunk *trk, char *path) :
Service(trk, 0)
{
        snprintf(buffer, sizeof(buffer), "%s", path);
}

StatThread::~StatThread()
{
        terminate();

        if(!au.isOpen())
        {
                trunk->setSymbol(SYM_OFFSET, "0");
                trunk->setSymbol(SYM_RECORDED, "0");
        }
        au.close();
}
void StatThread::run(void)
{
        struct stat ino;
        char msgbuf[32];
        char *ann;
        const char *sym;
        unsigned hour, min, sec;
        long duration;
        const char *mem = trunk->getMember();
        const char *ext = strrchr(buffer, '/');
        const char *mimetype = "none";
        if(ext)
                ext = strrchr(ext, '.');
        else
                ext = strrchr(buffer, '.');

        if(!ext)
                ext = "";

        if(!stricmp(ext, ".snd") || !stricmp(ext, ".au"))
        {
                mimetype = "audio/basic";
                if(!mem)
                        mem = "snd";
        }
        else if(!stricmp(ext, ".wav"))
        {
                mimetype = "audio/x-basic";
                if(!mem)
                        mem = "snd";
        }
       else if(!stricmp(ext, ".vox"))
        {
                mimetype = "audio/voxware";
                if(!mem)
                        mem = "snd";
        }
        else if(!stricmp(ext, ".ps") || !stricmp(ext, ".eps"))
                mimetype = "application/postscript";
        else if(!stricmp(ext, ".gif"))
                mimetype = "image/gif";
        else if(!stricmp(ext, ".tif") || !stricmp(ext, ".tiff"))
                mimetype = "image/tiff";
        else if(!stricmp(ext, ".txt") || !stricmp(ext, ".text"))
                mimetype = "text/plain";
        else if(!stricmp(ext, ".htm") || !stricmp(ext, ".html"))
                mimetype = "text/html";
        else if(!stricmp(ext, ".g3f"))
                mimetype = "image/g3fax";
        if(!mem)
        {
                if(!stricmp(ext, ".al") || !stricmp(ext, ".gsm"))
                        mem = "snd";
                else
                        mem = "any";
        }

        if(!stricmp(mem, "snd"))
        {
                if(!permitAudioAccess(buffer, false))
                {
                        trunk->setSymbol(SYM_ERROR, "access-denied");
                        Service::failure();
                }
        }
        else
        {
                if(!permitFileAccess(buffer))
                {
                        trunk->setSymbol(SYM_ERROR, "access-denied");
                        Service::failure();
                }
        }

        if(stat(buffer, &ino))
        {
                trunk->setSymbol(SYM_ERROR, "cannot-stat");
                Service::failure();
        }

        if(!isFile(buffer))
        {
                trunk->setSymbol(SYM_ERROR, "not-file");
                Service::failure();
        }

        sym = trunk->getKeyword("type");
        if(sym && *sym == '&')
		trunk->setSymbol(sym, mimetype, 16);

        sym = trunk->getKeyword("size");
        if(sym && *sym == '&')
        {
                snprintf(msgbuf, sizeof(msgbuf), "%ld", ino.st_size);
		trunk->setSymbol(sym, msgbuf, 11);
        }

        if(stricmp(mem, "snd"))
                Service::success();

        au.open(buffer);
        if(!au.isOpen())
        {
                trunk->setSymbol(SYM_ERROR, "cannot-open");
                Service::failure();
        }

        au.setPosition();
        snprintf(msgbuf, sizeof(msgbuf), "%ld", au.getPosition());
        trunk->setSymbol(SYM_OFFSET, msgbuf);
        trunk->setSymbol(SYM_RECORDED, msgbuf);
        ann = au.getAnnotation();
        if(!ann)
                ann = "";
        trunk->setSymbol(SYM_ANNOTATION, ann);

        sym = trunk->getKeyword("offset");
        if(sym && *sym == '&')
        	trunk->setSymbol(sym, msgbuf, 11);

        sym = trunk->getKeyword("encoding");
        if(sym && *sym == '&')
                trunk->setSymbol(sym, Trunk::getEncodingName(au.getEncoding()), 8);

        sym = trunk->getKeyword("annotation");
        if(sym  && *sym == '&')
        	trunk->setSymbol(sym, ann, 0);

        sym = trunk->getKeyword("duration");
        if(sym && *sym == '&')
        {
		trunk->setSymbol(sym, "", 9);
                switch(au.getRate(au.getEncoding()))
                {
                default:
                case rateUnknown:
                        duration = -1l;
                        trunk->setSymbol(sym, "unknown", 9);
                        break;
                case rate6khz:
                        duration = au.getPosition() / 6000l;
                        break;
                case rate8khz:
                        duration = au.getPosition() / 8000l;
                        break;
                }
                if(duration > -1)
                {
                        hour = duration / 3600l;
                        duration %= 3600l;
                        min = duration / 60l;
                        duration %= 60l;
                        snprintf(msgbuf, sizeof(msgbuf), "%02d:%02d:%02d",
                                hour, min, duration);
                        trunk->setSymbol(sym, msgbuf, 9);
                }
        }
        Service::success();
}

class fsStat : private Module
{
private:
        modtype_t getType(void)
                {return MODULE_THREAD;};

        char *getName(void)
                {return "fileinfo";};

        unsigned sleep(Trunk *trunk)
                {return 1;};

        char *dispatch(Trunk *trunk);
public:
        fsStat();
} fsstat;

class fsCount : private Module
{
private:
	modtype_t getType(void)
		{return MODULE_THREAD;};

	char *getName(void)
		{return "filecount";};

	unsigned sleep(Trunk *trunk)
		{return 10;};

	char *dispatch(Trunk *trunk);
public:
	fsCount();
} fscount;

class fsScan : private Module
{
private:
        modtype_t getType(void)
                {return MODULE_THREAD;};

        char *getName(void)
                {return "filescan";};

        unsigned sleep(Trunk *trunk)
                {return 10;};

        char *dispatch(Trunk *trunk);
public:
        fsScan();
} fsscan;

fsScan::fsScan() :
Module()
{
        aascript->addModule(this);
}

char *fsScan::dispatch(Trunk *trunk)
{
        new ScanThread(trunk);
        return NULL;
}

fsCount::fsCount() :
Module()
{
	aascript->addModule(this);
}

char *fsCount::dispatch(Trunk *trunk)
{
	new CountThread(trunk);
	return NULL;
}

fsStat::fsStat() :
Module()
{
	aascript->addModule(this);
}

char *fsStat::dispatch(Trunk *trunk)
{
        char buffer[65];
        const char *prefix = trunk->getPrefixPath();
        const char *file = trunk->getKeyword("file");
        const char *cp = trunk->getKeyword("maxTime");
        const char *ext;

        if(!file)
                file = trunk->getValue(NULL);

        if(!file)
        	return "no-file";

        ext = strrchr(file, '/');
        if(!ext)
                ext = file;
        ext = strrchr(ext, '.');
        if(!ext)
                ext = trunk->getKeyword("extension");
        if(!ext)
                ext = trunk->getSymbol(SYM_EXTENSION);

        if(prefix)
                snprintf(buffer, sizeof(buffer), "%s/%s%s", prefix, file, ext);
        else
                snprintf(buffer, sizeof(buffer), "%s%s", file, ext);

        new StatThread(trunk, buffer);
	return NULL;
}

#ifdef	CCXX_NAMESPACES
};
#endif
