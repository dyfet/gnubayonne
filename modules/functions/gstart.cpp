// Copyright (C) 2000 Open Source Telecom Corporation.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <server.h>
#include <cc++/strchar.h>
#include <cc++/audio.h>

#ifdef	HAVE_SSTREAM
#include <sstream>
#else
#include <strstream>
#endif

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

static class gstart : private Module
{
private:
        modtype_t getType(void)
                {return MODULE_GENERIC;};

        char *getName(void)
                {return "gstart";};

        char *dispatch(Trunk *trunk);
public:
        gstart();
} gstart;

gstart::gstart() :
Module()
{
        aascript->addModule(this);
}

char *gstart::dispatch(Trunk *trunk)
{
	Script::Symbol *sym;
	char args[512];
	char content[512];
	char buffer[256];
	unsigned len = 0;
	unsigned alen = 0;
	unsigned limit = 1;
	long leastbusy, longestidle = 0, mostavail = 0, avail, val, cap;
	TrunkGroup *first = NULL, *longest = NULL, *busy = NULL, *most = NULL;
	const char *submit = trunk->getKeyword("submit");
	const char *script = trunk->getKeyword("script");
	const char *select = trunk->getKeyword("select");
	const char *var = trunk->getKeyword("var");
	const char *mem = trunk->getMember();
	const char *cp;
	char *tok, *gp;
	TrunkGroup *grp;
	Trunk *dest;

	cp = trunk->getKeyword("limit");
	if(cp)
		limit = atoi(cp);

	// initialize select

	if(select)
		trunk->setSymbol(select, "", 16);

	// initialize var

	if(var)
		trunk->setSymbol(var, "", 16);

	// we collect a list of trunk group names passed as direct
	// command line arguments

	while(NULL != (cp = trunk->getValue(NULL)))
	{
		if(len)
			snprintf(buffer + len, sizeof(buffer) - len, ",%s", cp);
		else
			snprintf(buffer, sizeof(buffer), "%s", cp);
		len = strlen(buffer);
	}
	buffer[len] = 0;	// make sure always terminated

	// we examine a list of trunk groups passed to the command as
	// arguments, and try each one in turn, looking for which is
	// least busy, longest idle, and most available.  
	
	gp = strtok_r(buffer, ",;:", &tok);
	while(gp)
	{
		grp = getGroup(gp);
		if(grp)
		{
			avail = grp->getStat(STAT_AVAIL_CALLS);
			cap = grp->getCapacity();
			if(val < limit)
				grp = NULL;
		}
		
		if(!grp)
			goto next;

		if(!first)
			first = grp;

		// must have something avail to qualify

		if(!cap || !avail)
			goto longest;

		val = (avail * 1000) / cap;
		if(!busy || val > leastbusy)
		{
			busy = grp;
			leastbusy = val;
		}

longest:	// must be idle to qualify

		val = grp->getIdleTime();
		if(val > longestidle)
		{
			longest = grp;
			longestidle = val;
		}

		// must have ports avail to even qualify, and then
		// must be most

		if(avail > mostavail)
		{
			most = grp;
			mostavail = val;
		}

next:
		gp = strtok_r(NULL, ",;:", &tok);
	}	

	// make sure gstart.xxx is valid or has default

	if(!mem)
		mem = "first";

	// the gstart.xxx member determines which of the search results
	// we will use for this command.

	grp = first;
	if(!strnicmp(mem, "most", 4))		// most avail
		grp = most;
	else if(!strnicmp(mem, "least", 5))	// least busy
		grp = busy;
	else if(!strnicmp(mem, "long", 4))	// longest idle 
		grp = longest;

        // if we find no groups with available ports within limit
        // then we try an event or report error indicating all groups
        // are busy.

        if(!grp)
        {
                if(trunk->trunkEvent("start:busy"))
			return ((char *)(-1));
		
                return "gstart-no-groups-available";
        }

	// if we asked to save the selection, we will do so now

	if(select)
		trunk->setSymbol(select, grp->getName(), 16);

	// prep args for fifo mode direct start

	snprintf(args + alen, sizeof(args) - alen, "start %s %s=%s",
		grp->getName(), SYM_PARENT, trunk->getSymbol(SYM_GID));
	alen = strlen(args);

	// now parse any submitted args to be passed to child script

	if(submit)
        {
                snprintf(buffer, 255, "%s", submit);
                submit = strtok_r(buffer, ",", &tok);
        }

        while(submit && alen < sizeof(args))
        {
                sym = trunk->mapSymbol(submit);
                submit = strtok_r(NULL, ",", &tok);
                if(!sym)
                        continue;

                urlEncode(sym->data, content, sizeof(content));
                snprintf(args + alen, sizeof(args) - alen, " %s=%s", sym->id, content);
                alen = strlen(args);
        }

	// now we attempt to start the script, maybe we still can fail...

	trunk->trunkSignal(TRUNK_SIGNAL_STEP);	// child start may invoke start:xxx
	dest = fifo.command(args);
	if(!dest)
	{
		cp = dest->getSymbol(SYM_GID);
		if(cp)
			cp = strchr(cp, '-');
		if(cp && var)
			trunk->setSymbol(var, cp, 16);
		if(trunk->trunkEvent("start:busy"))
			return ((char *)(-1));
		return "gstart-ports-busy";
	}
	return ((char *)(-1));
}

#ifdef	CCXX_NAMESPACES
};
#endif
