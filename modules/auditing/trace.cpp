// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "bayonne.h"
#include <iomanip>

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

static RETSIGTYPE sigfault(int signo)
{
        Debug::stackTrace(signo);
        exit(signo);
}

class TraceDebug : public Debug
{
public:
	TraceDebug();
	bool debugTest(void);
	bool debugFinal(int signo);
	void debugState(Trunk *trunk, char *state);
	void debugScript(Trunk *trunk, char *msg);
	void debugStep(Trunk *trunk, Line *line);
} trace;

TraceDebug::TraceDebug() :
Debug()
{
	slog(Slog::levelInfo) << "debug: trace module loaded" << endl;
}

bool TraceDebug::debugTest(void)
{
	signal(SIGSEGV, sigfault);
        signal(SIGABRT, sigfault);

	return false;
}

bool TraceDebug::debugFinal(int signo)
{
	Debug::stackTrace(signo);
        return false;
}

void TraceDebug::debugScript(Trunk *trunk, char *msg)
{
	char buffer[32];

	enterMutex();
	trunk->getName(buffer);
	slog(Slog::levelDebug) << buffer << ": " << msg << endl;
	leaveMutex();
}

void TraceDebug::debugStep(Trunk *trunk, Line *line)
{
	int i;

	char buffer[32];
	trunk->getName(buffer);

	if(!line)
	{
		slog(Slog::levelDebug) << buffer << ": exit" << endl;
		return;
	}

	enterMutex();
	slog(Slog::levelDebug) << buffer << ": step ";
	slog() << setbase(16) << line->mask << " " << setbase(10);
	slog() << line->cmd << "(";
	for(i = 0; i < line->argc; ++i)
	{
		if(i)
			slog() << ",";
		slog() << line->args[i];
	}
	slog() << ")" << endl;	
	leaveMutex();
}

void TraceDebug::debugState(Trunk *trunk, char *state)
{
	char buffer[32];
	trunk->getName(buffer);

	enterMutex();
	slog(Slog::levelDebug) << buffer << ": " << state << endl;
	leaveMutex();
}

#ifdef	CCXX_NAMESPACES
};
#endif
