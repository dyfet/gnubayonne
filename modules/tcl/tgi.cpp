// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#undef	PACKAGE
#undef	VERSION

extern "C" {
#include <tcl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <signal.h>

static char tgi_token = '&';
static int tgi_port;
static Tcl_AsyncHandler tcl_sighup;
static Tcl_AsyncHandler tcl_timeout;

static void sighup(int signo)
{
	Tcl_AsyncMark(tcl_sighup);
}

static void sigalrm(int signo)
{
	Tcl_AsyncMark(tcl_timeout);
}

static int tgi_signals(ClientData cd, Tcl_Interp *tcl, int code)
{
	int err = TCL_ERROR;
	if(tcl)
	{
		switch((int)cd)
		{
		case SIGHUP:
			err = Tcl_Eval(tcl, "hangup");
			break;
		case SIGALRM:
			err = Tcl_Eval(tcl, "timeout");
			break;
		}
		if(err == TCL_ERROR)
			Tcl_Exit(-1);
	}
	else
		tcl->result = "ok";
	return TCL_OK;
}


static int tgi_down(ClientData cd, Tcl_Interp *tcl, int argc, char **argv)
{
	if(argc > 1)
	{
		Tcl_SetResult(tcl, "no args expected", NULL);
		return TCL_ERROR;
	}
	printf("DOWN\n");
	Tcl_SetResult(tcl, argv[0], TCL_STATIC);
	return TCL_OK;
}

static int tgi_compile(ClientData cd, Tcl_Interp *tcl, int argc, char **argv)
{
	if(argc > 1)
	{
		Tcl_SetResult(tcl, "no args expected", NULL);
		return TCL_ERROR;
	}
	printf("COMPILE\n");
	Tcl_SetResult(tcl, argv[0], TCL_STATIC);
	return TCL_OK;
}
static int tgi_idle(ClientData cd, Tcl_Interp *tcl, int argc, char **argv)
{
	if(argc > 1)
	{
		Tcl_SetResult(tcl, "no args expected", NULL);
		return TCL_ERROR;
	}
	printf("IDLE%c%d\n", tgi_token, tgi_port); 
	Tcl_SetResult(tcl, argv[0], TCL_STATIC);
	return TCL_OK;
}

static int tgi_busy(ClientData cd, Tcl_Interp *tcl, int argc, char **argv)
{
	if(argc > 1)
	{
		Tcl_SetResult(tcl, "no args expected", NULL);
		return TCL_ERROR;
	}
	printf("BUSY%c%d\n", tgi_token, tgi_port);
	Tcl_SetResult(tcl, argv[0], TCL_STATIC);
	return TCL_OK;
}

static int tgi_export(ClientData cd, Tcl_Interp *tcl, int argc, char **argv)
{
	char *var;
	int idx = 0;

	if(argc < 1)
	{
		Tcl_SetResult(tcl, "TGI invalid option", NULL);
		return TCL_ERROR;
	}
	while(idx++ < argc)
	{
		var = Tcl_GetVar(tcl, argv[idx], 0);
		if(!var)
			var="";
		printf("SET%c%d%c%s%c%s\n", tgi_token, tgi_port, 
			tgi_token, argv[idx], tgi_token, var);
	}
	Tcl_SetResult(tcl, argv[0], TCL_STATIC);
	return TCL_OK;
}

static int tgi_put(ClientData cd, Tcl_Interp *tcl, int argc, char **argv)
{
	char *var;
	int idx = 0;

	if(argc < 1)
	{
		Tcl_SetResult(tcl, "TGI invalid option", NULL);
		return TCL_ERROR;
	}
	while(idx++ < argc)
	{
		var = Tcl_GetVar(tcl, argv[idx], 0);
		if(!var)
			var="";
		printf("PUT%c%d%c%s%c%s\n", tgi_token, tgi_port, 
			tgi_token, argv[idx], tgi_token, var);
	}
	Tcl_SetResult(tcl, argv[0], TCL_STATIC);
	return TCL_OK;
}


static int tgi_post(ClientData cd, Tcl_Interp *tcl, int argc, char **argv)
{
	char *var;
	int idx = 0;

	if(argc < 1)
	{
		Tcl_SetResult(tcl, "TGI invalid option", NULL);
		return TCL_ERROR;
	}
	while(idx++ < argc)
	{
		var = Tcl_GetVar(tcl, argv[idx], 0);
		if(!var)
			var="";
		printf("POST%c%d%c%s%c%s\n", tgi_token, tgi_port, 
			tgi_token, argv[idx], tgi_token, var);
	}
	Tcl_SetResult(tcl, argv[0], TCL_STATIC);
	return TCL_OK;
}


int TGI_Init(Tcl_Interp *interp)
{
	char *tok, *cp, *val;
	char temp[256];

	if(Tcl_PkgRequire(interp, "tcl", "7.5", 0) == NULL)
		return TCL_ERROR;

	Tcl_CreateCommand(interp, "export", tgi_export, (ClientData)NULL, 
		(Tcl_CmdDeleteProc *)NULL);
	Tcl_CreateCommand(interp, "put", tgi_put, (ClientData)NULL,
		(Tcl_CmdDeleteProc *)NULL);
	Tcl_CreateCommand(interp, "post", tgi_post, (ClientData)NULL,
		(Tcl_CmdDeleteProc *)NULL);
	
	Tcl_CreateCommand(interp, "down", tgi_export, (ClientData)NULL, 
		(Tcl_CmdDeleteProc *)NULL);
	Tcl_CreateCommand(interp, "compile", tgi_put, (ClientData)NULL,
		(Tcl_CmdDeleteProc *)NULL);
	Tcl_CreateCommand(interp, "idle", tgi_post, (ClientData)NULL,
		(Tcl_CmdDeleteProc *)NULL);
	Tcl_CreateCommand(interp, "busy", tgi_export, (ClientData)NULL, 
		(Tcl_CmdDeleteProc *)NULL);

	tcl_sighup = Tcl_AsyncCreate(tgi_signals, (ClientData)SIGHUP);
	tcl_timeout = Tcl_AsyncCreate(tgi_signals, (ClientData)SIGALRM);


	if(Tcl_PkgProvide(interp, "TGI", "0.1") == TCL_ERROR)
		return TCL_ERROR;

	tok = getenv("SERVER_TOKEN");
	tgi_token = *tok;

	cp = getenv("PORT_NUMBER");
	tgi_port = atoi(cp);
	

	Tcl_SetVar(interp, "Server_Version", getenv("SERVER_VERSION"), 0);
	Tcl_SetVar(interp, "Server_Control", getenv("SERVER_CONTROL"), 0);
	Tcl_SetVar(interp, "Server_Token", getenv("SERVER_TOKEN"), 0);
	Tcl_SetVar(interp, "Server_Protocol", getenv("SERVER_PROTOCOL"), 0);
	Tcl_SetVar(interp, "Server_Node", getenv("SERVER_NODE"), 0);
	Tcl_SetVar(interp, "Port_Number", getenv("PORT_NUMBER"), 0);
	Tcl_SetVar(interp, "Port_Clid", getenv("PORT_CLID"), 0);
	Tcl_SetVar(interp, "Port_Dnid", getenv("PORT_DNID"), 0);
	Tcl_SetVar(interp, "Port_Digits", getenv("PORT_DIGITS"), 0);
	Tcl_SetVar(interp, "Temp_Audio", getenv("TEMP_AUDIO"), 0);

	cp = getenv("SERVER_RUNTIME");
	if(cp)
		sprintf(temp, "%s/temp", tok, 0);
	else
		sprintf(temp, "/tmp");
	Tcl_SetVar(interp, "Temp_Files", temp, 0);

	cp = getenv("PORT_QUERY");
	cp = strtok(cp, tok);
	while(cp)
	{
		val = strchr(cp, '=');
		if(val)
		{
			*(val++) = 0;
			if(islower(*cp))
				*cp = toupper(*cp);
			sprintf(temp, "Query_%s", cp);
			Tcl_SetVar(interp, temp, val, 0);
		}
		cp = strtok(NULL, tok);
	}
 
	signal(SIGHUP, sighup);
	signal(SIGALRM, sigalrm);
	return TCL_OK;
} 
	
}
