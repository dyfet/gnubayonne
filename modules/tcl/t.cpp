int TGI_Init(Tcl_Interp *interp)
{
	if(Tcl_PkgRequire(interp, "tcl", "7.5", 0) == NULL)
		return TCL_ERROR;

	Tcl_CreateCommand(interp, "export", tgi_export, (ClientData)NULL, (Tcl_CmdDeleteProc *)NULL);
	
	if(Tcl_PkgProvide(interp, "TGI", "0.1") == TCL_ERROR)
		return TCL_ERROR;

	return TCL_OK;
} 
