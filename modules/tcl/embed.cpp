// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#undef	PACKAGE
#undef	VERSION

extern "C" {
#include <tcl.h>
#include <unistd.h>

int TGI_Init(Tcl_Interp *interp);

int Tcl_AppInit(Tcl_Interp *interp)
{
	int status;
	status = Tcl_Init(interp);
	if(status != TCL_OK)
		return TCL_ERROR;

	status = TGI_Init(interp);
	if(status != TCL_OK)
		return TCL_ERROR;

	return TCL_OK;
}

void execinterp(char *cmd)
{
	static char *argv[3];
	int argc = 3;
	FILE	*fp;
	
	argv[0] = "-";
	argv[1] = cmd;
	argv[2] = NULL;

	return Tcl_Main(argc, argv, Tcl_AppInit);
}
	
}
