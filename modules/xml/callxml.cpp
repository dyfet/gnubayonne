// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <server.h>

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

class CallXMLTrunk : public TrunkImage
{
private:
	friend class CallXMLModule;

	void characters(const unsigned char *text, unsigned len);
	void startElement(const unsigned char *name, const unsigned char **attrib);
	void endElement(const unsigned char *name);
	bool loader(Trunk *trk, trunkdata_t *data);

	CallXMLTrunk();
	~CallXMLTrunk();
};

class CallXMLModule : private Module
{
private:
	modtype_t getType(void)
		{return MODULE_XML;};

	char *getName(void)
		{return "callxml";};

	TrunkImage *getXML(void)
		{return (TrunkImage *)new CallXMLTrunk;};

public:
	CallXMLModule();
} callxml;


CallXMLModule::CallXMLModule() : Module()
{
	slog(Slog::levelDebug) << "XML: Loading <CallXML> interpreter" << endl;
}

CallXMLTrunk::CallXMLTrunk() : TrunkImage()
{
	slog(Slog::levelDebug) << "XML: Creating <CallXML> instance" << endl;
}

CallXMLTrunk::~CallXMLTrunk()
{
	purge();
	slog(Slog::levelDebug) << "XML: Destroying <CallXML> instance" << endl;
}

void CallXMLTrunk::characters(const unsigned char *text, unsigned len)
{
}

void CallXMLTrunk::startElement(const unsigned char *name, const unsigned char **attrib)
{
}

void CallXMLTrunk::endElement(const unsigned char *name)
{
}

bool CallXMLTrunk::loader(Trunk *trk, trunkdata_t *data)
{
	slog(Slog::levelDebug) << "XML: Loading " << data->load.url;
	slog() << " for " << data->load.timeout / 1000 << " seconds" << endl;
	return false;
}

#ifdef	CCXX_NAMESPACES
};
#endif
