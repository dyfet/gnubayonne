// Copyright (C) 2000 Open Source Telecom Corporation.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <server.h>
#include <sys/stat.h>

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

#define	MAX_MAILBOX	256
#define	DAY	(3600l * 24l)

class MsgboxTrunk : public TrunkImage
{
private:
	friend class MsgboxModule;
	Dir *dir;

	void characters(const unsigned char *text, unsigned len) {};
	void startElement(const unsigned char *name, const unsigned char **attrib) {};
	void endElement(const unsigned char *name) {};
	bool loader(Trunk *trk, trunkdata_t *data);
	char *mystr(const char *temp);

	static int forward(const void *x, const void *y)
	{
		char **s1 = (char **)x;
		char **s2 = (char **)y;
		Thread::yield();
		return stricmp(*s1, *s2);
	};

        static int reverse(const void *x, const void *y)
        {
                char **s2 = (char **)x;
                char **s1 = (char **)y;
		Thread::yield();
                return stricmp(*s1, *s2);
        };


	MsgboxTrunk();
	~MsgboxTrunk();
};

class MsgboxModule : private Module, private Sync
{
private:
	modtype_t getType(void)
		{return MODULE_SQL;};

	char *getName(void)
		{return "msgs";};

	TrunkImage *getXML(void)
		{return (TrunkImage *)new MsgboxTrunk;};

	unsigned getInterval(void)
		{return 60;};

	char *getSyncName(void)
		{return "msgbox";};

	void schedule(void);

	void expire(char *path, size_t size);

	char *dispatch(Trunk *trunk);

public:
	MsgboxModule();
} msgboxmod;

class MsgRecordModule : private Module
{
private:
	modtype_t getType(void)
		{return MODULE_RECORD;};

	char *getName(void)
		{return "msgrecord";};

	char *dispatch(Trunk *trunk);

public:
	MsgRecordModule();
} msgrecord;

class MsgPlayModule : private Module
{
private:
        modtype_t getType(void)
                {return MODULE_PLAY;};

        char *getName(void)
                {return "msgplay";};

        char *dispatch(Trunk *trunk);

public:
        MsgPlayModule();
} msgplay;

class MsgSavedModule : private Module
{
private:
        modtype_t getType(void)
                {return MODULE_RECORD;};

        char *getName(void)
                {return "msgsaved";};

        char *dispatch(Trunk *trunk);

public:
        MsgSavedModule();
} msgsaved;

class MsgPlayedModule : private Module
{
private:
        modtype_t getType(void)
                {return MODULE_GENERIC;};

        char *getName(void)
                {return "msgplayed";};

        char *dispatch(Trunk *trunk);

public:
        MsgPlayedModule();
} msgplayed;

class MsgMoveModule : private Module
{
private:
        modtype_t getType(void)
                {return MODULE_GENERIC;};

        char *getName(void)
                {return "msgmove";};

        char *dispatch(Trunk *trunk);

public:
        MsgMoveModule();
} msgmove;

class MsgCopyModule : private Module	// by hard link!
{
private:
        modtype_t getType(void)
                {return MODULE_GENERIC;};

        char *getName(void)
                {return "msgcopy";};

        char *dispatch(Trunk *trunk);

public:
        MsgCopyModule();
} msgcopy;

class MsgEraseModule : private Module
{
private:
        modtype_t getType(void)
                {return MODULE_GENERIC;};

        char *getName(void)
                {return "msgerase";};

        char *dispatch(Trunk *trunk);

public:
        MsgEraseModule();
} msgerase;

static const char *getLogin(Trunk *trunk)
{
	const char *login = trunk->getSymbol(SYM_LOGIN);
	const char *id;

	if(!login)
		return NULL;

	if(!stricmp(login, "none"))
		return NULL;

	if(stricmp(login, "admin"))
		return login;

	id = trunk->getKeyword("id");
	if(!id)
		id = trunk->getKeyword("box");

	if(id)
		return id;

	return "admin";
}

static bool getBox(Trunk *trunk, char *prefix, unsigned len)
{
	const char *id = getLogin(trunk);

	if(!id)
		return false;

	snprintf(prefix, len, "msgs/%s", id);
	if(!isDir(prefix))
		return false;

	return true;
}

static bool getTarget(Trunk *trunk, char *prefix, unsigned len, const char *msg)
{
	const char *id = trunk->getKeyword("to");
	const char *cp = strrchr(msg, '/');

	if(cp)
		cp += 3;
	else
		cp = msg;

	if(!id)
		return false;

	snprintf(prefix, len, "msgs/%s", id);
	if(!isDir(prefix))
		return false;

	snprintf(prefix, len, "msgs/%s/n-%s", id, cp);
	return true;
}

static bool getMsg(Trunk *trunk, char *prefix, unsigned max)
{
	unsigned len = strlen(prefix);
	const char *msg = trunk->getKeyword("msg");

	if(!msg)
		msg = trunk->getValue(NULL);

	if(!msg)
		return false;

	snprintf(prefix + len, max - len, "/n-%s", msg);
	if(canAccess(prefix))
		return true;

	snprintf(prefix + len, max - len, "/o-%s", msg);
	if(canAccess(prefix))
		return true;

	snprintf(prefix + len, max - len, "/s-%s", msg);
	if(canAccess(prefix))
		return true;

	prefix[len] = 0;
	return false;
}

MsgRecordModule::MsgRecordModule() : Module()
{
	aascript->addModule(this);
}

MsgPlayModule::MsgPlayModule() : Module()
{
       	aascript->addModule(this);
}

MsgSavedModule::MsgSavedModule() : Module()
{
       	aascript->addModule(this);
}

MsgPlayedModule::MsgPlayedModule() : Module()
{
       	aascript->addModule(this);
}

MsgMoveModule::MsgMoveModule() : Module()
{
       	aascript->addModule(this);
}

MsgCopyModule::MsgCopyModule() : Module()
{
       	aascript->addModule(this);
}

MsgEraseModule::MsgEraseModule() : Module()
{
	aascript->addModule(this);
}

char *MsgMoveModule::dispatch(Trunk *trunk)
{
	char prefix[128], newpath[128];

	if(!getBox(trunk, prefix, 64))
		return "no-mailbox";

	if(!getMsg(trunk, prefix, 128))
		return "no-message";

	if(!getTarget(trunk, newpath, 128, prefix))
		return "no-target";

	rename(prefix, newpath);
}

char *MsgCopyModule::dispatch(Trunk *trunk)
{
	char prefix[128], newpath[128];

	if(!getBox(trunk, prefix, 64))
		return "no-mailbox";

	if(!getMsg(trunk, prefix, 128))
		return "no-message";

	if(!getTarget(trunk, newpath, 128, prefix))
		return "no-target";

	link(prefix, newpath);
}

char *MsgPlayedModule::dispatch(Trunk *trunk)
{
	char prefix[128], newpath[128];
	unsigned char len;

	if(!getBox(trunk, prefix, 64))
		return "no-mailbox";

	len = strlen(prefix) + 1;

	if(!getMsg(trunk, prefix, 128))
		return "no-message";

	if(prefix[len] != 'n')
		return NULL;

	strcpy(newpath, prefix);
	newpath[len] = 'o';
	rename(prefix, newpath);
	return NULL;
}

char *MsgSavedModule::dispatch(Trunk *trunk)
{
	char prefix[128], newpath[128];
	unsigned char len;

	if(!getBox(trunk, prefix, 64))
		return "no-mailbox";

	len = strlen(prefix) + 1;

	if(!getMsg(trunk, prefix, 128))
		return "no-message";

	if(prefix[len] == 's')
		return NULL;

	strcpy(newpath, prefix);
	newpath[len] = 's';
	rename(prefix, newpath);
	return NULL;
}

char *MsgEraseModule::dispatch(Trunk *trunk)
{
	char prefix[128];

	if(!getBox(trunk, prefix, 64))
		return "no-mailbox";

	if(!getMsg(trunk, prefix, 128))
		return "no-message";

	remove(prefix);
	return NULL;
}

char *MsgPlayModule::dispatch(Trunk *trunk)
{
	trunkdata_t *data = getData(trunk);
	const char *gain = trunk->getKeyword("gain");
	const char *vol = trunk->getKeyword("volume");
	const char *cp;

	if(!vol)
		vol = trunk->getSymbol(SYM_VOLUME);

	if(!vol)
		vol = "100";

	if(!getBox(trunk, data->play.list, 64))
		return "no-mailbox";

	if(!getMsg(trunk, data->play.list, 128))
		return "no-message";

	data->play.mode = PLAY_MODE_ONE;
	data->play.text = trunk->getKeyword("text");
	data->play.voice = trunk->getSymbol(SYM_VOICE);
	data->play.name = data->play.list;
	data->play.volume = atoi(vol);
	data->play.timeout = 0;
	data->play.repeat = 0;
	data->play.maxtime = 0;
	data->play.term = 0;
	data->play.lock = false;
	data->play.extension = ".au";
	
	if(gain)
		data->play.gain = (float)strtod(gain, NULL);
	else
		data->play.gain = 0.0;
	data->play.pitch = 0.0;
	data->play.speed = SPEED_NORMAL;
	data->play.limit = data->play.offset = 0;

	if(NULL != (cp = trunk->getKeyword("offset")))
		data->play.offset = atol(cp);

	if(NULL != (cp = trunk->getKeyword("limit")))
		data->play.limit = atol(cp);

	return NULL;
}

char *MsgRecordModule::dispatch(Trunk *trunk)
{
	trunkdata_t *data = getData(trunk);
	const char *var = trunk->getKeyword("var");
	const char *id = trunk->getKeyword("id");
	const char *ref = trunk->getKeyword("ref");
	char *cp;
	const char *ctype, *caller, *cname;
	time_t now;
	struct tm *dt, tbuf;

	trunk->setSymbol(SYM_RECORDED, "0");
	trunk->setSymbol(SYM_OFFSET, "0");

	if(!id)
		id = trunk->getKeyword("box");

	if(!id)
		id = trunk->getKeyword("to");

	if(!id)
		return "no-msgbox";

	snprintf(data->record.filepath, 65, "msgs/%s", id);
	if(!isDir(data->record.filepath))
		return "invalid-msgbox";

	time(&now);
	dt = localtime_r(&now, &tbuf);
	snprintf(data->record.filepath, 65, "msgs/%s/t-%ld-%03d.au", 
		id, now, trunk->getSymbol(SYM_ID));
	snprintf(data->record.savepath, 65, "msgs/%s/n-%ld-%03d.au",
		id, now, trunk->getSymbol(SYM_ID));
	data->record.name = data->record.filepath;
	data->record.save = data->record.savepath;

	cp = trunk->getKeyword("trim");
	if(!cp)
		cp = trunk->getSymbol(SYM_TRIM);

	if(!cp)
		cp = 0;

	data->record.trim = atoi(cp);
	data->record.frames = 0;
	
	cp = trunk->getKeyword("minSize");
	if(!cp)
		cp = "0";

	data->record.minsize = atoi(cp);
	data->record.text = trunk->getKeyword("text");
	data->record.term = trunk->getDigitMask("exit");
	data->record.offset = (unsigned long)-1;
	data->record.volume = 100;
	data->record.silence = 0;
	data->record.encoding = trunk->getKeyword("encoding");
	data->record.extension = ".au";
	data->record.gain = 0.0;
	data->record.info = false;
	data->record.append = false;

	if(trunk->getCapabilities() & TRUNK_CAP_STATION)
	{
		ctype = "I";
		caller = trunk->getKeyword(SYM_EXTENSION);
		cname = "";
	}
	else
	{
		ctype = "E";
		caller = trunk->getKeyword(SYM_CLID);
		cname = trunk->getKeyword(SYM_NAME);
		if(!caller)
		{
			caller = "UNKNOWN";
			ctype = "U";
		}
		if(!strnicmp(caller, "priv", 4))
			ctype = "P";
		else if(!isdigit(*caller))
			ctype = "U"; 
	}

	if(!caller)
	{
		caller = "UNKNOWN";
		ctype = "U";
	}

	if(!*caller)
	{
		caller = "UNKNOWN";
		ctype = "U";
	}

	if(!cname)
		cname = "";

	if(!ref)
		ref = "";

	snprintf(data->record.altinfo, 128, "%c:%s:%s:%04d%02d%02d:%02d%02d%02d:%s", 
		toupper(*ctype), caller, cname,
		dt->tm_year + 1900, dt->tm_mon + 1, dt->tm_mday,
		dt->tm_hour, dt->tm_min, dt->tm_sec, ref);

	if(var)
	{
		if(*var == '&')
			++var;
		trunk->setSymbol(var, 80);
		trunk->setSymbol(var, data->record.altinfo);
	}

	data->record.annotation = data->record.altinfo;
	return NULL;
}	

MsgboxModule::MsgboxModule() : Module(), Sync()
{
	mkdir("msgs", 0770);
	slog(Slog::levelDebug) << "load: message box module" << endl;
	aascript->addModule(this);
}

void MsgboxModule::expire(char *path, size_t size)
{
	Dir dir(path);
	const char *name;
	unsigned len = strlen(path);
	time_t now, ts;
	const char *cp;

	path[len++] = '/';
	size -= len;
	time(&now);
	while(NULL != (name = dir.getName()))
	{
		if(*name == '.')
			continue;

		snprintf(path + len, size, "%s", name);

		ts = atol(name + 2);
		switch(*name)
		{
		case 't':
			if(now - ts >= (3600l))
			{
				slog(Slog::levelInfo) << "msgbox: removing " << path << endl;
				remove(path);
			}
			break;
		case 'n':
			if(now - ts >= (DAY * 7l))
			{
				slog(Slog::levelInfo) << "msgbox: expired " << path << endl;
				remove(path);
			}
			break;			
		case 'o':
			if(now - ts >= (DAY * 30l))
			{
				slog(Slog::levelInfo) << "msgbox: purging " << path << endl;
				remove(path);
			}
		}					
	}
}
	

void MsgboxModule::schedule(void)
{
	const char *name;
	char path[256];
	Dir dir("msgs");

	while(NULL != (name = dir.getName()))
	{
		if(*name == '.')
			continue;

		snprintf(path, sizeof(path), "msgs/%s", name);
		if(!isDir(path))
			continue;

		expire(path, sizeof(path));
	}
}

char *MsgboxModule::dispatch(Trunk *trunk)
{
       	trunkdata_t *data = getData(trunk);
        const char *key;

        key = trunk->getKeyword("maxTime");
        if(!key)
                key = "60s";

	if(!getBox(trunk, data->load.filepath, 65))
		return "no-mailbox";
	
        data->load.attach = false;
        data->load.post = false;
	data->load.fail = NULL;
        data->load.section = "";
        key = trunk->getKeyword("maxTime");
        if(!key)
                key = "60s";
        data->load.timeout = getSecTimeout(key);
        data->load.parent = NULL;
        data->load.gosub = false;
        data->load.url = data->load.filepath;
        data->load.vars = NULL;
        data->load.userid[0] = 0;
        return NULL;
}

MsgboxTrunk::MsgboxTrunk() : TrunkImage()
{
	dir = NULL;
}

MsgboxTrunk::~MsgboxTrunk()
{
	if(dir)
		delete dir;
	purge();
}

bool MsgboxTrunk::loader(Trunk *trk, trunkdata_t *data)
{
	const char *mem = trk->getMember();
	const char *ext = trk->getKeyword("extension");
	const char *oldvar = trk->getKeyword("old");
	const char *newvar = trk->getKeyword("new");
	const char *savevar = trk->getKeyword("saved");
	AudioFile au;
	dir = new Dir(data->load.url);
	char buf[5];
	char sbuf[12];
	const char *old_names[MAX_MAILBOX][4], *old_index[MAX_MAILBOX];
	const char *new_names[MAX_MAILBOX][4], *new_index[MAX_MAILBOX];
	const char *save_names[MAX_MAILBOX][4], *save_index[MAX_MAILBOX];
	unsigned old_count = 0, new_count = 0, save_count = 0, trash_count = 0;
	const char **argv;
	const char *name, *cp;
	static const char *eof[] = {"", "0", "", NULL};
	unsigned count = 0, limit = 0;
	unsigned long totsize = 0;
	char path[256];
	const char *counts[5];

	if(!mem)
		mem = "none";

	if(!ext)
		ext = trk->getSymbol(SYM_EXTENSION);

	while(NULL != (name = dir->getName()))
	{
		Thread::yield();
		cp = strrchr(name, '.');
		if(!cp)
			continue;

		if(stricmp(cp, ext))
			continue;

		switch(*name)
		{
		case 'o':
			if(old_count < MAX_MAILBOX)
				old_index[old_count++] = mystr(name);
			break;
		case 'n':
			if(new_count < MAX_MAILBOX)
				new_index[new_count++] = mystr(name);
			break;
		case 's':
			if(save_count < MAX_MAILBOX)
				save_index[save_count++] = mystr(name);
			break;
		case 't':
			++trash_count;
			break;
		}			
	}
	if(*mem == 'f' || *mem == 'F')
	{
		qsort((char *)old_index, old_count, sizeof(char *), &forward);
                qsort((char *)new_index, new_count, sizeof(char *), &forward);
               	qsort((char *)save_index, save_count, sizeof(char *), &forward);
	}
	else if(*mem == 'r' || *mem == 'R')
	{
                qsort((char *)old_index, old_count, sizeof(char *), &reverse);
                qsort((char *)new_index, new_count, sizeof(char *), &reverse);
		qsort((char *)save_index, save_count, sizeof(char *), &reverse);
	}
	getCompile("#msgs");
	snprintf(buf, sizeof(buf), "%d", old_count);
	counts[0] = mystr(buf);	
	snprintf(buf, sizeof(buf), "%d", new_count);
	counts[1] = mystr(buf);
	snprintf(buf, sizeof(buf), "%d", save_count);
	counts[2] = mystr(buf);
	snprintf(buf, sizeof(buf), "%d", trash_count);
	counts[3] = mystr(buf);
	counts[4] = NULL;
	addCompile(0, "data", counts);
	putCompile(main);

	getCompile("#old");
	count = 0;
	while(count < old_count)
	{
		name = old_index[count];
                snprintf(path, sizeof(path), "%s/%s", data->load.url, name);
		au.open(path);
		au.setPosition();
		totsize += au.getPosition();
		snprintf(path, sizeof(path), "%ld", au.getPosition());
		old_names[count][0] = name + 2;
		old_names[count][1] = mystr(path);
		old_names[count][2] = mystr(au.getAnnotation());
		au.close();
		argv = (const char **)&old_names[count++];
		argv[3] = NULL;
		addCompile(0, "data", argv);
	}
	addCompile(0, "data", eof);
	addCompile(0, "return.exit", NULL);
	putCompile(current);	

	getCompile("#new");
        count = 0;
        while(count < new_count)
        {
                name = new_index[count];
                snprintf(path, sizeof(path), "%s/%s", data->load.url, name);
                au.open(path);
                au.setPosition();
                totsize += au.getPosition();
                snprintf(path, sizeof(path), "%ld", au.getPosition());
                new_names[count][0] = name + 2;
                new_names[count][1] = mystr(path);
                new_names[count][2] = mystr(au.getAnnotation());
                au.close();
                argv = (const char **)&new_names[count++];
                argv[3] = NULL;
                addCompile(0, "data", argv);
        }
        addCompile(0, "data", eof);
        addCompile(0, "return.exit", NULL);
        putCompile(current);

        getCompile("#saved");
        count = 0;
        while(count < save_count)
        {
                name = save_index[count];
                snprintf(path, sizeof(path), "%s/%s", data->load.url, name);
                au.open(path);
                au.setPosition();
                totsize += au.getPosition();
                snprintf(path, sizeof(path), "%ld", au.getPosition());
                save_names[count][0] = name + 2;
                save_names[count][1] = mystr(path);
                save_names[count][2] = mystr(au.getAnnotation());
                au.close();
                argv = (const char **)&save_names[count++];
                argv[3] = NULL;
                addCompile(0, "data", argv);
        }
        addCompile(0, "data", eof);
        addCompile(0, "return.exit", NULL);
        putCompile(current);

	trk->setData("#msgs");
	if(oldvar)
		if(*oldvar == '&')
			++oldvar;

	if(!oldvar)
		return false;

	snprintf(buf, sizeof(buf), "%d", old_count);
	trk->setSymbol(oldvar, sizeof(buf) - 1);
	trk->setSymbol(oldvar, buf);

        if(newvar)
                if(*newvar == '&')
                        ++newvar;

        if(!newvar)
                return false;

        snprintf(buf, sizeof(buf), "%d", new_count);
        trk->setSymbol(newvar, sizeof(buf) - 1);
        trk->setSymbol(newvar, buf);

        if(savevar)
                if(*savevar == '&')
                        ++savevar;

        if(!savevar)
                return false;

        snprintf(buf, sizeof(buf), "%d", save_count);
        trk->setSymbol(savevar, sizeof(buf) - 1);
        trk->setSymbol(savevar, buf);

	return true;
}

char *MsgboxTrunk::mystr(const char *temp)
{
	char *nt;

	if(!temp)
		temp = "";

        nt = (char *)alloc(strlen(temp) + 1);
        strcpy(nt, temp);
        return nt;
}

#ifdef	CCXX_NAMESPACES
};
#endif
