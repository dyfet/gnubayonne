// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <server.h>

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

class DirTrunk : public TrunkImage
{
private:
	friend class DirModule;

	Dir *dir;

	void characters(const unsigned char *text, unsigned len) {};
	void startElement(const unsigned char *name, const unsigned char **attrib) {};
	void endElement(const unsigned char *name) {};
	bool loader(Trunk *trk, trunkdata_t *data);
	char *mystr(const char *temp);

	static int forward(const void *x, const void *y)
	{
		char **s1 = (char **)x;
		char **s2 = (char **)y; 
		Thread::yield();
		return stricmp(*s1, *s2);
	};

        static int reverse(const void *x, const void *y)
        {
                char **s2 = (char **)x;
                char **s1 = (char **)y;
		Thread::yield();
                return stricmp(*s1, *s2);
        };


	DirTrunk();
	~DirTrunk();
};

class DirModule : private Module
{
private:
	modtype_t getType(void)
		{return MODULE_SQL;};

	char *getName(void)
		{return "dir";};

	TrunkImage *getXML(void)
		{return (TrunkImage *)new DirTrunk;};

	char *dispatch(Trunk *trunk);

public:
	DirModule();
} dirmod;


DirModule::DirModule() : Module()
{
	slog(Slog::levelDebug) << "load: directory scan module" << endl;
	aascript->addModule(this);
}

char *DirModule::dispatch(Trunk *trunk)
{
       	trunkdata_t *data = getData(trunk);
        const char *key, *url = trunk->getKeyword("prefix");

        key = trunk->getKeyword("maxTime");
        if(!key)
                key = "60s";

	url = trunk->getValue(url);
        if(!url)
                return "no-dir-prefix";

	if(!isDir(url))
		return "invalid-directory";

        data->load.attach = false;
        data->load.post = false;
        data->load.section = "";
        key = trunk->getKeyword("maxTime");
        if(!key)
                key = "60s";
        data->load.timeout = getSecTimeout(key);
        data->load.parent = NULL;
        data->load.gosub = false;
        data->load.url = url;
        data->load.vars = NULL;
        data->load.userid[0] = 0;
        return NULL;
}

DirTrunk::DirTrunk() : TrunkImage()
{
	dir = NULL;
}

DirTrunk::~DirTrunk()
{
	if(dir)
		delete dir;
	purge();
}

bool DirTrunk::loader(Trunk *trk, trunkdata_t *data)
{
	const char *mem = trk->getMember();
	const char *ext = trk->getKeyword("extension");
	const char *var = trk->getKeyword("var");
	AudioFile au;
	dir = new Dir(data->load.url);
	char buf[5];
	char sbuf[12];
	const char *names[256][4], *index[256];
	const char **argv;
	const char *name, *cp, *match = trk->getKeyword("match");
	static const char *eof[] = {"", "0", "", NULL};
	unsigned count = 0, limit = 0;
	unsigned long totsize = 0;
	char path[256];
	const char *totals[3];

	if(!mem)
		mem = "none";

	if(!match)
		match = "";

	if(!ext)
		ext = trk->getSymbol(SYM_EXTENSION);

	while(NULL != (name = dir->getName()))
	{
		Thread::yield();
		cp = strrchr(name, '.');
		if(!cp)
			continue;

		if(stricmp(cp, ext))
			continue;

		if(strnicmp(match, name, strlen(match)))
			continue;
		
		index[limit++] = mystr(name);
	}
	if(*mem == 'f' || *mem == 'F')
		qsort((char *)index, limit, sizeof(char *), &forward);
	else if(*mem == 'r' || *mem == 'R')
		qsort((char *)index, limit, sizeof(char *), &reverse);

	getCompile("#dir");
	while(count < limit)
	{
		name = index[count];
                snprintf(path, sizeof(path), "%s/%s", data->load.url, name);
		au.open(path);
		au.setPosition();
		totsize += au.getPosition();
		snprintf(path, sizeof(path), "%ld", au.getPosition());
		names[count][0] = name;
		names[count][1] = mystr(path);
		names[count][2] = mystr(au.getAnnotation());
		au.close();
		argv = (const char **)&names[count++];
		argv[3] = NULL;
		addCompile(0, "data", argv);
	}

	addCompile(0, "data", eof);
	addCompile(0, "return.exit", NULL);
	putCompile(main);	

	getCompile("#count");
	snprintf(buf, sizeof(buf), "%d", limit);
	totals[0] = mystr(buf);
	snprintf(sbuf, sizeof(sbuf), "%ld", totsize);
	totals[1] = mystr(sbuf);
	totals[2] = NULL;
	addCompile(0, "data", totals);	
	putCompile(current);

	trk->setData("#dir");
	if(var)
		if(*var == '&')
			++var;

	if(!var)
		return false;

	snprintf(buf, sizeof(buf), "%d", limit);
	trk->setSymbol(var, sizeof(buf) - 1);
	trk->setSymbol(var, buf);
	return true;
}

char *DirTrunk::mystr(const char *temp)
{
	char *nt;

	if(!temp)
		temp = "";

        nt = (char *)alloc(strlen(temp) + 1);
        strcpy(nt, temp);
        return nt;
}

#ifdef	CCXX_NAMESPACES
};
#endif
