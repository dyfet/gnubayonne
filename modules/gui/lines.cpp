// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "gui.h"

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

static void mklabel(FXComposite *f, const char *text, unsigned offset, unsigned width)
{
	FXLabel *label;

	if(width)
		label = new FXLabel(f, text, NULL,
			JUSTIFY_LEFT|LAYOUT_FIX_WIDTH|FRAME_LINE, offset, 0, width);
	else
		label = new FXLabel(f, text, NULL,
			JUSTIFY_LEFT|LAYOUT_FILL_X|FRAME_LINE, offset);
}

class TabLine : public Script
{
public:
	static unsigned port;
	FXHorizontalFrame *row;
	FXLabel *lname, *lstate, *lmask, *lscript, *lstep;
	char name[16], state[16], mask[9], script[32], step[64];

	TabLine(FXComposite *f);
	void onDetach(void);
	void onState(const char *state);
	void onStep(Line *line);
};

unsigned TabLine::port = 0;

TabLine::TabLine(FXComposite *f)
{
	Trunk *trunk = Driver::drvFirst->getTrunkPort(port++);
	row = NULL;

	if(!trunk)
		return;

	row = new FXHorizontalFrame(f,
		FRAME_NONE|LAYOUT_FILL_X|LAYOUT_FILL_ROW);

	row->setPadLeft(0);
	row->setPadRight(0);
	row->setPadTop(0);
	row->setPadBottom(0);
//	row->setHSpacing(0);

	trunk->getName(name);
	strcpy(state, "initial");
	strcpy(script, "");
	strcpy(mask, "");

	lname = new FXLabel(row, name, NULL,
		JUSTIFY_LEFT|LAYOUT_FIX_WIDTH|FRAME_NONE, 0, 0, 68);
	lname->setPadLeft(0);

	lstate = new FXLabel(row, state, NULL,
		JUSTIFY_LEFT|LAYOUT_FIX_WIDTH|FRAME_NONE, 80, 0, 68);
	lstate->setPadLeft(0);

	lmask = new FXLabel(row, mask, NULL,
		JUSTIFY_LEFT|LAYOUT_FIX_WIDTH|FRAME_NONE, 160, 0, 65);
	lmask->setPadLeft(0);

	lscript = new FXLabel(row, script, NULL,
		JUSTIFY_LEFT|LAYOUT_FIX_WIDTH|FRAME_NONE, 240, 0, 106);
	lscript->setPadLeft(0);

	lstep = new FXLabel(row, "", NULL,
		JUSTIFY_LEFT|LAYOUT_FILL_X|FRAME_NONE, 360);
	lstep->setPadLeft(0);
}

void TabLine::onDetach(void)
{
	lscript->setText("");
	lmask->setText("");
	lstep->setText("");
}

void TabLine::onStep(Line *line)
{
	unsigned len = 0, argc = 0;
	char *p;

	if(!line)
	{
		lscript->setText("exiting...");
		lmask->setText("");
		lstep->setText("");
		return;
	}
	snprintf(mask, sizeof(mask), "%08lx", line->mask);
	lmask->setText(mask);
	snprintf(script, sizeof(script), "%s", line->cmd);
	lscript->setText(script);

	while(len < sizeof(step) - 2 && argc < line->argc)
	{
		p = line->args[argc++];
		if(len)
			step[len++] = ',';
		while(*p && len < sizeof(step) - 1)
			step[len++] = *(p++);
	}
	step[len] = 0;
	lstep->setText(step);
}

void TabLine::onState(const char *state)
{
	lstate->setText(state);
}

TabLines::TabLines(FXComposite *f) :
FXVerticalFrame(f, FRAME_THICK|FRAME_RAISED|LAYOUT_FILL_Y|LAYOUT_FILL_COLUMN)
{
	unsigned count = Driver::drvFirst->getTrunkCount();
	setVSpacing(0);

	FXHorizontalFrame *header = new FXHorizontalFrame(this, 
		FRAME_NONE|LAYOUT_FILL_X|LAYOUT_FILL_ROW);

	header->setPadLeft(0);
	header->setPadRight(0);
	header->setPadBottom(0);
	header->setHSpacing(0);

	mklabel(header, "port", 0, 70);
	mklabel(header, "state", 80, 70);
	mklabel(header, "mask", 160, 70);
	mklabel(header, "script", 240, 110);
	mklabel(header, "step", 360, 0);

	FXScrollWindow *contents = new FXScrollWindow(this,
		LAYOUT_SIDE_TOP|FRAME_NONE|LAYOUT_FILL_X|LAYOUT_FILL_Y);

	FXVerticalFrame *frame = new FXVerticalFrame(contents,
		FRAME_NONE|LAYOUT_FILL_X|LAYOUT_FILL_Y);

	frame->setPadRight(0);
	frame->setPadTop(0);
	frame->setPadLeft(0);
	frame->setVSpacing(1);

	lines = new TabLine[count](frame);
}

void TabLines::onState(Trunk *trunk, const char *state)
{
	unsigned port = trunk->getId();

	if(!lines[port].row)
		return;

	lines[port].onState(state);
	if(!stricmp(state, "detach script"))
		lines[port].onDetach();
}

void TabLines::onStep(Trunk *trunk, Line *line)
{
        unsigned port = trunk->getId();

        if(!lines[port].row)
                return;

        lines[port].onStep(line);
}

#ifdef	CCXX_NAMESPACES
};
#endif
