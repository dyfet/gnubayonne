// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "gui.h"

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

GUIMain::GUIMain(FXApp *a) :
FXMainWindow(a, "Bayonne", NULL, NULL, DECOR_ALL, 0, 0, gui.getWidth(), gui.getHeight())
{
	menubar = new FXMenubar(this, 
		LAYOUT_SIDE_TOP | LAYOUT_FILL_X);
	separator = new FXHorizontalSeparator(this, 
		LAYOUT_SIDE_TOP | LAYOUT_FILL_X | SEPARATOR_GROOVE);
	contents = new FXHorizontalFrame(this,
		LAYOUT_SIDE_TOP | FRAME_NONE | LAYOUT_FILL_X | PACK_UNIFORM_WIDTH);

	tabs = new GUITabs(contents);

	filemenu = mkmenutitle("&FILE");
	mkmenuitem(filemenu, "&Compile", GUIMain::ID_COMPILE);
	mkmenuitem(filemenu, "&Down", GUIMain::ID_DOWN);
};

GUIMain::~GUIMain()
{
}

FXMenuPane *GUIMain::mkmenutitle(FXchar *name)
{
	FXMenuTitle *title;
	FXMenuPane *menu = new FXMenuPane(this);
	title = new FXMenuTitle(menubar, name, NULL, menu);
	title->setFont(gui.getMenuFont());
	return menu;
}

void GUIMain::mkmenuitem(FXMenuPane *menu, FXchar *str, FXuint id)
{
	FXMenuCommand *cmd = new FXMenuCommand(menu, str, NULL, this, id);
	cmd->setFont(gui.getMenuFont());
}

long GUIMain::onMenu(FXObject *, FXSelector sel, void *)
{
	FXuint id = SELID(sel);
	switch(id)
	{
	case ID_COMPILE:
		fifo.command("compile");
		return 1;
	case ID_DOWN:
		fifo.command("down");
		kill(mainpid, SIGINT);
	}
	return 1;
}

FXDEFMAP(GUIMain) GUIMainMap[] =
{
	FXMAPFUNCS(SEL_COMMAND, GUIMain::ID_COMPILE, GUIMain::ID_DOWN, GUIMain::onMenu)
};

FXIMPLEMENT(GUIMain, FXMainWindow, GUIMainMap, ARRAYNUMBER(GUIMainMap))

#ifdef	CCXX_NAMESPACES
};
#endif
