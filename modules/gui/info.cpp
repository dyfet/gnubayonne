// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "gui.h"

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

TabInfo::TabInfo(FXComposite *f) :
FXVerticalFrame(f, FRAME_THICK | FRAME_RAISED | LAYOUT_FILL_Y | LAYOUT_FILL_COLUMN)
{
	static char hdr[2][80];

	sprintf(hdr[0], "Bayonne Server Version %s using driver %s", 
		SYS_VERSION, plugins.getDriverName());

	mkheader(hdr[0]);
//	mkheader(hdr[1]);

	mkfield(TIMESLOTS_TOTAL, "Total Timeslots:");
	mkfield(TIMESLOTS_AVAIL, "Avail Timeslots:");
	mkfield(INCOMING_CALLS, "Incoming Calls:");
	mkfield(INCOMING_TOTAL, "Incoming Total:");
	mkfield(OUTGOING_CALLS, "Outgoing Calls:");
	mkfield(OUTGOING_TOTAL, "Outgoung Total:");
	mkfield(TRK_TOTAL, "Total Trunks:");
	mkfield(EXT_TOTAL, "Total Extensions:");
	mkfield(TIE_TOTAL, "Total Tie Lines:");

        snprintf(tstotal, sizeof(tstotal), "%d", Driver::drvFirst->getTrunkUsed());
        setField(TIMESLOTS_TOTAL, tstotal);

	snprintf(trkcount, sizeof(trkcount), "%d", Driver::drvFirst->getTrkCount());
	snprintf(extcount, sizeof(extcount), "%d", Driver::drvFirst->getExtCount());
	snprintf(tiecount, sizeof(tiecount), "%d", Driver::drvFirst->getTieCount());

	setField(TRK_TOTAL, trkcount);
	setField(EXT_TOTAL, extcount);
	setField(TIE_TOTAL, tiecount);
}

long TabInfo::offSelect(FXObject *sender, FXSelector sel, void *)
{
	gui.removeTimeout(timer);
	return 1;
}

long TabInfo::onSelect(FXObject *sender, FXSelector sel, void *)
{
	unsigned ports = Driver::drvFirst->getTrunkCount();
	unsigned avail = 0;
	char status[840];
	char *cp = status;
	Trunk *trunk;
	unsigned port = 0;
	unsigned incoming = 0, outgoing = 0;
	TrunkGroup *grp = getGroup();

	Driver::drvFirst->getStatus(status);

	while(port < ports)
	{
		trunk = Driver::drvFirst->getTrunkPort(port++);
		if(!trunk)
			continue;

		if(*cp == '-')
			++avail;
		else switch(trunk->getTrunkMode())
		{
		case TRUNK_MODE_INCOMING:
			++incoming;
			break;
		case TRUNK_MODE_OUTGOING:
			++outgoing;
			break;
		}
		++cp;
	}
	snprintf(tsavail, sizeof(tsavail), "%d", avail);
	setField(TIMESLOTS_AVAIL, tsavail);

	snprintf(tsincoming, sizeof(tsincoming), "%d", incoming);
	snprintf(tsoutgoing, sizeof(tsoutgoing), "%d", outgoing);
	snprintf(totin, sizeof(totin), "%ld", grp->getStat(STAT_SYS_INCOMING));
	snprintf(totout, sizeof(totout), "%ld", grp->getStat(STAT_SYS_OUTGOING));
	setField(INCOMING_CALLS, tsincoming);
	setField(OUTGOING_CALLS, tsoutgoing);
	setField(INCOMING_TOTAL, totin);
	setField(OUTGOING_TOTAL, totout);
	gui.addTimeout(1000, this, MINKEY);
	return 1;
}

void TabInfo::mkheader(FXchar *text)
{
	FXLabel *label = new FXLabel(this, text, NULL, JUSTIFY_LEFT);
	label->setFont(gui.getBodyFont());
	label->setWidth(this->getWidth());
}

void TabInfo::mkfield(int id, FXchar *text)
{
	FXHorizontalFrame *horizontal = new FXHorizontalFrame(this, 
		LAYOUT_FILL_ROW | LAYOUT_FILL_X);

	horizontal->setVSpacing(0);
	horizontal->setPadTop(0);
	horizontal->setPadBottom(0);
	horizontal->setPadLeft(0);

	FXLabel *label;

	label = new FXLabel(horizontal, text, NULL, 
		JUSTIFY_LEFT | LAYOUT_FIX_WIDTH, 0, 0, 230, 0);
	label->setFont(gui.getBodyFont());
	label = new FXLabel(horizontal, "", NULL, 
		JUSTIFY_LEFT | LAYOUT_FILL_X);
	label->setFont(gui.getBodyFont());
	data[id] = label;
}

FXDEFMAP(TabInfo) TabInfoMap[] = {
        FXMAPFUNCS(SEL_MAP, MINKEY, MAXKEY, TabInfo::onSelect),
	FXMAPFUNCS(SEL_TIMEOUT, MINKEY, MAXKEY, TabInfo::onSelect),
	FXMAPFUNCS(SEL_UNMAP, MINKEY, MAXKEY, TabInfo::offSelect)
};

FXIMPLEMENT(TabInfo, FXVerticalFrame, TabInfoMap, ARRAYNUMBER(TabInfoMap))


#ifdef	CCXX_NAMESPACES
};
#endif
