// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "server.h"
#include <fx.h>

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

class TabCall;
class TabLine;

class TabGroups : public FXVerticalFrame
{
private:
	FXDECLARE(TabGroups);

	TabGroups() {};
		
public:
	TabGroups(FXComposite *fname);
        long onSelect(FXObject *sender, FXSelector sel, void *);

};
class TabCalls : public FXVerticalFrame
{
private:
        TabCall *calls;

public:
        TabCalls(FXComposite *frame);
        void onState(Trunk *trunk, const char *state);
        void onLogin(Trunk *trunk);
};

class TabLines : public FXVerticalFrame, private Script
{
private:
	TabLine *lines;

public:
	TabLines(FXComposite *frame);
	void onStep(Trunk *trunk, Line *line);
	void onState(Trunk *trunk, const char *state);
};

class TabInfo : public FXVerticalFrame
{
private:
	FXDECLARE(TabInfo);
	FXLabel *data[9];
	FXTimer *timer;
	char tstotal[4], tsavail[4];
	char tsincoming[4], tsoutgoing[4];
	char trkcount[4], extcount[4], tiecount[4];
	char totin[8], totout[8];

protected:
	void mkheader(FXchar *text);
	void mkfield(int id, FXchar *text);
	TabInfo() {};

public:
	TabInfo(FXComposite *frame);

	long onSelect(FXObject *sender, FXSelector sel, void *);
	long offSelect(FXObject *sender, FXSelector sel, void *);

	inline void setField(int id, FXchar *value)
		{data[id]->setText(value);};

	enum
	{
		TIMESLOTS_TOTAL = 0,
		TIMESLOTS_AVAIL,
		INCOMING_CALLS,
		OUTGOING_CALLS,
		INCOMING_TOTAL,
		OUTGOING_TOTAL,
		TRK_TOTAL,
		TIE_TOTAL,
		EXT_TOTAL
	};
};

class GUITabs : public FXTabBook, private Script
{
protected:
	TabInfo *info;
	TabCalls *calls;
	TabGroups *groups;
	TabLines *lines;

	FXTabItem *items[4];
	// FXHorizontalFrame *frame[4];

	void mkitem(int id, FXchar *title);

public:
	GUITabs(FXComposite *frame);
	void onStep(Trunk *trunk, Line *line);
	void onState(Trunk *trunk, const char *state);
	void onLogin(Trunk *trunk)
		{calls->onLogin(trunk);};
};


class GUIMain : public FXMainWindow, private Script
{
private:
	FXDECLARE(GUIMain)

protected:
	GUIMain() {};

	GUITabs *tabs;

	FXMenubar *menubar;
	FXHorizontalSeparator *separator;
	FXHorizontalFrame *contents;
	FXMenuPane *filemenu, *tabmenu;

	void mkmenuitem(FXMenuPane *menu, FXchar *name, FXuint id);
	FXMenuPane *mkmenutitle(FXchar *name);

public:
	GUIMain(FXApp *a);
	virtual ~GUIMain();

	void onStep(Trunk *trunk, Line *line)
		{tabs->onStep(trunk, line);};

	void onState(Trunk *trunk, const char *state)
		{tabs->onState(trunk, state);};

	void onLogin(Trunk *trunk)
		{tabs->onLogin(trunk);};

	long onMenu(FXObject *, FXSelector, void *);

	enum {
		ID_COMPILE = FXMainWindow::ID_LAST,
		ID_DOWN
	};
};

class GUIApp : public FXApp, public Server, public Debug, public Keydata
{
private:
	friend class GUIDebug;
	static GUIApp *first;
	GUIApp *next;
	GUIMain *main;
	FXFont *menu, *title, *fixed, *body;
	bool running;

	void initial(void);
	void run(void);
	void stop(void);

	virtual void debugState(Trunk *trunk, char *state);
	virtual void debugStep(Trunk *trunk, Line *line);

	virtual void debugLogin(Trunk *trunk);

public:
	GUIApp();
	~GUIApp()
		{stop();};

	inline int getPoints(void)
		{return atoi(getLast("points"));};

	inline int getHeight(void)
		{return atoi(getLast("height"));};

	inline int getWidth(void)
		{return atoi(getLast("width"));};

	inline FXchar *getTypeface(void)
		{return (FXchar *)getLast("typeface");};

	inline FXchar *getFixed(void)
		{return (FXchar *)getLast("fixed");};

	inline FXFont *getMenuFont(void)
		{return menu;};

	inline FXFont *getTitleFont(void)
		{return title;};

	inline FXFont *getFixedFont(void)
		{return fixed;};

	inline FXFont *getBodyFont(void)
		{return body;};
};

extern GUIApp gui;

#ifdef	CCXX_NAMESPACES
};
#endif
