// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "gui.h"

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

static void mklabel(FXComposite *f, const char *text, unsigned offset, unsigned width)
{
	FXLabel *label;

	if(width)
		label = new FXLabel(f, text, NULL,
			JUSTIFY_LEFT|LAYOUT_FIX_WIDTH|FRAME_LINE, offset, 0, width);
	else
		label = new FXLabel(f, text, NULL,
			JUSTIFY_LEFT|LAYOUT_FILL_X|FRAME_LINE, offset);
}

class TabGroup
{
public:
	static TabGroup *first;
	TrunkGroup *group;
	TabGroup *next;
	FXHorizontalFrame *row;
	FXHorizontalSeparator *sep;

	FXLabel *lname, *lmembers, *lipeek, *lopeek, *litotal, *lototal;

	char members[5], ipeek[16], opeek[16], itotal[40], ototal[40];

	TabGroup(FXComposite *f, TrunkGroup *grp);
	void getStats(void);
};

TabGroup *TabGroup::first = NULL;

TabGroup::TabGroup(FXComposite *f, TrunkGroup *grp)
{
	next = first;
	first = this;
	group = grp;

	row = new FXHorizontalFrame(f,
		FRAME_NONE|LAYOUT_FILL_X|LAYOUT_FILL_ROW);

	row->setPadLeft(0);
	row->setPadRight(0);
	row->setPadTop(0);
	row->setPadBottom(0);

	lname = new FXLabel(row, group->getName(), NULL,
		JUSTIFY_LEFT|LAYOUT_FIX_WIDTH|FRAME_NONE, 0, 0, 116);
	lname->setPadLeft(0);

	snprintf(members, sizeof(members), "%d\n", group->getCapacity());
	lmembers = new FXLabel(row, members, NULL,
		JUSTIFY_RIGHT|LAYOUT_FIX_WIDTH|FRAME_NONE, 130, 0, 38); 

	strcpy(ipeek, "\n");
	strcpy(opeek, "\n");
	strcpy(itotal, "\n");
	strcpy(ototal, "\n");

	lipeek = new FXLabel(row, ipeek, NULL,
		JUSTIFY_RIGHT|LAYOUT_FIX_WIDTH|FRAME_NONE, 180, 0, 38);

	lopeek = new FXLabel(row, opeek, NULL,
		JUSTIFY_RIGHT|LAYOUT_FIX_WIDTH|FRAME_NONE, 230, 0, 38);

	litotal = new FXLabel(row, itotal, NULL,
		JUSTIFY_RIGHT|LAYOUT_FIX_WIDTH|FRAME_NONE, 280, 0, 78);

	lototal = new FXLabel(row, ototal, NULL,
		JUSTIFY_RIGHT|LAYOUT_FIX_WIDTH|FRAME_NONE, 370, 0, 78);

	sep = new FXHorizontalSeparator(f, 
		LAYOUT_FIX_WIDTH|SEPARATOR_GROOVE, 0, 0, 400);

	getStats();
}

void TabGroup::getStats(void)
{
	unsigned long sta[16];

	group->getStat(sta);

	snprintf(ipeek, sizeof(ipeek), "%d\n%d", sta[0], sta[2]);
	lipeek->setText(ipeek);

	snprintf(opeek, sizeof(opeek), "%d\n%d", sta[1], sta[3]);
	lopeek->setText(opeek);
}

TabGroups::TabGroups(FXComposite *f) :
FXVerticalFrame(f, FRAME_THICK|FRAME_RAISED|LAYOUT_FILL_Y|LAYOUT_FILL_COLUMN)
{
	TrunkGroup *grp = getGroup();
	setVSpacing(0);

	FXHorizontalFrame *header = new FXHorizontalFrame(this, 
		FRAME_NONE|LAYOUT_FILL_X|LAYOUT_FILL_ROW);

	header->setPadLeft(0);
	header->setPadRight(0);
	header->setPadBottom(0);
	header->setHSpacing(0);

	mklabel(header, "name\n", 0, 120);
	mklabel(header, "mem\n", 130, 40);
	mklabel(header, "peek\nin", 180, 40);
	mklabel(header, "peek\nout", 230, 40);
	mklabel(header, "total\nin", 280, 80);
	mklabel(header, "total\nout", 370, 80);

	FXScrollWindow *contents = new FXScrollWindow(this,
		LAYOUT_SIDE_TOP|FRAME_NONE|LAYOUT_FILL_X|LAYOUT_FILL_Y);

	FXVerticalFrame *frame = new FXVerticalFrame(contents,
		FRAME_NONE|LAYOUT_FILL_X|LAYOUT_FILL_Y);

	frame->setPadRight(0);
	frame->setPadTop(0);
	frame->setPadLeft(0);
	frame->setVSpacing(1);

	while(NULL != grp)
	{
		new TabGroup(frame, grp);
		grp = grp->getNext();
	}
}

long TabGroups::onSelect(FXObject *sender, FXSelector sel, void *)
{
	TabGroup *group = TabGroup::first;

	while(group)
	{
		group->getStats();
		group = group->next;
	}

	return 1;
}

FXDEFMAP(TabGroups) TabGroupsMap[] = {
        FXMAPFUNCS(SEL_MAP, MINKEY, MAXKEY, TabGroups::onSelect)
//        FXMAPFUNCS(SEL_TIMEOUT, MINKEY, MAXKEY, TabGroups::onSelect),
//        FXMAPFUNCS(SEL_UNMAP, MINKEY, MAXKEY, TabGroups::offSelect)
};

FXIMPLEMENT(TabGroups, FXVerticalFrame, TabGroupsMap, ARRAYNUMBER(TabGroupsMap))

#ifdef	CCXX_NAMESPACES
};
#endif
