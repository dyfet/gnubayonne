// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "gui.h"

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

GUITabs::GUITabs(FXComposite *f) :
FXTabBook(f, NULL, 0, LAYOUT_FILL_X|LAYOUT_FILL_Y|LAYOUT_RIGHT)
{
	mkitem(0, "&Info");
	info = new TabInfo(this);
	mkitem(1, "&Calls");
	calls = new TabCalls(this);
	mkitem(2, "&Groups");
	groups = new TabGroups(this);
	mkitem(3, "&Lines");
	lines = new TabLines(this);
}

void GUITabs::onState(Trunk *trunk, const char *state)
{
	calls->onState(trunk, state);
	lines->onState(trunk, state);
}

void GUITabs::onStep(Trunk *trunk, Script::Line *line)
{
	lines->onStep(trunk, line);
}

void GUITabs::mkitem(int id, FXchar *header)
{
	items[id] = new FXTabItem(this, header, NULL);
	items[id]->setFont(gui.getTitleFont());
}

#ifdef	CCXX_NAMESPACES
};
#endif
