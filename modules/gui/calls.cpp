// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "gui.h"

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

static void mklabel(FXComposite *f, const char *text, unsigned offset, unsigned width)
{
	FXLabel *label;

	if(width)
		label = new FXLabel(f, text, NULL,
			JUSTIFY_LEFT|LAYOUT_FIX_WIDTH|FRAME_LINE, offset, 0, width);
	else
		label = new FXLabel(f, text, NULL,
			JUSTIFY_LEFT|LAYOUT_FILL_X|FRAME_LINE, offset);
}

class TabCall
{
public:
	static unsigned port;
	FXHorizontalFrame *row;
	FXLabel *lname, *llogin, *ltime, *ldialed, *lcaller;
	char name[16], login[17], start[12], dialed[17], caller[65];

	TabCall(FXComposite *f);
	void onAttach(Trunk *trunk);
	void onDetach(void);
	void onLogin(Trunk *trunk);
};

unsigned TabCall::port = 0;

TabCall::TabCall(FXComposite *f)
{
	Trunk *trunk = Driver::drvFirst->getTrunkPort(port++);
	row = NULL;

	if(!trunk)
		return;

	row = new FXHorizontalFrame(f,
		FRAME_NONE|LAYOUT_FILL_X|LAYOUT_FILL_ROW);

	row->setPadLeft(0);
	row->setPadRight(0);
	row->setPadTop(0);
	row->setPadBottom(0);

	trunk->getName(name);
	strcpy(start, "--:--:--");
	strcpy(login, "none");
	strcpy(dialed, "");
	strcpy(caller, "");
	lname = new FXLabel(row, name, NULL,
		JUSTIFY_LEFT|LAYOUT_FIX_WIDTH|FRAME_NONE, 0, 0, 68);
	lname->setPadLeft(0);

	ltime = new FXLabel(row, start, NULL,
		JUSTIFY_LEFT|LAYOUT_FIX_WIDTH|FRAME_NONE, 80, 0, 56);
	ltime->setPadLeft(0);
	
	llogin = new FXLabel(row, login, NULL,
		JUSTIFY_LEFT|LAYOUT_FIX_WIDTH|FRAME_NONE, 150, 0, 116);
	llogin->setPadLeft(0);

	ldialed = new FXLabel(row, dialed, NULL,
		JUSTIFY_LEFT|LAYOUT_FIX_WIDTH|FRAME_NONE, 280, 0, 116);
	ldialed->setPadLeft(0);
	
	lcaller = new FXLabel(row, caller, NULL,
		JUSTIFY_LEFT|LAYOUT_FILL_X|FRAME_NONE, 410); 	
	lcaller->setPadLeft(0);
}

void TabCall::onDetach(void)
{
	strcpy(login, "none");
	strcpy(start, "--:--:--");
	strcpy(dialed, "");
	strcpy(caller, "");

	llogin->setText(login);
	ltime->setText(start);
	lcaller->setText(caller);
	ldialed->setText(dialed);
}

void TabCall::onLogin(Trunk *trunk)
{
	strcpy(login, trunk->getSymbol(SYM_LOGIN));
	llogin->setText(login);
}

void TabCall::onAttach(Trunk *trunk)
{
	time_t now;
	struct tm *dt;
	char *p, *q;
	char *cn;

	time(&now);
	dt = localtime(&now);

	snprintf(start, sizeof(start), "%02d:%02d:%02d",
		dt->tm_hour, dt->tm_min, dt->tm_sec);

	ltime->setText(start);

	strcpy(login, trunk->getSymbol(SYM_LOGIN));
	llogin->setText(login);

	if(trunk->getTrunkMode() != TRUNK_MODE_INCOMING)
	{
		lcaller->setText("[outgoing]");
		return;
	}

	p = trunk->getSymbol(SYM_DIALED);
	if(!p)
		p = "na";

	q = strchr(p, ';');
	if(q)
		snprintf(dialed, sizeof(dialed), "%s", q);
	else
		snprintf(dialed, sizeof(dialed), "%s", p);

	ldialed->setText(dialed);

	cn = trunk->getSymbol(SYM_NAME);
	if(!cn)	
		cn = "";

	q = NULL;
	if(!*cn)
	{
		cn = trunk->getSymbol(SYM_CALLER);
		if(!cn)
			cn = "unknown";
		else
			q = strchr(cn, ';');
		
		if(q)
			cn = q;
	}	
	snprintf(caller, sizeof(caller), "%s", cn);
	lcaller->setText(caller);	
}

TabCalls::TabCalls(FXComposite *f) :
FXVerticalFrame(f, FRAME_THICK|FRAME_RAISED|LAYOUT_FILL_Y|LAYOUT_FILL_COLUMN)
{
	unsigned count = Driver::drvFirst->getTrunkCount();
	setVSpacing(0);

	FXHorizontalFrame *header = new FXHorizontalFrame(this, 
		FRAME_NONE|LAYOUT_FILL_X|LAYOUT_FILL_ROW);

	header->setPadLeft(0);
	header->setPadRight(0);
	header->setPadBottom(0);
	header->setHSpacing(0);

	mklabel(header, "port", 0, 70);
	mklabel(header, "start", 80, 60);
	mklabel(header, "login", 150, 120);
	mklabel(header, "dialed", 280, 120);
	mklabel(header, "caller", 410, 0);

	FXScrollWindow *contents = new FXScrollWindow(this,
		LAYOUT_SIDE_TOP|FRAME_NONE|LAYOUT_FILL_X|LAYOUT_FILL_Y);

	FXVerticalFrame *frame = new FXVerticalFrame(contents,
		FRAME_NONE|LAYOUT_FILL_X|LAYOUT_FILL_Y);

	frame->setPadRight(0);
	frame->setPadTop(0);
	frame->setPadLeft(0);
	frame->setVSpacing(1);

	calls = new TabCall[count](frame);
}

void TabCalls::onLogin(Trunk *trunk)
{
	unsigned port = trunk->getId();

	if(!calls[port].row)
		return;

	calls[port].onLogin(trunk);
}

void TabCalls::onState(Trunk *trunk, const char *state)
{
	unsigned port = trunk->getId();

	if(!calls[port].row)
		return;

	if(!stricmp(state, "detach script"))
		calls[port].onDetach();
	else if(!stricmp(state, "attach script"))
		calls[port].onAttach(trunk);
}

#ifdef	CCXX_NAMESPACES
};
#endif
