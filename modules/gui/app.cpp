// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "gui.h"

#ifdef	CCXX_NAMESPACES
namespace ost {
using namespace std;
#endif

GUIApp *GUIApp::first = NULL;

GUIApp::GUIApp() :
FXApp("bayonne", "OST"), Server(keythreads.priGUI()), Debug(),
Keydata("/bayonne/gui")
{
	static Keydata::Define keys[] = {
		{"width", "600"},
		{"height", "400"},
		{"points", "10"},
		{"typeface", "helvetica"},
		{"fixed", "fixed"},
		{NULL, NULL}};

	char *argv[2];
	int argc = 1;

	next = first;
	first = this;

	Keydata::load("~bayonne/gui");
	Keydata::load(keys);

	argv[0] = "bayonne";
	argv[1] = NULL;

	main = NULL;
	running = false;

	init(argc, argv);
}

void GUIApp::stop()
{
	running = false;
	slog(Slog::levelDebug) << "gui: stopping" << endl;
	terminate();
	destroy();
}

void GUIApp::initial(void)
{
	slog(Slog::levelDebug) << "gui: starting session" << endl;
	menu = new FXFont((FXApp *)this, getTypeface(), getPoints(),
		FONTWEIGHT_NORMAL, FONTSLANT_REGULAR, FONTENCODING_DEFAULT);

	title = new FXFont((FXApp *)this, getTypeface(), getPoints() + 2,
		FONTWEIGHT_BOLD, FONTSLANT_REGULAR, FONTENCODING_DEFAULT);

	fixed = new FXFont((FXApp *)this, getFixed(), getPoints() + 2);

	body = new FXFont((FXApp *)this, getTypeface(), getPoints() + 2,
		FONTWEIGHT_NORMAL, FONTSLANT_REGULAR, FONTENCODING_DEFAULT);

	main = new GUIMain((FXApp *)this);

	create();
	main->show();
	running = true;
}

void GUIApp::run(void)
{
	setCancel(cancelImmediate);
	slog(Slog::levelDebug) << "gui: running" << endl;
	FXApp::run();
	kill(mainpid, SIGINT);
	slog(Slog::levelDebug) << "gui: stopping session" << endl;
	sleep(~0);
}

void GUIApp::debugState(Trunk *trunk, char *state)
{
	if(running)
		main->onState(trunk, state);
}

void GUIApp::debugLogin(Trunk *trunk)
{
	if(running)
		main->onLogin(trunk);
}

void GUIApp::debugStep(Trunk *trunk, Line *line)
{
	if(running)
		main->onStep(trunk, line);
}

GUIApp gui;

#ifdef	CCXX_NAMESPACES
};
#endif
