#!/bin/sh
# Copyright (C) 2001 Open Source Telecom Corporation.
# 
# This file is free software; as a special exception the author gives 
# unlimited permission to copy and/or distribute it, with or without
# modifications, as long as this notice is preserved.
# 
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

XDIALOG=`which gdialog 2>/dev/null`
CDIALOG=`which cdialog 2>/dev/null`
SETUP=`bayonne --setup`

if test -z "$CDIALOG" ; then CDIALOG="dialog" ; fi
if test -z "$XDIALOG" ; then XDIALOG=`which Xdialog 2>/dev/null` ; fi
if test -z "$XDIALOG" ; then XDIALOG="$CDIALOG" ; fi

if test -z "$DIALOG" ; then
	if test -z "$DISPLAY" ; then
		DIALOG=$CDIALOG
	else
		DIALOG=$XDIALOG
	fi
fi

for arg in "$*" ; do
	case "$arg" in
	-c | --console)
		DIALOG=$CDIALOG
		;;
	-t | --test)
		SETUP=""
		;;
	esac
done
	
if test -z "$SETUP" ; then 
	SETUP="./bayonne_setup"
else
	PREFIX=`bayonne --adminpath`	
fi

if test -z "$CONFIG" ; then CONFIG="exec $0" ; fi
if test -z "$PREFIX" ; then PREFIX="." ; fi
if test -z "$TITLE" ; then TITLE="Bayonne Admin" ; fi

export DIALOG SETUP CONFIG TITLE

if test -z "$MODULES" ; then
	modules=`cd $PREFIX ; ls *.cfg`
	MODULES="0+0+0"
	for module in $modules ; do
		name=`echo $module | sed s/[0-9][0-9]// | \
			sed s/.cfg$//`
		desc=`grep "^#desc\:" $PREFIX/$module | \
			sed s/^#desc[:]//`
		MODULES="$MODULES+$name+$desc"
	done
	export MODULES
fi 

(IFS="+" ; $DIALOG --title "Select module" --backtitle "$TITLE" --menu "\
You can select one of the following\n\
modules referenced in this menu to\n\
alter it's associated options" \
	$MODULES 2> /tmp/menu.tmp.$$)
retval=$?
TITLE="Bayonne Admin"
case $retval in
0)
	choice=`cat /tmp/menu.tmp.$$`
	rm -f /tmp/menu.tmp.$$
	exec $PREFIX/[0-9][0-9]$choice.cfg
	;;
esac


