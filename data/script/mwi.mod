::dialer
	foreach %extension %mwi.extensions
		dtmf %extension
		if %timer > 0 then flash %timer
		dtmf %code 
		flash 750ms
	loop
	exit

::generic
	begin
	if %mwi.mode == "on"
	then
		goto ::dialer timer=200ms code="*30"
	else
		goto ::dialer timer=200ms code="*31"
	endif
	exit
