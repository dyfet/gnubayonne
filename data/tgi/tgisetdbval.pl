#!/usr/bin/perl
#
# Copyright (C) 2000-2001 Open Source Telecom Corporation.
#  
# This file is free software; as a special exception the author gives
# unlimited permission to copy and/or distribute it, with or without 
# modifications, as long as this notice is preserved.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
# Adds the give key-value pair to the given database.
# This is useful for a simple key-value pair data store for a tgi app.
# Returns 0 on success, -1 on error.
# 
# V1.1 4/24/00 - created - rsb
#

#use strict;
use lib $ENV{'SERVER_LIBEXEC'};
use TGI;
use POSIX;
use SDBM_File;

my($version) = "1.1";
my($debug_level) = 0;
my($value);
my($return);

$database = $TGI::QUERY{dbase};
$key = $TGI::QUERY{key};
$value = $TGI::query{value};

$return = setval($key, $value, $dbase);

TGI::set("return", $return);

###############################################################################
# setval - sets key $_[0] and value $_[1] in the database $_[2],
# and returns 0 if o.k., 1 if the value already exists, and -1 on other error.
###############################################################################
sub setval {

    my($database) = $_[2];
    my($value) = $_[1];
    my($key) = $_[0];
    my(%hash);
    my($retval);
    
    tie(%hash, SDBM_File, $database, O_RDWR | O_CREAT, 0666);
    
    if (exists($hash{$key})) {
	$value = $hash{$key};
	$retval = 0;
    }
    else {
	$retval = -1;
    }

    untie %hash;
    return $retval; 
}

#EOF




