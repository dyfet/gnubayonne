# Copyright (C) 2000-2001 Open Source Telecom Corporation.
#  
# This file is free software; as a special exception the author gives
# unlimited permission to copy and/or distribute it, with or without 
# modifications, as long as this notice is preserved.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

package KEYDATA;

$VERSION = "0.01";

1;

sub load
{
	my($self) = {};
	my($section) = "_INIT";
	$filename = shift;
	open(CONFIG, "< $filename");
	while(<CONFIG>)
	{
		s/^\s+//g;
		s/\s+$//g;
		if(/^[A-Za-z]/)
		{
			my($keyword, $value) = split('=', $_, 2);
			$keyword = lc($keyword);
			$keyword =~ s/\s+//g;
			$value =~ s/^\s+//g;
			if($value =~ /^\"/)
			{
				$value =~ s/^\"//;
				$value =~ s/\"$//;
			}
			if($value =~ /^\'/)
			{
				$value =~ s/^\'//;
				$value =~ s/\'$//;
			}
			eval "\$$section\{$keyword\}=\$value";
		}
		if(/^\[/)
		{
			s/^\[//;
			s/\]$//;
			$section = uc($_);
			$section =~ s/\s+//g;
		}
		
	}
	close(CONFIG);
}
