#!/usr/local/bin/php
<?

include "TGI.php";

# get a new TGI Object
$tgi = new TGI();

# get some values from the TGI object
$serv_ver = $tgi->VERSION;
$serv_sw = $tgi->SERVER;

$port_num = $tgi->PORT;
$port_qry = $tgi->QUERY;
$port_dig =	$tgi->DIGITS;
$port_dnid = $tgi->DNID;
$port_cid = $tgi->CLID;

# open a tmp file to which we send some sample output.
$fh = fopen("/tmp/bayonnephp.txt", "w");
$date = date("Y/m/d H:i:s", time());

# prepare the output
$log_string = <<< EOD
Recorded as at: $date

Server Software: $serv_sw
Server Version: $serv_ver
---
Port Number: $port_num
Port Query: $port_qry
Port Digits: $port_dig
Port DNID: $port_dnid
Port CID: $port_cid
---
EOD;

# include the TGI variables in the output, as 'key'='value'
foreach(array_keys($tgi->getTGIvars()) as $qryKey) {
	$qryVal = $tgi->getVar($qryKey);
	$log_string .= "'$qryKey' = '$qryVal'\n";
}

# close the /tmp file.
fwrite($fh, $log_string . "\n");
fclose ($fh);

# set a return variable to the TGI.
$tgi->set("return","hellofromPHP");

# signal the TGI we're done.
$tgi->exitTGI(0);

exit;
?>
