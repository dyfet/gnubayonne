<?

# Copyright (C) 2002 Open Source Telecom Corporation.
#  
# This file is free software; as a special exception the author gives
# unlimited permission to copy and/or distribute it, with or without 
# modifications, as long as this notice is preserved.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
# TGI PHP support for TGI version 1.0 and 1.1

class TGI {

	var $PROTOCOL;
	var $SERVER;
	var $VERSION;
	var $CONTROL;
	var $PROTOCOL;
	var $DIGITS;
	var $QUERY;
	var $CLID;
	var $DNID;
	var $PORT;
	var $TOKEN;
	var $PAIRS;
	var $NODE;
	var $AUDIOFILE;
	var $QUERY;
	var $queryArr;

	# PHP class constructor for this object
	function TGI() {

		# setup class variable defaults
		$this->PROTOCOL = '1.00';
		$this->SERVER = null;
		$this->VERSION = null;
		$this->CONTROL = null;
		$this->PROTOCOL = null;
		$this->DIGITS = null;
		$this->QUERY = null;
		$this->CLID = null;
		$this->DNID = null;
		$this->PORT = '0';
		$this->TOKEN = ' ';
		$this->PAIRS = '&';
		$this->NODE = null;
		$this->AUDIOFILE = null;
		$this->QUERY = null;

		# get a bunch of variables from the TGI
		if (!is_null(getenv('SERVER_POLICIES'))) $POLICIES_arr = split(" ", getenv('SERVER_POLICIES'));
		if (!is_null(getenv('SERVER_VERSION'))) $this->VERSION = getenv('SERVER_VERSION');
		if (!is_null(getenv('SERVER_CONTROL'))) $this->CONTROL = getenv('SERVER_CONTROL');
		if (!is_null(getenv('SERVER_SOFTWARE'))) $this->SERVER = getenv('SERVER_SOFTWARE');
		if (!is_null(getenv('SERVER_VERSION'))) $this->PROTOCOL = getenv('SERVER_VERSION');
		if (!is_null(getenv('SERVER_PROTOCOL'))) $this->PROTOCOL = getenv('SERVER_PROTOCOL');
		if (!is_null(getenv('SERVER_NODE'))) $this->NODE = getenv('SERVER_NODE');
		if (getenv('SERVER_TOKEN') != '') $this->TOKEN = getenv('SERVER_TOKEN');
		if (getenv('SERVER_PAIRS') != '') $this->PAIRS = getenv('SERVER_PAIRS');
		if (getenv('PORT_NUMBER') != '') $this->PORT = getenv('PORT_NUMBER');
		if (!is_null(getenv('PORT_CLID'))) $this->CLID = getenv('PORT_CLID');
		if (!is_null(getenv('PORT_DNID'))) $this->DNID = getenv('PORT_DNID');
		if (!is_null(getenv('PORT_DIGITS'))) $this->DIGITS = getenv('PORT_DIGITS');
		if (!is_null(getenv('TEMP_AUDIO'))) $this->AUDIOFILE = getenv('TEMP_AUDIO');

		# call our own function which gets the key/value pairs from TGI
		$this->queryArr = $this->getTGIvars();

		# have NOT implemented the following code from TGI.pm in PHP, not sure what it was for.

		#if (!is_null(getenv('SERVER_RUNTIME')))
		#{
		#	chdir $ENV{'SERVER_RUNTIME'};
		#	$TempFile::TMPDIRECTORY = "$ENV{'SERVER_RUNTIME'}/temp";
		#};
	}

	# function to get all key/value pairs from TGI and store them in a class
	# variable named queryArr. Like TGI.pm, this is an associative array (or hash).
	function getTGIvars() {
		if(!is_null(getenv('PORT_QUERY'))) {
			$query_string = getenv('PORT_QUERY');
			$delim = $this->PAIRS;
			$pairs_arr = split($delim, $query_string);
			foreach($pairs_arr as $pairs_val)
			{
				# This catches equal signs in the keyvalue pair.
				# Look up "greediness" in the regex docs to explain the '?'
				$keyval_arr = split("=", $pairs_val);
				$keyword = $keyval_arr[0];
				$value = $keyval_arr[1];
				$queryArr[strtolower($keyword)] = $value;
			}
		}
		return $queryArr;
	}

	# function to return the value of a TGI variable given it's name.
	function getVar($name) {
		$qry = $this->queryArr;
		$value = $qry[$name];
		return $value;
	}

	# function to set the value of a variable in the TGI so that bayonne gets it.
	function set ($variable, $value)
	{
		$TOKEN = $this->TOKEN;
		$PORT = $this->PORT;
		print "SET$TOKEN$PORT$TOKEN$variable$TOKEN$value\n";
	}

	function size ($variable, $size)
	{
		$TOKEN = $this->TOKEN;
		$PORT = $this->PORT;
		print "SIZE$TOKEN$PORT$TOKEN$variable$TOKEN$size\n";
	}

	function put ($variable, $value)
	{
		$TOKEN = $this->TOKEN;
		print "PUT$TOKEN$variable$TOKEN$value\n";
	}

	function post ($variable, $value)
	{
		$TOKEN = $this->TOKEN;
		print "POST$TOKEN$variable$TOKEN$value\n";
	}

	function down()
	{
		print "DOWN\n";
	}

	function idle()
	{
		$PORT = $this->PORT;
		$TOKEN = $this->TOKEN;
		print "IDLE$TOKEN$PORT\n";
	}

	function busy()
	{
		$PORT = $this->PORT;
		$TOKEN = $this->TOKEN;
		print "BUSY$TOKEN$PORT\n";
	}

	function compile()
	{
		print "COMPILE\n";
	}

	function exitTGI($exitcode)
	{
		$TOKEN = $this->TOKEN;
		$pid = posix_getpid();
		print "EXIT$TOKEN$pid$TOKEN$exitcode\n";
	}

}

?>
