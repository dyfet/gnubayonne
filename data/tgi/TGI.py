#!/usr/bin/env python
# Copyright (C) 2000-2001 Open Source Telecom Corporation.
#  
# This file is free software; as a special exception the author gives
# unlimited permission to copy and/or distribute it, with or without 
# modifications, as long as this notice is preserved.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
# TGI Python support for TGI version 1.0 and 1.1

from os import environ
import string, os

PROTOCOL = "1.00"
SERVER = VERSION = CONTROL = DIGITS = None
QUERY = {}
CLID = DNID = None
PORT = "0"
TOKEN = " "
PAIRS = "&"
NODE = None
AUDIOFILE = None

if environ.has_key('SERVER_POLICIES'):
    POLICIES = string.split(environ['SERVER_POLICIES'])
if environ.has_key('SERVER_VERSION'):
    VERSION = environ['SERVER_VERSION']
if environ.has_key('SERVER_CONTROL'):
    CONTROL = environ['SERVER_CONTROL']
if environ.has_key('SERVER_SOFTWARE'):
    SERVER = environ['SERVER_SOFTWARE']
if environ.has_key('SERVER_PROTOCOL'):
    PROTOCOL = environ['SERVER_PROTOCOL']
if environ.has_key('SERVER_NODE'):
    NODE = environ['SERVER_NODE']
if environ.has_key('SERVER_TOKEN'):
    TOKEN = PAIRS = environ['SERVER_TOKEN']
if environ.has_key('PORT_NUMBER'):
    PORT = environ['PORT_NUMBER']
if environ.has_key('PORT_CLID'):
    CLID = environ['PORT_CLID']
if environ.has_key('PORT_DNID'):
    DNID = environ['PORT_DNID']
if environ.has_key('PORT_DIGITS'):
    DIGITS = environ['PORT_DIGITS']
if environ.has_key('TEMP_AUDIO'):
    AUDIOFILE = environ['TEMP_AUDIO']    
    
if environ.has_key('PORT_QUERY'):
    
    # Prepare a query parameters, ignoring invalid parms
    for i in filter(lambda x: len(x) == 2, map(lambda x: string.split(x, '=', 1),
	string.split(environ['PORT_QUERY'],  '&'))):

        QUERY[string.lower(i[0])] = i[1]

if environ.has_key('SERVER_RUNTIME'):
    os.chdir(environ['SERVER_RUNTIME'])
    # XXX - Python may not have this
    # $TempFile::TMPDIRECTORY = "$ENV{'SERVER_RUNTIME'}/temp";

def set(variable, value):
    print string.join(('SET', PORT, str(variable), str(value)), TOKEN)

def size(variable, size)
    print string.join(('SIZE', PORT, str(variable), str(size)), TOKEN)
    
def put(variable, value):
    print string.join(('PUT', str(variable), str(value)), TOKEN)

def post(variable, value):
    print string.join(('POST', str(variable), str(value)), TOKEN)

def down():
    print 'DOWN'
    
def idle():
    print string.join(('IDLE', PORT), TOKEN)
    
def busy():
    print string.join(('BUSY', PORT), TOKEN)
    
def compile():
    print 'COMPILE'

def exit(exitcode):
    print string.join(('EXIT', str(os.getpid()), str(exitcode)), TOKEN)
