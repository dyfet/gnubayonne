/* Copyright (C) 2000-2001 Open Source Telecom Corporation.
 
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software 
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/vfs.h>

int main(int argc, char **argv)
{
	struct statfs fs;
	const char *prefix = "server";
	const char *path = getenv("SERVER_RUNTIME");
	char *cp, *query = getenv("PORT_QUERY");
	long used, total;

	if(!path)
		path = "/var";

	/* set default query arguments */
	setenv("path", path, 1);
	setenv("prefix", prefix, 1);

	cp = query;
	while(cp && *cp)
	{
		*cp = tolower(*cp);
		++cp;
	}

        while(query)
        {
                cp = strchr(query, '&');
                if(cp)
                        *cp = 0;
                putenv(query);
                if(cp)
                        query = ++cp;
                else
                        query = NULL;
        }

	path = getenv("path");
	prefix = getenv("prefix");

	printf("size %s.used 3\n", prefix);
	printf("size %s.free 3\n", prefix);

	if(statfs(path, &fs))
	{
		printf("set %s.used 100\n", prefix);
		printf("set %s.free 0\n", prefix);
		exit(0);
	}
	
	used = fs.f_blocks - fs.f_bfree;
	total = fs.f_blocks / 100l;
	printf("set %s.used %ld\n", prefix, used / total);
	printf("set %s.free %ld\n", prefix, fs.f_bavail / total);

	exit(0);
}
