#!/usr/bin/perl
#
# Copyright (C) 2000-2001 Open Source Telecom Corporation.
#  
# This file is free software; as a special exception the author gives
# unlimited permission to copy and/or distribute it, with or without 
# modifications, as long as this notice is preserved.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

use lib $ENV{'SERVER_LIBEXEC'};
use TGI;

$string = $TGI::QUERY{string};

TGI::set("val1", $string);
TGI::set("val2", "testing");
TGI::set("val3", "1..2..3");
exit 3
