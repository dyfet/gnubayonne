#!/usr/bin/perl
#
# Copyright (C) 2000-2001 Open Source Telecom Corporation.
#  
# This file is free software; as a special exception the author gives
# unlimited permission to copy and/or distribute it, with or without 
# modifications, as long as this notice is preserved.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

use lib $ENV{'SERVER_LIBEXEC'};

use TGI;
use Sys::Syslog;
use MIME::Base64;

openlog('TGI', 'nowait', 'daemon');

$subject = undef;
$sender = undef;
$from = undef;
$to = undef;
$bc = undef;
$cc = undef;
$sensitivity = undef;
$importance = undef;
$name = undef;
$msg = undef;
$fwd = undef;

$subject = $TGI::QUERY{subject} if $TGI::QUERY{subject};
$sender = $TGI::QUERY{sender} if $TGI::QUERY{sender};
$from = $TGI::QUERY{from} if $TGI::QUERY{from};
$to = $TGI::QUERY{to} if $TGI::QUERY{to};
$bc = $TGI::QUERY{bc} if $TGI::QUERY{bc};
$cc = $TGI::QUERY{cc} if $TGI::QUERY{cc};
$sensitivity = $TGI::QUERY{sensitivity} if $TGI::QUERY{sensitivity};
$importance = $TGI::QUERY{importance} if $TGI::QUERY{importance};

if(!$subject)
	$subject = "VPIM Messsage";

if(!$sender)
	$sender = "VPIM User";

if(!$from)
	$from = '"Telephone Answering" <non-mail-user\@localhost>';

if(!$to)
	$to = "General Delivery <0\@localhost>";

if(!$sensitivity)
	$sensitivity = "Private";

if(!$importance)
	$importance = "High";

open(OUTPUT, "|/usr/sbin/sendmail -f$sender $deliver");
print OUTPUT "Subject: $subject\n";
print OUTPUT "From: $from\n";

putAddr("To", $to);
if($bc)
	putAddr("Bcc", $bc);

if($cc)
	putAddr("Cc", $cc);

now = time();

print OUTPUT 'MIME-Version: 1.0 (Voice 2.0)\n';
print OUTPUT 'Content-type: Multipart/Voice-Message; Version=2.0;\n';
print OUTPUT '  Boundry="MessageBoundry"\n';
print OUTPUT 'Content-Transfer-Encoding: 7bit\n';
print OUTPUT "Message-ID: $TGI::PORT-$now\@$TGI::NODE\n";
print OUTPUT "Sensitivity: $sensitivity\n";
print OUTPUT "Importance: $importance\n";

if($name)
	putAudio($name, 
		"Content-Disposition: inline; voice=Originator-Spoken-Name",
		"Content-ID: $name@$TGI::NODE");

if($fwd)
	putAudio($fwd,
		"Content-Description: Forwarded Message Annotation",
		"Content-Disposition: inline; voice=Voice-Message");

if($msg)
	putAudio($msg,
		"Content-Description: Bayonne Voice Message",
		"Content-Disposition: inline; voice=Voice-Message; filename=$msg");

close(OUTPUT);
exit 0;

sub putAddr
{
	$atype = $_[0];
	my(@addr) = split(',', $_[1]);

	foreach(@addr)
		print OUTPUT "$atype: $_\n";
}

sub putAudio
{
	my($msgfile)=$_[0];
	my($h1) = $_[1];
	my($h2) = $_[2];
	my($buffer) = "";
	
	print OUTPUT "\n--MessageBoundry\n";

	if($msgfile =~ /\.wav$/)
		print OUTPUT "Content-Type: audio/x-wav\n";
	else
		print OUTPUT "Content-Type: audio/basic\n";

	print OUTPUT "Content-Transfer-Encoding: Base64\n";
	print OUTPUT "$h1\n";
	print OUTPUT "$h2\n";

	print "\n";
	
	open(AUDIO, "<$msgfile");
	while(<AUDIO>)
	{
		sysread(AUDIO, $buffer, 60);
		print OUTPUT MIME::Base64::encode($buffer);
	}
	close(AUDIO);	
}
