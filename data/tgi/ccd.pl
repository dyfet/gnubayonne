#!/usr/bin/perl -w
#
# ccd.pl -- ccscript DBI access module
#
# Args:
# ccd.dbname: database to attach to
# ccd.dbuser: username to log on to database as
# ccd.dbpass: password to log on to database with
# ccd.op: database operation (SELECT, INSERT, UPDATE...)
# ccd.table: table to query
# ccd.columns: columns or fieldnames to operate on
# ccd.where: WHERE statement without the where
# ccd.values: values to insert for INSERT op or update for UPDATE op
#
# Returns:
# return: return value (1 is good, 0 is bad)
# ccd.rows: number of rows returned or modified by operation
# ccd.
#
# I think that this API is broken to start with: it's a pain to clear
# the globals that are used to pass arguments to this script every
# time you call it so that leftover arguments don't trigger unexpected
# behavior.  For example:
#
# set %ccd.where "something"
# libexec 10 ccd.pl %ccd.where ...
#
# time passes...
#
# set %ccd.op "some other op";
# libexec 10 ccd.pl %ccd.where %ccd.op
# 
# Now the second call is going to have its behavior modified if you
# use boilerplate code to call ccd.pl!  If you manually construct the
# libexec call then you won't include %ccd.where because you're not
# using it, so that's ok.  But I'll bet most people won't do that.
#
# Thanks to Benjamin Miller <BGMiller@dccinc.com> for pointing out a 
# call-by-value problem with the scrub() routine.
#
# (C) 2002 Open Source Telecom Corporation

use TGI;
use DBI;
use strict;
use vars qw($err $dbh);
use DBD::Pg;
use sigtrap qw(die normal-signals); # grep for atexit in perlfaq8(1) for why
if (defined $ENV{'SERVER_LIBEXEC'}) {
  use lib $ENV{'SERVER_LIBEXEC'};
}

END {
  if(defined $dbh) { 
    $dbh->disconnect();
  }
}

sub error {
  my $error = shift;

  TGI::set(return => '-1');
  if(defined $error) {
    TGI::set(tgierror => $error);
  }
  else {
    TGI::set(tgierror => '0');
  }
  exit 0;
}

my $table = $TGI::QUERY{'ccd.table'};
scrub(\$table);

my $dbname = $TGI::QUERY{'ccd.dbname'};
scrub(\$dbname);

my $dbuser = $TGI::QUERY{'ccd.dbuser'};
scrub(\$dbuser);
if(! defined $dbuser) {
  $dbuser = "postgres";
}

my $dbpass = $TGI::QUERY{'ccd.dbpass'};
scrub(\$dbpass);
if(! defined $dbpass) {
  $dbpass = "";
}

my $columns = $TGI::QUERY{'ccd.columns'};
scrub(\$columns);

my $where = $TGI::QUERY{'ccd.where'};
scrub(\$where);

my $op = $TGI::QUERY{'ccd.op'};
scrub(\$op);

my $values = $TGI::QUERY{'ccd.values'};
scrub(\$values);
# FIXME should probably check to see if op is defined and valid here
# but I'm not sure that we're not going to be called for things that
# don't access the database (in which case op would be undefined)...
warn "Connecting to database...\n";
my $dbh = DBI->connect("dbi:Pg:dbname=$dbname", "$dbuser", "$dbpass");
if(! $dbh) {
    error "Cannot connect: $DBI::errstr";
}

warn "Preparing statement...\n";
my $sth;
my $sql;

# We build these incrementally so that when perl reports an undefined
# value on a certain line, we can tell immediately which variable is
# causing the problem via the debugger.
if($op =~ /select/i) {
  $sql = "SELECT";
  $sql .= " $columns";
  $sql .= " FROM $table";
  $sql .= " WHERE $where";
}

if($op =~ /insert/i) {
  $sql = "INSERT INTO";
  $sql .= " $table";
  
  $sql .= " ($columns)";
  $sql .= " VALUES";
  $sql .= " ($values)";
  if(defined $where) {
    $sql .= " WHERE $where";
  }
}

if($op =~ /update/i) {
  $sql = "UPDATE";
  $sql .= " $table SET";
  $sql .= " $columns = ";
  $sql .= " $values";
  if(defined $where) {
    $sql .= " WHERE $where";
  }
}

$sth = $dbh->prepare_cached($sql);
warn "$sql\n";

if(! defined $sth) {
    error "Cannot prepare SQL statement: " . $dbh->errstr;
}

warn "Executing statement...\n";
$err = $sth->execute();
if(! defined $err) { # returns undef if error
    error "Cannot execute statement: ". $dbh->errstr;
}

my $dataref;
if($op =~ /select/i){
  warn "Fetching data...\n";
  $dataref = $sth->fetchall_arrayref();
  TGI::set('ccd.rows' => scalar @$dataref);

#dataref is a reference to an array $dataref
# ARRAY() $$dataref[0]
#  ARRAY() $$dataref[0][0]
#   column
#   column
#   column

  my $packed;
  if(scalar @$dataref == 1) {
    my $packed = join(',', @{${$dataref}[0]});
    TGI::set('ccd.row0' => $packed);
  }
}
TGI::set('return' => 1);
exit 0;

# This routine undefines any variables passed that have contents which
# mean "cleared" such as the nullstring "", "(null)", etc.
#
# The reason why we're checking for nullstrings is because Bayonne
# doesn't have the ability to delete variables, only wipe their
# contents.  So when the ccscript has been running for a while it may
# get in a situation where it needs to remove a variable (which has
# been set before) to signal to this script that we don't really want
# to specify columns for this operation, etc.  Since we can't wipe
# variables in the ccscript so that they pass the ! defined check used
# throughout this code, we clear them in the ccscript instead (so
# they're set to "") and check their contents here to see if they are
# equal to the nullstring "".

sub scrub {
  my $arg = shift;
  if(! defined $$arg || $$arg eq '' || $$arg eq '(null)') {
    $$arg = undef;
  }
}
