# Copyright (C) 2000-2001 Open Source Telecom Corporation.
#  
# This file is free software; as a special exception the author gives
# unlimited permission to copy and/or distribute it, with or without 
# modifications, as long as this notice is preserved.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
# TGI perl support for TGI version 1.0 and 1.1

package TGI;

$PROTOCOL = "1.00";
$SERVER = undef;
$VERSION = undef;
$CONTROL = undef;
$PROTOCOL = undef;
$DIGITS = undef;
$QUERY = undef;
$CLID = undef;
$DNID = undef;
$PORT = "0";
$TOKEN = " ";
$PAIRS = "&";
$NODE = undef;
$AUDIOFILE = undef;

$| = 1;

@POLICIES = split(/ /, $ENV{'SERVER_POLICIES'}) if $ENV{'SERVER_POLICIES'};
$VERSION = $ENV{'SERVER_VERSION'} if $ENV{'SERVER_VERSION'};
$CONTROL = $ENV{'SERVER_CONTROL'} if $ENV{'SERVER_CONTROL'};
$SERVER = $ENV{'SERVER_SOFTWARE'} if $ENV{'SERVER_SOFTWARE'};
$PROTOCOL = $ENV{'SERVER_VERSION'} if $ENV{'SERVER_VERSION'};
$PROTOCOL = $ENV{'SERVER_PROTOCOL'} if $ENV{'SERVER_PROTOCOL'};
$NODE = $ENV{'SERVER_NODE'} if $ENV{'SERVER_NODE'};
$TOKEN = $ENV{'SERVER_TOKEN'} if $ENV{'SERVER_TOKEN'};
$PAIRS = $ENV{'SERVER_TOKEN'} if $ENV{'SERVER_TOKEN'};
$PORT = $ENV{'PORT_NUMBER'} if $ENV{'PORT_NUMBER'};
$CLID = $ENV{'PORT_CLID'} if $ENV{'PORT_CLID'};
$DNID = $ENV{'PORT_DNID'} if $ENV{'PORT_DNID'};
$DIGITS = $ENV{'PORT_DIGITS'} if $ENV{'PORT_DIGITS'};
$AUDIOFILE = $ENV{'TEMP_AUDIO'} if $ENV{'TEMP_AUDIO'};

if($ENV{'PORT_QUERY'})
{
	my($query_string) = $ENV{'PORT_QUERY'};
	my(@pairs) = split($PAIRS, $query_string);
	foreach(@pairs)
	{
		# This catches equal signs in the keyvalue pair.
		# Look up "greediness" in the regex docs to explain the '?'
                $_ =~ /(.*?)=(.*)/;
                my($keyword, $value) = ($1, $2);
		$QUERY{lc($keyword)} = $value;
	}
}	

if ($ENV{'SERVER_RUNTIME'})
{
	chdir $ENV{'SERVER_RUNTIME'};
	$TempFile::TMPDIRECTORY = "$ENV{'SERVER_RUNTIME'}/temp";
};

1;

sub size
{
	my($self) = @_;
	my($variable) = shift;
	my($size) = shift;

	print "SIZE$TOKEN$PORT$TOKEN$variable$TOKEN$size\n";
};

sub set
{
	my($self) = @_;
	my($variable) = shift;
	my($value) = shift;
	
	print "SET$TOKEN$PORT$TOKEN$variable$TOKEN$value\n";
};

sub put
{
	my($self) = @_;
	my($variable) = shift;
	my($value) = shift;

	print "PUT$TOKEN$variable$TOKEN$value\n";
};

sub post
{
	my($self) = @_;
	my($variable) = shift;
	my($value) = shift;
	print "POST$TOKEN$variable$TOKEN$value\n";
};

sub down
{
	print "DOWN\n";
}

sub idle
{
	print "IDLE$TOKEN$PORT\n";
}

sub busy
{
	print "BUSY$TOKEN$PORT\n";
}

sub compile
{
	print "COMPILE\n";
}

sub exit
{
	my($exitcode) = shift;
	my($pid) = getpid();
	print "EXIT$TOKEN$pid$TOKEN$exitcode\n";
}

