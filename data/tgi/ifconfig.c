/* Copyright (C) 2000-2001 Open Source Telecom Corporation.
 
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software 
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>

int main(int argc, char **argv)
{
	char buffer[8192];
	struct ifconf ifc;
	int fd = socket(AF_INET, SOCK_DGRAM, 0);
	unsigned count;
	unsigned idx = 0;
	struct ifreq devifreq;
	char ifaddr[65];
	char ifmask[65];
	char ifmtu[65];
	char *ifname, *prefix;
	char *query = getenv("PORT_QUERY");
	char *cp;
	struct sockaddr_in *addr;

	/* set default query arguments */
	putenv("interface=lo");
	putenv("prefix=net");

	cp = query;
	while(cp && *cp)
	{
		*cp = tolower(*cp);
		++cp;
	}

        while(query)
        {
                cp = strchr(query, '&');
                if(cp)
                        *cp = 0;
                putenv(query);
                if(cp)
                        query = ++cp;
                else
                        query = NULL;
        }

	if(fd < 0)
		exit(-1);

	ifc.ifc_len = sizeof(buffer);
	ifc.ifc_buf = buffer;

	if(ioctl(fd, SIOCGIFCONF, &ifc) < 0)
		exit(-1);

	count = ifc.ifc_len / sizeof(struct ifreq);

	ifname = getenv("interface");
	prefix = getenv("prefix");

	while(idx < count)
	{
		if(!strcasecmp(ifc.ifc_req[idx].ifr_name, ifname))
			break;
		++idx;
	}		

	printf("size %s.name 16\n", prefix);
	printf("size %s.addr 16\n", prefix);
	printf("size %s.mask 16\n", prefix);
	printf("size %s.mtu 6\n", prefix);

	printf("set %s.name %s\n", prefix, ifc.ifc_req[idx].ifr_name);

	if(idx < count)
	{
		if(ifc.ifc_req[idx].ifr_addr.sa_family != AF_INET)
			idx = count;
	}

	if(idx >= count)
	{
		printf("set %s.addr 0.0.0.0\n", prefix);
		printf("set %s.mask 0.0.0.0\n", prefix);
		printf("set %s.mask 0\n", prefix);
		exit(0);
	}

	addr = (struct sockaddr_in *)&ifc.ifc_req[idx].ifr_addr;
	printf("set %s.addr %s\n", prefix, inet_ntoa(addr->sin_addr));
	
	strcpy(devifreq.ifr_name, ifc.ifc_req[idx].ifr_name);
	
	if(ioctl(fd, SIOCGIFNETMASK, &devifreq) == -1)
		printf("set %s.mask 255.255.255.255\n", prefix);
	else
	{
		addr = (struct sockaddr_in *)&devifreq.ifr_addr;
		printf("set %s.mask %s\n", prefix, inet_ntoa(addr->sin_addr));
	}

	if(ioctl(fd, SIOCGIFMTU, &devifreq) == -1)
		printf("set %s.mtu 0\n", prefix);
	else
		printf("set %s.mtu %d\n", prefix, devifreq.ifr_mtu);
	exit(0);
}
