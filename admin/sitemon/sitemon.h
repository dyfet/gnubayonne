// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <fx.h>
#include <string.h>
#if defined(__WIN32__) && !defined(__CYGWIN32__)
#include <winsock2.h>
#include <io.h>
#define	snprintf	_snprintf
#else
#include <unistd.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#endif

#define	MAX_NODES	32

#pragma	pack(1)

typedef struct {
        time_t  update;
        char name[16];
        struct in_addr addr;
	unsigned long uptime;
	unsigned long calls;
        unsigned char version;
        unsigned char buddies;
        unsigned char spansize;
        unsigned short ports;
        char stat[840];
}       statnode_t;

#pragma	pack()

class Node
{
private:
	friend class MyTable;

	FXHorizontalFrame *row;
	FXLabel *lname, *llive, *lstat, *luptime, *lcalls;

	statnode_t packet;
	struct sockaddr_in addr;
	unsigned short port;
	char live[5];
	char name[17];
	char uptime[12];
	char calls[12];

	void init(statnode_t *packet, struct sockaddr_in *addr);
	void status(statnode_t *packet);

public:
	Node();
};

class MyTable : public FXMainWindow
{
private:
	FXDECLARE(MyTable);

	FXFont *fixed, *general;	
	FXTooltip *tooltip;
	FXMenubar *menubar;
	FXTimer *timer;
	FXMenuPane *filemenu, *controlmenu;
	int warning, expires;
	FXColor red, yellow, green;

	FXScrollWindow *contents;
	FXVerticalFrame *frame;
	FXHorizontalFrame *headers;
	unsigned nodes, maxnodes;
	Node *node;

public:
	inline FXColor getGreen(void)
		{return green;};

	inline FXFont *getFixed(void)
		{return fixed;};

	inline FXFont *getGeneral(void)
		{return general;};

	inline FXVerticalFrame *getFrame(void)
		{return frame;};

	long onMenu(FXObject *object, FXSelector sel, void *ptr);
	long onTimer(FXObject *object, FXSelector sel, void *ptr);
	long onPacket(FXObject *object, FXSelector sel, void *ptr);

	enum
	{
		ID_TIMER = FXMainWindow::ID_LAST,
		ID_PACKET,
		ID_COMPILE,
		ID_DOWN,
		ID_RESTART,
		ID_INTERFACE,
		ID_QUIT		// must be last!
	};

	MyTable();
	~MyTable();
};

void setInterface(void);

extern FXApp *myapp;
extern MyTable *mytable;
extern int so;
