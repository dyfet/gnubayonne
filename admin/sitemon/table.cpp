// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "sitemon.h"
#include <time.h>

#define	TITLE	"Bayonne Site Monitor"

static const FXchar *fixedTypeface(void)
	{return myapp->reg().readStringEntry("DESKTOP", "fixed-typeface", "fixed");};

static int fixedSize(void)
	{return myapp->reg().readIntEntry("DESKTOP", "fixed-size", 12);};

static const FXchar *generalTypeface(void)
        {return myapp->reg().readStringEntry("DESKTOP", "general-typeface", "helvetica");};

static int generalSize(void)
        {return myapp->reg().readIntEntry("DESKTOP", "general-size", 10);};

static int regHeight(void)
	{return myapp->reg().readIntEntry("SITEMON", "height", 600);};

static int regWidth(void)
	{return myapp->reg().readIntEntry("SITEMON", "width", 800);};

static int regTop(void)
	{return myapp->reg().readIntEntry("SITEMON", "top", 0);};

static int regLeft(void)
	{return myapp->reg().readIntEntry("SITEMON", "left", 0);};

static int regNodes(void)
	{return myapp->reg().readIntEntry("BAYONNE", "nodes", MAX_NODES);};

MyTable::MyTable() : 
FXMainWindow(myapp, TITLE, NULL, NULL, DECOR_ALL, regLeft(), regTop(), regWidth(), regHeight())
{
	tooltip = new FXTooltip(myapp);
	FXVerticalFrame *interior;

	mytable = this;

	fixed = new FXFont(myapp, fixedTypeface(), fixedSize());
	general = new FXFont(myapp, generalTypeface(), generalSize());

	warning = myapp->reg().readIntEntry("BAYONNE", "warning-time", 15);
	expires = myapp->reg().readIntEntry("BAYONNE", "exipire-time", 30);
	green = myapp->reg().readColorEntry("BAYONNE", "active-color",
		FXRGB(32,160,32));

	red = myapp->reg().readColorEntry("BAYONNE", "expire-color",
		FXRGB(160, 0, 0));

	yellow = myapp->reg().readColorEntry("BAYONNE", "warning-color",
		FXRGB(160, 160, 0));

	menubar = new FXMenubar(this, 
		LAYOUT_SIDE_TOP | LAYOUT_FILL_X);

	new FXHorizontalSeparator(this, 
		LAYOUT_SIDE_TOP|LAYOUT_FILL_X|SEPARATOR_GROOVE);

	interior = new FXVerticalFrame(this,
		LAYOUT_SIDE_TOP|FRAME_THICK|LAYOUT_FILL_X|LAYOUT_FILL_Y);

        headers = new FXHorizontalFrame(interior,
                LAYOUT_FILL_ROW | FRAME_LINE | LAYOUT_FILL_X);

	contents=new FXScrollWindow(interior,
		LAYOUT_SIDE_TOP|FRAME_NONE|LAYOUT_FILL_X|LAYOUT_FILL_Y);

	frame=new FXVerticalFrame(contents, 
		FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X|LAYOUT_FILL_Y);

	frame->setVSpacing(0);
	frame->setPadBottom(0);
	frame->setPadTop(0);
	frame->setPadLeft(0);
	frame->setPadRight(0);

	new FXLabel(headers, "node", NULL, 
		JUSTIFY_LEFT | LAYOUT_FIX_WIDTH | FRAME_NONE, 0, 0, 130);
	new FXLabel(headers, "live", NULL,
		JUSTIFY_LEFT | LAYOUT_FIX_WIDTH | FRAME_NONE, 140, 0, 40);
	new FXLabel(headers, "calls", NULL,
		JUSTIFY_LEFT | LAYOUT_FIX_WIDTH | FRAME_NONE, 190, 0, 70);
	new FXLabel(headers, "stat", NULL,
		JUSTIFY_LEFT | LAYOUT_FILL_X | FRAME_NONE, 270);

	maxnodes = regNodes();
	node = new Node[maxnodes];
	nodes = 0;

	filemenu = new FXMenuPane(this);

	new FXMenuCommand(filemenu, 
		"&Interface", NULL, this, MyTable::ID_INTERFACE);

	new FXHorizontalSeparator(filemenu,
                LAYOUT_SIDE_TOP|LAYOUT_FILL_X|SEPARATOR_GROOVE);

	new FXMenuCommand(filemenu, 
		"&Quit\tCtl-Q",NULL,myapp,FXApp::ID_QUIT);

	new FXMenuTitle(menubar, "&File", NULL, filemenu);

	controlmenu = new FXMenuPane(this);

	new FXMenuCommand(controlmenu, 
		"&Compile", NULL, this, MyTable::ID_COMPILE);

	new FXMenuCommand(controlmenu,
		"&Down", NULL, this, MyTable::ID_DOWN);

	new FXMenuCommand(controlmenu,
		"&Restart", NULL, this, MyTable::ID_RESTART);

	new FXMenuTitle(menubar, "&Control", NULL, controlmenu);

	timer = myapp->addTimeout(1000, this, ID_TIMER);
};

MyTable::~MyTable()
{
	myapp->removeTimeout(timer);

	delete filemenu;
	delete controlmenu;
	delete fixed;
	delete general;

	myapp->reg().writeIntEntry("SITEMON", "height", getHeight());
	myapp->reg().writeIntEntry("SITEMON", "width", getWidth());
	myapp->reg().writeIntEntry("SITEMON", "top", getY());
	myapp->reg().writeIntEntry("SITEMON", "left", getX());
	myapp->reg().write();
}

long MyTable::onMenu(FXObject *obj, FXSelector sel, void *)
{
	FXuint id = SELID(sel);
	switch(id)
	{
	}
	return 1;
}

long MyTable::onTimer(FXObject *obj, FXSelector sel, void *)
{
	time_t now, diff;
	
	time(&now);
	unsigned i = 0;
	while(i < nodes)
	{
		diff = now - node[i].packet.update;
		if(diff > 999)
			diff = 999;
		snprintf(node[i].live, 4, "%d", diff);
		if(diff >= expires)
			node[i].llive->setTextColor(red);
		else if(diff >= warning)
			node[i].llive->setTextColor(yellow);
		node[i].llive->setText(node[i].live);
		++i;
	}

	myapp->addTimeout(1000, this, ID_TIMER);
	return 1;
}

long MyTable::onPacket(FXObject *obj, FXSelector sel, void *)
{
	struct sockaddr_in addr;
	statnode_t packet;
#ifdef	__WIN32__
	int alen = sizeof(addr);
#else
	size_t alen = sizeof(addr);
#endif
	char *buffer = (char *)&packet;
	int rtn;
	unsigned i;

	memset(&packet, 0, sizeof(packet));
	memset(&addr, 0, sizeof(addr));
	rtn = recvfrom(so, buffer, sizeof(packet), 0, (struct sockaddr *)&addr, &alen);

	if(rtn < sizeof(packet))
		return 1;

	if(strncmp(buffer, "*MON", 4))
		return 1;

	for(i = 0; i < nodes; ++i)
		if(!memcmp(&addr, &node->addr, sizeof(addr)))
			break;

	if(i == maxnodes)
		return 1;

	if(i == nodes)
		node[nodes++].init(&packet, &addr);
	else
		node[i].status(&packet);

	return 1;
}

FXDEFMAP(MyTable) MyTableMap[] =
{
	FXMAPFUNCS(SEL_COMMAND, MyTable::ID_COMPILE, MyTable::ID_QUIT, MyTable::onMenu),
	FXMAPFUNC(SEL_TIMEOUT, MyTable::ID_TIMER, MyTable::onTimer),
	FXMAPFUNC(SEL_IO_READ, MyTable::ID_PACKET, MyTable::onPacket)

};

FXIMPLEMENT(MyTable, FXMainWindow, MyTableMap, ARRAYNUMBER(MyTableMap))
