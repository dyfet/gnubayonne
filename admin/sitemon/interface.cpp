// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "sitemon.h"

#ifdef	__WIN32__
WSAEVENT hEvent = WSA_INVALID_EVENT;
WSADATA wsd;
#endif

void setInterface(void)
{
	const char *ap = myapp->reg().readStringEntry("BAYONNE", "interface", "*");
	int pp = myapp->reg().readIntEntry("BAYONNE", "monitor", 7070);

	struct sockaddr addr;
	struct sockaddr_in *sin = (struct sockaddr_in *)&addr;
	unsigned long *ul = (unsigned long *)&sin->sin_addr;

#ifdef	__WIN32__
	if(hEvent == WSA_INVALID_EVENT)
	{
		WORD version = MAKEWORD (2, 2);
		WSAStartup(version, &wsd);
		hEvent = WSACreateEvent();
	}
#endif

	int myso = socket(AF_INET, SOCK_DGRAM, 0);

	if(so > -1)
	{
#ifdef	__WIN32__
		myapp->removeInput(hEvent, INPUT_READ);
#else
		myapp->removeInput(so, INPUT_READ);
#endif
		shutdown(so, 2);
#ifdef	__WIN32__
		closesocket(so);
#else
		close(so);
#endif
		so = -1;
	}

	if(myso < 0)
		return;

	memset(&addr, 0, sizeof(addr));
	*ul = htonl(INADDR_ANY);
	if(strcmp(ap, "*"))
		*ul = (unsigned long)inet_addr(ap);

	sin->sin_port = htons((unsigned short)pp);
	sin->sin_family = AF_INET;
	
	if(bind(myso, &addr, sizeof(struct sockaddr_in)))
		return;

	so = myso;
#ifdef	__WIN32__

	WSAEventSelect(so, hEvent, FD_READ);
	myapp->addInput(hEvent, INPUT_READ, mytable, MyTable::ID_PACKET);
#else
	myapp->addInput(so, INPUT_READ, mytable, MyTable::ID_PACKET);
#endif
}
	
