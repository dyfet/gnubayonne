// Copyright (C) 2000 Open Source Telecom Corporation.
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "sitemon.h"
#include <time.h>

Node::Node()
{
	row = new FXHorizontalFrame(mytable->getFrame(), 
		LAYOUT_FILL_ROW | FRAME_NONE | LAYOUT_FILL_X);

	row->setVSpacing(0);
	row->setPadBottom(0);
	row->setPadTop(0);
	row->setPadLeft(row->getPadLeft() - 1);

	strcpy(name, "");
	strcpy(live, "");
	strcpy(calls, "");

	lname = new FXLabel(row, name, NULL,
		JUSTIFY_LEFT | LAYOUT_FIX_WIDTH | FRAME_NONE, 0, 0, 130);
	lname->setFont(mytable->getGeneral());

	lname->setPadTop(1);
	lname->setPadBottom(0);

	llive = new FXLabel(row, live, NULL,
		JUSTIFY_LEFT | LAYOUT_FIX_WIDTH | FRAME_NONE, 140, 0, 40);
	llive->setFont(mytable->getGeneral());
	

	llive->setPadTop(1);
	llive->setPadBottom(0);

	lcalls = new FXLabel(row, calls, NULL,
		JUSTIFY_LEFT | LAYOUT_FIX_WIDTH | FRAME_NONE, 190, 0, 70);
	lcalls->setFont(mytable->getGeneral());

	lcalls->setPadTop(1);
	lcalls->setPadBottom(0);

	packet.stat[packet.ports] = 0;
	lstat = new FXLabel(row, "", NULL,
		 JUSTIFY_LEFT | LAYOUT_FILL_X | FRAME_NONE, 270);
	lstat->setFont(mytable->getFixed());

	lstat->setPadTop(1);
	lstat->setPadBottom(0);
}

void Node::init(statnode_t *data, struct sockaddr_in *from)
{
	memcpy(&addr, from, sizeof(addr));
	memcpy(&packet, data, sizeof(packet));
	
	strncpy(name, packet.name, 16);
	name[16] = 0;
	strcpy(live, "0");
	snprintf(calls, sizeof(calls), "%ld", packet.calls);
	time(&packet.update);
	packet.stat[packet.ports] = 0;

	lname->setText(name);
	llive->setText(live);
	llive->setTextColor(mytable->getGreen());
	lcalls->setText(calls);
	lstat->setText(packet.stat);
}

void Node::status(statnode_t *data)
{
	memcpy(&packet, data, sizeof(packet));
	strcpy(live, "0");
	snprintf(calls, sizeof(calls), "%ld", packet.calls);
	time(&packet.update);
	packet.stat[packet.ports] = 0;
	llive->setText(live);
	llive->setTextColor(mytable->getGreen());
	lcalls->setText(calls);
	lstat->setText(packet.stat);
}


