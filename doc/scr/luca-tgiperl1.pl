#!/usr/bin/perl
# use lib '/usr/libexec/bayonne/';     # 1.0.x
use lib '/usr/local/libexec/bayonne/'; # 1.2.x
use TGI;
$fatt1 = $TGI::QUERY{'var1'};
$fatt2 = $TGI::QUERY{'var2'};
$result = $fatt1*$fatt2;  # any operation
TGI::set("res",$result);
exit
